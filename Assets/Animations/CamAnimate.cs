﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamAnimate : MonoBehaviour {

    private Animator anim;


    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetFloat("animateSettings", 0f);
        anim.SetFloat("animateChapter", 0f);
    }

    public void AnimateSettings()
    { 
        if (anim.GetFloat("animateSettings") > 0f)
        {
            anim.SetFloat("animateSettings", -1f);
        }
        else
        {
            anim.SetFloat("animateSettings", 1f);
        }
    }

    public void AnimateChapter()
    {
        if (anim.GetFloat("animateChapter") > 0f)
        {
            anim.SetFloat("animateChapter", -1f);
        }
        else
        {
            anim.SetFloat("animateChapter", 1f);
        }
    }
}
