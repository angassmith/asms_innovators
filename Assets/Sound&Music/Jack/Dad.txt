Script 2

S2 L1 �Jack, did I hear correctly?�

S2 L2 �Are you trying to get your sibling to do your chores?�

S2 L3 �You know we have a roster, a chores roster.�

S2 L4 �We divide up the house chores and share them equally.�

S2 L5 �Sure, kids, let�s show it to Sam.�

S2 L6 �See Jack, we all have an equal share of chores.�

S2 L7 �Chores are everyone�s responsibility.�

S2 L8 �Does your family share the chores?�