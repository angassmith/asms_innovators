﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseTransition : MonoBehaviour
{
    public GameObject go;

    public bool transition = false;

    public float changeCutoffTime = 1.0f;

    private float cutOff = 0.0f;

    public void UpdateTransition()
    {
        transition = true;
    }

    public void Update()
    {

        if (transition != false)
        {
            cutOff += Time.deltaTime;

            go.GetComponent<Renderer>().material.SetFloat("_Cutoff", cutOff);
        }


    



    /*if (cutOff < changeCutoffTime)
    {
        cutOff += Time.deltaTime;
    }
    go.GetComponent<Renderer>().material.SetFloat("_Cutoff", cutOff);*/
    }
}