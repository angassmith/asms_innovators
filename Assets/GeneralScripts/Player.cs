﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using RevolitionGames;
using RevolitionGames.Inventory;
using RevolitionGames.Interaction;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.ThirdPerson;
using RevolitionGames.Utilities.Alex;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames
{
	[RequireComponent(typeof(ThirdPersonUserControl))]
	[RequireComponent(typeof(ThirdPersonCharacter))]
	[RequireComponent(typeof(PlayerAnimationController))]
	public class Player : MonoBehaviour, IInteractor
	{
		[SerializeField]
		private PlayerInteractDetector _interactDetector;
		public PlayerInteractDetector InteractDetector { get { return _interactDetector; } }

		[SerializeField]
		[HideInInspector]
		private ThirdPersonUserControl _rotateInputHandler;
		/// <summary>
		/// I have no idea if this is what <see cref="ThirdPersonUserControl"/> is actually for,
		/// but disabling it stops the player from rotating (this affects the keyboard, idk about the joystick)
		/// </summary>
		public ThirdPersonUserControl RotateInputHandler {
			get {
				if (_rotateInputHandler == null) _rotateInputHandler.GetComponent<ThirdPersonUserControl>();
				return _rotateInputHandler;
			}
		}

		[SerializeField]
		[HideInInspector]
		private ThirdPersonCharacter _forwardInputHandler;
		/// <summary>
		/// I have no idea if this is what <see cref="ThirdPersonCharacter"/> is actually for,
		/// but disabling it stops forward movement (this affects the keyboard, idk about the joystick)
		/// </summary>
		public ThirdPersonCharacter ForwardInputHandler {
			get {
				if (_forwardInputHandler == null) _forwardInputHandler = GetComponent<ThirdPersonCharacter>();
				return _forwardInputHandler;
			}
		}

		[SerializeField]
		[HideInInspector]
		private PlayerAnimationController _animationController;
		public PlayerAnimationController AnimationController {
			get {
				if (_animationController == null) _animationController.GetComponent<PlayerAnimationController>();
				return _animationController;
			}
		}

		private void Start()
		{
			this.LogIfEditorFieldNull(_interactDetector, "_interactDetector");
			//	this.LogIfEditorFieldNull(_rotateInputHandler, "_rotateInputHandler");
			//	this.LogIfEditorFieldNull(_forwardInputHandler, "_forwardInputHandler");
			//	this.LogIfEditorFieldNull(_animationController, "_animationController");
		}

        //private void Update()
        //{
        //    Debug.Log("Player's Position" + transform.position);
        //}
    }
}

//*/