﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using Fungus;
using RevolitionGames.Interaction;
using RevolitionGames.NPCs;
using RevolitionGames.Quests;
using RevolitionGames.SceneManagement;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames
{
	public class PlayModeStage : GameStage<PlayModeStage, PlayModeSceneData>
	{
		private StorySlot _storySlot;
		public StorySlot StorySlot { get { return _storySlot; } }
		public Story Story { get { return _storySlot.Story; } }

		private readonly NPCHost _npcHost;
		public NPCHost NPCHost { get { return _npcHost; } }

		private readonly PlayerUI _playerUI;
		public PlayerUI PlayerUI { get { return _playerUI; } }

		public PlayModeStage(Game game, StorySlot storySlot, NPCHost npcHost, PlayerUI playerUI)
			: base(game)
		{
			ThrowIfArgNull(storySlot, "storySlot");
			ThrowIfArgNull(npcHost, "npcHost");
			ThrowIfArgNull(playerUI, "playerUI");

			this._storySlot = storySlot;
			this._npcHost = npcHost;
			this._playerUI = playerUI;
		}

		//Idk if this is how saving will be set up
		//	public static PlayModeStage Load(Game game, DirectoryInfo path, )
		//	{
		//		ThrowIfArgNull(game, "game");
		//		ThrowIfArgNull(path, "path");
		//	
		//		if (!path.Exists)
		//		{
		//			return new PlayModeStage(
		//				game: game,
		//				story: new Story(
		//					storyProvider:
		//			);
		//		}
		//	}

		protected override void OnEnter(SceneTransition transition)
		{
			base.OnEnter(transition);


		}

		protected override void OnFirstSceneLoaded(SceneTransition transition)
		{
			base.OnFirstSceneLoaded(transition);

			//Start the story after the SceneData, Player, etc have been set up
			//Using Completed might not be ideal, but it's ok
			//Actually, no, it means that dialogues etc will only open after
			//loading & transition effects have finished, which is good
			transition.Completed += delegate {
				this._storySlot.Activate(containingStage: this);
			};

			this.PlayerUI.gameObject.SetActive(true);
		}

		protected override void OnExit(SceneTransition transition)
		{
			base.OnExit(transition);

			this.PlayerUI.gameObject.SetActive(false);

			//Stop all executing flowcharts (relevant when exiting while one is running == exiting without saving)
			//TODO: Maybe remove DontDestroyOnLoad from flowcharts to avoid the need for this?
			var flowcharts = (
				from transform in new TransformEnumerator(this.Game.FlowchartParent.transform)
				let flowchart = transform.GetComponent<Flowchart>()
				where flowchart != null
				select flowchart
			);
			foreach (var f in flowcharts)
			{
				f.StopAllBlocks();
				f.StopAllCoroutines();
				f.StopAllBlocks();
				f.StopAllCoroutines();
				f.StopAllBlocks();
				f.StopAllCoroutines();
				f.StopAllBlocks();
				f.StopAllCoroutines();
				Debug.Log("#510: Flowchart has executing blocks: " + f.HasExecutingBlocks(), f);
				Game.RunAfter(
					new WaitForSeconds(5f),
					delegate {
						Debug.Log("#511: Flowchart has executing blocks: " + f.HasExecutingBlocks(), f);
					}
				);

				var executionCtx = f.GetComponent<StoryFlowchartExecutionContext>();
				if (executionCtx != null) {
					UnityEngine.Object.Destroy(executionCtx);
				}
			}
		}

		/// <summary>
		/// Switches to the specified play-mode scene. Do not use this to switch to scenes in other stages,
		/// such as main menu or minigame scenes (use methods in <see cref="Game"/> instead).
		/// </summary>
		public SceneTransition SwitchToPlaymodeScene(string sceneName, int spawnPointNum)
		{
			return StartSceneTransition(
				new PlayModeSceneData(
					stage: this,
					sceneName: sceneName,
					spawnPointNum: spawnPointNum,
					interactableHost: new InteractableHost()
				)
			);
		}
	}
}

//*/