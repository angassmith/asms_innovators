﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Interaction;
using RevolitionGames.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UObject = UnityEngine.Object;

namespace RevolitionGames
{
	public class PlayModeSceneData : SceneData<PlayModeStage, PlayModeSceneData>
	{
		public int SpawnPointNum { get; private set; }

		private Player _player; // Set when player is spawned
		public Player Player { get { return _player; } }

		private readonly InteractableHost _interactableHost;
		public InteractableHost InteractableHost { get { return _interactableHost; } }

		public PlayModeSceneData(PlayModeStage stage, string sceneName, int spawnPointNum, InteractableHost interactableHost)
			: base(stage, sceneName)
		{
			ThrowIfArgNull(interactableHost, "interactableHost");
			if (spawnPointNum < 0) throw new ArgumentOutOfRangeException("spawnPointNum", spawnPointNum, "Cannot be less than zero.");

			this.SpawnPointNum = spawnPointNum;
			this._interactableHost = interactableHost;
		}

		protected override void OnSceneLoaded(SceneTransition transition)
		{
			GameObject playerObject = SpawnPlayer(this.SpawnPointNum);

            // If the player was spawned in the scene
            if (playerObject != null) {
                // Set the player interact detector
                _player = playerObject.GetComponent<Player>();

				if (_player == null) Debug.Log("No Player component attached");
				else Debug.Log("Spawned and set player");

                // Enable the player UI
                this.Stage.PlayerUI.gameObject.SetActive(true);
            }
            // Otherwise hide the player UI
            else {
                this.Stage.PlayerUI.gameObject.SetActive(false);
            }
		}

		private GameObject SpawnPlayer(int spawnPointNum)
		{
			Debug.Log("Spawning the player");

            // Get the spawn point in the scene with the correct spawn point number
            SpawnPoint[] spawnPointsInScene = UObject.FindObjectsOfType<SpawnPoint>();
            SpawnPoint spawnPoint = spawnPointsInScene.SingleOrDefault(x => x.Number == spawnPointNum);

            // If the spawn point was found, it won't be the default value for the class
            // Spawn the player at the spawn point
            if (spawnPoint != default(SpawnPoint)) {
                //Debug.Log("Spawning player at " + spawnPoint.transform.position);
                GameObject player = UObject.Instantiate(
					original: this.Stage.Game.PlayerCharacterPrefab,
					position: spawnPoint.transform.position,
					rotation: spawnPoint.transform.rotation
				);
                return player;
            }

            // The spawn point wasn't found, return null
            Debug.LogError("Spawn point " + spawnPointNum + " not found in scene " + SceneManager.GetActiveScene().name);
            return null;
		}

		public override SceneChangeEffect CreateSceneEnterEffect(SceneTransition transition)
		{
			return new FadeSceneChangeEffect(
				coroutineHost: this.Game,
				startOpacity: 1,
				endOpacity: 0,
				canvasElementGroup: transition.PreparedLoadScreen
			);
		}

		public override SceneChangeEffect CreateSceneExitEffect(SceneTransition transition)
		{
			return new FadeSceneChangeEffect(
				coroutineHost: this.Game,
				startOpacity: 0,
				endOpacity: 1,
				canvasElementGroup: transition.PreparedLoadScreen
			);
		}
	}
}

//*/