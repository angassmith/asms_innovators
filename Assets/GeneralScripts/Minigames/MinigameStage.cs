﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using RevolitionGames.Interaction;
using RevolitionGames.NPCs;
using RevolitionGames.Quests;
using RevolitionGames.SceneManagement;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames
{
	public class MinigameStage : GameStage<MinigameStage, MinigameSceneData>
	{
		private readonly Story _story;
		public Story Story { get { return _story; } }

		public MinigameStage(Game game)
			: base(game)
		{

		}

		//Idk if this is how saving will be set up
		//	public static PlayModeStage Load(Game game, DirectoryInfo path, )
		//	{
		//		ThrowIfArgNull(game, "game");
		//		ThrowIfArgNull(path, "path");
		//	
		//		if (!path.Exists)
		//		{
		//			return new PlayModeStage(
		//				game: game,
		//				story: new Story(
		//					storyProvider:
		//			);
		//		}
		//	}

		/// <summary>
		/// Switches to the specified minigame scene. Do not use this to switch to scenes in other stages,
		/// such as play mode or main menu scenes (use methods in <see cref="Game"/> instead).
		/// </summary>
		public void SwitchToMinigameScene(string sceneName)
		{
			StartSceneTransition(
				new MinigameSceneData(
					stage: this,
					sceneName: sceneName
				)
			);
		}
	}
}

//*/