﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Interaction;
using RevolitionGames.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UObject = UnityEngine.Object;

namespace RevolitionGames
{
	public class MinigameSceneData : SceneData<MinigameStage, MinigameSceneData>
	{


		public MinigameSceneData(MinigameStage stage, string sceneName)
			: base(stage, sceneName)
		{
			
		}

		public override SceneChangeEffect CreateSceneEnterEffect(SceneTransition transition)
		{
			return DummySceneChangeEffect.Empty;
		}

		public override SceneChangeEffect CreateSceneExitEffect(SceneTransition transition)
		{
			return DummySceneChangeEffect.Empty;
		}
	}
}

//*/