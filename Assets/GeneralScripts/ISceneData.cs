﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace RevolitionGames
{
	public interface ISceneData : IDiscardable
	{
		IGameStage Stage { get; }
		string SceneName { get; }
		bool HasEntered { get; }
		bool HasExited { get; }
		bool IsInUse { get; }

		SceneChangeEffect CreateSceneEnterEffect(SceneTransition transition);
		SceneChangeEffect CreateSceneExitEffect(SceneTransition transition);
	}
}

//*/