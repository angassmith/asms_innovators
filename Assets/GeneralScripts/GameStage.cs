﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames
{
	public abstract class GameStage<TStage, TSceneData> : IGameStage
		where TStage : GameStage<TStage, TSceneData>
		where TSceneData : SceneData<TStage, TSceneData>
	{
		//Shortcuts for easy access
		public static TStage Current {
			get { return Game.Current.GetCurrentStageAs<TStage>(); }
		}
		public static bool IsCurrent { get { return Game.Current.CurrentStage is TStage; } }

		private TSceneData _currentScene;
		public TSceneData CurrentScene {
			get { return _currentScene; }
			protected set {
				var oldValue = _currentScene;
				_currentScene = value;
				SceneChanged.Fire(this, new ValueChangedEventArgs<ISceneData>(oldValue, value)); //Fire even if equal
			}
		}
		ISceneData IGameStage.CurrentScene { get { return _currentScene; } }
		public event EventHandler<ValueChangedEventArgs<ISceneData>> SceneChanged;

		private readonly Game _game;
		public Game Game { get { return _game; } }

		public bool HasEntered { get; private set; }
		public bool HasExited { get; private set; }
		public bool IsInUse { get { return HasEntered && !HasExited; } }

		public bool IsDiscarded { get { return HasExited; } }
		public event EventHandler<DiscardEventArgs> Discarded;

		public GameStage(Game game)
		{
			if (game == null) throw new ArgumentNullException("game");

			this._game = game;

			this.Game.StageChanged += OnStageChanged;
			this.Game.SceneTransitionStarted += OnSceneTransitionStarted;
			this.Game.SceneTransitionUnloadedOldScene += OnSceneTransitionUnloadedOldScene;
		}

		//Stage enter methods
		protected virtual void OnEnteringTransitionStarted(SceneTransition transition) { }
		protected virtual void OnEnter(SceneTransition transition) { }
		protected virtual void OnFirstSceneLoaded(SceneTransition transition) { }

		//Stage exit methods
		protected virtual void OnExitingTransitionStarted(SceneTransition transition) { }
		protected virtual void OnExit(SceneTransition transition) { }

		private void OnSceneTransitionStarted(object sender, SceneTransitionEventArgs e)
		{
			if (e.Transition.IsStageTransition)
			{
				if (ReferenceEquals(e.Transition.NewScene.Stage, this))
				{
					OnEnteringTransitionStarted(e.Transition);
				}
				else if (ReferenceEquals(e.Transition.OldScene.Stage, this))
				{
					OnExitingTransitionStarted(e.Transition);
				}
			}
		}

		private void OnSceneTransitionUnloadedOldScene(object sender, SceneTransitionEventArgs e)
		{
			//If the target game stage is this instance
			if (ReferenceEquals(e.Transition.NewScene.Stage, this))
			{
				//Always set CurrentStage and fire events, even if CurrentStage doesn't actually change
				this.CurrentScene = (TSceneData)e.Transition.NewScene; //Safe to cast as NewScene.Stage == this
			}
		}

		private void OnStageChanged(object sender, ValueChangedEventArgs<IGameStage> e)
		{
			Debug.Log("#610");

			var transition = this.Game.OngoingTransition;

			//The transition is null when changing to the first scene and stage, i.e. when
			//changing to _initial. The actual scene change happens before there is an
			//instance of Game, so a SceneTransition, which requires an instance of Game,
			//is not possible. Instead, when transition is null, OnEnter() etc is run immediately.
			if (transition == null)
			{
				Debug.Log("#501");

				HasEntered = true;

				OnEnter(transition: null);

				//The first stage change happens in Game.Start(), so the scene is already loaded
				OnFirstSceneLoaded(transition: null);

				return;
			}

			if (ReferenceEquals(e.OldValue, this))
			{
				Debug.Log("#611");

				this.Game.StageChanged -= OnStageChanged;
				this.Game.SceneTransitionStarted -= OnSceneTransitionStarted;

				HasExited = true;
				OnExit(transition);
				OnDiscarded();

				Debug.Log("#612");
			}
			else if (ReferenceEquals(e.NewValue, this))
			{
				Debug.Log("#503");
				HasEntered = true;

				transition.NewSceneLoaded += delegate {
					Debug.Log("#504");
					OnFirstSceneLoaded(transition);
				};

				OnEnter(transition);
			}
		}


		protected SceneTransition StartSceneTransition(TSceneData target)
		{
			//Events on Game are used to set CurrentScene (see OnSceneTransitionUnloadedOldScene),
			//so this is all that's needed. It's worth putting in a method here as otherwise it's
			//not clear that this will work. Note that this method is protected.
			return this.Game.StartTransition(target);
		}

		private void OnDiscarded()
		{
			OnDiscardedImpl();
			Discarded.Fire(this, new DiscardEventArgs(this));
			Discarded = null;
		}

		protected virtual void OnDiscardedImpl() { }


		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}
}

//*/