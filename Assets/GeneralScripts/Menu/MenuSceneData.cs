﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames
{
	public class MenuSceneData : SceneData<MenuStage, MenuSceneData>
	{


		public MenuSceneData(MenuStage stage, string sceneName)
			: base(stage, sceneName)
		{

		}

		public override SceneChangeEffect CreateSceneEnterEffect(SceneTransition transition)
		{
			return new FadeSceneChangeEffect(
				coroutineHost: this.Game,
				startOpacity: 1,
				endOpacity: 0,
				canvasElementGroup: transition.PreparedLoadScreen
			);
		}

		public override SceneChangeEffect CreateSceneExitEffect(SceneTransition transition)
		{
			return new FadeSceneChangeEffect(
				coroutineHost: this.Game,
				startOpacity: 0,
				endOpacity: 1,
				canvasElementGroup: transition.PreparedLoadScreen
			);
		}
	}
}

//*/