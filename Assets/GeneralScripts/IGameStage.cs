﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests;
using RevolitionGames.Utilities.Alex;

namespace RevolitionGames
{
	public interface IGameStage : IDiscardable
	{
		Game Game { get; }
		ISceneData CurrentScene { get; }
		bool HasEntered { get; }
		bool HasExited { get; }
		bool IsInUse { get; }

		event EventHandler<ValueChangedEventArgs<ISceneData>> SceneChanged;
	}
}

//*/