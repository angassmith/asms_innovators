﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Interaction;
using RevolitionGames.SceneManagement;
using RevolitionGames.Quests;
using RevolitionGames.NPCs;
using RevolitionGames.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Xml.Linq;
using System.IO;
using System.Xml.Schema;
using RevolitionGames.Utilities.Alex;
using UnityEngine.Events;
using RevolitionGames.Saving;
using RevolitionGames.Saving.Types;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames
{
	[Serializable]
	public class Game : MonoBehaviour
	{
		private static Game _current;
		public static Game Current {
			get {
				if (_current == null)
				{
					Game[] foundGames = FindObjectsOfType<Game>();

					if (foundGames.Length < 1) throw new InvalidOperationException("No Game is present in the scene.");
					if (foundGames.Length > 1) throw new InvalidOperationException("Multiple Games are present in the scene.");
					Debug.Log("Found a single Game (at path '" + foundGames[0].GetHierarchyPath() + "')");

					_current = foundGames[0];
				}
				return _current;
			}
		}

		/// <summary>Shortcut for <see cref="Game.Current"/>.CurrentStage.CurrentScene</summary>
		public static ISceneData CurScene {
			get {
				Debug.Log("Current: " + Current);
				Debug.Log("Current.CurrentStage: " + Current.CurrentStage);
				Debug.Log("Current.CurrentStage.CurrentScene: " + Current.CurrentStage.CurrentScene);
				return Current.CurrentStage.CurrentScene;
			}
		}

		public const string XExperienceNamespaceUri = "http://www.revolitiongames.com/experience";
		public static readonly XNamespace XExperienceNamespace = XNamespace.Get(XExperienceNamespaceUri);

		private static readonly Version _currentVersion = new Version(); //TODO
		public static Version CurrentVersion { get { return _currentVersion; } }

		//No decision made as this won't completely work
		//	//Decision about how saving will work:
		//	//All systems function similarly to Story, where the save path for that system is injected,
		//	//and all not-completely-saved dependencies are injected, but the actual stuff that's saved is
		//	//not injected. For the partially-saved sub-systems, the same thing appl--- [TODO: Figure out issue
		//	//with direct parent not being able to provide the path when the child is injected]

		//Idk if these will go here
		//	public const string RevGamesDirName = "RevolitionGames";
		//	public const string RootSaveDirName = "Save";
		//	public const string ScenesSaveDirName = "Scenes";
		//	public const string 


		private IGameStage _currentStage;
		public IGameStage CurrentStage {
			get { return _currentStage; }
			private set {
				var oldValue = _currentStage;
				_currentStage = value;
				StageChanged.Fire(this, new ValueChangedEventArgs<IGameStage>(oldValue, value)); //Fire even if equal

				//The new stage's CurrentScene property is set when the StageChanged event is fired, so we can now
				//fire the SceneChanged event (which triggers SceneData.OnEnter() and so on)
				SceneChanged.Fire(
					this,
					new ValueChangedEventArgs<ISceneData>(
						oldValue == null ? null : oldValue.CurrentScene, //Null when switching to the first stage
						value.CurrentScene
					)
				);

				//Also have to clear and set up the scene change event
				if (oldValue != null) {
					oldValue.SceneChanged -= OnSceneChanged;
				}
				value.SceneChanged += OnSceneChanged;
			}
		}
		private void OnSceneChanged(object sender, ValueChangedEventArgs<ISceneData> e) {
			SceneChanged.Fire(this, e);
		}
		public event EventHandler<ValueChangedEventArgs<IGameStage>> StageChanged;
		public event EventHandler<ValueChangedEventArgs<ISceneData>> SceneChanged;



		private SceneTransition _ongoingTransition;
		public SceneTransition OngoingTransition {
			get { return _ongoingTransition; }
		}
		public bool TransitionIsOngoing { get { return _ongoingTransition != null; } }
		/// <summary>
		/// Runs after <see cref="OngoingTransition"/> is set but before
		/// <see cref="SceneTransitionStarted"/> fires. <see cref="SceneTransition"/> uses
		/// this event to trigger its Start method.
		/// </summary>
		internal event EventHandler<SceneTransitionEventArgs> PreviewSceneTransitionStart;
		public event EventHandler<SceneTransitionEventArgs> SceneTransitionStarted;
		public event EventHandler<SceneTransitionEventArgs> SceneTransitionUnloadedOldScene;
		public event EventHandler<SceneTransitionEventArgs> SceneTransitionLoadedNewScene;
		public event EventHandler<SceneTransitionEventArgs> SceneTransitionCompleted;


		public PlayModeStageSaveHost SaveHost { get; private set; }


		[SerializeField] //And edit in inspector
        private NotificationText _notificationText;
		public NotificationText NotificationText { get; private set; }

        [SerializeField]
		private SceneChanger _sceneChanger;
        public SceneChanger SceneChanger { get { return _sceneChanger; } }

		[SerializeField]
		private PlayerUI _playerUI;

		/// <summary>
        /// The prefab to use for the player character.
        /// </summary>
        [SerializeField] // edit in inspector
        private GameObject _playerCharacterPrefab;
		public GameObject PlayerCharacterPrefab { get { return _playerCharacterPrefab; } }

		[SerializeField]
		private GameObject _flowchartParent;
		public GameObject FlowchartParent { get { return _flowchartParent; } }

		public bool InteractionOccurring = false; //This should probably be changed, idk what it's for

		public Game()
		{

		}

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void OnApplicationStarted()
		{
			//This method will be run exactly once, before the first scene is loaded (and regardless of
			//whether there is a Game in the scene, as it's static). It runs before any GameObjects have
			//received any initialization messages (eg. Awake()).
			//The unity documentation only talks about the behaviour of the AfterSceneLoad option (so says
			//that it runs after Awake())
			//Source of info that this runs before Awake:
			//https://feedback.unity3d.com/suggestions/control-when-method-with-runtimeinitializeonloadmethodattribute-will-run

			//Load the initial scene (if it is not the current scene).
			//However, this is not an instant operation. Therefore, all GameObjects in the scenes that
			//were loaded in the editor will receive Awake/OnEnable callbacks before the scene loading
			//command takes effect (which in turn results in the GameObjects in the scene receiving
			//OnDisable/OnDestroy callbacks). After this, the specified scene will begin loading.
			var sceneToLoad = SceneInfo.Initial.Name;
			if (SceneManager.GetActiveScene().name != sceneToLoad) {
				SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Single);
			}
		}

		private void Start()
		{
			UnityInit.LogIfEditorFieldNull(_notificationText, "_notificationText");
			UnityInit.LogIfEditorFieldNull(_sceneChanger, "_sceneChanger");
			UnityInit.LogIfEditorFieldNull(_playerUI, "_playerUI");
			UnityInit.LogIfEditorFieldNull(_playerCharacterPrefab, "_playerCharacterPrefab");

			this.SaveHost = new PlayModeStageSaveHost(
				game: this,
				storyProgressSaver: new DummyStoryProgressSaver()
			);

			this.StageChanged += (s, e) => {
				var oldPlayModeStage = e.OldValue as PlayModeStage;
				if (oldPlayModeStage != null)
				{
					this.SaveHost.UpdateTo(stage: oldPlayModeStage);
				}
			};

			//	this._interactableHost = new InteractableHost();
			//	
			//	this._npcHost = new NPCHost(new NPCGroup(NPCCodeNames.NPCGroups.AllNPCs, 0.02f));
			//	
			//	//Load NPCs (using NPCInitMonos, hence this is in Awake())
			//	new CSharpNPCProvider().InitializeNPCs(this._npcHost); //TODO get the NPC Host to populate its data dynamically
			//	
			//	ReloadStoryAndProgress();
			//	
			//	SceneManager.sceneLoaded += SpawnPlayer;

			//	//Temp:
			//	LoadAndPlay(new DirectoryInfo(Application.persistentDataPath + "/RevGames/Experience/Save/Main"));

			if (SceneManager.GetActiveScene().name == SceneInfo.Initial.Name)
			{
				Debug.Log("#430");
				ActivateInitialSceneStage();
			}
			Debug.Log("#431: " + SceneManager.GetActiveScene().name + ", " + SceneInfo.Initial.Name);
        }

		

		private void ActivateInitialSceneStage()
		{
			var gameLoadScene = new GameLoadSceneData(
				stage: new GameLoadStage(game: this)
			);

			//As the scene is already loaded (before an instance of Game exists), a transition
			//doesn't make sense (and won't work). The new stage's CurrentScene property still
			//needs to be set somehow, so is set by the GameLoadStage constructor. This does
			//not trigger SceneData.OnEnter() and so on, as Game.CurrentStage is not yet equal
			//to the new stage.
			//CurrentStage is then set, which triggers Game.StageChanged and Game.SceneChanged,
			//the latter of which triggers SceneData.OnEnter() and so on.

			this.CurrentStage = gameLoadScene.Stage;
		}

		public void SwitchToMenu()
		{
			//Switching scenes is not instantaneous, so we have to let Update(), OnDestroy(), etc calls run
			//with the current stage, before switching to the next stage once the current scene is unloaded

			StartTransition(
				new MenuSceneData(
					stage: new MenuStage(game: this),
					sceneName: SceneInfo.MainMenu.Name
				)
			);

			//	//Both closures and unsubscription are needed here, but C# doesn't provide any easy way
			//	//to unsubscribe a closure from within that closure. There are therefore three choices that I know of:
			//	// - Use a mutable Ref<T> type or mutate local variables captured by a closure (idk if this works)
			//	//   to give the delegate access to itself
			//	// - Set a field equal to that closure and have the closure access that field (problematic if multiple
			//	//   closures run at the same time)
			//	// - Use a manual closure class
			//	// - Use an event that automatically clears all subscribers
			//	//I went with the last one as it seemed the simplest
			//	CurrentStage.CurrentScene.PrevSceneUnloaded_AutoUnsub += delegate
			//	{
			//		//Wait until the current scene is unloaded before changing the game stage
			//		this.CurrentStage = nextStage;
			//	};
		}

		public void LoadAndPlay()
		{
			PlayModeSceneData firstScene;
			PlayModeStage newStage = this.SaveHost.PrevSave.Create(
				game: this,
				storyProvider: new CSharpStoryProvider(),
				playerUI: this._playerUI,
				firstScene: out firstScene
			);

			StartTransition(firstScene);

			//	Play(
			//		new PlayModeSceneData(
			//			stage: new PlayModeStage(
			//				game: this,
			//				storySlot: new StorySlot(
			//					storyProvider: new CSharpStoryProvider(),
			//					saveData: this.SaveHost.PrevSave.StoryProgress
			//				),
			//				npcHost: new NPCHost(
			//					allNPCsGroup: new NPCGroup(NPCCodeNames.NPCGroups.AllNPCs, 0.02f),
			//					npcProvider: new CSharpNPCProvider()
			//				),
			//				playerUI: this._playerUI
			//			),
			//			sceneName: SceneInfo.Classroom.Name, //Temp
			//			spawnPointNum: 1, //Temp. Using an index rather than a location is also temporary
			//			interactableHost: new InteractableHost()
			//		)
			//	);

			SetNotificationTextSubcriptions(); //TODO: Refactor so this isn't needed
		}

		public void SwitchToMinigame(string firstSceneName)
		{
			SwitchToMinigame(
				new MinigameSceneData(
					stage:  new MinigameStage(game: this),
					sceneName: firstSceneName
				)
			);
		}

		public void SwitchToMinigame(MinigameSceneData newScene)
		{
			StartTransition(newScene);
		}

		public SceneTransition StartTransition(ISceneData newScene)
		{
			var transition = new SceneTransition(
				game: this,
				newScene: newScene
			);

			StartTransition(transition);

			return transition;
		}

		public void StartTransition(SceneTransition transition)
		{
			Debug.Log(
				"Attempting to start scene transition from scene '"
				+ transition.OldScene.SceneName
				+ "' to scene '"
				+ transition.NewScene.SceneName
				+ "'."
			);

			if (transition == null) throw new ArgumentNullException("transition");

			if (!ReferenceEquals(transition.Game, this)) throw new ArgumentException(
				"The specified transition applies to a different Game instance."
			);

			if (transition.HasStarted) throw new ArgumentException(
				"The specified transition has already started previously.",
				"transition"
			);

			if (!ReferenceEquals(transition.OldScene, CurrentStage.CurrentScene)) throw new ArgumentException(
				"The specified transition specifies that the old scene must be '" + transition.OldScene.SceneName + "' "
				+ "but the current scene is '" + CurrentStage.CurrentScene.SceneName + "'."
			);

			if (TransitionIsOngoing) throw new InvalidOperationException(
				"Cannot start a new transition from current scene '" + CurrentStage.CurrentScene.SceneName + "' "
				+ "to scene '" + transition.NewScene.SceneName + "' as there is already an ongoing transition "
				+ "from scene '" + _ongoingTransition.OldScene.SceneName + "' "
				+ "to scene '" + _ongoingTransition.NewScene.SceneName + "'."
			);

			//Set OngoingTransition property first to make sure it's value is
			//what the SceneTransition expects when starting
			_ongoingTransition = transition;

			//Trigger the SceneTransition to start executing
			PreviewSceneTransitionStart.Fire(this, new SceneTransitionEventArgs(OngoingTransition));

			//Fire started event after SceneTransition has set up stuff and started executing
			SceneTransitionStarted.Fire(this, new SceneTransitionEventArgs(OngoingTransition));

			//Set up scene load/unload events
			OngoingTransition.OldSceneUnloaded += (s, e) =>
			{
				this.SceneTransitionUnloadedOldScene.Fire(this, e);
				if (OngoingTransition.IsStageTransition) {
					CurrentStage = OngoingTransition.NewScene.Stage;
				}
			};
			OngoingTransition.NewSceneLoaded += (s, e) => this.SceneTransitionLoadedNewScene.Fire(this, e);

			//Set up what happens when the transition is completed
			OngoingTransition.Completed += (s, e) =>
			{
				this._ongoingTransition = null;
				// ^ Clearing OngoingTransition first is ok as the EventArgs provide the transition anyway

				this.SceneTransitionCompleted.Fire(this, e);
			};
		}

        //	/// <summary>
        //	/// Starts a new story, loads the first scene.
        //	/// </summary>
        //	public void StartNewStory() {
        //	    SceneChanger.LoadScene("classroom", 1);
        //	    this._story = new Story(new CSharpStoryProvider());
        //	    SetNotificationTextSubcriptions();
        //	}
		//	
        //	public void ReloadStory() {
        //	    ReloadStoryAndProgress();
        //	    //TODO Load appropriate scene
        //	    SetNotificationTextSubcriptions();
        //	}

        
		//TODO: Probably move this somewhere else
        /// <summary>
        /// Gets the notification text to subscribe to events from the current story.
        /// </summary>
        private void SetNotificationTextSubcriptions() {
            if (_notificationText != null && CurrentStage is PlayModeStage) {
                _notificationText.SubscribeToStoryEvents(((PlayModeStage)CurrentStage).Story);
            }
        }

		//Helper for easy access shortcuts
		/// <summary>
		/// Returns <see cref="CurrentStage"/> if it is of type <see cref="TStage"/>,
		/// otherwise throws an <see cref="InvalidOperationException"/>.
		/// </summary>
		public TStage GetCurrentStageAs<TStage>()
			where TStage : class, IGameStage
		{
			var stage = CurrentStage as TStage;
			if (stage == null) throw new InvalidOperationException("The current stage is not a '" + typeof(TStage).FullName + "'.");
			return stage;
		}

		//	public StorySlot CreateStorySlot(DirectoryInfo savePath)
		//	{
		//		return new StorySlot(
		//			storyProvider: new CSharpStoryProvider(),
		//			saveData: this.
		//			storySaveFolder: new DirectoryInfo(Path.Combine(savePath.FullName, "StoryProgress")),
		//			mainStoryProgressSchema: /*XmlSchema.Read(
		//				new StringReader(Resources.Load<TextAsset>("_Systems/Quests/Resources/StoryProgress.xsd").text),
		//				XmlUtils.XmlValidationExceptionUnityLogger
		//			)*/ new XmlSchema(),
		//			questProgressSchema: /*XmlSchema.Read(
		//				new StringReader(Resources.Load<TextAsset>("_Systems/Quests/Resources/QuestProgress.xsd").text),
		//				XmlUtils.XmlValidationExceptionUnityLogger
		//			)*/ new XmlSchema()
		//		);
		//	}
	}
}

//*/