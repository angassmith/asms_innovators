﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using RevolitionGames;

public class LoadScene : MonoBehaviour {

    [SerializeField]
    private string SceneToSwitchTo;

    public void LoadTheMinigame(string scenetoswitchto)
    {
        Game.Current.SwitchToMinigame(SceneToSwitchTo); //hi

    }

	
}
