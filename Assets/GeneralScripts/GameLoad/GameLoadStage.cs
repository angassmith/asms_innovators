﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.SceneManagement;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames
{
	public class GameLoadStage : GameStage<GameLoadStage, GameLoadSceneData>
	{
		/// <summary>
		/// Creates and sets up a <see cref="GameLoadStage"/>. Does not load the <see cref="SceneInfo.Initial"/>
		/// scene, as by the time there is an instance of <see cref="Game"/>, that has already been loaded.
		/// <para/>
		/// However, this does set the CurrentScene property
		/// (which does not trigger SceneData.OnEnter() as Game.CurrentScene is not equal to this new object),
		/// as the only other way to do so would be through a transition - which wouldn't work,
		/// as the scene's already been loaded.
		/// </summary>
		public GameLoadStage(Game game) : base(game)
		{
			this.CurrentScene = new GameLoadSceneData(stage: this);
		}

		protected override void OnFirstSceneLoaded(SceneTransition transition)
		{
			base.OnFirstSceneLoaded(transition);

			Debug.Log("#500");

			//	//Switch to the main menu after 3 seconds
			//	this.Game.StartCoroutine(
			//		CoroutineUtils.RunAfterDelay(
			//			seconds: 3f,
			//			action: delegate { Debug.Log("#433"); this.Game.SwitchToMenu(); }
			//		)
			//	);

			this.Game.SwitchToMenu();
		}
	}
}

//*/