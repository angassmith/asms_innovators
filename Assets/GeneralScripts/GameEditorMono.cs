﻿/*

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames
{
    /// <summary>
    /// A class used to provide an inspector for the current <see cref="Game"/>
    /// </summary>
    public class GameEditorMono : MonoBehaviour
    {
		[SerializeField]
		[HideInInspector]
		private Game _editedGame;
		public Game EditedGame {
			get { return _editedGame ?? (_editedGame = new Game()); }
		}
    }

}

//*/