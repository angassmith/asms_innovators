﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RevolitionGames.Choices;
using RevolitionGames.Inventory;
using RevolitionGames.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;
using RevolitionGames.Interaction;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames
{
	public class PlayerUI : MonoBehaviour
	{
		[SerializeField] //And edit in inspector
        private CameraFacingUI _interactIcon;
        public CameraFacingUI interactIcon { get { return _interactIcon; } }

		[SerializeField]
		private PlayerInventory _inventory;
		public PlayerInventory Inventory { get { return _inventory; } }

		[SerializeField]
		private Button _yesButton;
		public Button YesButton { get { return _yesButton; } }

		[SerializeField]
		private Button _noButton;
		public Button NoButton { get { return _noButton; } }

        [SerializeField]
        private Joystick _joystickcontrol;
        public Joystick JoystickControl { get { return _joystickcontrol; } }

        [SerializeField]
        private Button _phonebutton;
        public Button PhoneButton { get { return _phonebutton; }  }

        [SerializeField]
        private Button _invButton;
        public Button InvButton { get { return _invButton; } }

        [SerializeField]
        private Button _pauseButton;
        public Button PauseButton { get { return _pauseButton; } }

		[SerializeField]
		private AudioClip _doorInteractSound;
		public AudioClip DoorEnterSound { get { return _doorInteractSound; } }

		[SerializeField]
		private AudioClip _itemInteractSound;
		public AudioClip ItemCollectSound { get { return _itemInteractSound; } }

		[SerializeField]
		private Sprite _doorInteractIcon;
		public Sprite DoorInteractIcon { get { return _doorInteractIcon; } }

        public GameObject QuestEndCanvasPrefab;

        public CameraFacingUI camUIScript;

		private bool _dialogOpen;
		public bool DialogOpen {
			get { return _dialogOpen; }
			set {
				YesButton.interactable = !value;
				NoButton.interactable = !value;
                JoystickControl.enabled = !value;
                PhoneButton.interactable = !value;
                InvButton.interactable = !value;
				PauseButton.interactable = !value;
				//	Debug.Log("520: " + PlayModeSceneData.Current);
				//	Debug.Log("521: " + PlayModeSceneData.Current.Player);
				//	Debug.Log("522: " + PlayModeSceneData.Current.Player.ForwardInputHandler);
				//	Debug.Log("523: " + PlayModeSceneData.Current.Player.ForwardInputHandler.enabled);
				PlayModeSceneData.Current.Player.ForwardInputHandler.enabled = !value;
				PlayModeSceneData.Current.Player.RotateInputHandler.enabled = !value;
				PlayModeSceneData.Current.Player.AnimationController.enabled = !value;
				//TODO: Minigames button, inventory button, etc
				_dialogOpen = value;
			}
		}

        //void Update()
        //{
        //    if (camUIScript == null)
        //    {
        //        camUIScript = GetComponent<CameraFacingUI>();
        //    }
        //}

        /// <summary>
        /// Shows the interact icon above the given interactable.
        /// </summary>
        /// <param name="interactable">The interactable to indicate.</param>
        public void DisplayInteractIcon(Interactable interactable, InteractDetector detector, Sprite iconPref)
        {
            interactIcon.SetTarget(interactable, detector, iconPref);
            interactIcon.gameObject.SetActive(true);
        }

        /// <summary>
        /// Hides the interact icon.
        /// </summary>
        public void HideInteractIcon() {
            interactIcon.gameObject.SetActive(false);
        }

        public QuestEndCanvas DisplayQuestEnd(string name, IEnumerable<ChoiceData> choices) {
            GameObject canvas = Instantiate(QuestEndCanvasPrefab) as GameObject;
            QuestEndCanvas qeCanvas = canvas.GetComponent<QuestEndCanvas>();
            qeCanvas.SetQuestName(name);
            qeCanvas.AddChoices(choices);
            return qeCanvas;
        }

        /// <summary>
        /// Disables the UI on Awake.
        /// Calls DontDestroyOnLoad as well, as the DontDestroyOnLoad script may not have called Awake yet.
        /// </summary>
        private void Awake()
		{
            // Disable the UI in the loading scene
            // The UI will be re-enabled when the game starts
            if (SceneManager.GetActiveScene().buildIndex == 0) { //TODO: Improve this as it will at some point fail
                this.gameObject.SetActive(false);
            }

            DontDestroyOnLoad(this.gameObject);
		}
	}
}

//*/