﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.SceneManagement;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace RevolitionGames
{
	public abstract class SceneData<TStage, TSceneData> : ISceneData
		where TStage : GameStage<TStage, TSceneData>
		where TSceneData : SceneData<TStage, TSceneData>
	{
		//Shortcuts for easy access
		public static TSceneData Current {
			get { return Game.Current.GetCurrentStageAs<TStage>().CurrentScene; }
		}
		public static bool IsCurrent {
			get { return Game.Current.CurrentStage is TStage && Game.Current.CurrentStage.CurrentScene is TSceneData; }
		}

		private readonly TStage _stage;

		public TStage Stage { get { return _stage; } }
		IGameStage ISceneData.Stage { get { return _stage; } }

		public Game Game { get { return this.Stage.Game; } }

		public string SceneName { get; private set; }

		public bool HasEntered { get; private set; }
		public bool HasExited { get; private set; }
		public bool IsInUse { get { return HasEntered && !HasExited; } }

		public bool IsDiscarded { get { return HasExited; } }
		public event EventHandler<DiscardEventArgs> Discarded;

		//?//	public event EventHandler<SceneTransitionEventArgs> EnteringTransitionStarted;
		//?//	public event EventHandler<SceneTransitionEventArgs> EnteringTransitionStarted_AutoUnsub;
		//?//	public event EventHandler<SceneTransitionEventArgs> ExitingTransitionStarted;
		//?//	public event EventHandler<SceneTransitionEventArgs> ExitingTransitionStarted_AutoUnsub;

		/// <summary>
		/// Creates a new <see cref="ISceneData"/>.
		/// Note: The scene will not actually be loaded until a <see cref="SceneTransition"/> is created,
		/// and activated using <see cref="Game.StartTransition(SceneTransition)"/>.
		/// </summary>
		public SceneData(TStage stage, string sceneName)
		{
			ThrowIfArgNull(stage, "stage");
			ThrowIfArgNull(sceneName, "sceneName");

			if (!SceneInfo.ScenesByName.ContainsKey(sceneName)) throw new ArgumentException(
				"Scene '" + sceneName + "' is not registered in SceneInfo."
			);

			this._stage = stage;
			this.SceneName = sceneName;

			this.Stage.SceneChanged += OnSceneDataChanged;
			//	this.Stage.Game.StageChanged += OnStageChanged;

			this.Stage.Game.SceneTransitionStarted += OnSceneTransitionStarted;
		}


		public abstract SceneChangeEffect CreateSceneEnterEffect(SceneTransition transition);
		public abstract SceneChangeEffect CreateSceneExitEffect(SceneTransition transition);

		//Scene enter methods
		protected virtual void OnEnteringTransitionStarted(SceneTransition transition) { }
		/// <summary></summary>
		/// <param name="transition">Null when switching to the first scene. This should only be relevant to GameLoadSceneData.</param>
		protected virtual void OnEnter(SceneTransition transition) { }
		protected virtual void OnSceneLoaded(SceneTransition transition) { }

		//Scene exit methods
		protected virtual void OnExitingTransitionStarted(SceneTransition transition) { }
		protected virtual void OnExit(SceneTransition transition) { }


		

		private void OnSceneTransitionStarted(object sender, SceneTransitionEventArgs e)
		{
			if (ReferenceEquals(e.Transition.NewScene, this))
			{
				OnEnteringTransitionStarted(e.Transition);
				//?//	EnteringTransitionStarted.Fire(this, new SceneTransitionEventArgs(e.Transition));
				//?//	EnteringTransitionStarted_AutoUnsub.Fire(this, new SceneTransitionEventArgs(e.Transition));
				//?//	EnteringTransitionStarted_AutoUnsub = null;
			}
			else if (ReferenceEquals(e.Transition.OldScene, this))
			{
				OnExitingTransitionStarted(e.Transition);
				//?//	ExitingTransitionStarted.Fire(this, new SceneTransitionEventArgs(e.Transition));
				//?//	ExitingTransitionStarted_AutoUnsub.Fire(this, new SceneTransitionEventArgs(e.Transition));
				//?//	ExitingTransitionStarted_AutoUnsub = null;
			}
		}

		
		


		private void OnSceneDataChanged(object sender, ValueChangedEventArgs<ISceneData> e)
		{
			OnSceneOrStageChanged(oldScene: e.OldValue, newScene: e.NewValue);
		}
		//	private void OnStageChanged(object sender, ValueChangedEventArgs<IGameStage> e)
		//	{
		//		//A SceneData's Stage property determines what stage is activated when the SceneData is activated.
		//		//This means that CurrentScene and Stage will always point to each other (when the SceneData is active).
		//		//Because of this, this is safe to do:
		//		OnSceneOrStageChanged(
		//			oldScene: e.OldValue == null ? null : e.OldValue.CurrentScene, //Null when switching to the first scene
		//			newScene: e.NewValue.CurrentScene
		//		);
		//	}

		private void OnSceneOrStageChanged(ISceneData oldScene, ISceneData newScene)
		{
			var transition = this.Game.OngoingTransition;

			//The transition is null when changing to the first scene and stage, i.e. when
			//changing to _initial. The actual scene change happens before there is an
			//instance of Game, so a SceneTransition, which requires an instance of Game,
			//is not possible. Instead, when transition is null, OnEnter() etc is run immediately.
			if (transition == null)
			{
				HasEntered = true;

				OnEnter(transition: null);

				SceneUtils.SceneLoadedAutoUnsub += delegate {
					OnSceneLoaded(transition: null);
				};

				return;
			}

			if (ReferenceEquals(oldScene, this))
			{
				this.Stage.SceneChanged -= OnSceneDataChanged;
				//	this.Game.StageChanged -= OnStageChanged;
				this.Game.SceneTransitionStarted -= OnSceneTransitionStarted;

				HasExited = true;

				OnExit(transition);
				OnDiscarded();
			}
			else if (ReferenceEquals(newScene, this))
			{
				HasEntered = true;

				transition.NewSceneLoaded += delegate {
					OnSceneLoaded(transition);
				};
				OnEnter(transition);
			}
		}

		private void OnDiscarded()
		{
			Discarded.Fire(this, new DiscardEventArgs(this));
			OnDiscardedImpl();
		}
		protected virtual void OnDiscardedImpl() { }

		//	/// <summary>
		//	/// Loads or reloads the scene specified by <see cref="SceneName"/>, using whatever transitions are needed,
		//	/// and sets up any necessary properties and external state (after waiting for events in some cases).
		//	/// </summary>
		//	public SceneTransition Activate()
		//	{
		//		//	var prevScene = Game.CurScene;
		//		//	
		//		//	SceneUtils.SceneUnloadedAutoUnsub += scene => {
		//		//		if (scene.name == prevScene.SceneName && !IsDiscarded) {
		//		//			OnPrevSceneUnloaded();
		//		//			if (PrevSceneUnloaded_AutoUnsub != null) PrevSceneUnloaded_AutoUnsub.Invoke(scene);
		//		//		}
		//		//	};
		//		//	
		//		//	SceneUtils.SceneLoadedAutoUnsub += (scene, mode) => {
		//		//		if (scene.name == SceneName && !IsDiscarded) {
		//		//			OnSpecifiedSceneLoaded();
		//		//			StartLoadTransition(prev: prevScene);
		//		//			if (SceneLoaded_AutoUnsub != null) SceneLoaded_AutoUnsub.Invoke(scene, mode);
		//		//		}
		//		//	};
		//	
		//		ActivatedImpl();
		//	}

		//	#region Scene load/unload events
		//	
		//	private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		//	{
		//		if (scene.name == this.SceneName)
		//		{
		//			OnSceneLoaded();
		//			StartLoadTransition(prev: );
		//			SceneLoaded_AutoUnsub.Fire(this, EventArgs.Empty);
		//			SceneLoaded_AutoUnsub = null;
		//		}
		//	}
		//	
		//	private void OnSceneUnloaded(Scene scene)
		//	{
		//		if (scene.name == this.SceneName)
		//		{
		//			OnPrevSceneUnloaded();
		//			PrevSceneUnloaded_AutoUnsub.Fire(this, EventArgs.Empty);
		//			PrevSceneUnloaded_AutoUnsub = null;
		//		}
		//	}
		//	
		//	#endregion




		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion

	}
}

//*/