﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames
{
	public abstract class SceneChangeEffect
	{
		public bool HasStarted { get; private set; }
		public bool HasFinished { get; private set; }
		public bool IsInProgress { get { return HasStarted && !HasFinished; } }

		/// <summary>Note: All subscribers are automatically cleared after firing (as this only fires once).</summary>
		public event EventHandler<SceneChangeEffectEventArgs> Started;
		/// <summary>Note: All subscribers are automatically cleared after firing (as this only fires once).</summary>
		public event EventHandler<SceneChangeEffectEventArgs> Finished;

		public void Start(SceneTransition transition)
		{
			HasStarted = true;
			Started.Fire(this, new SceneChangeEffectEventArgs(this));
			Started = null;
			StartImpl(transition);
		}

		protected abstract void StartImpl(SceneTransition transition);

		protected void Finish()
		{
			HasFinished = true;
			Finished.Fire(this, new SceneChangeEffectEventArgs(this));
			Finished = null;
		}
	}

	public class SceneChangeEffectEventArgs : EventArgs
	{
		public SceneChangeEffect Effect { get; private set; }

		public SceneChangeEffectEventArgs(SceneChangeEffect effect)
		{
			if (effect == null) throw new ArgumentNullException("effect");

			this.Effect = effect;
		}
	}
}

//*/