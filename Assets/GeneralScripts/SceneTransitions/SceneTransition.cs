﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.UI;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.SceneManagement;
//	#if UNITY_ADS
//	using UnityEngine.Advertisements;
//	#endif

namespace RevolitionGames
{
	public sealed class SceneTransition
	{
		public Game Game { get; private set; }

		public ISceneData OldScene { get; private set; }
		public ISceneData NewScene { get; private set; }
		/// <summary>True if <see cref="OldScene"/>.Stage and <see cref="NewScene"/>.Stage are different.</summary>
		public bool IsStageTransition { get { return !ReferenceEquals(OldScene.Stage, NewScene.Stage); } }

		/// <summary>Note: Null while <see cref="HasStarted"/> is false.</summary>
		public SceneChangeEffect OldSceneExitEffect { get; private set; }
		/// <summary>Note: Null while <see cref="HasStarted"/> is false.</summary>
		public SceneChangeEffect NewSceneEnterEffect { get; private set; }

		private readonly ISceneChangeLoadScreenProvider LoadScreenProvider;
		/// <summary>Note: Null if no load screen is needed or <see cref="HasStarted"/> is false.</summary>
		public CanvasGroup PreparedLoadScreen { get; private set; }

		private readonly SceneTransitionEventArgs CachedEventArgs;

		/// <summary>Note: All subscribers are automatically cleared after firing (as this only fires once).</summary>
		public event EventHandler<SceneTransitionEventArgs> Started;
		/// <summary>Note: All subscribers are automatically cleared after firing (as this only fires once).</summary>
		public event EventHandler<SceneTransitionEventArgs> OldSceneUnloaded;
		/// <summary>Note: All subscribers are automatically cleared after firing (as this only fires once).</summary>
		public event EventHandler<SceneTransitionEventArgs> NewSceneLoaded;
		/// <summary>Note: All subscribers are automatically cleared after firing (as this only fires once).</summary>
		public event EventHandler<SceneTransitionEventArgs> Completed;

		public bool HasStarted { get; private set; }
		public bool OldSceneHasUnloaded { get; private set; }
		public bool NewSceneHasLoaded { get; private set; }
		public bool IsComplete { get; private set; }
		public bool IsInProgress { get { return HasStarted && !IsComplete; } }



		public SceneTransition(Game game, ISceneData newScene)
		{
			Debug.Log(
				"Attempting to create scene transition from current scene '"
				+ game.CurrentStage.CurrentScene.SceneName
				+ "' to scene '"
				+ newScene.SceneName
				+ "'."
			);

			ThrowIfArgNull(game, "game");
			ThrowIfArgNull(newScene, "newScene");

			var oldScene = game.CurrentStage.CurrentScene;

			if (newScene.HasEntered) throw new ArgumentException(
				"The target ISceneData has already been entered in the past. "
				+ "(Did you forget to create a new ISceneData instance (with the same scene name) "
				+ "and pass that in instead?)",
				"newScene"
			);
			if (!ReferenceEquals(oldScene.Stage, newScene.Stage) && newScene.Stage.HasEntered) {
				throw new ArgumentException(
					"The target IGameStage is different from the current stage (so the SceneTransition is "
					+ "also a stage transition), but has already been entered in the past. "
					+ "(Did you forget to create a new IGameStage instance and pass that in instead?)",
					"newScene"
				);
			}

			this.Game = game;
			this.OldScene = oldScene;
			this.NewScene = newScene;

			this.CachedEventArgs = new SceneTransitionEventArgs(this);

			this.LoadScreenProvider = new SceneChangeLoadScreenProvider();

			Game.PreviewSceneTransitionStart += Game_PreviewSceneTransitionStarted;
		}

		#region Scene transition execution (Note: these methods execute in display order)

		private void Game_PreviewSceneTransitionStarted(object sender, SceneTransitionEventArgs e)
		{
			if (ReferenceEquals(e.Transition, this)) {
				Start();
			}
		}

		private void Start()
		{
			Game.PreviewSceneTransitionStart -= Game_PreviewSceneTransitionStarted;

			this.PreparedLoadScreen = LoadScreenProvider.PrepareLoadScreen(transition: this);

			//	if (UnityEngine.Random.Range (0f, 1f) < 1f / 3) 
			//	{
			//		Debug.Log ("displaying ad");
			//		#if UNITY_ADS
			//		if (Advertisement.IsReady())
			//		{
			//			Advertisement.Show();
			//		}
			//		#endif
			//	}

			//Note: These methods must be called after PreparedLoadScreen is set up
			this.OldSceneExitEffect = OldScene.CreateSceneExitEffect(transition: this);
			this.NewSceneEnterEffect = NewScene.CreateSceneEnterEffect(transition: this);

			HasStarted = true;
			FireStarted();

			//Start the exit effect after firing the Started event to make the
			//effect's Started event run second. This order is easier to work with, and
			//subscribers can use the exit effect's events if needed anyway.
			OldSceneExitEffect.Finished += OnOldSceneExitEffectFinished;
			OldSceneExitEffect.Start(transition: this);
		}

		private void OnOldSceneExitEffectFinished(object sender, SceneChangeEffectEventArgs e)
		{
			OldSceneExitEffect.Finished -= OnOldSceneExitEffectFinished;

			SceneManager.LoadSceneAsync(NewScene.SceneName, LoadSceneMode.Single);
			// ^ AFAIK you don't actually have to yield the AsyncOperation, but I haven't tested this



			SceneManager.sceneUnloaded += OnPrevSceneUnloaded;
		}

		private void OnPrevSceneUnloaded(Scene scene)
		{
			if (scene.name == OldScene.SceneName) //Multiple scenes might unload so have to check
			{
				SceneManager.sceneUnloaded -= OnPrevSceneUnloaded;

				OldSceneHasUnloaded = true;
				FireOldSceneUnloaded();

				SceneManager.sceneLoaded += OnNewSceneLoaded;
			}
		}

		private void OnNewSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (scene.name != NewScene.SceneName) {
				Debug.LogError(
					"Expected scene '" + NewScene.SceneName + "' to load, "
					+ "but scene '" + scene.name + "' loaded instead."
				);
				return;
			}

			SceneManager.sceneLoaded -= OnNewSceneLoaded;

			NewSceneHasLoaded = true;
			FireNewSceneLoaded();

			//Start effect after firing loaded event, same as with started event
			NewSceneEnterEffect.Finished += OnNewSceneEnterEffectFinished;
			NewSceneEnterEffect.Start(transition: this);
		}

		private void OnNewSceneEnterEffectFinished(object sender, SceneChangeEffectEventArgs e)
		{
			NewSceneEnterEffect.Finished -= OnNewSceneEnterEffectFinished;

			Complete();
		}

		private void Complete()
		{
			IsComplete = true;
			FireCompleted();
		}

		#endregion


		#region Events

		private void FireStarted() {
			Started.Fire(this, CachedEventArgs);
			Started = null;
		}
		private void FireOldSceneUnloaded() {
			OldSceneUnloaded.Fire(this, CachedEventArgs);
			OldSceneUnloaded = null;
		}
		private void FireNewSceneLoaded() {
			NewSceneLoaded.Fire(this, CachedEventArgs);
			NewSceneLoaded = null;
		}
		private void FireCompleted() {
			Completed.Fire(this, CachedEventArgs);
			Completed = null;
		}

		#endregion


		#region Utilities

		private static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}

	public class SceneTransitionEventArgs : EventArgs
	{
		public SceneTransition Transition { get; private set; }

		public SceneTransitionEventArgs(SceneTransition transition)
		{
			if (transition == null) throw new ArgumentNullException("transition");

			this.Transition = transition;
		}
	}
}

//*/