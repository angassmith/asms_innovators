﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames
{
	public interface ISceneChangeLoadScreenProvider
	{
		/// <summary>Note: Returns null if no load screen is needed.</summary>
		CanvasGroup PrepareLoadScreen(SceneTransition transition);
		void CleanUpAllLoadScreens();
	}
}

//*/