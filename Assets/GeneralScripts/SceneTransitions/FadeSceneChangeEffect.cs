﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.SceneManagement
{
	public class FadeSceneChangeEffect : SceneChangeEffect
	{
		public const float DefaultFadeDuration = 0.8f;

		private readonly MonoBehaviour coroutineHost;
		private readonly CanvasGroup canvasElementGroup;

		public float StartOpacity { get; private set; }
		public float EndOpacity { get; private set; }
		public float StartTime { get; private set; }
		public float Duration { get; private set; }

		/// <summary>
		/// Creates a new <see cref="FadeSceneChangeEffect"/>.
		/// </summary>
		/// <param name="canvasElementGroup">Note: Can be null</param>
		public FadeSceneChangeEffect(MonoBehaviour coroutineHost, float startOpacity, float endOpacity, CanvasGroup canvasElementGroup, float duration = DefaultFadeDuration)
		{
			if (coroutineHost == null) throw new ArgumentNullException("coroutineHost");

			//Would validate that opacities & duration are in range but floats are approximate. Instead:
			startOpacity = Mathf.Clamp01(startOpacity);
			endOpacity = Mathf.Clamp01(endOpacity);
			duration = Math.Max(0, duration);

			this.coroutineHost = coroutineHost;
			this.StartOpacity = startOpacity;
			this.EndOpacity = endOpacity;
			this.canvasElementGroup = canvasElementGroup;
			this.StartTime = Time.time;
			this.Duration = duration;
		}

		protected override void StartImpl(SceneTransition transition)
		{
			if (canvasElementGroup == null) {
				Finish();
				return;
			}

			canvasElementGroup.interactable = true;
			coroutineHost.StartCoroutine(FadeCoroutine());
		}

		private IEnumerator FadeCoroutine()
		{
			while (Time.time < StartTime + Duration)
			{
				canvasElementGroup.alpha = Mathf.Lerp(StartTime, Duration, Time.time);
				yield return null;
			}
			Finish();
		}
	}
}

//*/