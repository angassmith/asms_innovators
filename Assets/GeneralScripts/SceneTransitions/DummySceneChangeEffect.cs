﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.SceneManagement
{
	public class DummySceneChangeEffect : SceneChangeEffect
	{
		public static readonly DummySceneChangeEffect Empty = new DummySceneChangeEffect();

		public DummySceneChangeEffect() { }

		protected override void StartImpl(SceneTransition transition)
		{
			Finish();
		}
	}
}

//*/