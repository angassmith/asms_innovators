﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.SceneManagement;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames
{
	public class SceneChangeLoadScreenProvider : ISceneChangeLoadScreenProvider
	{
		private ReadOnlyCollection<string> LoadMessages = new ReadOnlyCollection<string>(
			new[] {
				"Try playing some minigames using the phone icon",
				"NPCs will often give you hints about what to do next when you talk to them",
				"etc",
			}
		);

		public CanvasGroup PrepareLoadScreen(SceneTransition transition)
		{
			var oldSceneType = SceneInfo.GetSceneType(transition.OldScene.SceneName);
			var newSceneType = SceneInfo.GetSceneType(transition.NewScene.SceneName);
			
			if (oldSceneType == SceneType.Menu && newSceneType == SceneType.Menu)
			{
				return null; //No loading screen
			}
			else if (oldSceneType == SceneType.Minigame && newSceneType == SceneType.Minigame)
			{
				return null; //No loading screen
			}
			else if (oldSceneType == SceneType.Initial || newSceneType == SceneType.Initial)
			{
				return null; //No loading screen
			}
			else
			{
				var message = LoadMessages.RandomElement(rng: ThreadSafeRandom.Random);

				//TODO: Prepare the loading screen (& replace this)
				return null;
			}
			
		}

		public void CleanUpAllLoadScreens()
		{
			//TODO: Hide the loading screen
		}
	}
}

//*/