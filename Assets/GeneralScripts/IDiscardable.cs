﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RevolitionGames
{
	public interface IDiscardable
	{
		bool IsDiscarded { get; }

		/// <summary>
		/// Fired when the object is discarded (after <see cref="IsDiscarded"/> becomes true).
		/// Only fires once, so all subscribers are cleared after firing.
		/// </summary>
		event EventHandler<DiscardEventArgs> Discarded;
	}

	public class DiscardEventArgs : EventArgs
	{
		public IDiscardable DiscardedObj { get; private set; }

		public DiscardEventArgs(IDiscardable discardedObj)
		{
			if (discardedObj == null) throw new InvalidOperationException("discardedObj");

			this.DiscardedObj = discardedObj;
		}
	}


	[Serializable]
	public class ArgumentDiscardedException : ArgumentException
	{
		public ArgumentDiscardedException() : base(GetMessage()) { }

		public ArgumentDiscardedException(string paramName)
			: base(message: GetMessage(), paramName: paramName)
		{ }

		public ArgumentDiscardedException(string paramName, object actualValue)
			: base(message: GetMessage(actualValue), paramName: paramName)
		{ }

		public ArgumentDiscardedException(string paramName, IDiscardable actualValue)
			: this(paramName, (object)actualValue)
		{ }

		public ArgumentDiscardedException(string message, Exception innerException)
			: base(message, innerException)
		{ }

		public ArgumentDiscardedException(string paramName, string message)
			: base(message: message, paramName: paramName)
		{ }

		public ArgumentDiscardedException(string paramName, string message, Exception innerException)
			: base(message: message, paramName: paramName, innerException: innerException)
		{ }

		protected ArgumentDiscardedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{ }

		public static string GetMessage(object actualValue = null)
		{
			if (actualValue == null) {
				return "Object has been discarded, so cannot be used for this parameter.";
			} else {
				if (actualValue is IDiscardable) {
					return (
						"IDiscardable '" + actualValue + "' of type '" + actualValue.GetType().FullName + "' "
						+ "has been discarded (IsDiscarded is true), so cannot be used for this parameter."
					);
				} else {
					return (
						"Object '" + actualValue + "' of type '" + actualValue.GetType().FullName + "' "
						+ "has been discarded, so cannot be used for this parameter."
					);
				}
			}
		}
	}
}

//*/