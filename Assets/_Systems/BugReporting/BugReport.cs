﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace RevolitionGames.BugReporting
{
	[Serializable]
	public class BugReport
	{
		public string IssueTitle { get; private set; }
		public string IssueDescription { get; private set; }
		public Version GameVersion { get; private set; }
		public DateTime ReportDate { get; private set; }
		public ActivityLog ActivityLog { get; private set; }
		//TODO: Include OS, device type, user id, and more
		public MailAddress ReporterAddress { get; private set; }

		public BugReport(string issueTitle, string issueDescription, Version gameVersion, DateTime reportDate, ActivityLog activityLog, MailAddress reporterAddress = null)
		{
			ThrowIfArgNull(issueTitle, "issueTitle");
			ThrowIfArgNull(issueDescription, "issueDescription");
			ThrowIfArgNull(gameVersion, "gameVersion");
			ThrowIfArgNull(reportDate, "reportDate");
			ThrowIfArgNull(activityLog, "activityLog");

			this.IssueTitle = issueTitle;
			this.IssueDescription = issueDescription;
			this.GameVersion = gameVersion;
			this.ReportDate = reportDate;
			this.ActivityLog = activityLog;
			this.ReporterAddress = reporterAddress;
		}

		public BugReport(string issueTitle, string issueDescription, Version gameVersion, ActivityLog activityLog, MailAddress reporterAddress = null)
			: this(
				issueTitle: issueTitle,
				issueDescription: issueDescription,
				gameVersion: gameVersion,
				reportDate: DateTime.Now,
				activityLog: activityLog,
				reporterAddress: reporterAddress
			)
		{ }

		public BugReport(string issueTitle, string issueDescription, Game context, MailAddress reporterAddress = null)
			: this(
				issueTitle: issueTitle,
				issueDescription: issueDescription,
				gameVersion: Game.CurrentVersion,
				reportDate: DateTime.Now,
				activityLog: context.ActivityLog,
				reporterAddress: reporterAddress
			)
		{ }

		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}
}

//*/