﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.BugReporting
{
	[Serializable]
	public class ActivityLog
	{
		private List<ActivityLogItem> _items;
		public ReadOnlyCollection<ActivityLogItem> Items { get; private set; }

		public ActivityLog()
		{
			this._items = new List<ActivityLogItem>();
			this.Items = this._items.AsReadOnly();
		}

		public void Log(ActivityLogItem item)
		{
			ThrowIfArgNull(item, "item");
			_items.Add(item);
		}

		public void Log(string codeName, string message)
		{
			_items.Add(new ActivityLogItem(codeName: codeName, message: message));
		}

		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}

	[Serializable]
	public class ActivityLogItem
	{
		public string CodeName { get; private set; }
		public string Message { get; private set; }

		public ActivityLogItem(string codeName, string message)
		{
			ThrowIfArgNull(codeName, "codeName");
			ThrowIfArgNull(message, "message");

			this.CodeName = codeName;
			this.Message = message;
		}

		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}
}

//*/