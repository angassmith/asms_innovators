﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RevolitionGames
{
	public class CameraAntiClip : MonoBehaviour 
	{

		Transform player;
		public float maxDist = 3.26f;

        private void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }

        void Update()
		{
            //	player = GameObject.FindGameObjectWithTag("Player").transform;
            //Debug.Log("Camera Player Pos" + player.position);
        }

		void FixedUpdate()
		{

			RaycastHit hit;

			if(Physics.Raycast(player.position, -this.transform.forward, out hit, maxDist))
			{
                Debug.DrawRay(player.position, -this.transform.forward);
                Vector3.Lerp(transform.position, hit.point, 0.2f);
			}
		}

	}
}

