﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAnalyticsHeatmap;

public class AnalyticsPlayerData : MonoBehaviour {

	IEnumerator Start()
	{
		while (true) 
		{
			HeatmapEvent.Send("PlayerPosition", transform, Time.time);
			yield return new WaitForSeconds (5f);
			Debug.Log ("Sent Player Position For Heatmapping");
		}
	}
}
