﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.SceneManagement;
using RevolitionGames.SceneManagement;

public class PlayerAnimationController : MonoBehaviour
{

    private Animator anim;
    public float speed;
    private bool run = false;
    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        run = SceneInfo.GetByName(SceneManager.GetActiveScene().name).CanRun;

        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");

        if (h > 0 || h < 0)
        {
            if (run)
            {
                anim.SetBool("walk", true);
            } else
            {
                anim.SetBool("properWalk", true);
            }
        }
        else if (h == 0)
        {
            if (run)
            {
                anim.SetBool("walk", false);
            } else
            {
                anim.SetBool("properWalk", false);
            }
        }

        if (v > 0 || v < 0)
        {
            if (run)
            {
                anim.SetBool("walk", true);
            } else
            {
                anim.SetBool("properWalk", true);
            }
        }
        else if (v == 0)
        {
            if (run)
            {
                anim.SetBool("walk", false);
            } else
            {
                anim.SetBool("properWalk", false);
            }
        }
    }

    void OnAnimatorMove()
    {
        if (anim)
        {
            Vector3 newPosition = transform.position;
            newPosition.z += anim.GetFloat("inputH") * Time.deltaTime;
            transform.position = newPosition;
        }
    }

	private void OnDisable()
	{
		if (anim != null) {
			anim.SetBool("walk", false);
            anim.SetBool("properWalk", false);
		}
	}
}