﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Fungus;
using RevolitionGames.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames.Interaction
{
	public class DoorInteract : Interactable<Player>
	{

		public const float DoorMaxInteractAngle = 45; //Degrees
		public const float DoorMaxInteractHorizontalDist = 2;
		public const float DoorMaxInteractYDiff = 4;

		[SerializeField] //set in inspector
		private string targetScene;

        [SerializeField] //set in inspector
        private int targetSpawnPoint;

        public bool IsInteractable = false;

		public bool InteractionIsOngoing { get; private set; }

		public override bool CanTakeFocus { get { return IsInteractable; } } //Doors interact as soon as they're in the interact region, so focus is irrelevant (should this be changed though?)

		public override float MaxInteractAngle { get { return DoorMaxInteractAngle; } }
		public override float MaxInteractHorizontalDist { get { return DoorMaxInteractHorizontalDist; } }
		public override float MaxInteractYDiff { get { return DoorMaxInteractYDiff; } }

		public event System.EventHandler InteractStarted;
		public event System.EventHandler InteractFinished;

		private void Update()
        {

        }

		protected override void OnEnterInteractRegion(InteractDetector<Player> detector)
		{
			Debug.Log("Door entered interact region", this);
			//if (targetScene == null) Debug.LogError("TargetScene is null", context: this);
			//SwitchLevel(detector);
		}

		private void SwitchLevel(InteractDetector<Player> detector)
		{
			try
			{
				InteractionIsOngoing = true;
				detector.enabled = false;
                PlayModeStage.Current.PlayerUI.DialogOpen = true;
				InteractStarted.Fire(this, EventArgs.Empty);
                Debug.Log("Change the scene to " + targetScene);
				PlayModeStage.Current.SwitchToPlaymodeScene(targetScene, targetSpawnPoint);
			}
			finally //Runs even if an exception is thrown or a yield break is hit
			{
				InteractionIsOngoing = false;
				detector.enabled = true;
                PlayModeStage.Current.PlayerUI.DialogOpen = false;
				InteractFinished.Fire(this, EventArgs.Empty);
			}
		}

		protected override void OnExitInteractRegion(InteractDetector<Player> detector)
		{
			Debug.Log("Door exited interact region", this);
			return;
		}

		protected override void OnGotFocus(InteractDetector<Player> detector)
		{
			Debug.Log("Door got interact focus", this);
            if (IsInteractable)
            {
				var iconPref = PlayModeStage.Current.PlayerUI.DoorInteractIcon;
                PlayModeStage.Current.PlayerUI.DisplayInteractIcon(this, detector, iconPref);
            }
        }

		protected override void OnLostFocus(InteractDetector<Player> detector)
		{
			Debug.Log("Door lost interact focus", this);
            PlayModeStage.Current.PlayerUI.HideInteractIcon();
        }

		protected override void OnInteractTriggered(InteractDetector<Player> detector)
		{
            if (this.HasInteractFocus(detector) && IsInteractable)
            {
                if (targetScene == null) Debug.LogError("TargetScene is null", context: this);

				SwitchLevel(detector);

				PlayModeStage.Current.PlayerUI.HideInteractIcon();

				AudioSource audio = FindObjectOfType<AudioSource>();
				var clip = PlayModeStage.Current.PlayerUI.DoorEnterSound;
				if (clip != null) {
					audio.clip = clip;
					audio.Play();
				}
            }
        }

		public override Vector3 GetInteractIconPosition(InteractDetector detector)
		{
			return new Vector3(
				detector.Renderer.bounds.center.x,
				detector.Renderer.bounds.center.y + (detector.Renderer.bounds.size.y / 2) + StandardInteractIconYOffset,
				detector.Renderer.bounds.center.z
			);
		}
	}
}
