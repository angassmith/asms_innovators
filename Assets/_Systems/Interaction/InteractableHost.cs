﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Interaction
{
	public class InteractableHost //Deliberately non-serializable or unity-object-derived
	{
		public KeyedRefreshingCache<Type, RefreshingListCache<Interactable>> AllInteractablesDict { get; private set; }

		public IEnumerable<Interactable> AllInteractables {
			get { return AllInteractablesDict.CachedValues.SelectMany(x => x.Value); }
		}

		public InteractableHost()
		{
			AllInteractablesDict = new KeyedRefreshingCache<Type, RefreshingListCache<Interactable>>(
				cacheFactory: type => new RefreshingListCache<Interactable>(
					finder: () => (Interactable[])Resources.FindObjectsOfTypeAll(type) //Array downcast is safe according to unity docs example
				)
			);
		}
	}
}

//*/