﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Fungus;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.Interaction
{

	/// <summary>
	/// Note: Do not derive from this - derive from the generic version instead.
	/// </summary>
	public abstract class InteractDetector : MonoBehaviour
	{
		public Renderer Renderer { get; private set; }

		protected virtual void Start()
		{
			this.Renderer = this.GetComponentInChildren<Renderer>();
		}
	}

	/// <summary>
	/// Note: Other classes, especially <see cref="Interactable"/>s, are allowed to disable this MonoBehaviour.
	/// </summary>
	public abstract class InteractDetector<TInteractor> : InteractDetector
		where TInteractor : IInteractor
	{
		[SerializeField] //And edit in inspector
		private TInteractor _interactor;
		public TInteractor Interactor { get { return _interactor; } }

		private Interactable<TInteractor> _focusedInteractable;
		public Interactable<TInteractor> FocusedInteractable {
			get {
				return _focusedInteractable;
			}
			private set {
				//Store old value for comparison
				var oldVal = _focusedInteractable;

				//Set value either way (values may be considered equal but not actually be equal, so should still be replaced)
				_focusedInteractable = value;

				//Fire event if value changed
				if (oldVal != value) {
					FocusedInteractableChanged.Fire(
						this,
						new ValueChangedEventArgs<Interactable<TInteractor>>(oldVal, value)
					);
				}
			}
		}

		public event EventHandler<ValueChangedEventArgs<Interactable<TInteractor>>> FocusedInteractableChanged;
		

		//Design decisions:
		//From the interactables within the given maximum 2d (x+z) radius, 2d (x+z) angle, and y-position difference,
		//set the focus to the closest (in x+z coordinates) interactable
		//TODO: add an option to first do a ray cast, and if it hits an interactable that's within the radius+angle,
		//use that interactable. This will fix the doors-difficult-to-enter issue.
		
		private void Update() //Original version used LateUpdate, idk why or if it's needed here
		{
			if (PlayModeSceneData.IsCurrent)
			{
				UpdateInteractableDetection();
			}
		}

		private void UpdateInteractableDetection()
		{
			//Setup
			Vector3 detectorPos = this.transform.position;
			Vector3 detectorForwardDir = this.transform.forward;
			Vector2 detectorForwardDir2d = new Vector2(detectorForwardDir.x, detectorForwardDir.z);

			Interactable<TInteractor> focused = null;
			float shortestSqrDist = float.PositiveInfinity;

			var allInteractables = PlayModeSceneData.Current.InteractableHost.AllInteractables.OfType<Interactable<TInteractor>>();
			foreach (Interactable<TInteractor> interactable in allInteractables)
			{
				float sqrInteractableDist;

				bool isInRegion = IsInInteractRegion(interactable, detectorPos, detectorForwardDir2d, out sqrInteractableDist);

				if (!isInRegion) {
					//This interactable's interact status is fully known (as it cannot possibly have the focus)
					interactable.SetInteractRegionStatusINTERNAL(this, inRegion: false, hasFocus: false);
					continue;
				}

				//Check if the current interactable is a better candidate for the focused interactable
				if (sqrInteractableDist < shortestSqrDist && interactable.CanTakeFocus)
				{
					if (focused != null) {
						//The previous candidate for the focused interactable is now known to not have the focus,
						//so we can now notify it of it's new interaction status
						focused.SetInteractRegionStatusINTERNAL(this, inRegion: true, hasFocus: false);
					}

					//This new candidate for the focused interactable might not actually end up focused,
					//so we can't notify it of anything yet
					focused = interactable;
					shortestSqrDist = sqrInteractableDist;
				}
				else
				{
					//This interactable's interact status is fully known (as it cannot possibly have the focus)
					interactable.SetInteractRegionStatusINTERNAL(this, inRegion: true, hasFocus: false);
				}
			}

			if (focused != null)
			{
				//The focused candidate is now definitely focused
				//Note: The new focus is gained after the previous focus is lost, which is a useful property
				focused.SetInteractRegionStatusINTERNAL(this, inRegion: true, hasFocus: true);
			}

			this.FocusedInteractable = focused; //Set property and hence fire event if changed
		}




		public bool IsInInteractRegion(Interactable<TInteractor> interactable)
		{
			Vector3 detectorPos = this.transform.position;
			Vector3 detectorForwardDir = this.transform.forward;
			Vector2 detectorForwardDir2d = new Vector2(detectorForwardDir.x, detectorForwardDir.z);

			float dummy;
			return IsInInteractRegion(interactable, detectorPos, detectorForwardDir2d, out dummy);
		}

		private bool IsInInteractRegion(Interactable<TInteractor> interactable, Vector3 detectorPos, Vector3 detectorForwardDir2d, out float sqrInteractable2dDist)
		{
			//Note: Extra arguments (detectorPos etc) are passed and returned to avoid recalculation

			//Setup
			Vector3 detectorToInteractableDir = interactable.transform.position - detectorPos;
			Vector2 detectorToInteractableDir2d = new Vector2(detectorToInteractableDir.x, detectorToInteractableDir.z);
			float detectorInteractableYDiff = Math.Abs(detectorToInteractableDir.y);

			sqrInteractable2dDist = detectorToInteractableDir2d.sqrMagnitude;

			//Check vertical (y) distance
			if (detectorInteractableYDiff > interactable.MaxInteractYDiff) return false;
			
			//Check horizontal (x, z) distance
			float sqrInteractableMax2dDist = interactable.MaxInteractHorizontalDist * interactable.MaxInteractHorizontalDist;
			if (sqrInteractable2dDist > sqrInteractableMax2dDist) return false;

			//Check horizontal (x, z) angle difference
			float angleFromForward = Vector2.Angle(detectorForwardDir2d, detectorToInteractableDir2d); //Uses degrees
			if (angleFromForward > interactable.MaxInteractAngle) return false;

			return true;
		}



		/// <summary>
		/// Triggers interaction on all registered <see cref="Interactable{TInteractor}"/>s,
		/// provided that the current <see cref="InteractDetector{TInteractor}"/> is enabled
		/// (the <see cref="Behaviour"/>, <see cref="GameObject"/>, and parent <see cref="GameObject"/>s are enabled/active)
		/// </summary>
		public void Interact()
		{
			if (this.enabled && this.gameObject.activeInHierarchy)
			{
				var allInteractables = PlayModeSceneData.Current.InteractableHost.AllInteractables.OfType<Interactable<TInteractor>>();
				foreach (Interactable<TInteractor> interactable in allInteractables)
				{
					interactable.TriggerInteractINTERNAL(detector: this);
				}
			}
		}

		#region Old: First Apr 2017 Redo

		//	/// <summary>
		//	/// The <see cref="Interactable"/>s that are within their respective interact regions,
		//	/// and if they require it, have the interact focus.
		//	/// </summary>
		//	private HashSet<InteractableInHashSet> InteractablesInRegion = new HashSet<InteractableInHashSet>();
		//	
		//	private Interactable FocusedInteractable;
		//	
		//	
		//	private IEnumerable<Interactable> GetInteractablesInRespectiveMaxAngles()
		//	{
		//		Vector3 detectorPos = this.transform.position;
		//	
		//		Vector3 detectorForwardDir = this.transform.forward;
		//		Vector2 detectorForwardDir2d = new Vector2(detectorForwardDir.x, detectorForwardDir.z);
		//	
		//		foreach (Interactable interactable in Game.Current.InteractableHost.AllInteractables)
		//		{
		//			Vector3 detectorToInteractableDir = interactable.transform.position - detectorPos;
		//			Vector2 detectorToInteractableDir2d = new Vector2(detectorToInteractableDir.x, detectorToInteractableDir.z);
		//	
		//			float angleFromForward = Vector2.Angle(detectorForwardDir2d, detectorToInteractableDir2d); //Uses degrees
		//	
		//			if (angleFromForward <= interactable.MaxInteractAngle) yield return interactable;
		//		}
		//	}
		//	
		//	private IEnumerable<InteractableWithDist> GetInteractablesInRespectiveRegions()
		//	{
		//		Vector3 detectorPos = this.transform.position;
		//	
		//		return from interactable in GetInteractablesInRespectiveMaxAngles()
		//		       let sqrInteractableDist = (interactable.transform.position - detectorPos).sqrMagnitude //WRONG (uses 3d coords)
		//		       let sqrInteractableMaxDist = interactable.MaxInteractDistance * interactable.MaxInteractDistance
		//		       where sqrInteractableDist <= sqrInteractableMaxDist
		//		       select new InteractableWithDist(interactable, sqrInteractableDist);
		//	}
		//	
		//	private void UpdateInteractableDetection()
		//	{
		//		Vector3 detectorPos = this.transform.position;
		//	
		//		var interactablesInRegion = GetInteractablesInRespectiveRegions();
		//	
		//		Interactable focused = null;
		//		float shortestSqrDist = float.PositiveInfinity;
		//	
		//		foreach (var interactable in interactablesInRegion)
		//		{
		//			if (interactable.SqrDistance < shortestSqrDist)
		//			{
		//				focused = interactable.Interactable;
		//				shortestSqrDist = interactable.SqrDistance;
		//			}
		//		}
		//	
		//		foreach (var interactable in interactablesInRegion)
		//		{
		//			if 
		//		}
		//	}
		//	
		//	//	private void ClearDetectedInteractables()
		//	//	{
		//	//		if (this.FocusedInteractable != null) {
		//	//			this.FocusedInteractable.OnLostFocus();
		//	//			this.FocusedInteractable = null;
		//	//		}
		//	//	
		//	//		foreach (var interactable in this.InteractablesInRegion) interactable.Interactable.OnExitInteractRegion();
		//	//		this.InteractablesInRegion.Clear();
		//	//	}
		//	
		//	private struct InteractableWithDist
		//	{
		//		public readonly Interactable Interactable;
		//		public readonly float SqrDistance;
		//	
		//		public InteractableWithDist(Interactable interactable, float sqrDistance)
		//		{
		//			if (interactable == null) throw new ArgumentNullException("interactable");
		//	
		//			this.Interactable = interactable;
		//			this.SqrDistance = sqrDistance;
		//		}
		//	}
		//	
		//	private struct InteractableInHashSet
		//	{
		//		public readonly Interactable Interactable;
		//		public readonly bool StillInRegion;
		//	
		//		public InteractableInHashSet(Interactable interactable, bool stillInRegion = true)
		//		{
		//			if (interactable == null) throw new ArgumentNullException("interactable");
		//	
		//			this.Interactable = interactable;
		//			this.StillInRegion = stillInRegion;
		//		}
		//	
		//		public override int GetHashCode() {
		//			return Interactable.GetHashCode();
		//		}
		//		public override bool Equals(object obj) {
		//			return obj is InteractableInHashSet && this.Equals((InteractableInHashSet)obj);
		//		}
		//		public bool Equals(InteractableInHashSet other) {
		//			return this.Interactable.Equals(other.Interactable);
		//		}
		//	}

		#endregion

		#region Old: Last 2016 version

		//	//	/// <summary>
		//	//	/// This is sorta the same as Isabella's script, except I'm using an OverlapSphere to detect the colliders and I'm only grabbing the closest collider for interaction. 
		//	//	/// This is probably gonna work for objects but I might have to change a few things and set up some objects to test it with. Also I will need to add in the putting the objects in the inventory.
		//	//	/// It has the same sorta idea with the looping and the Interact() function as Alex said as well.
		//	//	/// 
		//	//	/// PS. The icons don't work currently, but I'll get onto that
		//	//	/// </summary>
		//	
		//	
		//	//	public float InteractRadius = 10f;
		//	public float InteractArcSize = 30;
		//	//	Collider[] colliders;
		//	//	public LayerMask mask;
		//	//	public Flowchart flowchart; //this is the flowchart
		//	//	public GameObject interactableObj; //this is an interactable object; might not be needed, we'll see
		//	//	public string flowblock; //change this string the name of the flowchart you want to call
		//	//	public GameObject iconSpeech; //this is your icon for an npc interaction
		//	//	public GameObject iconObj; //this is your icon for an object
		//	
		//	public Interactable toInteractWith;
		//	public Interactable prevToInteractWith;
		//	
		//	
		//	void LateUpdate()
		//	{
		//		prevToInteractWith = toInteractWith;
		//	
		//		float smallestSqrDist = InteractRadius;
		//		Interactable nearestInteractable = null;
		//	
		//		//	Collider[] allNearbyColliders = Physics.OverlapSphere(transform.position, InteractRadius, mask);
		//		foreach (Interactable interactable in Game.Current.InteractableHost.AllInteractables)
		//		{
		//			float sqrDist = (transform.position - interactable.transform.position).sqrMagnitude;
		//	
		//			if (sqrDist < smallestSqrDist)// && Mathf.DeltaAngle(Vector2.Angle(General.Vector3ToVector2XZ(interactable.transform.position), General.Vector3ToVector2XZ(this.transform.position)), this.transform.eulerAngles.y) < InteractArcSize)
		//			{
		//				smallestSqrDist = sqrDist;
		//				nearestInteractable = interactable;
		//			}
		//		}
		//	
		//		if (nearestInteractable == null)
		//		{
		//			toInteractWith = null;
		//		} else
		//		{
		//			Interactable interactComponent = nearestInteractable.GetComponent<Interactable>();
		//			if (interactComponent == null)
		//			{
		//				toInteractWith = null;
		//			}
		//			else
		//			{
		//				toInteractWith = interactComponent;
		//			}
		//		}
		//	
		//	
		//		if (!ReferenceEquals(prevToInteractWith, toInteractWith)) //If it's a different gameobject
		//		{
		//			if (prevToInteractWith != null)
		//			{
		//				prevToInteractWith.SetAboutToInteractState(false);
		//			}
		//			if (toInteractWith != null)
		//			{
		//				toInteractWith.SetAboutToInteractState(true);
		//	
		//				if (toInteractWith.AutoInteract)
		//				{
		//					toInteractWith.Interact();
		//				}
		//			}
		//		}
		//	}

		#endregion
	}
}
