﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames.Interaction {
    public class InteractButton : MonoBehaviour {

        public void Interact() {
            PlayModeSceneData.Current.Player.InteractDetector.Interact();
        }
    }
}
