﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Fungus;
using RevolitionGames.Quests;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.Interaction
{
	public class SimpleInteract : Interactable<Player>
	{
		
		public const float SimpleMaxInteractAngle = 30; //Degrees
		public const float SimpleMaxInteractHorizontalDist = 4;
		public const float SimpleMaxInteractYDiff = 3;

        [SerializeField]
        private Sprite iconPref;

		[SerializeField] //And edit in inspector
		private CodeNameRef _SimpleCodeName;
		public CodeNameRef SimpleCodeName { get { return _SimpleCodeName; } }

        public bool IsInteractable = true; //TODO: Improve (and use behaviour overrides)

		private Renderer _renderer;
		private Renderer Renderer {
			get {
				if (_renderer == null) _renderer = this.GetComponentInChildren<Renderer>();
				return _renderer;
			}
		}

		public override float MaxInteractAngle { get { return SimpleMaxInteractAngle; } }
		public override float MaxInteractHorizontalDist { get { return SimpleMaxInteractHorizontalDist; } }
		public override float MaxInteractYDiff { get { return SimpleMaxInteractYDiff; } }

		public override bool CanTakeFocus { get { return IsInteractable; } }

		public event System.EventHandler InteractOccurred;

		protected override void AwakeImpl()
		{
			base.AwakeImpl();
			if (SimpleCodeName == null) Debug.LogError("SimpleCodeName is null", context: this);
		}

		private void Update()
        {
            if (iconPref == null)
            {
                //iconPref = (Sprite)Resources.Load("Textures&Models/UI/4cbMjjgni");
                iconPref = PlayModeStage.Current.PlayerUI.camUIScript.icons[1];
            }
        }

		protected override void OnEnterInteractRegion(InteractDetector<Player> detector) {
			return;
		}

		protected override void OnExitInteractRegion(InteractDetector<Player> detector) {
			return;
		}

		protected override void OnGotFocus(InteractDetector<Player> detector)
		{
            if (IsInteractable) {
                PlayModeStage.Current.PlayerUI.DisplayInteractIcon(this, detector, iconPref);
            }
		}

		protected override void OnLostFocus(InteractDetector<Player> detector)
		{
            PlayModeStage.Current.PlayerUI.HideInteractIcon();
		}

		protected override void OnInteractTriggered(InteractDetector<Player> detector)
		{
			if (this.HasInteractFocus(detector) && IsInteractable)
			{
                InteractOccurred.Fire(this, EventArgs.Empty);
                PlayModeStage.Current.PlayerUI.HideInteractIcon();
            }
		}

		public override Vector3 GetInteractIconPosition(InteractDetector detector)
		{
			return new Vector3(
				Renderer.bounds.center.x,
				Renderer.bounds.center.y + (Renderer.bounds.size.y / 2) + StandardInteractIconYOffset,
				Renderer.bounds.center.z
			);
		}
	}
}
