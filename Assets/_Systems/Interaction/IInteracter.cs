﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Interaction
{
	/// <summary>
	/// Base interface for objects that can detect other object and decide to interact with them,
	/// for example the <see cref="Player"/> class, and possibly NPCs in future
	/// </summary>
	public interface IInteractor
	{
		GameObject gameObject { get; }
	}
}

//*/