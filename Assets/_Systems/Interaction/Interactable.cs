﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Interaction
{
	public abstract class Interactable : TrackedMono
	{
		public const float StandardInteractIconYOffset = 0.1f;

		protected sealed override bool TrackInEditor { get { return false; } }
		protected sealed override void UpdateTracking(TrackingEvent evt) {
			//The game might not have been created in Awake(), and might have been destroyed
			//in OnDestroy() or OnApplicationQuit().
            try {
				if (PlayModeSceneData.IsCurrent) {
					PlayModeSceneData.Current.InteractableHost.AllInteractablesDict[this.GetType()].Refresh();
				}
            } catch (InvalidOperationException e) {
                Debug.LogWarning(e, context: this);
            }
		}

		public abstract Vector3 GetInteractIconPosition(InteractDetector detector);


		/// <summary>
		/// True if the <see cref="Interactable"/> can take the interaction focus, otherwise, false.
		/// </summary>
		public abstract bool CanTakeFocus { get; }

		/// <summary>
		/// The maximum angle from the forward direction at which the <see cref="Interactable"/> can be interacted with (in degrees).
		/// </summary>
		public abstract float MaxInteractAngle { get; }
		/// <summary>
		/// The maximum distance at which the <see cref="Interactable"/> can be interacted with.
		/// </summary>
		public abstract float MaxInteractHorizontalDist { get; }
		/// <summary>
		/// The maximum vertical difference between the positions of the <see cref="Interactable"/>
		/// and <see cref="InteractDetector"/> at which interaction can occur.
		/// </summary>
		public abstract float MaxInteractYDiff { get; }


		#region Utilities

		protected static T ThrowIfArgNull<T>(T obj, string argName)
		{
			if (obj == null) throw new ArgumentNullException(argName);
			else return obj;
		}

		#endregion
	}

	public abstract class Interactable<TInteractor> : Interactable
		where TInteractor : IInteractor
	{
		

		




		//TODO: Docs
		protected abstract void OnEnterInteractRegion(InteractDetector<TInteractor> detector);
		protected abstract void OnExitInteractRegion(InteractDetector<TInteractor> detector);
		protected abstract void OnGotFocus(InteractDetector<TInteractor> detector);
		protected abstract void OnLostFocus(InteractDetector<TInteractor> detector);
		protected abstract void OnInteractTriggered(InteractDetector<TInteractor> detector);


		internal void TriggerInteractINTERNAL(InteractDetector<TInteractor> detector) {
			OnInteractTriggered(detector);
		}




		[NonSerialized]
		private HashSet<InteractDetector<TInteractor>> DetectorsWithThisInRegion = new HashSet<InteractDetector<TInteractor>>();

		public bool IsInInteractRegion(InteractDetector<TInteractor> detector) {
			return DetectorsWithThisInRegion.Contains(detector);
		}
		public bool HasInteractFocus(InteractDetector<TInteractor> detector) {
			return ReferenceEquals(detector.FocusedInteractable, this);
		}

		internal void SetInteractRegionStatusINTERNAL(InteractDetector<TInteractor> detector, bool inRegion, bool hasFocus)
		{
			//Note: The order is important here
			if (!this.IsInInteractRegion(detector) && inRegion) OnEnterInteractRegion(detector);
			if (detector.FocusedInteractable != this && hasFocus) OnGotFocus(detector);
			if (detector.FocusedInteractable == this && !hasFocus) OnLostFocus(detector);
			if (this.IsInInteractRegion(detector) && !inRegion) OnExitInteractRegion(detector);

			if (inRegion) {
				DetectorsWithThisInRegion.Add(detector);
			} else {
				DetectorsWithThisInRegion.Remove(detector);
			}
		}


		#region Old

		//	/// <summary>
		//	/// True if <see cref="IsInInteractRegion"/>, <see cref="HasInteractFocus"/>, ongoing interactions,
		//	/// and any other relevant state is set such that interaction is possible.
		//	/// </summary>
		//	public abstract bool CanInteractNow { get; }
		//	
		//	/// <summary>
		//	/// Fired when interaction begins. Fired automatically by <see cref="Interact()"/>.
		//	/// </summary>
		//	public event EventHandler InteractionStarted;
		//	
		//	/// <summary>
		//	/// Fired when interaction finishes.
		//	/// If the interaction is not ongoing (<see cref="Interact(out bool)"/> returned false),
		//	/// this is fired automatically by <see cref="TryTriggerInteract(out bool, out bool)"/>.
		//	/// Otherwise, if the interaction is ongoing, <see cref="FinishOngoingInteraction()"/> must
		//	/// be called by the deriving class. The <see cref="InteractDetector"/> is disabled until this is fired.
		//	/// </summary>
		//	public event EventHandler InteractionFinished;
		//	
		//	/// <summary>
		//	/// If <see cref="Interact(out bool)"/> signals that the interaction is ongoing,
		//	/// this must be called when the interaction is finished so that the <see cref="InteractDetector"/>
		//	/// can be re-enabled.
		//	/// </summary>
		//	protected void FinishOngoingInteraction()
		//	{
		//		InteractionFinished.Fire(this, EventArgs.Empty);
		//	}
		//	
		//	/// <summary>
		//	/// Called on all <see cref="Interactable"/>s by <see cref="InteractDetector.Interact()"/>.
		//	/// </summary>
		//	/// <param name="ongoing">
		//	/// Signals whether the interaction is ongoing beyond the end of this method.
		//	/// If true, the <see cref="InteractDetector"/> will be disabled until <see cref="InteractionFinished"/>
		//	/// is fired.
		//	/// </param>
		//	protected abstract void Interact(out bool ongoing);
		//	
		//	/// <summary>
		//	/// If <see cref="CanInteractNow"/> returns true, fires <see cref="InteractionStarted"/>
		//	/// then calls <see cref="Interact(out bool)"/>.
		//	/// Then, if the interaction is not ongoing, fires <see cref="InteractionFinished"/>.
		//	/// </summary>
		//	/// <param name="ongoing">True if <see cref="CanInteractNow"/> was true and the interaction is ongoing.</param>
		//	/// <returns>Returns true if <see cref="CanInteractNow"/> was true, otherwise returns false.</returns>
		//	internal bool TryTriggerInteract(out bool ongoing)
		//	{
		//		if (CanInteractNow)
		//		{
		//			InteractionStarted.Fire(this, EventArgs.Empty);
		//			Interact(out ongoing);
		//			if (!ongoing) InteractionFinished.Fire(this, EventArgs.Empty);
		//	
		//			return true;
		//		}
		//		else
		//		{
		//			ongoing = false;
		//			return false;
		//		}
		//	}

		#endregion
	}
}
