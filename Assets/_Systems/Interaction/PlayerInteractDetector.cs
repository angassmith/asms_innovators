﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Interaction
{
	/// <summary>
	/// Non-generic derived type to keep unity happy
	/// </summary>
	public class PlayerInteractDetector : InteractDetector<Player>, IInteractor
	{
		
	}
}

//*/