﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Fungus;
using RevolitionGames.Pathfinding;
using RevolitionGames.Quests;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.Interaction
{
	[RequireComponent(typeof(NPCPathfinder))]
	public class NPCInteract : Interactable<Player>, IBehaviourOverridable<NPCInteractBehaviour>
	{
		
		public const float NPCMaxInteractAngle = 30; //Degrees
		public const float NPCMaxInteractHorizontalDist = 4;
		public const float NPCMaxInteractYDiff = 3;

		[SerializeField] //And edit in inspector
		private CodeNameRef _NPCCodeName;
		public CodeNameRef NPCCodeName { get { return _NPCCodeName; } }

        [SerializeField]
        private Sprite iconPref;

		/// <summary>Temporarily here until behaviour overrides are used</summary>
		public Flowchart FlowChart;
		/// <summary>Temporarily here until behaviour overrides are used</summary>
		public string InteractBlockName;

		/// <summary>This may or may not be kept (to be decided)</summary>
		private NPCInteractBehaviour _defaultBehaviour;
		private NPCInteractBehaviour DefaultBehaviour {
			get {
				if (_defaultBehaviour == null)
				{
					if (this.NPCCodeName == null) {
						Debug.LogError("NPCCodeName is null", this);
						return null;
					}
					_defaultBehaviour = new NPCInteractBehaviour(
						new StoryFlowchartExecutor(
							this.FlowChart,
							this.FlowChart.FindBlock(this.InteractBlockName)
						),
						targetCodeName: this.NPCCodeName.CodeName
					);
				}
				return _defaultBehaviour;
			}
		}

		private Renderer _renderer;
		private Renderer Renderer {
			get {
				if (_renderer == null) _renderer = this.GetComponentInChildren<Renderer>();
				return _renderer;
			}
		}


		private OngoingNPCInteraction OngoingInteraction { get; set; }
		public bool InteractionIsOngoing { get { return OngoingInteraction != null; } }
		

		public override float MaxInteractAngle { get { return NPCMaxInteractAngle; } }
		public override float MaxInteractHorizontalDist { get { return NPCMaxInteractHorizontalDist; } }
		public override float MaxInteractYDiff { get { return NPCMaxInteractYDiff; } }

		public override bool CanTakeFocus { get { return true; } }

		string IBehaviourOverridable<NPCInteractBehaviour>.CodeName { get { return NPCCodeName.CodeName; } }

		public event System.EventHandler InteractStarted;
		public event System.EventHandler<TaskFlowchartStoppedEventArgs> InteractFinished;
		protected override void AwakeImpl()
		{
			base.AwakeImpl();
			if (NPCCodeName == null) Debug.LogError("NPCCodeName is null", context: this);
		}

		private void Update()
        {
            if (iconPref == null)
            {
                //iconPref = (Sprite)Resources.Load("Textures&Models/UI/4cbMjjgni");
                iconPref = PlayModeStage.Current.PlayerUI.camUIScript.icons[0];
            }
        }

		protected override void OnEnterInteractRegion(InteractDetector<Player> detector) {
			return;
		}

		protected override void OnExitInteractRegion(InteractDetector<Player> detector) {
			return;
		}

		protected override void OnGotFocus(InteractDetector<Player> detector)
		{
			PlayModeStage.Current.PlayerUI.DisplayInteractIcon(this, detector, iconPref);
		}

		protected override void OnLostFocus(InteractDetector<Player> detector)
		{
            PlayModeStage.Current.PlayerUI.HideInteractIcon();
		}

		protected override void OnInteractTriggered(InteractDetector<Player> detector)
		{
			if (InteractionIsOngoing)
			{
				Debug.LogWarning("An interaction is ongoing so the InteractDetector<Player> is meant to be disabled.");
				return;
			}

			if (this.HasInteractFocus(detector))
			{

				Debug.Log("ayy#1");

				IActiveStoryPart behavContext;
				var behav = PlayModeStage.Current.Story.BehaviourOverrideProvider.SelectCurrentBehaviour(
					target: this,
					context: out behavContext,
					fallback: DefaultBehaviour,
					fallbackContext: null
				);

				OngoingInteraction = new OngoingNPCInteraction(
					npc: this,
					detector: detector,
					behaviour: behav,
					pathfinder: this.gameObject.GetComponent<NPCPathfinder>(),
					controllingTask: behavContext == null ? null : behavContext.Slot as TaskSlot
				);
				OngoingInteraction.Start();

				//	try {
				//		//Normal method
				//	} catch (InvalidOperationException) {
				//		//Temporary method for when theres no behaviour overrides
				//		//TODO: Add behaviour overrides, and remove this
				//		Debug.Log("Presenting the sayDialog");
				//		FlowChart.ExecuteBlock(InteractBlockName);
				//	
				//		InteractStarted.Fire(this, EventArgs.Empty);
				//	}
			}
		}

		public override Vector3 GetInteractIconPosition(InteractDetector detector)
		{
			return new Vector3(
				Renderer.bounds.center.x,
				Renderer.bounds.center.y + (Renderer.bounds.size.y / 2) + StandardInteractIconYOffset,
				Renderer.bounds.center.z
			);
		}

		private class OngoingNPCInteraction
		{
			public NPCInteract NPC { get; private set; }
			public InteractDetector<Player> Detector { get; private set; }
			public NPCInteractBehaviour Behaviour { get; private set; }
			private NPCPathfinder pathfinder;
			public TaskSlot ControllingTask { get; private set; }

			public OngoingNPCInteraction(NPCInteract npc, InteractDetector<Player> detector, NPCInteractBehaviour behaviour, NPCPathfinder pathfinder, TaskSlot controllingTask = null)
			{
				ThrowIfArgNull(npc, "npc");
				ThrowIfArgNull(detector, "detector");
				ThrowIfArgNull(behaviour, "behaviour");
				ThrowIfArgNull(pathfinder, "pathfinder");

				this.NPC = npc;
				this.Detector = detector;
				this.Behaviour = behaviour;
				this.pathfinder = pathfinder;
				this.ControllingTask = controllingTask;
				Debug.Log("pathfinder: " + pathfinder);
			}

			// Start after construction so that InteractionIsOngoing can become
			// true before anything in the flowchart is executed.
			public void Start()
			{
				Detector.enabled = false;
                PlayModeStage.Current.PlayerUI.HideInteractIcon();
                PlayModeStage.Current.PlayerUI.DialogOpen = true;

				Behaviour.FlowchartExecutor.FlowchartStopped += OnFlowchartStopped;
				try {
					Behaviour.FlowchartExecutor.StartFlowchart(ControllingTask);
				} catch (InvalidOperationException) {
                    PlayModeStage.Current.PlayerUI.DialogOpen = false;
					throw;
				}

				pathfinder.TalkStart(Detector.transform);

				NPC.InteractStarted.Fire(NPC, EventArgs.Empty);
			}

			private void OnFlowchartStopped(object sender, TaskFlowchartStoppedEventArgs e)
			{
				Debug.Log("Interaction finished");

				Behaviour.FlowchartExecutor.FlowchartStopped -= OnFlowchartStopped;

				Detector.enabled = true;
                PlayModeStage.Current.PlayerUI.DialogOpen = false;
				//NPCInteractIcon is re-enabled by an npc taking the interact focus

				NPC.OngoingInteraction = null;

				NPC.InteractFinished.Fire(NPC, e);

				pathfinder.TalkEnd();
			}
		}
	}





	public class NPCInteractBehaviour : BehaviourOverride
	{
		public StoryFlowchartExecutor FlowchartExecutor { get; private set; }

		public NPCInteractBehaviour(StoryFlowchartExecutor flowchartExecutor, string targetCodeName) : base(targetCodeName)
		{
			if (flowchartExecutor == null) throw new ArgumentNullException("flowchartExecutor");
			this.FlowchartExecutor = flowchartExecutor;
		}
	}
}
