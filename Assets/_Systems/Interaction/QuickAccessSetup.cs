﻿/*

using UnityEngine;
using System.Collections;
using System.Linq;

public class QuickAccessSetup : MonoBehaviour
{

    public GameObject player;
    public GameObject npcInteractIcon;
    public GameObject itemInteractIcon;
    ///<summary>Leave empty/null to automatically fill</summary>
    public GameObject[] allInteractableObjects;
    //Add more here

    void Start()
    {
        QuickAccess.player = player;
        QuickAccess.npcInteractIcon = npcInteractIcon;
        QuickAccess.itemInteractIcon = itemInteractIcon;
        QuickAccess.allInteractableObjects = allInteractableObjects != null && allInteractableObjects.Length != 0 ? allInteractableObjects : Object.FindObjectsOfType<Interactable>().Select(x => x.gameObject).ToArray();
        //Add more here
    }
}

//*/