﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class changeScene : MonoBehaviour {

    public Texture2D fadeOutTexture; // texture that'll overlay during fading. might be nice too add a loading graphic instead of a black screen..
    public float fadeSpeed = 0.8f; // the fading speed

    private int drawDepth = -1000;  // the texture's order in draw hiearchy; lower numbers == rendered further forward
    private float alpha = 1.0f;     // texture's alpha value; (0-1)
    private int fadeDir = -1;       // the direction to fade: in = 1 , out = 1

	public changeScene()
	{
		SceneManager.sceneLoaded += (s, e) => { OnLevelLoaded(); };
	}

	void OnGUI () {
        // fade out/in the alpha value using a direction, a speed and Time.deltatime to convert operation to seconds
        alpha += fadeDir * fadeSpeed * Time.deltaTime;
        // force (clamp) the value to be between 0 and 1
        alpha = Mathf.Clamp01(alpha);

        //set the colour of GUI.
        GUI.color = new Color(GUI.color.r, GUI.color.g, alpha);                         //setting alpha
        GUI.depth = drawDepth;                                                          //ensure black renders on top
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture );  //draw texture as screen filling        
    }
    // set fadeDir to the direction parameter
    public float BeginFade (int direction)  {
        fadeDir = direction;
        return (fadeSpeed);
    }

    // OnLevelWasLoaded is called when a level is loaded. It takes loaded level index (int) as a parameter that can be limited in certain scenes
    private void OnLevelLoaded ()  {
        //alpha = 1;        // only enable if alpha isn't set to 1 by default
        BeginFade(-1);      // call the fade in function
    }
}
