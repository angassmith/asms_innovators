﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine.SceneManagement;

namespace RevolitionGames.SceneManagement
{
	public class SceneInfo
	{
		public static readonly SceneInfo Initial        = new SceneInfo("_initial"     , 0 , SceneType.Initial   , canRun: false);
		public static readonly SceneInfo MainMenu       = new SceneInfo("MainMenu"     , 1 , SceneType.Menu      , canRun: false);
		public static readonly SceneInfo SchoolMap      = new SceneInfo("SchoolMap"    , 2 , SceneType.PlayMode3D, canRun: true );
		public static readonly SceneInfo Art            = new SceneInfo("art"          , 3 , SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo Classroom      = new SceneInfo("classroom"    , 4 , SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo Gym            = new SceneInfo("gym"          , 5 , SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo Library        = new SceneInfo("library"      , 6 , SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo Kitchen        = new SceneInfo("kitchen"      , 7 , SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo MusicRoom      = new SceneInfo("musicroom"    , 8 , SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo ScienceLab     = new SceneInfo("science"      , 9 , SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo Office         = new SceneInfo("office"       , 10, SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo Bedroom        = new SceneInfo("bedroom"      , 11, SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo House          = new SceneInfo("house"        , 12, SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo Principal      = new SceneInfo("principal"    , 13, SceneType.PlayMode3D, canRun: false);
		public static readonly SceneInfo KoalaRun       = new SceneInfo("Scene_Endless", 14, SceneType.Minigame  , canRun: false);
		public static readonly SceneInfo JacobSIPLevel1 = new SceneInfo("Main"         , 15, SceneType.Minigame  , canRun: false);
		public static readonly SceneInfo JacobSIPLevel2 = new SceneInfo("Main 2"       , 16, SceneType.Minigame  , canRun: false);
		public static readonly SceneInfo JacobSIPLevel3 = new SceneInfo("Main 3"       , 17, SceneType.Minigame  , canRun: false);
		public static readonly SceneInfo ChapterSelect  = new SceneInfo("ChapterSelect", 18, SceneType.Menu      , canRun: false);
		public static readonly SceneInfo Settings       = new SceneInfo("Settings"     , 19, SceneType.Menu      , canRun: false);
        public static readonly SceneInfo Resources      = new SceneInfo("resources"    , 20, SceneType.Menu      , canRun: false);
        public static readonly SceneInfo Credits        = new SceneInfo("Credits"      , 21, SceneType.Menu      , canRun: false);

        public static readonly ReadOnlyDictionary<string, SceneInfo> ScenesByName;

		static SceneInfo()
		{
			ScenesByName = new ReadOnlyDictionary<string, SceneInfo>(
				new Dictionary<string, SceneInfo> {
					{ Initial       .Name, Initial        },
					{ MainMenu      .Name, MainMenu       },
					{ SchoolMap     .Name, SchoolMap      },
					{ Art           .Name, Art            },
					{ Classroom     .Name, Classroom      },
					{ Gym           .Name, Gym            },
					{ Library       .Name, Library        },
					{ Kitchen       .Name, Kitchen        },
					{ MusicRoom     .Name, MusicRoom      },
					{ ScienceLab    .Name, ScienceLab     },
					{ Office        .Name, Office         },
					{ Bedroom       .Name, Bedroom        },
					{ House         .Name, House          },
					{ Principal     .Name, Principal      },
					{ KoalaRun      .Name, KoalaRun       },
					{ JacobSIPLevel1.Name, JacobSIPLevel1 },
					{ JacobSIPLevel2.Name, JacobSIPLevel2 },
					{ JacobSIPLevel3.Name, JacobSIPLevel3 },
					{ ChapterSelect .Name, ChapterSelect  },
					{ Settings      .Name, Settings       },
                    { Resources     .Name, Resources      },
                    { Credits       .Name, Credits        },
                }
			);
		}

		public string Name { get; private set; }
		public int BuildIndex { get; private set; }
		public SceneType Type { get; private set; }
        public bool CanRun { get; private set; }

		public SceneInfo(string name, int buildIndex, SceneType type, bool canRun)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (buildIndex < 0) throw new ArgumentOutOfRangeException("buildIndex", buildIndex, "Cannot be less than zero.");

			this.Name = name;
			this.BuildIndex = buildIndex;
			this.Type = type;
            this.CanRun = canRun;
		}

		public static bool Equals(SceneInfo a, SceneInfo b)
		{
			return a.Name == b.Name && a.BuildIndex == b.BuildIndex;
		}

		public override bool Equals(object obj)
		{
			var sceneInfo = obj as SceneInfo;
			if (sceneInfo != null) return Equals(this, sceneInfo);
			else return false;
		}

		public override int GetHashCode()
		{
			var hash = 17;
			hash = hash * 23 + Name.GetHashCode();
			hash = hash * 23 + BuildIndex.GetHashCode();
			return hash;
		}

		public static bool operator ==(SceneInfo a, SceneInfo b) { return Equals(a, b); }
		public static bool operator !=(SceneInfo a, SceneInfo b) { return !(a == b); }

		public static SceneInfo GetByName(string sceneName)
		{
			SceneInfo info;
			if (ScenesByName.TryGetValue(sceneName, out info)) {
				return info;
			} else {
				throw new InvalidOperationException("Scene '" + sceneName + "' is not registered in SceneInfo.");
			}
		}

		public static SceneType GetSceneType(string sceneName)
		{
			return GetByName(sceneName).Type;
		}
	}

	public enum SceneType
	{
		Initial,
		Menu,
		PlayMode3D,
		Minigame,
	}
}

//*/