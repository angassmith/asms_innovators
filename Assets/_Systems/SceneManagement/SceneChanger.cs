﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames.SceneManagement
{
	/// <summary>
	/// Used to change scenes during the game.
	/// </summary>
	public class SceneChanger : MonoBehaviour
	{
		/// <summary>
		/// The fader to use when switching scenes.
		/// </summary>
		[SerializeField] // edit in inspector
		private ScreenFader _fader;
		public ScreenFader Fader { get { return _fader; } }

		public string CurrentTransitionTarget { get; private set; }
		public bool TransitionInProgress { get { return CurrentTransitionTarget != null; } }

		private void Awake()
		{
			this.LogIfEditorFieldNull(_fader, "_fader");
		}

		/// <summary>
		/// Loads the scene with the specified name, with a fade in/out effect used.
		/// The player will be spawned at the specified spawn point.
		/// </summary>
		/// <param name="sceneName">The name of the scene to load.</param>
		public void LoadSceneWithFade(string sceneName)
		{
			if (TransitionInProgress) throw CreateTransitionInProgressEx(
				existingtransitionTarget: this.CurrentTransitionTarget,
				intendedNewScene: sceneName
			);

			CurrentTransitionTarget = sceneName;

			float fadeTime = _fader.BeginFade(direction: FadeDirection.IncreasingOverlayOpacity);
			LoadSceneAfterDelayInternal(sceneName, delay: fadeTime);
		}

		/// <summary>
		/// Loads the scene with the specified name.
		/// </summary>
		/// <param name="sceneName">The name of the scene to load.</param>
		public void LoadScene(string sceneName)
		{
			if (sceneName == null) throw new ArgumentNullException("sceneName");

			if (TransitionInProgress) throw CreateTransitionInProgressEx(
				existingtransitionTarget: this.CurrentTransitionTarget,
				intendedNewScene: sceneName
			);

			LoadSceneInternal(sceneName);
		}

		private void LoadSceneInternal(string sceneName)
		{
			CurrentTransitionTarget = sceneName;

			SceneManager.LoadScene(sceneName);

			CurrentTransitionTarget = null;
		}

		/// <summary>
		/// Loads the scene with the specified name, after the specified delay.
		/// </summary>
		/// <param name="sceneName">The name of the scene to load.</param>
		/// <param name="delay">The time to wait before loading the scene, in seconds.</param>
		public void LoadSceneAfterDelay(string sceneName, float delay)
		{
			if (sceneName == null) throw new ArgumentNullException("sceneName");
			//Would validate that delay isn't negative but floats are approximate so that would be unreliable. Instead:
			delay = Math.Max(delay, 0);

			if (TransitionInProgress) throw CreateTransitionInProgressEx(
				existingtransitionTarget: this.CurrentTransitionTarget,
				intendedNewScene: sceneName
			);

			CurrentTransitionTarget = sceneName;

			StartCoroutine(
				CoroutineUtils.RunAfterDelay(delay, () => LoadSceneInternal(sceneName))
			);
		}

		private void LoadSceneAfterDelayInternal(string sceneName, float delay)
		{
			CurrentTransitionTarget = sceneName;

			StartCoroutine(
				CoroutineUtils.RunAfterDelay(delay, () => LoadSceneInternal(sceneName))
			);
		}

		

		public static InvalidOperationException CreateTransitionInProgressEx(string existingtransitionTarget, string intendedNewScene)
		{
			return new InvalidOperationException(
				"A transition from current scene '" + SceneManager.GetActiveScene().name + "' "
				+ "to scene '" + existingtransitionTarget + "' is already in progress, "
				+ "so a new one to scene '" + intendedNewScene + "' cannot be started."
			);
		}
	}
}
