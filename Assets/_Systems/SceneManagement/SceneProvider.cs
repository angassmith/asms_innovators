﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames.SceneManagement
{
	public sealed class SceneProvider : MonoBehaviour
	{



		//Unity can't serialize or display dictionaries, so we have to use an underlying list,
		//and transform it to a dictionary. AFAIK there also isn't any good time at which it should be copied across,
		//so it is copied across every time a change is made.

		[SerializeField]
		[HideInInspector]
		private List<SceneRef> _sceneRefsList = new List<SceneRef>();

		private Dictionary<string, SceneRef> _sceneRefsDict = new Dictionary<string, SceneRef>();
		private ReadOnlyDictionary<string, SceneRef> _sceneRefsReadOnlyDict;
		public ReadOnlyDictionary<string, SceneRef> SceneRefs {
			get {
				if (_sceneRefsReadOnlyDict == null) _sceneRefsReadOnlyDict = new ReadOnlyDictionary<string, SceneRef>(_sceneRefsDict);
				return _sceneRefsReadOnlyDict;
			}
		}

		public int MainMenu       { get { return GetSceneByName("MainMenu"); } }
		public int SchoolMap      { get { return GetSceneByName("SchoolMap"); } }
		public int Art            { get { return GetSceneByName("art"); } }
		public int Classroom      { get { return GetSceneByName("Classroom"); } }
		public int Gym            { get { return GetSceneByName("gym"); } }
		public int Library        { get { return GetSceneByName("library"); } }
		public int Kitchen        { get { return GetSceneByName("kitchen"); } }
		public int MusicRoom      { get { return GetSceneByName("musicroom"); } }
		public int ScienceLab     { get { return GetSceneByName("science"); } }
		public int Office         { get { return GetSceneByName("office"); } }
		public int Bedroom        { get { return GetSceneByName("bedroom"); } }
		public int House          { get { return GetSceneByName("house"); } }
		public int KoalaRun       { get { return GetSceneByName("Scene_Endless"); } }
		public int JacobSIPLevel1 { get { return GetSceneByName("Main"); } }
		public int JacobSIPLevel2 { get { return GetSceneByName("Main 2"); } }
		public int JacobSIPLevel3 { get { return GetSceneByName("Main 3"); } }



		public int GetSceneByName(string name)
		{
			if (name == null) throw new ArgumentNullException("name");
			return _sceneRefsDict[name].SceneIndex;
		}

		//	public void Start() //Testing
		//	{
		//		//foreach (Scene s in SceneManager.GetAllScenes())
		//		for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
		//		{
		//			if (!SceneManager.GetSceneByBuildIndex(i).isLoaded) {
		//				SceneManager.LoadScene(i, LoadSceneMode.Additive);
		//			}
		//			Scene s = SceneManager.GetSceneByBuildIndex(i);
		//			Debug.LogFormat("Name: '{0}', Path: '{1}', Index: '{2}'", s.name, s.path, s.buildIndex);
		//		}
		//	}

#if UNITY_EDITOR
		[UnityEditor.CustomEditor(typeof(SceneProvider))]
		private class Editor : UnityEditor.Editor
		{
			private static bool inited;

			private static GUIStyle whiteTextStyle;
			private static GUIStyle inactiveRightMiniButtonStyle;
			private static GUIStyle inactiveMidMiniButtonStyle;

			private static void StaticInit() //Unity doesn't allow static constructors to call certain functions
			{
				if (!inited)
				{
					whiteTextStyle = new GUIStyle();
					whiteTextStyle.SetForAllStates(new GUIStyleState() { textColor = Color.white });

					inactiveRightMiniButtonStyle = new GUIStyle(EditorStyles.miniButtonRight);
					inactiveRightMiniButtonStyle.SetForAllStates(textColor: Color.grey);

					inactiveMidMiniButtonStyle = new GUIStyle(EditorStyles.miniButtonMid);
					inactiveMidMiniButtonStyle.SetForAllStates(textColor: Color.gray);

					inited = true;
				}
			}



			//private Vector2 listScrollPos;


			public override void OnInspectorGUI()
			{
				StaticInit();

				base.OnInspectorGUI();

				var sceneProvider = (SceneProvider)target;

				GUILayout.Label("Scene References: ", new GUIStyle(whiteTextStyle) { fontStyle = FontStyle.Bold });

				EditorGUILayout.BeginFadeGroup(1);
				{
					int i = 0;
					foreach (SceneRef sceneRef in sceneProvider._sceneRefsList)
					{
						EditorGUILayout.InspectorTitlebar(foldout: false, targetObj: sceneRef, expandable: false);


						EditorGUILayout.BeginHorizontal();
						{
							GUILayout.Label("Name:");
							var newName = EditorGUILayout.TextField(sceneRef.name);
							GUILayout.Label("Index:");
							var newIndex = EditorGUILayout.IntField(sceneRef.SceneIndex);

							bool changed = false;
							if (newName != sceneRef.name)
							{
								if (sceneProvider._sceneRefsDict.ContainsKey(newName)) {
									Debug.Log("Duplicate scene name '" + newName + "' in " + typeof(SceneProvider).Name + ".");
								} else {
									sceneRef.name = newName;
									changed = true;
								}
							}

							if (newIndex != sceneRef.SceneIndex)
							{
								sceneRef.SetSceneIndexINTERNAL(newIndex);
								changed = true;
							}

							if (changed) BuildSceneRefsDict();
						}
						EditorGUILayout.EndHorizontal();


						EditorGUILayout.BeginHorizontal();
						{
							
							if (GUILayout.Button("Remove", EditorStyles.miniButtonLeft))
							{
								var removed = sceneProvider._sceneRefsList[i];
								sceneProvider._sceneRefsList.RemoveAt(i);
								DestroyImmediate(removed);
								BuildSceneRefsDict();
								break; //Can't edit during a foreach
							}

							if (GUILayout.Button("Insert before", EditorStyles.miniButtonMid))
							{
								var newSceneRef = CreateInstance<SceneRef>();
								newSceneRef.name = "(" + Guid.NewGuid().ToString() + ")";
								sceneProvider._sceneRefsList.Insert(i, newSceneRef);
								BuildSceneRefsDict();
								break; //Can't edit during a foreach
							}

							if (i > 0)
							{
								if (GUILayout.Button(@"/\", EditorStyles.miniButtonMid))
								{
									SceneRef current = sceneProvider._sceneRefsList[i];
									sceneProvider._sceneRefsList.RemoveAt(i);
									sceneProvider._sceneRefsList.Insert(i - 1, current);
									break; //Can't edit during a foreach
								}
							}
							else
							{
								GUILayout.Button(@"/\", inactiveMidMiniButtonStyle);
							}

							if (i < sceneProvider._sceneRefsList.Count - 1)
							{
								if (GUILayout.Button(@"\/", EditorStyles.miniButtonRight))
								{
									SceneRef current = sceneProvider._sceneRefsList[i];
									sceneProvider._sceneRefsList.RemoveAt(i);
									sceneProvider._sceneRefsList.Insert(i + 1, current);
									break; //Can't edit during a foreach
								}
							}
							else
							{
								GUILayout.Button(@"\/", inactiveRightMiniButtonStyle);
							}
						}
						EditorGUILayout.EndHorizontal();


						i++;
					}

					if (GUILayout.Button("Add")) {
						var newSceneRef = CreateInstance<SceneRef>();
						newSceneRef.name = "(" + Guid.NewGuid().ToString() + ")";
						sceneProvider._sceneRefsList.Add(newSceneRef);
						BuildSceneRefsDict();
					}

				}
				EditorGUILayout.EndFadeGroup();
			}

			private void BuildSceneRefsDict()
			{
				var sceneProvider = (SceneProvider)target;

				sceneProvider._sceneRefsDict.Clear();
				//Can't create a new dictionary instance as the read only version is attached to a specific instance
				foreach (SceneRef scene in sceneProvider._sceneRefsList)
				{
					sceneProvider._sceneRefsDict.Add(scene.name, scene);
				}
			}
		}
#endif
	}
}

//*/