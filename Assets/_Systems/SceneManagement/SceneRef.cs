﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames.SceneManagement
{
	public class SceneRef : ScriptableObject
	{
		[SerializeField] //And make editable in inspector
		private int _sceneIndex;
		public int SceneIndex { get { return _sceneIndex; } }
		
		internal void SetSceneIndexINTERNAL(int newIndex)
		{
			this._sceneIndex = newIndex;
		}

		//	[SerializeField] //Test if this works for objects of type Scene
		//	private Scene _scene;
		//	public Scene Scene { get { return _scene; } }
		//	
		//	public void Awake() //Might have to use Start() instead
		//	{
		//		this._scene = SceneManager.GetSceneAt(SceneIndex);
		//	}
	}
}

//*/