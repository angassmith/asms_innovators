﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Likeability
{
	public class LikeabilityChangedEventArgs : EventArgs
	{
		public LikeabilityChangedEventArgs Source;
		public NPCSetLikeability UpdatedInst { get; private set; }
		public float ChangeAmount { get; private set; }
		public float NewValue { get; private set; }
		public string Reason { get; private set; }
		public bool Propagate { get; private set; }

		public LikeabilityChangedEventArgs RootSource {
			get { return EnumerateSourceChain().Last(); }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="source">The likeability change event that triggered the current change. Null if this is the first change.</param>
		/// <param name="updatedInst"></param>
		/// <param name="changeAmount"></param>
		/// <param name="newValue"></param>
		/// <param name="reason"></param>
		/// <param name="propagate"></param>
		public LikeabilityChangedEventArgs(LikeabilityChangedEventArgs source, NPCSetLikeability updatedInst, float changeAmount, float newValue, string reason, bool propagate)
		{
			if (updatedInst == null) throw new ArgumentNullException("updatedInst");
			if (reason == null) throw new ArgumentNullException("reason");

			this.Source = source;
			this.UpdatedInst = updatedInst;
			this.ChangeAmount = changeAmount;
			this.NewValue = newValue;
			this.Reason = reason;
			this.Propagate = propagate;
		}

		/// <summary>
		/// Returns the current object, then its source (if it has one),
		/// then that event's source (if it has one), and so on.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<LikeabilityChangedEventArgs> EnumerateSourceChain() {
			var cur = this;
			while (cur != null) {
				yield return cur;
				cur = cur.Source;
			}
		}
	}
}

//*/