﻿/*

using System;
using System.Collections;
using Fungus;
using RevolitionGames;
using RevolitionGames.Likeability;
using RevolitionGames.NPCs;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.Likeability
{
	[CommandInfo("Likeability", "NPC Likeability Update", "Changes the likeability of an NPC using a preset reason")]
	public class ChangeNPCSetLikeability : Command
	{
		[SerializeField] //And edit in inspector
		private CodeNameRef _npcSetName;
		public CodeNameRef NPCSetName { get { return _npcSetName; } }

		[NonSerialized]
		private NPCSet _npcSet;
		public NPCSet NPCSet {
			get {
				if (_npcSet == null) {
					if (!Game.Current.NPCHost.AllNPCSets.TryGetValue(_npcSetName.CodeName, out _npcSet)) {
						throw new InvalidOperationException("No NPCSet with name '" + _npcSetName.CodeName + "' has been registered.");
					}
				}
				return _npcSet;
			}
		}

		[SerializeField]
		private LikeabilityChangeReason _change;
		public LikeabilityChangeReason Change { get { return _change; } }

		public override void OnEnter()
		{
			NPCSet.Likeability.ChangePlayerLikeability(Change);

			Continue();
		}
	}

	[CommandInfo("Likeability", "Custom NPC Likeability Update", "Changes the likeability of an NPC using a custom reason")]
	public class CustomChangeNPCSetLikeability : Command
	{
		[SerializeField] //And edit in inspector
		private CodeNameRef _npcSetName;
		public CodeNameRef NPCSetName { get { return _npcSetName; } }

		[NonSerialized]
		private NPCSet _npcSet;
		public NPCSet NPCSet {
			get {
				if (_npcSet == null) {
					if (!Game.Current.NPCHost.AllNPCSets.TryGetValue(_npcSetName.CodeName, out _npcSet)) {
						throw new InvalidOperationException("No NPCSet with name '" + _npcSetName.CodeName + "' has been registered.");
					}
				}
				return _npcSet;
			}
		}

		[SerializeField]
		private int _change;
		public int Change { get { return _change; } }

		[SerializeField]
		public string _reason;
		public string Reason { get { return _reason; } }

		public override void OnEnter()
		{
			NPCSet.Likeability.ChangePlayerLikeability(Change, Reason);
			
			Continue();
		}
	}

	//	[CommandInfo("Likeability", "Group NPC Likeability Update", "Changes the likeability of an NPC group using a preset reason")]
	//	public class ChangeGroupLikeability : Command
	//	{
	//		[SerializeField]
	//		private string _npcGroupName;
	//		[NonSerialized]
	//		private NPCGroup _npcGroup;
	//		public NPCGroup NPCGroup {
	//			get {
	//				if (_npcGroup == null)
	//				{
	//					NPCSet found;
	//					if (Game.Current.NPCHost.AllNPCSets.TryGetValue(_npcGroupName, out found) && (found is NPCGroup)) {
	//						_npcGroup = (NPCGroup)found;
	//					} else {
	//						throw new InvalidOperationException("No NPCGroup with name '" + _npcGroupName + "' has been registered.");
	//					}
	//				}
	//				return _npcGroup;
	//			}
	//		}
	//	
	//		[SerializeField]
	//		private LikeabilityChangeReason _reason;
	//		public LikeabilityChangeReason Reason { get { return _reason; } }
	//	
	//		public override void OnEnter()
	//		{
	//			NPCGroup.Likeability.ChangePlayerLikeability(Reason);
	//	
	//			Continue();
	//		}
	//	
	//	}
	//	
	//	[CommandInfo("Likeability", "Custom Group NPC Likeability Update", "Changes the likeability of an NPC group using a custom reason")]
	//	public class CustomChangeGroupLikeability : Command
	//	{
	//		[SerializeField]
	//		private string _npcGroupName;
	//		[NonSerialized]
	//		private NPCGroup _npcGroup;
	//		public NPCGroup NPCGroup {
	//			get {
	//				if (_npcGroup == null)
	//				{
	//					NPCSet found;
	//					if (Game.Current.NPCHost.AllNPCSets.TryGetValue(_npcGroupName, out found) && (found is NPCGroup)) {
	//						_npcGroup = (NPCGroup)found;
	//					} else {
	//						throw new InvalidOperationException("No NPCGroup with name '" + _npcGroupName + "' has been registered.");
	//					}
	//				}
	//				return _npcGroup;
	//			}
	//		}
	//	
	//		[SerializeField]
	//		private int _change;
	//		public int Change { get { return _change; } }
	//	
	//		[SerializeField]
	//		public string _reason;
	//		public string Reason { get { return _reason; } }
	//	
	//		public override void OnEnter()
	//		{
	//			NPCGroup.Likeability.ChangePlayerLikeability(Change, Reason);
	//	
	//			Continue();
	//		}
	//	
	//	}
}

//*/