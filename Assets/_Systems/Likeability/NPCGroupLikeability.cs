﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using RevolitionGames.Utilities.Alex;
using RevolitionGames.NPCs;
using UnityEngine;

namespace RevolitionGames.Likeability
{
	public class NPCGroupLikeability : NPCSetLikeability
	{
		public const int DefaultLikeability = 0;
		public const int DefaultMinLikeability = 0;
		public const int DefaultMaxLikeability = 0;
		public const int DefaultSummedLikeability = 0;

		public NPCGroup NPCGroup { get; private set; }

		public float PropagationStrength { get; private set; }
		
		private IncrementalMean _incrementalMean = new IncrementalMean();
		//IncrementalRange is inefficient/ugly/useless when the values have to be modified, as
		//modification requires complete recalculation, so cached values are used instead
		private float? minPlayerLikeability;
		private float? maxPlayerLikeability;

		/// <summary>Mean player likeability.</summary>
		public override float PlayerLikeability {
			get {
				if (NPCGroup.NPCs.Count == 0) return DefaultLikeability;
				else return (float)_incrementalMean.Mean;
			}
		}
		public override float SummedPlayerLikeability {
			get {
				if (NPCGroup.NPCs.Count == 0) return DefaultSummedLikeability;
				else return (float)_incrementalMean.Sum;
			}
		}

		public override float MinPlayerLikeability {
			get {
				if (minPlayerLikeability == null)
				{
					if (NPCGroup.NPCs.Count == 0) minPlayerLikeability = DefaultMinLikeability;
					else minPlayerLikeability = NPCGroup.NPCs.Min(npc => npc.Likeability.PlayerLikeability);
				}
				return minPlayerLikeability.Value;
			}
		}
		public override float MaxPlayerLikeability {
			get {
				if (maxPlayerLikeability == null)
				{
					if (NPCGroup.NPCs.Count == 0) maxPlayerLikeability = DefaultMaxLikeability;
					else maxPlayerLikeability = NPCGroup.NPCs.Max(npc => npc.Likeability.PlayerLikeability);
				}
				return maxPlayerLikeability.Value;
			}
		}



		public NPCGroupLikeability(NPCGroup npcGroup, float propagationStrength)
		{
			if (npcGroup == null) throw new ArgumentNullException("npcGroup");
			if (propagationStrength < 0) throw new ArgumentOutOfRangeException("Cannot be less than zero.", "propagationStrength");

			this.NPCGroup = npcGroup;
			this.PropagationStrength = propagationStrength;

			npcGroup.NPCAdded += OnNPCAdded;
			npcGroup.NPCRemoved += OnNPCRemoved;

			this.LikeabilityChanged += (sender, e) => {
				minPlayerLikeability = null;
				maxPlayerLikeability = null;
			};

			foreach (var npc in npcGroup.NPCs)
			{
				IncludeNPC(npc);
			}
		}


		private void IncludeNPC(NPCData npc)
		{
			npc.Likeability.LikeabilityChanged += OnNPCLikeabilityChanged;

			_incrementalMean.Incorporate(npc.Likeability.PlayerLikeability);
		}

		private void ExcludeNPC(NPCData npc)
		{
			npc.Likeability.LikeabilityChanged -= OnNPCLikeabilityChanged;

			_incrementalMean.Remove(npc.Likeability.PlayerLikeability);
		}

		private void OnNPCAdded(object sender, AddRemoveEventArgs<NPCData> e) { IncludeNPC(e.Item); }

		private void OnNPCRemoved(object sender, AddRemoveEventArgs<NPCData> e) { ExcludeNPC(e.Item); }



		private void OnNPCLikeabilityChanged(object sender, LikeabilityChangedEventArgs e)
		{
			var oldMean = _incrementalMean.Mean;
			_incrementalMean.Modify(e.ChangeAmount);

			PropagateChangeIfNeeded(e);

			if (!e.EnumerateSourceChain().Any(evt => ReferenceEquals(evt.UpdatedInst, this)))
			{
				//This block will not be entered if the npc's change was triggered by a change to
				//the likeability of the whole current group. In this way we prevent redundant events
				//for group likeability changes.
				//Changes caused by other groups, and changes caused by propagation, may still
				//cause redundant events, but I don't know of any way to avoid this

				FireLikeabilityChanged(
					sender: this,
					e: new LikeabilityChangedEventArgs(
						source: e,
						updatedInst: this,
						changeAmount: (float)(_incrementalMean.Mean - oldMean),
						newValue: (float)_incrementalMean.Mean,
						reason: e.Reason,
						propagate: false //Irrelevant
					)
				);
			}
		}

		private void PropagateChangeIfNeeded(LikeabilityChangedEventArgs e)
		{
			if (e.Propagate)
			{
				foreach (NPCData npc in NPCGroup.NPCs)
				{
					if (!ReferenceEquals(npc, e.UpdatedInst)) //Don't propagate back to the source of the propagation
					{
						ChangePlayerLikeabilityInternal(
							set: npc.Likeability,
							amount: e.ChangeAmount * PropagationStrength,
							reason: e.Reason,
							propagate: false, //Only propagate once
							sourceEvent: e
						);
					}
				}
			}
		}

		protected override void ChangePlayerLikeabilityInternal(float amount, string reason, bool propagate, LikeabilityChangedEventArgs sourceEvent)
		{
			var currentEvent = new LikeabilityChangedEventArgs(
				source: sourceEvent,
				updatedInst: this,
				changeAmount: amount,
				newValue: PlayerLikeability + amount, //This can be predicted (fortunately)
				reason: reason,
				propagate: propagate
			);

			foreach (NPCData npc in NPCGroup.NPCs)
			{
				ChangePlayerLikeabilityInternal(
					set: npc.Likeability,
					amount: amount,
					reason: reason,
					propagate: propagate,
					sourceEvent: currentEvent
				);
			}

			FireLikeabilityChanged(this, currentEvent);
		}
	}
}

//*/