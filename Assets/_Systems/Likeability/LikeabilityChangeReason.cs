﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

namespace RevolitionGames.Likeability
{
	public enum LikeabilityChangeReason
	{
		//Entries set to more than 1000 are undecided
		MadeFunOf = 1001,
		Punched = 1002,
		Excluded = 1003,
		SharedFood = 1004,
		MadeFriends = 1005,
		CoveredFor = 1006,
		HelpedUpAfterInjury = 1007,
		ApologisedTo = 1008,

	}
}

//*/