﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;
using RevolitionGames;
using RevolitionGames.Likeability;
using RevolitionGames.NPCs;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace Assets._Systems.Likeability
{
	[CommandInfo("Likeability", "Custom NPC Likeability Update", "Changes the likeability of an NPC using a custom reason")]
	public class CustomChangeLikeabilityCommand : Command
	{
        [SerializeField] //And edit in inspector
        private string _npcSetName = "";
		public string NPCSetName { get { return _npcSetName; } }

		
		private NPCSet _npcSet;
		public NPCSet NPCSet {
			get {
				if (_npcSet == null) {
                    //Debug.Log(_npcSetName);
					if (!PlayModeStage.Current.NPCHost.AllNPCSets.TryGetValue(_npcSetName, out _npcSet)) {
						throw new InvalidOperationException("No NPCSet with name '" + _npcSetName   + "' has been registered.");
					}
				}
				return _npcSet;
			}
		}

		[SerializeField]
		private int _change;
		public int Change { get { return _change; } }

        [SerializeField]
        public string _reason = "";
		public string Reason { get { return _reason; } }

		public override void OnEnter()
		{
			NPCSet.Likeability.ChangePlayerLikeability(Change, Reason);
            //Debug.Log(Game.Current.NPCHost.AllNPCs.Likeability.PlayerLikeability);
			
			Continue();
		}
	}
}

//*/