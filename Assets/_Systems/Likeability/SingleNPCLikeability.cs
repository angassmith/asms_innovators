﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using RevolitionGames.Likeability;
using UnityEngine;


namespace RevolitionGames
{
	public class SingleNPCLikeability : NPCSetLikeability
	{
		private float _playerLikeability = 0;
		public override float PlayerLikeability { get { return _playerLikeability; } }

		/// <summary>Irrelevant for individual NPCs</summary>
		public override float SummedPlayerLikeability { get { return _playerLikeability; } }
		/// <summary>Irrelevant for individual NPCs</summary>
		public override float MaxPlayerLikeability { get { return _playerLikeability; } }
		/// <summary>Irrelevant for individual NPCs</summary>
		public override float MinPlayerLikeability { get { return _playerLikeability; } }

		protected override void ChangePlayerLikeabilityInternal(float amount, string reason, bool propagate, LikeabilityChangedEventArgs sourceEvent)
		{
			_playerLikeability += amount;
			FireLikeabilityChanged(
				sender: this,
				e: new LikeabilityChangedEventArgs(
					source: sourceEvent,
					updatedInst: this,
					changeAmount: amount,
					newValue: _playerLikeability,
					reason: reason,
					propagate: propagate
				)
			);
		}
	}







	//	#if UNITY_EDITOR
	//	[UnityEditor.CustomEditor(typeof(NPC), editorForChildClasses:true)]
	//	public class NPCEditor : UnityEditor.Editor
	//	{
	//		public sealed override void OnInspectorGUI()
	//		{
	//			DrawDefaultInspector();
	//	
	//			NPC targetNPC = target as NPC;
	//			if (targetNPC == null) return;
	//	
	//			targetNPC._playerLikeability = EditorGUILayout.FloatField("Starting Player Likeability", targetNPC._playerLikeability);
	//	
	//			OnInspectorGUI2();
	//		}
	//	
	//		protected virtual void OnInspectorGUI2() { }
	//	}
	//	#endif
}

//*/