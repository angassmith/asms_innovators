﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Likeability
{
	public abstract class NPCSetLikeability
	{
		public abstract float PlayerLikeability { get; }
		public abstract float MaxPlayerLikeability { get; }
		public abstract float MinPlayerLikeability { get; }
		public abstract float SummedPlayerLikeability { get; }
		public float PlayerLikeabilityRange { get { return MinPlayerLikeability - MaxPlayerLikeability; } }
		
		public event EventHandler<LikeabilityChangedEventArgs> LikeabilityChanged;


		protected void FireLikeabilityChanged(object sender, LikeabilityChangedEventArgs e)
		{
			if (LikeabilityChanged != null) LikeabilityChanged.Invoke(sender, e);
		}

		public void ChangePlayerLikeability(LikeabilityChangeReason amount, bool propagate = true) {
			ChangePlayerLikeability((float)amount, amount.ToString(), propagate);
        }

        //Note: 'reason' makes the calling code clearer, and may be used in future (eg. for logging or analytics)
        public void ChangePlayerLikeability(float amount, string reason, bool propagate = true) {
			ChangePlayerLikeabilityInternal(this, amount, reason, propagate, sourceEvent: null);
		}

		/// <summary>
		/// Static wrapper for <see cref="ChangePlayerLikeabilityInternal(float, string, bool, LikeabilityChangedEventArgs)"/>,
		/// so that any derived class can call it on any other derived class.
		/// </summary>
		/// <param name="sourceEvent">The likeability change event that triggered the current change. Null if this is the first change.</param>
		protected static void ChangePlayerLikeabilityInternal(NPCSetLikeability set, float amount, string reason, bool propagate, LikeabilityChangedEventArgs sourceEvent) {
			set.ChangePlayerLikeabilityInternal(amount, reason, propagate, sourceEvent);
		}

		protected abstract void ChangePlayerLikeabilityInternal(float amount, string reason, bool propagate, LikeabilityChangedEventArgs sourceEvent);
	}
}

//*/