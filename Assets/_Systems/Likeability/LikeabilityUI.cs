﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.Likeability
{
	public class LikeabilityUI : MonoBehaviour {

		private const float LowerLikeabilityLimit = -1000;
		private const float UpperLikeabilityLimit = 1000;

		[SerializeField]
		private Image averageLikeabilityBar;
		[SerializeField]
		private Color lowBarColour;
		[SerializeField]
		private Color fullBarColour;

		[SerializeField]
		private Image lowestLikeabilityMarker;
		[SerializeField]
		private Color lowMarkerColour;
		[SerializeField]
		private Color fullMarkerColour;

		[SerializeField]
		private Text averagelikeabilityText;
		[SerializeField]
		private Text lowestlikeabilityText;

        private float fillAmount; 

		private void Start()
		{
            //Debug.Log(Game.Current);
            //Debug.Log(Game.Current.NPCHost);
            //Debug.Log(Game.Current.NPCHost.AllNPCs);
            //Debug.Log(Game.Current.NPCHost.AllNPCs.Likeability);
            averageLikeabilityBar.fillAmount = 0.5f;
			PlayModeStage.Current.NPCHost.AllNPCs.Likeability.LikeabilityChanged += OnLikeabilityChanged;
			UpdateUI();
		}

		private void OnLikeabilityChanged(object sender, LikeabilityChangedEventArgs e)
		{
			UpdateUI();
		}

		private void UpdateUI()
		{
            //if (fillAmount != likeabilityVisualScale.fillAmount)
            //{
            //likeabilityVisualScale.fillAmount = Mathf.Lerp(likeabilityVisualScale.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
            //}
            //float likeabilityRatio = Game.Current.NPCHost.AllNPCs.Likeability.PlayerLikeability / likeabilityMax;


            float avgLikeability = PlayModeStage.Current.NPCHost.AllNPCs.Likeability.PlayerLikeability;
			float lowestLikeability = PlayModeStage.Current.NPCHost.AllNPCs.Likeability.MinPlayerLikeability;

			float avgLikeabilityFraction = MapBetweenRanges(avgLikeability, LowerLikeabilityLimit, UpperLikeabilityLimit, 0, 1);
			float lowestLikeabilityFraction = MapBetweenRanges(lowestLikeability, LowerLikeabilityLimit, UpperLikeabilityLimit, 0, 1);

            //Update average likeability scale

            averageLikeabilityBar.fillAmount = avgLikeabilityFraction;
            averageLikeabilityBar.color = Color.Lerp(lowBarColour, fullBarColour, avgLikeabilityFraction);

            //Update lowest likeability scale
            lowestLikeabilityMarker.rectTransform.anchorMin = new Vector2(lowestLikeabilityFraction, 0.5f);
			lowestLikeabilityMarker.rectTransform.anchorMax = new Vector2(lowestLikeabilityFraction, 0.5f);
			lowestLikeabilityMarker.color = Color.Lerp(lowMarkerColour, fullMarkerColour, lowestLikeabilityFraction);

			//Update likeability texts
			averagelikeabilityText.text = "Overall Likeability: " + (int)avgLikeability;
			lowestlikeabilityText.text = "Lowest Likeability: " + (int)lowestLikeability;
		}

		private static float MapBetweenRanges(float value, float inMin, float inMax, float outMin, float outMax)
		{
			float inRange = inMax - inMin;
			float outRange = outMax - outMin;

			float inOffset = value - inMin;
			float fraction = inOffset / inRange;
			float outOffset = fraction * outRange;
			return outOffset + outMin;

			//	return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
			//	//(80 - -1000) * (1 - 0) / (1000 - -1000) + 0;
			//	// 1080 * 1 / 2000 = 0.54
		}

	}
}