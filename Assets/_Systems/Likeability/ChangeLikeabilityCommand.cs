﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;
using RevolitionGames;
using RevolitionGames.Likeability;
using RevolitionGames.NPCs;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.Likeability
{
	[CommandInfo("Likeability", "NPC Likeability Update", "Changes the likeability of an NPC using a preset reason")]
	public class ChangeLikeabilityCommand : Command
	{
		[SerializeField] //And edit in inspector
		private string _npcSetName;
		public string NPCSetName { get { return _npcSetName; } }

		[NonSerialized]
		private NPCSet _npcSet;
		public NPCSet NPCSet {
			get {
				if (_npcSet == null) {
					if (!PlayModeStage.Current.NPCHost.AllNPCSets.TryGetValue(_npcSetName, out _npcSet)) {
						throw new InvalidOperationException("No NPCSet with name '" + _npcSetName + "' has been registered.");
					}
				}
				return _npcSet;
			}
		}

		[SerializeField]
		private LikeabilityChangeReason _change;
		public LikeabilityChangeReason Change { get { return _change; } }

		public override void OnEnter()
		{
			NPCSet.Likeability.ChangePlayerLikeability(Change);
            

			Continue();
		}
	}
}

//*/