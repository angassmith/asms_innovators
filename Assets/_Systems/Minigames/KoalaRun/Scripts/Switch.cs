﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {

    public GameObject Panel;
	
	// Update is called once per frame
	void Update () {
	
	}

    public void errythang()
    {
        if (Panel.activeSelf == false)
        {
            Panel.SetActive(true);
        }
        else if (Panel.activeSelf == true)
        {
            Panel.SetActive(false);
        }
    }

    public void pause()
    {
        Time.timeScale = 0;
    }
}
