﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Linq;
using UnityEngine;

public static class Likeability
{
	public static float OverallLikeability { get; private set; }
	
	public static float OverallFamilyLikeability { get; private set; }
	public static float OverallFriendLikeability { get; private set; }
	public static float OverallStudentLikeability { get; private set; }
	public static float OverallTeacherLikeability { get; private set; }

	public static float MinimumLikeability { get; private set; }

	public static event EventHandler<LikeabilityChangeEventArgs> OverallLikeabilityChanged;
	
	//TODO: Call this after all NPCs have been created
	public static void Setup()
	{

		UpdateOverallLikeability();
	}

	public static void UpdateOverallLikeability()
	{



		float totalLikeability        = 0;
		float totalFamilyLikeability  = 0;
		float totalFriendLikeability  = 0;
		float totalStudentLikeability = 0;
		float totalTeacherLikeability = 0;
		float totalCount   = 0.0000001F; //Avoid divide by zero errors
		float familyCount  = 0.0000001F;
		float friendCount  = 0.0000001F;
		float studentCount = 0.0000001F;
		float teacherCount = 0.0000001F;
		float minLikeability = float.PositiveInfinity;
		
		/*foreach (NPC npc in allNPCs)
		{
			if (npc == null) continue; //Should never happen

			totalCount++;
			totalLikeability += npc.PlayerLikeability;

			minLikeability = Math.Min(minLikeability, npc.PlayerLikeability);

			if (((int)npc.Type & (int)NPCTypeFlags.Family ) != 0) { familyCount++;  totalFamilyLikeability  += npc.PlayerLikeability; }
			if (((int)npc.Type & (int)NPCTypeFlags.Friend ) != 0) { friendCount++;  totalFriendLikeability  += npc.PlayerLikeability; }
			if (((int)npc.Type & (int)NPCTypeFlags.Student) != 0) { studentCount++; totalStudentLikeability += npc.PlayerLikeability; }
			if (((int)npc.Type & (int)NPCTypeFlags.Teacher) != 0) { teacherCount++; totalTeacherLikeability += npc.PlayerLikeability; }
		}*/

		OverallLikeability        = totalLikeability        / totalCount  ;
		OverallFamilyLikeability  = totalFamilyLikeability  / familyCount ;
		OverallFriendLikeability  = totalFriendLikeability  / friendCount ;
		OverallStudentLikeability = totalStudentLikeability / studentCount;
		OverallTeacherLikeability = totalTeacherLikeability / teacherCount;
		MinimumLikeability = minLikeability;

		if (OverallLikeabilityChanged != null) {
			OverallLikeabilityChanged(null, new LikeabilityChangeEventArgs(OverallLikeability));
		}
	}

	/*public static void DecayAllPlayerLikeabilities()
	{
		NPC[] allNPCs = NPC.AllNPCs;

		foreach (NPC npc in allNPCs) {
			npc.DecayPlayerLikeabilityNow();
		}

		UpdateOverallLikeability();
	}*/
}

public class LikeabilityChangeEventArgs : EventArgs
{
	public readonly float NewOverallLikeability;
	public LikeabilityChangeEventArgs(float newOverallLikeability) {
		this.NewOverallLikeability = newOverallLikeability;
	}
}






//*/