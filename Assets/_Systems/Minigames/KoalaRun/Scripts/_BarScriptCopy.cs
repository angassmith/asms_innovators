﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class _BarScriptCopy : MonoBehaviour
{
    private static float fillAmount;

    [SerializeField]
    private static float lerpSpeed;

    [SerializeField]
    private static Image content;

    [SerializeField]
    private static Text valueText;

    [SerializeField]
    private static Color fullColour;

    [SerializeField]
    private static Color lowColour;

    public static float MaxValue { get; set;}

    public static float Value
    {
        set
        {
            string[] tmp = valueText.text.Split(':');
            valueText.text = tmp[0] + ":" + value;
            fillAmount = Map(value, 0, MaxValue, 0, 1);
        }
    }

	public static void Awake()
    {
        Likeability.OverallLikeabilityChanged += OnOverallLikeabilityChanged;
    }
	
    private static void OnOverallLikeabilityChanged(object sender, LikeabilityChangeEventArgs e)
    {
        //Do Stuff
        if (fillAmount != content.fillAmount)
        {
            content.fillAmount = Mathf.Lerp(content.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
            Debug.Log(fillAmount);
        }

        content.color = Color.Lerp(lowColour, fullColour, fillAmount);

    }

    private static float Map(float value, float inMin, float inMax, float outMin, float outMax)
    {
        return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
        //(80 - 0) * (1 - 0) / (100 - 0) + 0;
        // 80 * 1 / 100 = 0.8
    }
}
