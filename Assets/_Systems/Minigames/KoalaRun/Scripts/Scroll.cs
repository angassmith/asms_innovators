﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour {

    public float hSpeed = 0.5f;
    public float vSpeed = 0.5f;

    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(Time.time * hSpeed, Time.time * vSpeed);
	}
}
