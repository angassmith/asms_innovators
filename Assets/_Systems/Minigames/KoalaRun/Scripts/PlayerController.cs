﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using RevolitionGames;
using UnityEngine.UI;
using RevolitionGames.SceneManagement;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	public float jumpForce;

    public static bool disableMovement;

	public float jumpTime;
	private float jumpTimeCounter;

	private Rigidbody2D myRigidbody;

	public bool grounded;
	public LayerMask whatIsGround;

    public Text countDown;

	private Collider2D myCollider;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody2D>();
		myCollider = GetComponent<Collider2D>();
        disableMovement = true;
		jumpTimeCounter = jumpTime;
        StartCoroutine(Wait());
	}
	
	// Update is called once per frame
	void Update () {

        if (!disableMovement)
        {
            grounded = Physics2D.IsTouchingLayers(myCollider, whatIsGround);

            myRigidbody.velocity = new Vector2(moveSpeed, myRigidbody.velocity.y);

            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                if (grounded)
                {
                    myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
                }
            }

            if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
            {
                if (jumpTimeCounter > 0)
                {
                    myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, jumpForce);
                    jumpTimeCounter -= Time.deltaTime;
                }
            }

            if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0))
            {
                jumpTimeCounter = 0;
            }

            if (grounded)
            {
                jumpTimeCounter = jumpTime;
            }
        }
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "RESET")
        {
            disableMovement = true;
            //StartCoroutine(Wait());
            StartCoroutine(Dead());
        }
    }

    IEnumerator Dead()
    {
        yield return new WaitForSeconds(1f);
        MinigameStage.Current.SwitchToMinigameScene(MinigameSceneData.Current.SceneName); //Reload scene & data
    }

    IEnumerator Wait()
    {
        if(countDown.enabled == false)
        {
            countDown.enabled = true;
        }
        countDown.text = "3";
        yield return new WaitForSeconds(0.5f);
        countDown.text = "2";
        yield return new WaitForSeconds(0.5f);
        countDown.text = "1";
        yield return new WaitForSeconds(0.5f);
        countDown.text = "GO!";
        yield return new WaitForSeconds(0.25f);
        disableMovement = false;
        countDown.enabled = false;
    }

}
