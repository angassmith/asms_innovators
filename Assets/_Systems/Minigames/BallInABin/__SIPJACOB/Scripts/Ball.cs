﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using RevolitionGames;
using RevolitionGames.SceneManagement;

public class Ball : MonoBehaviour {

    public int nextScene; //TODO: Remove?
    public string sceneName;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D other)
    { 
        if (other.gameObject.tag == "Goal")
        {
			if (SceneInfo.GetSceneType(sceneName) == SceneType.PlayMode3D) {
				Game.Current.LoadAndPlay();
			} else {
				MinigameStage.Current.SwitchToMinigameScene(sceneName);
			}
        }
    }
}
