﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using RevolitionGames;

public class UIController : MonoBehaviour {

    public int currentScene = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Redo()
    {
		MinigameStage.Current.SwitchToMinigameScene(MinigameSceneData.Current.SceneName); //Reload scene & data
    }
    
}
