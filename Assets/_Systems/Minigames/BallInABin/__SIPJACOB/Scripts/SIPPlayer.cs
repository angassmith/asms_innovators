﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Controller2D))]
public class SIPPlayer : MonoBehaviour {

    public float jumpHeight = 4;
    public float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;
    float moveSpeed = 6;


    static bool moveLeft = false;
    static bool moveRight = false;
    public static bool moveJump = false;

    bool isJumping = false;

    float jumps = 2;
    //float maxJumps = 2;
    float gravity;
    float jumpVelocity = 8;
    Vector3 velocity;
    float velocityXSmoothing;

    Controller2D controller;
    Animator anim;
    SpriteRenderer sprRend;

    float keyboardInputX;
    float touchInputX;

    int runStateHash = Animator.StringToHash("Player_Run");
    int idleStateHash = Animator.StringToHash("Idle");
    int jumpStateHash = Animator.StringToHash("Jump");
    //int fallStateHash = Animator.StringToHash("Fall");

    // Use this for initialization
    void Start () {
        controller = GetComponent<Controller2D>();
        anim = GetComponent<Animator>();
        sprRend = GetComponent<SpriteRenderer>();

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
	}

    // Update is called once per frame
    void LateUpdate() {

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
            isJumping = false;
        }

        if(controller.collisions.below)
        {
            jumps = 2;
        } 

        if(controller.collisions.left || controller.collisions.right)
        {
            jumps = 1;
        }

        keyboardInputX = Input.GetAxisRaw("Horizontal");
        float inputX = Mathf.Clamp(touchInputX + keyboardInputX, min: -1, max: 1);

        if (moveJump || Input.GetKey(KeyCode.Space))
        {
            if (controller.collisions.below)
            {
                isJumping = true;
                jumps = jumps - 1;
                velocity.y = jumpVelocity;
            }
            else if (jumps >= 1)
            {
                isJumping = true;
                velocity.y = jumpVelocity;
                //anim.SetTrigger(jumpStateHash);
                jumps = jumps - 1;
            }
        } 

        if(isJumping)
        {
            anim.SetTrigger(jumpStateHash);
        }

        anim.SetFloat("Speed", moveSpeed);
        AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);



        float targetVelocityX = inputX * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if ((inputX != 0 && controller.collisions.below) || ((moveLeft || moveRight) && controller.collisions.below))
        {
            anim.SetTrigger(runStateHash);
        }
        else if(inputX == 0 && controller.collisions.below || ((!moveLeft || !moveRight) && controller.collisions.below))
            anim.SetTrigger(idleStateHash);

        if (inputX < 0 || moveLeft)
        {
            sprRend.flipX = true;
        }
        else if (inputX > 0 || moveRight)
            sprRend.flipX = false;

        /*if(Input.GetKeyDown(KeyCode.Space) && velocity.y > 0)
        {
            anim.SetTrigger(jumpStateHash);
        }*/
                
    }


    public void StartLeft()
    {
        moveLeft = true;
        touchInputX += -1;
        //float targetVelocityX = input.x * moveSpeed;
        //velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
    }
    public void StartRight()
    {
        moveRight = true;
        touchInputX += 1;
        //float targetVelocityX = input.x * moveSpeed;
        //velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
    }
    public void StopLeft()
    {
        moveLeft = false;
        touchInputX += 1;
        //float targetVelocityX = input.x * moveSpeed;
        //velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
    }
    public void StopRight()
    {
        moveRight = false;
        touchInputX += -1;
        //float targetVelocityX = input.x * moveSpeed;
        //velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
    }
    public void StartJump()
    {
        moveJump = true;
    }
    public void StopJump()
    {
        moveJump = false;
    }
}