﻿using UnityEngine;
using System.Collections;

public class SIPPlayerController : MonoBehaviour {

	public GameObject player;
	Animator anim;
	SpriteRenderer sprRend;
	Rigidbody2D rigid;

	private float inputDirection = 0; // X value of vector
	private float vVel = 0; // Y value of vector
	private Vector2 moveVector;
	//private float gravity = 1.0f;
	private float speed = 5.0f;

	bool isGrounded;

	int runStateHash = Animator.StringToHash("Player_Run");
	int idleStateHash = Animator.StringToHash("Idle");

	// Use this for initialization
	void Start ()
	{
		anim = GetComponent<Animator>();
		sprRend = GetComponent<SpriteRenderer>();
		rigid = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		moveVector = new Vector2(Input.GetAxisRaw("Horizontal") * speed, rigid.velocity.y);
		rigid.velocity = moveVector;

		if(SIPPlayer.moveJump)
		{
			rigid.AddForce(new Vector2(0, vVel));
		}

		anim.SetFloat("Speed", speed);
		AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(0);

		if(inputDirection != 0)
		{ 
			anim.SetTrigger(runStateHash);
		}
		else
			anim.SetTrigger(idleStateHash);

		if (inputDirection < 0)
		{
			sprRend.flipX = true;
		} else if(inputDirection > 0)
			sprRend.flipX = false;

	}

}