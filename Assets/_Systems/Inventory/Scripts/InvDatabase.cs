﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames.Inventory
{
    /// <summary>
    /// Loads item information from an XML file, and holds ItemDef references so they can be retrieved.
    /// </summary>
	public class InvDatabase
	{
        public const string XItemDefs = ItemDef.ItemNamespace + "itemDefinitions";
		public const string XItem = ItemDef.ItemNamespace + "item";

		private static readonly TokenKey<ItemDef> _itemDefFromXmlToken = new TokenKey<ItemDef>();
		internal static readonly TokenKeySetter<ItemDef> ItemDefFromXmlTokenSetter = new TokenKeySetter<ItemDef>(_itemDefFromXmlToken);

		private MultiKeyDict<int, string, ItemDef> Database = new MultiKeyDict<int, string, ItemDef>();

        public void InitialiseDatabase(string itemDefsSchemaFile, string itemDefsXMLFile, string itemSpritesDir) {
            TextAsset xmlFile = Resources.Load(itemDefsXMLFile) as TextAsset;

            XDocument xdoc = XDocument.Parse(xmlFile.text);

            /*foreach (XElement xitem in xdoc.Elements()) {
                Debug.Log("xitem: " + xitem.Name);
            }*/

            foreach (XElement xitem in xdoc.Element(XItemDefs).Descendants(XItem)) {
                ItemDef itemDef = ItemDef.CreateFromXml(xitem, itemSpritesDir, _itemDefFromXmlToken);
                Database.Add(
                    itemDef.ID,
                    itemDef.CodeName,
                    itemDef
                );
            }
        }
		
		
		/*private void UpdateSource()
		{
			var itemDefsXmlSchema = XmlSchema.Read(
				File.OpenRead(itemDefsSchemaFile),
				XmlValidationExceptionLogger
			);

			var xreaderSettings = new System.Xml.XmlReaderSettings();
			xreaderSettings.Schemas.Add(itemDefsXmlSchema);
			xreaderSettings.ValidationType = System.Xml.ValidationType.Schema;
			xreaderSettings.ValidationEventHandler += XmlValidationExceptionLogger;

			var xreader = System.Xml.XmlReader.Create(File.OpenRead(itemDefsXMLFile), xreaderSettings);
			XDocument xdoc = XDocument.Load(xreader);

			foreach (XElement xitem in xdoc.Element(XItemDefs).Descendants(XItem))
			{
				ItemDef itemDef = ItemDef.CreateFromXml(xitem, itemSpritesDir, _itemDefFromXmlToken);
				Database.Add(
					itemDef.ID,
					itemDef.CodeName,
					itemDef
				);
			}

			#region other
			//	foreach (XElement xquest in xdoc.Element("questList").Descendants("quest"))
			//	{
			//		string questName = (string)xquest.Element("name");
			//		string questDescription = (string)xquest.Element("description");
			//	
			//		List<TaskCreator> tasks = new List<TaskCreator>();
			//		foreach (XElement xtask in xquest.Element("taskList").Descendants())
			//		{
			//			switch (xtask.Name.ToString())
			//			{
			//				case "specificCollectionTask":
			//					tasks.Add(
			//						new SpecificCollectionTaskCreator(
			//							//TODO: Put in the correct order
			//							(string)xtask.Element("taskName"),
			//							(string)xtask.Element("description"),
			//							(int)xtask.Element("quantity"),
			//							(string)xtask.Element("collectableItemName")
			//						)
			//					);
			//					break;
			//				case "typeCollectionTask":
			//					tasks.Add(
			//						new TypeCollectionTaskCreator(
			//							//TODO: Put in the correct order
			//							(string)xtask.Element("taskName"),
			//							(string)xtask.Element("description"),
			//							(int)xtask.Element("quantity"),
			//							(string)xtask.Element("collectableItemName")
			//						)
			//					);
			//					break;
			//			}
			//		}
			//	
			//		Quests.Add(new QuestCreator(questName, questDescription, tasks)); //TODO: Correct order
			//		//Where Quests is a public property ( get; private set; ) of type List<QuestCreator>
			//	}
			#endregion
		} */

		private void XmlValidationExceptionLogger(object sender, ValidationEventArgs e)
		{
			if (e.Severity == XmlSeverityType.Error) {
				Debug.LogException(e.Exception);
			} else {
				Debug.LogWarning(e.Exception);
			}
		}

		public ItemDef GetById(int id) {
			return Database.GetValueByKey1(id);
		}

		public bool TryGetById(int id, out ItemDef itemDef) {
			return Database.TryGetValueByKey1(id, out itemDef);
		}

		public ItemDef GetByCodeName(string codeName) {
            try {
                return Database.GetValueByKey2(codeName);
            } catch (KeyNotFoundException) {
                Debug.LogError("Could not find item definition with codename " + codeName);
                throw;
            }
		}

		public bool TryGetByCodeName(string codeName, out ItemDef itemDef) {
			return Database.TryGetValueByKey2(codeName, out itemDef);
		}

		public ItemDef GetExact(int id, string codeName) {
			var found = Database.GetValueByKey1(id);

			if (found == null)
				throw new KeyNotFoundException("An item with the specified id '" + id + "' was found, but it was null.");

			if (found.CodeName == codeName) {
				return found;
			} else {
				throw new KeyNotFoundException(
					"An item with the specified id '"
					+ id
					+ "' was found, but it had CodeName '"
					+ found.CodeName
					+ "' instead of CodeName '"
					+ codeName
					+ "' (Note: Both IDs and CodeNames are independently unique)."
				);
			}
		}

		public bool TryGetExact(int id, string codeName, out ItemDef itemDef)
		{
			ItemDef found;
			if (Database.TryGetValueByKey1(id, out found))
			{
				if (found == null) {
					itemDef = null;
					return false;
				}

				if (found.CodeName == codeName) {
					itemDef = found;
					return true;
				} else {
					itemDef = null;
					return false;
				}
			}
			else
			{
				itemDef = null;
				return false;
			}
		}
	}
}