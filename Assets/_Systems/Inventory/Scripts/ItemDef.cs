﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using UnityEngine;


namespace RevolitionGames.Inventory
{
	public sealed class ItemDef
	{
		private static readonly TokenLock<ItemDef> FromXmlToken = new TokenLock<ItemDef>(new ItemDef(), InvDatabase.ItemDefFromXmlTokenSetter);

        public const string ItemNamespace = "{http://www.revolitiongames.com/experience/items}";

        public const string XId = "id";
		public const string XDisplayName = ItemDef.ItemNamespace + "displayName";
		public const string XCodeName = ItemDef.ItemNamespace + "codeName";
		public const string XDescription = ItemDef.ItemNamespace + "desc";
		public const string XValue = ItemDef.ItemNamespace + "value";
		public const string XConsumable = ItemDef.ItemNamespace + "consumable";
		public const string XIconFileName = ItemDef.ItemNamespace + "icon";

		public int ID { get; private set; }
		public string DisplayName { get; private set; }
		public string CodeName { get; private set; }
		public string Description { get; private set; }
		public int Value { get; private set; }
		public bool Consumable { get; private set; }
		public Sprite Icon { get; private set; }
		public Sprite LargeIcon { //This may be added later
			get { return Icon; }
		}

		private ItemDef() { }

		/// <exception cref="ArgumentNullException">displayName, codeName, or description is null, empty, or whitespace</exception>
		/// <exception cref="ArgumentOutOfRangeException">id or value is less than 0</exception>
		private ItemDef(int id, string displayName, string codeName, string description, int value, bool consumable, string iconFileName, string itemSpritesDir)
		{
			if (id    < 0) throw new ArgumentOutOfRangeException("id"   , id   , "id cannot be less that 0");
			if (value < 0) throw new ArgumentOutOfRangeException("value", value, "value cannot be less than 0");
			if (IsNullOrWhiteSpace(displayName)) throw new ArgumentException("displayName " + "is null, empty, or whitespace", "displayName");
			if (IsNullOrWhiteSpace(codeName   )) throw new ArgumentException("codeName "    + "is null, empty, or whitespace", "codeName"   );
			if (IsNullOrWhiteSpace(description)) throw new ArgumentException("description " + "is null, empty, or whitespace", "description");

			Icon = Resources.Load<Sprite>(Path.Combine(itemSpritesDir, codeName));

			this.ID = id;
			this.DisplayName = displayName;
			this.CodeName = codeName;
			this.Description = description;
			this.Value = value;
			this.Consumable = consumable;
		}

		/// <exception cref="TokenMismatchException">token is invalid</exception>
		/// <exception cref="+more"></exception>
		public static ItemDef CreateFromXml(XElement itemDef, string itemSpritesDir, TokenKey<ItemDef> token)
		{
			FromXmlToken.Check(token, "ItemDef.ctor");

			if (itemDef == null) throw new ArgumentNullException("itemDef");
			if (itemSpritesDir == null) throw new ArgumentNullException("itemSpritesDir");

			return new ItemDef(
				(int)itemDef.Attribute(XId),
				(string)itemDef.Element(XDisplayName),
				(string)itemDef.Element(XCodeName),
				(string)itemDef.Element(XDescription),
				(int)itemDef.Element(XValue),
				(bool)itemDef.Element(XConsumable),
				(string)itemDef.Element(XIconFileName),
				itemSpritesDir
			);
		}

		//Source: https://referencesource.microsoft.com/#mscorlib/system/string.cs,55e241b6143365ef
		private static bool IsNullOrWhiteSpace(string value) {
			if (value == null) return true;
			for(int i = 0; i < value.Length; i++) {
				if(!char.IsWhiteSpace(value[i])) return false;
			}
			return true;
		}

		public override string ToString()
		{
			return string.Concat(
				"ItemDef { ",
				"ID: " + ID + ", ",
				"DisplayName: " + DisplayName + ", ",
				"Value: " + Value + ", ",
				"CodeName: " + CodeName + ", ",
				"Consumable: " + Consumable + ", ",
				"Description: " + Description,
				" }"
			);
		}
	}
}

//*/