﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RevolitionGames.Inventory
{
    /// <summary>
    /// The UI slot for an inventory item.
    /// </summary>
	public class InventoryItemSlot : Selectable
	{
        /// <summary>
        /// The inventory item that this slot is displaying.
        /// </summary>
        public InventoryItem InvItem { get; private set; }

        /// <summary>
        /// The icon for the item.
        /// </summary>
        private Image icon;

        /// <summary>
        /// The text for the quantity of the item.
        /// </summary>
        private Text count;

		private static event EventHandler<InventoryItemSlot, EventArgs> _itemSelected;
		/// <summary>Fired when an item is selected. Duplicate subscriptions will be ignored.</summary>
		public static event EventHandler<InventoryItemSlot, EventArgs> ItemSelected {
			add {
				_itemSelected -= value;
				_itemSelected += value;
			}
			remove { _itemSelected -= value; }
		}

		private static event EventHandler<InventoryItemSlot, EventArgs> _itemDeselected;
		/// <summary>Fired when an item is selected. Duplicate subscriptions will be ignored.</summary>
		public static event EventHandler<InventoryItemSlot, EventArgs> ItemDeselected {
			add {
				_itemDeselected -= value;
				_itemDeselected += value;
			}
			remove { _itemDeselected -= value; }
		}

        /// <summary>
        /// Sets the inventory item for the slot.
        /// </summary>
        /// <param name="item">Inventory item the slot should display.</param>
        public void SetInventoryItem(InventoryItem item) {
            InvItem = item;
            UpdateDisplay();
        }
	
        /// <summary>
        /// Updates the display on start.
        /// </summary>
		protected override void Start() {
            base.Start();
            UpdateDisplay();
		}

        /// <summary>
        /// Initialises references if necessary, and updates the displayed icon and count.
        /// </summary>
        public void UpdateDisplay() {
            InitialiseRefs();
            icon.sprite = InvItem.ItemReference.Icon;
            count.text = InvItem.Count.ToString();
        }

        /// <summary>
        /// Initialises the references to the item icon and the count text field.
        /// </summary>
        private void InitialiseRefs() {
            if (icon == null) {
                icon = transform.Find("Icon").GetComponent<Image>();
            }

            if (count == null) {
                count = GetComponentInChildren<Text>();
            }
        }

        /// <summary>
        /// Calls the ItemSelected event.
        /// </summary>
        /// <param name="eventData"></param>
        public override void OnSelect(BaseEventData eventData)
		{
			base.OnSelect(eventData);
			if (_itemSelected != null) _itemSelected(this, EventArgs.Empty);
		}

        /// <summary>
        /// Calls the ItemDeselected event.
        /// </summary>
        /// <param name="eventData"></param>
		public override void OnDeselect(BaseEventData eventData)
		{
			base.OnDeselect(eventData);
            if (_itemDeselected != null) _itemDeselected(this, EventArgs.Empty);
		}
	}
}