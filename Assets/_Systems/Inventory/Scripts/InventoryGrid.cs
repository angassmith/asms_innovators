﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RevolitionGames.Inventory
{
    /// <summary>
    /// Controls the displayed inventory items in the inventory menu.
    /// Used on a game object with a Grid Layout Group component.
    /// </summary>
    public class InventoryGrid : MonoBehaviour
	{
        /// <summary>
        /// The prefab for an inventory slot.
        /// </summary>
        public GameObject InventorySlotPrefab;

        /// <summary>
        /// A dictionary of the item slots, using the item codenames as a key.
        /// </summary>
        private Dictionary<string, InventoryItemSlot> itemSlots = new Dictionary<string, InventoryItemSlot>();

        /// <summary>
        /// The player inventory to display items for.
        /// </summary>
        private PlayerInventory playerInventory;

        /// <summary>
        /// Adds the items already in the player inventory to the grid.
        /// Registers to the player inventory events so it can respond to them.
        /// </summary>
        void Awake() {
            playerInventory = FindObjectOfType<PlayerInventory>();
            if (playerInventory != null) {
                AddItems(playerInventory.AllItems);

                playerInventory.ItemAdded += AddItem;
                playerInventory.ItemUpdated += UpdateItem;
                playerInventory.ItemRemoved += RemoveItem;
            } else {
                Debug.LogError("No player inventory found in scene.");
            }
        }

        /// <summary>
        /// Adds an inventory item to the grid.
        /// </summary>
        /// <param name="invItem">The item to add.</param>
        public void AddItem(InventoryItem invItem, EventArgs e) {
            // Create a new item slot
            GameObject itemSlot = Instantiate(InventorySlotPrefab, this.transform) as GameObject;
            InventoryItemSlot invItemSlot = itemSlot.GetComponent<InventoryItemSlot>();

            // Set the inventory item for the slot
            if (invItemSlot != null) {
                invItemSlot.SetInventoryItem(invItem);
                itemSlots.Add(invItem.ItemReference.CodeName, invItemSlot);
            }
            else {
                Debug.LogError("No InventoryItemSlot found on " + itemSlot.GetDebugName());
            }
        }

        /// <summary>
        /// Updates an existing inventory item in the grid.
        /// </summary>
        /// <param name="invItem">The item to update.</param>
        public void UpdateItem(InventoryItem invItem, EventArgs e) {
            InventoryItemSlot invItemSlot = null;

            // Find and update the item slot display
            itemSlots.TryGetValue(invItem.ItemReference.CodeName, out invItemSlot);
            if (invItemSlot != null) {
                invItemSlot.UpdateDisplay();
            }

            else {
                Debug.LogWarning("Attempted to update display for item with codename " + invItem.ItemReference.CodeName
                    + ". No item with that codename exists in the inventory grid.");
            }
        }

        /// <summary>
        /// Removes an inventory item from the grid.
        /// </summary>
        /// <param name="invItem">The item to remove.</param>
        public void RemoveItem(InventoryItem invItem, EventArgs e) {

            InventoryItemSlot invItemSlot = null;

            // Find and remove the item slot display
            itemSlots.TryGetValue(invItem.ItemReference.CodeName, out invItemSlot);

            if (invItemSlot != null) {
                Destroy(invItemSlot.gameObject);
                itemSlots.Remove(invItem.ItemReference.CodeName);
            } else {
                Debug.LogWarning("Attempted to remove item with codename " + invItem.ItemReference.CodeName
                    + " from inventory grid. No item with that codename exists in the inventory grid.");
            }
        }

        /// <summary>
        /// Adds the list of items to the inventory grid.
        /// </summary>
        /// <param name="itemsToAdd">The list of items to add.</param>
        private void AddItems(IEnumerable<InventoryItem> itemsToAdd) {
            foreach (InventoryItem item in itemsToAdd) {
                AddItem(item, EventArgs.Empty);
            }
        }
    }
}

