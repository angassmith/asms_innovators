﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

namespace RevolitionGames.Inventory {

    /// <summary>
    /// Contains the inventory items that the player has, and how many of each item.
    /// </summary>
    public class PlayerInventory : MonoBehaviour {

        /// <summary>
        /// A dictionary of the player's inventory items, using the item codenames as a key.
        /// </summary>
        private Dictionary<string, InventoryItem> items = new Dictionary<string, InventoryItem>();

        private InvDatabase _itemDatabase;
        /// <summary>
        /// The database of item definitions.
        /// </summary>
        public InvDatabase ItemDatabase {
            get {
                if (_itemDatabase == null) {
                    InitialiseDatabase();
                }
                return _itemDatabase;
            }
        }

		/// <summary>
        /// Returns a collection of all items in the inventory.
        /// </summary>
        /// <returns>An object that enumerates all items in the inventory.</returns>
        public IEnumerable<InventoryItem> AllItems { get { return items.Values; } }

        /* Note: The following fields are intended to be set in the Unity inspector */
        #region Inspector variables
        /// <summary>The name of the file under Resources which contains the item def schema.</summary>
        [SerializeField]
        private string itemDefsSchemaFile = "ItemDefsSchema";

        /// <summary>The name of the file under Resources which contains the item def xml.</summary>
        [SerializeField]
        private string itemDefsXMLFile = "ItemDefinitions";

        /// <summary>The name of the folder under Resources which contains the item sprites.</summary>
        [SerializeField]
        private string itemSpritesDir = "sprites/Items/";
        #endregion

        #region Events
        private event EventHandler<InventoryItem, EventArgs> _itemAdded;
        /// <summary>Fired when an item is added. Duplicate subscriptions will be ignored.</summary>
        public event EventHandler<InventoryItem, EventArgs> ItemAdded {
            add {
                _itemAdded -= value;
                _itemAdded += value;
            }
            remove { _itemAdded -= value; }
        }

        private event EventHandler<InventoryItem, EventArgs> _itemUpdated;
        /// <summary>Fired when an item is updated. Duplicate subscriptions will be ignored.</summary>
        public event EventHandler<InventoryItem, EventArgs> ItemUpdated {
            add {
                _itemUpdated -= value;
                _itemUpdated += value;
            }
            remove { _itemUpdated -= value; }
        }

        private event EventHandler<InventoryItem, EventArgs> _itemRemoved;
        /// <summary>Fired when an item is removed. Duplicate subscriptions will be ignored.</summary>
        public event EventHandler<InventoryItem, EventArgs> ItemRemoved {
            add {
                _itemRemoved -= value;
                _itemRemoved += value;
            }
            remove { _itemRemoved -= value; }
        }
        #endregion

        /// <summary>
        /// Initialises the item definition database, if necessary.
        /// </summary>
        void Awake() {
            if (_itemDatabase == null) {
                InitialiseDatabase();
            }
        }

        /// <summary>
        /// Initialises the inventory database, if necessary.
        /// </summary>
        private void InitialiseDatabase() {
            if (itemDefsSchemaFile == null) throw new InvalidOperationException("itemDefsSchemaFile is null");
            if (itemDefsXMLFile == null) throw new InvalidOperationException("itemDefsXMLFile is null");
            if (itemSpritesDir == null) throw new InvalidOperationException("itemSpritesDir is null");

            _itemDatabase = new InvDatabase();
            _itemDatabase.InitialiseDatabase(itemDefsSchemaFile, itemDefsXMLFile, itemSpritesDir);
        }

        /// <summary>
        /// Adds the item with the specified codename to the player's inventory.
        /// Increase the count of the item if it is already present.
        /// </summary>
        /// <param name="codeName">Unique codename for the item.</param>
        public void AddItem(string codeName) {
            AddItem(codeName, 1);
        }

        /// <summary>
        /// Adds a number of the item with the specified codename to the player's inventory.
        /// Increase the count of the item if it is already present.
        /// </summary>
        /// <param name="codeName">Unique codename for the item.</param>
        /// <param name="count">The count of the item.</param>
        public void AddItem(string codeName, int count)
		{
			ItemDef itemDef;
			if (ItemDatabase.TryGetByCodeName(codeName, out itemDef)) {
                AddItem(itemDef, count);
			} else {
                Debug.LogError("Attempted to add item with codename " + itemDef.CodeName + " to player's inventory. No item with that codename exists in the database.");
            }
        }

        /// <summary>
        /// Adds a number of the specified item definition to the player's inventory.
        /// Increase the count of the item if it is already present.
        /// Note: Assumes itemDef is not null.
        /// </summary>
        /// <param name="itemDef">Definition for the item.</param>
        /// <param name="count">The count of the item.</param>
        public void AddItem(ItemDef itemDef, int count) {

            InventoryItem invItem;

            // Check if the player's inventory already contains the item
            // If so, increase the item count
            if (items.TryGetValue(itemDef.CodeName, out invItem))
			{
                invItem.IncreaseCount(count);
                if (_itemUpdated != null) _itemUpdated(invItem, EventArgs.Empty);
            }
            else // Otherwise, create a new item
			{
				invItem = new InventoryItem(itemDef, count);
                items.Add(itemDef.CodeName, invItem);

                if (_itemAdded != null) _itemAdded(invItem, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Removes the item with the specified codename from the player's inventory.
        /// Reduces the count of the item by one if multiple are present.
        /// </summary>
        /// <param name="codeName">Unique codename for the item.</param>
        public void RemoveItem(string codeName) {
            RemoveItem(codeName, 1);
        }

        /// <summary>
        /// Removes a number of the item with the specified codename from the player's inventory.
        /// </summary>
        /// <param name="codeName">Unique codename for the item.</param>
        /// <param name="count">The count to remove.</param>
        public void RemoveItem(string codeName, int count)
		{
            InventoryItem invItem = null;

            // Check if the player's inventory contains the item
            // If so, decrease the item count
			if (items.TryGetValue(codeName, out invItem))
			{
				invItem.DecreaseCount(count);

                // Remove the item if the count reaches zero
                if (invItem.Count <= 0) {
                    items.Remove(codeName);
                    if (_itemRemoved != null) _itemRemoved(invItem, EventArgs.Empty);
                }
                else {
                    if (_itemUpdated != null) _itemUpdated(invItem, EventArgs.Empty);
                }
            }
            else
			{
                Debug.LogError("Attempted to remove item with codename " + codeName + " from player's inventory. No item with that codename exists in the inventory.");
            }
        }

        /// <summary>
        /// Returns the InventoryItem for the specified item definition,
        /// if one exists in the inventory.
        /// </summary>
        /// <param name="itemDef">The item definition.</param>
        /// <returns>The inventory item if it is present, null otherwise.</returns>
        public InventoryItem GetItem(ItemDef itemDef) {
            InventoryItem invItem;
            items.TryGetValue(itemDef.CodeName, out invItem);
            return invItem;
        }

		/// <summary>
        /// If an InventoryItem exists for the specified item definition,
        /// returns true and passes it through an out parameter. Otherwise,
		/// returns false.
        /// </summary>
		public bool TryGetItem(ItemDef itemDef, out InventoryItem item)
		{
			return items.TryGetValue(itemDef.CodeName, out item);
		}

        /// <summary>
        /// Removes all the items from the player's inventory.
        /// </summary>
        public void ClearAllItems()
		{
			//The ItemRemoved needs to be fired, but it should always be
			//fired after the relevant item has been removed, not before.
			//We could swap out the items dictionary for a new dictionary,
			//but that would screw up any enumerators created by AllItems
			//that are still being stored.
			//All items could therefore removed one at a time (which probably
			//isn't too expensive performance wise, though it isn't ideal)
			//However, enumerating and editing a dictionary is difficult.
			//I've therefore just left this as-is

            // Send all the item removed signals first
            foreach (InventoryItem invItem in items.Values) {
                if (_itemRemoved != null) _itemRemoved(invItem, EventArgs.Empty);
            }

            // Clear the items dictionary
            items.Clear();
        }

        /// <summary>
        /// Adds the items required for the first quest to the 
        /// inventory, for testing purposes.
        /// </summary>
        public void AddFirstQuestItems() {
            AddItem("colouredPencil", 3);
            AddItem("ruler", 1);
            AddItem("paper", 1);
            AddItem("scissors", 1);
        }

        /// <summary>
        /// Adds the items required for the second quest to the 
        /// inventory, for testing purposes.
        /// </summary>
        public void AddSecondQuestItems() {
            AddItem("colouredMarker", 3);
            AddItem("cardboard", 1);
            AddItem("corkBoard", 1);
            AddItem("scissors", 1);
            AddItem("tape", 1);
        }

        /// <summary>
        /// Adds the items required for the third quest to the 
        /// inventory, for testing purposes.
        /// </summary>
        public void AddThirdQuestItems() {
            AddItem("cone", 8);
            AddItem("football", 1);
            AddItem("whistle", 1);
            AddItem("waterBottle", 1);
            AddItem("bibs", 15);
        }

        /// <summary>
        /// Adds some items to the inventory for testing purposes.
        /// </summary>
        private void AddItemsTest() {
            AddItem("bio_text", 1);
            AddItem("math_text", 2);
            AddItem("eng_text", 1);
            AddItem("pencil", 11);
            AddItem("rare_pepe", 2);

            /*foreach (var pair in items) {
                Debug.Log(pair.Value.ToString());
            }*/
        }
    }
}


