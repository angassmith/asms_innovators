﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Inventory;

namespace RevolitionGames.SceneComponents
{
	public class ItemEventArgs : EventArgs
	{
		public ItemDef ItemDef { get; private set; }

		public ItemEventArgs(ItemDef itemDef)
		{
			ThrowIfArgNull(itemDef, "itemDef");
			this.ItemDef = itemDef;
		}

		protected static T ThrowIfArgNull<T>(T arg, string argName)
		{
			if (arg == null) throw new ArgumentNullException(argName);
			else return arg;
		}
	};

	public class CollectableItemEventArgs : ItemEventArgs
	{
		public CollectableItem CollectableItem { get; private set; }

		public CollectableItemEventArgs(CollectableItem collectableItem)
			: base(ThrowIfArgNull(collectableItem, "collectableItem").ItemDef)
		{
			ThrowIfArgNull(collectableItem, "collectableItem");
			this.CollectableItem = collectableItem;
		}
	}
}

//*/