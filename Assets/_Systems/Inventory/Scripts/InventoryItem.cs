﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RevolitionGames.Inventory {

    /// <summary>
    /// Represents an item in the player's inventory.
    /// </summary>
    public class InventoryItem {

        /// <summary>
        /// A reference to the properties of the item.
        /// </summary>
        public ItemDef ItemReference { get; private set; }

        /// <summary>
        /// How many of the item is in the player's inventory.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Creates an inventory item for the supplied item, with a count of one.
        /// </summary>
        /// <param name="itemDef">The item definition.</param>
        public InventoryItem(ItemDef itemDef) : this(itemDef, 1) {
        }

        /// <summary>
        /// Creates an inventory item for the supplied item, with the specified count.
        /// </summary>
        /// <param name="itemDef">The item definition.</param>
        /// <param name="num">The number of identical items.</param>
        public InventoryItem(ItemDef itemDef, int num) {
            ItemReference = itemDef;

            SetCount(num);
        }

        /// <summary>
        /// Returns a string representation of the inventory item, suitable for logging.
        /// </summary>
        /// <returns>A string representation of the inventory item.</returns>
        public override string ToString() {
            return string.Concat("InventoryItem: { " + ItemReference.ToString() 
                + ", Count: " + Count + " }"
            );
        }

        /// <summary>
        /// Increases the count of the inventory item by one.
        /// </summary>
        public void IncreaseCount() {
            Count++;
        }

        /// <summary>
        /// Increases the count of the inventory item by the specified number.
        /// </summary>
        public void IncreaseCount(int num) {
            Count += num;
        }

        /// <summary>
        /// Decreases the count of the inventory item by one.
        /// </summary>
        public void DecreaseCount() {
            SetCount(Count - 1);
        }

        /// <summary>
        /// Decreases the count of the inventory item by the specified number.
        /// </summary>
        public void DecreaseCount(int num) {
            SetCount(Count - num);
        }

        /// <summary>
        /// Sets the count of the item to the specified number.
        /// Count is set to zero if a number less than zero is used.
        /// </summary>
        /// <param name="num">The number of identical items to set.</param>
        private void SetCount(int num) {
            if (num >= 0) {
                Count = num;
            } else {
                Count = 0;
                Debug.LogWarning("Attempted to set count of " + ItemReference.CodeName + " to " + num);
            }
        }
    }
}
