﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace RevolitionGames.Inventory
{
    /// <summary>
    /// A panel for displaying the details of an inventory item.
    /// </summary>
	public class InventoryDetailsPanel : UIBehaviour
	{
        /// <summary>
        /// The item that is selected, i.e. the details are being displayed.
        /// </summary>
        public InventoryItemSlot SelectedItem { get; private set; }

        /* Note: The following fields are intended to be set in the Unity inspector */

        #region Inspector variables
        /// <summary>The text where the item name is displayed.</summary>
        [SerializeField]
        private Text nameTextbox = null;

        /// <summary>The image where the item icon is displayed.</summary>
        [SerializeField]
        private Image itemIconImage = null;

        /// <summary>The text where the item description is displayed.</summary>
        [SerializeField]
        private Text descriptionTextbox = null;

        /// <summary>The default title to to use when no item is selected.</summary>
        [SerializeField]
        private string defaultDetailsTitle = "Inventory";

        /// <summary>The default description to to use when no item is selected.</summary>
        [SerializeField]
        private string defaultDetailsDescription = "This is where you store items";

        /// <summary>The default image to to use when no item is selected.</summary>
        [SerializeField]
        private Sprite defaultIconImage = null;

        #endregion

        /// <summary>
        /// Registers to the events for items being selected and deselected.
        /// </summary>
        protected override void Awake() {
			base.Awake();
            InventoryItemSlot.ItemSelected += OnItemSelected;
            InventoryItemSlot.ItemDeselected += OnItemDeselected;
		}

        /// <summary>
        /// Displays the item details when an item is selected.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="e"></param>
		private void OnItemSelected(InventoryItemSlot item, EventArgs e)
		{
			if (ReferenceEquals(item, null)) return;
			if (ReferenceEquals(item, SelectedItem)) return;
			else SetItem(item);
		}

        /// <summary>
        /// Displays the default details when an item is deselected.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="e"></param>
		private void OnItemDeselected(InventoryItemSlot item, EventArgs e)
		{
			if (ReferenceEquals(item, null)) return;
			if (ReferenceEquals(item, SelectedItem))
			{
                SetDefault();
			}
		}

        /// <summary>
        /// Sets the item being displayed, and shows the details for the item.
        /// </summary>
        /// <param name="item">The item to display.</param>
		private void SetItem(InventoryItemSlot item)
		{
			SelectedItem = item;
            nameTextbox.text = item.InvItem.ItemReference.DisplayName;
            itemIconImage.sprite = item.InvItem.ItemReference.Icon;
            descriptionTextbox.text = item.InvItem.ItemReference.Description;
		}

        /// <summary>
        /// Sets the displayed details to their default values.
        /// </summary>
		private void SetDefault()
		{
			SelectedItem = null;
			nameTextbox.text = defaultDetailsTitle;
            itemIconImage.sprite = defaultIconImage;
			descriptionTextbox.text = defaultDetailsDescription;
		}
	}
}
