﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using RevolitionGames.Quests;

namespace RevolitionGames.Saving
{
	public class SaveDecoder
	{
		//	private static readonly XmlSchema FallbackSchema = XmlSchema.Read(
		//		new StringReader(
		//			@"<?xml version=""1.0"" encoding=""utf-8""?>"
		//			+ @"<xs:schema"
		//			+ @"targetNamespace=""http://www.revolitiongames.com/experience/quests/saving"
		//			+ @"elementFormDefault=""qualified"
		//			+ @"xmlns=""http://www.revolitiongames.com/experience/quests/saving"
		//			+ @"xmlns:quests=""http://www.revolitiongames.com/experience/quests"
		//			+ @"xmlns:xs=""http://www.w3.org/2001/XMLSchema"">"
		//			+ @"TODO"
		//		)
		//	);

		public Game Game { get; private set; }

		public SaveDecoder(Game game)
		{
			ThrowIfArgNull(game, "game");

			this.Game = game;
		}

		public PlayModeStage DecodeSave(FileInfo save)
		{

		}

		public PlayModeStage DecodePlayModeStage(XElement stage)
		{
			s

			return new PlayModeStage(
				game: Game,
				storySlot: DecodeStorySlot(
			);
		}

		public StorySlot DecodeStorySlot(XElement storySlot)
		{

		}

		#region Utilities

		public T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}
}

//*/