﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Saving.Types
{
	public class DummyStoryProgressSaver : IStoryProgressSaver
	{
		public MainStoryProgressSaveData LoadMainProgress()
		{
			return MainStoryProgressSaveData.GetLoadFailFallback();
		}

		public QuestProgressSaveData LoadQuestProgress(string codeName, QuestSaveID saveID)
		{
			return QuestProgressSaveData.GetLoadFailFallback(codeName: codeName);
		}

		public void SaveMainProgress(MainStoryProgressSaveData mainProgress)
		{
			return;
		}

		public void SaveQuestProgress(QuestSaveID saveID, QuestProgressSaveData questProgress)
		{
			return;
		}
	}
}

//*/