﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Saving.Types
{
	public interface IStoryProgressSaver
	{
		MainStoryProgressSaveData LoadMainProgress();
		QuestProgressSaveData LoadQuestProgress(string codeName, QuestSaveID saveID);
		void SaveMainProgress(MainStoryProgressSaveData mainProgress);
		void SaveQuestProgress(QuestSaveID saveID, QuestProgressSaveData questProgress);
	}
}

//*/