﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using RevolitionGames.Quests;
using RevolitionGames.Utilities.Alex;
using XmlReader = System.Xml.XmlReader;

namespace RevolitionGames.Saving.Types
{
	public class XmlStoryProgressSaver : IStoryProgressSaver
	{
		private static readonly XName XStoryProgress = "{" + XQuestSavingNamespaceUri + "}storyProgress";
		private static readonly XName XCurrentEpisode = "{" + XQuestSavingNamespaceUri + "}currentEpisode";
		private static readonly XName XFocusQuest = "{" + XQuestSavingNamespaceUri + "}focusQuest";
		private static readonly XName XCompletedEpisodesList = "{" + XQuestSavingNamespaceUri + "}focusQuest";
		private static readonly XName XCompletedEpisode = "{" + XQuestSavingNamespaceUri + "}focusQuest";
		private static readonly XName XQuestSaveIDList = "{" + XQuestSavingNamespaceUri + "}questSaveIDs";
		private static readonly XName XQuestSaveIDDef = "{" + XQuestSavingNamespaceUri + "}quest";
		private static readonly XName XCodeName = "{" + XQuestsNamespaceUri + "}codeName";
		private static readonly XName XQuestSaveID = "{" + XQuestSavingNamespaceUri + "}saveID";
		private static readonly XName XQuestProgress = "{" + XQuestSavingNamespaceUri + "}questProgress";
		private static readonly XName XTaskProgressesList = "{" + XQuestSavingNamespaceUri + "}taskProgresses";
		private static readonly XName XTaskProgress = "{" + XQuestSavingNamespaceUri + "}taskProgress";
		private static readonly XName XStoryPartStatus = "{" + XQuestSavingNamespaceUri + "}status";

		private const string XQuestsNamespaceUri = "http://www.revolitiongames.com/experience/quests";
		private const string XQuestSavingNamespaceUri = "http://www.revolitiongames.com/experience/quests/saving";
		private static readonly XNamespace XQuestsNamespace = XQuestSavingNamespaceUri;
		private static readonly XNamespace XQuestSavingNamespace = XQuestSavingNamespaceUri;

		private readonly XmlSchema _mainStoryProgressSchema;
		private readonly XmlSchema _questProgressSchema;

		private readonly DirectoryInfo _storySaveFolder;
		private readonly string _mainProgressPath;
		private readonly string _questProgressesFolder;

		public XmlStoryProgressSaver(XmlSchema mainStoryProgressSchema, XmlSchema questProgressSchema, DirectoryInfo storySaveFolder)
		{
			if (questProgressSchema == null) throw new ArgumentNullException("questProgressSchema");
			if (storySaveFolder == null) throw new ArgumentNullException("storySaveFolder");

			this._mainStoryProgressSchema = mainStoryProgressSchema;
			this._questProgressSchema = questProgressSchema;
			this._storySaveFolder = storySaveFolder;

			this._questProgressesFolder = Path.Combine(_storySaveFolder.FullName, "QuestProgresses");
			this._mainProgressPath = Path.Combine(_storySaveFolder.FullName, "MainStoryProgress.xml");
		}



		public MainStoryProgressSaveData LoadMainProgress()
		{
			return LoadMainProgress(this._mainStoryProgressSchema, this._mainProgressPath);
		}

		public QuestProgressSaveData LoadQuestProgress(string codeName, QuestSaveID saveID)
		{
			return LoadQuestProgress(codeName, this._questProgressSchema, GetQuestProgressPath(saveID));
		}

		public void SaveMainProgress(MainStoryProgressSaveData mainProgress)
		{
			SaveMainProgress(mainProgress, this._mainProgressPath);
		}

		public void SaveQuestProgress(QuestSaveID saveID, QuestProgressSaveData questProgress)
		{
			SaveQuestProgress(questProgress, GetQuestProgressPath(saveID));
		}

		/// <summary>
		/// Constructs the path of the file that the quest-progress with the specified
		/// save-ID is stored in.
		/// </summary>
		private string GetQuestProgressPath(QuestSaveID saveID)
		{
			return Path.Combine(
				this._questProgressesFolder,
				"quest-" + "{" + saveID.ID.ToString() + "}" + ".xml"
			);
		}



		#region Main progress loading

		private static MainStoryProgressSaveData LoadMainProgress(XmlSchema mainStoryProgressSchema, string path)
		{
			if (!File.Exists(path)) {
				return MainStoryProgressSaveData.GetLoadFailFallback();
			}

			var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

			using (stream) {
				return LoadMainProgress(mainStoryProgressSchema, stream);
			}
		}

		private static MainStoryProgressSaveData LoadMainProgress(XmlSchema mainStoryProgressSchema, FileStream stream)
		{
			ThrowIfArgNull(mainStoryProgressSchema, "mainStoryProgressSchema");
			ThrowIfArgNull(stream, "stream");

			var readerSettings = new System.Xml.XmlReaderSettings();
			readerSettings.Schemas.Add(mainStoryProgressSchema);
			readerSettings.ValidationType = System.Xml.ValidationType.Schema;
			readerSettings.ValidationFlags |= XmlSchemaValidationFlags.ProcessIdentityConstraints | XmlSchemaValidationFlags.ProcessSchemaLocation | XmlSchemaValidationFlags.ReportValidationWarnings;
			readerSettings.ValidationEventHandler += XmlUtils.XmlValidationExceptionUnityLogger;
			// ^ AFAIK The validation-event-handler supplied when constructing the schema is used
			//when validating the schema itself - this one is used when using the schema to validate something else

			var xStoryProgress = XDocument.Load(
				XmlReader.Create(stream, readerSettings)
			);

			var xStoryProgressRoot = xStoryProgress.Element(XStoryProgress);

			var currentEpisode = xStoryProgressRoot.Element(XCurrentEpisode).Value;
				
			var focusQuest = xStoryProgressRoot.Element(XFocusQuest).ValueOrNil();

			var completedEpisodes = (
				xStoryProgressRoot.Element(XCompletedEpisodesList)
				.Elements(XCompletedEpisode)
				.Select(x => x.Attribute(XCodeName).Value)
			);

			var questSaveIDs = (
				xStoryProgressRoot
				.Element(XQuestSaveIDList)
				.Elements(name: XQuestSaveIDDef)
				.Select(x =>
					new KeyValuePair<string, QuestSaveID>(
						x.Attribute(name: XCodeName).Value,
						new QuestSaveID(new Guid(x.Attribute(name: XQuestSaveID).Value))
					)
				)
			);

			return new MainStoryProgressSaveData(
				currentEpisode: currentEpisode,
				focusQuest: focusQuest,
				questSaveIDs: questSaveIDs,
				completedEpisodes: completedEpisodes
			);
		}

		#endregion


		#region Main progress saving

		private static void SaveMainProgress(MainStoryProgressSaveData mainProgress, string path)
		{
			ThrowIfArgNull(mainProgress, "mainProgress");
			ThrowIfArgNull(path, "path");

			//Useful resource about using namespaces when creating xml documents with Linq to Xml:
			//https://docs.microsoft.com/en-us/dotnet/articles/csharp/programming-guide/concepts/linq/how-to-create-a-document-with-namespaces-linq-to-xml

			var doc = new XDocument(
				new XDeclaration("1.0", "utf-8", "no"),
				new XElement(
					XStoryProgress,
					new XElement(
						XCurrentEpisode,
						mainProgress.CurrentEpisode
					),
					new XElement(
						XFocusQuest,
						(object)mainProgress.FocusQuest ?? XmlUtils.Nil
					),
					new XElement(
						XCompletedEpisodesList,
						from completedEpisodeCodeName in mainProgress.CompletedEpisodes
						select new XElement(
							XCompletedEpisode,
							new XAttribute(XCodeName, completedEpisodeCodeName)
						)
					),
					new XElement(
						XQuestSaveIDList,
						new XAttribute("xmlns", XQuestSavingNamespace),
						new XAttribute(XNamespace.Xmlns + "xsi", XmlSchema.InstanceNamespace),
						from codeNameAndSaveID in mainProgress.QuestSaveIDs
						select new XElement(
							XQuestSaveIDDef,
							new XAttribute(XCodeName, codeNameAndSaveID.Key),
							new XAttribute(XQuestSaveID, codeNameAndSaveID.Value.ID)
						)
					)
				)
			);

			//TODO: Handle file access exceptions somehow
			doc.Save(
				new StreamWriter(File.Open(path, FileMode.Create, FileAccess.Write, FileShare.None))
				// ^ Creates any missing parent folders, and creates or overwrites the file
			);
		}

		#endregion


		#region Quest progress loading

		private static QuestProgressSaveData LoadQuestProgress(string codeName, XmlSchema questProgressSchema, string path)
		{
			if (!File.Exists(path)) {
				return QuestProgressSaveData.GetLoadFailFallback(codeName);
			}

			var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

			using (stream) {
				return LoadQuestProgress(codeName, questProgressSchema, stream);
			}
		}

		private static QuestProgressSaveData LoadQuestProgress(string codeName, XmlSchema questProgressSchema, FileStream stream)
		{
			ThrowIfArgNull(codeName, "codeName");
			ThrowIfArgNull(questProgressSchema, "questProgressSchema");
			ThrowIfArgNull(stream, "stream");

			var readerSettings = new System.Xml.XmlReaderSettings();
			readerSettings.Schemas.Add(questProgressSchema);
			readerSettings.ValidationType = System.Xml.ValidationType.Schema;
			readerSettings.ValidationFlags |= XmlSchemaValidationFlags.ProcessIdentityConstraints | XmlSchemaValidationFlags.ProcessSchemaLocation | XmlSchemaValidationFlags.ReportValidationWarnings;
			readerSettings.ValidationEventHandler += XmlUtils.XmlValidationExceptionUnityLogger;
			// ^ AFAIK The validation-event-handler supplied when constructing the schema is used
			//when validating the schema itself - this is used when using the schema to validate something else

			var xQuestProgressDoc = XDocument.Load(
				XmlReader.Create(stream, readerSettings)
			);

			var xQuestProgressRoot = xQuestProgressDoc.Element(XQuestProgress);

			var taskProgresses = (
				xQuestProgressRoot
				.Element(XTaskProgressesList)
				.Elements(XTaskProgress)
				.Select(
					xTaskProgress => new KeyValuePair<string, TaskProgress>(
						xTaskProgress.Attribute(XCodeName).Value,
						new TaskProgress(
							status: ParseStoryPartStatus(xTaskProgress.Attribute(XStoryPartStatus).Value)
						)
					)
				)
			);

			return new QuestProgressSaveData(
				codeName: codeName,
				status: ParseStoryPartStatus(xQuestProgressRoot.Element(XStoryPartStatus).Value), 
				taskProgresses: taskProgresses.ToDictionary(kvp => kvp.Key, kvp => kvp.Value)
			);
		}

		#endregion


		#region Quest progress saving

		private void SaveQuestProgress(QuestProgressSaveData questProgress, string path)
		{
			if (questProgress.Status == StoryPartStatus.Unused) return; //Nothing to save

			var doc = new XDocument(
				new XDeclaration("1.0", "utf-8", "no"),
				new XElement(
					XQuestProgress,
					new XElement(
						XStoryPartStatus,
						FormatStoryPartStatus(questProgress.Status)
					),
					new XElement(
						XTaskProgressesList,
						from taskProgress in questProgress.TaskProgresses
						select new XElement(
							XTaskProgress,
							new XAttribute(XCodeName, taskProgress.Key),
							new XAttribute(XStoryPartStatus, taskProgress.Value)
						)
					)
				)
			);

			doc.Save(
				new StreamWriter(File.Open(path, FileMode.Create, FileAccess.Write, FileShare.None)) //Creates any missing parent folders, and creates or overwrites the file
			);
		}

		#endregion


		#region Type formatting utils

		private static string FormatStoryPartStatus(StoryPartStatus status)
		{
			switch (status)
			{
				case StoryPartStatus.Unused: return "unused";
				case StoryPartStatus.Active: return "active";
				case StoryPartStatus.Completed: return "completed";
				case StoryPartStatus.Destroyed: return "destroyed";
				default: throw new ArgumentException("Invalid StoryPartStatus '" + status + "'.", "status");
			}
		}

		private static StoryPartStatus ParseStoryPartStatus(string status)
		{
			return (StoryPartStatus)Enum.Parse(typeof(StoryPartStatus), status, ignoreCase: true);
		}

		#endregion


		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}
}

//*/