﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Saving.Types
{
	public sealed class MainStoryProgressSaveData
	{
		/// <summary>Creates a <see cref="MainStoryProgressSaveData"/> for use when one is not found when loading</summary>
		public static MainStoryProgressSaveData GetLoadFailFallback()
		{
			return new MainStoryProgressSaveData(
				currentEpisode: "--NOTHING-SAVED--",
				focusQuest: null,
				questSaveIDs: Enumerable.Empty<KeyValuePair<string, QuestSaveID>>(),
				completedEpisodes: Enumerable.Empty<string>()
			);
		}

		public string CurrentEpisode { get; private set; }
		public string FocusQuest { get; private set; }
		private readonly Dictionary<string, QuestSaveID> _questSaveIDs;
		public ReadOnlyDictionary<string, QuestSaveID> QuestSaveIDs { get; private set; }
		public ReadOnlyCollection<string> CompletedEpisodes { get; private set; }

		internal MainStoryProgressSaveData(string currentEpisode, string focusQuest, IEnumerable<KeyValuePair<string, QuestSaveID>> questSaveIDs, IEnumerable<string> completedEpisodes)
		{
			if (currentEpisode == null) throw new ArgumentNullException("currentEpisode");
			//(focusQuest can be null)
			if (questSaveIDs == null) throw new ArgumentNullException("questSaveIds");
			if (completedEpisodes == null) throw new ArgumentNullException("completedEpisodes");

			this.CurrentEpisode = currentEpisode;
			this.FocusQuest = focusQuest;
			this._questSaveIDs = questSaveIDs.ToDictionary(keySelector: x => x.Key, elementSelector: x => x.Value);
			this.QuestSaveIDs = new ReadOnlyDictionary<string, QuestSaveID>(this._questSaveIDs);
			this.CompletedEpisodes = completedEpisodes.ToList().AsReadOnly();
		}

		/// <summary>
		/// If a quest save-ID exists for the specified code name, returns it,
		/// otherwise generates and registers a new one.
		/// </summary>
		/// <param name="questCodeName"></param>
		/// <returns></returns>
		internal QuestSaveID GetQuestSaveID(string questCodeName)
		{
			QuestSaveID saveID;
			if (_questSaveIDs.TryGetValue(questCodeName, out saveID)) {
				return saveID;
			} else {
				saveID = new QuestSaveID(Guid.NewGuid());
				_questSaveIDs.Add(questCodeName, saveID);
				return saveID;
			}
		}
	}
}

//*/