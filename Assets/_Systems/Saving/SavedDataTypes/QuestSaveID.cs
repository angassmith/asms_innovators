﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Saving.Types
{
	public struct QuestSaveID
	{
		public readonly Guid ID;

		public QuestSaveID(Guid ID)
		{
			this.ID = ID;
		}
	}
}

//*/