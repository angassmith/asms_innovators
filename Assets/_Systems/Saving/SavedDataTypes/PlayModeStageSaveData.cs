﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.NPCs;
using RevolitionGames.Quests;

namespace RevolitionGames.Saving.Types
{
	public class PlayModeStageSaveData
	{
		public PlayModeSceneSaveData CurrentScene { get; private set; }
		public ReadOnlyDictionary<string, float> NPCPlayerLikeabilities { get; private set; }
		public StoryProgressSaveData StoryProgress { get; private set; }

		public PlayModeStageSaveData(PlayModeSceneSaveData currentScene, IEnumerable<KeyValuePair<string, float>> npcPlayerLikeabilities, StoryProgressSaveData storyProgress)
		{
			ThrowIfArgNull(currentScene, "currentScene");
			ThrowIfArgNull(npcPlayerLikeabilities, "npcPlayerLikeabilities");
			ThrowIfArgNull(storyProgress, "storyProgress");

			this.CurrentScene = currentScene;
			this.NPCPlayerLikeabilities = new ReadOnlyDictionary<string, float>(
				npcPlayerLikeabilities.ToDictionary(keySelector: x => x.Key, elementSelector: x => x.Value)
			);
			this.StoryProgress = storyProgress;
		}

		public static PlayModeStageSaveData Snapshot(PlayModeStageSaveData prev, PlayModeStage stage)
		{
			return new PlayModeStageSaveData(
				PlayModeSceneSaveData.Snapshot(stage.CurrentScene),
				stage.NPCHost.AllNPCs.NPCs.ToDictionary(
					keySelector: npc => npc.Name,
					elementSelector: npc => npc.Likeability.PlayerLikeability
				),
				storyProgress: new StoryProgressSaveData(
					saver: prev.StoryProgress.Saver,
					prevStoredProgress: prev.StoryProgress,
					currentProgress: stage.Story.Progress
				)
			);
		}

		/// <summary>
		/// Creates a new <see cref="PlayModeStage"/> and <see cref="PlayModeSceneData"/> from the data
		/// stored in the <see cref="PlayModeSceneSaveData"/>. Does not activate the created objects
		/// (so the returned stage's CurrentScene property will be null, and must be set by the calling code).
		/// </summary>
		public PlayModeStage Create(Game game, IStoryProvider storyProvider, PlayerUI playerUI, out PlayModeSceneData firstScene)
		{
			var npcHost = new NPCHost(
				allNPCsGroup: new NPCGroup(NPCCodeNames.NPCGroups.AllNPCs, propagationStrength: 0.02f),
				npcProvider: new CSharpNPCProvider()
			);

			foreach (NPCData npc in npcHost.AllNPCs.NPCs)
			{
				float savedLikeability;
				if (NPCPlayerLikeabilities.TryGetValue(npc.Name, out savedLikeability))
				{
					npc.Likeability.ChangePlayerLikeability(savedLikeability, "load", propagate: false);
				}
			}

			var playModeStage = new PlayModeStage(
				game: game,
				storySlot: new StorySlot(
					storyProvider: storyProvider,
					saveData: this.StoryProgress
				),
				npcHost: npcHost,
				playerUI: playerUI
			);

			firstScene = this.CurrentScene.CreateSceneData(stage: playModeStage);

			return playModeStage;
		}

		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}
}