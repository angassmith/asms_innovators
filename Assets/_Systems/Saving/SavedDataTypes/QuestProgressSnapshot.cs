﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests;
using RevolitionGames.Quests.Saving;
using RevolitionGames.Utilities.Alex;

namespace RevolitionGames.Saving.Types
{
	public struct QuestProgressSaveData
	{
		public static QuestProgressSaveData Unused(string codeName) {
			return new QuestProgressSaveData(
				codeName,
				StoryPartStatus.Unused,
				Enumerable.Empty<KeyValuePair<string, TaskProgress>>()
			);
		}

		public static QuestProgressSaveData GetLoadFailFallback(string codeName) {
			return Unused(codeName);
		}

		public string CodeName { get; private set; }
		public StoryPartStatus Status { get; private set; }
		public ReadOnlyDictionary<string, TaskProgress> TaskProgresses { get; private set; }

		private int? _hashCode;

		public QuestProgressSaveData(string codeName, StoryPartStatus status, IEnumerable<KeyValuePair<string, TaskProgress>> taskProgresses)
		{
			if (string.IsNullOrEmpty(codeName)) throw new ArgumentException("Cannot be null or empty.", "codeName");
			if (taskProgresses == null) throw new ArgumentNullException("taskProgresses");

			this.CodeName = codeName;
			this.Status = status;
			this.TaskProgresses = new ReadOnlyDictionary<string, TaskProgress>(taskProgresses.ToDictionary(x => x.Key, x => x.Value));

			if (status == StoryPartStatus.Unused && this.TaskProgresses.Count != 0) {
				throw new ArgumentException("status == StoryPartStatus.Unused but taskProgresses is not empty.", "taskProgresses");
			}

			this._hashCode = null;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				if (this._hashCode != null) return this._hashCode.GetValueOrDefault();

				int hash = 17;
				hash = hash * 23 + this.CodeName.GetHashCode();
				hash = hash * 23 + this.Status.GetHashCode();
				foreach (var kvp in this.TaskProgresses) {
					hash = hash * 23 + kvp.Key.GetHashCode();
					hash = hash * 23 + kvp.Value.GetHashCode();
				}
				this._hashCode = hash;
				return hash;
			}
		}
		public static bool Equals(QuestProgressSaveData a, QuestProgressSaveData b)
		{
			if (ReferenceEquals(a, b)) return true;

			if ((a._hashCode != null) && (b._hashCode != null) && (a._hashCode != b._hashCode)) return false;
			if (a.CodeName != b.CodeName) return false;
			if (a.Status != b.Status) return false;
			if (!EnumerableUtils.DictionariesEqual(a.TaskProgresses, b.TaskProgresses)) return false;
			return true;
		}
		public override bool Equals(object obj) {
			return obj is QuestProgressSaveData && Equals(this, (QuestProgressSaveData)obj);
		}
		public static bool operator ==(QuestProgressSaveData a, QuestProgressSaveData b) { return Equals(a, b); }
		public static bool operator !=(QuestProgressSaveData a, QuestProgressSaveData b) { return !(a == b); }
	}
}

//*/