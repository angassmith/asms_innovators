﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using RevolitionGames.Quests;
using RevolitionGames.Quests.Saving;
using RevolitionGames.Utilities.Alex;
using XmlReader = System.Xml.XmlReader;

namespace RevolitionGames.Saving.Types
{
	public class StoryProgressSaveData
	{
		public IStoryProgressSaver Saver { get; private set; }

		//	private bool _isMainProgressSaved; //TODO: Add this if there are performance issues
		private readonly MainStoryProgressSaveData _mainStoryProgress;
		public MainStoryProgressSaveData MainStoryProgress { get { return _mainStoryProgress; } }

		private readonly ReadOnlyDictionary<string, QuestProgressWSavedFlag> _questProgresses;
		public IEnumerable<KeyValuePair<string, QuestProgressSaveData>> QuestProgresses {
			get {
				return _questProgresses.Select(
					kvp => new KeyValuePair<string, QuestProgressSaveData>(kvp.Key, kvp.Value.Progress)
				);
			}
		}

		public StoryProgressSaveData(IStoryProgressSaver saver)
		{
			ThrowIfArgNull(saver, "saver");

			this.Saver = saver;

			this._mainStoryProgress = saver.LoadMainProgress();
			this._questProgresses = new ReadOnlyDictionary<string, QuestProgressWSavedFlag>(
				new Dictionary<string, QuestProgressWSavedFlag>()
			);
		}

		public StoryProgressSaveData(IStoryProgressSaver saver, StoryProgressSaveData prevStoredProgress, StoryProgress currentProgress)
		{
			//	var oldMainProgress = this._mainStoryProgress;
			var newMainProgress = new MainStoryProgressSaveData(
				currentEpisode: currentProgress.CurrentEpisode,
				focusQuest: currentProgress.FocusQuest,
				questSaveIDs: prevStoredProgress.MainStoryProgress.QuestSaveIDs.AsEnumerable(),
				completedEpisodes: currentProgress.CompletedEpisodes
			);

			this._mainStoryProgress = newMainProgress;

			//	bool equal = (
			//		newMainProgress.CurrentEpisode == oldMainProgress.CurrentEpisode
			//		&& newMainProgress.FocusQuest == oldMainProgress.FocusQuest
			//		&& Enumerable.All(
			//			newMainProgress.QuestSaveIDs,
			//			newQSaveID => oldMainProgress.QuestSaveIDs.Contains(newQSaveID)
			//		)
			//	);
			//	
			//	if (!equal) _isMainProgressSaved = false;

			var newQuestProgresses = new Dictionary<string, QuestProgressWSavedFlag>(prevStoredProgress._questProgresses);

			foreach (QuestProgress newQuestProgress in currentProgress.QuestProgresses.Select(x => x.Value))
			{
				QuestProgressWSavedFlag storedQuestProgress;
				if (prevStoredProgress._questProgresses.TryGetValue(newQuestProgress.CodeName, out storedQuestProgress)
					&& QuestProgressSaveData.Equals(newQuestProgress, storedQuestProgress.Progress)
				) {
					//Do nothing - newQuestProgresses entry is already up to date
				}
				else
				{
					//Update to the new value and mark as not saved
					newQuestProgresses[newQuestProgress.CodeName] = new QuestProgressWSavedFlag(
						questProgress: new QuestProgressSaveData(
							codeName: newQuestProgress.CodeName,
							status: newQuestProgress.Status,
							taskProgresses: newQuestProgress.TaskProgresses.AsEnumerable()
						),
						isSaved: false
					);
				}
			}

			this._questProgresses = new ReadOnlyDictionary<string, QuestProgressWSavedFlag>(newQuestProgresses);
		}


		#region Progress get methods

		public QuestProgressSaveData GetQuestProgress(string codeName)
		{
			QuestProgressWSavedFlag stored;
			if (_questProgresses.TryGetValue(codeName, out stored))
			{
				return stored.Progress;
			}
			else
			{
				return Saver.LoadQuestProgress(codeName, this.MainStoryProgress.GetQuestSaveID(codeName));
				// ^ This uses the load fail fallback if the saveID is not found on disk
			}
		}

		#endregion


		#region Progress save methods

		public void SaveUnsaved()
		{
			Saver.SaveMainProgress(_mainStoryProgress);

			foreach (var questProgress in _questProgresses.Values)
			{
				if (!questProgress.IsSaved)
				{
					Saver.SaveQuestProgress(
						saveID: _mainStoryProgress.QuestSaveIDs[questProgress.Progress.CodeName],
						questProgress: questProgress.Progress
					);
				}
			}
		}

		public void ForceSaveAll()
		{
			Saver.SaveMainProgress(_mainStoryProgress);

			foreach (var questProgress in _questProgresses.Values)
			{
				Saver.SaveQuestProgress(
					saveID: _mainStoryProgress.QuestSaveIDs[questProgress.Progress.CodeName],
					questProgress: questProgress.Progress
				);
			}
		}

		#endregion


		private struct QuestProgressWSavedFlag
		{
			public readonly QuestProgressSaveData Progress;
			public readonly bool IsSaved;

			public QuestProgressWSavedFlag(QuestProgressSaveData questProgress, bool isSaved)
			{
				if (questProgress == null) throw new ArgumentNullException("questProgress");

				this.Progress = questProgress;
				this.IsSaved = isSaved;
			}
		}

		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion


		#region old

		//	#region Progress update methods
		//	
		//	public void UpdateStoredProgress(StoryProgress storyProgress)
		//	{
		//		//	var oldMainProgress = this._mainStoryProgress;
		//		var newMainProgress = new MainStoryProgressSaveData(
		//			currentEpisode: storyProgress.CurrentEpisode,
		//			focusQuest: storyProgress.FocusQuest,
		//			questSaveIDs: storyProgress.QuestSaveIDs.AsEnumerable(),
		//			completedEpisodes: storyProgress.CompletedEpisodes.AsEnumerable()
		//		);
		//	
		//		this._mainStoryProgress = newMainProgress;
		//	
		//		//	bool equal = (
		//		//		newMainProgress.CurrentEpisode == oldMainProgress.CurrentEpisode
		//		//		&& newMainProgress.FocusQuest == oldMainProgress.FocusQuest
		//		//		&& Enumerable.All(
		//		//			newMainProgress.QuestSaveIDs,
		//		//			newQSaveID => oldMainProgress.QuestSaveIDs.Contains(newQSaveID)
		//		//		)
		//		//	);
		//		//	
		//		//	if (!equal) _isMainProgressSaved = false;
		//	
		//		foreach (QuestProgress newQuestProgress in storyProgress.QuestProgresses.Values)
		//		{
		//			QuestProgressWSavedFlag storedQuestProgress;
		//			if (this._questProgresses.TryGetValue(newQuestProgress.CodeName, out storedQuestProgress)
		//				&& QuestProgressSaveData.Equals(newQuestProgress, storedQuestProgress.Progress)
		//			) {
		//				//Do nothing - already up to date
		//			}
		//			else
		//			{
		//				this._questProgresses[newQuestProgress.CodeName] = new QuestProgressWSavedFlag(
		//					questProgress: new QuestProgressSaveData(
		//						codeName: newQuestProgress.CodeName,
		//						status: newQuestProgress.Status,
		//						taskProgresses: newQuestProgress.TaskProgresses.AsEnumerable()
		//					),
		//					isSaved: false
		//				);
		//			}
		//		}
		//	}
		//	
		//	#endregion

		#endregion
	}
}

//*/