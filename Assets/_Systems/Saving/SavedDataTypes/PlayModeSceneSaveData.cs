﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Interaction;
using UnityEngine;

namespace RevolitionGames.Saving.Types
{
	public class PlayModeSceneSaveData
	{
		public string SceneName { get; private set; }
		public ReadOnlyDictionary<string, Vector3> NPCPositions { get; private set; }

		public PlayModeSceneSaveData(string sceneName, IEnumerable<KeyValuePair<string, Vector3>> npcPositions)
		{
			ThrowIfArgNull(sceneName, "sceneName");
			ThrowIfArgNull(npcPositions, "npcPositions");

			this.SceneName = sceneName;
			this.NPCPositions = new ReadOnlyDictionary<string, Vector3>(
				npcPositions.ToDictionary(keySelector: x => x.Key, elementSelector: x => x.Value)
			);
		}

		public static PlayModeSceneSaveData Snapshot(PlayModeSceneData scene)
		{
			return new PlayModeSceneSaveData(
				scene.SceneName,
				new Dictionary<string, Vector3>() //TODO: NPC positions (after deadline)

			);
		}

		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		public PlayModeSceneData CreateSceneData(PlayModeStage stage)
		{
			return new PlayModeSceneData(
				stage: stage,
				sceneName: this.SceneName,
				spawnPointNum: 0, //TEMP
				interactableHost: new InteractableHost()
			);
		}

		#endregion
	}
}

//*/