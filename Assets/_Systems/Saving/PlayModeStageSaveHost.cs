﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Saving.Types;
using RevolitionGames.SceneManagement;
using UnityEngine;

namespace RevolitionGames.Saving
{
	public class PlayModeStageSaveHost
	{
		public Game Game { get; private set; }
		public PlayModeStageSaveData PrevSave { get; private set; }

		public PlayModeStageSaveHost(Game game, PlayModeStageSaveData prevSave)
		{
			if (game == null) throw new ArgumentNullException("game");
			if (prevSave == null) throw new ArgumentNullException("prevSave");

			this.Game = game;
			this.PrevSave = prevSave;
		}

		public PlayModeStageSaveHost(Game game, IStoryProgressSaver storyProgressSaver)
		{
			//TODO: Add non-dummy saving, add in a path, etc
			//Current planned path (for just one save):
			//	Application.persistentDataPath + "/RevGames/Experience/Save/Main"

			if (game == null) throw new ArgumentNullException("game");
			if (storyProgressSaver == null) throw new ArgumentNullException("storyProgressSaver");

			this.Game = game;
			this.PrevSave = new PlayModeStageSaveData(
				currentScene: new PlayModeSceneSaveData(
					sceneName: SceneInfo.Classroom.Name, //TODO: Load stuff like this from disk
					npcPositions: Enumerable.Empty<KeyValuePair<string, Vector3>>()
				),
				npcPlayerLikeabilities: Enumerable.Empty<KeyValuePair<string, float>>(),
				storyProgress: new StoryProgressSaveData( //This loads the story progress from disk (depending on storyProgressSaver)
					saver: storyProgressSaver
				)
			);
		}


		public void UpdateTo(PlayModeStage stage)
		{
			this.PrevSave = PlayModeStageSaveData.Snapshot(
				prev: this.PrevSave,
				stage: stage
			);
		}
	}
}

//*/