﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Saving
{
	public interface ISaveSurrogate<TTarget, TData>
	{
		TData GetSaveDataFromTarget(TTarget target);

		TTarget LoadTargetFromData(TData data);
	}
}

//*/