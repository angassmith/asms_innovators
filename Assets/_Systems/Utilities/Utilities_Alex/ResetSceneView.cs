﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Reflection;

public class ResetSceneView
{
	[MenuItem("Tools/Reset Scene View")]
	static void Reset()
	{
		if (SceneView.lastActiveSceneView!= null)
		{
			MethodInfo info = (
				 SceneView
				.lastActiveSceneView
				.GetType()
				.GetMethod(
					 "OnNewProjectLayoutWasCreated",
					 BindingFlags.Instance|BindingFlags.NonPublic
				)
			);
			info.Invoke(SceneView.lastActiveSceneView, null);
		}
	}
}

#endif