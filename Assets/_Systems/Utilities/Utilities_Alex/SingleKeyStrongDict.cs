﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

public abstract class StrongDict<TKey, TValue> : StrongDictBase<TValue>, IDictionary<TKey, TValue>, IDictionary
{
	private Dictionary<TKey, ValHolder> dict = new Dictionary<TKey, ValHolder>();

	public sealed override int Count { get { return dict.Count; } }

	public ReadOnlyCollection<TKey> Keys {
		get {
			TKey[] arr = new TKey[dict.Count];
			dict.Keys.CopyTo(arr, 0);
			return Array.AsReadOnly(arr);
		}
	}

	public sealed override ReadOnlyCollection<TValue> Values {
		get {
			TValue[] arr = new TValue[dict.Count];
			int i = 0;
			foreach (ValHolder val in dict.Values) {
				arr[i] = val.Get();
				i++;
			}
			return Array.AsReadOnly(arr);
		}
	}

	
	/// <summary>
	/// Initializes a strong-entry in the dictionary. Strong entries should also be public properties/fields of the deriving class. Can only be called while CanInitStrongEntriesNow is true.
	/// </summary>
	/// <param name="key"></param>
	/// <param name="getter">Example: () => SomeKey</param>
	/// <param name="setter">Example: v => SomeKey = v</param>
	/// <param name="val"></param>
	/// <exception cref="InvalidOperationException">CanInitStrongEntriesNow is false</exception>
	/// <exception cref="ArgumentNullException">key, getter, or setter is null</exception>
	/// <exception cref="ArgumentException">An element with the same key already exists in the StrongDict</exception>
	protected void InitStrongEntry(TKey key, Func<TValue> getter, Action<TValue> setter, TValue val)
	{
		if (!CanInitStrongEntriesNow) throw new InvalidOperationException("InitStrongEntry may only be called during the call to InitStrongEntries() by the StrongDict base constructor.");

		if (key == null) { throw new ArgumentNullException("key"); }
		if (getter == null) { throw new ArgumentNullException("getter"); }
		if (setter == null) { throw new ArgumentNullException("setter"); }

		dict.Add(key, new ValHolder.Strong(getter, setter)); //Will throw a 'key already exists' ArgumentException when relevant

		setter(val);
	}

	/// <summary>
	/// Adds a runtime-entry to the StrongDict
	/// </summary>
	protected void Add(TKey key, TValue value) {
		ThrowIfReadOnly();
		if (key == null) throw new ArgumentNullException("key");
		dict.Add(key, new ValHolder.Runtime(value));
	}

	/// <summary>
	/// Clears all runtime-items from the StrongDict. Does not remove strong-items.
	/// </summary>
	protected sealed override void Clear()
	{
		ThrowIfReadOnly();
		Dictionary<TKey, ValHolder> newDict = new Dictionary<TKey, ValHolder>();
		foreach (var kvp in dict)
		{
			if (kvp.Value is ValHolder.Strong) newDict.Add(kvp.Key, kvp.Value);
		}
		dict.Clear();
		dict = newDict;
	}

	public bool ContainsKey(TKey key) { return dict.ContainsKey(key); }

	public sealed override bool ContainsValue(TValue value)
	{
		foreach (var kvp in dict)
		{
			if (Equals(value, kvp.Value.Get())) return true;
		}
		return false;
	}

	public bool TryGetValue(TKey key, out TValue val)
	{
		ValHolder valHolder;
		if (dict.TryGetValue(key, out valHolder)) {
			val = valHolder.Get();
			return true;
		} else {
			val = default(TValue);
			return false;
		}
	}

	public TValue this[TKey key] {
		get {
			return dict[key].Get();
		}
		protected set {
			ThrowIfReadOnly();
			dict[key].Set(value);
		}
	}

	/// <summary>
	/// Sets the value associated with key if key is found in the StrongDict, otherwise adds key and val to the dictionary (as a runtime-entry). Returns true if an entry was added.
	/// </summary>
	/// <exception cref="ArgumentNullException">key is null</exception>
	protected bool SetOrAdd(TKey key, TValue val)
	{
		ThrowIfReadOnly();
		if (key == null) throw new ArgumentNullException("key");

		ValHolder valHolder;
		if (dict.TryGetValue(key, out valHolder)) {
			valHolder.Set(val);
			return false;
		} else {
			dict.Add(key, new ValHolder.Runtime(val));
			return true;
		}
	}

	/// <summary>
	/// Removes the runtime-item with the given key from the StrongDict. Does not remove strong-items. True if an item was removed.
	/// </summary>
	protected bool Remove(TKey key)
	{
		ThrowIfReadOnly();
		ValHolder valHolder;
		if (dict.TryGetValue(key, out valHolder)) {
			if (valHolder is ValHolder.Runtime) { dict.Remove(key); return true; }
			return false;
		} else {
			return false;
		}
	}

	/// <summary>
	/// Copies the items in the dictionary to an array, with a specified offset (see arrayIndex).
	/// </summary>
	/// <param name="array"></param>
	/// <param name="arrayIndex">0 copies the first entry to the start of the array, positive values copy the first entry to an offset item in the array, and negative values skip some dictionary entries before copying to the start of the array.</param>
	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
	{
		if (array == null) return;

		int i = arrayIndex;
		foreach (var kvp in dict) {
			if (i >= array.Length) break;
			if (i < 0) continue;
			array[i] = new KeyValuePair<TKey, TValue>(kvp.Key, kvp.Value.Get());
			i++;
		}
	}

	KeyValuePair<TKey, TValue>[] ToArray() {
		KeyValuePair<TKey, TValue>[] array = new KeyValuePair<TKey, TValue>[Count];
		this.CopyTo(array, 0);
		return array;
	}

	public Enumerator GetEnumerator() {
		return new Enumerator(this);
	}



	#region Interface Implementations (Note: write methods will throw errors when IsReadOnly is true)

	ICollection<TKey> IDictionary<TKey, TValue>.Keys { get { return Keys; } }
	ICollection<TValue> IDictionary<TKey, TValue>.Values { get { return Values; } }
	ICollection IDictionary.Keys { get { return Keys; } }
	ICollection IDictionary.Values { get { return Values; } }

	bool IDictionary.IsFixedSize { get { return IsReadOnly; } }
	bool ICollection.IsSynchronized { get { return false; } }
	object ICollection.SyncRoot { get { return null; } }

	void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item) { ThrowIfReadOnly(); Add(item.Key, item.Value); }
	void IDictionary<TKey, TValue>.Add(TKey key, TValue value) { ThrowIfReadOnly(); Add(key, value); }
	bool IDictionary<TKey, TValue>.Remove(TKey key) { ThrowIfReadOnly(); return Remove(key); }
	void ICollection<KeyValuePair<TKey, TValue>>.Clear() { ThrowIfReadOnly(); Clear(); }
	void IDictionary.Clear() { ThrowIfReadOnly(); Clear(); }

	TValue IDictionary<TKey, TValue>.this[TKey key] {
		get { return this[key]; }
		set { ThrowIfReadOnly(); this[key] = value; }
	}

	IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator() { return GetEnumerator(); }
	IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
	IDictionaryEnumerator IDictionary.GetEnumerator() { return GetEnumerator(); }

	void IDictionary.Add(object key, object value) { ThrowIfReadOnly(); Add(CheckAndCastKey<TKey>(key), CheckAndCastValue<TValue>(value)); }
	bool IDictionary.Contains(object key) { return IfValidKey<TKey, bool>(key, ContainsKey, false); }
	void IDictionary.Remove(object key) { ThrowIfReadOnly(); IfValidKey<TKey, bool>(key, Remove, false); }

	object IDictionary.this[object key] {
		get { return this[CheckAndCastKey<TKey>(key)]; }
		set { ThrowIfReadOnly(); this[CheckAndCastKey<TKey>(key)] = CheckAndCastValue<TValue>(value); }
	}

	bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item) {
		TValue val;
		if (TryGetValue(item.Key, out val)) return Equals(item.Value, val);
		else return false;
	}

	bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item) {
		ThrowIfReadOnly();
		if (((ICollection<KeyValuePair<TKey, TValue>>)this).Contains(item)) return Remove(item.Key);
		else return false;
	}

	void ICollection.CopyTo(Array array, int index)
	{
		if (array == null) return;
		if (array is KeyValuePair<TKey, TValue>[]) CopyTo((KeyValuePair<TKey, TValue>[])array, index);
		else throw new TypeMismatchException("The provided array was of type '" + array.GetType().FullName + "' instead of type '" + typeof(KeyValuePair<TKey, TValue>).FullName + "'.");
	}

	#endregion
	



	// struct for performance reasons, same as for built in collections (eg. Dictionary.Enumerator).
	// see: http://stackoverflow.com/q/3168311
	public struct Enumerator : IEnumerator<KeyValuePair<TKey, TValue>>, IDictionaryEnumerator
	{
		public readonly StrongDict<TKey, TValue> StrongDict;
		public bool IsDisposed { get; private set; }
		private Dictionary<TKey, ValHolder>.Enumerator innerEnumerator; //readonly sounds like it would affect performance

		public Enumerator(StrongDict<TKey, TValue> strongDict) {
			this.StrongDict = strongDict;
			this.IsDisposed = false;
			this.innerEnumerator = strongDict.dict.GetEnumerator();
		}

		public bool MoveNext() {
			if (IsDisposed) return false;
			return innerEnumerator.MoveNext();
		}

		public KeyValuePair<TKey, TValue> Current { get {
				if (IsDisposed) return new KeyValuePair<TKey, TValue>();
				KeyValuePair<TKey, ValHolder> innerCurrent = innerEnumerator.Current;
				return new KeyValuePair<TKey, TValue>(innerCurrent.Key, innerCurrent.Value.Get());
		} }
		TKey CurrentKey { get {
			if (IsDisposed) return default(TKey);
			return innerEnumerator.Current.Key;
		} }
		TValue CurrentValue { get {
			if (IsDisposed) return default(TValue);
			return innerEnumerator.Current.Value.Get();
		} }

		DictionaryEntry IDictionaryEnumerator.Entry { get {
			if (IsDisposed) return new DictionaryEntry();
			KeyValuePair<TKey, ValHolder> innerCurrent = innerEnumerator.Current;
			return new DictionaryEntry(innerCurrent.Key, innerCurrent.Value.Get());
		} }

		void IDisposable.Dispose() { innerEnumerator.Dispose(); IsDisposed = true; }
		void IEnumerator.Reset() { if (!IsDisposed) ((IEnumerator)innerEnumerator).Reset(); }
		object IEnumerator.Current { get { return Current; } }
		object IDictionaryEnumerator.Key { get { return CurrentKey; } }
		object IDictionaryEnumerator.Value { get { return CurrentValue; } }
	}

	

}

//*/