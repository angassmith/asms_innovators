﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

/// <summary>
/// Provides easy and readable runtime generic constraints.
/// </summary>
namespace ExtendedConstraints
{
	
	public abstract class Constraint
	{
		private static Dictionary<Type, Tuple<bool, string>> EvaluatedApplications = new Dictionary<Type, Tuple<bool, string>>();

		//Just used as the type in the above dictionary.
		//Jitting should be low cost as it is empty, and will only happen once for each combination
		//Other more complex methods/types also have to be jitted anyway.
		private struct ConstraintApplication<T, TConstraint>
			where TConstraint : Constraint, new()
		{ }

		///<summary>When Name is accessed, this will be trimmed, then enclosed in round brackets if NameNeedsEnclosing is true</summary>
		protected abstract string NameInternal { get; }
		protected virtual bool NameNeedsEnclosing { get { return false; } }

		private string TrimmedName { get { return NameInternal == null ? "" : NameInternal.Trim(); } }
		public string Name {
			get {
				if (NameNeedsEnclosing) return "(" + TrimmedName + ")";
				else return TrimmedName;
			}
		}

		protected abstract bool IsMatchInternal<T>();

		/// <summary>Returns this.Name</summary>
		public override string ToString() {
			return Name;
		}

		/// <summary>
		/// Holds a lazy-initialized instance of TConstraint.
		/// </summary>
		public static class Cache<TConstraint> where TConstraint : Constraint, new()
		{
			private static TConstraint _cachedInstance;
			public static TConstraint CachedInst {
				get { return _cachedInstance ?? (_cachedInstance = new TConstraint()); }
			}
		}

		public static bool IsMatch<T, TConstraint>(TConstraint constraintInstance = null)
			where TConstraint : Constraint, new()
		{
			Type constraintApplicationType = typeof(ConstraintApplication<T, TConstraint>);

			Tuple<bool, string> prevResult;
			if (EvaluatedApplications.TryGetValue(constraintApplicationType, out prevResult))
			{
				return prevResult.Item1;
			}
			else
			{
				TConstraint constraint = constraintInstance ?? Cache<TConstraint>.CachedInst;

				bool matchedNow = constraint.IsMatchInternal<T>();
				EvaluatedApplications.Add(constraintApplicationType, Tuple.Create(matchedNow, constraint.Name));
				return matchedNow;
			}
		}

		public static bool IsMatch<T, TConstraint>(out string constraintName)
			where TConstraint : Constraint, new()
		{
			Type constraintApplicationType = typeof(ConstraintApplication<T, TConstraint>);

			Tuple<bool, string> prevResult;
			if (EvaluatedApplications.TryGetValue(constraintApplicationType, out prevResult))
			{
				constraintName = prevResult.Item2;
				return prevResult.Item1;
			}
			else
			{
				TConstraint constraint = Cache<TConstraint>.CachedInst;

				bool matchedNow = constraint.IsMatchInternal<T>();
				constraintName = constraint.Name;
				EvaluatedApplications.Add(constraintApplicationType, Tuple.Create(matchedNow, constraintName));
				return matchedNow;
			}
		}
		
		/// <summary>
		/// Alternative form of ConstraintResult.Enforce()
		/// </summary>
		/// <exception cref="ArgumentNullException">constraint is null</exception>
		public static void Enforce(ConstraintResult constraint)
		{
			if (ReferenceEquals(constraint, null)) throw new ArgumentNullException("constraint");
			constraint.Enforce();
		}
	}

	#region Documentation
	/// <summary>
	/// Provides easy and readable runtime generic constraints. Eg. Constrain&lt;T&gt;.To&lt;Constraints.Enum&gt;("T").Enforce();
	/// </summary>
	/// <example>
	/// <code>
	/// using ExtendedConstraints;
	/// using Cts = ExtendedConstraints.Constraints;
	/// 
	/// public class Blah&lt;[ConstrainedTo(typeof(Cts.Enum))] T&gt;
	/// 	where T : struct, IFormattable, IConvertible, IComparable
	/// {
	/// 	static Blah()
	/// 	{
	/// 		Constrain&lt;T&gt;.To&lt;Cts.Enum&gt;("T").Enforce();
	/// 		Constraint.Enforce(Constrain&lt;T&gt;.To&lt;Cts.Enum&gt;("T")); //Identical to the previous line
	/// 	}
	/// 	
	/// 	[CompoundConstraint(
	/// 		"(constrain T3 to", typeof(Cts.Interface), ")",
	/// 		"or ((constrain T3 to", typeof(Cts.Sealed), ") and (constrain T4 to", typeof(Cts.Sealed), "))"
	/// 	)] //This is equivalent to the below line (neither is more or less preferable):
	/// 	[CompoundConstraint("(constrain T3 to Interface) or ((constrain T3 to Sealed) and (constrain T4 to Sealed))")]
	/// 	public void Foo&lt;
	/// 		[ConstrainedToNot(typeof(Cts.Abstract))] T1,
	/// 		[ConstrainedTo(typeof(Cts.Either&lt;Cts.Exactly&lt;int&gt;, Cts.Exactly&lt;uint&gt;&gt;))] T2,
	/// 		T3,
	/// 		T4
	/// 	&gt;()
	/// 		where T2 : struct, IFormattable, IConvertible, IComparable //Apply as many proper constraints as possible
	/// 	{
	/// 		Constrain&lt;T1&gt;.ToNot&lt;Cts.Abstract&gt;("T1").Enforce();
	/// 		Constrain&lt;T1&gt;.To&lt;Cts.Not&lt;Cts.Abstract&gt;&gt;("T1").Enforce(); //Identical, but not preferable
	/// 		
	/// 		Constrain&lt;T2&gt;.To&lt;Cts.Either&lt;Cts.Exactly&lt;int&gt;, Cts.Exactly&lt;uint&gt;&gt;&gt;().Enforce();
	/// 
	/// 		Constraint.Enforce(
	/// 			Constrain&lt;T3&gt;.To&lt;Cts.Interface&gt;("T3")
	/// 			|
	/// 			(Constrain&lt;T3&gt;.To&lt;Cts.Sealed&gt;("T3") &amp; Constrain&lt;T4&gt;.To&lt;Cts.Sealed&gt;("T4"))
	/// 		);
	/// 
	/// 		//Generally, use ___.Enforce(); with single constraints,
	/// 		//and Constraint.Enforce(___); with compound constraints.
	/// 	}
	/// }
	/// </code>
	/// </example>
	#endregion
	public static class Constrain<T>
	{
		public static ConstraintResult To<TConstraint>(string genericArgName = null) where TConstraint : Constraint, new()
		{
			string constraintName;
			bool wasMet = Constraint.IsMatch<T, TConstraint>(out constraintName);
			return new ConstraintResult(wasMet, constraintName, typeof(T), false, genericArgName);
		}

		public static ConstraintResult ToNot<TConstraint>(string genericArgName = null) where TConstraint : Constraint, new()
		{
			string constraintName;
			bool wasMet = Constraint.IsMatch<T, TConstraint>(out constraintName);
			return new ConstraintResult(wasMet, constraintName, typeof(T), true, genericArgName);
		}
	}

	public sealed class ConstraintResult
	{
		public bool WasMet { get; private set; }

		//Description is lazy-initialized to avoid unnecessary string concatenation
		private string _description;
		private string _constraintName;
		private Type _testedType;
		private bool _isInverse;
		private string _genericArgName;
		public string Description {
			get {
				if (_description == null)
				{
					string typeStr = (_testedType == null ? "<type missing>" : _testedType.FullName);
					string inverseStr = (_isInverse ? "not " : "");
					string argStr = "generic-argument" + (_genericArgName == null ? "" : (" '" + _genericArgName + "'") );

					this._description = "constrain " + argStr + " to " + inverseStr + "'" + _constraintName + "', with " + argStr + " == " + typeStr;

					this._constraintName = null; //Clear these as they're no longer needed
					this._testedType = null;
					this._genericArgName = null;
				}
				return _description;
			}
		}


		private ConstraintResult(bool wasMet, string description) {
			this.WasMet = wasMet;
			this._description = description;
		}

		public ConstraintResult(bool wasMet, string constraintName, Type testedType, bool isInverse, string genericArgName)
		{
			this.WasMet = wasMet;
			this._constraintName = constraintName;
			this._testedType = testedType;
			this._isInverse = isInverse;
			this._genericArgName = genericArgName;
			this._description = null;
		}

		public void Enforce()
		{
			if (!WasMet)
			{
				throw new ConstraintException(this);
			}
		}

		public override string ToString()
		{
			return (
				"("
				+ this.Description
				+ " [" + (this.WasMet ? "MET" : "FAILED") + "]"
				+ ")"
			);
		}

		public static ConstraintResult operator &(ConstraintResult left, ConstraintResult right) {
			if (left == null) throw new ArgumentNullException("left");
			if (right == null) throw new ArgumentNullException("right");
			return new ConstraintResult(
				left.WasMet && right.WasMet,
				left.ToString() + " AND " + right.ToString()
			);
		}

		public static ConstraintResult operator |(ConstraintResult left, ConstraintResult right) {
			if (left == null) throw new ArgumentNullException("left");
			if (right == null) throw new ArgumentNullException("right");
			return new ConstraintResult(
				left.WasMet || right.WasMet,
				left.ToString() + " OR " + right.ToString()
			);
		}

		public static ConstraintResult operator ^(ConstraintResult left, ConstraintResult right) {
			if (left == null) throw new ArgumentNullException("left");
			if (right == null) throw new ArgumentNullException("right");
			return new ConstraintResult(
				left.WasMet != right.WasMet,
				left.ToString() + " XOR " + right.ToString()
			);
		}

		public static ConstraintResult operator !(ConstraintResult x) {
			if (x == null) throw new ArgumentNullException("x");
			return new ConstraintResult(
				!x.WasMet,
				"NOT" + x.ToString() //Deliberately "not" instead of "not "
			);
		}

		#region About && and ||
		// Originally, the true() and false() operators were added to make && and || work the
		// same as & and | (with no short circuiting, due to the description strings).
		// However, this also made things like "if (someConstraintResult) { }" possible. This calls
		// true(someCompoundConstraintResult), which was set up to always return false (to avoid short-circuiting).
		// Defining an implicit cast to bool may or may not avoid this.
		// 
		// The && and || operators were previously added as follows:
		// 
		// public static bool operator false(CompoundConstraintResult x) { return false; }
		// public static bool operator true(CompoundConstraintResult x) { return false; }
		// 
		// Overloading the true and false operators allows for the short-circuiting versions of | and & (|| and &&)
		// to be used. However, short circuiting is not wanted here, because the Description needs to contain
		// both operands.
		//  - "a && b" evaluates to "false(a) ? a : (a & b)"
		//    Normally, false(a) is true if a is definitely false - eg. for ints if a is 0, so '0 & b' will always return 0.
		//    Here, the & operator always needs to execute, so false(a) always returns false.
		//  - "a || b" evaluates to "true(a) ? a : (a | b)"
		//    Normally, false(a) is true if a is definitely true - eg. for ints if a has all bits true,
		//    so '(allBitsTrue) | b' will always return allBitsTrue.
		//    Here, the | operator always needs to execute, so true(a) always returns false.
		#endregion
	}


	/// <summary>
	/// Basic extended-constraint types. Constraint types outside this class should be prepended with 'Constraint', eg. 'EnumConstraint'.
	/// </summary>
	static class Constraints
	{

		#region Constraint operators

		/// <summary>
		/// AND Constraint Operator
		/// </summary>
		public sealed class Both<TConstraint1, TConstraint2> : Constraint
			where TConstraint1 : Constraint, new()
			where TConstraint2 : Constraint, new()
		{
			private static readonly string name = Cache<TConstraint1>.CachedInst.Name + " and " + Cache<TConstraint2>.CachedInst.Name;

			protected override string NameInternal { get { return name; } }
			protected override bool NameNeedsEnclosing { get { return true; } }

			protected override bool IsMatchInternal<T>() {
				return IsMatch<T, TConstraint1>(Cache<TConstraint1>.CachedInst)
					&& IsMatch<T, TConstraint2>(Cache<TConstraint2>.CachedInst);
			}
		}

		/// <summary>
		/// OR Constraint Operator
		/// </summary>
		public sealed class Either<TConstraint1, TConstraint2> : Constraint
			where TConstraint1 : Constraint, new()
			where TConstraint2 : Constraint, new()
		{
			private static readonly string name = Cache<TConstraint1>.CachedInst.Name + " or " + Cache<TConstraint2>.CachedInst.Name;

			protected override string NameInternal { get { return name; } }
			protected override bool NameNeedsEnclosing { get { return true; } }

			protected override bool IsMatchInternal<T>() {
				return IsMatch<T, TConstraint1>(Cache<TConstraint1>.CachedInst)
					|| IsMatch<T, TConstraint2>(Cache<TConstraint2>.CachedInst);
			}
		}

		/// <summary>
		/// XOR Constraint Operator
		/// </summary>
		public sealed class XOR<TConstraint1, TConstraint2> : Constraint
			where TConstraint1 : Constraint, new()
			where TConstraint2 : Constraint, new()
		{
			private static readonly string name = Cache<TConstraint1>.CachedInst.Name + " xor " + Cache<TConstraint2>.CachedInst.Name;

			protected override string NameInternal { get { return name; } }
			protected override bool NameNeedsEnclosing { get { return true; } }

			protected override bool IsMatchInternal<T>() {
				return IsMatch<T, TConstraint1>(Cache<TConstraint1>.CachedInst)
					!= IsMatch<T, TConstraint2>(Cache<TConstraint2>.CachedInst);
			}
		}
		
		/// <summary>
		/// NOT Constraint Operator
		/// </summary>
		public sealed class Not<TConstraint> : Constraint
			where TConstraint : Constraint, new()
		{
			private static readonly string name = "not " + Cache<TConstraint>.CachedInst.Name;

			protected override string NameInternal { get { return name; } }
			protected override bool NameNeedsEnclosing { get { return true; } }

			protected override bool IsMatchInternal<T>() {
				return !IsMatch<T, TConstraint>(Cache<TConstraint>.CachedInst);
			}
		}
		
		/// <summary>
		/// TYPE1 &amp;&amp; !TYPE2 Constraint operator
		/// </summary>
		public sealed class FirstOnly<TMatched, TNonMatched> : Constraint
			where TMatched : Constraint, new()
			where TNonMatched : Constraint, new()
		{
			private static readonly string name = Cache<TMatched>.CachedInst.Name + " but not " + Cache<TNonMatched>.CachedInst.Name;

			protected override string NameInternal { get { return name; } }
			protected override bool NameNeedsEnclosing { get { return true; } }

			protected override bool IsMatchInternal<T>() {
				return IsMatch<T, TMatched>(Cache<TMatched>.CachedInst)
					&& !IsMatch<T, TNonMatched>(Cache<TNonMatched>.CachedInst);
			}
		}

		#endregion

		#region Modifier Constraints

		public sealed class Sealed : Constraint
		{
			protected override string NameInternal { get { return "sealed"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsSealed || typeof(T).IsValueType; //IsValueType may or may not be needed
			}
		}

		public sealed class Abstract : Constraint
		{
			protected override string NameInternal { get { return "abstract"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsAbstract;
			}
		}

		public sealed class Serializable : Constraint
		{
			protected override string NameInternal { get { return "serializable"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsSerializable;
			}
		}

		#endregion

		#region General base type constraints

		/// <summary>
		/// Behaves the same as the normal 'T : TBase' constraint (works where TBase is an interface or class)
		/// </summary>
		public sealed class Is<TBase> : Constraint where TBase : class
		{
			private static readonly string name = "is " + typeof(TBase).Name;
			
			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(TBase).IsAssignableFrom(typeof(T));
			}
		}

		/// <summary>
		/// Behaves the same as the normal 'T : TBase' constraint, but only works where TBase is a class
		/// </summary>
		public sealed class DerivesFrom<[ConstrainedTo(typeof(Class))] TBase> : Constraint where TBase : class
		{
			private static readonly string name = "derives from " + typeof(TBase).Name;

			static DerivesFrom() {
				Constrain<TBase>.To<Class>("TBase");
			}

			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(TBase).IsAssignableFrom(typeof(T));
			}
		}

		/// <summary>
		/// Matches classes that derive from TBase but are not equal to TBase
		/// </summary>
		public sealed class DerivesFromButNotEqualTo<[ConstrainedTo(typeof(Class))] TBase> : Constraint where TBase : class
		{
			private static readonly string name = "derives from but not equal to " + typeof(TBase).Name;

			static DerivesFromButNotEqualTo() {
				Constrain<TBase>.To<Class>("TBase");
			}

			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsSubclassOf(typeof(TBase)) && (typeof(TBase) != typeof(T));
			}
		}

		/// <summary>
		/// Behaves the same as the normal 'T : TIface' constraint, but only works where TIface is an interface
		/// </summary>
		public sealed class Implements<[ConstrainedTo(typeof(Interface))] TIface> : Constraint where TIface : class
		{
			private static readonly string name = "implements " + typeof(TIface).Name;

			static Implements() {
				Constrain<TIface>.To<Interface>("TIBase");
			}

			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(TIface).IsAssignableFrom(typeof(T));
			}
		}

		/// <summary>
		/// Matches classes and structs (but not interfaces) that implement TIBase
		/// </summary>
		public sealed class SolidImplements<[ConstrainedTo(typeof(Interface))] TIface> : Constraint
		{
			private static readonly string name = "solid type implements " + typeof(TIface).Name;

			static SolidImplements() {
				Constrain<TIface>.To<Interface>();
			}

			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return !typeof(T).IsInterface && typeof(TIface).IsAssignableFrom(typeof(T));
			}
		}

		/// <summary>
		/// Matches interfaces that extend an interface TIface. Eg. Constraints.InterfaceExtends&lt;IEnumerable&gt; would match IEnumerable&lt;int&gt;
		/// </summary>
		public sealed class InterfaceExtends<[ConstrainedTo(typeof(Interface))] TIface> : Constraint
		{
			private static readonly string name = "interface extends " + typeof(TIface).Name;

			static InterfaceExtends() {
				Constrain<TIface>.To<Interface>();
			}

			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsInterface && typeof(TIface).IsAssignableFrom(typeof(T));
			}
		}

		#endregion

		#region Specific base type constraints

		/// <summary>
		/// Matches types that derive from Delegate, but not Enum itself
		/// </summary>
		public sealed class Enum : Constraint
		{
			protected override string NameInternal { get { return "enum"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsEnum;
			}
		}

		/// <summary>
		/// Matches types that derive from Delegate, but not Delegate itself
		/// </summary>
		public sealed class Delegate : Constraint
		{
			protected override string NameInternal { get { return "delegate"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsSubclassOf(typeof(System.Delegate));
			}
		}

		/// <summary>
		/// Matches types that derive from MulticastDelegate, but not MulticastDelegate itself
		/// </summary>
		public sealed class MulticastDelegate : Constraint
		{
			protected override string NameInternal { get { return "delegate"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsSubclassOf(typeof(System.MulticastDelegate));
			}
		}

		/// <summary>
		/// Matches array types (eg. int[]), but not the Array class itself
		/// </summary>
		public sealed class Array : Constraint
		{
			protected override string NameInternal { get { return "array"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsArray;
			}
		}

		#endregion

		#region Type of type constraints (class, interface, etc)

		public sealed class Interface : Constraint
		{
			protected override string NameInternal { get { return "interface"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsInterface;
			}
		}

		/// <summary>
		/// Uses Type.IsClass, which differs from the normal 'class' constraint in that it does not allow interfaces
		/// </summary>
		public sealed class Class : Constraint
		{
			protected override string NameInternal { get { return "class"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsClass;
			}
		}

		/// <summary>
		/// Uses Type.IsValueType, which differs from the normal 'struct' constraint in that it allows Nullable&lt;&gt;s
		/// </summary>
		public sealed class Struct : Constraint
		{
			protected override string NameInternal { get { return "struct"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType;
			}
		}

		/// <summary>
		/// Allows classes and interfaces (and the Enum and ValueType types) (same as the normal 'class' constraint)
		/// </summary>
		public sealed class ReferenceType : Constraint
		{
			//	private readonly Type valueTypeType = typeof(ValueType);
			//	private readonly Type enumType = typeof(Enum);

			protected override string NameInternal { get { return "class"; } }

			protected override bool IsMatchInternal<T>() {
				return !typeof(T).IsValueType; //IsValueType already checks Enum and ValueType
			}
		}

		/// <summary>
		/// Allows structs but does not allow Nullable&lt;&gt;s (same as the normal 'struct' constraint)
		/// </summary>
		public sealed class NonNullableStruct : Constraint
		{
			protected override string NameInternal { get { return "class"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType && (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>));
			}
		}

		/// <summary>
		/// Allows classes and Nullable&lt;&gt;s
		/// </summary>
		public sealed class AnyNullable : Constraint
		{
			protected override string NameInternal { get { return "class"; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType && (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>));
			}
		}

		#endregion

		#region Constructor constraints

		/// <summary>
		/// Behaves the same as the default 'new' constraint. Generic versions allow non-abstract types with a public constructor that matches the specified parameter types
		/// </summary>
		public sealed class New : Constraint {
			protected override string NameInternal { get { return "new()"; } }
			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType || ((!typeof(T).IsAbstract) && typeof(T).GetConstructor(new Type[0]) != null);
			}
		}
		public sealed class New<T1> : Constraint {
			private static readonly string name = "new(" + typeof(T1).Name + ")";
			protected override string NameInternal { get { return name; } }
			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType || ((!typeof(T).IsAbstract) && typeof(T).GetConstructor(new Type[] { typeof(T1) }) != null);
			}
		}
		public sealed class New<T1, T2> : Constraint {
			private static readonly string name = "new(" + typeof(T1) .Name+ ", " + typeof(T2).Name + ")";
			protected override string NameInternal { get { return name; } }
			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType || ((!typeof(T).IsAbstract) && typeof(T).GetConstructor(new Type[] { typeof(T1), typeof(T2) }) != null);
			}
		}
		public sealed class New<T1, T2, T3> : Constraint {
			private static readonly string name = "new(" + typeof(T1).Name + ", " + typeof(T2).Name + ", " + typeof(T3).Name + ")";
			protected override string NameInternal { get { return name; } }
			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType || ((!typeof(T).IsAbstract) && typeof(T).GetConstructor(new Type[] { typeof(T1), typeof(T2), typeof(T3) }) != null);
			}
		}
		public sealed class New<T1, T2, T3, T4> : Constraint {
			private static readonly string name = "new(" + typeof(T1).Name + ", " + typeof(T2).Name + ", " + typeof(T3).Name + ", " + typeof(T4).Name + ")";
			protected override string NameInternal { get { return name; } }
			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType || ((!typeof(T).IsAbstract) && typeof(T).GetConstructor(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4) }) != null);
			}
		}
		public sealed class New<T1, T2, T3, T4, T5> : Constraint {
			private static readonly string name = "new(" + typeof(T1).Name + ", " + typeof(T2).Name + ", " + typeof(T3).Name + ", " + typeof(T4).Name + ", " + typeof(T5).Name + ")";
			protected override string NameInternal { get { return name; } }
			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsValueType || ((!typeof(T).IsAbstract) && typeof(T).GetConstructor(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5) }) != null);
			}
		}

		#endregion

		#region Attribute constraints

		/// <summary>
		/// Matches types that have the specified attribute. Does not look at parent types' attributes.
		/// </summary>
		public sealed class Has<TAttribute> : Constraint
		{
			private static readonly string name = "has attribute (not inherited) " + typeof(TAttribute).Name;

			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsDefined(typeof(TAttribute), false);
			}
		}

		/// <summary>
		/// Matches types that have the specified attribute, or are descended from a type that has the specified attribute.
		/// </summary>
		/// <typeparam name="TAttribute"></typeparam>
		public sealed class HasInherited<TAttribute> : Constraint
		{
			private static readonly string name = "has or has inherited attribute " + typeof(TAttribute).Name;

			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T).IsDefined(typeof(TAttribute), false);
			}
		}

		#endregion

		#region Other constraints

		/// <summary>
		/// Matches any type. For potential generalisation purposes
		/// </summary>
		public sealed class Any : Constraint
		{
			protected override string NameInternal { get { return "any"; } }

			protected override bool IsMatchInternal<T>() {
				return true;
			}
		}

		/// <summary>
		/// Matches where typeof(T) == typeof(TIBase). Most useful when used with a 'not' constraint operator
		/// </summary>
		public sealed class Exactly<TIBase> : Constraint
		{
			private static readonly string name = "exactly " + typeof(TIBase).Name;

			protected override string NameInternal { get { return name; } }

			protected override bool IsMatchInternal<T>() {
				return typeof(T) == typeof(TIBase);
			}
		}

		#endregion
	}

	[Serializable]
	public class ConstraintException : Exception
	{
		public ConstraintException() : base("A type did not match an extended constraint.") { }
		public ConstraintException(string message, Exception innerException = null) : base(message, innerException) { }
		public ConstraintException(ConstraintResult failed)
			: base("The following singular or compound constraint was not met: " + (failed == null ? "''" : failed.ToString()))
		{ }
		protected ConstraintException(
			System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}

	/// <summary>
	/// Does not constrain the parameter, just marks that it has been constrained
	/// </summary>
	[System.AttributeUsage(AttributeTargets.GenericParameter, Inherited = false, AllowMultiple = true)]
	sealed class ConstrainedToAttribute : Attribute
	{
		public Type Constraint { get; private set; }

		/// <summary>
		/// Does not constrain the parameter, just marks that it has been constrained
		/// </summary>
		public ConstrainedToAttribute(Type constraint) {
			this.Constraint = constraint;
		}
	}

	/// <summary>
	/// Does not constrain the parameter, just marks that it has been constrained
	/// </summary>
	[System.AttributeUsage(AttributeTargets.GenericParameter, Inherited = false, AllowMultiple = true)]
	sealed class ConstrainedToNotAttribute : Attribute
	{
		public Type Constraint { get; private set; }

		/// <summary>
		/// Does not constrain the parameter, just marks that it has been constrained
		/// </summary>
		public ConstrainedToNotAttribute(Type constraint) {
			this.Constraint = constraint;
		}
	}

	/// <summary>
	/// Does not constrain any parameters, just marks how they have been constrained
	/// </summary>
	[System.AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
	sealed class CompoundConstraintAttribute : Attribute
	{
		private string _description;
		/// <summary>
		/// Either returns the description passed to the constructor,
		/// or joins the elements in the arrayRepresentation passed to the constructor, with spaces in between
		/// </summary>
		public string Description {
			get {
				if (_description == null)
					_description = string.Join(" ", _arrayRepresentation.Select(x => string.Concat(x)).ToArray());
				return _description;
			}
		}

		private object[] _arrayRepresentation;
		/// <summary>
		/// Either returns the arrayRepresentation passed to the constructor,
		/// or returns a single element array containing the description passed to the constructor
		/// </summary>
		public object[] ArrayRepresentation {
			get {
				if (_arrayRepresentation == null)
					_arrayRepresentation = new object[] { _description };
				return _arrayRepresentation;
			}
		}

		/// <summary>
		/// Does not constrain any parameters, just marks how they have been constrained.
		/// Example: [CompoundConstrained("(constrain T1 to Enum) or (constrain T2 to Delegate)")]
		/// </summary>
		public CompoundConstraintAttribute(string description) {
			this._description = description;
		}
		/// <summary>
		/// Does not constrain any parameters, just marks how they have been constrained.
		/// Example: [CompoundConstrained("(constrain T1 to", typeof(Constraints.Enum), ") or (constrain T2 to", typeof(Constraints.Delegate), ")")]
		/// </summary>
		public CompoundConstraintAttribute(params object[] arrayRepresentation) {
			this._arrayRepresentation = arrayRepresentation;
		}
	}
}


//*/