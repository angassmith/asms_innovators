﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

public sealed class ReferenceEqualityComparer<T> : EqualityComparer<T>, IEqualityComparer
{
	private static ReferenceEqualityComparer<T> _defaultRefComparer;
	public static ReferenceEqualityComparer<T> DefaultRefComparer {
		get { return _defaultRefComparer ?? (_defaultRefComparer = new ReferenceEqualityComparer<T>()); }
	}

	public override bool Equals(T x, T y) {
		return ReferenceEquals(x, y);
	}

	public override int GetHashCode(T obj) {
		return obj == null ? 0 : obj.GetHashCode();
	}

	bool IEqualityComparer.Equals(object x, object y) {
		return ReferenceEquals(x, y);
	}

	int IEqualityComparer.GetHashCode(object obj) {
		return obj == null ? 0 : obj.GetHashCode();
	}
}

//*/