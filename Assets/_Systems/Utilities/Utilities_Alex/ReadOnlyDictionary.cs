﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

/// <summary>
/// A read-only wrapper for a <see cref="Dictionary{TKey, TValue}"/>
/// </summary>
public class ReadOnlyDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollection //IReadOnlyDictionary<> and IReadOnlyCollection are .NET 4.5+
{
	private IDictionary<TKey, TValue> dictionary;

	public ReadOnlyDictionary(IDictionary<TKey, TValue> source) {
		if (source == null) throw new ArgumentNullException("source");
		this.dictionary = source;
	}

	public const string NotSupportedExceptionMessage = "Cannot modify a ReadOnlyDictionary";

	public bool IsFixedSize { get { return true; } }
	public bool IsReadOnly { get { return true; } }

	public int                 Count  { get { return dictionary.Count ; } }
	public ICollection<TKey>   Keys   { get { return dictionary.Keys  ; } }
	public ICollection<TValue> Values { get { return dictionary.Values; } }

	public TValue this[TKey key] { get { return dictionary[key]; } }

	public          bool   ContainsKey  (TKey   key                  ) { return dictionary.ContainsKey  (key           ); }
	public          bool   TryGetValue  (TKey   key, out TValue value) { return dictionary.TryGetValue  (key, out value); }
	public override bool   Equals       (object obj                  ) { return dictionary.Equals       (obj           ); }
	public override int    GetHashCode  (                            ) { return dictionary.GetHashCode  (              ); }
	public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() { return dictionary.GetEnumerator(); }
	public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) { dictionary.CopyTo(array, arrayIndex); }

	public bool ContainsValue(TValue value) {
		foreach (TValue val in this.Values) {
			if (Equals(value, val)) return true;
		}
		return false;
	}

	public KeyValuePair<TKey, TValue>[] ToArray()
	{
		KeyValuePair<TKey, TValue>[] arr = new KeyValuePair<TKey, TValue>[this.Count];
		int i = 0;
		foreach (var entry in this) {
			arr[i] = entry;
			i++;
		}
		return arr;
	}

	#region Interface Implementations

	#region Supported

	IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
	bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item) { return dictionary.Contains(item); }
	bool ICollection.IsSynchronized { get { return false; } }
	object ICollection.SyncRoot { get { return null; } }
	
	void ICollection.CopyTo(Array array, int index) {
		//Lazy implementation. Template for a proper implementation:
		//https://referencesource.microsoft.com/#mscorlib/system/collections/objectmodel/readonlycollection.cs
		ICollection dictAsICollection = dictionary as ICollection;
		if (dictAsICollection != null) {
			dictAsICollection.CopyTo(array, index);
		} else {
			this.ToArray().CopyTo(array, index);
		}
	}

	#endregion

	#region Not Supported

	void IDictionary<TKey, TValue>.Add(TKey key, TValue value) { throw new NotSupportedException(NotSupportedExceptionMessage); }
	bool IDictionary<TKey, TValue>.Remove(TKey key) { throw new NotSupportedException(NotSupportedExceptionMessage); }
	void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item) { throw new NotSupportedException(NotSupportedExceptionMessage); }
	void ICollection<KeyValuePair<TKey, TValue>>.Clear() { throw new NotSupportedException(NotSupportedExceptionMessage); }
	bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item) { throw new NotSupportedException(NotSupportedExceptionMessage); }

	#endregion

	TValue IDictionary<TKey, TValue>.this[TKey key] {
		get { return this[key]; }
		set { throw new NotSupportedException(NotSupportedExceptionMessage); }
	}

	#endregion
}