﻿using System;
using System.Collections;

//	/// <summary>An empty class, with only a single instance. Uses include making inherited parameterless constructors be explicitly specified</summary>
/// <summary>An empty class, with no instances. Uses include making inherited parameterless constructors be explicitly specified</summary>
[System.Serializable] //It may be for fields used in serializable generic classes (IDK if this is actually required, or if null can be serialized regardless of the type)
public abstract class VoidClass
{
	//	//Because VoidClass is abstract, it can never be instantiated, even with
	//	//System.Runtime.Serialization.FormatterServices.GetUninitializedObject().
	//	//It cannot be derived from, because it's constructor is private. However,
	//	//nested types can access the parent type's private members, so a nested
	//	//VoidClassInstance class can access the constructor.
	//	//It is still possible to create a second instance of VoidClass, but this
	//	//involves reflecting to access VoidClassInstance, then using
	//	//GetUninitializedObject().

	//	/// <summary>The only instance of VoidClass</summary>
	//	public static readonly VoidClass BackingVoid = new VoidClassInstance();

	//It's abstract, so can't be instantiated; and has only a private constructor, so can't be derived from

	//Similar to UninstantiableClass, but has some different uses

	public const VoidClass Void = null;
	
	private VoidClass() {
		//	if (!ReferenceEquals(BackingVoid, null)) throw new InvalidOperationException("There may only ever be one instance of VoidClass");
		throw new InvalidOperationException("There may never be any instances of VoidClass");
	}

	//	private sealed class VoidClassInstance : VoidClass
	//	{
	//		public VoidClassInstance() : base() { } //Private members are accessible from nested types
	//	}


	/// <summary>Returns true if obj is null or (somehow) is of type VoidClass</summary>
	public override bool Equals(object obj) {
		return obj == null || (obj is VoidClass);
	}
	
	/// <summary>Returns true</summary>
	public bool Equals(VoidClass other) {
		return true;
	}
	
	/// <summary>Returns true</summary>
	public static bool operator ==(VoidClass left, VoidClass right) {
		return true;
	}
	/// <summary>Returns false</summary>
	public static bool operator !=(VoidClass left, VoidClass right) {
		return false;
	}
	
	/// <summary>Returns true</summary>
	public static bool Equals(VoidClass left, VoidClass right) {
		return true;
	}
	
	/// <summary>Returns 0</summary>
	public override int GetHashCode() { return 0; }
	
	/// <summary>Returns "VoidClass.Void"</summary>
	public override string ToString() { return "VoidClass.Void"; }

	//	/// <summary>Returns true if obj is of type VoidClass and is not null</summary>
	//	public override bool Equals(object obj) {
	//		return obj != null && (obj is VoidClass);
	//	}
	//	
	//	/// <summary>Returns true if other is not null</summary>
	//	public bool Equals(VoidClass other) {
	//		return !ReferenceEquals(other, null);
	//	}
	//	
	//	/// <summary>Returns VoidClass.Equals(left, right)</summary>
	//	public static bool operator ==(VoidClass left, VoidClass right) {
	//		return Equals(left, right);
	//	}
	//	/// <summary>Returns !VoidClass.Equals(left, right)</summary>
	//	public static bool operator !=(VoidClass left, VoidClass right) {
	//		return !Equals(left, right);
	//	}
	//	/// <summary>Returns VoidClass.Equals(left, right)</summary>
	//	public static bool operator ==(VoidClass left, object right) {
	//		return Equals(left, right);
	//	}
	//	/// <summary>Returns !VoidClass.Equals(left, right)</summary>
	//	public static bool operator !=(VoidClass left, object right) {
	//		return !Equals(left, right);
	//	}
	//	/// <summary>Returns VoidClass.Equals(left, right)</summary>
	//	public static bool operator ==(object left, VoidClass right) {
	//		return Equals(left, right);
	//	}
	//	/// <summary>Returns !VoidClass.Equals(left, right)</summary>
	//	public static bool operator !=(object left, VoidClass right) {
	//		return !Equals(left, right);
	//	}
	//	
	//	/// <summary>Returns true if both are null, or neither are null and both are of type VoidClass</summary>
	//	public static new bool Equals(object left, object right)
	//	{
	//		bool leftNull = ReferenceEquals(left, null);
	//		bool rightNull = ReferenceEquals(right, null);
	//		return (leftNull && rightNull) || (!(leftNull || rightNull) && (left is VoidClass) && (right is VoidClass));
	//	}
	//	/// <summary>Returns true if both are null, or neither are null and right is of type VoidClass</summary>
	//	public static bool Equals(VoidClass left, object right) {
	//		bool leftNull = ReferenceEquals(left, null);
	//		bool rightNull = ReferenceEquals(right, null);
	//		return (leftNull && rightNull) || (!(leftNull || rightNull) && (right is VoidClass));
	//	}
	//	/// <summary>Returns true if both are null, or neither are null and left is of type VoidClass</summary>
	//	public static bool Equals(object left, VoidClass right) {
	//		bool leftNull = ReferenceEquals(left, null);
	//		bool rightNull = ReferenceEquals(right, null);
	//		return (leftNull && rightNull) || (!(leftNull || rightNull) && (left is VoidClass));
	//	}
	//	/// <summary>Returns true if both or neither are null</summary>
	//	public static bool Equals(VoidClass left, VoidClass right) {
	//		bool leftNull = ReferenceEquals(left, null);
	//		bool rightNull = ReferenceEquals(right, null);
	//		return (leftNull && rightNull) || (!(leftNull || rightNull));
	//	}
	//	
	//	/// <summary>Returns 0</summary>
	//	public override int GetHashCode() { return 0; }
	//	
	//	/// <summary>Returns "VoidClass.Void"</summary>
	//	public override string ToString() { return "VoidClass.Void"; }
}