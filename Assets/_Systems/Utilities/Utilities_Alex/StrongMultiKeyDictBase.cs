﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

public abstract class StrongMultiKeyDictBase<TPublicEntry, TPrivateEntry, TValue> : StrongDictBase<TValue>, IEnumerable<TPublicEntry>
	where TPublicEntry : struct, IMultiKeyDictEntry<TValue>
	where TPrivateEntry : struct, IMultiKeyDictEntry<StrongDictBase<TValue>.ValHolder>
{
	public StrongMultiKeyDictBase()
	{
		
	}

	protected abstract TPublicEntry ConvertToPublicEntry(TPrivateEntry entry);
	//	protected abstract TPrivateEntry ConvertToPrivateRuntimeEntry(TPublicEntry entry);
	//	protected abstract TPrivateEntry ConvertToPrivateStrongEntry(TPublicEntry entry);
	
	protected abstract IMultiKeyDictBase<TPrivateEntry, ValHolder> InternalDict { get; set; }

	public Enumerator GetEnumerator() { return new Enumerator(this); }

	IEnumerator<TPublicEntry> IEnumerable<TPublicEntry>.GetEnumerator() { return GetEnumerator(); }
	IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

	//	public bool ContainsEntry(TPublicEntry entry) {
	//		return InternalDict.ContainsEntry(ConvertToPrivateRuntimeEntry(entry));
	//	}
	//	public abstract bool ContainsEntry(TPublicEntry entry);
	public abstract bool ContainsAnyKeysOfEntry(TPublicEntry entry);

	/// <summary>
	/// Copies the items in the dictionary to an array, with a specified offset (see arrayIndex).
	/// </summary>
	/// <param name="array"></param>
	/// <param name="arrayIndex">0 copies the first entry to the start of the array, positive values copy the first entry to an offset item in the array, and negative values skip some dictionary entries before copying to the start of the array.</param>
	public void CopyTo(TPublicEntry[] array, int arrayIndex)
	{
		if (array == null) return;

		int i = arrayIndex;
		foreach (var entry in InternalDict) {
			if (i >= array.Length) break;
			if (i < 0) continue;
			array[i] = ConvertToPublicEntry(entry);
			i++;
		}
	}

	public TPublicEntry[] ToArray() {
		TPublicEntry[] array = new TPublicEntry[Count];
		this.CopyTo(array, 0);
		return array;
	}

	protected class Helper
	{
		public StrongMultiKeyDictBase<TPublicEntry, TPrivateEntry, TValue> Dict;

		public Helper(StrongMultiKeyDictBase<TPublicEntry, TPrivateEntry, TValue> dict)
		{
			if (dict == null) throw new ArgumentNullException("dict");
			this.Dict = dict;
		}

		//	public void CheckIfKeyNull(object key) {
		//		if (key == null) throw new ArgumentNullException("[a key]", "A dictionary key cannot be null");
		//	}

		public ValHolder.Strong HelpInitStrongEntry(Func<TValue> getter, Action<TValue> setter)
		{
			//Note: Adding an entry to the internal dictionary will do null-checks on keys
			
			//Temp debugging code (now fixed):
			//	//	throw new InvalidOperationException("Why doesn't this fail?!");
			//	//	if (Dict == null)
			//	//		throw new Exception("wtf how is Dict null?!"); //How does this throw a NullReferenceException???
			//	try {
			//	
			//		//These two lines are not temp:
			//		//	if (!Dict.CanInitStrongEntriesNow)
			//		//		throw new InvalidOperationException("InitStrongEntry may only be called during the call to InitStrongEntries() by the StrongDictBase constructor.");
			//	
			//		throw new Exception("foo: " + (Dict == null) + ", " + (ReferenceEquals(Dict, null)));
			//		if (Dict.CanInitStrongEntriesNow) return null;
			//		throw new InvalidOperationException("InitStrongEntry may only be called during the call to InitStrongEntries() by the StrongDictBase constructor.");
			//	} catch (Exception e) { throw new Exception("wtf how???", e); }
			//	
			//	//	throw new InvalidOperationException("foo: " + (ReferenceEquals(Dict, null))); //Throws an NRE
			//	//	throw new InvalidOperationException("blah"); //Works fine
			//	if (null == null) { } //Works fine
			//	if (new object() == null) { } //Works fine
			//	var a = new object(); //Works fine
			//	if (a == null) { } //Works fine
			//	
			//	
			//	if (this == null) { throw new InvalidOperationException("this == null"); } //Throws InvalidOperationException. How does 'this == null'?
			//	object c = Dict; //Throws an NRE
			//	if (c == null) { } //Unknown
			//	var b = Dict; //Throws an NRE
			//	if (b == null) { } //Unknown
			//	
			//	if (Dict.IsNull()) { } //Throws an NRE
			//	if (null == Dict) { } //Throws an NRE
			//	if (null != Dict) { } //Throws an NRE
			//	if (Equals(null, Dict)) { } //Throws an NRE
			//	if (ReferenceEquals(null, Dict)) { } //Throws an NRE
			//	if (Dict == null) { } //Throws an NRE
			//	if (Dict != null) { } //Throws an NRE
			//	if (Equals(Dict, null)) { } //Throws an NRE
			//	if (ReferenceEquals(Dict, null)) { } //Throws an NRE
			//	if (!Dict.CanInitStrongEntriesNow)
			//		throw new InvalidOperationException("InitStrongEntry may only be called during the call to InitStrongEntries() by the StrongDictBase constructor.");

			if (!Dict.CanInitStrongEntriesNow)
				throw new InvalidOperationException("InitStrongEntry may only be called during the call to InitStrongEntries() by the StrongDictBase constructor.");

			if (getter == null) throw new ArgumentNullException("getter");
			if (setter == null) throw new ArgumentNullException("setter");
			return new ValHolder.Strong(getter, setter);
		}

		public bool TryGetValue<TKey>(TKey key, ValueLookupFunc<TKey, TValue> lookupFunc, out TValue val)
		{
			ValHolder valHolder;
			if (lookupFunc(key, out valHolder)) {
				val = valHolder.Get();
				return true;
			} else {
				val = default(TValue);
				return false;
			}
		}

		public bool TryGetEntry<TKey>(TKey key, EntryLookupFunc<TKey, TPrivateEntry> lookupFunc, out TPublicEntry entry)
		{
			TPrivateEntry foundEntry;
			if (lookupFunc(key, out foundEntry)) {
				entry = Dict.ConvertToPublicEntry(foundEntry);
				return true;
			} else {
				entry = default(TPublicEntry);
				return false;
			}
		}

		public bool RemoveRuntimeEntry<TKey>(TKey key, ValueLookupFunc<TKey, TValue> lookupFunc, Action<TKey> remover)
		{
			Dict.ThrowIfReadOnly();
			ValHolder valHolder;
			if (lookupFunc(key, out valHolder)) {
				if (valHolder is ValHolder.Runtime) { remover(key); return true; }
				return true;
			} else {
				return false;
			}
		}

		public ReadOnlyCollection<TValue> GetValues(ICollection<ValHolder> innerValues)
		{
			TValue[] arr = new TValue[innerValues.Count];
			int i = 0;
			foreach (ValHolder val in innerValues) {
				arr[i] = val.Get();
				i++;
			}
			return Array.AsReadOnly(arr);
		}

		public bool ContainsValue(ICollection<ValHolder> innerValues, TValue value)
		{
			foreach (ValHolder val in innerValues)
			{
				if (Equals(value, val.Get())) return true;
			}
			return false;
		}

		public void Clear(IMultiKeyDictBase<TPrivateEntry, ValHolder> newDict)
		{
			Dict.ThrowIfReadOnly();
			IMultiKeyDictBase<TPrivateEntry, ValHolder> oldDict = Dict.InternalDict;
			foreach (var entry in oldDict)
			{
				if (entry.Value is ValHolder.Strong) newDict.Add(entry);
			}
			oldDict.Clear();
			Dict.InternalDict = newDict;
		}

		//	public bool ContainsEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8>(MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>, TValue>.ProtectedMemberAccessor accessor, TPrivateEntry entry)
		//	{
		//		if (ReferenceEquals(entry, null)) throw new ArgumentNullException("entry");
		//		TPrivateEntry foundEntry;
		//		if (accessor.TryGetEntryByKey1((TKey1)entry.GetUntypedKey1(), out foundEntry)) {
		//			if (foundEntry.Equals(entry)) return true;
		//		}
		//		return false;
		//	}
		
		//	public bool ContainsAnyKeysOfEntry(TPrivateEntry entry)
		//	{
		//		return Dict.InternalDict.ContainsAnyKeysOfEntry(entry);
		//	}

		/// <summary>
		/// True if an item in objs is null, otherwise, false. False if objs itself is null (it contains no null items)
		/// </summary>
		public static bool AnyNull(params object[] objs) {
			if (objs == null) return false;
			for (int i = 0; i < objs.Length; i++) {
				if (objs[i] == null) return true;
			}
			return false;
		}
	}

	// struct for performance reasons, same as for built in collections (eg. Dictionary.Enumerator).
	// see: http://stackoverflow.com/q/3168311
	public struct Enumerator : IEnumerator<TPublicEntry>
	{
		public readonly StrongMultiKeyDictBase<TPublicEntry, TPrivateEntry, TValue> StrongDict;
		public bool IsDisposed { get; private set; }
		private IEnumerator<TPrivateEntry> innerEnumerator; //readonly sounds like it would affect performance

		public Enumerator(StrongMultiKeyDictBase<TPublicEntry, TPrivateEntry, TValue> strongDict) {
			this.StrongDict = strongDict;
			this.IsDisposed = false;
			this.innerEnumerator = strongDict.InternalDict.GetEnumerator();
		}

		public bool MoveNext() {
			if (IsDisposed) return false;
			return innerEnumerator.MoveNext();
		}

		public TPublicEntry Current { get {
				if (IsDisposed) return default(TPublicEntry);
				return StrongDict.ConvertToPublicEntry(innerEnumerator.Current);
		} }
		TValue CurrentValue { get {
			if (IsDisposed) return default(TValue);
			return innerEnumerator.Current.Value.Get();
		} }

		void IDisposable.Dispose() { innerEnumerator.Dispose(); IsDisposed = true; }
		void IEnumerator.Reset() { if (!IsDisposed) innerEnumerator.Reset(); }
		object IEnumerator.Current { get { return Current; } }
	}
}

//*/