﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UObject = UnityEngine.Object;

public static class ShowAllObjects
{

	private static Dictionary<UObject, HideFlags> OriginalStates = new Dictionary<UObject, HideFlags>(ReferenceEqualityComparer<UObject>.DefaultRefComparer);

	public const HideFlags AffectedFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector | HideFlags.NotEditable;

	[MenuItem("Tools/Show And Edit Objects/Reset")]
	public static void Reset()
	{
		foreach (var entry in OriginalStates) {
			if (entry.Key != null) {
				entry.Key.hideFlags = entry.Value;
			}
		}
		OriginalStates.Clear();
	}

	[MenuItem("Tools/Show And Edit Objects/All")]
	public static void ShowAll()
	{
		foreach (UObject obj in Resources.FindObjectsOfTypeAll<UObject>())
		{
			if (obj != null)
			{
				if ((obj.hideFlags & AffectedFlags) != 0)
				{
					if (!OriginalStates.ContainsKey(obj)) OriginalStates.Add(obj, obj.hideFlags);
					obj.hideFlags &= ~AffectedFlags; //Remove the flags
				}
			}
		}
	}

	[MenuItem("Tools/Show And Edit Objects/GameObjects")]
	public static void ShowAllGameObjects() {
		foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll<GameObject>())
		{
			if (gameObject != null)
			{
				if (!OriginalStates.ContainsKey(gameObject)) OriginalStates.Add(gameObject, gameObject.hideFlags);
				gameObject.hideFlags &= ~AffectedFlags; //Remove the flags
			}
		}
	}

	[MenuItem("Tools/Show And Edit Objects/Cameras")]
	public static void ShowAllCameras() { ShowAllWithComponentOfType<Camera>(); }
	[MenuItem("Tools/Show And Edit Objects/Canvases")]
	public static void ShowAllCanvases() { ShowAllWithComponentOfType<Canvas>(); }
	[MenuItem("Tools/Show And Edit Objects/Lights")]
	public static void ShowAllLights() { ShowAllWithComponentOfType<Light>(); }
	[MenuItem("Tools/Show And Edit Objects/Rigidbodies")]
	public static void ShowAllRigidbodies() { ShowAllWithComponentOfType<Rigidbody>(); }
	[MenuItem("Tools/Show And Edit Objects/Renderers")]
	public static void ShowAllRenderers() { ShowAllWithComponentOfType<Renderer>(); }
	[MenuItem("Tools/Show And Edit Objects/UIBehaviours")]
	public static void ShowAllUIBehaviours() { ShowAllWithComponentOfType<UIBehaviour>(); }
	[MenuItem("Tools/Show And Edit Objects/MonoBehaviours")]
	public static void ShowAllMonoBehaviours() { ShowAllWithComponentOfType<MonoBehaviour>(); }
	[MenuItem("Tools/Show And Edit Objects/Colliders")]
	public static void ShowAllColliders() { ShowAllWithComponentOfType<Collider>(); }

	public static void ShowAllWithComponentOfType<T>() where T : Component
	{
		foreach (Component component in Resources.FindObjectsOfTypeAll<T>())
		{
			if (component != null && component.gameObject != null)
			{
				if (!OriginalStates.ContainsKey(component)) OriginalStates.Add(component, component.hideFlags);
				component.hideFlags &= ~AffectedFlags; //Remove the flags

				if (!OriginalStates.ContainsKey(component.gameObject))
					OriginalStates.Add(component.gameObject, component.gameObject.hideFlags);
				component.gameObject.hideFlags &= ~AffectedFlags; //Remove the flags
			}
		}
	}
}

#endif