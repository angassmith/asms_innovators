﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using Save;
using UnityEngine;

/// <summary>An empty class, which can never be instantiated, but is not static</summary>
//	[DeliberatelyNonSerializable]
public abstract class UninstantiableClass
{
	//It's abstract, so can't be instantiated; and has only a private constructor, so can't be derived from

	//Similar to VoidClass, but has some different uses
	
	private UninstantiableClass() {
		throw new InvalidOperationException("There may never be any instances of UninstantiableClass");
	}
	
	/// <summary>Returns true if obj is null or (somehow) is of type UninstantiableClass</summary>
	public override bool Equals(object obj) {
		return obj == null || (obj is UninstantiableClass);
	}
	
	/// <summary>Returns true</summary>
	public bool Equals(UninstantiableClass other) {
		return true;
	}
	
	/// <summary>Returns true</summary>
	public static bool operator ==(UninstantiableClass left, UninstantiableClass right) {
		return true;
	}
	/// <summary>Returns false</summary>
	public static bool operator !=(UninstantiableClass left, UninstantiableClass right) {
		return false;
	}
	
	/// <summary>Returns true</summary>
	public static bool Equals(UninstantiableClass left, UninstantiableClass right) {
		return true;
	}

	/// <summary>Returns 0</summary>
	public override int GetHashCode() { return 0; }

	/// <summary>Returns "null"</summary>
	public override string ToString() { return "null"; }
}