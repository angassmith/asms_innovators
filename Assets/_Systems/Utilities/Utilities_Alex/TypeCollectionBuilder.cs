﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using System.Collections.ObjectModel;

public class TypeCollectionBuilder {

	private readonly Type[] TypeArr;

	private TypeCollectionBuilder(Type[] typeArr) {
		this.TypeArr = typeArr;
	}

	public static TypeCollectionBuilder Create                                                                       () { return new TypeCollectionBuilder(new Type[] {                                                                                                                                                                                                       }); }
	public static TypeCollectionBuilder Create<T1                                                                   >() { return new TypeCollectionBuilder(new Type[] { typeof(T1)                                                                                                                                                                                            }); }
	public static TypeCollectionBuilder Create<T1, T2                                                               >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2)                                                                                                                                                                                }); }
	public static TypeCollectionBuilder Create<T1, T2, T3                                                           >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3)                                                                                                                                                                    }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4                                                       >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4)                                                                                                                                                        }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5                                                   >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5)                                                                                                                                            }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6                                               >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6)                                                                                                                                }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7                                           >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7)                                                                                                                    }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8                                       >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8)                                                                                                        }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8, T9                                   >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9)                                                                                            }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10                              >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10)                                                                               }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11                         >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11)                                                                  }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12                    >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12)                                                     }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13               >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13)                                        }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14          >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14)                           }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15     >() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14), typeof(T15)              }); }
	public static TypeCollectionBuilder Create<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>() { return new TypeCollectionBuilder(new Type[] { typeof(T1), typeof(T2), typeof(T3), typeof(T4), typeof(T5), typeof(T6), typeof(T7), typeof(T8), typeof(T9), typeof(T10), typeof(T11), typeof(T12), typeof(T13), typeof(T14), typeof(T15), typeof(T16) }); }

	public Type[] AsArray() {
		return TypeArr;
	}
	public List<Type> AsList() {
		return new List<Type>(TypeArr);
	}
	public ReadOnlyCollection<Type> AsReadOnlyCollection() {
		return Array.AsReadOnly(TypeArr);
	}
	public Collection<Type> AsCollection() {
		return new Collection<Type>(TypeArr);
	}

}
