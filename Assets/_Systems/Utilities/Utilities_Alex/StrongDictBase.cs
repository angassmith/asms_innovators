﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

public abstract class StrongDictBase
{
	private readonly bool _canInitStrongEntriesNow = false;
	protected bool CanInitStrongEntriesNow { get { return _canInitStrongEntriesNow; } }

	public abstract int Count { get; }

	public virtual bool IsReadOnly { get { return true; } }

	protected StrongDictBase()
	{
		_canInitStrongEntriesNow = true;
		InitStrongEntries();
		_canInitStrongEntriesNow = false;
	}

	protected abstract void InitStrongEntries();

	protected abstract void Clear();

	protected delegate bool ValueLookupFunc<TKey, TValue>(TKey key, out StrongDictBase<TValue>.ValHolder val);
	protected delegate bool EntryLookupFunc<TKey, TPrivateEntry>(TKey key, out TPrivateEntry val);

	// ------------------------ Short Helper Methods ------------------------

	protected TKey CheckAndCastKey<TKey>(object key) {
		if (key == null) throw new ArgumentNullException("key");
		else if ((key is TKey)) return (TKey)key;
		else throw TypeMismatchException.CreateForKey<TKey>(key);
	}

	protected TValue CheckAndCastValue<TValue>(object value) {
		if (value == null) return (TValue)value; //Can't just do 'return null' here, but this is identical
		else if ((value is TValue)) return (TValue)value;
		else throw TypeMismatchException.CreateForKey<TValue>(value);
	}

	protected TRet IfValidKey<TKey, TRet>(object key, Func<TKey, TRet> func, TRet def) {
		if (func == null) throw new ArgumentNullException("func");
		else if (key == null) throw new ArgumentNullException("key");
		else if ((key is TKey)) return func((TKey)key);
		else return def;
	}
	protected TRet IfValidValue<TValue, TRet>(object value, Func<TValue, TRet> func, TRet def) {
		if (func == null) throw new ArgumentNullException("func");
		else if (value == null) throw new ArgumentNullException("key");
		else if ((value is TValue)) return func((TValue)value);
		else return def;
	}

	protected void IfValidKey<TKey>(object key, Action<TKey> action) {
		if (action == null) throw new ArgumentNullException("action");
		else if (key == null) throw new ArgumentNullException("key");
		else if ((key is TKey)) action((TKey)key);
		else return;
	}
	protected void IfValidValue<TValue>(object value, Action<TValue> action) {
		if (action == null) throw new ArgumentNullException("action");
		else if (value == null) throw new ArgumentNullException("key");
		else if ((value is TValue)) action((TValue)value);
		else return;
	}

	protected InvalidOperationException CreateReadOnlyException()
	{
		return new InvalidOperationException("The StrongDict (of type " + this.GetType() + ") is read-only.");
	}

	protected void ThrowIfReadOnly() {
		if (this.IsReadOnly) throw CreateReadOnlyException();
	}

	// ------------------------ TypeMismatchException ------------------------
	//Currently (Nov 2016) only used in the short helper methods

	[Serializable]
	public class TypeMismatchException : Exception
	{
		public TypeMismatchException() { }
		public TypeMismatchException(string message) : base(message) { }
		public TypeMismatchException(string message, Exception inner) : base(message, inner) { }
		protected TypeMismatchException(
			System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
		public static TypeMismatchException CreateForKey<TKey>(object providedKey) {
			return new TypeMismatchException("The provided key '" + ToString(providedKey) + "' was of type '" + GetTypeName(providedKey) + "' when a key of type '" + typeof(TKey).FullName + "' was needed.");
		}
		public static TypeMismatchException CreateForValue<TValue>(object providedVal) {
			return new TypeMismatchException("The provided value '" + ToString(providedVal) + "' was of type '" + GetTypeName(providedVal) + "' when a value of type '" + typeof(TValue).FullName + "' was needed.");
		}
		private static string ToString(object obj) {
			if (obj == null) return "null";
			if (obj is string && (string)obj == "") return "\"\"";
			else return obj.ToString();
		}
		private static string GetTypeName(object obj) {
			if (obj == null) return "null";
			else return obj.GetType().FullName;
		}
	}
}

public abstract class StrongDictBase<TValue> : StrongDictBase
{

	public abstract ReadOnlyCollection<TValue> Values { get; }
	public abstract bool ContainsValue(TValue value);

	public abstract class ValHolder
	{
		private ValHolder() { }

		public abstract TValue Get();
		public abstract void Set(TValue val);

		public class Strong : ValHolder
		{
			public readonly Func<TValue> Getter;
			public readonly Action<TValue> Setter;
			public Strong(Func<TValue> getter, Action<TValue> setter) {
				if (getter == null) throw new ArgumentNullException("getter");
				if (setter == null) throw new ArgumentNullException("setter");
				this.Getter = getter;
				this.Setter = setter;
			}

			public override TValue Get() { return Getter(); }
			public override void Set(TValue val) { Setter(val); }
		}

		public class Runtime : ValHolder
		{
			public TValue Value;
			public Runtime(TValue value) {
				this.Value = value;
			}

			public override TValue Get() { return Value; }
			public override void Set(TValue val) { Value = val; }
		}
	}
}

//*/