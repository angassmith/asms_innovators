﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;


[Serializable]
public class InvalidSetupException : Exception
{
	public InvalidSetupException() { }
	public InvalidSetupException(string message) : base(message) { }
	public InvalidSetupException(string message, Exception inner) : base(message, inner) { }
	protected InvalidSetupException(
	  System.Runtime.Serialization.SerializationInfo info,
	  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}

//*/