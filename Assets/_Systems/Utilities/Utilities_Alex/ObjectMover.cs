﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections;
using Save;


public class ObjectMoverEditorWindow : EditorWindow {

	[SerializeField]
	public UnityEngine.Object DisplayedObj;
	[SerializeField]
	private bool firstRun = true;

	[MenuItem ("Window/Object Mover")]
	public static void Open()
	{
		ObjectMoverEditorWindow testWin = (ObjectMoverEditorWindow)EditorWindow.GetWindow(typeof(ObjectMoverEditorWindow), false, "Object Mover");
		testWin.minSize = new Vector2(150, 40);
	}
	
	void OnGUI() {
		if (firstRun)
		{
			firstRun = false;
			this.position = new Rect(this.position.x, this.position.y, 200, 40);
		}

		//Show object selection field
		//	DisplayedObj = EditorGUI.ObjectField(new Rect(0, 0, this.position.width, 20), DisplayedObj, typeof(UnityEngine.Object), true);
		DisplayedObj = EditorGUILayout.ObjectField(DisplayedObj, typeof(UnityEngine.Object), true);

		//If the object is not null, show a representation of the object that can be dragged into other fields
		if (DisplayedObj != null)
		{
			//	EditorGUI.InspectorTitlebar(new Rect(0, 20, this.position.width, 20), false, DisplayedObj, false);
			EditorGUILayout.InspectorTitlebar(false, DisplayedObj, false);
		}
	}
}

//	[CustomEditor()]
//	public class ObjectMoverEditor : Editor
//	{
//		public UnityEngine.Object Value;
//	
//		public override void OnInspectorGUI()
//		{
//			Value = EditorGUILayout.ObjectField("Object: ", Value, typeof(UnityEngine.Object), true);
//		}
//	}

#endif