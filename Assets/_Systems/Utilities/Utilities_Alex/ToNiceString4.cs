﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;


public static class NiceString
{
	private static string ToNiceString(IEnumerable ienum, bool jsonFriendly, int recursionLimit, bool showPrivate, bool allowSameTypeRecursion, int recursion, List<object> parents)
	{
		//?//	//Setup json stuff
		//?//	string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		//Setup
		Type type = ienum.GetType();

		//Check for basic type (like string, primitives, decimal, object)
		if (IsBasicType(type))
		{
			return FormatBasicType(ienum, jsonFriendly);
		}

		//Check for byte[]
		if (type == typeof(byte[]))
		{
			return FormatByteArray((byte[])ienum, jsonFriendly);
		}

		//Check recursion limit (types checked above are exempt)
		if (recursion > recursionLimit)
		{
			return FormatError("RL", ienum, jsonFriendly);
		}
		recursion += 1;

		//Check for dictionary
		if (Implements(ienum, typeof(IDictionary)))
		{
			return ToNiceStringDict((IDictionary)ienum, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents);
		}

		//Check for infinite recursion
		for (int i = 0; i < parents.Count; i++)
		{
			if (ReferenceEquals(ienum, parents[i])) { return FormatError("P(" + (parents.Count - i) + ")", ienum, jsonFriendly); }
		}
		//Check for same-type recursion
		if (!allowSameTypeRecursion)
		{
			foreach (object parent in parents)
			{
				if (parent.GetType() == type) { return FormatError("STRec", ienum, jsonFriendly); }
			}
		}
		//Append current object to parents
		parents.Add(ienum);

		//Setup
		string result = "";

		//Enumerate
		foreach (object item in ienum)
		{
			if (Implements(item, typeof(IDictionary))) { result += ToNiceStringDict((IDictionary)item, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents) + ", "; }
			else if (Implements(item, typeof(IEnumerable))) { result += ToNiceString((IEnumerable)item, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents) + ", "; }
			else { result += ToNiceString(item, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents) + ", "; }
		}

		//Finish
		result = result.Trim(' ', ',');
		if (result == "") { result = "[]"; }
		else { result = "[ " + result + " ]"; }

		//Remove current object from parents
		parents.RemoveAt(parents.Count - 1);

		return result;
	}





	private static string ToNiceStringDict(IDictionary idict, bool jsonFriendly, int recursionLimit, bool showPrivate, bool allowSameTypeRecursion, int recursion, List<object> parents)
	{
		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		//Check recursion limit
		if (recursion > recursionLimit)
		{
			return FormatError("RL", idict, jsonFriendly);
		}
		recursion += 1;

		//Check for infinite recursion
		for (int i = 0; i < parents.Count; i++)
		{
			if (ReferenceEquals(idict, parents[i])) { return FormatError("P(" + (parents.Count - i) + ")", idict, jsonFriendly); }
		}
		//Check for same-type recursion
		if (!allowSameTypeRecursion)
		{
			foreach (object parent in parents)
			{
				if (parent.GetType() == idict.GetType()) { return FormatError("STR", idict, jsonFriendly); }
			}
		}
		//Append current object to parents
		parents.Add(idict);

		//Setup
		string result = "";

		//Keys
		List<string> keyRes_s = new List<string>();
		foreach (object key in idict.Keys)
		{
			if (Implements(key, typeof(IDictionary))) { keyRes_s.Add(ToNiceStringDict((IDictionary)key, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents)); }
			else if (Implements(key, typeof(IEnumerable))) { keyRes_s.Add(ToNiceString((IEnumerable)key, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents)); }
			else { keyRes_s.Add(ToNiceString(key, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents)); }
		}

		//Values
		List<string> valRes_s = new List<string>();
		foreach (object value in idict.Values)
		{
			if (Implements(value, typeof(IDictionary))) { valRes_s.Add(ToNiceStringDict((IDictionary)value, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents)); }
			else if (Implements(value, typeof(IEnumerable))) { valRes_s.Add(ToNiceString((IEnumerable)value, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents)); }
			else { valRes_s.Add(ToNiceString(value, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents)); }
		}

		//Note: Can't just iterate over the KeyValuePairs in the dictionary,
		//as the dictionary just returns Objects when iterated
			
		//Combine
		if (keyRes_s.Count != valRes_s.Count) { return FormatError("Err", idict, jsonFriendly); }
		for (var i = 0; i < keyRes_s.Count; i++)
		{
			result += "{" + jq + "key" + jq + ":" + keyRes_s[i] + ", " + jq + "value" + jq + ":" + valRes_s[i] + "}, ";
		}

		//Finish
		result = result.Trim(' ', ',');
		if (result == "") { result = "[]"; }
		else { result = "[ " + result + " ]"; }

		//Remove current object from parents
		parents.RemoveAt(parents.Count - 1);

		return result;
	}





	/// <summary>Check if an object's type implements an interface</summary>
	public static bool Implements(object obj, Type iface)
	{
		return obj.GetType().GetInterfaces().Contains(iface);
	}

	/// <summary>Check if an object's type implements an interface</summary>
	public static bool Implements(Type type, Type iface)
	{
		return type.GetInterfaces().Contains(iface);
	}



	///<summary>Turn an object into a nice string. Some more complex types (eg certain IEnumerables, certain Streams) may cause errors</summary>
	public static string ToNiceString(this object obj, bool jsonFriendly = false, int recursionLimit = 10, bool showPrivate = false, bool allowSameTypeRecursion = false)
	{
		return ToNiceString(obj, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, 0, new List<object>());
	}

	private static string ToNiceString(object obj, bool jsonFriendly, int recursionLimit, bool showPrivate, bool allowSameTypeRecursion, int recursion, List<object> parents)
	{
		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		//Setup
		if (obj == null) { return "null"; }
		Type type = obj.GetType();

		//Check for basic type
		if (IsBasicType(type))
		{
			return FormatBasicType(obj, jsonFriendly);
		}

		//Check for other types
		{
			MemoryStream objAsMemStream = obj as MemoryStream;
			if (objAsMemStream != null)
			{
				return FormatMemoryStream(objAsMemStream, jsonFriendly);
			}

			Stream objAsStream = obj as Stream;
			if (objAsStream != null)
			{
				return FormatError("Stream", obj, jsonFriendly); //This might be improved in future
			}

			Type objAsType = obj as Type;
			if (objAsType != null)
			{
				return FormatType(objAsType, jsonFriendly);
			}
		}

		//Check recursion limit (types checked above are exempt)
		if (recursion > recursionLimit)
		{
			return FormatError("RL", obj, jsonFriendly);
		}
		recursion += 1;

		//Check for IEnumerable
		if (Implements(obj, typeof(IEnumerable)))
		{
			return ToNiceString((IEnumerable)obj, jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents);
		}

		//Check for infinite recursion
		for (int i = 0; i < parents.Count; i++)
		{
			if (ReferenceEquals(obj, parents[i])) { return FormatError("P(" + (parents.Count - i) + ")", obj, jsonFriendly); }
		}
		//Check for same-type recursion
		if (!allowSameTypeRecursion)
		{
			foreach (object parent in parents)
			{
				if (parent.GetType() == type) { return FormatError("STR", obj, jsonFriendly); }
			}
		}
		//Append current object to parents
		parents.Add(obj);

		//Setup 2
		string res = "";
		string publicRes = "";
		string privateRes = "";


		//Public properties
		PropertyInfo[] publicPInfos = type.GetProperties();
		List<string> publicXInfoNames = new List<string>();
		foreach (PropertyInfo pinfo in publicPInfos)
		{
			try
			{
				publicRes += jq + pinfo.Name + jq + ": " + ToNiceString(pinfo.GetValue(obj, null), jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion,  recursion, parents) + ", ";
			}
			catch
			{
				publicRes += jq + pinfo.Name + jq + ": " + jq + "<Err>" + jq + ", ";
			}
			publicXInfoNames.Add(pinfo.Name);
		}
		//Public fields
		FieldInfo[] publicFInfos = type.GetFields();
		foreach (var finfo in publicFInfos)
		{
			publicRes += jq + finfo.Name + jq + ": " + ToNiceString(finfo.GetValue(obj), jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents) + ", ";
		}
		//Finish public stuff
		publicRes = (publicRes.EndsWith(", ")) ? publicRes.Substring(0, publicRes.Length - 2) : publicRes; //Remove last comma
		if (publicRes.Length == 0)
		{
			publicRes = " ";
		}
		else
		{
			publicRes = " " + publicRes + " ";
		}


		if (showPrivate)
		{
			//Non-public properties
			BindingFlags nonPublicBindingFlags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
			PropertyInfo[] privatePInfos = type.GetProperties(nonPublicBindingFlags);
			foreach (PropertyInfo pinfo in privatePInfos)
			{
				if (!publicXInfoNames.Contains(pinfo.Name))
				{
					try
					{
						privateRes += jq + pinfo.Name + jq + ": " + ToNiceString(pinfo.GetValue(obj, null), jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents) + ", ";
					}
					catch
					{
						privateRes += jq + pinfo.Name + jq + ": " + jq + "<Err>" + jq + ", ";
					}
				}
			}
			//Non-public fields
			FieldInfo[] privateFInfos = type.GetFields(nonPublicBindingFlags);
			foreach (var finfo in privateFInfos)
			{
				if (!publicXInfoNames.Contains(finfo.Name))
				{
					privateRes += jq + finfo.Name + jq + ": " + ToNiceString(finfo.GetValue(obj), jsonFriendly, recursionLimit, showPrivate, allowSameTypeRecursion, recursion, parents) + ", ";
				}
			}
			//Finish non-public stuff
			privateRes = (privateRes.EndsWith(", ")) ? privateRes.Substring(0, privateRes.Length - 2) : privateRes; //Remove last comma
			if (privateRes.Length == 0)
			{
				privateRes = " ";
			}
			else
			{
				privateRes = " " + privateRes + " ";
			}
		}
		else
		{
			privateRes = " ";
		}

		//Finish
		if      (publicRes != " " && privateRes != " ") { res = "{ " + jq + "[Public]" + jq + ": {" + publicRes + "}, " + jq + "[Hidden]" + jq + ": {" + privateRes + "} }"; }
		else if (publicRes != " " && privateRes == " ") { res = "{" + publicRes + "}"; }
		else if (publicRes == " " && privateRes != " ") { res = "{ " + jq + "[Hidden]" + jq + ": {" + privateRes + "} }"; }
		else if (publicRes == " " && privateRes == " ") { res = "{ }"; }

		//Remove current object from parents
		parents.RemoveAt(parents.Count - 1);

		return res;
	}





	public static string FormatBasicType(object obj, bool jsonFriendly)
	{
		if (obj == null) return "null";

		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		Type type = obj.GetType();
		if (type == typeof(sbyte  )) return jq + "sbyte:"   + obj.ToString() + jq;
		if (type == typeof(byte   )) return jq + "byte:" + ((byte)obj).ToString("X2") + jq;
		if (type == typeof(int    )) return obj.ToString();
		if (type == typeof(uint   )) return jq + "uint:"    + obj.ToString() + jq;
		if (type == typeof(short  )) return jq + "short:"   + obj.ToString() + jq;
		if (type == typeof(ushort )) return jq + "ushort:"  + obj.ToString() + jq;
		if (type == typeof(long   )) return jq + "long:"    + obj.ToString() + jq;
		if (type == typeof(ulong  )) return jq + "ulong:"   + obj.ToString() + jq;
		if (type == typeof(float  )) return FormatFloat((float)obj, jsonFriendly);
		if (type == typeof(double )) return jq + "double:"  + obj.ToString() + jq;
		if (type == typeof(char   )) return jq + "char:"    + obj.ToString() + jq;
		if (type == typeof(bool   )) return (bool)obj ? (jsonFriendly ? "true" : "True") : (jsonFriendly ? "false" : "False");
		if (type == typeof(object )) return jq + "object:"  + obj.ToString() + jq;
		if (type == typeof(string )) return "\"" + obj.ToString() + "\"";
		if (type == typeof(decimal)) return jq + "decimal:" + obj.ToString() + jq;
		if (type.IsEnum            ) return FormatEnum((Enum)obj, jsonFriendly);
		return FormatError("Err", obj, jsonFriendly);
	}

	public static bool IsBasicType(Type type)
	{
		if (type == null) return false;
		if (type.IsPrimitive || type == typeof(string) || type == typeof(object) || type == typeof(decimal) || type.IsEnum)
			return true;
		else
			return false;
	}


	public static string FormatFloat(float val, bool jsonFriendly)
	{
		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		//Special values
		if      (float.IsPositiveInfinity(val)) return jq + "float.Infinity" + jq;
		else if (float.IsNegativeInfinity(val)) return jq + "float.NegativeInfinity" + jq;
		else if (float.IsNaN(val)             ) return jq + "float.NaN" + jq;

		//Normal values
		string asStr = val.ToString();
		if (asStr.Contains('e') || asStr.Contains('E')) return asStr;
		else return asStr + "e+1";
		//Always use scientific-notation, to indicate that it's a float
		//Doesn't need json quotes
	}


	public static string FormatEnum(Enum val, bool jsonFriendly)
	{
		if (val == null) return "null";

		if (jsonFriendly) return "\"enum:" + FormatCSharpType(val.GetType()) + "." + val.ToString() + "\"";
		return FormatCSharpType(val.GetType()) + "." + val.ToString();
	}



	public static string FormatByteArray(byte[] byteArr, bool jsonFriendly)
	{
		if (byteArr == null) return "null";

		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		string result = "";
		foreach (byte item in byteArr)
		{
			result += item.ToString("X2") + "-";
		}
		result = jq + "ByteArray:" + result + jq;
		return result;
	}




	/// <summary>
	/// Converts the stream to a byte array, then returns all bytes in the array (in hex form) separated by dashes, prepended with "MemoryStream:", and surrounded with double quotes if jsonFriednly is true
	/// </summary>
	public static string FormatMemoryStream(MemoryStream stream, bool jsonFriendly)
	{
		if (stream == null) return "null";

		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		string result = "";
		byte[] byteArr = stream.ToArray();
		foreach (byte item in byteArr)
		{
			result += item.ToString("X2") + "-";
		}
		result = jq + "MemoryStream:" + result + jq;
		return result;
	}



	/// <summary>
	/// Returns "Type:" + FormatCSharpType(type), surrounded in double quotes if jsonFriendly is true, or "null" if type is null;
	/// </summary>
	public static string FormatType(Type type, bool jsonFriendly)
	{
		if (type == null) return "null";

		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		return jq + "Type:" + FormatCSharpType(type) + jq;
	}

	/// <summary>
	/// Returns FormatCSharpType(obj.GetType()) + " '" + obj.ToString() + "'" (surrounded by double quotes if jsonFriendly is true) if the object is not null; otherwise returns "null"
	/// </summary>
	public static string FormatObjSimple(object obj, bool jsonFriendly)
	{
		if (obj == null) return "null";
			
		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		return jq + FormatCSharpType(obj.GetType()) + " '" + obj.ToString() + "'" + jq;
	}

	private static string FormatError(string errorAbbr, object target, bool jsonFriendly)
	{
		//Setup json stuff
		string jq = jsonFriendly ? "\"" : ""; //JsonQuote

		return jq + "<" + errorAbbr + ": " + FormatObjSimple(target, jsonFriendly) + ">" + jq;
	}

	/// <summary>
	/// Returns a string that represents the type the way it would be represented in C#. [may or may not work properly]
	/// </summary>
	public static string FormatCSharpType(Type type)
	{
		if (type == null) return "null";

		if (type.IsGenericParameter) return type.Name;

		string res = "";

		//Namespace
		res += type.Namespace;

		//Type name, taking into account nesting, and including generics
		; ; ; {
			List<Type> nestedHeirachy = new List<Type>();
			Type current = type;
			while (current != null)
			{
				nestedHeirachy.Add(current);
				if (current.IsNested) {
					current = type.DeclaringType;
				} else {
					current = null;
					break;
				}
			}

			nestedHeirachy.Reverse();
			foreach (Type t in nestedHeirachy)
			{
				//Type name with generics
				res += "." + FormatGenericTypeName(t);
			}
		}

		//Get rid of possible initial period (and get rid of multiple if there's multiple for some reason)
		res = res.TrimStart('.');

		return res;
	}

	public static string FormatGenericTypeName(Type type)
	{
		if (type == null) return "null";

		string res = "";

		//Name
		int typeNameEndIndex = type.Name.IndexOfAny(new char[] { '`', '[' });
		res += type.Name.Substring(0, typeNameEndIndex >= 0 ? typeNameEndIndex : type.Name.Length);

		//Generic arguments
		Type[] genericArgs = type.GetGenericArguments();
		if (genericArgs.Length != 0) {
			res += "<";
			foreach (Type arg in genericArgs) {
				res += FormatCSharpType(arg) + ", ";
			}
			res = res.Trim(' ', ',');
			res += ">";
		}

		return res;
	}
		


		

}