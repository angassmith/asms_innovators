﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MiscCollections
{
	public static class EnumerableValidator
	{
		public static IEnumerable<T> ValidateItems<T>(this IEnumerable<T> source, Func<T, bool> isValidPredicate, Func<T, int, string, Exception> exceptionFactory, string validationName = null)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (isValidPredicate == null) throw new ArgumentNullException("isValidPredicate");
			if (exceptionFactory == null) throw new ArgumentNullException("exceptionFactory");

			int i = 0;
			foreach (T item in source)
			{
				if (!isValidPredicate(item))
				{
					string message = (
						"Element " + i + " "
						+ "of IEnumerable<" + typeof(T).FullName + "> "
						+ "of type '" + source.GetType() + "' "
						+ "valid to pass validator predicate "
						+ (string.IsNullOrEmpty(validationName) ? "" : "'" + validationName + "'")
						+ "."
					);

					throw exceptionFactory.Invoke(item, i, message);
				}

				i++;
			}

			return source;
		}

		public static IEnumerable<T> ValidateItems<T>(this IEnumerable<T> source, Func<T, bool> isValidPredicate, Func<string, Exception> exceptionFactory, string validationName = null) {
			return ValidateItems(source, isValidPredicate, (item, i, message) => exceptionFactory.Invoke(message), validationName);
		}

		public static IEnumerable<T> ValidateItems<T>(this IEnumerable<T> source, Func<T, bool> isValidPredicate, string validationName = null) {
			return ValidateItems(source, isValidPredicate, (item, i, message) => new InvalidOperationException(message), validationName);
		}



		public static IEnumerable<T> ValidateItemsNonNull<T>(this IEnumerable<T> source, Func<T, int, string, Exception> exceptionFactory) {
			return ValidateItems(source, x => x != null, exceptionFactory, "non-null");
		}

		public static IEnumerable<T> ValidateItemsNonNull<T>(this IEnumerable<T> source, Func<string, Exception> exceptionFactory) {
			return ValidateItems(source, x => x != null, exceptionFactory, "non-null");
		}

		public static IEnumerable<T> ValidateItemsNonNull<T>(this IEnumerable<T> source) {
			return ValidateItems(source, x => x != null, "non-null");
		}


		public static IEnumerable<string> ValidateItemsNonNullOrEmpty(this IEnumerable<string> source, Func<string, int, string, Exception> exceptionFactory) {
			return ValidateItems(source, x => !string.IsNullOrEmpty(x), exceptionFactory, "non-null");
		}

		public static IEnumerable<string> ValidateItemsNonNullOrEmpty(this IEnumerable<string> source, Func<string, Exception> exceptionFactory) {
			return ValidateItems(source, x => !string.IsNullOrEmpty(x), exceptionFactory, "non-null");
		}

		public static IEnumerable<string> ValidateItemsNonNullOrEmpty(this IEnumerable<string> source) {
			return ValidateItems(source, x => !string.IsNullOrEmpty(x), "non-null");
		}

		//Scrapped idea - making the validator delegate throw an exception for invalid items
		//This just makes calling the validate functions much more verbose
		//
		//	public static IEnumerable<T> ValidateItems<T>(this IEnumerable<T> source, Action<T> validator, Func<T, int, string, Exception, Exception> exceptionFactory)
		//	{
		//		if (source == null) throw new ArgumentNullException("source");
		//		if (validator == null) throw new ArgumentNullException("isValidPredicate");
		//		if (exceptionFactory == null) throw new ArgumentNullException("exceptionFactory");
		//	
		//		int i = 0;
		//		foreach (T item in source)
		//		{
		//			try
		//			{
		//				validator(item);
		//			}
		//			catch (Exception e)
		//			{
		//	
		//				string message = (
		//					"Element " + i + " "
		//					+ "of IEnumerable<" + typeof(T).FullName + "> "
		//					+ "of type '" + source.GetType() + "' "
		//					+ "failed to pass the provided validator."
		//				);
		//	
		//				throw exceptionFactory.Invoke(item, i, message, e);
		//			}
		//	
		//			i++;
		//		}
		//	
		//		return source;
		//	}
		//	
		//	public static IEnumerable<T> ValidateItems<T>(this IEnumerable<T> source, Action<T> validator, Func<string, Exception, Exception> exceptionFactory) {
		//		return ValidateItems(source, validator, (item, i, message, innerEx) => exceptionFactory.Invoke(message, innerEx));
		//	}
		//	
		//	public static IEnumerable<T> ValidateItems<T>(this IEnumerable<T> source, Action<T> validator) {
		//		return ValidateItems(source, validator, (item, i, message, innerEx) => new InvalidOperationException(message, innerEx));
		//	}
		//	
		//	
		//	
		//	public static IEnumerable<T> ValidateItemsNonNull<T>(this IEnumerable<T> source, Func<T, int, string, Exception, Exception> exceptionFactory) {
		//		return ValidateItems(source, x => { if (x == null) throw new InvalidOperationException("Element of sequence was null."); }, exceptionFactory);
		//	}
	}
}

//*/