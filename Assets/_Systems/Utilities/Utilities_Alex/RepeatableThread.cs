﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Threading;

public abstract class RepeatableThreadBase : IDisposable
{
	private volatile bool _disposing;
	private volatile bool _disposed;
	private readonly Thread WorkerThread;
	private volatile bool _running = false;
	private bool _halfRunning = false;
	private volatile AutoResetEvent validJoinTime = new AutoResetEvent(false);
	private volatile AutoResetEvent runNow        = new AutoResetEvent(false); //DIFFERENCE

	///<summary>Gets a value indicating whether the RepeatableThread is currently disposing</summary>
	public bool Disposing { get { return _disposing; } }
	///<summary>Gets a value indicating whether the RepeatableThread has been disposed</summary>
	public bool Disposed { get { return _disposed; } }
	///<summary>Gets a value indicating whether the RepeatableThread is running</summary>
	public bool Running { get { return _running; } }
	///<summary>Gets a value indicating whether the RepeatableThread is running at all (running or starting/stopping)</summary>
	public bool HalfRunning { get { return _running || _halfRunning; } }

	///<summary>Gets or sets a value indicating the name of the RepeatableThread</summary>
	public string Name { get { return WorkerThread.Name; } set { WorkerThread.Name = value; } }
	///<summary>Gets or sets a value indicating the scheduling priority of the RepeatableThread</summary>
	public ThreadPriority Priority { get { return WorkerThread.Priority; } set { WorkerThread.Priority = value; } }

	protected abstract Action MethodRunner { get; }

	internal RepeatableThreadBase(bool methodEqualsNull)
	{
		if (methodEqualsNull) throw new ArgumentNullException("method", "The method cannot be null");

		WorkerThread = new Thread(Worker);
		WorkerThread.Start(); //DIFFERENCE
	}

	///<summary>Should be called first by RunTask() methods, before copying the arguments</summary>
	/// <exception cref="RepeatableThreadRunningStateException"></exception>
	protected void RunTaskPart1_RunningStateCheckAndSet()
	{
		if (_halfRunning || _running) throw new RepeatableThreadRunningStateException(true, "The RepeatableThread's worker thread is already running or starting a task");
		_halfRunning = true;
	}
	///<summary>Should be called last by RunTask() methods, after copying the arguments</summary>
	protected void RunTaskPart3_StartAndRunningStateSet()
	{
		_running = true;
		runNow.Set(); //Prompt waiting thread to run again
		_halfRunning = false;
	}

	///<summary>Disposes of the RepeatableThread once the current task has finished</summary>
	public void Dispose()
	{
		_disposing = true;
	}

	///<summary>Abort()s the thread, stopping the current task, and disposes of the RepeatableThread</summary>
	public void Kill()
	{
		_disposing = true;
		WorkerThread.Abort();
		_disposed = true;
		_disposing = false;
	}

	///<summary>Interrupt()s the thread, if it is running</summary>
	///<exception cref="RepeatableThreadRunningStateException"></exception>
	public void InterruptTask()
	{
		if (_running) WorkerThread.Interrupt();
		else throw new RepeatableThreadRunningStateException(false, "The thread must be running in order to interrupt it");
	}

	///<summary>Returns when the thread stops running</summary>
	public void JoinTask()
	{
		validJoinTime.WaitOne();
	}
	///<summary>Returns when the thread stops running, or at the end of the timeout. Returns true if the thread stopped running within the timeout</summary>
	public bool JoinTask(int millisecondsTimeout)
	{
		return validJoinTime.WaitOne(millisecondsTimeout);
	}
	///<summary>Returns when the thread stops running, or at the end of the timeout. Returns true if the thread stopped running within the timeout</summary>
	public bool JoinTask(TimeSpan timeout)
	{
		return validJoinTime.WaitOne(timeout);
	}

	protected void Worker()
	{
		while (!_disposing)
		{
			//Wait until interrupted
			//	try
			//	{
			//		Thread.Sleep(Timeout.Infinite);
			//	} catch (ThreadInterruptedException) { } //Not really an exception - thrown when the sleep is interrupted
			runNow.WaitOne(); //DIFFERENCE

			if (_disposing) break;

			_running = true;

			//Run method
			MethodRunner();

			_running = false;
			validJoinTime.Set();
		}

		_disposed = true;
		_disposing = false;
	}
}

/// <summary>
/// The exception that is thrown when a RepeatableThread's running/not state makes the given operation invalid
/// </summary>
public class RepeatableThreadRunningStateException : Exception
{
	public readonly bool running;
	public RepeatableThreadRunningStateException(bool running, string message) : base(message) {
		this.running = running;
	}
	public RepeatableThreadRunningStateException(bool running, string message, Exception innerException) : base(message, innerException) {
		this.running = running;
	}
}



public class RepeatableThread<T1, T2, T3, T4> : RepeatableThreadBase {

	private readonly Action<T1, T2, T3, T4> method;
	private T1 arg1;
	private T2 arg2;
	private T3 arg3;
	private T4 arg4;

	protected override Action MethodRunner {
		get { return () => method(arg1, arg2, arg3, arg4); }
	}

	///<summary>Creates a new repeatable thread, that will execute the specified method with different arguments each repetition</summary>
	///<exception cref="ArgumentNullException"></exception>
	public RepeatableThread(Action<T1, T2, T3, T4> method) : base(method == null)
	{
		this.method = method;
	}

	///<summary>If the thread is not running or starting a task, starts a task on the thread</summary>
	///<exception cref="RepeatableThreadRunningStateException"></exception>
	public void RunTask(T1 arg1, T2 arg2, T3 arg3, T4 arg4)
	{
		RunTaskPart1_RunningStateCheckAndSet();

		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;
		this.arg4 = arg4;

		RunTaskPart3_StartAndRunningStateSet();
	}
}



public class RepeatableThread<T1, T2, T3> : RepeatableThreadBase {

	private readonly Action<T1, T2, T3> method;
	private T1 arg1;
	private T2 arg2;
	private T3 arg3;

	protected override Action MethodRunner {
		get { return () => method(arg1, arg2, arg3); }
	}

	///<summary>Creates a new repeatable thread, that will execute the specified method with different arguments each repetition</summary>
	///<exception cref="ArgumentNullException"></exception>
	public RepeatableThread(Action<T1, T2, T3> method) : base(method == null)
	{
		this.method = method;
	}

	///<summary>If the thread is not running or starting a task, starts a task on the thread</summary>
	///<exception cref="RepeatableThreadRunningStateException"></exception>
	public void RunTask(T1 arg1, T2 arg2, T3 arg3)
	{
		RunTaskPart1_RunningStateCheckAndSet();

		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;

		RunTaskPart3_StartAndRunningStateSet();
	}
}



public class RepeatableThread<T1, T2> : RepeatableThreadBase {

	private readonly Action<T1, T2> method;
	private T1 arg1;
	private T2 arg2;

	protected override Action MethodRunner {
		get { return () => method(arg1, arg2); }
	}

	///<summary>Creates a new repeatable thread, that will execute the specified method with different arguments each repetition</summary>
	///<exception cref="ArgumentNullException"></exception>
	public RepeatableThread(Action<T1, T2> method) : base(method == null)
	{
		this.method = method;
	}

	///<summary>If the thread is not running or starting a task, starts a task on the thread</summary>
	///<exception cref="RepeatableThreadRunningStateException"></exception>
	public void RunTask(T1 arg1, T2 arg2)
	{
		RunTaskPart1_RunningStateCheckAndSet();

		this.arg1 = arg1;
		this.arg2 = arg2;

		RunTaskPart3_StartAndRunningStateSet();
	}
}



public class RepeatableThread<T1> : RepeatableThreadBase {

	private readonly Action<T1> method;
	private T1 arg1;

	protected override Action MethodRunner {
		get { return () => method(arg1); }
	}

	///<summary>Creates a new repeatable thread, that will execute the specified method with different arguments each repetition</summary>
	///<exception cref="ArgumentNullException"></exception>
	public RepeatableThread(Action<T1> method) : base(method == null)
	{
		this.method = method;
	}

	///<summary>If the thread is not running or starting a task, starts a task on the thread</summary>
	///<exception cref="RepeatableThreadRunningStateException"></exception>
	public void RunTask(T1 arg1)
	{
		RunTaskPart1_RunningStateCheckAndSet();

		this.arg1 = arg1;

		RunTaskPart3_StartAndRunningStateSet();
	}
}



public class RepeatableThread : RepeatableThreadBase {

	private readonly Action method;

	protected override Action MethodRunner {
		get { return () => method(); }
	}

	///<summary>Creates a new repeatable thread, that will execute the specified method with different arguments each repetition</summary>
	///<exception cref="ArgumentNullException"></exception>
	public RepeatableThread(Action method) : base(method == null)
	{
		this.method = method;
	}

	///<summary>If the thread is not running or starting a task, starts a task on the thread</summary>
	///<exception cref="RepeatableThreadRunningStateException"></exception>
	public void RunTask()
	{
		RunTaskPart1_RunningStateCheckAndSet();

		RunTaskPart3_StartAndRunningStateSet();
	}
}









//Example code from earlier (in case modifications have made it not work):
//	public class RepeatableThread<T1, T2> : IDisposable {
//	
//		private volatile bool _disposing;
//		private volatile bool _disposed;
//		private readonly Action<T1, T2> _method;
//		private T1 _arg1;
//		private T2 _arg2;
//		private readonly Thread WorkerThread;
//		private volatile bool _running = false;
//		private bool _halfRunning = false;
//		private volatile AutoResetEvent validJoinTime = new AutoResetEvent(false);
//		private volatile AutoResetEvent runNow        = new AutoResetEvent(false);
//	
//		///<summary>Gets a value indicating whether the RepeatableThread is currently disposing</summary>
//		public bool Disposing { get { return _disposing; } }
//		///<summary>Gets a value indicating whether the RepeatableThread has been disposed</summary>
//		public bool Disposed { get { return _disposed; } }
//		///<summary>Gets a value indicating whether the RepeatableThread is running</summary>
//		public bool Running { get { return _running; } }
//		///<summary>Gets a value indicating whether the RepeatableThread is running at all, that is, running or starting/stopping</summary>
//		public bool HalfRunning { get { return _running || _halfRunning; } }
//	
//		///<summary>Gets or sets a value indicating the name of the RepeatableThread</summary>
//		public string Name { get { return WorkerThread.Name; } set { WorkerThread.Name = value; } }
//		///<summary>Gets or sets a value indicating the scheduling priority of the RepeatableThread</summary>
//		public ThreadPriority Priority { get { return WorkerThread.Priority; } set { WorkerThread.Priority = value; } }
//	
//		///<summary>Creates a new repeatable thread, that will execute the specified method with different arguments each repetition</summary>
//		///<exception cref="ArgumentNullException"></exception>
//		public RepeatableThread(Action<T1, T2> method)
//		{
//			if (method == null) throw new ArgumentNullException("method", "The method cannot be null");
//			
//			WorkerThread = new Thread(Worker);
//			_method = method;
//			WorkerThread.Start();
//		}
//	
//		///<summary>If the thread is not running or starting a task, starts a task on the thread</summary>
//		///<exception cref="RepeatableThreadRunningStateException"></exception>
//		public void RunTask(T1 arg1, T2 arg2)
//		{
//			if (_halfRunning || _running) throw new RepeatableThreadRunningStateException(true, "The RepeatableThread's worker thread is already running or starting a task");
//			_halfRunning = true;
//	
//			_arg1 = arg1;
//			_arg2 = arg2;
//	
//			runNow.Set();
//			_halfRunning = false;
//		}
//	
//		///<summary>Disposes of the RepeatableThread once the current task has finished</summary>
//		public void Dispose()
//		{
//			_disposing = true;
//		}
//	
//		///<summary>Abort()s the thread, stopping the current task, and disposes of the RepeatableThread</summary>
//		public void Kill()
//		{
//			_disposing = true;
//			WorkerThread.Abort();
//			_disposed = true;
//			_disposing = false;
//		}
//	
//		///<summary>Interrupt()s the thread, if it is running</summary>
//		///<exception cref="RepeatableThreadRunningStateException"></exception>
//		public void InterruptTask()
//		{
//			if (_running) WorkerThread.Interrupt();
//			else throw new RepeatableThreadRunningStateException(false, "The thread must be running in order to interrupt it");
//		}
//	
//		///<summary>Returns when the thread stops running</summary>
//		public void JoinTask()
//		{
//			validJoinTime.WaitOne();
//		}
//		///<summary>Returns when the thread stops running, or after the timeout. Returns true if the thread stopped running within the timeout</summary>
//		public bool JoinTask(int millisecondsTimeout)
//		{
//			return validJoinTime.WaitOne(millisecondsTimeout);
//		}
//		///<summary>Returns when the thread stops running, or after the timeout. Returns true if the thread stopped running within the timeout</summary>
//		public bool JoinTask(TimeSpan timeout)
//		{
//			return validJoinTime.WaitOne(timeout);
//		}
//	
//		private void Worker()
//		{
//			while (!_disposing)
//			{
//				runNow.WaitOne();
//	
//				if (_disposing) break;
//	
//				_running = true;
//	
//				//Run method
//				_method(_arg1, _arg2); //Note: _method is never null (readonly, and the constructor does a null check)
//	
//				_running = false;
//				validJoinTime.Set();
//			}
//			
//			_disposed = true;
//			_disposing = false;
//		}
//	}

//*/