﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


[Serializable]
public class ImpossibleSwitchException : Exception
{
	public ImpossibleSwitchException() : base("All possible cases in a switch statement failed.") { }
	public ImpossibleSwitchException(object val) : base("All possible cases in a switch statement failed. The actual value was '" + val + "'.") { }
	protected ImpossibleSwitchException(
	  SerializationInfo info,
	  StreamingContext context) : base(info, context) { }
}

//*/