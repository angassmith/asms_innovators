﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

namespace FixedLengthArrays
{

	public abstract class FSArrBase
	{
		public abstract bool IsZeroSize { get; }
		public abstract long TotalSlots { get; }
		public abstract long[] Dimensions { get; }

		private static int Round(double x) {
			return (int)(Math.Round(x) + 0.1);
			//+0.1 avoids any floating point errors (eg. Math.Round() might (idk) give 5.9999999,
			//which might cast to 5 or 6 (idk which)).
		}

		public abstract class NumBase
		{
			public abstract long GetValue();
			public abstract int GetRadix();
		} 

		public abstract class NumBase<TDerived> : NumBase
			where TDerived : NumBase<TDerived>, new()
		{
			protected abstract int InitGetRadix();
			protected abstract long InitGetValue();

			private static readonly long _value;
			public static long Value { get { return _value; } }
			public override long GetValue() { return _value; }

			private static readonly int _radix;
			public static int Radix { get { return _radix; } }
			public override int GetRadix() { return _radix; }

			static NumBase()
			{
				TDerived inst = new TDerived();
				_radix = inst.InitGetRadix();
				_value = inst.InitGetValue();
			}
		}

		public abstract class MultiDigitNumBase<TDerived> : NumBase<TDerived>
			where TDerived : MultiDigitNumBase<TDerived>, new()
		{
			protected abstract NumBase[] InitGetDigitsDescendingSignificance();

			/// <summary>
			/// The highest radix of a digit in Digits, to the power of Digits.Count
			/// </summary>
			protected sealed override int InitGetRadix() {
				return Round(Math.Pow(_digits.Aggregate(0, (maxBaseSoFar, cur) => Math.Max(maxBaseSoFar, cur.GetRadix())), _digits.Count));
			}
			protected sealed override long InitGetValue() {
				ReadOnlyCollection<NumBase> digits = _digits;
				int radix = Radix;

				long value = 0;
				for (int i = 0; i < digits.Count; i++) {
					value += digits[i].GetValue() * Round(Math.Pow(radix, i));
				}
				return value;
			}

			private static readonly ReadOnlyCollection<NumBase> _digits;
			public static ReadOnlyCollection<NumBase> Digits { get { return _digits; } }
			public ReadOnlyCollection<NumBase> GetDigits() { return _digits; }

			public static int DigitCount { get { return _digits.Count; } }
			public int GetDigitCount() { return _digits.Count; }

			static MultiDigitNumBase()
			{
				TDerived inst = new TDerived();
				_digits = Array.AsReadOnly(inst.InitGetDigitsDescendingSignificance());
			}
		}
		
		public abstract class DecDigit<TDerived> : NumBase<TDerived>
			where TDerived : DecDigit<TDerived>, new()
		{
			protected sealed override int InitGetRadix() { return 10; }
		}
		public abstract class HexDigit<TDerived> : NumBase<TDerived>
			where TDerived : HexDigit<TDerived>, new()
		{
			protected sealed override int InitGetRadix() { return 16; }
		}
	}

	public abstract class FSArrBase<X, Y, Z, W, TItem> : FSArrBase
		where X : FSArrBase.NumBase, new()
		where Y : FSArrBase.NumBase, new()
		where Z : FSArrBase.NumBase, new()
		where W : FSArrBase.NumBase, new()
	{
		private readonly TItem[] singleDimArr;
		private readonly TItem[,] twoDimArr;
		private readonly TItem[,,] threeDimArr;
		private readonly TItem[,,,] fourDimArr;

		public sealed override bool IsZeroSize { get { return activeDims.Length == 0; } }
		public sealed override long TotalSlots {
			get {
				return activeDims.Aggregate((long)0, (totalSoFar, cur) => totalSoFar * cur.LengthInfo.GetValue());
			}
		}

		private readonly long _xlen;
		private readonly long _ylen;
		private readonly long _zlen;
		private readonly long _wlen;
		public sealed override long[] Dimensions { get { return new long[4] { _xlen, _ylen, _zlen, _wlen }; } }

		private readonly DimInfo[] activeDims;

		private enum DimType {
			X, Y, Z, W
		}

		private class DimInfo
		{
			public readonly DimType DimType;
			public readonly NumBase LengthInfo;
			public DimInfo(DimType type, NumBase lengthInfo) {
				this.DimType = type;
				this.LengthInfo = lengthInfo;
			}
			public int SelectDim(int x, int y, int z, int w) {
				switch (DimType) {
					case DimType.X: return x;
					case DimType.Y: return y;
					case DimType.Z: return z;
					case DimType.W: return w;
					default: throw new InvalidStateException("DimType is invalid.");
				}
			}
		}

		public FSArrBase()
		{
			_xlen = new X().GetValue();
			_ylen = new Y().GetValue();
			_zlen = new Z().GetValue();
			_wlen = new W().GetValue();

			if (   _xlen < 1
				|| _ylen < 1
				|| _zlen < 1
				|| _wlen < 1
			) {
				activeDims = new DimInfo[0];
			}
			else
			{
				int activeDimCount = 0;
				if (_xlen > 1) activeDimCount++;
				if (_ylen > 1) activeDimCount++;
				if (_zlen > 1) activeDimCount++;
				if (_wlen > 1) activeDimCount++;

				activeDims = new DimInfo[activeDimCount];
				int curActiveDimNum = 0;
				if (_xlen > 1) { activeDims[curActiveDimNum] = new DimInfo(DimType.X, new X()); curActiveDimNum++; }
				if (_ylen > 1) { activeDims[curActiveDimNum] = new DimInfo(DimType.Y, new Y()); curActiveDimNum++; }
				if (_zlen > 1) { activeDims[curActiveDimNum] = new DimInfo(DimType.Z, new Z()); curActiveDimNum++; }
				if (_wlen > 1) { activeDims[curActiveDimNum] = new DimInfo(DimType.W, new W()); curActiveDimNum++; }

				switch (activeDimCount) {
					case 0: singleDimArr = new TItem[1]; break;
					case 1: singleDimArr = new TItem[activeDims[0].LengthInfo.GetValue()]; break;
					case 2: twoDimArr    = new TItem[activeDims[0].LengthInfo.GetValue(), activeDims[1].LengthInfo.GetValue()]; break;
					case 3: threeDimArr  = new TItem[activeDims[0].LengthInfo.GetValue(), activeDims[1].LengthInfo.GetValue(), activeDims[2].LengthInfo.GetValue()]; break;
					case 4: fourDimArr   = new TItem[activeDims[0].LengthInfo.GetValue(), activeDims[1].LengthInfo.GetValue(), activeDims[2].LengthInfo.GetValue(), activeDims[3].LengthInfo.GetValue()]; break;
				}
			}

		}

		protected TItem GetAtIndex(int x, int y, int z, int w)
		{
			switch (activeDims.Length)
			{
				case 0: throw new InvalidOperationException("The fixed-size-array is of zero size, so items cannot be accessed.");
				case 1: return singleDimArr[activeDims[0].SelectDim(x, y, z, w)];
				case 2: return twoDimArr   [activeDims[0].SelectDim(x, y, z, w), activeDims[1].SelectDim(x, y, z, w)];
				case 3: return threeDimArr [activeDims[0].SelectDim(x, y, z, w), activeDims[1].SelectDim(x, y, z, w), activeDims[2].SelectDim(x, y, z, w)];
				case 4: return fourDimArr  [activeDims[0].SelectDim(x, y, z, w), activeDims[1].SelectDim(x, y, z, w), activeDims[2].SelectDim(x, y, z, w), activeDims[3].SelectDim(x, y, z, w)];
				default: throw new InvalidStateException("activeDims is of an invalid length (=" + activeDims.Length + ")");
			}
		}
		protected void SetAtIndex(int x, int y, int z, int w, TItem value)
		{
			switch (activeDims.Length)
			{
				case 0: throw new InvalidOperationException("The fixed-size-array is of zero size, so items cannot be accessed.");
				case 1: singleDimArr[activeDims[0].SelectDim(x, y, z, w)] = value; break;
				case 2: twoDimArr   [activeDims[0].SelectDim(x, y, z, w), activeDims[1].SelectDim(x, y, z, w)] = value; break;
				case 3: threeDimArr [activeDims[0].SelectDim(x, y, z, w), activeDims[1].SelectDim(x, y, z, w), activeDims[2].SelectDim(x, y, z, w)] = value; break;
				case 4: fourDimArr  [activeDims[0].SelectDim(x, y, z, w), activeDims[1].SelectDim(x, y, z, w), activeDims[2].SelectDim(x, y, z, w), activeDims[3].SelectDim(x, y, z, w)] = value; break;
				default: throw new InvalidStateException("activeDims is of an invalid length (=" + activeDims.Length + ")");
			}
		}

		protected TItem this[int x, int y, int z, int w] {
			get {
				return GetAtIndex(x, y, z, w);
			}
			set {
				SetAtIndex(x, y, z, w, value);
			}
		}


		[Serializable]
		public class InvalidStateException : Exception
		{
			public InvalidStateException() { }
			public InvalidStateException(string message) : base(message) { }
			public InvalidStateException(string message, Exception inner) : base(message, inner) { }
			protected InvalidStateException(
			  System.Runtime.Serialization.SerializationInfo info,
			  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
		}
	}









	// -------- Single Dimensional Fixed Size Arrays --------


	public class FLArr<X, TItem> : FSArrBase<X, U1, U1, U1, TItem>
		where X : FSArrBase.NumBase, new()
	{
		public TItem this[int x] { get { return base[x, 0, 0, 0]; } set { base[x, 0, 0, 0] = value; } }
	}
	public class FLArr<X1, X2, TItem> : FLArr<N<X1, X2>, TItem>
		where X1 : FSArrBase.NumBase, new()
		where X2 : FSArrBase.NumBase, new()
	{ }
	public class FLArr<X1, X2, X3, TItem> : FLArr<N<X1, X2, X3>, TItem>
		where X1 : FSArrBase.NumBase, new()
		where X2 : FSArrBase.NumBase, new()
		where X3 : FSArrBase.NumBase, new()
	{ }


	// -------- Two Dimensional Fixed Size Arrays --------


	public class FSArr2D<X, Y, TItem> : FSArrBase<X, Y, U1, U1, TItem>
		where X : FSArrBase.NumBase, new()
		where Y : FSArrBase.NumBase, new()
	{
		public TItem this[int x, int y] { get { return base[x, y, 0, 0]; } set { base[x, y, 0, 0] = value; } }
	}
	public class FSArr2D<X1, X2, Y1, Y2, TItem> : FSArr2D<N<X1, X2>, N<Y1, Y2>, TItem>
		where X1 : FSArrBase.NumBase, new()
		where X2 : FSArrBase.NumBase, new()
		where Y1 : FSArrBase.NumBase, new()
		where Y2 : FSArrBase.NumBase, new()
	{ }


	// -------- Three Dimensional Fixed Size Arrays --------


	public class FSArr3D<X, Y, Z, TItem> : FSArrBase<X, Y, Z, U1, TItem>
		where X : FSArrBase.NumBase, new()
		where Y : FSArrBase.NumBase, new()
		where Z : FSArrBase.NumBase, new()
	{
		public TItem this[int x, int y, int z] { get { return base[x, y, z, 0]; } set { base[x, y, z, 0] = value; } }
	}
	public class FSArr3D<X1, X2, Y1, Y2, Z1, Z2, TItem> : FSArr3D<N<X1, X2>, N<Y1, Y2>, N<Z1, Z2>, TItem>
		where X1 : FSArrBase.NumBase, new()
		where X2 : FSArrBase.NumBase, new()
		where Y1 : FSArrBase.NumBase, new()
		where Y2 : FSArrBase.NumBase, new()
		where Z1 : FSArrBase.NumBase, new()
		where Z2 : FSArrBase.NumBase, new()
	{ }



	
	// -------- Four Dimensional Fixed Size Arrays --------
	
	
	public class FSArr4D<X, Y, Z, W, TItem> : FSArrBase<X, Y, Z, W, TItem>
		where X : FSArrBase.NumBase, new()
		where Y : FSArrBase.NumBase, new()
		where Z : FSArrBase.NumBase, new()
		where W : FSArrBase.NumBase, new()
	{
		public new TItem this[int x, int y, int z, int w] { get { return base[x, y, z, w]; } set { base[x, y, z, w] = value; } }
	}
	public class FSArr4D<X1, X2, Y1, Y2, Z1, Z2, W1, W2, TItem> : FSArr4D<N<X1, X2>, N<Y1, Y2>, N<Z1, Z2>, N<W1, W2>, TItem>
		where X1 : FSArrBase.NumBase, new()
		where X2 : FSArrBase.NumBase, new()
		where Y1 : FSArrBase.NumBase, new()
		where Y2 : FSArrBase.NumBase, new()
		where Z1 : FSArrBase.NumBase, new()
		where Z2 : FSArrBase.NumBase, new()
		where W1 : FSArrBase.NumBase, new()
		where W2 : FSArrBase.NumBase, new()
	{ }





	
	// ---------------- Number Types ----------------

	
	/// <summary>Radix-independent 0, used in FixedSizeArrays</summary>
	public sealed class Z0 : FSArrBase.NumBase<Z0> {
		protected sealed override int InitGetRadix() { return 0; }
		// ^ Max-radix is used when combined with other digits, and max-radix will only be 0 if all digits are Z0 (so radix becomes irrelevant)
		protected sealed override long InitGetValue() { return 0; }
	}

	/// <summary>Unary 1, used in FixedSizeArrays</summary>
	public sealed class U1 : FSArrBase.NumBase<U1>
	{
		protected sealed override int InitGetRadix() { return 1; }
		protected sealed override long InitGetValue() { return 0; }
	}

	/*#*<summary>Decimal     0, used in FixedSizeArrays</summary>*#/ public sealed class D0 : FSArrBase.DecDigit<D0> { protected override long InitGetValue() { return 0;  } }
	/*#*<summary>Decimal     1, used in FixedSizeArrays</summary>*#/ public sealed class D1 : FSArrBase.DecDigit<D1> { protected override long InitGetValue() { return 1;  } }
	/*#*<summary>Decimal     2, used in FixedSizeArrays</summary>*#/ public sealed class D2 : FSArrBase.DecDigit<D2> { protected override long InitGetValue() { return 2;  } }
	/*#*<summary>Decimal     3, used in FixedSizeArrays</summary>*#/ public sealed class D3 : FSArrBase.DecDigit<D3> { protected override long InitGetValue() { return 3;  } }
	/*#*<summary>Decimal     4, used in FixedSizeArrays</summary>*#/ public sealed class D4 : FSArrBase.DecDigit<D4> { protected override long InitGetValue() { return 4;  } }
	/*#*<summary>Decimal     5, used in FixedSizeArrays</summary>*#/ public sealed class D5 : FSArrBase.DecDigit<D5> { protected override long InitGetValue() { return 5;  } }
	/*#*<summary>Decimal     6, used in FixedSizeArrays</summary>*#/ public sealed class D6 : FSArrBase.DecDigit<D6> { protected override long InitGetValue() { return 6;  } }
	/*#*<summary>Decimal     7, used in FixedSizeArrays</summary>*#/ public sealed class D7 : FSArrBase.DecDigit<D7> { protected override long InitGetValue() { return 7;  } }
	/*#*<summary>Decimal     8, used in FixedSizeArrays</summary>*#/ public sealed class D8 : FSArrBase.DecDigit<D8> { protected override long InitGetValue() { return 8;  } }
	/*#*<summary>Decimal     9, used in FixedSizeArrays</summary>*#/ public sealed class D9 : FSArrBase.DecDigit<D9> { protected override long InitGetValue() { return 9;  } }
	/*#*<summary>Hexadecimal 0, used in FixedSizeArrays</summary>*#/ public sealed class H0 : FSArrBase.HexDigit<H0> { protected override long InitGetValue() { return 0;  } }
	/*#*<summary>Hexadecimal 1, used in FixedSizeArrays</summary>*#/ public sealed class H1 : FSArrBase.HexDigit<H1> { protected override long InitGetValue() { return 1;  } }
	/*#*<summary>Hexadecimal 2, used in FixedSizeArrays</summary>*#/ public sealed class H2 : FSArrBase.HexDigit<H2> { protected override long InitGetValue() { return 2;  } }
	/*#*<summary>Hexadecimal 3, used in FixedSizeArrays</summary>*#/ public sealed class H3 : FSArrBase.HexDigit<H3> { protected override long InitGetValue() { return 3;  } }
	/*#*<summary>Hexadecimal 4, used in FixedSizeArrays</summary>*#/ public sealed class H4 : FSArrBase.HexDigit<H4> { protected override long InitGetValue() { return 4;  } }
	/*#*<summary>Hexadecimal 5, used in FixedSizeArrays</summary>*#/ public sealed class H5 : FSArrBase.HexDigit<H5> { protected override long InitGetValue() { return 5;  } }
	/*#*<summary>Hexadecimal 6, used in FixedSizeArrays</summary>*#/ public sealed class H6 : FSArrBase.HexDigit<H6> { protected override long InitGetValue() { return 6;  } }
	/*#*<summary>Hexadecimal 7, used in FixedSizeArrays</summary>*#/ public sealed class H7 : FSArrBase.HexDigit<H7> { protected override long InitGetValue() { return 7;  } }
	/*#*<summary>Hexadecimal 8, used in FixedSizeArrays</summary>*#/ public sealed class H8 : FSArrBase.HexDigit<H8> { protected override long InitGetValue() { return 8;  } }
	/*#*<summary>Hexadecimal 9, used in FixedSizeArrays</summary>*#/ public sealed class H9 : FSArrBase.HexDigit<H9> { protected override long InitGetValue() { return 9;  } }
	/*#*<summary>Hexadecimal A, used in FixedSizeArrays</summary>*#/ public sealed class HA : FSArrBase.HexDigit<HA> { protected override long InitGetValue() { return 10; } }
	/*#*<summary>Hexadecimal B, used in FixedSizeArrays</summary>*#/ public sealed class HB : FSArrBase.HexDigit<HB> { protected override long InitGetValue() { return 11; } }
	/*#*<summary>Hexadecimal C, used in FixedSizeArrays</summary>*#/ public sealed class HC : FSArrBase.HexDigit<HC> { protected override long InitGetValue() { return 12; } }
	/*#*<summary>Hexadecimal D, used in FixedSizeArrays</summary>*#/ public sealed class HD : FSArrBase.HexDigit<HD> { protected override long InitGetValue() { return 13; } }
	/*#*<summary>Hexadecimal E, used in FixedSizeArrays</summary>*#/ public sealed class HE : FSArrBase.HexDigit<HE> { protected override long InitGetValue() { return 14; } }
	/*#*<summary>Hexadecimal F, used in FixedSizeArrays</summary>*#/ public sealed class HF : FSArrBase.HexDigit<HF> { protected override long InitGetValue() { return 15; } }

	/// <summary>
	/// Multi-digit number, where all digits are interpreted as having the same radix as the digit with the highest radix, and itself having a radix of Math.Pow((highest radix of a digit), (num digits))
	/// </summary>
	public class N<Dig1, Dig2, Dig3, Dig4, Dig5> : FSArrBase.MultiDigitNumBase<N<Dig1, Dig2, Dig3, Dig4, Dig5>>
		where Dig1 : FSArrBase.NumBase, new()
		where Dig2 : FSArrBase.NumBase, new()
		where Dig3 : FSArrBase.NumBase, new()
		where Dig4 : FSArrBase.NumBase, new()
		where Dig5 : FSArrBase.NumBase, new()
	{
		protected override FSArrBase.NumBase[] InitGetDigitsDescendingSignificance() {
			return new FSArrBase.NumBase[] {
				new Dig5(),
				new Dig4(),
				new Dig3(),
				new Dig2(),
				new Dig1(),
			};
		}
	}

	/// <summary>
	/// Multi-digit number, where all digits are interpreted as having the same radix as the digit with the highest radix, and itself having a radix of Math.Pow((highest radix of a digit), (num digits))
	/// </summary>
	public class N<Dig1, Dig2, Dig3, Dig4> : FSArrBase.MultiDigitNumBase<N<Dig1, Dig2, Dig3, Dig4>>
		where Dig1 : FSArrBase.NumBase, new()
		where Dig2 : FSArrBase.NumBase, new()
		where Dig3 : FSArrBase.NumBase, new()
		where Dig4 : FSArrBase.NumBase, new()
	{
		protected override FSArrBase.NumBase[] InitGetDigitsDescendingSignificance() {
			return new FSArrBase.NumBase[] {
				new Dig4(),
				new Dig3(),
				new Dig2(),
				new Dig1(),
			};
		}
	}

	/// <summary>
	/// Multi-digit number, where all digits are interpreted as having the same radix as the digit with the highest radix, and itself having a radix of Math.Pow((highest radix of a digit), (num digits))
	/// </summary>
	public class N<Dig1, Dig2, Dig3> : FSArrBase.MultiDigitNumBase<N<Dig1, Dig2, Dig3>>
		where Dig1 : FSArrBase.NumBase, new()
		where Dig2 : FSArrBase.NumBase, new()
		where Dig3 : FSArrBase.NumBase, new()
	{
		protected override FSArrBase.NumBase[] InitGetDigitsDescendingSignificance() {
			return new FSArrBase.NumBase[] {
				new Dig3(),
				new Dig2(),
				new Dig1(),
			};
		}
	}

	/// <summary>
	/// Multi-digit number, where all digits are interpreted as having the same radix as the digit with the highest radix, and itself having a radix of Math.Pow((highest radix of a digit), (num digits))
	/// </summary>
	public class N<Dig1, Dig2> : FSArrBase.MultiDigitNumBase<N<Dig1, Dig2>>
		where Dig1 : FSArrBase.NumBase, new()
		where Dig2 : FSArrBase.NumBase, new()
	{
		protected override FSArrBase.NumBase[] InitGetDigitsDescendingSignificance() {
			return new FSArrBase.NumBase[] {
				new Dig2(),
				new Dig1(),
			};
		}
	}
}


//*/