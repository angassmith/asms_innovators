﻿/*

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public abstract class Obj
{
	public static bool AllEqual(object[] left, object[] right)
	{
		if (left == null && right == null) return true;
		else if (left == null || right == null) return false;
		else if (left.Length != right.Length) return false;
		for (int i = 0; i < left.Length; i++)
		{
			object leftItem = left[i];
			object rightItem = right[i];
			if (leftItem == null && rightItem == null) continue;
			else if (leftItem == null && !rightItem.Equals(leftItem)) return false;
			else if (!leftItem.Equals(rightItem)) return false;
		}
		return true;
	}

	public static Obj<T1                > Create<T1                >(T1 Item1                                        ) { return new Obj<T1                >(Item1                            ); }
	public static Obj<T1, T2            > Create<T1, T2            >(T1 Item1, T2 Item2                              ) { return new Obj<T1, T2            >(Item1, Item2                     ); }
	public static Obj<T1, T2, T3        > Create<T1, T2, T3        >(T1 Item1, T2 Item2, T3 Item3                    ) { return new Obj<T1, T2, T3        >(Item1, Item2, Item3              ); }
	public static Obj<T1, T2, T3, T4    > Create<T1, T2, T3, T4    >(T1 Item1, T2 Item2, T3 Item3, T4 Item4          ) { return new Obj<T1, T2, T3, T4    >(Item1, Item2, Item3, Item4       ); }
	public static Obj<T1, T2, T3, T4, T5> Create<T1, T2, T3, T4, T5>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5) { return new Obj<T1, T2, T3, T4, T5>(Item1, Item2, Item3, Item4, Item5); }

	// From System.Tuple
	public static int CombineHashCodes(int h1, int h2                                                                                                                       ) { return (((h1 << 5) + h1) ^ h2); }
	public static int CombineHashCodes(int h1, int h2, int h3                                                                                                               ) { return CombineHashCodes(CombineHashCodes(h1, h2), h3); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4                                                                                                       ) { return CombineHashCodes(CombineHashCodes(h1, h2), CombineHashCodes(h3, h4)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5                                                                                               ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4), h5); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6                                                                                       ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4), CombineHashCodes(h5, h6)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7                                                                               ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4), CombineHashCodes(h5, h6, h7)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8                                                                       ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4), CombineHashCodes(h5, h6, h7, h8)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9                                                               ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4, h5, h6, h7, h8), h9); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10                                                      ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4, h5, h6, h7, h8), CombineHashCodes(h9, h10)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10, int h11                                             ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4, h5, h6, h7, h8), CombineHashCodes(h9, h10, h11)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10, int h11, int h12                                    ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4, h5, h6, h7, h8), CombineHashCodes(h9, h10, h11, h12)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10, int h11, int h12, int h13                           ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4, h5, h6, h7, h8), CombineHashCodes(h9, h10, h11, h12, h13)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10, int h11, int h12, int h13, int h14                  ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4, h5, h6, h7, h8), CombineHashCodes(h9, h10, h11, h12, h13, h14)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10, int h11, int h12, int h13, int h14, int h15         ) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4, h5, h6, h7, h8), CombineHashCodes(h9, h10, h11, h12, h13, h14, h15)); }
	public static int CombineHashCodes(int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10, int h11, int h12, int h13, int h14, int h15, int h16) { return CombineHashCodes(CombineHashCodes(h1, h2, h3, h4, h5, h6, h7, h8), CombineHashCodes(h9, h10, h11, h12, h13, h14, h15, h16)); }
	
	public static int GetHashCode<T1                                                                   >(T1 Item1                                                                                                                                                                    ) { return ReferenceEquals(Item1, null) ? 0 : Item1.GetHashCode(); }
	public static int GetHashCode<T1, T2                                                               >(T1 Item1, T2 Item2                                                                                                                                                          ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2)                                                                                                                                                                                                                                                                                               ); }
	public static int GetHashCode<T1, T2, T3                                                           >(T1 Item1, T2 Item2, T3 Item3                                                                                                                                                ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3)                                                                                                                                                                                                                                                                           ); }
	public static int GetHashCode<T1, T2, T3, T4                                                       >(T1 Item1, T2 Item2, T3 Item3, T4 Item4                                                                                                                                      ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4)                                                                                                                                                                                                                                                       ); }
	public static int GetHashCode<T1, T2, T3, T4, T5                                                   >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5                                                                                                                            ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5)                                                                                                                                                                                                                                   ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6                                               >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6                                                                                                                  ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6)                                                                                                                                                                                                               ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7                                           >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7                                                                                                        ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7)                                                                                                                                                                                           ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8                                       >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8                                                                                              ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8)                                                                                                                                                                       ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9                                   >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9                                                                                    ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8), GetHashCode(Item9)                                                                                                                                                   ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10                              >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10                                                                        ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8), GetHashCode(Item9), GetHashCode(Item10)                                                                                                                              ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11                         >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11                                                            ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8), GetHashCode(Item9), GetHashCode(Item10), GetHashCode(Item11)                                                                                                         ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12                    >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12                                                ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8), GetHashCode(Item9), GetHashCode(Item10), GetHashCode(Item11), GetHashCode(Item12)                                                                                    ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13               >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12, T13 Item13                                    ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8), GetHashCode(Item9), GetHashCode(Item10), GetHashCode(Item11), GetHashCode(Item12), GetHashCode(Item13)                                                               ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14          >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12, T13 Item13, T14 Item14                        ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8), GetHashCode(Item9), GetHashCode(Item10), GetHashCode(Item11), GetHashCode(Item12), GetHashCode(Item13), GetHashCode(Item14)                                          ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15     >(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12, T13 Item13, T14 Item14, T15 Item15            ) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8), GetHashCode(Item9), GetHashCode(Item10), GetHashCode(Item11), GetHashCode(Item12), GetHashCode(Item13), GetHashCode(Item14), GetHashCode(Item15)                     ); }
	public static int GetHashCode<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5, T6 Item6, T7 Item7, T8 Item8, T9 Item9, T10 Item10, T11 Item11, T12 Item12, T13 Item13, T14 Item14, T15 Item15, T16 Item16) { return CombineHashCodes(GetHashCode(Item1), GetHashCode(Item2), GetHashCode(Item3), GetHashCode(Item4), GetHashCode(Item5), GetHashCode(Item6), GetHashCode(Item7), GetHashCode(Item8), GetHashCode(Item9), GetHashCode(Item10), GetHashCode(Item11), GetHashCode(Item12), GetHashCode(Item13), GetHashCode(Item14), GetHashCode(Item15), GetHashCode(Item16)); }
}

[System.Serializable]
/// <summary>Simple custom objects</summary>
public class Obj<T1> : Obj
{
	public T1 Item1;
	public Obj(T1 t1) {
		this.Item1 = t1;
	}
	public static bool operator ==(Obj<T1> left, Obj<T1> right) {
		bool leftNull = ReferenceEquals(left, null);
		bool rightNull = ReferenceEquals(right, null);
		if (leftNull && rightNull) return true;
		else if (leftNull || rightNull) return false;
		else return AllEqual(
			new object[] { left.Item1 },
			new object[] { right.Item1 }
		);
	}
	public static bool operator !=(Obj<T1> left, Obj<T1> right) { return !(left == right); }
	public override bool Equals(object obj) { return this == (obj as Obj<T1>); }
	public override int GetHashCode() { return GetHashCode(Item1); }
}
[System.Serializable]
/// <summary>Simple custom objects</summary>
public class Obj<T1, T2> : Obj
{
	public T1 Item1;
	public T2 Item2;
	public Obj(T1 Item1, T2 Item2) {
		this.Item1 = Item1;
		this.Item2 = Item2;
	}
	public static bool operator ==(Obj<T1, T2> left, Obj<T1, T2> right) {
		bool leftNull = ReferenceEquals(left, null);
		bool rightNull = ReferenceEquals(right, null);
		if (leftNull && rightNull) return true;
		else if (leftNull || rightNull) return false;
		else return AllEqual(
			new object[] { left.Item1, left.Item2 },
			new object[] { right.Item1, right.Item2 }
		);
	}
	public static bool operator !=(Obj<T1, T2> left, Obj<T1, T2> right) { return !(left == right); }
	public override bool Equals(object obj) { return this == (obj as Obj<T1, T2>); }
	public override int GetHashCode() { return GetHashCode(Item1, Item2); }
}
[System.Serializable]
/// <summary>Simple custom objects</summary>
public class Obj<T1, T2, T3> : Obj
{
	public T1 Item1;
	public T2 Item2;
	public T3 Item3;
	public Obj(T1 Item1, T2 Item2, T3 Item3) {
		this.Item1 = Item1;
		this.Item2 = Item2;
		this.Item3 = Item3;
	}
	public static bool operator ==(Obj<T1, T2, T3> left, Obj<T1, T2, T3> right) {
		bool leftNull = ReferenceEquals(left, null);
		bool rightNull = ReferenceEquals(right, null);
		if (leftNull && rightNull) return true;
		else if (leftNull || rightNull) return false;
		else return AllEqual(
			new object[] { left.Item1, left.Item2, left.Item3 },
			new object[] { right.Item1, right.Item2, right.Item3 }
		);
	}
	public static bool operator !=(Obj<T1, T2, T3> left, Obj<T1, T2, T3> right) { return !(left == right); }
	public override bool Equals(object obj) { return this == (obj as Obj<T1, T2, T3>); }
	public override int GetHashCode() { return GetHashCode(Item1, Item2, Item3); }
}
[System.Serializable]
/// <summary>Simple custom objects</summary>
public class Obj<T1, T2, T3, T4> : Obj
{
	public T1 Item1;
	public T2 Item2;
	public T3 Item3;
	public T4 Item4;
	public Obj(T1 Item1, T2 Item2, T3 Item3, T4 Item4) {
		this.Item1 = Item1;
		this.Item2 = Item2;
		this.Item3 = Item3;
		this.Item4 = Item4;
	}
	public static bool operator ==(Obj<T1, T2, T3, T4> left, Obj<T1, T2, T3, T4> right) {
		bool leftNull = ReferenceEquals(left, null);
		bool rightNull = ReferenceEquals(right, null);
		if (leftNull && rightNull) return true;
		else if (leftNull || rightNull) return false;
		else return AllEqual(
			new object[] { left.Item1, left.Item2, left.Item3, left.Item4 },
			new object[] { right.Item1, right.Item2, right.Item3, right.Item4 }
		);
	}
	public static bool operator !=(Obj<T1, T2, T3, T4> left, Obj<T1, T2, T3, T4> right) { return !(left == right); }
	public override bool Equals(object obj) { return this == (obj as Obj<T1, T2, T3, T4>); }
	public override int GetHashCode() { return GetHashCode(Item1, Item2, Item3, Item4); }
}
[System.Serializable]
/// <summary>Simple custom objects</summary>
public class Obj<T1, T2, T3, T4, T5> : Obj
{
	public T1 Item1;
	public T2 Item2;
	public T3 Item3;
	public T4 Item4;
	public T5 Item5;
	public Obj(T1 Item1, T2 Item2, T3 Item3, T4 Item4, T5 Item5) {
		this.Item1 = Item1;
		this.Item2 = Item2;
		this.Item3 = Item3;
		this.Item4 = Item4;
		this.Item5 = Item5;
	}
	public static bool operator ==(Obj<T1, T2, T3, T4, T5> left, Obj<T1, T2, T3, T4, T5> right) {
		bool leftNull = ReferenceEquals(left, null);
		bool rightNull = ReferenceEquals(right, null);
		if (leftNull && rightNull) return true;
		else if (leftNull || rightNull) return false;
		else return AllEqual(
			new object[] { left.Item1, left.Item2, left.Item3, left.Item4, left.Item5 },
			new object[] { right.Item1, right.Item2, right.Item3, right.Item4, right.Item5 }
		);
	}
	public static bool operator !=(Obj<T1, T2, T3, T4, T5> left, Obj<T1, T2, T3, T4, T5> right) { return !(left == right); }
	public override bool Equals(object obj) { return this == (obj as Obj<T1, T2, T3, T4, T5>); }
	public override int GetHashCode() { return GetHashCode(Item1, Item2, Item3, Item4, Item5); }
}


//*/