﻿/*

using System.Collections.ObjectModel;

/// <summary>
/// Note: Currently does not support handling hash code collisions
/// <para/>
/// This single-key version might be useful for generalizations, but not much else
/// </summary>
[System.Serializable]
public class MultiKeyDict<TKey1, TValue>
	: MultiKeyDictBase<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass,
		MultiKeyDictEntry<TKey1, TValue>, TValue>
{
	public MultiKeyDict()  { }
	public MultiKeyDict(params MultiKeyDictEntry<TKey1, TValue>[] entries) {
		if (entries == null) return;
		foreach (var entry in entries) {
			CheckIfEntryHasValue(entry);
			this.Add(entry);
		}
	}

	public ReadOnlyCollection<TKey1> Key1s { get { return GetKey1s(); } }
	
	public void Add(TKey1 key1, TValue value) {
		AddInternal(value, key1, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void);
	}
	
	public TValue this[TKey1 key] { get { return GetValueByKey1Internal(key); } set { SetValueByKey1Internal(key, value); } }

	public MultiKeyDictEntry<TKey1, TValue> GetEntryByKey1(TKey1 key) { return GetEntryByKey1Internal(key); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TValue> entry) { return TryGetEntryByKey1Internal(key, out entry); }

	public TValue GetValueByKey1(TKey1 key) { return GetValueByKey1Internal(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue value) { return TryGetValueByKey1Internal(key, out value); }

	public bool SetValueByKey1(TKey1 key, TValue value) { return SetValueByKey1Internal(key, value); }
	
	public void RemoveByKey1(TKey1 key) { RemoveByKey1Internal(key); }

	public bool TryRemoveByKey1(TKey1 key) { return TryRemoveByKey1Internal(key); }

	public bool ContainsKey1(TKey1 key) { return ContainsKey1Internal(key); }
}

/// <summary>
/// Note: Currently does not support handling hash code collisions
/// </summary>
[System.Serializable]
public class MultiKeyDict<TKey1, TKey2, TValue>
	: MultiKeyDictBase<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass,
		MultiKeyDictEntry<TKey1, TKey2, TValue>, TValue>
{
	public MultiKeyDict()  { }
	public MultiKeyDict(params MultiKeyDictEntry<TKey1, TKey2, TValue>[] entries) {
		if (entries == null) return;
		foreach (var entry in entries) {
			CheckIfEntryHasValue(entry);
			this.Add(entry);
		}
	}

	public ReadOnlyCollection<TKey1> Key1s { get { return GetKey1s(); } }
	public ReadOnlyCollection<TKey2> Key2s { get { return GetKey2s(); } }
	
	public void Add(TKey1 key1, TKey2 key2, TValue value) {
		AddInternal(value, key1, key2, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void);
	}
	
	public TValue this[TKey1 key] { get { return GetValueByKey1Internal(key); } set { SetValueByKey1Internal(key, value); } }
	public TValue this[TKey2 key] { get { return GetValueByKey2Internal(key); } set { SetValueByKey2Internal(key, value); } }

	public MultiKeyDictEntry<TKey1, TKey2, TValue> GetEntryByKey1(TKey1 key) { return GetEntryByKey1Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TValue> GetEntryByKey2(TKey2 key) { return GetEntryByKey2Internal(key); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TValue> entry) { return TryGetEntryByKey1Internal(key, out entry); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TValue> entry) { return TryGetEntryByKey2Internal(key, out entry); }

	public TValue GetValueByKey1(TKey1 key) { return GetValueByKey1Internal(key); }
	public TValue GetValueByKey2(TKey2 key) { return GetValueByKey2Internal(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue value) { return TryGetValueByKey1Internal(key, out value); }
	public bool TryGetValueByKey2(TKey2 key, out TValue value) { return TryGetValueByKey2Internal(key, out value); }

	public bool SetValueByKey1(TKey1 key, TValue value) { return SetValueByKey1Internal(key, value); }
	public bool SetValueByKey2(TKey2 key, TValue value) { return SetValueByKey2Internal(key, value); }
	
	public void RemoveByKey1(TKey1 key) { RemoveByKey1Internal(key); }
	public void RemoveByKey2(TKey2 key) { RemoveByKey2Internal(key); }

	public bool TryRemoveByKey1(TKey1 key) { return TryRemoveByKey1Internal(key); }
	public bool TryRemoveByKey2(TKey2 key) { return TryRemoveByKey2Internal(key); }

	public bool ContainsKey1(TKey1 key) { return ContainsKey1Internal(key); }
	public bool ContainsKey2(TKey2 key) { return ContainsKey2Internal(key); }
}



/// <summary>
/// Note: Currently does not support handling hash code collisions
/// </summary>
[System.Serializable]
public class MultiKeyDict<TKey1, TKey2, TKey3, TValue>
	: MultiKeyDictBase<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass,
		MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue>, TValue>
{
	public MultiKeyDict()  { }
	public MultiKeyDict(params MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue>[] entries) {
		if (entries == null) return;
		foreach (var entry in entries) {
			CheckIfEntryHasValue(entry);
			this.Add(entry);
		}
	}

	public ReadOnlyCollection<TKey1> Key1s { get { return GetKey1s(); } }
	public ReadOnlyCollection<TKey2> Key2s { get { return GetKey2s(); } }
	public ReadOnlyCollection<TKey3> Key3s { get { return GetKey3s(); } }
	
	public void Add(TKey1 key1, TKey2 key2, TKey3 key3, TValue value) {
		AddInternal(value, key1, key2, key3, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void);
	}
	
	public TValue this[TKey1 key] { get { return GetValueByKey1Internal(key); } set { SetValueByKey1Internal(key, value); } }
	public TValue this[TKey2 key] { get { return GetValueByKey2Internal(key); } set { SetValueByKey2Internal(key, value); } }
	public TValue this[TKey3 key] { get { return GetValueByKey3Internal(key); } set { SetValueByKey3Internal(key, value); } }

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> GetEntryByKey1(TKey1 key) { return GetEntryByKey1Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> GetEntryByKey2(TKey2 key) { return GetEntryByKey2Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> GetEntryByKey3(TKey3 key) { return GetEntryByKey3Internal(key); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> entry) { return TryGetEntryByKey1Internal(key, out entry); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> entry) { return TryGetEntryByKey2Internal(key, out entry); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> entry) { return TryGetEntryByKey3Internal(key, out entry); }

	public TValue GetValueByKey1(TKey1 key) { return GetValueByKey1Internal(key); }
	public TValue GetValueByKey2(TKey2 key) { return GetValueByKey2Internal(key); }
	public TValue GetValueByKey3(TKey3 key) { return GetValueByKey3Internal(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue value) { return TryGetValueByKey1Internal(key, out value); }
	public bool TryGetValueByKey2(TKey2 key, out TValue value) { return TryGetValueByKey2Internal(key, out value); }
	public bool TryGetValueByKey3(TKey3 key, out TValue value) { return TryGetValueByKey3Internal(key, out value); }

	public bool SetValueByKey1(TKey1 key, TValue value) { return SetValueByKey1Internal(key, value); }
	public bool SetValueByKey2(TKey2 key, TValue value) { return SetValueByKey2Internal(key, value); }
	public bool SetValueByKey3(TKey3 key, TValue value) { return SetValueByKey3Internal(key, value); }
	
	public void RemoveByKey1(TKey1 key) { RemoveByKey1Internal(key); }
	public void RemoveByKey2(TKey2 key) { RemoveByKey2Internal(key); }
	public void RemoveByKey3(TKey3 key) { RemoveByKey3Internal(key); }

	public bool TryRemoveByKey1(TKey1 key) { return TryRemoveByKey1Internal(key); }
	public bool TryRemoveByKey2(TKey2 key) { return TryRemoveByKey2Internal(key); }
	public bool TryRemoveByKey3(TKey3 key) { return TryRemoveByKey3Internal(key); }

	public bool ContainsKey1(TKey1 key) { return ContainsKey1Internal(key); }
	public bool ContainsKey2(TKey2 key) { return ContainsKey2Internal(key); }
	public bool ContainsKey3(TKey3 key) { return ContainsKey3Internal(key); }
}



/// <summary>
/// Note: Currently does not support handling hash code collisions
/// </summary>
[System.Serializable]
public class MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TValue>
	: MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass,
		MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue>, TValue>
{
	public MultiKeyDict()  { }
	public MultiKeyDict(params MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue>[] entries) {
		if (entries == null) return;
		foreach (var entry in entries) {
			CheckIfEntryHasValue(entry);
			this.Add(entry);
		}
	}

	public ReadOnlyCollection<TKey1> Key1s { get { return GetKey1s(); } }
	public ReadOnlyCollection<TKey2> Key2s { get { return GetKey2s(); } }
	public ReadOnlyCollection<TKey3> Key3s { get { return GetKey3s(); } }
	public ReadOnlyCollection<TKey4> Key4s { get { return GetKey4s(); } }
	
	public void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TValue value) {
		AddInternal(value, key1, key2, key3, key4, VoidClass.Void, VoidClass.Void, VoidClass.Void, VoidClass.Void);
	}
	
	public TValue this[TKey1 key] { get { return GetValueByKey1Internal(key); } set { SetValueByKey1Internal(key, value); } }
	public TValue this[TKey2 key] { get { return GetValueByKey2Internal(key); } set { SetValueByKey2Internal(key, value); } }
	public TValue this[TKey3 key] { get { return GetValueByKey3Internal(key); } set { SetValueByKey3Internal(key, value); } }
	public TValue this[TKey4 key] { get { return GetValueByKey4Internal(key); } set { SetValueByKey4Internal(key, value); } }

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> GetEntryByKey1(TKey1 key) { return GetEntryByKey1Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> GetEntryByKey2(TKey2 key) { return GetEntryByKey2Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> GetEntryByKey3(TKey3 key) { return GetEntryByKey3Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> GetEntryByKey4(TKey4 key) { return GetEntryByKey4Internal(key); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> entry) { return TryGetEntryByKey1Internal(key, out entry); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> entry) { return TryGetEntryByKey2Internal(key, out entry); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> entry) { return TryGetEntryByKey3Internal(key, out entry); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> entry) { return TryGetEntryByKey4Internal(key, out entry); }

	public TValue GetValueByKey1(TKey1 key) { return GetValueByKey1Internal(key); }
	public TValue GetValueByKey2(TKey2 key) { return GetValueByKey2Internal(key); }
	public TValue GetValueByKey3(TKey3 key) { return GetValueByKey3Internal(key); }
	public TValue GetValueByKey4(TKey4 key) { return GetValueByKey4Internal(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue value) { return TryGetValueByKey1Internal(key, out value); }
	public bool TryGetValueByKey2(TKey2 key, out TValue value) { return TryGetValueByKey2Internal(key, out value); }
	public bool TryGetValueByKey3(TKey3 key, out TValue value) { return TryGetValueByKey3Internal(key, out value); }
	public bool TryGetValueByKey4(TKey4 key, out TValue value) { return TryGetValueByKey4Internal(key, out value); }

	public bool SetValueByKey1(TKey1 key, TValue value) { return SetValueByKey1Internal(key, value); }
	public bool SetValueByKey2(TKey2 key, TValue value) { return SetValueByKey2Internal(key, value); }
	public bool SetValueByKey3(TKey3 key, TValue value) { return SetValueByKey3Internal(key, value); }
	public bool SetValueByKey4(TKey4 key, TValue value) { return SetValueByKey4Internal(key, value); }
	
	public void RemoveByKey1(TKey1 key) { RemoveByKey1Internal(key); }
	public void RemoveByKey2(TKey2 key) { RemoveByKey2Internal(key); }
	public void RemoveByKey3(TKey3 key) { RemoveByKey3Internal(key); }
	public void RemoveByKey4(TKey4 key) { RemoveByKey4Internal(key); }

	public bool TryRemoveByKey1(TKey1 key) { return TryRemoveByKey1Internal(key); }
	public bool TryRemoveByKey2(TKey2 key) { return TryRemoveByKey2Internal(key); }
	public bool TryRemoveByKey3(TKey3 key) { return TryRemoveByKey3Internal(key); }
	public bool TryRemoveByKey4(TKey4 key) { return TryRemoveByKey4Internal(key); }

	public bool ContainsKey1(TKey1 key) { return ContainsKey1Internal(key); }
	public bool ContainsKey2(TKey2 key) { return ContainsKey2Internal(key); }
	public bool ContainsKey3(TKey3 key) { return ContainsKey3Internal(key); }
	public bool ContainsKey4(TKey4 key) { return ContainsKey4Internal(key); }
}



/// <summary>
/// Note: Currently does not support handling hash code collisions
/// </summary>
[System.Serializable]
public class MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TValue>
	: MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass,
		MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue>, TValue>
{
	public MultiKeyDict()  { }
	public MultiKeyDict(params MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue>[] entries) {
		if (entries == null) return;
		foreach (var entry in entries) {
			this.Add(entry); //This also calls CheckIfEntryHasValue()
		}
	}
	
	public ReadOnlyCollection<TKey1> Key1s { get { return GetKey1s(); } }
	public ReadOnlyCollection<TKey2> Key2s { get { return GetKey2s(); } }
	public ReadOnlyCollection<TKey3> Key3s { get { return GetKey3s(); } }
	public ReadOnlyCollection<TKey4> Key4s { get { return GetKey4s(); } }
	public ReadOnlyCollection<TKey5> Key5s { get { return GetKey5s(); } }
	
	public void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TValue value) {
		AddInternal(value, key1, key2, key3, key4, key5, VoidClass.Void, VoidClass.Void, VoidClass.Void);
	}
	
	public TValue this[TKey1 key] { get { return GetValueByKey1Internal(key); } set { SetValueByKey1Internal(key, value); } }
	public TValue this[TKey2 key] { get { return GetValueByKey2Internal(key); } set { SetValueByKey2Internal(key, value); } }
	public TValue this[TKey3 key] { get { return GetValueByKey3Internal(key); } set { SetValueByKey3Internal(key, value); } }
	public TValue this[TKey4 key] { get { return GetValueByKey4Internal(key); } set { SetValueByKey4Internal(key, value); } }
	public TValue this[TKey5 key] { get { return GetValueByKey5Internal(key); } set { SetValueByKey5Internal(key, value); } }

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> GetEntryByKey1(TKey1 key) { return GetEntryByKey1Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> GetEntryByKey2(TKey2 key) { return GetEntryByKey2Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> GetEntryByKey3(TKey3 key) { return GetEntryByKey3Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> GetEntryByKey4(TKey4 key) { return GetEntryByKey4Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> GetEntryByKey5(TKey5 key) { return GetEntryByKey5Internal(key); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> entry) { return TryGetEntryByKey1Internal(key, out entry); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> entry) { return TryGetEntryByKey2Internal(key, out entry); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> entry) { return TryGetEntryByKey3Internal(key, out entry); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> entry) { return TryGetEntryByKey4Internal(key, out entry); }
	public bool TryGetEntryByKey5(TKey5 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> entry) { return TryGetEntryByKey5Internal(key, out entry); }

	public TValue GetValueByKey1(TKey1 key) { return GetValueByKey1Internal(key); }
	public TValue GetValueByKey2(TKey2 key) { return GetValueByKey2Internal(key); }
	public TValue GetValueByKey3(TKey3 key) { return GetValueByKey3Internal(key); }
	public TValue GetValueByKey4(TKey4 key) { return GetValueByKey4Internal(key); }
	public TValue GetValueByKey5(TKey5 key) { return GetValueByKey5Internal(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue value) { return TryGetValueByKey1Internal(key, out value); }
	public bool TryGetValueByKey2(TKey2 key, out TValue value) { return TryGetValueByKey2Internal(key, out value); }
	public bool TryGetValueByKey3(TKey3 key, out TValue value) { return TryGetValueByKey3Internal(key, out value); }
	public bool TryGetValueByKey4(TKey4 key, out TValue value) { return TryGetValueByKey4Internal(key, out value); }
	public bool TryGetValueByKey5(TKey5 key, out TValue value) { return TryGetValueByKey5Internal(key, out value); }

	public bool SetValueByKey1(TKey1 key, TValue value) { return SetValueByKey1Internal(key, value); }
	public bool SetValueByKey2(TKey2 key, TValue value) { return SetValueByKey2Internal(key, value); }
	public bool SetValueByKey3(TKey3 key, TValue value) { return SetValueByKey3Internal(key, value); }
	public bool SetValueByKey4(TKey4 key, TValue value) { return SetValueByKey4Internal(key, value); }
	public bool SetValueByKey5(TKey5 key, TValue value) { return SetValueByKey5Internal(key, value); }
	
	public void RemoveByKey1(TKey1 key) { RemoveByKey1Internal(key); }
	public void RemoveByKey2(TKey2 key) { RemoveByKey2Internal(key); }
	public void RemoveByKey3(TKey3 key) { RemoveByKey3Internal(key); }
	public void RemoveByKey4(TKey4 key) { RemoveByKey4Internal(key); }
	public void RemoveByKey5(TKey5 key) { RemoveByKey5Internal(key); }

	public bool TryRemoveByKey1(TKey1 key) { return TryRemoveByKey1Internal(key); }
	public bool TryRemoveByKey2(TKey2 key) { return TryRemoveByKey2Internal(key); }
	public bool TryRemoveByKey3(TKey3 key) { return TryRemoveByKey3Internal(key); }
	public bool TryRemoveByKey4(TKey4 key) { return TryRemoveByKey4Internal(key); }
	public bool TryRemoveByKey5(TKey5 key) { return TryRemoveByKey5Internal(key); }

	public bool ContainsKey1(TKey1 key) { return ContainsKey1Internal(key); }
	public bool ContainsKey2(TKey2 key) { return ContainsKey2Internal(key); }
	public bool ContainsKey3(TKey3 key) { return ContainsKey3Internal(key); }
	public bool ContainsKey4(TKey4 key) { return ContainsKey4Internal(key); }
	public bool ContainsKey5(TKey5 key) { return ContainsKey5Internal(key); }
}



/// <summary>
/// Note: Currently does not support handling hash code collisions
/// </summary>
[System.Serializable]
public class MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue>
	: MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass,
		MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue>, TValue>
{
	public MultiKeyDict()  { }
	public MultiKeyDict(params MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue>[] entries) {
		if (entries == null) return;
		foreach (var entry in entries) {
			this.Add(entry); //This also calls CheckIfEntryHasValue()
		}
	}
	
	public ReadOnlyCollection<TKey1> Key1s { get { return GetKey1s(); } }
	public ReadOnlyCollection<TKey2> Key2s { get { return GetKey2s(); } }
	public ReadOnlyCollection<TKey3> Key3s { get { return GetKey3s(); } }
	public ReadOnlyCollection<TKey4> Key4s { get { return GetKey4s(); } }
	public ReadOnlyCollection<TKey5> Key5s { get { return GetKey5s(); } }
	public ReadOnlyCollection<TKey6> Key6s { get { return GetKey6s(); } }
	
	public void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TValue value) {
		AddInternal(value, key1, key2, key3, key4, key5, key6, VoidClass.Void, VoidClass.Void);
	}
	
	public TValue this[TKey1 key] { get { return GetValueByKey1Internal(key); } set { SetValueByKey1Internal(key, value); } }
	public TValue this[TKey2 key] { get { return GetValueByKey2Internal(key); } set { SetValueByKey2Internal(key, value); } }
	public TValue this[TKey3 key] { get { return GetValueByKey3Internal(key); } set { SetValueByKey3Internal(key, value); } }
	public TValue this[TKey4 key] { get { return GetValueByKey4Internal(key); } set { SetValueByKey4Internal(key, value); } }
	public TValue this[TKey5 key] { get { return GetValueByKey5Internal(key); } set { SetValueByKey5Internal(key, value); } }
	public TValue this[TKey6 key] { get { return GetValueByKey6Internal(key); } set { SetValueByKey6Internal(key, value); } }

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> GetEntryByKey1(TKey1 key) { return GetEntryByKey1Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> GetEntryByKey2(TKey2 key) { return GetEntryByKey2Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> GetEntryByKey3(TKey3 key) { return GetEntryByKey3Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> GetEntryByKey4(TKey4 key) { return GetEntryByKey4Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> GetEntryByKey5(TKey5 key) { return GetEntryByKey5Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> GetEntryByKey6(TKey6 key) { return GetEntryByKey6Internal(key); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> entry) { return TryGetEntryByKey1Internal(key, out entry); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> entry) { return TryGetEntryByKey2Internal(key, out entry); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> entry) { return TryGetEntryByKey3Internal(key, out entry); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> entry) { return TryGetEntryByKey4Internal(key, out entry); }
	public bool TryGetEntryByKey5(TKey5 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> entry) { return TryGetEntryByKey5Internal(key, out entry); }
	public bool TryGetEntryByKey6(TKey6 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> entry) { return TryGetEntryByKey6Internal(key, out entry); }

	public TValue GetValueByKey1(TKey1 key) { return GetValueByKey1Internal(key); }
	public TValue GetValueByKey2(TKey2 key) { return GetValueByKey2Internal(key); }
	public TValue GetValueByKey3(TKey3 key) { return GetValueByKey3Internal(key); }
	public TValue GetValueByKey4(TKey4 key) { return GetValueByKey4Internal(key); }
	public TValue GetValueByKey5(TKey5 key) { return GetValueByKey5Internal(key); }
	public TValue GetValueByKey6(TKey6 key) { return GetValueByKey6Internal(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue value) { return TryGetValueByKey1Internal(key, out value); }
	public bool TryGetValueByKey2(TKey2 key, out TValue value) { return TryGetValueByKey2Internal(key, out value); }
	public bool TryGetValueByKey3(TKey3 key, out TValue value) { return TryGetValueByKey3Internal(key, out value); }
	public bool TryGetValueByKey4(TKey4 key, out TValue value) { return TryGetValueByKey4Internal(key, out value); }
	public bool TryGetValueByKey5(TKey5 key, out TValue value) { return TryGetValueByKey5Internal(key, out value); }
	public bool TryGetValueByKey6(TKey6 key, out TValue value) { return TryGetValueByKey6Internal(key, out value); }

	public bool SetValueByKey1(TKey1 key, TValue value) { return SetValueByKey1Internal(key, value); }
	public bool SetValueByKey2(TKey2 key, TValue value) { return SetValueByKey2Internal(key, value); }
	public bool SetValueByKey3(TKey3 key, TValue value) { return SetValueByKey3Internal(key, value); }
	public bool SetValueByKey4(TKey4 key, TValue value) { return SetValueByKey4Internal(key, value); }
	public bool SetValueByKey5(TKey5 key, TValue value) { return SetValueByKey5Internal(key, value); }
	public bool SetValueByKey6(TKey6 key, TValue value) { return SetValueByKey6Internal(key, value); }
	
	public void RemoveByKey1(TKey1 key) { RemoveByKey1Internal(key); }
	public void RemoveByKey2(TKey2 key) { RemoveByKey2Internal(key); }
	public void RemoveByKey3(TKey3 key) { RemoveByKey3Internal(key); }
	public void RemoveByKey4(TKey4 key) { RemoveByKey4Internal(key); }
	public void RemoveByKey5(TKey5 key) { RemoveByKey5Internal(key); }
	public void RemoveByKey6(TKey6 key) { RemoveByKey6Internal(key); }

	public bool TryRemoveByKey1(TKey1 key) { return TryRemoveByKey1Internal(key); }
	public bool TryRemoveByKey2(TKey2 key) { return TryRemoveByKey2Internal(key); }
	public bool TryRemoveByKey3(TKey3 key) { return TryRemoveByKey3Internal(key); }
	public bool TryRemoveByKey4(TKey4 key) { return TryRemoveByKey4Internal(key); }
	public bool TryRemoveByKey5(TKey5 key) { return TryRemoveByKey5Internal(key); }
	public bool TryRemoveByKey6(TKey6 key) { return TryRemoveByKey6Internal(key); }

	public bool ContainsKey1(TKey1 key) { return ContainsKey1Internal(key); }
	public bool ContainsKey2(TKey2 key) { return ContainsKey2Internal(key); }
	public bool ContainsKey3(TKey3 key) { return ContainsKey3Internal(key); }
	public bool ContainsKey4(TKey4 key) { return ContainsKey4Internal(key); }
	public bool ContainsKey5(TKey5 key) { return ContainsKey5Internal(key); }
	public bool ContainsKey6(TKey6 key) { return ContainsKey6Internal(key); }
}



/// <summary>
/// Note: Currently does not support handling hash code collisions
/// </summary>
[System.Serializable]
public class MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue>
	: MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass,
		MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue>, TValue>
{
	public MultiKeyDict()  { }
	public MultiKeyDict(params MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue>[] entries) {
		if (entries == null) return;
		foreach (var entry in entries) {
			this.Add(entry); //This also calls CheckIfEntryHasValue()
		}
	}
	
	public ReadOnlyCollection<TKey1> Key1s { get { return GetKey1s(); } }
	public ReadOnlyCollection<TKey2> Key2s { get { return GetKey2s(); } }
	public ReadOnlyCollection<TKey3> Key3s { get { return GetKey3s(); } }
	public ReadOnlyCollection<TKey4> Key4s { get { return GetKey4s(); } }
	public ReadOnlyCollection<TKey5> Key5s { get { return GetKey5s(); } }
	public ReadOnlyCollection<TKey6> Key6s { get { return GetKey6s(); } }
	public ReadOnlyCollection<TKey7> Key7s { get { return GetKey7s(); } }
	
	public void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TValue value) {
		AddInternal(value, key1, key2, key3, key4, key5, key6, key7, VoidClass.Void);
	}
	
	public TValue this[TKey1 key] { get { return GetValueByKey1Internal(key); } set { SetValueByKey1Internal(key, value); } }
	public TValue this[TKey2 key] { get { return GetValueByKey2Internal(key); } set { SetValueByKey2Internal(key, value); } }
	public TValue this[TKey3 key] { get { return GetValueByKey3Internal(key); } set { SetValueByKey3Internal(key, value); } }
	public TValue this[TKey4 key] { get { return GetValueByKey4Internal(key); } set { SetValueByKey4Internal(key, value); } }
	public TValue this[TKey5 key] { get { return GetValueByKey5Internal(key); } set { SetValueByKey5Internal(key, value); } }
	public TValue this[TKey6 key] { get { return GetValueByKey6Internal(key); } set { SetValueByKey6Internal(key, value); } }
	public TValue this[TKey7 key] { get { return GetValueByKey7Internal(key); } set { SetValueByKey7Internal(key, value); } }

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> GetEntryByKey1(TKey1 key) { return GetEntryByKey1Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> GetEntryByKey2(TKey2 key) { return GetEntryByKey2Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> GetEntryByKey3(TKey3 key) { return GetEntryByKey3Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> GetEntryByKey4(TKey4 key) { return GetEntryByKey4Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> GetEntryByKey5(TKey5 key) { return GetEntryByKey5Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> GetEntryByKey6(TKey6 key) { return GetEntryByKey6Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> GetEntryByKey7(TKey7 key) { return GetEntryByKey7Internal(key); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> entry) { return TryGetEntryByKey1Internal(key, out entry); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> entry) { return TryGetEntryByKey2Internal(key, out entry); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> entry) { return TryGetEntryByKey3Internal(key, out entry); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> entry) { return TryGetEntryByKey4Internal(key, out entry); }
	public bool TryGetEntryByKey5(TKey5 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> entry) { return TryGetEntryByKey5Internal(key, out entry); }
	public bool TryGetEntryByKey6(TKey6 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> entry) { return TryGetEntryByKey6Internal(key, out entry); }
	public bool TryGetEntryByKey7(TKey7 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> entry) { return TryGetEntryByKey7Internal(key, out entry); }

	public TValue GetValueByKey1(TKey1 key) { return GetValueByKey1Internal(key); }
	public TValue GetValueByKey2(TKey2 key) { return GetValueByKey2Internal(key); }
	public TValue GetValueByKey3(TKey3 key) { return GetValueByKey3Internal(key); }
	public TValue GetValueByKey4(TKey4 key) { return GetValueByKey4Internal(key); }
	public TValue GetValueByKey5(TKey5 key) { return GetValueByKey5Internal(key); }
	public TValue GetValueByKey6(TKey6 key) { return GetValueByKey6Internal(key); }
	public TValue GetValueByKey7(TKey7 key) { return GetValueByKey7Internal(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue value) { return TryGetValueByKey1Internal(key, out value); }
	public bool TryGetValueByKey2(TKey2 key, out TValue value) { return TryGetValueByKey2Internal(key, out value); }
	public bool TryGetValueByKey3(TKey3 key, out TValue value) { return TryGetValueByKey3Internal(key, out value); }
	public bool TryGetValueByKey4(TKey4 key, out TValue value) { return TryGetValueByKey4Internal(key, out value); }
	public bool TryGetValueByKey5(TKey5 key, out TValue value) { return TryGetValueByKey5Internal(key, out value); }
	public bool TryGetValueByKey6(TKey6 key, out TValue value) { return TryGetValueByKey6Internal(key, out value); }
	public bool TryGetValueByKey7(TKey7 key, out TValue value) { return TryGetValueByKey7Internal(key, out value); }

	public bool SetValueByKey1(TKey1 key, TValue value) { return SetValueByKey1Internal(key, value); }
	public bool SetValueByKey2(TKey2 key, TValue value) { return SetValueByKey2Internal(key, value); }
	public bool SetValueByKey3(TKey3 key, TValue value) { return SetValueByKey3Internal(key, value); }
	public bool SetValueByKey4(TKey4 key, TValue value) { return SetValueByKey4Internal(key, value); }
	public bool SetValueByKey5(TKey5 key, TValue value) { return SetValueByKey5Internal(key, value); }
	public bool SetValueByKey6(TKey6 key, TValue value) { return SetValueByKey6Internal(key, value); }
	public bool SetValueByKey7(TKey7 key, TValue value) { return SetValueByKey7Internal(key, value); }
	
	public void RemoveByKey1(TKey1 key) { RemoveByKey1Internal(key); }
	public void RemoveByKey2(TKey2 key) { RemoveByKey2Internal(key); }
	public void RemoveByKey3(TKey3 key) { RemoveByKey3Internal(key); }
	public void RemoveByKey4(TKey4 key) { RemoveByKey4Internal(key); }
	public void RemoveByKey5(TKey5 key) { RemoveByKey5Internal(key); }
	public void RemoveByKey6(TKey6 key) { RemoveByKey6Internal(key); }
	public void RemoveByKey7(TKey7 key) { RemoveByKey7Internal(key); }

	public bool TryRemoveByKey1(TKey1 key) { return TryRemoveByKey1Internal(key); }
	public bool TryRemoveByKey2(TKey2 key) { return TryRemoveByKey2Internal(key); }
	public bool TryRemoveByKey3(TKey3 key) { return TryRemoveByKey3Internal(key); }
	public bool TryRemoveByKey4(TKey4 key) { return TryRemoveByKey4Internal(key); }
	public bool TryRemoveByKey5(TKey5 key) { return TryRemoveByKey5Internal(key); }
	public bool TryRemoveByKey6(TKey6 key) { return TryRemoveByKey6Internal(key); }
	public bool TryRemoveByKey7(TKey7 key) { return TryRemoveByKey7Internal(key); }

	public bool ContainsKey1(TKey1 key) { return ContainsKey1Internal(key); }
	public bool ContainsKey2(TKey2 key) { return ContainsKey2Internal(key); }
	public bool ContainsKey3(TKey3 key) { return ContainsKey3Internal(key); }
	public bool ContainsKey4(TKey4 key) { return ContainsKey4Internal(key); }
	public bool ContainsKey5(TKey5 key) { return ContainsKey5Internal(key); }
	public bool ContainsKey6(TKey6 key) { return ContainsKey6Internal(key); }
	public bool ContainsKey7(TKey7 key) { return ContainsKey7Internal(key); }
}



/// <summary>
/// Note: Currently does not support handling hash code collisions
/// </summary>
[System.Serializable]
public class MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>
	: MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8,
		MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>, TValue>
{
	public MultiKeyDict()  { }
	public MultiKeyDict(params MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>[] entries) {
		if (entries == null) return;
		foreach (var entry in entries) {
			this.Add(entry); //This also calls CheckIfEntryHasValue()
		}
	}
	
	public ReadOnlyCollection<TKey1> Key1s { get { return GetKey1s(); } }
	public ReadOnlyCollection<TKey2> Key2s { get { return GetKey2s(); } }
	public ReadOnlyCollection<TKey3> Key3s { get { return GetKey3s(); } }
	public ReadOnlyCollection<TKey4> Key4s { get { return GetKey4s(); } }
	public ReadOnlyCollection<TKey5> Key5s { get { return GetKey5s(); } }
	public ReadOnlyCollection<TKey6> Key6s { get { return GetKey6s(); } }
	public ReadOnlyCollection<TKey7> Key7s { get { return GetKey7s(); } }
	public ReadOnlyCollection<TKey8> Key8s { get { return GetKey8s(); } }
	
	public void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TKey8 key8, TValue value) {
		AddInternal(value, key1, key2, key3, key4, key5, key6, key7, key8);
	}
	
	public TValue this[TKey1 key] { get { return GetValueByKey1Internal(key); } set { SetValueByKey1Internal(key, value); } }
	public TValue this[TKey2 key] { get { return GetValueByKey2Internal(key); } set { SetValueByKey2Internal(key, value); } }
	public TValue this[TKey3 key] { get { return GetValueByKey3Internal(key); } set { SetValueByKey3Internal(key, value); } }
	public TValue this[TKey4 key] { get { return GetValueByKey4Internal(key); } set { SetValueByKey4Internal(key, value); } }
	public TValue this[TKey5 key] { get { return GetValueByKey5Internal(key); } set { SetValueByKey5Internal(key, value); } }
	public TValue this[TKey6 key] { get { return GetValueByKey6Internal(key); } set { SetValueByKey6Internal(key, value); } }
	public TValue this[TKey7 key] { get { return GetValueByKey7Internal(key); } set { SetValueByKey7Internal(key, value); } }
	public TValue this[TKey8 key] { get { return GetValueByKey8Internal(key); } set { SetValueByKey8Internal(key, value); } }

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> GetEntryByKey1(TKey1 key) { return GetEntryByKey1Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> GetEntryByKey2(TKey2 key) { return GetEntryByKey2Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> GetEntryByKey3(TKey3 key) { return GetEntryByKey3Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> GetEntryByKey4(TKey4 key) { return GetEntryByKey4Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> GetEntryByKey5(TKey5 key) { return GetEntryByKey5Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> GetEntryByKey6(TKey6 key) { return GetEntryByKey6Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> GetEntryByKey7(TKey7 key) { return GetEntryByKey7Internal(key); }
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> GetEntryByKey8(TKey8 key) { return GetEntryByKey8Internal(key); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) { return TryGetEntryByKey1Internal(key, out entry); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) { return TryGetEntryByKey2Internal(key, out entry); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) { return TryGetEntryByKey3Internal(key, out entry); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) { return TryGetEntryByKey4Internal(key, out entry); }
	public bool TryGetEntryByKey5(TKey5 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) { return TryGetEntryByKey5Internal(key, out entry); }
	public bool TryGetEntryByKey6(TKey6 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) { return TryGetEntryByKey6Internal(key, out entry); }
	public bool TryGetEntryByKey7(TKey7 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) { return TryGetEntryByKey7Internal(key, out entry); }
	public bool TryGetEntryByKey8(TKey8 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) { return TryGetEntryByKey8Internal(key, out entry); }

	public TValue GetValueByKey1(TKey1 key) { return GetValueByKey1Internal(key); }
	public TValue GetValueByKey2(TKey2 key) { return GetValueByKey2Internal(key); }
	public TValue GetValueByKey3(TKey3 key) { return GetValueByKey3Internal(key); }
	public TValue GetValueByKey4(TKey4 key) { return GetValueByKey4Internal(key); }
	public TValue GetValueByKey5(TKey5 key) { return GetValueByKey5Internal(key); }
	public TValue GetValueByKey6(TKey6 key) { return GetValueByKey6Internal(key); }
	public TValue GetValueByKey7(TKey7 key) { return GetValueByKey7Internal(key); }
	public TValue GetValueByKey8(TKey8 key) { return GetValueByKey8Internal(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue value) { return TryGetValueByKey1Internal(key, out value); }
	public bool TryGetValueByKey2(TKey2 key, out TValue value) { return TryGetValueByKey2Internal(key, out value); }
	public bool TryGetValueByKey3(TKey3 key, out TValue value) { return TryGetValueByKey3Internal(key, out value); }
	public bool TryGetValueByKey4(TKey4 key, out TValue value) { return TryGetValueByKey4Internal(key, out value); }
	public bool TryGetValueByKey5(TKey5 key, out TValue value) { return TryGetValueByKey5Internal(key, out value); }
	public bool TryGetValueByKey6(TKey6 key, out TValue value) { return TryGetValueByKey6Internal(key, out value); }
	public bool TryGetValueByKey7(TKey7 key, out TValue value) { return TryGetValueByKey7Internal(key, out value); }
	public bool TryGetValueByKey8(TKey8 key, out TValue value) { return TryGetValueByKey8Internal(key, out value); }

	public bool SetValueByKey1(TKey1 key, TValue value) { return SetValueByKey1Internal(key, value); }
	public bool SetValueByKey2(TKey2 key, TValue value) { return SetValueByKey2Internal(key, value); }
	public bool SetValueByKey3(TKey3 key, TValue value) { return SetValueByKey3Internal(key, value); }
	public bool SetValueByKey4(TKey4 key, TValue value) { return SetValueByKey4Internal(key, value); }
	public bool SetValueByKey5(TKey5 key, TValue value) { return SetValueByKey5Internal(key, value); }
	public bool SetValueByKey6(TKey6 key, TValue value) { return SetValueByKey6Internal(key, value); }
	public bool SetValueByKey7(TKey7 key, TValue value) { return SetValueByKey7Internal(key, value); }
	public bool SetValueByKey8(TKey8 key, TValue value) { return SetValueByKey8Internal(key, value); }
	
	public void RemoveByKey1(TKey1 key) { RemoveByKey1Internal(key); }
	public void RemoveByKey2(TKey2 key) { RemoveByKey2Internal(key); }
	public void RemoveByKey3(TKey3 key) { RemoveByKey3Internal(key); }
	public void RemoveByKey4(TKey4 key) { RemoveByKey4Internal(key); }
	public void RemoveByKey5(TKey5 key) { RemoveByKey5Internal(key); }
	public void RemoveByKey6(TKey6 key) { RemoveByKey6Internal(key); }
	public void RemoveByKey7(TKey7 key) { RemoveByKey7Internal(key); }
	public void RemoveByKey8(TKey8 key) { RemoveByKey8Internal(key); }

	public bool TryRemoveByKey1(TKey1 key) { return TryRemoveByKey1Internal(key); }
	public bool TryRemoveByKey2(TKey2 key) { return TryRemoveByKey2Internal(key); }
	public bool TryRemoveByKey3(TKey3 key) { return TryRemoveByKey3Internal(key); }
	public bool TryRemoveByKey4(TKey4 key) { return TryRemoveByKey4Internal(key); }
	public bool TryRemoveByKey5(TKey5 key) { return TryRemoveByKey5Internal(key); }
	public bool TryRemoveByKey6(TKey6 key) { return TryRemoveByKey6Internal(key); }
	public bool TryRemoveByKey7(TKey7 key) { return TryRemoveByKey7Internal(key); }
	public bool TryRemoveByKey8(TKey8 key) { return TryRemoveByKey8Internal(key); }

	public bool ContainsKey1(TKey1 key) { return ContainsKey1Internal(key); }
	public bool ContainsKey2(TKey2 key) { return ContainsKey2Internal(key); }
	public bool ContainsKey3(TKey3 key) { return ContainsKey3Internal(key); }
	public bool ContainsKey4(TKey4 key) { return ContainsKey4Internal(key); }
	public bool ContainsKey5(TKey5 key) { return ContainsKey5Internal(key); }
	public bool ContainsKey6(TKey6 key) { return ContainsKey6Internal(key); }
	public bool ContainsKey7(TKey7 key) { return ContainsKey7Internal(key); }
	public bool ContainsKey8(TKey8 key) { return ContainsKey8Internal(key); }
}


//*/