﻿/*

using System;
using System.Linq;

public interface IMultiKeyDictEntry : ICloneable {
	bool HasValue { get; }
	int KeyCount { get; }
	Type[] GetTypeParameters();
	object GetUntypedValue();
	object GetUntypedKey1();
	object GetUntypedKey2();
	object GetUntypedKey3();
	object GetUntypedKey4();
	object GetUntypedKey5();
	object GetUntypedKey6();
	object GetUntypedKey7();
	object GetUntypedKey8();
}

public interface IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> : IMultiKeyDictEntry
{
	TValue Value { get; set; }
	TKey1 GetKey1();
	TKey2 GetKey2();
	TKey3 GetKey3();
	TKey4 GetKey4();
	TKey5 GetKey5();
	TKey6 GetKey6();
	TKey7 GetKey7();
	TKey8 GetKey8();
}

public static class MultiKeyDictEntry
{
	public static MultiKeyDictEntry<TKey1,                                                  TValue> Create<TKey1,                                                  TValue>(TKey1 key1,                                                                                     TValue value) { return new MultiKeyDictEntry<TKey1,                                                  TValue>(value, key1                                          ); }
	public static MultiKeyDictEntry<TKey1, TKey2,                                           TValue> Create<TKey1, TKey2,                                           TValue>(TKey1 key1, TKey2 key2,                                                                         TValue value) { return new MultiKeyDictEntry<TKey1, TKey2,                                           TValue>(value, key1, key2                                    ); }
	public static MultiKeyDictEntry<TKey1, TKey2, TKey3,                                    TValue> Create<TKey1, TKey2, TKey3,                                    TValue>(TKey1 key1, TKey2 key2, TKey3 key3,                                                             TValue value) { return new MultiKeyDictEntry<TKey1, TKey2, TKey3,                                    TValue>(value, key1, key2, key3                              ); }
	public static MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4,                             TValue> Create<TKey1, TKey2, TKey3, TKey4,                             TValue>(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4,                                                 TValue value) { return new MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4,                             TValue>(value, key1, key2, key3, key4                        ); }
	public static MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5,                      TValue> Create<TKey1, TKey2, TKey3, TKey4, TKey5,                      TValue>(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5,                                     TValue value) { return new MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5,                      TValue>(value, key1, key2, key3, key4, key5                  ); }
	public static MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6,               TValue> Create<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6,               TValue>(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6,                         TValue value) { return new MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6,               TValue>(value, key1, key2, key3, key4, key5, key6            ); }
	public static MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7,        TValue> Create<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7,        TValue>(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7,             TValue value) { return new MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7,        TValue>(value, key1, key2, key3, key4, key5, key6, key7      ); }
	public static MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> Create<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TKey8 key8, TValue value) { return new MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>(value, key1, key2, key3, key4, key5, key6, key7, key8); }

	public static string EntryToString<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>(IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry)
	{
		if (entry == null) return "null";
#pragma warning disable 0184, 0162 //Unity shows these (incorrectly) for some reason (also note putting 'CS' in front of the numbers doesn't work with Unity)
		if (entry is MultiKeyDictEntry<TKey1                                                 , TValue>) return NormalEntryToString(entry.HasValue, TypeCollectionBuilder.Create<TKey1,                                                  TValue>().AsArray(), entry.GetKey1(),                                                                                                                        entry.Value);
		if (entry is MultiKeyDictEntry<TKey1, TKey2                                          , TValue>) return NormalEntryToString(entry.HasValue, TypeCollectionBuilder.Create<TKey1, TKey2,                                           TValue>().AsArray(), entry.GetKey1(), entry.GetKey2(),                                                                                                       entry.Value);
		if (entry is MultiKeyDictEntry<TKey1, TKey2, TKey3                                   , TValue>) return NormalEntryToString(entry.HasValue, TypeCollectionBuilder.Create<TKey1, TKey2, TKey3,                                    TValue>().AsArray(), entry.GetKey1(), entry.GetKey2(), entry.GetKey3(),                                                                                      entry.Value);
		if (entry is MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4                            , TValue>) return NormalEntryToString(entry.HasValue, TypeCollectionBuilder.Create<TKey1, TKey2, TKey3, TKey4,                             TValue>().AsArray(), entry.GetKey1(), entry.GetKey2(), entry.GetKey3(), entry.GetKey4(),                                                                     entry.Value);
		if (entry is MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5                     , TValue>) return NormalEntryToString(entry.HasValue, TypeCollectionBuilder.Create<TKey1, TKey2, TKey3, TKey4, TKey5,                      TValue>().AsArray(), entry.GetKey1(), entry.GetKey2(), entry.GetKey3(), entry.GetKey4(), entry.GetKey5(),                                                    entry.Value);
		if (entry is MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6              , TValue>) return NormalEntryToString(entry.HasValue, TypeCollectionBuilder.Create<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6,               TValue>().AsArray(), entry.GetKey1(), entry.GetKey2(), entry.GetKey3(), entry.GetKey4(), entry.GetKey5(), entry.GetKey6(),                                   entry.Value);
		if (entry is MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7       , TValue>) return NormalEntryToString(entry.HasValue, TypeCollectionBuilder.Create<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7,        TValue>().AsArray(), entry.GetKey1(), entry.GetKey2(), entry.GetKey3(), entry.GetKey4(), entry.GetKey5(), entry.GetKey6(), entry.GetKey7(),                  entry.Value);
		if (entry is MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>) return NormalEntryToString(entry.HasValue, TypeCollectionBuilder.Create<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>().AsArray(), entry.GetKey1(), entry.GetKey2(), entry.GetKey3(), entry.GetKey4(), entry.GetKey5(), entry.GetKey6(), entry.GetKey7(), entry.GetKey8(), entry.Value);
#pragma warning restore 0184, 0162
		else return UnknownEntryToString(entry);
	}

	private static string NormalEntryToString(bool hasValue, Type[] types, params object[] values)
	{
		string res = "";
		res += "IMultiKeyDictEntry<";
		res += string.Join(", ", types.Select(x => x.Name).ToArray());
		if (hasValue) {
			res += "> { ";
			for (int i = 0; i < values.Length - 1; i++) { //Loop through all but the last
				res += "Key" + i + ": " + ToString(values[i]) + ", ";
			}
			res += "Value: " + ToString(values[values.Length - 1]);
			res += " }";
		} else {
			res += "> { HasValue = False }";
		}
		return res;
	}

	private static string UnknownEntryToString<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>(IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry)
	{
		if (entry.HasValue) {
			string res = "";
			res += entry.GetType().FullName;
			res += " { ";
			res += "Key1: ";  try { res += ToString(entry.GetKey1()); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; } res += ", ";
			res += "Key2: ";  try { res += ToString(entry.GetKey2()); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; } res += ", ";
			res += "Key3: ";  try { res += ToString(entry.GetKey3()); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; } res += ", ";
			res += "Key4: ";  try { res += ToString(entry.GetKey4()); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; } res += ", ";
			res += "Key5: ";  try { res += ToString(entry.GetKey5()); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; } res += ", ";
			res += "Key6: ";  try { res += ToString(entry.GetKey6()); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; } res += ", ";
			res += "Key7: ";  try { res += ToString(entry.GetKey7()); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; } res += ", ";
			res += "Key8: ";  try { res += ToString(entry.GetKey8()); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; } res += ", ";
			res += "Value: "; try { res += ToString(entry.Value    ); } catch (Exception e) { res += "<Error: " + ToString(e) + ">"; }
			res += " } ";
			return res;
		} else {
			return entry.GetType().FullName + " { HasValue = False }";
		}
	}

	private static string ToString(object obj) {
		if (obj == null) return "null";
		string asStr = obj.ToString();
		if (string.IsNullOrEmpty(asStr)) return "\"\"";
		else return asStr;
	}

	public static int GetEntryHashCode<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>(IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry)
	{
		if (entry == null) return 0;
		else if (!entry.HasValue) return 0;

		TKey1 key1 = entry.GetKey1();
		TKey2 key2 = entry.GetKey2();
		TKey3 key3 = entry.GetKey3();
		TKey4 key4 = entry.GetKey4();
		TKey5 key5 = entry.GetKey5();
		TKey6 key6 = entry.GetKey6();
		TKey7 key7 = entry.GetKey7();
		TKey8 key8 = entry.GetKey8();
		TValue val  = entry.Value;

		return Obj.CombineHashCodes(
			key1 == null ? 0 : key1.GetHashCode(),
			key2 == null ? 0 : key2.GetHashCode(),
			key3 == null ? 0 : key3.GetHashCode(),
			key4 == null ? 0 : key4.GetHashCode(),
			key5 == null ? 0 : key5.GetHashCode(),
			key6 == null ? 0 : key6.GetHashCode(),
			key7 == null ? 0 : key7.GetHashCode(),
			key8 == null ? 0 : key8.GetHashCode(),
			val  == null ? 0 : val .GetHashCode()
		);
	}

	public static bool EntriesEqual<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>(IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> left, IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> right)
	{
		if (ReferenceEquals(left, null) && ReferenceEquals(right, null)) return true;
		else if (ReferenceEquals(left, null) || ReferenceEquals(right, null)) return false;

		if (!left.HasValue && !right.HasValue) return true;
		else if (!left.HasValue || !right.HasValue) return false;

		if (left.GetType() != right.GetType()) return false;

		return (
			   EqualsInternal(left.GetKey1(), right.GetKey1())
			&& EqualsInternal(left.GetKey2(), right.GetKey2())
			&& EqualsInternal(left.GetKey3(), right.GetKey3())
			&& EqualsInternal(left.GetKey4(), right.GetKey4())
			&& EqualsInternal(left.GetKey5(), right.GetKey5())
			&& EqualsInternal(left.GetKey6(), right.GetKey6())
			&& EqualsInternal(left.GetKey7(), right.GetKey7())
			&& EqualsInternal(left.GetKey8(), right.GetKey8())
			&& EqualsInternal(left.Value, right.Value)
		);
	}

	private static bool EqualsInternal<T>(T left, T right)
	{
		if (ReferenceEquals(left, null) && ReferenceEquals(right, null)) return true;
		else if (ReferenceEquals(left, null) && right.Equals(null)) return true;
		else if (ReferenceEquals(right, null) && left.Equals(null)) return true;
		else if (left.Equals(right) || right.Equals(left)) return true;
		else return false;
	}

	public static bool EntryEqualsObject<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>(IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry, object obj)
	{
		bool entryNull = ReferenceEquals(entry, null);
		bool objNull = ReferenceEquals(obj, null);
		if (entryNull && objNull) return true;
		else if (entryNull || objNull) return false;

		var objAsEntry = obj as IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>;
		if (!ReferenceEquals(objAsEntry, null)) {
			return EntriesEqual(entry, objAsEntry);
		} else {
			return false;
		}
	}
}

[System.Serializable]
public struct MultiKeyDictEntry<TKey1, TValue> : IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>
{
	private bool _hasValue;
	public bool HasValue { get { return _hasValue; } }
	public TValue Value { get; set; }
	public readonly TKey1 Key1;

	int IMultiKeyDictEntry.KeyCount { get { return 1; } }
	Type[] IMultiKeyDictEntry.GetTypeParameters() { return new Type[] { typeof(TKey1), typeof(TValue) }; }
	object IMultiKeyDictEntry.GetUntypedValue() { return Value; }
	TKey1     IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey1() { return Key1; }
	VoidClass IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey2() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey3() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey4() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey5() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey6() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey7() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey8() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey1() { return Key1; }
	object IMultiKeyDictEntry.GetUntypedKey2() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey3() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey4() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey5() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey6() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey7() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey8() { return VoidClass.Void; }

	public MultiKeyDictEntry(TValue value, TKey1 key1) {
		this._hasValue = true;
		this.Value = value;
		this.Key1 = key1;
	}

	public MultiKeyDictEntry<TKey1, TValue> Clone() { return this; }
	object ICloneable.Clone() { return this; }
	public override bool Equals(object obj) { return MultiKeyDictEntry.EntryEqualsObject(this, obj); }
	public bool Equals(MultiKeyDictEntry<TKey1, TValue> other){ return MultiKeyDictEntry.EntriesEqual(this, other); }
	public override int GetHashCode() { return MultiKeyDictEntry.GetEntryHashCode(this); }
	public override string ToString() { return MultiKeyDictEntry.EntryToString(this); }
}

[System.Serializable]
public struct MultiKeyDictEntry<TKey1, TKey2, TValue> : IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>
{
	private bool _hasValue;
	public bool HasValue { get { return _hasValue; } }
	public TValue Value { get; set; }
	public readonly TKey1 Key1;
	public readonly TKey2 Key2;

	int IMultiKeyDictEntry.KeyCount { get { return 2; } }
	Type[] IMultiKeyDictEntry.GetTypeParameters() { return new Type[] { typeof(TKey1), typeof(TKey2), typeof(TValue) }; }
	object IMultiKeyDictEntry.GetUntypedValue() { return Value; }
	TKey1     IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey1() { return Key1; }
	TKey2     IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey2() { return Key2; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey3() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey4() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey5() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey6() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey7() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey8() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey1() { return Key1; }
	object IMultiKeyDictEntry.GetUntypedKey2() { return Key2; }
	object IMultiKeyDictEntry.GetUntypedKey3() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey4() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey5() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey6() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey7() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey8() { return VoidClass.Void; }

	public MultiKeyDictEntry(TValue value, TKey1 key1, TKey2 key2) {
		this._hasValue = true;
		this.Value = value;
		this.Key1 = key1;
		this.Key2 = key2;
	}

	public MultiKeyDictEntry<TKey1, TKey2, TValue> Clone() { return this; }
	object ICloneable.Clone() { return this; }
	public override bool Equals(object obj) { return MultiKeyDictEntry.EntryEqualsObject(this, obj); }
	public bool Equals(MultiKeyDictEntry<TKey1, TKey2, TValue> other){ return MultiKeyDictEntry.EntriesEqual(this, other); }
	public override int GetHashCode() { return MultiKeyDictEntry.GetEntryHashCode(this); }
	public override string ToString() { return MultiKeyDictEntry.EntryToString(this); }
}

[System.Serializable]
public struct MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> : IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>
{
	private bool _hasValue;
	public bool HasValue { get { return _hasValue; } }
	public TValue Value { get; set; }
	public readonly TKey1 Key1;
	public readonly TKey2 Key2;
	public readonly TKey3 Key3;

	int IMultiKeyDictEntry.KeyCount { get { return 3; } }
	Type[] IMultiKeyDictEntry.GetTypeParameters() { return new Type[] { typeof(TKey1), typeof(TKey2), typeof(TKey3), typeof(TValue) }; }
	object IMultiKeyDictEntry.GetUntypedValue() { return Value; }
	TKey1     IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey1() { return Key1; }
	TKey2     IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey2() { return Key2; }
	TKey3     IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey3() { return Key3; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey4() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey5() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey6() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey7() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey8() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey1() { return Key1; }
	object IMultiKeyDictEntry.GetUntypedKey2() { return Key2; }
	object IMultiKeyDictEntry.GetUntypedKey3() { return Key3; }
	object IMultiKeyDictEntry.GetUntypedKey4() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey5() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey6() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey7() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey8() { return VoidClass.Void; }

	public MultiKeyDictEntry(TValue value, TKey1 key1, TKey2 key2, TKey3 key3) {
		this._hasValue = true;
		this.Value = value;
		this.Key1 = key1;
		this.Key2 = key2;
		this.Key3 = key3;
	}
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> Clone() { return this; }
	object ICloneable.Clone() { return this; }
	public override bool Equals(object obj) { return MultiKeyDictEntry.EntryEqualsObject(this, obj); }
	public bool Equals(MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> other){ return MultiKeyDictEntry.EntriesEqual(this, other); }
	public override int GetHashCode() { return MultiKeyDictEntry.GetEntryHashCode(this); }
	public override string ToString() { return MultiKeyDictEntry.EntryToString(this); }
}

[System.Serializable]
public struct MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> : IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>
{
	private bool _hasValue;
	public bool HasValue { get { return _hasValue; } }
	public TValue Value { get; set; }
	public readonly TKey1 Key1;
	public readonly TKey2 Key2;
	public readonly TKey3 Key3;
	public readonly TKey4 Key4;

	int IMultiKeyDictEntry.KeyCount { get { return 4; } }
	Type[] IMultiKeyDictEntry.GetTypeParameters() { return new Type[] { typeof(TKey1), typeof(TKey2), typeof(TKey3), typeof(TKey4), typeof(TValue) }; }
	object IMultiKeyDictEntry.GetUntypedValue() { return Value; }
	TKey1     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey1() { return Key1; }
	TKey2     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey2() { return Key2; }
	TKey3     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey3() { return Key3; }
	TKey4     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey4() { return Key4; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey5() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey6() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey7() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, VoidClass, VoidClass, VoidClass, VoidClass, TValue>.GetKey8() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey1() { return Key1; }
	object IMultiKeyDictEntry.GetUntypedKey2() { return Key2; }
	object IMultiKeyDictEntry.GetUntypedKey3() { return Key3; }
	object IMultiKeyDictEntry.GetUntypedKey4() { return Key4; }
	object IMultiKeyDictEntry.GetUntypedKey5() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey6() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey7() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey8() { return VoidClass.Void; }

	public MultiKeyDictEntry(TValue value, TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4) {
		this._hasValue = true;
		this.Value = value;
		this.Key1 = key1;
		this.Key2 = key2;
		this.Key3 = key3;
		this.Key4 = key4;
	}
	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> Clone() { return this; }
	object ICloneable.Clone() { return this; }
	public override bool Equals(object obj) { return MultiKeyDictEntry.EntryEqualsObject(this, obj); }
	public bool Equals(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> other){ return MultiKeyDictEntry.EntriesEqual(this, other); }
	public override int GetHashCode() { return MultiKeyDictEntry.GetEntryHashCode(this); }
	public override string ToString() { return MultiKeyDictEntry.EntryToString(this); }
}

[System.Serializable]
public struct MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> : IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>
{
	private bool _hasValue;
	public bool HasValue { get { return _hasValue; } }
	public TValue Value { get; set; }
	public readonly TKey1 Key1;
	public readonly TKey2 Key2;
	public readonly TKey3 Key3;
	public readonly TKey4 Key4;
	public readonly TKey5 Key5;

	int IMultiKeyDictEntry.KeyCount { get { return 5; } }
	Type[] IMultiKeyDictEntry.GetTypeParameters() { return new Type[] { typeof(TKey1), typeof(TKey2), typeof(TKey3), typeof(TKey4), typeof(TKey5), typeof(TValue) }; }
	object IMultiKeyDictEntry.GetUntypedValue() { return Value; }
	TKey1     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>.GetKey1() { return Key1; }
	TKey2     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>.GetKey2() { return Key2; }
	TKey3     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>.GetKey3() { return Key3; }
	TKey4     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>.GetKey4() { return Key4; }
	TKey5     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>.GetKey5() { return Key5; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>.GetKey6() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>.GetKey7() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, VoidClass, VoidClass, VoidClass, TValue>.GetKey8() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey1() { return Key1; }
	object IMultiKeyDictEntry.GetUntypedKey2() { return Key2; }
	object IMultiKeyDictEntry.GetUntypedKey3() { return Key3; }
	object IMultiKeyDictEntry.GetUntypedKey4() { return Key4; }
	object IMultiKeyDictEntry.GetUntypedKey5() { return Key5; }
	object IMultiKeyDictEntry.GetUntypedKey6() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey7() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey8() { return VoidClass.Void; }

	public MultiKeyDictEntry(TValue value, TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5) {
		this._hasValue = true;
		this.Value = value;
		this.Key1 = key1;
		this.Key2 = key2;
		this.Key3 = key3;
		this.Key4 = key4;
		this.Key5 = key5;
	}

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> Clone() { return this; }
	object ICloneable.Clone() { return this; }
	public override bool Equals(object obj) { return MultiKeyDictEntry.EntryEqualsObject(this, obj); }
	public bool Equals(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> other){ return MultiKeyDictEntry.EntriesEqual(this, other); }
	public override int GetHashCode() { return MultiKeyDictEntry.GetEntryHashCode(this); }
	public override string ToString() { return MultiKeyDictEntry.EntryToString(this); }
}

[System.Serializable]
public struct MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> : IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>
{
	private bool _hasValue;
	public bool HasValue { get { return _hasValue; } }
	public TValue Value { get; set; }
	public readonly TKey1 Key1;
	public readonly TKey2 Key2;
	public readonly TKey3 Key3;
	public readonly TKey4 Key4;
	public readonly TKey5 Key5;
	public readonly TKey6 Key6;

	int IMultiKeyDictEntry.KeyCount { get { return 6; } }
	Type[] IMultiKeyDictEntry.GetTypeParameters() { return new Type[] { typeof(TKey1), typeof(TKey2), typeof(TKey3), typeof(TKey4), typeof(TKey5), typeof(TKey6), typeof(TValue) }; }
	object IMultiKeyDictEntry.GetUntypedValue() { return Value; }
	TKey1     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>.GetKey1() { return Key1; }
	TKey2     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>.GetKey2() { return Key2; }
	TKey3     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>.GetKey3() { return Key3; }
	TKey4     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>.GetKey4() { return Key4; }
	TKey5     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>.GetKey5() { return Key5; }
	TKey6     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>.GetKey6() { return Key6; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>.GetKey7() { return VoidClass.Void; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, VoidClass, VoidClass, TValue>.GetKey8() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey1() { return Key1; }
	object IMultiKeyDictEntry.GetUntypedKey2() { return Key2; }
	object IMultiKeyDictEntry.GetUntypedKey3() { return Key3; }
	object IMultiKeyDictEntry.GetUntypedKey4() { return Key4; }
	object IMultiKeyDictEntry.GetUntypedKey5() { return Key5; }
	object IMultiKeyDictEntry.GetUntypedKey6() { return Key6; }
	object IMultiKeyDictEntry.GetUntypedKey7() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey8() { return VoidClass.Void; }

	public MultiKeyDictEntry(TValue value, TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6) {
		this._hasValue = true;
		this.Value = value;
		this.Key1 = key1;
		this.Key2 = key2;
		this.Key3 = key3;
		this.Key4 = key4;
		this.Key5 = key5;
		this.Key6 = key6;
	}

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> Clone() { return this; }
	object ICloneable.Clone() { return this; }
	public override bool Equals(object obj) { return MultiKeyDictEntry.EntryEqualsObject(this, obj); }
	public bool Equals(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> other){ return MultiKeyDictEntry.EntriesEqual(this, other); }
	public override int GetHashCode() { return MultiKeyDictEntry.GetEntryHashCode(this); }
	public override string ToString() { return MultiKeyDictEntry.EntryToString(this); }
}

[System.Serializable]
public struct MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> : IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>
{
	private bool _hasValue;
	public bool HasValue { get { return _hasValue; } }
	public TValue Value { get; set; }
	public readonly TKey1 Key1;
	public readonly TKey2 Key2;
	public readonly TKey3 Key3;
	public readonly TKey4 Key4;
	public readonly TKey5 Key5;
	public readonly TKey6 Key6;
	public readonly TKey7 Key7;

	int IMultiKeyDictEntry.KeyCount { get { return 7; } }
	Type[] IMultiKeyDictEntry.GetTypeParameters() { return new Type[] { typeof(TKey1), typeof(TKey2), typeof(TKey3), typeof(TKey4), typeof(TKey5), typeof(TKey6), typeof(TKey7), typeof(TValue) }; }
	object IMultiKeyDictEntry.GetUntypedValue() { return Value; }
	TKey1     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>.GetKey1() { return Key1; }
	TKey2     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>.GetKey2() { return Key2; }
	TKey3     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>.GetKey3() { return Key3; }
	TKey4     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>.GetKey4() { return Key4; }
	TKey5     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>.GetKey5() { return Key5; }
	TKey6     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>.GetKey6() { return Key6; }
	TKey7     IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>.GetKey7() { return Key7; }
	VoidClass IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, VoidClass, TValue>.GetKey8() { return VoidClass.Void; }
	object IMultiKeyDictEntry.GetUntypedKey1() { return Key1; }
	object IMultiKeyDictEntry.GetUntypedKey2() { return Key2; }
	object IMultiKeyDictEntry.GetUntypedKey3() { return Key3; }
	object IMultiKeyDictEntry.GetUntypedKey4() { return Key4; }
	object IMultiKeyDictEntry.GetUntypedKey5() { return Key5; }
	object IMultiKeyDictEntry.GetUntypedKey6() { return Key6; }
	object IMultiKeyDictEntry.GetUntypedKey7() { return Key7; }
	object IMultiKeyDictEntry.GetUntypedKey8() { return VoidClass.Void; }

	public MultiKeyDictEntry(TValue value, TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7) {
		this._hasValue = true;
		this.Value = value;
		this.Key1 = key1;
		this.Key2 = key2;
		this.Key3 = key3;
		this.Key4 = key4;
		this.Key5 = key5;
		this.Key6 = key6;
		this.Key7 = key7;
	}

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> Clone() { return this; }
	object ICloneable.Clone() { return this; }
	public override bool Equals(object obj) { return MultiKeyDictEntry.EntryEqualsObject(this, obj); }
	public bool Equals(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> other){ return MultiKeyDictEntry.EntriesEqual(this, other); }
	public override int GetHashCode() { return MultiKeyDictEntry.GetEntryHashCode(this); }
	public override string ToString() { return MultiKeyDictEntry.EntryToString(this); }
}

[System.Serializable]
public struct MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> : IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>
{
	private bool _hasValue;
	public bool HasValue { get { return _hasValue; } }
	public TValue Value { get; set; }
	public readonly TKey1 Key1;
	public readonly TKey2 Key2;
	public readonly TKey3 Key3;
	public readonly TKey4 Key4;
	public readonly TKey5 Key5;
	public readonly TKey6 Key6;
	public readonly TKey7 Key7;
	public readonly TKey8 Key8;

	int IMultiKeyDictEntry.KeyCount { get { return 8; } }
	Type[] IMultiKeyDictEntry.GetTypeParameters() { return new Type[] { typeof(TKey1), typeof(TKey2), typeof(TKey3), typeof(TKey4), typeof(TKey5), typeof(TKey6), typeof(TKey7), typeof(TKey8), typeof(TValue) }; }
	object IMultiKeyDictEntry.GetUntypedValue() { return Value; }
	TKey1 IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>.GetKey1() { return Key1; }
	TKey2 IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>.GetKey2() { return Key2; }
	TKey3 IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>.GetKey3() { return Key3; }
	TKey4 IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>.GetKey4() { return Key4; }
	TKey5 IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>.GetKey5() { return Key5; }
	TKey6 IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>.GetKey6() { return Key6; }
	TKey7 IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>.GetKey7() { return Key7; }
	TKey8 IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>.GetKey8() { return Key8; }
	object IMultiKeyDictEntry.GetUntypedKey1() { return Key1; }
	object IMultiKeyDictEntry.GetUntypedKey2() { return Key2; }
	object IMultiKeyDictEntry.GetUntypedKey3() { return Key3; }
	object IMultiKeyDictEntry.GetUntypedKey4() { return Key4; }
	object IMultiKeyDictEntry.GetUntypedKey5() { return Key5; }
	object IMultiKeyDictEntry.GetUntypedKey6() { return Key6; }
	object IMultiKeyDictEntry.GetUntypedKey7() { return Key7; }
	object IMultiKeyDictEntry.GetUntypedKey8() { return Key8; }

	public MultiKeyDictEntry(TValue value, TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TKey8 key8) {
		this._hasValue = true;
		this.Value = value;
		this.Key1 = key1;
		this.Key2 = key2;
		this.Key3 = key3;
		this.Key4 = key4;
		this.Key5 = key5;
		this.Key6 = key6;
		this.Key7 = key7;
		this.Key8 = key8;
	}

	public MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> Clone() { return this; }
	object ICloneable.Clone() { return this; }
	public override bool Equals(object obj) { return MultiKeyDictEntry.EntryEqualsObject(this, obj); }
	public bool Equals(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> other){ return MultiKeyDictEntry.EntriesEqual(this, other); }
	public override int GetHashCode() { return MultiKeyDictEntry.GetEntryHashCode(this); }
	public override string ToString() { return MultiKeyDictEntry.EntryToString(this); }
}




//*/