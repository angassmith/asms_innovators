﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

/// <summary>
/// Use 'using PropagatingOperators;' to enable propagating operator extension methods
/// </summary>
namespace PropagatingOperators
{

	public static class General
	{
		/// <summary>
		/// True if an item in objs is null, otherwise, false. False if objs itself is null (it contains no null items)
		/// </summary>
		public static bool AnyNull(params object[] objs) {
			if (objs == null) return false;
			for (int i = 0; i < objs.Length; i++) {
				if (objs[i] == null) return true;
			}
			return false;
		}

		//	public static FirstTypeInferredHolder<TSource> Infer<TSource>(this TSource source) {
		//		return new FirstTypeInferredHolder<TSource>(source);
		//	}
	}

	#region NullProp

	/// <summary>
	/// Use 'using PropagatingOperators;' to enable propagating operator extension methods
	/// </summary>
	/// <remarks>
	/// Note that using an extension method on a null object does not throw a NullReferenceException
	/// </remarks>
	public static class NullProp
	{
		/// <summary>
		/// Null-propagating operator, for class return types.
		/// <para/>
		/// Example: var foo = someGameObject.transform.parent.NullP(x => x.gameObject.GetComponen&lt;Camera&gt;()).NullP(x => x.targetTexture);
		/// </summary>
		/// <remarks>
		/// -	The following are identical:
		///		var foo = someGameObject.transform.parent.NullP(x => x.gameObject.GetComponen&lt;Camera&gt;()).NullP(x => x.targetTexture);
		///		var foo = someGameObject.transform.parent.NullP(x => x.gameObject.GetComponen&lt;Camera&gt;().NullP(x => x.targetTexture));
		///	-	The following will fail when someGameObject.transform.parent is null:
		///		var foo = someGameObject.transform.parent.NullP(x => x.gameObject.GetComponent&lt;Camera&gt;()).targetTexture;
		/// </remarks>
		/// <param name="ignored">Used for overload resolution</param>
		public static TResult NullP<TSource, TResult>(this TSource source, Func<TSource, TResult> func)
			where TResult : class
		{
			if (source == null || func == null) return null;
			else return func(source);
		}

		/// <summary>
		/// Null-propagating operator, for Nullable-struct return types.
		/// </summary>
		/// <param name="ignored">Used for overload resolution</param>
		public static TResult? NullPS<TSource, TResult>(this TSource source, Func<TSource, TResult> func)
			where TResult : struct
		{
			if (source == null || func == null) return null;
			else return func(source);
		}

		/// <summary>
		/// Null-propagating operator, with no return type
		/// </summary>
		public static void NullP<TSource>(this TSource source, Action<TSource> action)
		{
			if (source != null && action != null) action(source);
		}

		/// <summary>
		/// Null-propagating operator for any return type
		/// </summary>
		public static Wrapper<TResult> NullP<TSource, TResult>(this Wrapper<TSource> source, Func<TSource, TResult> func) {
			return NullPInternal(source, func);
		}

		private static Wrapper<TResult> NullPInternal<TSource, TResult>(this Wrapper<TSource> source, Func<TSource, TResult> func)
		{
			if (source == null || func == null || source.Value == null) return null;
			else return new Wrapper<TResult>(func(source.Value));
		}

		/// <summary>Repeated null-propagating operator, for an eventual class return type, and any part-way return types.</summary>
		public static TR NullP<T1, T2, TR>(T1 source, Func<T1, T2> func1, Func<T2, TR> func2) where TR : class {
			if (General.AnyNull(func1, func2)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullP(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for an eventual nullable-struct return type, and any part-way return types.</summary>
		public static TR? NullPS<T1, T2, TR>(T1 source, Func<T1, T2> func1, Func<T2, TR> func2) where TR : struct {
			if (General.AnyNull(func1, func2)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPS(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for any part-way and final return types.</summary>
		public static Wrapper<TR> NullP<T1, T2, TR>(Wrapper<T1> source, Func<T1, T2> func1, Func<T2, TR> func2) {
			if (General.AnyNull(source, func1, func2)) return null; else return source.NullPInternal(func1).NullPInternal(func2);
		}

		/// <summary>Repeated null-propagating operator, for an eventual class return type, and any part-way return types.</summary>
		public static TR NullP<T1, T2, T3, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, TR> func3) where TR : class {
			if (General.AnyNull(func1, func2, func3)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullP(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for an eventual nullable-struct return type, and any part-way return types.</summary>
		public static TR? NullPS<T1, T2, T3, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, TR> func3) where TR : struct {
			if (General.AnyNull(func1, func2, func3)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPS(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for any part-way and final return types.</summary>
		public static Wrapper<TR> NullP<T1, T2, T3, TR>(Wrapper<T1> source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, TR> func3) {
			if (General.AnyNull(func1, func2, func3)) return null; else return source.NullPInternal(func1).NullPInternal(func2).NullPInternal(func3);
		}

		/// <summary>Repeated null-propagating operator, for an eventual class return type, and any part-way return types.</summary>
		public static TR NullP<T1, T2, T3, T4, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, TR> func4) where TR : class {
			if (General.AnyNull(func1, func2, func3, func4)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullP(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for an eventual nullable-struct return type, and any part-way return types.</summary>
		public static TR? NullPS<T1, T2, T3, T4, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, TR> func4) where TR : struct {
			if (General.AnyNull(func1, func2, func3, func4)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPS(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for any part-way and final return types.</summary>
		public static Wrapper<TR> NullP<T1, T2, T3, T4, TR>(Wrapper<T1> source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, TR> func4) {
			if (General.AnyNull(func1, func2, func3, func4)) return null; else return source.NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4);
		}

		/// <summary>Repeated null-propagating operator, for an eventual class return type, and any part-way return types.</summary>
		public static TR NullP<T1, T2, T3, T4, T5, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, TR> func5) where TR : class {
			if (General.AnyNull(func1, func2, func3, func4, func5)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullP(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for an eventual nullable-struct return type, and any part-way return types.</summary>
		public static TR? NullPS<T1, T2, T3, T4, T5, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, TR> func5) where TR : struct {
			if (General.AnyNull(func1, func2, func3, func4, func5)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPS(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for any part-way and final return types.</summary>
		public static Wrapper<TR> NullP<T1, T2, T3, T4, T5, TR>(Wrapper<T1> source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, TR> func5) {
			if (General.AnyNull(func1, func2, func3, func4, func5)) return null; else return source.NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5);
		}

		/// <summary>Repeated null-propagating operator, for an eventual class return type, and any part-way return types.</summary>
		public static TR NullP<T1, T2, T3, T4, T5, T6, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, TR> func6) where TR : class {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6).NullP(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for an eventual nullable-struct return type, and any part-way return types.</summary>
		public static TR? NullPS<T1, T2, T3, T4, T5, T6, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, TR> func6) where TR : struct {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6).NullPS(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for any part-way and final return types.</summary>
		public static Wrapper<TR> NullP<T1, T2, T3, T4, T5, T6, TR>(Wrapper<T1> source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, TR> func6) {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6)) return null; else return source.NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6);
		}

		/// <summary>Repeated null-propagating operator, for an eventual class return type, and any part-way return types.</summary>
		public static TR NullP<T1, T2, T3, T4, T5, T6, T7, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, T7> func6, Func<T7, TR> func7) where TR : class {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6, func7)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6).NullPInternal(func7).NullP(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for an eventual nullable-struct return type, and any part-way return types.</summary>
		public static TR? NullPS<T1, T2, T3, T4, T5, T6, T7, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, T7> func6, Func<T7, TR> func7) where TR : struct {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6, func7)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6).NullPInternal(func7).NullPS(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for any part-way and final return types.</summary>
		public static Wrapper<TR> NullP<T1, T2, T3, T4, T5, T6, T7, TR>(Wrapper<T1> source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, T7> func6, Func<T7, TR> func7) {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6, func7)) return null; else return source.NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6).NullPInternal(func7);
		}

		/// <summary>Repeated null-propagating operator, for an eventual class return type, and any part-way return types.</summary>
		public static TR NullP<T1, T2, T3, T4, T5, T6, T7, T8, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, T7> func6, Func<T7, T8> func7, Func<T8, TR> func8) where TR : class {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6, func7, func8)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6).NullPInternal(func7).NullPInternal(func8).NullP(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for an eventual nullable-struct return type, and any part-way return types.</summary>
		public static TR? NullPS<T1, T2, T3, T4, T5, T6, T7, T8, TR>(T1 source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, T7> func6, Func<T7, T8> func7, Func<T8, TR> func8) where TR : struct {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6, func7, func8)) return null; else return new Wrapper<T1>(source).NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6).NullPInternal(func7).NullPInternal(func8).NullPS(x => x.Value);
		}
		/// <summary>Repeated null-propagating operator, for any part-way and final return types.</summary>
		public static Wrapper<TR> NullP<T1, T2, T3, T4, T5, T6, T7, T8, TR>(Wrapper<T1> source, Func<T1, T2> func1, Func<T2, T3> func2, Func<T3, T4> func3, Func<T4, T5> func4, Func<T5, T6> func5, Func<T6, T7> func6, Func<T7, T8> func7, Func<T8, TR> func8) {
			if (General.AnyNull(func1, func2, func3, func4, func5, func6, func7, func8)) return null; else return source.NullPInternal(func1).NullPInternal(func2).NullPInternal(func3).NullPInternal(func4).NullPInternal(func5).NullPInternal(func6).NullPInternal(func7).NullPInternal(func8);
		}
		
		
	}

	#endregion

	#region ThrowIfProp

	public static class ThrowIfProp
	{
		public static T ThrowIfNull<T>(this T source, string name = null, string message = null) {
			if (ReferenceEquals(source, null)) {
				throw new ArgumentNullException(name, message);
			} else {
				return source;
			}
		}

		/// <summary>
		/// Attempts to downcast source to TResult, throwing useful error messages if the cast fails.
		/// </summary>
		public static TResult DowncastElseThrow<TSource, TResult>(this TSource source, string name = null)
			where TResult : TSource
		{
			if (typeof(TSource) == typeof(TResult)) return (TResult)source;

			if (ReferenceEquals(source, null))
			{
				if (!typeof(TResult).IsValueType) {
					return default(TResult); // = null because of the above check
				} else {
					throw new InvalidCastException(
						"Converting object "
						+ (string.IsNullOrEmpty(name) ? "" : ("contained in member or parameter '" + name + "' "))
						+ " to type '"
						+ typeof(TResult).FullName
						+ "' failed as the destination type is a value type and the source object was null."
					);
				}
			}

			if (source is TResult) {
				return (TResult)source;
			} else {
				throw new InvalidCastException(
					"Converting object (of type '"
					+ source.GetType().FullName
					+ "' and with string representation '"
					+ source.ToString()
					+ "') "
					+ (string.IsNullOrEmpty(name) ? "" : (" contained in member or parameter '" + name + "' "))
					+ "to type '"
					+ typeof(TResult).FullName
					+ "' failed as the source object was not of a type equal to or inheriting from the destination type."
				);
			}
			

			//Doing "(TResult)(object)source;" will not call user defined casts.
			//Doing so is only possible with reflection or using dynamic.
			//Found online:
			//	
			//	(TResult)(dynamic)source;
			//	Source: http://stackoverflow.com/a/6795648/4149474
			//	
			//	public static T2 Convert<T1, T2>(T1 obj)
			//	{
			//		var conversionOperator = typeof(T1).GetMethods(BindingFlags.Static | BindingFlags.Public)
			//		.Where(m => m.Name == "op_Explicit" || m.Name == "op_Implicit")
			//		.Where(m => m.ReturnType == typeof(T2))
			//		.Where(m => m.GetParameters().Length == 1 && m.GetParameters()[0].ParameterType == typeof(T1))
			//		.FirstOrDefault();
			//		
			//		if (conversionOperator != null)
			//			return (T2)conversionOperator.Invoke(null, new object[]{obj});
			//		
			//		throw new Exception("No conversion operator found");
			//	}
			//	Source: http://stackoverflow.com/a/16396341/4149474
			//	
			//	public static T To<T> (object obj)
			//	{
			//		Type sourceType = obj.GetType ();
			//		MethodInfo op = sourceType.GetMethods ()
			//						.Where (m => m.ReturnType == typeof (T))
			//						.Where (m => m.Name == "op_Implicit" || m.Name == "op_Explicit")
			//						.FirstOrDefault();
			//		
			//		return (op != null)
			//			? (T) op.Invoke (null, new [] { obj })
			//			: (T) Convert.ChangeType (obj, typeof (T));
			//	}
			//	Source: http://stackoverflow.com/a/6795745/4149474
			
		}

		/// <summary>
		/// Example: <see cref="string"/> x = ((<see cref="object"/>)"foo").DowncastElseThrow().DowncastElseThrow&lt;<see cref="string"/>&gt;();
		/// </summary>
		public static FirstTypeInferredHolder<TSource> DowncastElseThrow<TSource>(this TSource source) {
			return new FirstTypeInferredHolder<TSource>(source);
		}

		
	}

	#endregion


	#region ReplaceProp

	public static class ReplaceProp
	{
		/// <summary>
		/// Uses ReferenceEquals to compare <paramref name="source"/>, <paramref name="match"/>, and <see cref="null"/>.
		/// If all comparisons fail, uses the default <see cref="EqualityComparer{T}"/> to determine if
		/// <paramref name="source"/> is equal to <paramref name="match"/>.
		/// If source is equal to <paramref name="match"/>, returns <paramref name="replacement"/>,
		/// otherwise returns <paramref name="source"/>. 
		/// </summary>
		/// <remarks>
		/// The default EqualityComparer is given by EqualityComparer&lt;T&gt;.Default. The .NET documentation says: "The Default property checks whether type T implements the System.IEquatable&lt;T&gt; generic interface and if so returns an EqualityComparer&lt;T&gt; that uses that implementation. Otherwise it returns an EqualityComparer&lt;T&gt; that uses the overrides of Object.Equals and Object.GetHashCode provided by T."
		/// </remarks>
		public static T Replace<T>(this T source, T match, T replacement = default(T)) {
			if (ReferenceEquals(source, match)) return replacement;
			else if (ReferenceEquals(source, null)) return source;
			else if (ReferenceEquals(match, null)) return source;
			else return EqualityComparer<T>.Default.Equals(source, match) ? replacement : source;
		}

		/// <summary>
		/// Uses ReferenceEquals to compare <paramref name="source"/>, <paramref name="match"/>, and <see cref="null"/>.
		/// If all comparisons fail, uses the default <see cref="EqualityComparer{T}"/> to determine equality.
		/// If source is equal to <paramref name="match"/>, returns <paramref name="replacement"/>,
		/// otherwise returns <paramref name="source"/>. 
		/// </summary>
		public static T ReplaceDef<T>(this T source, T replacement = default(T)) {
			if (ReferenceEquals(source, null)) return replacement;
			else return EqualityComparer<T>.Default.Equals(source, default(T)) ? replacement : source;
		}

		/// <summary>
		/// Uses <paramref name="comparer"/> to determine if <paramref name="source"/> is equal to <paramref name="match"/>.
		/// If so, returns <paramref name="replacement"/>, otherwise returns <paramref name="source"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException">comparer is null</exception>
		public static T Replace<T>(this T source, T match,  IEqualityComparer<T> comparer, T replacement)
		{
			if (ReferenceEquals(comparer, null)) throw new ArgumentNullException("comparer");
			return comparer.Equals(source, match) ? replacement : source;
		}

		/// <summary>
		/// Uses <paramref name="comparer"/> to determine if <paramref name="source"/>
		/// is equal to default(<typeparamref name="T"/>). If so, returns <paramref name="replacement"/>,
		/// otherwise returns <paramref name="source"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException">comparer is null</exception>
		public static T ReplaceDef<T>(this T source,  IEqualityComparer<T> comparer, T replacement)
		{
			if (ReferenceEquals(comparer, null)) throw new ArgumentNullException("comparer");
			return comparer.Equals(source, default(T)) ? replacement : source;
		}

		/// <summary>
		/// Uses <paramref name="predicate"/> to determine if <paramref name="source"/> should be replaced.
		/// If so, returns <paramref name="replacement"/>, otherwise returns <paramref name="source"/>
		/// </summary>
		/// <exception cref="ArgumentException">selector is null</exception>
		public static T Replace<T>(this T source, Func<T, bool> predicate, T replacement = default(T)) {
			if (ReferenceEquals(predicate, null)) throw new ArgumentNullException("selector");
			return predicate(source) ? replacement : source;
		}
	}

	#endregion

	public class FirstTypeInferredHolder<TSource>
	{
		public readonly TSource Source;

		public FirstTypeInferredHolder(TSource source) {
			this.Source = source;
		}

		public TResult DowncastElseThrow<TResult>(string name = null)
			where TResult : TSource
		{
			return Source.DowncastElseThrow<TSource, TResult>(name);
		}
	}
	

	//Maybe add other propagating operators in future

	public abstract class ClassConstraint<T> where T : class {
		private ClassConstraint() { }
	}

	public static class Wrapper {
		public static Wrapper<T> Create<T>(T value) {
			return new Wrapper<T>(value);
		}
	}
	public sealed class Wrapper<T> {
		public readonly T Value;
		public Wrapper(T value) {
			this.Value = value;
		}
	}
}

//*/