﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections;
using Save;


public class SecondInspector : EditorWindow {

	[SerializeField]
	public UnityEngine.Object InspectedObj;
	[SerializeField]
	public bool FoldoutInspector = true;
	[SerializeField]
	public Vector2 ScrollPosition;

	[MenuItem ("Window/Second Inspector")]
	public static void Open()
	{
		SecondInspector testWin = (SecondInspector)EditorWindow.GetWindow(typeof(SecondInspector), false, "2nd Inspector");
		testWin.minSize = new Vector2(280, 100);
	}
	
	void OnGUI() {
		//Show object selection field
		InspectedObj = EditorGUILayout.ObjectField(InspectedObj, typeof(UnityEngine.Object), true);

		//If the displayed object is not null, show a representation of the object that can be dragged into other fields
		if (InspectedObj != null)
		{
			//Start a scroll view
			ScrollPosition = EditorGUILayout.BeginScrollView(ScrollPosition, false, false);
			{

				FoldoutInspector = EditorGUILayout.InspectorTitlebar(FoldoutInspector, InspectedObj, true);

				//Show the inspector-section if the above inspector-title-bar is folded out
				if (FoldoutInspector)
				{
					Editor displayedObjEditor = Editor.CreateEditor(InspectedObj);
					// ^ Recreating the editor each time (or does it?) may or may not be slow.
					try {
						displayedObjEditor.OnInspectorGUI();
					} catch (Exception e) { Debug.LogException(e); }
				}
			}
			EditorGUILayout.EndScrollView();
		}
	}
}

#endif