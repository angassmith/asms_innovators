﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public interface IMultiKeyDictBase : IEnumerable
{
	int Count { get; }
	int Capacity { get; }
	int KeyCount { get; }
	void Clear();
}

public interface IMultiKeyDictBaseTEntry<TEntry> : IMultiKeyDictBase, IEnumerable<TEntry>
	where TEntry : struct
{
	ReadOnlyCollection<TEntry> Entries { get; }
	void Add(TEntry entry);
	bool ContainsEntry(TEntry entry);
	bool ContainsAnyKeysOfEntry(TEntry entry);
	void CopyTo(TEntry[] array, int arrayIndex);
	TEntry[] ToArray();
	IEnumerable<TEntry> EnumerateEntries();
	IEnumerable<TEntry> EnumerateEntries_Enumerable();
	IEnumerator<TEntry> EnumerateEntries_Enumerator();
}

public interface IMultiKeyDictBaseTValue<TValue> : IMultiKeyDictBase
{
	ReadOnlyCollection<TValue> Values { get; }
	bool ContainsValue(TValue value);
	bool ContainsValue(TValue value, IEqualityComparer<TValue> comparer);
	IEnumerable<TValue> EnumerateValues();
	IEnumerable<TValue> EnumerateValues_Enumerable();
	IEnumerator<TValue> EnumerateValues_Enumerator();
}

public interface IMultiKeyDictBase<TEntry, TValue> : IMultiKeyDictBaseTEntry<TEntry>, IMultiKeyDictBaseTValue<TValue>
	where TEntry : struct
{
	
}


[System.Serializable]
public abstract class MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TEntry, TValue>
	: IMultiKeyDictBase<TEntry, TValue>,  IEnumerable<TEntry>
	where TEntry : struct, IMultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>
{
	private readonly Dictionary<int, uint> Key1IndexLookup;
	private readonly Dictionary<int, uint> Key2IndexLookup;
	private readonly Dictionary<int, uint> Key3IndexLookup;
	private readonly Dictionary<int, uint> Key4IndexLookup;
	private readonly Dictionary<int, uint> Key5IndexLookup;
	private readonly Dictionary<int, uint> Key6IndexLookup;
	private readonly Dictionary<int, uint> Key7IndexLookup;
	private readonly Dictionary<int, uint> Key8IndexLookup;
	private TEntry[] IndexedValues = new TEntry[16];

	private uint NextNewIndex = 0; //Also the Count of IndexedValues (where counting includes freed items)
	private readonly Stack<uint> NextFreeIndices = new Stack<uint>();

	private readonly int _keyCount;
	public int KeyCount { get { return _keyCount; } }

	



	protected MultiKeyDictBase()
	{
		//	if (keyCount < 1 || keyCount > 8) throw new ArgumentOutOfRangeException("keyCount", keyCount, "A MultiKeyDictionaryBase derived class must have between 1 and 8 keys (inclusive)");
		//	this.KeyCount = keyCount;
		
		if      (typeof(TEntry) == typeof(MultiKeyDictEntry<TKey1,                                                  TValue>)) _keyCount = 1;
		else if (typeof(TEntry) == typeof(MultiKeyDictEntry<TKey1, TKey2,                                           TValue>)) _keyCount = 2;
		else if (typeof(TEntry) == typeof(MultiKeyDictEntry<TKey1, TKey2, TKey3,                                    TValue>)) _keyCount = 3;
		else if (typeof(TEntry) == typeof(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4,                             TValue>)) _keyCount = 4;
		else if (typeof(TEntry) == typeof(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5,                      TValue>)) _keyCount = 5;
		else if (typeof(TEntry) == typeof(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6,               TValue>)) _keyCount = 6;
		else if (typeof(TEntry) == typeof(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7,        TValue>)) _keyCount = 7;
		else if (typeof(TEntry) == typeof(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>)) _keyCount = 8;
		else { throw new ArgumentException("The type argument TEntry provided to MultiKeyDictBase when deriving from it must be one of the predefined MultiKeyDictEntry<...> structs (with the correct type arguments). '" + typeof(TEntry).FullName + "' is not one of these structs."); }

		if (KeyCount >= 1) Key1IndexLookup = new Dictionary<int, uint>();
		if (KeyCount >= 2) Key2IndexLookup = new Dictionary<int, uint>();
		if (KeyCount >= 3) Key3IndexLookup = new Dictionary<int, uint>();
		if (KeyCount >= 4) Key4IndexLookup = new Dictionary<int, uint>();
		if (KeyCount >= 5) Key5IndexLookup = new Dictionary<int, uint>();
		if (KeyCount >= 6) Key6IndexLookup = new Dictionary<int, uint>();
		if (KeyCount >= 7) Key7IndexLookup = new Dictionary<int, uint>();
		if (KeyCount >= 8) Key8IndexLookup = new Dictionary<int, uint>();
	}




	public int Count { get { return Key1IndexLookup.Count; } }
	public int Capacity { get { return IndexedValues.Length; } }

	public ReadOnlyCollection<TEntry> Entries {
		get {
			TEntry[] entries = new TEntry[this.Count];
			int i = 0;
			foreach (var item in this.EnumerateEntries()) {
				entries[i] = item;
				i++;
			}
			return Array.AsReadOnly(entries);
		}
	}
	public ReadOnlyCollection<TValue> Values {
		get {
			TValue[] values = new TValue[this.Count];
			int i = 0;
			foreach (var item in this.EnumerateValues()) {
				values[i] = item;
				i++;
			}
			return Array.AsReadOnly(values);
		}
	}
	protected ReadOnlyCollection<TKey1> GetKey1s() { TKey1[] keys = new TKey1[this.Count]; int i = 0; foreach (var item in this.EnumerateEntries()) { keys[i] = (TKey1)item.GetKey1(); i++; } return Array.AsReadOnly(keys); }
	protected ReadOnlyCollection<TKey2> GetKey2s() { TKey2[] keys = new TKey2[this.Count]; int i = 0; foreach (var item in this.EnumerateEntries()) { keys[i] = (TKey2)item.GetKey2(); i++; } return Array.AsReadOnly(keys); }
	protected ReadOnlyCollection<TKey3> GetKey3s() { TKey3[] keys = new TKey3[this.Count]; int i = 0; foreach (var item in this.EnumerateEntries()) { keys[i] = (TKey3)item.GetKey3(); i++; } return Array.AsReadOnly(keys); }
	protected ReadOnlyCollection<TKey4> GetKey4s() { TKey4[] keys = new TKey4[this.Count]; int i = 0; foreach (var item in this.EnumerateEntries()) { keys[i] = (TKey4)item.GetKey4(); i++; } return Array.AsReadOnly(keys); }
	protected ReadOnlyCollection<TKey5> GetKey5s() { TKey5[] keys = new TKey5[this.Count]; int i = 0; foreach (var item in this.EnumerateEntries()) { keys[i] = (TKey5)item.GetKey5(); i++; } return Array.AsReadOnly(keys); }
	protected ReadOnlyCollection<TKey6> GetKey6s() { TKey6[] keys = new TKey6[this.Count]; int i = 0; foreach (var item in this.EnumerateEntries()) { keys[i] = (TKey6)item.GetKey6(); i++; } return Array.AsReadOnly(keys); }
	protected ReadOnlyCollection<TKey7> GetKey7s() { TKey7[] keys = new TKey7[this.Count]; int i = 0; foreach (var item in this.EnumerateEntries()) { keys[i] = (TKey7)item.GetKey7(); i++; } return Array.AsReadOnly(keys); }
	protected ReadOnlyCollection<TKey8> GetKey8s() { TKey8[] keys = new TKey8[this.Count]; int i = 0; foreach (var item in this.EnumerateEntries()) { keys[i] = (TKey8)item.GetKey8(); i++; } return Array.AsReadOnly(keys); }




	protected void CheckIfKeyNull(object key) {
		if (key == null) throw new ArgumentNullException("A dictionary key cannot be null");
	}
	protected void CheckIfEntryHasValue(TEntry entry) {
		if (!entry.HasValue) throw new ArgumentException("Only MultiKeyDictEntries that have been created using the non-parameterless constructor can be added to a MultiKeyDict (using this constructor sets HasValue to true).");
	}


	public void Add(TEntry entry)
	{
		CheckIfEntryHasValue(entry);
		AddInternal(entry.Value, entry.GetKey1(), entry.GetKey2(), entry.GetKey3(), entry.GetKey4(), entry.GetKey5(), entry.GetKey6(), entry.GetKey7(), entry.GetKey8());
	}

	protected void AddInternal(TValue value, TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TKey8 key8)
	{

		int key1hc = 0;
		int key2hc = 0;
		int key3hc = 0;
		int key4hc = 0;
		int key5hc = 0;
		int key6hc = 0;
		int key7hc = 0;
		int key8hc = 0;
		if (KeyCount >= 1) { CheckIfKeyNull(key1); key1hc = key1.GetHashCode(); if (Key1IndexLookup.ContainsKey(key1hc)) throw new ArgumentException("An element with the same key#1 HashCode is already present in the MultiKeyDictionaryBase", "key#1"); }
		if (KeyCount >= 2) { CheckIfKeyNull(key2); key2hc = key2.GetHashCode(); if (Key2IndexLookup.ContainsKey(key2hc)) throw new ArgumentException("An element with the same key#2 HashCode is already present in the MultiKeyDictionaryBase", "key#2"); }
		if (KeyCount >= 3) { CheckIfKeyNull(key3); key3hc = key3.GetHashCode(); if (Key3IndexLookup.ContainsKey(key3hc)) throw new ArgumentException("An element with the same key#3 HashCode is already present in the MultiKeyDictionaryBase", "key#3"); }
		if (KeyCount >= 4) { CheckIfKeyNull(key4); key4hc = key4.GetHashCode(); if (Key4IndexLookup.ContainsKey(key4hc)) throw new ArgumentException("An element with the same key#4 HashCode is already present in the MultiKeyDictionaryBase", "key#4"); }
		if (KeyCount >= 5) { CheckIfKeyNull(key5); key5hc = key5.GetHashCode(); if (Key5IndexLookup.ContainsKey(key5hc)) throw new ArgumentException("An element with the same key#5 HashCode is already present in the MultiKeyDictionaryBase", "key#5"); }
		if (KeyCount >= 6) { CheckIfKeyNull(key6); key6hc = key6.GetHashCode(); if (Key6IndexLookup.ContainsKey(key6hc)) throw new ArgumentException("An element with the same key#6 HashCode is already present in the MultiKeyDictionaryBase", "key#6"); }
		if (KeyCount >= 7) { CheckIfKeyNull(key7); key7hc = key7.GetHashCode(); if (Key7IndexLookup.ContainsKey(key7hc)) throw new ArgumentException("An element with the same key#7 HashCode is already present in the MultiKeyDictionaryBase", "key#7"); }
		if (KeyCount >= 8) { CheckIfKeyNull(key8); key8hc = key8.GetHashCode(); if (Key8IndexLookup.ContainsKey(key8hc)) throw new ArgumentException("An element with the same key#8 HashCode is already present in the MultiKeyDictionaryBase", "key#8"); }


		//Get available index (must come after potential exceptions, to avoid extra pops/increments)
		uint index;
		if (NextFreeIndices.Count == 0) {
			index = NextNewIndex;
			NextNewIndex++;
		} else {
			index = NextFreeIndices.Pop();
		}

		//Add
		if (KeyCount >= 1) { Key1IndexLookup.Add(key1hc, index); }
		if (KeyCount >= 2) { Key2IndexLookup.Add(key2hc, index); }
		if (KeyCount >= 3) { Key3IndexLookup.Add(key3hc, index); }
		if (KeyCount >= 4) { Key4IndexLookup.Add(key4hc, index); }
		if (KeyCount >= 5) { Key5IndexLookup.Add(key5hc, index); }
		if (KeyCount >= 6) { Key6IndexLookup.Add(key6hc, index); }
		if (KeyCount >= 7) { Key7IndexLookup.Add(key7hc, index); }
		if (KeyCount >= 8) { Key8IndexLookup.Add(key8hc, index); }
		AddToIndexedValues(index, value, key1, key2, key3, key4, key5, key6, key7, key8);
	}

	private void AddToIndexedValues(uint index, TValue value, TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TKey8 key8)
	{
		//Reallocate to twice the size if needed
		if (index >= IndexedValues.Length)
		{
			TEntry[] oldIndexedValues = IndexedValues;
			IndexedValues = new TEntry[oldIndexedValues.Length * 2];
			oldIndexedValues.CopyTo(IndexedValues, 0);
		}

		//Set value at index
		if (KeyCount == 1) IndexedValues[index] = (TEntry)(IMultiKeyDictEntry<TKey1, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>)MultiKeyDictEntry.Create(key1,                                           value);
		if (KeyCount == 2) IndexedValues[index] = (TEntry)(IMultiKeyDictEntry<TKey1, TKey2    , VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>)MultiKeyDictEntry.Create(key1, key2,                                     value);
		if (KeyCount == 3) IndexedValues[index] = (TEntry)(IMultiKeyDictEntry<TKey1, TKey2    , TKey3    , VoidClass, VoidClass, VoidClass, VoidClass, VoidClass, TValue>)MultiKeyDictEntry.Create(key1, key2, key3,                               value);
		if (KeyCount == 4) IndexedValues[index] = (TEntry)(IMultiKeyDictEntry<TKey1, TKey2    , TKey3    , TKey4    , VoidClass, VoidClass, VoidClass, VoidClass, TValue>)MultiKeyDictEntry.Create(key1, key2, key3, key4,                         value);
		if (KeyCount == 5) IndexedValues[index] = (TEntry)(IMultiKeyDictEntry<TKey1, TKey2    , TKey3    , TKey4    , TKey5    , VoidClass, VoidClass, VoidClass, TValue>)MultiKeyDictEntry.Create(key1, key2, key3, key4, key5,                   value);
		if (KeyCount == 6) IndexedValues[index] = (TEntry)(IMultiKeyDictEntry<TKey1, TKey2    , TKey3    , TKey4    , TKey5    , TKey6    , VoidClass, VoidClass, TValue>)MultiKeyDictEntry.Create(key1, key2, key3, key4, key5, key6,             value);
		if (KeyCount == 7) IndexedValues[index] = (TEntry)(IMultiKeyDictEntry<TKey1, TKey2    , TKey3    , TKey4    , TKey5    , TKey6    , TKey7    , VoidClass, TValue>)MultiKeyDictEntry.Create(key1, key2, key3, key4, key5, key6, key7,       value);
		if (KeyCount == 8) IndexedValues[index] = (TEntry)(IMultiKeyDictEntry<TKey1, TKey2    , TKey3    , TKey4    , TKey5    , TKey6    , TKey7    , TKey8    , TValue>)MultiKeyDictEntry.Create(key1, key2, key3, key4, key5, key6, key7, key8, value);
	}




	protected TValue GetValueByKey1Internal(TKey1 key) { CheckIfKeyNull(key); return IndexedValues[Key1IndexLookup[key.GetHashCode()]].Value; }
	protected TValue GetValueByKey2Internal(TKey2 key) { CheckIfKeyNull(key); return IndexedValues[Key2IndexLookup[key.GetHashCode()]].Value; }
	protected TValue GetValueByKey3Internal(TKey3 key) { CheckIfKeyNull(key); return IndexedValues[Key3IndexLookup[key.GetHashCode()]].Value; }
	protected TValue GetValueByKey4Internal(TKey4 key) { CheckIfKeyNull(key); return IndexedValues[Key4IndexLookup[key.GetHashCode()]].Value; }
	protected TValue GetValueByKey5Internal(TKey5 key) { CheckIfKeyNull(key); return IndexedValues[Key5IndexLookup[key.GetHashCode()]].Value; }
	protected TValue GetValueByKey6Internal(TKey6 key) { CheckIfKeyNull(key); return IndexedValues[Key6IndexLookup[key.GetHashCode()]].Value; }
	protected TValue GetValueByKey7Internal(TKey7 key) { CheckIfKeyNull(key); return IndexedValues[Key7IndexLookup[key.GetHashCode()]].Value; }
	protected TValue GetValueByKey8Internal(TKey8 key) { CheckIfKeyNull(key); return IndexedValues[Key8IndexLookup[key.GetHashCode()]].Value; }

	//	protected TValue GetValueByKey1Internal(TKey1 key, out uint index) { CheckIfKeyNull(key); index = Key1IndexLookup[key.GetHashCode()]; return IndexedValues[index].Value; }
	//	protected TValue GetValueByKey2Internal(TKey2 key, out uint index) { CheckIfKeyNull(key); index = Key2IndexLookup[key.GetHashCode()]; return IndexedValues[index].Value; }
	//	protected TValue GetValueByKey3Internal(TKey3 key, out uint index) { CheckIfKeyNull(key); index = Key3IndexLookup[key.GetHashCode()]; return IndexedValues[index].Value; }
	//	protected TValue GetValueByKey4Internal(TKey4 key, out uint index) { CheckIfKeyNull(key); index = Key4IndexLookup[key.GetHashCode()]; return IndexedValues[index].Value; }
	//	protected TValue GetValueByKey5Internal(TKey5 key, out uint index) { CheckIfKeyNull(key); index = Key5IndexLookup[key.GetHashCode()]; return IndexedValues[index].Value; }
	//	protected TValue GetValueByKey6Internal(TKey6 key, out uint index) { CheckIfKeyNull(key); index = Key6IndexLookup[key.GetHashCode()]; return IndexedValues[index].Value; }
	//	protected TValue GetValueByKey7Internal(TKey7 key, out uint index) { CheckIfKeyNull(key); index = Key7IndexLookup[key.GetHashCode()]; return IndexedValues[index].Value; }
	//	protected TValue GetValueByKey8Internal(TKey8 key, out uint index) { CheckIfKeyNull(key); index = Key8IndexLookup[key.GetHashCode()]; return IndexedValues[index].Value; }

	protected bool TryGetValueByKey1Internal(TKey1 key, out TValue value) { CheckIfKeyNull(key); uint index; if (Key1IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey2Internal(TKey2 key, out TValue value) { CheckIfKeyNull(key); uint index; if (Key2IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey3Internal(TKey3 key, out TValue value) { CheckIfKeyNull(key); uint index; if (Key3IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey4Internal(TKey4 key, out TValue value) { CheckIfKeyNull(key); uint index; if (Key4IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey5Internal(TKey5 key, out TValue value) { CheckIfKeyNull(key); uint index; if (Key5IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey6Internal(TKey6 key, out TValue value) { CheckIfKeyNull(key); uint index; if (Key6IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey7Internal(TKey7 key, out TValue value) { CheckIfKeyNull(key); uint index; if (Key7IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey8Internal(TKey8 key, out TValue value) { CheckIfKeyNull(key); uint index; if (Key8IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }

	protected bool TryGetValueByKey1Internal(TKey1 key, out TValue value, out uint index) { CheckIfKeyNull(key); if (Key1IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey2Internal(TKey2 key, out TValue value, out uint index) { CheckIfKeyNull(key); if (Key2IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey3Internal(TKey3 key, out TValue value, out uint index) { CheckIfKeyNull(key); if (Key3IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey4Internal(TKey4 key, out TValue value, out uint index) { CheckIfKeyNull(key); if (Key4IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey5Internal(TKey5 key, out TValue value, out uint index) { CheckIfKeyNull(key); if (Key5IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey6Internal(TKey6 key, out TValue value, out uint index) { CheckIfKeyNull(key); if (Key6IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey7Internal(TKey7 key, out TValue value, out uint index) { CheckIfKeyNull(key); if (Key7IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }
	protected bool TryGetValueByKey8Internal(TKey8 key, out TValue value, out uint index) { CheckIfKeyNull(key); if (Key8IndexLookup.TryGetValue(key.GetHashCode(), out index)) { value = IndexedValues[index].Value; return true; } else { value = default(TValue); return false; } }



	protected bool SetValueByKey1Internal(TKey1 key, TValue value) { CheckIfKeyNull(key); uint index; if (Key1IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey2Internal(TKey2 key, TValue value) { CheckIfKeyNull(key); uint index; if (Key2IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey3Internal(TKey3 key, TValue value) { CheckIfKeyNull(key); uint index; if (Key3IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey4Internal(TKey4 key, TValue value) { CheckIfKeyNull(key); uint index; if (Key4IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey5Internal(TKey5 key, TValue value) { CheckIfKeyNull(key); uint index; if (Key5IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey6Internal(TKey6 key, TValue value) { CheckIfKeyNull(key); uint index; if (Key6IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey7Internal(TKey7 key, TValue value) { CheckIfKeyNull(key); uint index; if (Key7IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey8Internal(TKey8 key, TValue value) { CheckIfKeyNull(key); uint index; if (Key8IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }

	protected bool SetValueByKey1Internal(TKey1 key, TValue value, out uint index) { CheckIfKeyNull(key); if (Key1IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey2Internal(TKey2 key, TValue value, out uint index) { CheckIfKeyNull(key); if (Key2IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey3Internal(TKey3 key, TValue value, out uint index) { CheckIfKeyNull(key); if (Key3IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey4Internal(TKey4 key, TValue value, out uint index) { CheckIfKeyNull(key); if (Key4IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey5Internal(TKey5 key, TValue value, out uint index) { CheckIfKeyNull(key); if (Key5IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey6Internal(TKey6 key, TValue value, out uint index) { CheckIfKeyNull(key); if (Key6IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey7Internal(TKey7 key, TValue value, out uint index) { CheckIfKeyNull(key); if (Key7IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }
	protected bool SetValueByKey8Internal(TKey8 key, TValue value, out uint index) { CheckIfKeyNull(key); if (Key8IndexLookup.TryGetValue(key.GetHashCode(), out index)) { IndexedValues[index].Value = value; return true; } else { return false; } }



	protected TEntry GetEntryByKey1Internal(TKey1 key) { CheckIfKeyNull(key); return IndexedValues[Key1IndexLookup[key.GetHashCode()]]; }
	protected TEntry GetEntryByKey2Internal(TKey2 key) { CheckIfKeyNull(key); return IndexedValues[Key2IndexLookup[key.GetHashCode()]]; }
	protected TEntry GetEntryByKey3Internal(TKey3 key) { CheckIfKeyNull(key); return IndexedValues[Key3IndexLookup[key.GetHashCode()]]; }
	protected TEntry GetEntryByKey4Internal(TKey4 key) { CheckIfKeyNull(key); return IndexedValues[Key4IndexLookup[key.GetHashCode()]]; }
	protected TEntry GetEntryByKey5Internal(TKey5 key) { CheckIfKeyNull(key); return IndexedValues[Key5IndexLookup[key.GetHashCode()]]; }
	protected TEntry GetEntryByKey6Internal(TKey6 key) { CheckIfKeyNull(key); return IndexedValues[Key6IndexLookup[key.GetHashCode()]]; }
	protected TEntry GetEntryByKey7Internal(TKey7 key) { CheckIfKeyNull(key); return IndexedValues[Key7IndexLookup[key.GetHashCode()]]; }
	protected TEntry GetEntryByKey8Internal(TKey8 key) { CheckIfKeyNull(key); return IndexedValues[Key8IndexLookup[key.GetHashCode()]]; }

	protected TEntry GetEntryByKey1Internal(TKey1 key, out uint index) { CheckIfKeyNull(key); index = Key1IndexLookup[key.GetHashCode()]; return IndexedValues[index]; }
	protected TEntry GetEntryByKey2Internal(TKey2 key, out uint index) { CheckIfKeyNull(key); index = Key2IndexLookup[key.GetHashCode()]; return IndexedValues[index]; }
	protected TEntry GetEntryByKey3Internal(TKey3 key, out uint index) { CheckIfKeyNull(key); index = Key3IndexLookup[key.GetHashCode()]; return IndexedValues[index]; }
	protected TEntry GetEntryByKey4Internal(TKey4 key, out uint index) { CheckIfKeyNull(key); index = Key4IndexLookup[key.GetHashCode()]; return IndexedValues[index]; }
	protected TEntry GetEntryByKey5Internal(TKey5 key, out uint index) { CheckIfKeyNull(key); index = Key5IndexLookup[key.GetHashCode()]; return IndexedValues[index]; }
	protected TEntry GetEntryByKey6Internal(TKey6 key, out uint index) { CheckIfKeyNull(key); index = Key6IndexLookup[key.GetHashCode()]; return IndexedValues[index]; }
	protected TEntry GetEntryByKey7Internal(TKey7 key, out uint index) { CheckIfKeyNull(key); index = Key7IndexLookup[key.GetHashCode()]; return IndexedValues[index]; }
	protected TEntry GetEntryByKey8Internal(TKey8 key, out uint index) { CheckIfKeyNull(key); index = Key8IndexLookup[key.GetHashCode()]; return IndexedValues[index]; }
	
	protected bool TryGetEntryByKey1Internal(TKey1 key, out TEntry entry) { CheckIfKeyNull(key); uint index; if (Key1IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey2Internal(TKey2 key, out TEntry entry) { CheckIfKeyNull(key); uint index; if (Key2IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey3Internal(TKey3 key, out TEntry entry) { CheckIfKeyNull(key); uint index; if (Key3IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey4Internal(TKey4 key, out TEntry entry) { CheckIfKeyNull(key); uint index; if (Key4IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey5Internal(TKey5 key, out TEntry entry) { CheckIfKeyNull(key); uint index; if (Key5IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey6Internal(TKey6 key, out TEntry entry) { CheckIfKeyNull(key); uint index; if (Key6IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey7Internal(TKey7 key, out TEntry entry) { CheckIfKeyNull(key); uint index; if (Key7IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey8Internal(TKey8 key, out TEntry entry) { CheckIfKeyNull(key); uint index; if (Key8IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }

	protected bool TryGetEntryByKey1Internal(TKey1 key, out TEntry entry, out uint index) { CheckIfKeyNull(key); if (Key1IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey2Internal(TKey2 key, out TEntry entry, out uint index) { CheckIfKeyNull(key); if (Key2IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey3Internal(TKey3 key, out TEntry entry, out uint index) { CheckIfKeyNull(key); if (Key3IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey4Internal(TKey4 key, out TEntry entry, out uint index) { CheckIfKeyNull(key); if (Key4IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey5Internal(TKey5 key, out TEntry entry, out uint index) { CheckIfKeyNull(key); if (Key5IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey6Internal(TKey6 key, out TEntry entry, out uint index) { CheckIfKeyNull(key); if (Key6IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey7Internal(TKey7 key, out TEntry entry, out uint index) { CheckIfKeyNull(key); if (Key7IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }
	protected bool TryGetEntryByKey8Internal(TKey8 key, out TEntry entry, out uint index) { CheckIfKeyNull(key); if (Key8IndexLookup.TryGetValue(key.GetHashCode(), out index)) { entry = IndexedValues[index]; return true; } else { entry = default(TEntry); return false; } }






	protected void RemoveByKey1Internal(TKey1 startingKey) { uint index; TEntry entry = GetEntryByKey1Internal(startingKey, out index); RemoveInternal(entry, index); }
	protected void RemoveByKey2Internal(TKey2 startingKey) { uint index; TEntry entry = GetEntryByKey2Internal(startingKey, out index); RemoveInternal(entry, index); }
	protected void RemoveByKey3Internal(TKey3 startingKey) { uint index; TEntry entry = GetEntryByKey3Internal(startingKey, out index); RemoveInternal(entry, index); }
	protected void RemoveByKey4Internal(TKey4 startingKey) { uint index; TEntry entry = GetEntryByKey4Internal(startingKey, out index); RemoveInternal(entry, index); }
	protected void RemoveByKey5Internal(TKey5 startingKey) { uint index; TEntry entry = GetEntryByKey5Internal(startingKey, out index); RemoveInternal(entry, index); }
	protected void RemoveByKey6Internal(TKey6 startingKey) { uint index; TEntry entry = GetEntryByKey6Internal(startingKey, out index); RemoveInternal(entry, index); }
	protected void RemoveByKey7Internal(TKey7 startingKey) { uint index; TEntry entry = GetEntryByKey7Internal(startingKey, out index); RemoveInternal(entry, index); }
	protected void RemoveByKey8Internal(TKey8 startingKey) { uint index; TEntry entry = GetEntryByKey8Internal(startingKey, out index); RemoveInternal(entry, index); }

	protected bool TryRemoveByKey1Internal(TKey1 startingKey) { uint index; TEntry entry; if (TryGetEntryByKey1Internal(startingKey, out entry, out index)) { RemoveInternal(entry, index); return true; } else { return false; } }
	protected bool TryRemoveByKey2Internal(TKey2 startingKey) { uint index; TEntry entry; if (TryGetEntryByKey2Internal(startingKey, out entry, out index)) { RemoveInternal(entry, index); return true; } else { return false; } }
	protected bool TryRemoveByKey3Internal(TKey3 startingKey) { uint index; TEntry entry; if (TryGetEntryByKey3Internal(startingKey, out entry, out index)) { RemoveInternal(entry, index); return true; } else { return false; } }
	protected bool TryRemoveByKey4Internal(TKey4 startingKey) { uint index; TEntry entry; if (TryGetEntryByKey4Internal(startingKey, out entry, out index)) { RemoveInternal(entry, index); return true; } else { return false; } }
	protected bool TryRemoveByKey5Internal(TKey5 startingKey) { uint index; TEntry entry; if (TryGetEntryByKey5Internal(startingKey, out entry, out index)) { RemoveInternal(entry, index); return true; } else { return false; } }
	protected bool TryRemoveByKey6Internal(TKey6 startingKey) { uint index; TEntry entry; if (TryGetEntryByKey6Internal(startingKey, out entry, out index)) { RemoveInternal(entry, index); return true; } else { return false; } }
	protected bool TryRemoveByKey7Internal(TKey7 startingKey) { uint index; TEntry entry; if (TryGetEntryByKey7Internal(startingKey, out entry, out index)) { RemoveInternal(entry, index); return true; } else { return false; } }
	protected bool TryRemoveByKey8Internal(TKey8 startingKey) { uint index; TEntry entry; if (TryGetEntryByKey8Internal(startingKey, out entry, out index)) { RemoveInternal(entry, index); return true; } else { return false; } }
	//Note: [Try]GetEntryByKeyXInternal() does a null check on key

	private void RemoveInternal(TEntry entry, uint index)
	{
		//Remove each key from it's containing key-to-index lookup dictionary
		if (KeyCount >= 1) { Key1IndexLookup.Remove(entry.GetKey1().GetHashCode()); }
		if (KeyCount >= 2) { Key2IndexLookup.Remove(entry.GetKey2().GetHashCode()); }
		if (KeyCount >= 3) { Key3IndexLookup.Remove(entry.GetKey3().GetHashCode()); }
		if (KeyCount >= 4) { Key4IndexLookup.Remove(entry.GetKey4().GetHashCode()); }
		if (KeyCount >= 5) { Key5IndexLookup.Remove(entry.GetKey5().GetHashCode()); }
		if (KeyCount >= 6) { Key6IndexLookup.Remove(entry.GetKey6().GetHashCode()); }
		if (KeyCount >= 7) { Key7IndexLookup.Remove(entry.GetKey7().GetHashCode()); }
		if (KeyCount >= 8) { Key8IndexLookup.Remove(entry.GetKey8().GetHashCode()); }

		//'Remove' from indexed values
		IndexedValues[index] = default(TEntry);

		//Push freed index
		NextFreeIndices.Push(index);
	}





	protected bool ContainsKey1Internal(TKey1 key) { CheckIfKeyNull(key); return Key1IndexLookup.ContainsKey(key.GetHashCode()); }
	protected bool ContainsKey2Internal(TKey2 key) { CheckIfKeyNull(key); return Key2IndexLookup.ContainsKey(key.GetHashCode()); }
	protected bool ContainsKey3Internal(TKey3 key) { CheckIfKeyNull(key); return Key3IndexLookup.ContainsKey(key.GetHashCode()); }
	protected bool ContainsKey4Internal(TKey4 key) { CheckIfKeyNull(key); return Key4IndexLookup.ContainsKey(key.GetHashCode()); }
	protected bool ContainsKey5Internal(TKey5 key) { CheckIfKeyNull(key); return Key5IndexLookup.ContainsKey(key.GetHashCode()); }
	protected bool ContainsKey6Internal(TKey6 key) { CheckIfKeyNull(key); return Key6IndexLookup.ContainsKey(key.GetHashCode()); }
	protected bool ContainsKey7Internal(TKey7 key) { CheckIfKeyNull(key); return Key7IndexLookup.ContainsKey(key.GetHashCode()); }
	protected bool ContainsKey8Internal(TKey8 key) { CheckIfKeyNull(key); return Key8IndexLookup.ContainsKey(key.GetHashCode()); }

	public bool ContainsEntry(TEntry entry)
	{
		if (ReferenceEquals(entry, null)) throw new ArgumentNullException("entry");
		TEntry foundEntry;
		if (TryGetEntryByKey1Internal(entry.GetKey1(), out foundEntry)) {
			if (foundEntry.Equals(entry)) return true;
		}
		return false;
	}
	public bool ContainsAnyKeysOfEntry(TEntry entry)
	{
		if (ReferenceEquals(entry, null)) throw new ArgumentNullException("entry");
		if (KeyCount >= 1) { if (ContainsKey1Internal(entry.GetKey1())) return true; }
		if (KeyCount >= 2) { if (ContainsKey2Internal(entry.GetKey2())) return true; }
		if (KeyCount >= 3) { if (ContainsKey3Internal(entry.GetKey3())) return true; }
		if (KeyCount >= 4) { if (ContainsKey4Internal(entry.GetKey4())) return true; }
		if (KeyCount >= 5) { if (ContainsKey5Internal(entry.GetKey5())) return true; }
		if (KeyCount >= 6) { if (ContainsKey6Internal(entry.GetKey6())) return true; }
		if (KeyCount >= 7) { if (ContainsKey7Internal(entry.GetKey7())) return true; }
		if (KeyCount >= 8) { if (ContainsKey8Internal(entry.GetKey8())) return true; }
		return false;
	}

	public bool ContainsValue(TValue value) {
		return ContainsValue(value, EqualityComparer<TValue>.Default);
	}
	public bool ContainsValue(TValue value, IEqualityComparer<TValue> comparer)
	{
		if (ReferenceEquals(comparer, null)) throw new ArgumentNullException("comparer");
		foreach (TValue item in this.EnumerateValues()) {
			if (comparer.Equals(value, item)) return true;
		}
		return false;
	}



	public void Clear()
	{
		if (KeyCount >= 1) Key1IndexLookup.Clear();
		if (KeyCount >= 2) Key2IndexLookup.Clear();
		if (KeyCount >= 3) Key3IndexLookup.Clear();
		if (KeyCount >= 4) Key4IndexLookup.Clear();
		if (KeyCount >= 5) Key5IndexLookup.Clear();
		if (KeyCount >= 6) Key6IndexLookup.Clear();
		if (KeyCount >= 7) Key7IndexLookup.Clear();
		if (KeyCount >= 8) Key8IndexLookup.Clear();
		for (int i = 0; i < NextNewIndex; i++) {
			if (IndexedValues[i].HasValue) IndexedValues[i] = default(TEntry);
		}
		NextNewIndex = 0;
		NextFreeIndices.Clear();
	}

	/// <summary>
	/// Copies the items in the dictionary to an array, with a specified offset (see arrayIndex).
	/// </summary>
	/// <param name="array"></param>
	/// <param name="arrayIndex">0 copies the first entry to the start of the array, positive values copy the first entry to an offset item in the array, and negative values skip some dictionary entries before copying to the start of the array.</param>
	public void CopyTo(TEntry[] array, int arrayIndex)
	{
		if (array == null) return;

		int i = arrayIndex;
		foreach (TEntry entry in this) {
			if (i >= array.Length) break;
			if (i < 0) continue;
			array[i] = entry;
			i++;
		}
	}

	public TEntry[] ToArray()
	{
		TEntry[] array = new TEntry[Count];
		CopyTo(array, 0);
		return array;
	}




	public abstract class EnumeratorBase
	{
		private readonly MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TEntry, TValue> _dict;
		private int _curIndex = -1;
		private TEntry curItem;

		protected MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TEntry, TValue> Dict { get { return _dict; } }
		protected int CurrentIndex { get { return _curIndex; } }
		protected TEntry CurrentItem {
			get {
				if (_curIndex == -1) throw new InvalidOperationException("You must call MoveNext() before you can get the current enumerated value");
				else return curItem;
			}
		}


		public EnumeratorBase(MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TEntry, TValue> dict) {
			this._dict = dict;
		}

		public bool MoveNext()
		{
			//Get count of in-range items (freed/used items, but not never-used ones)
			uint count = _dict.NextNewIndex;

			//Get next index that is used
			do {
				_curIndex++;
			}
			while (!_dict.IndexedValues[_curIndex].HasValue && _curIndex < count);

			//Stop enumerating if curIndex is out-of-range
			if (_curIndex >= count) {
				return false;
			} else {
				curItem = _dict.IndexedValues[_curIndex];
				return true;
			}
		}

		public void Reset() {
			_curIndex = -1;
		}

		public void Dispose() { }
	}


	public EntryEnumerator EnumerateEntries() { //Entries can be enumerated explicitly
		return new EntryEnumerator(this);
	}
	IEnumerator<TEntry> IEnumerable<TEntry>.GetEnumerator() { //Or implicitly
		return this.EnumerateEntries();
	}
	IEnumerator IEnumerable.GetEnumerator() { //(Or also using this, but this shouldn't be used)
		return this.EnumerateEntries();
	}
	IEnumerable<TEntry> IMultiKeyDictBaseTEntry<TEntry>.EnumerateEntries() { return this.EnumerateEntries(); } //(Or through the base interface)
	IEnumerable<TEntry> IMultiKeyDictBaseTEntry<TEntry>.EnumerateEntries_Enumerable() { return this.EnumerateEntries(); } //(Or through the base interface)
	IEnumerator<TEntry> IMultiKeyDictBaseTEntry<TEntry>.EnumerateEntries_Enumerator() { return this.EnumerateEntries(); } //''
	public class EntryEnumerator : EnumeratorBase, IEnumerator<TEntry>, IEnumerable<TEntry> {

		public EntryEnumerator(MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TEntry, TValue> dict) : base(dict) { }

		public EntryEnumerator GetEnumerator() {
			return new EntryEnumerator(Dict);
		}
		IEnumerator<TEntry> IEnumerable<TEntry>.GetEnumerator() { return this.GetEnumerator(); }
		IEnumerator IEnumerable.GetEnumerator() { return this.GetEnumerator(); }

		public TEntry Current {
			get { return CurrentItem; }
		}
		object IEnumerator.Current { get { return this.Current; } }
	}


	public ValueEnumerator EnumerateValues() { //Values can be enumerated explicitly
		return new ValueEnumerator(this);
	}
	IEnumerable<TValue> IMultiKeyDictBaseTValue<TValue>.EnumerateValues() { return this.EnumerateValues(); } //(Or through the base interface)
	IEnumerable<TValue> IMultiKeyDictBaseTValue<TValue>.EnumerateValues_Enumerable() { return this.EnumerateValues(); } //(Or through the base interface)
	IEnumerator<TValue> IMultiKeyDictBaseTValue<TValue>.EnumerateValues_Enumerator() { return this.EnumerateValues(); } //''
	public class ValueEnumerator : EnumeratorBase, IEnumerator<TValue>, IEnumerable<TValue> {

		public ValueEnumerator(MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TEntry, TValue> dict) : base(dict) { }

		public ValueEnumerator GetEnumerator() {
			return new ValueEnumerator(Dict);
		}
		IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator() { return this.GetEnumerator(); }
		IEnumerator IEnumerable.GetEnumerator() { return this.GetEnumerator(); }

		public TValue Current {
			get { return CurrentItem.Value; }
		}
		object IEnumerator.Current { get { return this.Current; } }
	}


	/// <summary>
	/// Gives access to protected members, without needing to derive from MultiKeyDictionaryBase. Use with caution.
	/// <para/>
	/// Useful, for example, when creating a base class for a group of MultiKeyDictionary wrapper classes
	/// </summary>
	public ProtectedMemberAccessor GetAccessor() {
		return new ProtectedMemberAccessor(this);
	}

	/// <summary>
	/// Gives access to protected members, without needing to derive from MultiKeyDictionaryBase. Use with caution.
	/// <para/>
	/// Useful, for example, when creating a base class for a group of MultiKeyDictionary wrapper classes
	/// </summary>
	public class ProtectedMemberAccessor
	{
		public readonly MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TEntry, TValue> Dict; 

		public ProtectedMemberAccessor(MultiKeyDictBase<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TEntry, TValue> dict)
		{
			if (dict == null) throw new ArgumentNullException("dict");
			this.Dict = dict;
		}

		public void Add(TValue value, TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TKey8 key8) {
			Dict.AddInternal(value, key1, key2, key3, key4, key5, key6, key7, key8);
		}

		protected void CheckIfKeyNull(object key) { Dict.CheckIfKeyNull(key); }
		protected void CheckIfEntryHasValue(TEntry entry) { Dict.CheckIfEntryHasValue(entry); }

		public ReadOnlyCollection<TKey1> GetKey1s() { return Dict.GetKey1s(); }
		public ReadOnlyCollection<TKey2> GetKey2s() { return Dict.GetKey2s(); }
		public ReadOnlyCollection<TKey3> GetKey3s() { return Dict.GetKey3s(); }
		public ReadOnlyCollection<TKey4> GetKey4s() { return Dict.GetKey4s(); }
		public ReadOnlyCollection<TKey5> GetKey5s() { return Dict.GetKey5s(); }
		public ReadOnlyCollection<TKey6> GetKey6s() { return Dict.GetKey6s(); }
		public ReadOnlyCollection<TKey7> GetKey7s() { return Dict.GetKey7s(); }
		public ReadOnlyCollection<TKey8> GetKey8s() { return Dict.GetKey8s(); }

		public TValue GetValueByKey1(TKey1 key) { return Dict.GetValueByKey1Internal(key); }
		public TValue GetValueByKey2(TKey2 key) { return Dict.GetValueByKey2Internal(key); }
		public TValue GetValueByKey3(TKey3 key) { return Dict.GetValueByKey3Internal(key); }
		public TValue GetValueByKey4(TKey4 key) { return Dict.GetValueByKey4Internal(key); }
		public TValue GetValueByKey5(TKey5 key) { return Dict.GetValueByKey5Internal(key); }
		public TValue GetValueByKey6(TKey6 key) { return Dict.GetValueByKey6Internal(key); }
		public TValue GetValueByKey7(TKey7 key) { return Dict.GetValueByKey7Internal(key); }
		public TValue GetValueByKey8(TKey8 key) { return Dict.GetValueByKey8Internal(key); }
		
		public bool TryGetValueByKey1(TKey1 key, out TValue value) { return Dict.TryGetValueByKey1Internal(key, out value); }
		public bool TryGetValueByKey2(TKey2 key, out TValue value) { return Dict.TryGetValueByKey2Internal(key, out value); }
		public bool TryGetValueByKey3(TKey3 key, out TValue value) { return Dict.TryGetValueByKey3Internal(key, out value); }
		public bool TryGetValueByKey4(TKey4 key, out TValue value) { return Dict.TryGetValueByKey4Internal(key, out value); }
		public bool TryGetValueByKey5(TKey5 key, out TValue value) { return Dict.TryGetValueByKey5Internal(key, out value); }
		public bool TryGetValueByKey6(TKey6 key, out TValue value) { return Dict.TryGetValueByKey6Internal(key, out value); }
		public bool TryGetValueByKey7(TKey7 key, out TValue value) { return Dict.TryGetValueByKey7Internal(key, out value); }
		public bool TryGetValueByKey8(TKey8 key, out TValue value) { return Dict.TryGetValueByKey8Internal(key, out value); }
		
		public bool TryGetValueByKey1(TKey1 key, out TValue value, out uint index) { return Dict.TryGetValueByKey1Internal(key, out value, out index); }
		public bool TryGetValueByKey2(TKey2 key, out TValue value, out uint index) { return Dict.TryGetValueByKey2Internal(key, out value, out index); }
		public bool TryGetValueByKey3(TKey3 key, out TValue value, out uint index) { return Dict.TryGetValueByKey3Internal(key, out value, out index); }
		public bool TryGetValueByKey4(TKey4 key, out TValue value, out uint index) { return Dict.TryGetValueByKey4Internal(key, out value, out index); }
		public bool TryGetValueByKey5(TKey5 key, out TValue value, out uint index) { return Dict.TryGetValueByKey5Internal(key, out value, out index); }
		public bool TryGetValueByKey6(TKey6 key, out TValue value, out uint index) { return Dict.TryGetValueByKey6Internal(key, out value, out index); }
		public bool TryGetValueByKey7(TKey7 key, out TValue value, out uint index) { return Dict.TryGetValueByKey7Internal(key, out value, out index); }
		public bool TryGetValueByKey8(TKey8 key, out TValue value, out uint index) { return Dict.TryGetValueByKey8Internal(key, out value, out index); }
		
		public bool SetValueByKey1(TKey1 key, TValue value) { return Dict.SetValueByKey1Internal(key, value); }
		public bool SetValueByKey2(TKey2 key, TValue value) { return Dict.SetValueByKey2Internal(key, value); }
		public bool SetValueByKey3(TKey3 key, TValue value) { return Dict.SetValueByKey3Internal(key, value); }
		public bool SetValueByKey4(TKey4 key, TValue value) { return Dict.SetValueByKey4Internal(key, value); }
		public bool SetValueByKey5(TKey5 key, TValue value) { return Dict.SetValueByKey5Internal(key, value); }
		public bool SetValueByKey6(TKey6 key, TValue value) { return Dict.SetValueByKey6Internal(key, value); }
		public bool SetValueByKey7(TKey7 key, TValue value) { return Dict.SetValueByKey7Internal(key, value); }
		public bool SetValueByKey8(TKey8 key, TValue value) { return Dict.SetValueByKey8Internal(key, value); }
		
		public bool SetValueByKey1(TKey1 key, TValue value, out uint index) { return Dict.SetValueByKey1Internal(key, value, out index); }
		public bool SetValueByKey2(TKey2 key, TValue value, out uint index) { return Dict.SetValueByKey2Internal(key, value, out index); }
		public bool SetValueByKey3(TKey3 key, TValue value, out uint index) { return Dict.SetValueByKey3Internal(key, value, out index); }
		public bool SetValueByKey4(TKey4 key, TValue value, out uint index) { return Dict.SetValueByKey4Internal(key, value, out index); }
		public bool SetValueByKey5(TKey5 key, TValue value, out uint index) { return Dict.SetValueByKey5Internal(key, value, out index); }
		public bool SetValueByKey6(TKey6 key, TValue value, out uint index) { return Dict.SetValueByKey6Internal(key, value, out index); }
		public bool SetValueByKey7(TKey7 key, TValue value, out uint index) { return Dict.SetValueByKey7Internal(key, value, out index); }
		public bool SetValueByKey8(TKey8 key, TValue value, out uint index) { return Dict.SetValueByKey8Internal(key, value, out index); }
		
		public TEntry GetEntryByKey1(TKey1 key) { return Dict.GetEntryByKey1Internal(key); }
		public TEntry GetEntryByKey2(TKey2 key) { return Dict.GetEntryByKey2Internal(key); }
		public TEntry GetEntryByKey3(TKey3 key) { return Dict.GetEntryByKey3Internal(key); }
		public TEntry GetEntryByKey4(TKey4 key) { return Dict.GetEntryByKey4Internal(key); }
		public TEntry GetEntryByKey5(TKey5 key) { return Dict.GetEntryByKey5Internal(key); }
		public TEntry GetEntryByKey6(TKey6 key) { return Dict.GetEntryByKey6Internal(key); }
		public TEntry GetEntryByKey7(TKey7 key) { return Dict.GetEntryByKey7Internal(key); }
		public TEntry GetEntryByKey8(TKey8 key) { return Dict.GetEntryByKey8Internal(key); }
		
		public TEntry GetEntryByKey1(TKey1 key, out uint index) { return Dict.GetEntryByKey1Internal(key, out index); }
		public TEntry GetEntryByKey2(TKey2 key, out uint index) { return Dict.GetEntryByKey2Internal(key, out index); }
		public TEntry GetEntryByKey3(TKey3 key, out uint index) { return Dict.GetEntryByKey3Internal(key, out index); }
		public TEntry GetEntryByKey4(TKey4 key, out uint index) { return Dict.GetEntryByKey4Internal(key, out index); }
		public TEntry GetEntryByKey5(TKey5 key, out uint index) { return Dict.GetEntryByKey5Internal(key, out index); }
		public TEntry GetEntryByKey6(TKey6 key, out uint index) { return Dict.GetEntryByKey6Internal(key, out index); }
		public TEntry GetEntryByKey7(TKey7 key, out uint index) { return Dict.GetEntryByKey7Internal(key, out index); }
		public TEntry GetEntryByKey8(TKey8 key, out uint index) { return Dict.GetEntryByKey8Internal(key, out index); }
		
		public bool TryGetEntryByKey1(TKey1 key, out TEntry entry) { return Dict.TryGetEntryByKey1Internal(key, out entry); }
		public bool TryGetEntryByKey2(TKey2 key, out TEntry entry) { return Dict.TryGetEntryByKey2Internal(key, out entry); }
		public bool TryGetEntryByKey3(TKey3 key, out TEntry entry) { return Dict.TryGetEntryByKey3Internal(key, out entry); }
		public bool TryGetEntryByKey4(TKey4 key, out TEntry entry) { return Dict.TryGetEntryByKey4Internal(key, out entry); }
		public bool TryGetEntryByKey5(TKey5 key, out TEntry entry) { return Dict.TryGetEntryByKey5Internal(key, out entry); }
		public bool TryGetEntryByKey6(TKey6 key, out TEntry entry) { return Dict.TryGetEntryByKey6Internal(key, out entry); }
		public bool TryGetEntryByKey7(TKey7 key, out TEntry entry) { return Dict.TryGetEntryByKey7Internal(key, out entry); }
		public bool TryGetEntryByKey8(TKey8 key, out TEntry entry) { return Dict.TryGetEntryByKey8Internal(key, out entry); }
		
		public bool TryGetEntryByKey1(TKey1 key, out TEntry entry, out uint index) { return Dict.TryGetEntryByKey1Internal(key, out entry, out index); }
		public bool TryGetEntryByKey2(TKey2 key, out TEntry entry, out uint index) { return Dict.TryGetEntryByKey2Internal(key, out entry, out index); }
		public bool TryGetEntryByKey3(TKey3 key, out TEntry entry, out uint index) { return Dict.TryGetEntryByKey3Internal(key, out entry, out index); }
		public bool TryGetEntryByKey4(TKey4 key, out TEntry entry, out uint index) { return Dict.TryGetEntryByKey4Internal(key, out entry, out index); }
		public bool TryGetEntryByKey5(TKey5 key, out TEntry entry, out uint index) { return Dict.TryGetEntryByKey5Internal(key, out entry, out index); }
		public bool TryGetEntryByKey6(TKey6 key, out TEntry entry, out uint index) { return Dict.TryGetEntryByKey6Internal(key, out entry, out index); }
		public bool TryGetEntryByKey7(TKey7 key, out TEntry entry, out uint index) { return Dict.TryGetEntryByKey7Internal(key, out entry, out index); }
		public bool TryGetEntryByKey8(TKey8 key, out TEntry entry, out uint index) { return Dict.TryGetEntryByKey8Internal(key, out entry, out index); }
		
		public void RemoveByKey1(TKey1 startingKey) { Dict.RemoveByKey1Internal(startingKey); }
		public void RemoveByKey2(TKey2 startingKey) { Dict.RemoveByKey2Internal(startingKey); }
		public void RemoveByKey3(TKey3 startingKey) { Dict.RemoveByKey3Internal(startingKey); }
		public void RemoveByKey4(TKey4 startingKey) { Dict.RemoveByKey4Internal(startingKey); }
		public void RemoveByKey5(TKey5 startingKey) { Dict.RemoveByKey5Internal(startingKey); }
		public void RemoveByKey6(TKey6 startingKey) { Dict.RemoveByKey6Internal(startingKey); }
		public void RemoveByKey7(TKey7 startingKey) { Dict.RemoveByKey7Internal(startingKey); }
		public void RemoveByKey8(TKey8 startingKey) { Dict.RemoveByKey8Internal(startingKey); }
		
		public bool TryRemoveByKey1(TKey1 startingKey) { return Dict.TryRemoveByKey1Internal(startingKey); }
		public bool TryRemoveByKey2(TKey2 startingKey) { return Dict.TryRemoveByKey2Internal(startingKey); }
		public bool TryRemoveByKey3(TKey3 startingKey) { return Dict.TryRemoveByKey3Internal(startingKey); }
		public bool TryRemoveByKey4(TKey4 startingKey) { return Dict.TryRemoveByKey4Internal(startingKey); }
		public bool TryRemoveByKey5(TKey5 startingKey) { return Dict.TryRemoveByKey5Internal(startingKey); }
		public bool TryRemoveByKey6(TKey6 startingKey) { return Dict.TryRemoveByKey6Internal(startingKey); }
		public bool TryRemoveByKey7(TKey7 startingKey) { return Dict.TryRemoveByKey7Internal(startingKey); }
		public bool TryRemoveByKey8(TKey8 startingKey) { return Dict.TryRemoveByKey8Internal(startingKey); }

		public bool ContainsKey1(TKey1 key) { return Dict.ContainsKey1Internal(key); }
		public bool ContainsKey2(TKey2 key) { return Dict.ContainsKey2Internal(key); }
		public bool ContainsKey3(TKey3 key) { return Dict.ContainsKey3Internal(key); }
		public bool ContainsKey4(TKey4 key) { return Dict.ContainsKey4Internal(key); }
		public bool ContainsKey5(TKey5 key) { return Dict.ContainsKey5Internal(key); }
		public bool ContainsKey6(TKey6 key) { return Dict.ContainsKey6Internal(key); }
		public bool ContainsKey7(TKey7 key) { return Dict.ContainsKey7Internal(key); }
		public bool ContainsKey8(TKey8 key) { return Dict.ContainsKey8Internal(key); }
	}

}







//*/