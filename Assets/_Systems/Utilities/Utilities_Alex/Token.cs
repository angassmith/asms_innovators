﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

/// <summary>
/// The private object that is used to access methods of another class
/// <para/>
/// Example use (in the accessing class):
/// private static readonly <see cref="TokenKey{TAccessed}"/> _tAcessedToken = new <see cref="TokenKey{TAccessed}"/>();
/// </summary>
public sealed class TokenKey<TAccessed>
{
	private object _actualToken = null;
	/// <summary>ActualToken defaults to null, and is non-null if and only if Set() has been called with all arguments non-null</summary>
	public object ActualToken {
		get { return _actualToken; }
	}

	/// <summary>True if ActualToken is non-null, otherwise, false</summary>
	public bool IsSet {
		get { return _actualToken != null; }
	}

	public TokenKey() { }

	
	/// <summary>
	/// Sets ActualToken to newToken.ActualToken, but only if caller and newToken are non-null, and IsSet is false
	/// </summary>
	/// <exception cref="ArgumentNullException">caller or newToken is null</exception>
	/// <exception cref="InvalidOperationException">Set has already been successfully called</exception>
	public void Set(TAccessed caller, TokenLock<TAccessed> newToken)
	{
		if (caller == null) throw new ArgumentNullException("caller");
		if (newToken == null) throw new ArgumentNullException("newToken");
		if (IsSet) throw new InvalidOperationException("The token cannot be set multiple times");
		_actualToken = newToken.ActualToken;
	}
}

/// <summary>
/// The publicly (or internally) visible object that is used by the accessed class in order to set the private token.
/// <para/>
/// Example use (in the accessing class):
/// internal static readonly <see cref="TokenKeySetter{TAccessed}"/> TAccessedTokenSetter
///     = new <see cref="TokenKeySetter{TAccessed}"/>(_tAccessedToken);
/// </summary>
/// <typeparam name="TAccessed"></typeparam>
public sealed class TokenKeySetter<TAccessed>
{
	private TokenKey<TAccessed> _set;

	public TokenKeySetter(TokenKey<TAccessed> set)
	{
		if (set == null) throw new ArgumentNullException("set");
		this._set = set;
	}

	/// <summary>
	/// Calls the Set method of the <see cref="TokenKey{TAccessed}"/> provided to the constructor
	/// </summary>
	/// <exception cref="ArgumentNullException">caller or newToken is null</exception>
	/// <exception cref="InvalidOperationException">Set has already been successfully called</exception>
	public void Set(TAccessed caller, TokenLock<TAccessed> newToken)
	{
		_set.Set(caller, newToken);
	}
}

/// <summary>
/// The private object that is used to set and validate the token used by another class to access this class.
/// <para/>
/// Example use (in TAccessed):
/// private static readonly <see cref="TokenLock{TAccessed}"/> Token
///     = new <see cref="TokenLock{TAccessed}"/>(new TAccessed(), &lt;accessing-class&gt;.TAccessedTokenSetter);
/// </summary>
/// <typeparam name="TAccessed"></typeparam>
public sealed class TokenLock<TAccessed>
{
	private readonly object _actualToken = new object();
	public object ActualToken { get { return _actualToken; } }

	public TokenLock(TAccessed caller, params TokenKeySetter<TAccessed>[] setters)
	{
		foreach (TokenKeySetter<TAccessed> setter in setters) {
			setter.Set(caller, this);
		}
	}

	public void Check(TokenKey<TAccessed> mustEqual) {
		if (mustEqual == null) throw new TokenMismatchException(typeof(TAccessed));
		if (!ReferenceEquals(this.ActualToken, mustEqual.ActualToken)) throw new TokenMismatchException(typeof(TAccessed));
	}
	public void Check(TokenKey<TAccessed> mustEqual, string methodName) {
		if (mustEqual == null) throw new TokenMismatchException(typeof(TAccessed), methodName);
		if (!ReferenceEquals(this.ActualToken, mustEqual.ActualToken)) throw new TokenMismatchException(typeof(TAccessed), methodName);
	}
}

[Serializable]
public class TokenMismatchException : ArgumentException
{
	public TokenMismatchException() { }
	public TokenMismatchException(string message) : base(message) { }
	public TokenMismatchException(string message, Exception inner) : base(message, inner) { }
	public TokenMismatchException(Type accessedType) : base("A token-requiring method in type '" + (accessedType == null ? "" : accessedType.FullName) + "' was passed an invalid or null token.") { }
	public TokenMismatchException(Type accessedType, string methodName) : base("The token-requiring method '" + methodName + "' in type '" + (accessedType == null ? "" : accessedType.FullName) + "' was passed an invalid or null token.") { }
	protected TokenMismatchException(
	  System.Runtime.Serialization.SerializationInfo info,
	  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
}

//Token Example:
//	class A
//	{
//		private static readonly TokenKey<B> _bToken = new TokenKey<B>();
//	
//		internal static readonly TokenKeySetter<T> BTokenSetter = new TokenKeySetter(_bToken);
//	
//		public static void Blah()
//		{
//			B.Foo(_bToken);
//		}
//	}
//	
//	class B
//	{
//		private static readonly TokenLock<B> Token = new TokenLock<B>(new B(), A.BTokenSetter);
//	
//		public B() { }
//	
//		internal static void Foo(TokenKey<B> token)
//		{
//			Token.Check(token);
//			Debug.Log("Foo() ran successfully");
//		}
//	}

#region old
//	public class Token2<TAccessed>
//	{
//		private object _actualToken = null;
//		/// <summary>ActualToken defaults to null, and is non-null if and only if Set() has been called with all arguments non-null</summary>
//		public object ActualToken {
//			get { return _actualToken; }
//		}
//	
//		/// <summary>True if ActualToken is null, otherwise, false</summary>
//		public bool IsUnset {
//			get { return _actualToken == null; }
//		}
//	
//		private Token2(object actualToken) {
//			this._actualToken = actualToken;
//		}
//	
//		public void Check(Token2<TAccessed> mustEqual) {
//			if (mustEqual == null) throw new TokenMismatchException(typeof(TAccessed));
//			if (!ReferenceEquals(this.ActualToken, mustEqual.ActualToken)) throw new TokenMismatchException(typeof(TAccessed));
//		}
//		public void Check(Token2<TAccessed> mustEqual, string methodName) {
//			if (mustEqual == null) throw new TokenMismatchException(typeof(TAccessed), methodName);
//			if (!ReferenceEquals(this.ActualToken, mustEqual.ActualToken)) throw new TokenMismatchException(typeof(TAccessed), methodName);
//		}
//	
//		public void Set(TAccessed caller, Token2<TAccessed> newToken)
//		{
//			if (caller == null) throw new ArgumentNullException("caller");
//			if (newToken == null) throw new ArgumentNullException("newToken");
//			if (newToken.ActualToken == null) throw new ArgumentNullException("newToken.ActualToken");
//			if (ActualToken != null) throw new InvalidOperationException("The token cannot be set multiple times");
//			_actualToken = newToken.ActualToken;
//		}
//	
//		public Token2<TAccessed> CreateKey() {
//			return new Token2<TAccessed>(null);
//		}
//	
//		public Token2<TAccessed> CreateLock() {
//			return new Token2<TAccessed>(new object());
//		}
//	}
//	
//	public class Token<TAccessed>
//	{
//		public void Check(Token<TAccessed> mustEqual) {
//			if (!ReferenceEquals(this, mustEqual)) throw new TokenMismatchException(typeof(TAccessed));
//		}
//		public void Check(Token<TAccessed> mustEqual, string methodName) {
//			if (!ReferenceEquals(this, mustEqual)) throw new TokenMismatchException(typeof(TAccessed), methodName);
//		}
//	
//		//	public static Token<TAccessed> Set(TAccessed caller, Token<TAccessed> newToken)
//		//	{
//		//		if (caller == null) throw new ArgumentNullException("caller");
//		//		if (Token != null) throw new InvalidOperationException("The token cannot be set multiple times");
//		//		Token = newToken;
//		//	}
//	}
//	
//	public class TokenHolder<TAccessed>
//	{
//		public Token<TAccessed> Token { get; private set; }
//	
//		public TokenHolder() { }
//	
//		public void Set(TAccessed caller, Token<TAccessed> newToken)
//		{
//			if (caller == null) throw new ArgumentNullException("caller");
//			if (Token != null) throw new InvalidOperationException("The token cannot be set multiple times");
//			Token = newToken;
//		}
//	}
//	
//	
//	class A2
//	{
//		private static readonly TokenHolder<B2> BTokenHolder = new TokenHolder<B2>();
//	
//		public static void SetBToken(B2 caller, Token<B2> newToken) {
//			BTokenHolder.Set(caller, newToken);
//		}
//	
//		public void Blah()
//		{
//			B2.Foo(BTokenHolder.Token);
//		}
//	}
//	
//	class B2
//	{
//		private static Token<B2> Token = new Token<B2>();
//	
//		static B2() {
//			A2.SetBToken(new B2(), Token);
//		}
//	
//		public B2() { }
//	
//		public static void Foo(Token<B2> token)
//		{
//			Token.Check(token, "Foo");
//		}
//	}
//	
//	class A1
//	{
//		private static object token;
//	
//		public static void SetToken(B1 caller, object newVal)
//		{
//			if (caller == null) throw new ArgumentNullException();
//			if (token != null) throw new InvalidOperationException();
//			else token = newVal;
//		}
//	}
//	
//	
//	class B1
//	{
//		private static object Token = new object();
//	
//		static B1()
//		{
//			A1.SetToken(new B1(), Token);
//		}
//	
//		private B1() { }
//		public B1(object token)
//		{
//			if (token != Token) throw new ArgumentException("Invalid token");
//	
//		}
//	}
#endregion

//*/