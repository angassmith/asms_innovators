﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

/// <summary>
/// Allows access to IEnumberable&lt;T&gt; methods for a Transform's children
/// </summary>
public class TransformEnumerator : IEnumerable<Transform>, IEnumerator<Transform>
{
	public readonly Transform Parent;
	public int CurrentIndex { get; private set; }

	public TransformEnumerator(Transform parent)
	{
		if (parent == null) throw new ArgumentNullException("parent");
		this.Parent = parent;

		CurrentIndex = -1;
	}

	public IEnumerator<Transform> GetEnumerator() {
		return new TransformEnumerator(this.Parent);
	}


	public Transform Current {
		get {
			return Parent.GetChild(CurrentIndex);
		}
	}

	public bool MoveNext()
	{
		int nextIndex = CurrentIndex + 1;
		if (nextIndex < Parent.childCount) {
			CurrentIndex = nextIndex;
			return true;
		} else {
			return false;
		}
	}

	public void Reset() {
		CurrentIndex = -1;
	}
	
	IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
	object IEnumerator.Current { get { return Current; } }
	public void Dispose() { }
}

//*/