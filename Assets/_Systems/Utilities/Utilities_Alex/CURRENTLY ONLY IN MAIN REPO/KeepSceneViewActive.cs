﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;

public class KeepSceneViewActive : MonoBehaviour
{

	public bool keepSceneViewActive = false;

	void Start()
	{
		if (this.keepSceneViewActive && Application.isEditor)
		{
			UnityEditor.SceneView.FocusWindowIfItsOpen(typeof(UnityEditor.SceneView));
		}
	}

	void Update()
	{
		if (this.keepSceneViewActive && Application.isEditor)
		{
			UnityEditor.SceneView.FocusWindowIfItsOpen(typeof(UnityEditor.SceneView));
		}
	}
}

#endif