﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Utilities.Alex
{
	public static class CoroutineUtils
	{
		/// <summary>
		/// Runs the specified method after the provided yield instruction has completed.
		/// The coroutine is run by <paramref name="coroutineHost"/> (so is dependent on whether it is enabled etc).
		/// </summary>
		public static void RunAfter(this MonoBehaviour coroutineHost, YieldInstruction yieldInstruction, Action action)
		{
			coroutineHost.StartCoroutine(WaitForIterator(yieldInstruction, action));
		}
		private static IEnumerator WaitForIterator(YieldInstruction yieldInstruction, Action callback)
		{
			yield return yieldInstruction;
			callback();
		}


		public static IEnumerator RunAfterDelay(float seconds, Action action)
		{
			Debug.Log("#434");
			if (action == null) throw new ArgumentNullException("action");
			seconds = seconds > 0 ? seconds : 0; //Floats are approximate so shouldn't throw exception

			return RunAfterDelayIterator(seconds, action);
		}
		private static IEnumerator RunAfterDelayIterator(float seconds, Action action)
		{
			Debug.Log("#435");
			yield return new WaitForSeconds(seconds);
			Debug.Log("#436");
			action();
		}

		//Add more similar methods
	}
}

//*/