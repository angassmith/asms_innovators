﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Utilities.Alex
{
	//TODO: ADD METHODS IN THIS CLASS TO MISCCOLLECTIONS PROJECT

	public static class EnumerableUtils
	{

		/// <summary>
		/// Selects a random element from source, by enumerating over the entire collection once.
		/// </summary>
		/// <remarks>
		/// Access to <paramref name="rng"/> is not synchronised across multiple threads (no lock is used).
		/// </remarks>
		/// <exception cref="ArgumentNullException"><paramref name="source"/> or <paramref name="rng"/> is null</exception>
		/// <exception cref="ArgumentException"><paramref name="source"/> is empty</exception>
		public static T RandomElement<T>(this IEnumerable<T> source, Random rng)
		{
			T result;
			if (TryGetRandomElement(source, rng, out result)) {
				return result;
			} else {
				throw new ArgumentException("source is empty.");
			}
		}

		//Adapted from http://stackoverflow.com/a/648240/4149474
		//See also https://en.wikipedia.org/wiki/Reservoir_sampling for proof of randomness
		//TODO: Try to implement something that can select multiple random elements (unique and non-unique versions),
		//in a single pass, without knowing the count in advance
		//May be of use: http://stackoverflow.com/a/35065765/4149474
		/// <summary>
		/// Selects a random element from source, by enumerating over the entire collection once.
		/// Returns true if there is at least one element in source, otherwise returns false.
		/// </summary>
		/// <remarks>
		/// Access to <paramref name="rng"/> is not synchronised across multiple threads (no lock is used).
		/// </remarks>
		/// <exception cref="ArgumentNullException"><paramref name="source"/> or <paramref name="rng"/> is null</exception>
		public static bool TryGetRandomElement<T>(this IEnumerable<T> source, Random rng, out T result)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (rng == null) throw new ArgumentNullException("rng");

			result = default(T);
			int count = 0;
			foreach (T element in source)
			{
				count++;

				if (rng.Next(maxValue: count) == 0)
				{
					result = element;
				}
			}

			if (count > 0) return true;
			else return false;
		}


		public static bool DictionariesEqual<TKey, TValue>(IDictionary<TKey, TValue> a, IDictionary<TKey, TValue> b)
		{
			if (ReferenceEquals(a, b)) return true;
			if ((a == null) || (b == null)) return false;
			if (a.Count != b.Count) return false;

			var valueComparer = EqualityComparer<TValue>.Default;

			foreach (var kvp in a)
			{
				TValue value2;
				if (!b.TryGetValue(kvp.Key, out value2)) return false;
				if (!valueComparer.Equals(kvp.Value, value2)) return false;
			}
			return true;
		}
	}
}

//*/