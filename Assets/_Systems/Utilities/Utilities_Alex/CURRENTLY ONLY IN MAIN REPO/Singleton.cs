﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UObject = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames
{


	//Adapted from:
	// - http://wiki.unity3d.com/index.php/Singleton
	// - http://gamedevrant.blogspot.com.au/2013/07/unity-and-singleton-design-pattern.html

	public abstract class SingletonBase : TrackedMono
	{
		protected static bool InPlayMode {
			get {
#if UNITY_EDITOR
				return EditorApplication.isPlaying;
#else
				return true;
#endif		
			}
		}


		/// <summary>You can call this if you want, but there should be no need. DO NOT call it from the constructor</summary>
		protected abstract void InitSingleton();

#if UNITY_EDITOR
		/// <summary>DO NOT call this outside editor mode</summary>
		protected abstract void EditorOnlyReInitSingleton();
#endif




		protected sealed override bool TrackInEditor { get { return true; } }




#if UNITY_EDITOR
		/// <summary>
		/// Derive from this class to create custom singleton editors
		/// </summary>
		[CustomEditor(typeof(SingletonBase), editorForChildClasses:true)]
		[InitializeOnLoad]
		public class SingletonEditor : Editor
		{
			protected SingletonBase targetSingleton;

			public sealed override void OnInspectorGUI()
			{
				if (targetSingleton == null) targetSingleton = (SingletonBase)target;

				targetSingleton.InitSingleton();

				if (GUILayout.Button("Manual Re-Initialize Singleton")) {
					targetSingleton.EditorOnlyReInitSingleton();
				}

				base.OnInspectorGUI();
			}
			public virtual void OnInspectorGUIExt() { }

			//	public sealed override bool RequiresConstantRepaint()
			//	{
			//		if (targetSingleton == null) targetSingleton = (SingletonBase)target;
			//	
			//		targetSingleton.InitSingleton();
			//	
			//		return true;
			//	}
		}
#endif
	}

	public abstract class Singleton<T> : SingletonBase where T : Singleton<T> {

		private static object syncRoot = new object();

		private static T _instance;

		/// <summary>The singleton-instance. Throws a SingletonException if the singleton is irrecoverably invalid. Use 'Go to definition' for details</summary>
		/// <exception cref="SingletonException">
		/// 1: SingletonTerminated is true or the internal instance has been destroyed.
		/// 2: Multiple instances are present in the scene
		/// </exception>
		public static T Instance {
			get {
				lock (syncRoot)
				{
					if (_singletonTerminated || IsDestroyed(_instance)) {
#if UNITY_EDITOR
						if (!EditorApplication.isPlaying)
						{
							_singletonTerminated = false;
							_instance = null;
							return Instance; //Recurse once
						}
#endif
						throw CreateSingletonTerminatedEx();
					}
					else if (ReferenceEquals(_instance, null))
					{
						T[] foundObjects = Resources.FindObjectsOfTypeAll<T>();
						if (foundObjects.Length == 1)
						{
							LogSingletonFoundInScene(foundObjects[0]);

							_instance = foundObjects[0];
							DontDestroyOnLoad_IfPlayMode(_instance);
							return _instance;
						}
						else if (foundObjects.Length == 0)
						{
							GameObject newSingletonGObj = new GameObject("[Auto-Singleton: " + typeof(T).Name + "]");
							T newInstance = newSingletonGObj.AddComponent<T>();

							DontDestroyOnLoad_IfPlayMode(newSingletonGObj);
							DontDestroyOnLoad_IfPlayMode(newInstance); //Just in case this is needed

							LogSingletonCreated(newInstance);

							_instance = newInstance;
							return _instance;
						}
						else
						{
							throw CreateMultipleSingletonsFoundEx();
						}
					}
					else
					{
						return _instance;
					}
				}
			}
		}

		private static bool _singletonTerminated = false;
		/// <summary>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed, 
		/// it will create a buggy ghost object that will stay on the Editor scene
		/// even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </summary>
		public static bool SingletonTerminated { get { return _singletonTerminated; } }

		protected Singleton() { }


		protected sealed override void UpdateTracking(TrackingEvent evt)
		{
			switch (evt)
			{
				case TrackingEvent.Awake:
				case TrackingEvent.Reset:
					InitSingleton();
					break;

				case TrackingEvent.Destroy:
				case TrackingEvent.ApplicationQuit:
					_singletonTerminated = true;
					break;
			}
		}

		/// <summary>You can call this if you want, but there should be no need. DO NOT call it from the constructor</summary>
		protected sealed override void InitSingleton()
		{
			if (_instance == null)
			{
				DontDestroyOnLoad_IfPlayMode(this);
				_instance = (T)this;

			}
			else
			{
				if (!ReferenceEquals(_instance, this))
				{
					//	string exceptionStr = (
					//		"[Singleton] Singleton '"
					//		+ typeof(T).FullName
					//		+ "' has already been initialized, but another instance was created (at path '"
					//		+ this.GetHierarchyPath()
					//		+ "'). DestroyImmediately() was called on this new instance."
					//	);
					//	try {
					//		DestroyImmediate(this);
					//	} catch (Exception e) {
					//		throw new SingletonException(exceptionStr, e);
					//	}
					//	throw new SingletonException(exceptionStr);
					this.enabled = false;
					Debug.LogError(
						"[Singleton] Singleton '"
						+ typeof(T).FullName
						+ "' has already been initialized, but another instance was created (at path '"
						+ this.GetHierarchyPath()
						+ "'). DestroyImmediately() will be called on the new instance immediately after this log message.",
						this
					);
					DestroyImmediate(this);
				}
			}
		}

#if UNITY_EDITOR
		/// <summary>DO NOT call this outside editor mode</summary>
		protected sealed override void EditorOnlyReInitSingleton()
		{
			_instance = null;
			_singletonTerminated = false;
			InitSingleton();
		}
#endif

	
		protected static SingletonException CreateSingletonTerminatedEx() {
			return new SingletonException(
				"[Singleton] The singleton '"
				+ typeof(T).FullName
				+ "' has been terminated (OnDestroy or OnApplicationQuit was called on the instance,"
				+ "or the instance was detected to be non-null but destroyed)."
				+ "It will not be recreated."
			);
		}

		protected static void LogSingletonFoundInScene(T foundInstance)
		{
			Debug.Log(
				"[Singleton] An instance of singleton '"
				+ typeof(T).FullName
				+ "' was needed, and exactly one was found in the scene (object '"
				+ foundInstance.GetDebugName()
				+ "'), so that one was used.",
				foundInstance
			);
		}

		protected static void LogSingletonCreated(T newInstance)
		{
			Debug.Log(
				"[Singleton] An instance of singleton '"
				+ typeof(T).FullName
				+ "' was needed but none existed in the game, so a new instance was created (at path '"
				+ newInstance.GetDebugName()
				+ "').",
				newInstance
			);
		}

		protected static SingletonException CreateMultipleSingletonsFoundEx()
		{
			return new SingletonException(
				"[Singleton] Singleton '"
				+ typeof(T).FullName
				+ "' has not been initialized, and multiple instances are present in the scene."
			);
		}

		protected static TArg ThrowIfArgNullOrDestroyed<TArg>(TArg arg, string argName)
			where TArg : UObject
		{
			if (ReferenceEquals(arg, null)) throw new ArgumentNullException(argName);
			if (IsDestroyed(arg)) throw new ArgumentException("UnityEngine.Object is destroyed", argName);
			return arg;
		}


		protected static bool IsDestroyed(UObject obj) {
			return !ReferenceEquals(obj, null) && obj.Equals(null);
		}

		protected static void DontDestroyOnLoad_IfPlayMode(UObject obj)
		{
			ThrowIfArgNullOrDestroyed(obj, "obj");
			if (InPlayMode) UObject.DontDestroyOnLoad(obj);
		}
		protected static void DontDestroyOnLoad_IfPlayMode(GameObject gObj)
		{
			ThrowIfArgNullOrDestroyed(gObj, "gObj");
			if (InPlayMode) UObject.DontDestroyOnLoad(gObj.transform.root.gameObject);
		}
		protected static void DontDestroyOnLoad_IfPlayMode(Component component)
		{
			ThrowIfArgNullOrDestroyed(component, "component");
			if (InPlayMode) {
				if (component.transform.root == component.transform) UObject.DontDestroyOnLoad(component);
				else UObject.DontDestroyOnLoad(component.transform.root.gameObject);
			}
		}
	}


	/*

	public abstract class SingletonHolderBase : MonoBehaviour
	{
		/// <summary>You can call this if you want, but there should be no need. DO NOT call it from the constructor</summary>
		protected abstract void InitSingleton();

	#if UNITY_EDITOR
		/// <summary>DO NOT call this outside editor mode</summary>
		protected abstract void EditorOnlyReInitSingleton();
	#endif

	

	#if UNITY_EDITOR
		/// <summary>
		/// Derive from this class to create custom singleton editors
		/// </summary>
		[CustomEditor(typeof(SingletonHolderBase), editorForChildClasses:true)]
		[InitializeOnLoad]
		public class SingletonEditor : Editor
		{
			protected SingletonHolderBase targetSingleton;

			public sealed override void OnInspectorGUI()
			{
				if (targetSingleton == null) targetSingleton = (SingletonHolderBase)target;

				targetSingleton.InitSingleton();

				if (GUILayout.Button("Manual Re-Initialize Singleton")) {
					targetSingleton.EditorOnlyReInitSingleton();
				}

				base.OnInspectorGUI();
			}
			public virtual void OnInspectorGUIExt() { }

			//	public sealed override bool RequiresConstantRepaint()
			//	{
			//		if (targetSingleton == null) targetSingleton = (SingletonBase)target;
			//	
			//		targetSingleton.InitSingleton();
			//	
			//		return true;
			//	}
		}
	#endif
	}

	public class SingletonHolder<T> : SingletonHolderBase
		where T : UObject
	{
		private static object syncRoot = new object();

		private T _instance;

		/// <summary>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed, 
		/// it will create a buggy ghost object that will stay on the Editor scene
		/// even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </summary>
		public bool SingletonTerminated { get; private set; }



		public SingletonHolder()
		{

		}

		protected static bool InPlayMode {
			get {
#if UNITY_EDITOR
				return EditorApplication.isPlaying;
#else
				return true;
#endif		
			}
		}

		public T GetSingletonInstance()
		{
			lock (syncRoot)
			{
				if (SingletonTerminated || IsDestroyed(_instance)) {
					if (InPlayMode)
					{
						SingletonTerminated = false;
						_instance = null;
						return GetSingletonInstance(); //Recurse once (_instance is now null rather than destroyed)
					}
					else
					{
						throw CreateSingletonTerminatedEx();
					}
				}


				if (!ReferenceEquals(_instance, null)) return _instance;


				T[] foundObjects = Resources.FindObjectsOfTypeAll<T>();
				if (foundObjects.Length == 1)
				{
					LogSingletonFoundInScene(foundObjects[0]);

					_instance = foundObjects[0];
					DontDestroyOnLoad_IfPlayMode(_instance);
					return _instance;
				}
				else if (foundObjects.Length == 0)
				{
					GameObject newSingletonGObj = new GameObject("[Auto-Singleton: " + typeof(T).Name + "]");
					T newInstance = newSingletonGObj.AddComponent<T>();

					DontDestroyOnLoad_IfPlayMode(newSingletonGObj);
					DontDestroyOnLoad_IfPlayMode(newInstance); //Just in case this is needed

					LogSingletonCreated(newInstance);

					_instance = newInstance;
					return _instance;
				}
				else
				{
					throw CreateMultipleSingletonsFoundEx();
				}
			}
		}

		protected static bool IsDestroyed(UObject obj) {
			return !ReferenceEquals(obj, null) && obj.Equals(null);
		}

		protected static SingletonException CreateSingletonTerminatedEx() {
			return new SingletonException(
				"[Singleton] The singleton '"
				+ typeof(T).FullName
				+ "' has been terminated (OnDestroy or OnApplicationQuit was called on the instance,"
				+ "or the instance was detected to be non-null but destroyed)."
				+ "It will not be recreated."
			);
		}

		protected static void LogSingletonFoundInScene(T foundInstance)
		{
			Debug.Log(
				"[Singleton] An instance of singleton '"
				+ typeof(T).FullName
				+ "' was needed, and exactly one was found in the scene (object '"
				+ foundInstance.GetDebugName()
				+ "'), so that one was used.",
				foundInstance
			);
		}

		protected static void LogSingletonCreated(T newInstance)
		{
			Debug.Log(
				"[Singleton] An instance of singleton '"
				+ typeof(T).FullName
				+ "' was needed but none existed in the game, so a new instance was created (at path '"
				+ newInstance.GetDebugName()
				+ "').",
				newInstance
			);
		}

		protected static SingletonException CreateMultipleSingletonsFoundEx()
		{
			return new SingletonException(
				"[Singleton] Singleton '"
				+ typeof(T).FullName
				+ "' has not been initialized, and multiple instances are present in the scene."
			);
		}

		protected static TArg ThrowIfArgNullOrDestroyed<TArg>(TArg arg, string argName)
			where TArg : UObject
		{
			if (ReferenceEquals(arg, null)) throw new ArgumentNullException(argName);
			if (IsDestroyed(arg)) throw new ArgumentException("UnityEngine.Object is destroyed", argName);
			return arg;
		}

		/// <summary>You can call this if you want, but there should be no need. DO NOT call it from the constructor</summary>
		protected sealed override void InitSingleton()
		{
			if (_instance == null)
			{
				DontDestroyOnLoad_IfPlayMode(this);
				_instance = (T)this;

			}
			else
			{
				if (!ReferenceEquals(_instance, this))
				{
					//	string exceptionStr = (
					//		"[Singleton] Singleton '"
					//		+ typeof(T).FullName
					//		+ "' has already been initialized, but another instance was created (at path '"
					//		+ this.GetHierarchyPath()
					//		+ "'). DestroyImmediately() was called on this new instance."
					//	);
					//	try {
					//		DestroyImmediate(this);
					//	} catch (Exception e) {
					//		throw new SingletonException(exceptionStr, e);
					//	}
					//	throw new SingletonException(exceptionStr);
					this.enabled = false;
					Debug.LogError(
						"[Singleton] Singleton '"
						+ typeof(T).FullName
						+ "' has already been initialized, but another instance was created (at path '"
						+ this.GetHierarchyPath()
						+ "'). DestroyImmediately() will be called on the new instance immediately after this log message.",
						this
					);
					DestroyImmediate(this);
				}
			}
		}

#if UNITY_EDITOR
		/// <summary>DO NOT call this outside editor mode</summary>
		protected sealed override void EditorOnlyReInitSingleton()
		{
			_instance = null;
			SingletonTerminated = false;
			InitSingleton();
		}
#endif

		/// <summary>DO NOT create a method called OnDestroy. Instead, override OnDestroyExt. DO NOT call this (it is visible only to provide a warning)</summary>
		protected void OnDestroy() {
			SingletonTerminated = true;
			OnDestroyExt();
		}
		/// <summary>DO NOT create a method called OnDestroy. Instead, override OnDestroyExt</summary>
		protected virtual void OnDestroyExt() { }


		/// <summary>DO NOT create a method called OnApplicationQuit. Instead, override OnApplicationQuitExt. DO NOT call this (it is visible only to provide a warning)</summary>
		protected void OnApplicationQuit() {
			SingletonTerminated = true;
			OnApplicationQuitExt();
		}
		/// <summary>DO NOT create a method called OnApplicationQuit. Instead, override OnApplicationQuitExt</summary>
		protected virtual void OnApplicationQuitExt() { }

		protected static void DontDestroyOnLoad_IfPlayMode(UObject obj)
		{
			ThrowIfArgNullOrDestroyed(obj, "obj");
			if (InPlayMode) UObject.DontDestroyOnLoad(obj);
		}
		protected static void DontDestroyOnLoad_IfPlayMode(GameObject gObj)
		{
			ThrowIfArgNullOrDestroyed(gObj, "gObj");
			if (InPlayMode) UObject.DontDestroyOnLoad(gObj.transform.root.gameObject);
		}
		protected static void DontDestroyOnLoad_IfPlayMode(Component component)
		{
			ThrowIfArgNullOrDestroyed(component, "component");
			if (InPlayMode) {
				if (component.transform.root == component.transform) UObject.DontDestroyOnLoad(component);
				else UObject.DontDestroyOnLoad(component.transform.root.gameObject);
			}
		}
	}

	*/


	/*

	public static class UnityObjectLocator
	{
		private static object syncRoot = new object();

		//private static T _instance;

		/// <summary>
		/// When Unity quits, it destroys objects in a random order.
		/// In principle, a Singleton is only destroyed when application quits.
		/// If any script calls Instance after it have been destroyed, 
		/// it will create a buggy ghost object that will stay on the Editor scene
		/// even after stopping playing the Application. Really bad!
		/// So, this was made to be sure we're not creating that buggy ghost object.
		/// </summary>
		public static bool SingletonTerminated { get; private set; }

		private static bool InPlayMode {
			get {
	#if UNITY_EDITOR
				return EditorApplication.isPlaying;
	#else
				return true;
	#endif		
			}
		}

		public static T FindSingletonInstance<T>()
			where T : UObject
		{
			lock (syncRoot)
			{

			}
		}

		public static T GetSingletonInstance<T>(ref T _instance)
			where T : UObject
		{
			lock (syncRoot)
			{
				if (SingletonTerminated || IsDestroyed(_instance)) {
					if (InPlayMode)
					{
						SingletonTerminated = false;
						_instance = null;
						return GetSingletonInstance(ref _instance); //Recurse once (_instance is now null rather than destroyed)
					}
					else
					{
						throw CreateSingletonTerminatedEx<T>();
					}
				}


				if (!ReferenceEquals(_instance, null)) return _instance;


				T[] foundObjects = Resources.FindObjectsOfTypeAll<T>();
				if (foundObjects.Length == 1)
				{
					LogSingletonFoundInScene(foundObjects[0]);

					_instance = foundObjects[0];
					DontDestroyOnLoad_IfPlayMode(_instance);
					return _instance;
				}
				else if (foundObjects.Length == 0)
				{
					GameObject newSingletonGObj = new GameObject("[Auto-Singleton: " + typeof(T).Name + "]");
					T newInstance = newSingletonGObj.AddComponent<T>();

					DontDestroyOnLoad_IfPlayMode(newSingletonGObj);
					DontDestroyOnLoad_IfPlayMode(newInstance); //Just in case this is needed

					LogSingletonCreated(newInstance);

					_instance = newInstance;
					return _instance;
				}
				else
				{
					throw CreateMultipleSingletonsFoundEx<T>();
				}
			}
		}

		public static bool IsDestroyed(UObject obj) {
			return !ReferenceEquals(obj, null) && obj.Equals(null);
		}

		public static SingletonException CreateSingletonTerminatedEx<T>()
			where T : UObject
		{
			return new SingletonException(
				"[Singleton] The singleton '"
				+ typeof(T).FullName
				+ "' has been terminated (OnDestroy or OnApplicationQuit was called on the instance,"
				+ "or the instance was detected to be non-null but destroyed)."
				+ "It will not be recreated."
			);
		}

		public static void LogSingletonFoundInScene<T>(T foundInstance)
			where T : UObject
		{
			Debug.Log(
				"[Singleton] An instance of singleton '"
				+ typeof(T).FullName
				+ "' was needed, and exactly one was found in the scene (object '"
				+ foundInstance.GetDebugName()
				+ "'), so that one was used.",
				foundInstance
			);
		}

		public static void LogSingletonCreated<T>(T newInstance)
			where T : UObject
		{
			Debug.Log(
				"[Singleton] An instance of singleton '"
				+ typeof(T).FullName
				+ "' was needed but none existed in the game, so a new instance was created (at path '"
				+ newInstance.GetDebugName()
				+ "').",
				newInstance
			);
		}

		public static SingletonException CreateMultipleSingletonsFoundEx<T>()
			where T : UObject
		{
			return new SingletonException(
				"[Singleton] Singleton '"
				+ typeof(T).FullName
				+ "' has not been initialized, and multiple instances are present in the scene."
			);
		}

		public static TArg ThrowIfArgNullOrDestroyed<TArg>(TArg arg, string argName)
			where TArg : UObject
		{
			if (ReferenceEquals(arg, null)) throw new ArgumentNullException(argName);
			if (IsDestroyed(arg)) throw new ArgumentException("UnityEngine.Object is destroyed", argName);
			return arg;
		}

		public static void DontDestroyOnLoad_IfPlayMode(UObject obj)
		{
			ThrowIfArgNullOrDestroyed(obj, "obj");
			if (InPlayMode) UObject.DontDestroyOnLoad(obj);
		}
		public static void DontDestroyOnLoad_IfPlayMode(GameObject gObj)
		{
			ThrowIfArgNullOrDestroyed(gObj, "gObj");
			if (InPlayMode) UObject.DontDestroyOnLoad(gObj.transform.root.gameObject);
		}
		public static void DontDestroyOnLoad_IfPlayMode(Component component)
		{
			ThrowIfArgNullOrDestroyed(component, "component");
			if (InPlayMode) {
				if (component.transform.root == component.transform) UObject.DontDestroyOnLoad(component);
				else UObject.DontDestroyOnLoad(component.transform.root.gameObject);
			}
		}
	}

	*/







	[Serializable]
	public class SingletonException : InvalidOperationException
	{
		public SingletonException() { }
		public SingletonException(string message) : base(message) { }
		public SingletonException(string message, Exception inner) : base(message, inner) { }
		protected SingletonException(
		  System.Runtime.Serialization.SerializationInfo info,
		  System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}


//*/
