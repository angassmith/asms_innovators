﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames
{
	public class KeyedRefreshingCache<TKey, TCache> : IRefreshingCache
		where TCache : IRefreshingCache
	{
		private static NotSupportedException WriteNotSupportedEx {
			get { return new NotSupportedException("Type is read-only."); }
		}

		private Dictionary<TKey, TCache> _dict = new Dictionary<TKey, TCache>();

		public Func<TKey, TCache> CacheFactory { get; private set; }

		public event EventHandler Refreshed;

		/// <summary>
		/// Note: The same instance is always returned, so the returned instance is modified as the cache is modified.
		/// </summary>
		public ReadOnlyDictionary<TKey, TCache> CachedValues { get; private set; }

		public KeyedRefreshingCache(Func<TKey, TCache> cacheFactory)
		{
			if (cacheFactory == null) throw new ArgumentNullException("cacheFactory");
			this.CacheFactory = cacheFactory;
			this.CachedValues = new ReadOnlyDictionary<TKey, TCache>(_dict);
		}
		
		

		public void Refresh()
		{
			_dict.Clear();
			if (Refreshed != null) Refreshed.Invoke(this, EventArgs.Empty);
		}

		public TCache this[TKey key] {
			get {
				TCache res;
				if (_dict.TryGetValue(key, out res)) {
					return res;
				} else {
					res = CacheFactory(key);
					_dict.Add(key, res);
					return res;
				}
			}
		}

		//	public class CachedValuesDict : IDictionary<TKey, TCache>
		//	{
		//		private KeyedRefreshableCache<TKey, TCache> _cache;
		//	
		//		internal CachedValuesDict(KeyedRefreshableCache<TKey, TCache> cache)
		//		{
		//			if (cache == null) throw new ArgumentNullException("cache");
		//			this._cache = cache;
		//		}
		//	
		//		public int Count { get { return _cache._dict.Where(x => x != null ; } }
		//		public ICollection<TKey> Keys { get { return _cache._dict.Keys; } }
		//	
		//		public ICollection<TCache> Values { get { return _cache._dict.Values; } }
		//	
		//		bool ICollection<KeyValuePair<TKey, TCache>>.IsReadOnly { get { return true; } }
		//	
		//		public TCache this[TKey key] {
		//			get { return _cache._dict[key]; }
		//		}
		//	
		//		ReadOnlyCollection<TCache> IDictionary<TKey, TCache>.this[TKey key] {
		//			get { return this[key]; }
		//			set { throw WriteNotSupportedEx; }
		//		}
		//	
		//		public void Clear() { _dict.Clear(); }
		//		public bool KeyIsCached(TKey key) { return _dict.ContainsKey(key); }
		//		public IEnumerator<KeyValuePair<TKey, ReadOnlyCollection<TCache>>> GetEnumerator() { return _dict.GetEnumerator(); }
		//		IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }
		//	
		//		bool IDictionary<TKey, TCache>.TryGetValue(TKey key, out TCache value) { value = this[key]; return true; }
		//		bool ICollection<KeyValuePair<TKey, TCache>>.Contains(KeyValuePair<TKey, TCache> item) { return ((ICollection<KeyValuePair<TKey, TCache>>)_dict).Contains(item); }
		//		void ICollection<KeyValuePair<TKey, TCache>>.CopyTo(KeyValuePair<TKey, TCache>[] array, int arrayIndex) { ((ICollection<KeyValuePair<TKey, TCache>>)_dict).CopyTo(array, arrayIndex); }
		//	
		//		void IDictionary<TKey, TCache>.Add(TKey key, TCache value) { throw WriteNotSupportedEx; }
		//		bool IDictionary<TKey, TCache>.Remove(TKey key) { throw WriteNotSupportedEx; }
		//		void ICollection<KeyValuePair<TKey, TCache>>.Add(KeyValuePair<TKey, TCache> item) { throw WriteNotSupportedEx; }
		//		bool ICollection<KeyValuePair<TKey, TCache>>.Remove(KeyValuePair<TKey, TCache> item) { throw WriteNotSupportedEx; }
		//	
		//		bool IDictionary<TKey, TCache>.ContainsKey(TKey key) { return KeyIsCached(key); }
		//	}
	}
}

//*/