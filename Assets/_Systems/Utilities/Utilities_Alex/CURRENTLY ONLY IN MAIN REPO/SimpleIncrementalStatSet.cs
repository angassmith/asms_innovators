﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

public class IncrementalMean
{
	public double Sum { get; private set; }
	public int Count { get; private set; }
	
	/// <summary>Calculates and returns the mean (Sum/Count).</summary>
	public double Mean { get { return Sum / Count; } }
	

	public IncrementalMean() { }
	public IncrementalMean(IEnumerable<double> initialData) {
		Incorporate(initialData);
	}
	public IncrementalMean(params double[] initialData) {
		Incorporate(initialData);
	}



	public void Incorporate(double addition)
	{
		Sum += addition;
		Count++;
	}
	public void Incorporate(IEnumerable<double> additions) {
		foreach (double addition in additions) {
			Incorporate(addition);
		}
	}
	public void Incorporate(params double[] additions) {
		Incorporate((IEnumerable<double>)additions);
	}



	/// <summary>
	/// Changes the sum and mean to account for a modification to one of the data points.
	/// Does this by adding <paramref name="changeAmount"/> to the sum, and leaving count the same.
	/// </summary>
	public void Modify(double changeAmount)
	{
		Sum += changeAmount;
		//Keep Count the same
	}
	public void Modify(IEnumerable<double> changeAmounts) {
		foreach (double change in changeAmounts) {
			Incorporate(change);
		}
	}
	public void Modify(params double[] changeAmounts) {
		Modify((IEnumerable<double>)changeAmounts);
	}



	public void Remove(double item)
	{
		Sum -= item;
		Count--;
	}
	public void Remove(IEnumerable<double> items) {
		foreach (double item in items) {
			Remove(item);
		}
	}
	public void Remove(params double[] items) {
		Remove((IEnumerable<double>)items);
	}



	public void Reset()
	{
		Sum = 0;
		Count = 0;
	}
}

public class IncrementalRange
{
	public double Min { get; private set; }
	public double Max { get; private set; }

	/// <summary>Calculates and returns the range (Max - Min)</summary>
	public double Range { get { return Max - Min; } }

	public IncrementalRange() { }
	public IncrementalRange(IEnumerable<double> initialData) {
		Incorporate(initialData);
	}
	public IncrementalRange(params double[] initialData) {
		Incorporate(initialData);
	}


	public void Incorporate(double addition)
	{
		Min = Math.Min(Min, addition);
		Max = Math.Max(Max, addition);
	}
	public void Incorporate(IEnumerable<double> additions) {
		foreach (double addition in additions) {
			Incorporate(addition);
		}
	}
	public void Incorporate(params double[] additions) {
		Incorporate((IEnumerable<double>)additions);
	}


	public void Reset()
	{
		Min = 0;
		Max = 0;
	}
}
 
//*/