﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames
{
	public class PathID : ScriptableObject
	{
		public const string PathIDAssetExtension = ".pathId.asset";

		[SerializeField]
		[HideInInspector]
		private string _referencedPath;
		public string ReferencedPath {
			get {
#if UNITY_EDITOR
				UpdatePath();
#endif
				return _referencedPath;
			}
		}

#if UNITY_EDITOR
		private void OnValidate() { UpdatePath(); } //Editor only
		private void OnReset() { UpdatePath(); } // ''
		private void Awake() { UpdatePath(); } //Idk if this runs in the editor
		private void Start() { UpdatePath(); } // ''

		private void UpdatePath()
		{
			string idPath = AssetDatabase.GetAssetPath(this);
			_referencedPath = idPath.Substring(0, idPath.Length - PathIDAssetExtension.Length);
		}
#endif

#if UNITY_EDITOR
		[MenuItem("Component/Path ID")]
		public static PathID Create()
		{
			return CreateInstance<PathID>();
		}

		[MenuItem("Component/Path ID Asset")]
		public static void CreateAsset()
		{
			PathID asset = CreateInstance<PathID>();
			
			string selPath = AssetDatabase.GetAssetPath(Selection.activeObject);
			if (selPath == "") selPath = "Assets/New Path ID";

			string path = selPath + PathIDAssetExtension;
			
			AssetDatabase.CreateAsset(asset, path);

			AssetDatabase.SaveAssets();
        	AssetDatabase.Refresh();
			EditorUtility.FocusProjectWindow();
			Selection.activeObject = asset;
		}
#endif
	}
}

//*/