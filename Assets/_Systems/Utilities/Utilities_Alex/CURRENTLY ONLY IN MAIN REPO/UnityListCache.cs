﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine;
using UObject = UnityEngine.Object;

namespace RevolitionGames.Utilities.Alex
{
	/// <summary>
	/// UNTESTED cache for UnityEngine.Objects.
	/// Removes destroyed objects automatically.
	/// This should hopefully remove duplicates created by the serializer.
	/// </summary>
	public class UnityListCache<T> : ICollection<T>
		where T : UObject
	{
		private List<T> _list;
		public EqualityComparer<T> Comparer { get; private set; }

		private int _version;

		public UnityListCache()
		{
			_list = new List<T>();
			Comparer = EqualityComparer<T>.Default;
		}

		public int Count {
			get {
				Collapse();
				return _list.Count;
			}
		}

		bool ICollection<T>.IsReadOnly { get { return false; } }

		public int Collapse()
		{
			int newSize = 0;

			using (var e = this.GetEnumerator()) {
				while (e.MoveNext()) newSize++;
			}

			return newSize;
		}

		

		/// <summary>
		/// Enumerates the <see cref="UnityListCache{T}"/>,
		/// collapsing it to remove destroyed entries as it goes.
		/// If the <see cref="UnityListCache{T}"/> is already collapsed, this is almost as fast as normal enumeration.
		/// <para/>
		/// Collapsing at the start of enumeration could cause problems if other threads
		/// (the Unity loading thread(s), for example)
		/// destroyed some entries during enumeration.
		/// </summary>
		public IEnumerator<T> GetEnumerator()
		{
			return GetEnumeratorIterator(_version);
		}

		private IEnumerator<T> GetEnumeratorIterator(int startVersion)
		{
			//Now that deferred execution has started, check the version
			if (this._version != startVersion) throw CreateEnumeratorVersionException();

			int insertIndex = 0;
			for (int i = 0; i < _list.Count; i++)
			{
				T item = _list[i];

				if (item) //If not null or disposed
				{
					if (i != insertIndex)
					{
						_list[i] = null;
						_list[insertIndex] = item;
					}
					insertIndex++;

					yield return item;

					//Now that deferred execution has resumed, check the version
					if (this._version != startVersion) throw CreateEnumeratorVersionException();
				}
			}

			_list.RemoveRange(insertIndex, _list.Count - insertIndex);
		}

		private InvalidOperationException CreateEnumeratorVersionException()
		{
			return new InvalidOperationException("The collection has been modified since the IEnumerator<T> was created.");
		}

		public void Add(T item) {
			//No need to collapse
			_version++;
			_list.Add(item);
		}

		public bool Remove(T item) {
			//No need to collapse

			if (!item) return false; //The list cannot visibly contain a null or disposed item

			_version++;
			return _list.Remove(item);
		}

		public void Clear() {
			_version++;
			_list.Clear();
		}

		public bool Contains(T item)
		{
			foreach (var entry in this) {
				if (Comparer.Equals(entry, item)) return true;
			}

			return false;
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			if (array == null) throw new ArgumentNullException("array");
			if (arrayIndex < 0) throw new ArgumentOutOfRangeException("arrayIndex", arrayIndex, "Cannot be less than 0.");

			if (this.Count > array.Length - arrayIndex) { //Calling Count collapses the list
				throw new ArgumentException(
					"The number of elements in the source ICollection<T> is greater than the available space "
					+ "from arrayIndex to the end of the destination array."
				);
			}

			int i = arrayIndex;
			foreach (var entry in this) {
				array[i] = entry;
				i++;
			}
		}

		IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

		#region Utilities

		/// <summary>Used to prevent code from possibly being optimised away</summary>
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static void DoNothing<TObj>(TObj obj) { }

		/// <summary>Used to prevent code from possibly being optimised away</summary>
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static void DoNothing() { }

		#endregion
	}
}

//*/