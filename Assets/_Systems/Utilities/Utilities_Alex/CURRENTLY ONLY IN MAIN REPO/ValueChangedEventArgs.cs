﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Utilities.Alex
{

	public class ValueChangedEventArgs<T> : EventArgs
	{
		public T OldValue { get; private set; }
		public T NewValue { get; private set; }

		public ValueChangedEventArgs(T oldValue, T newValue)
		{
			this.OldValue = oldValue;
			this.NewValue = newValue;
		}
	}
}

//*/