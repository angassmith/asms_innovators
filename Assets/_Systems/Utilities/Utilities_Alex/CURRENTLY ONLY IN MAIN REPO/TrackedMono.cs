﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames
{
	public abstract class TrackedMono : MonoBehaviour
	{
		protected enum TrackingEvent
		{
			Awake,
			Start,
			Reset,
			Destroy,
			ApplicationQuit,
		}

		protected abstract bool TrackInEditor { get; }

		protected abstract void UpdateTracking(TrackingEvent evt);

		private bool IsInPlayMode {
			get {
#if UNITY_EDITOR
				return EditorApplication.isPlaying;
#else
				return true;
#endif
			}
		}

		private void TrackIfNeeded(TrackingEvent evt)
		{
			if (TrackInEditor || IsInPlayMode) {
				UpdateTracking(evt);
			}
		}

		/** <summary>Override to provide functionality for the relevant MonoBehaviour callback method</summary> */ protected virtual void AwakeImpl() { }
		/** <summary>Override to provide functionality for the relevant MonoBehaviour callback method</summary> */ protected virtual void StartImpl() { }
		/** <summary>Override to provide functionality for the relevant MonoBehaviour callback method</summary> */ protected virtual void OnDestroyImpl() { }
		/** <summary>Override to provide functionality for the relevant MonoBehaviour callback method</summary> */ protected virtual void OnApplicationQuitImpl() { }
		/** <summary>Override to provide functionality for the relevant MonoBehaviour callback method</summary> */ protected virtual void ResetImpl() { }

		/** <summary>Do not create a method with this name. Instead, override &lt;this-name&gt;Impl(). Do not call this (it is visible only to provide a warning)</summary> */ protected void Awake            () { TrackIfNeeded(TrackingEvent.Awake  ); AwakeImpl(); }
		/** <summary>Do not create a method with this name. Instead, override &lt;this-name&gt;Impl(). Do not call this (it is visible only to provide a warning)</summary> */ protected void Start            () { TrackIfNeeded(TrackingEvent.Start  ); AwakeImpl(); }
		/** <summary>Do not create a method with this name. Instead, override &lt;this-name&gt;Impl(). Do not call this (it is visible only to provide a warning)</summary> */ protected void OnDestroy        () { TrackIfNeeded(TrackingEvent.Destroy); OnDestroyImpl(); }
		/** <summary>Do not create a method with this name. Instead, override &lt;this-name&gt;Impl(). Do not call this (it is visible only to provide a warning)</summary> */ protected void OnApplicationQuit() { TrackIfNeeded(TrackingEvent.Destroy); OnApplicationQuitImpl(); }
		/** <summary>Do not create a method with this name. Instead, override &lt;this-name&gt;Impl(). Do not call this (it is visible only to provide a warning)</summary> */ protected void Reset            () { TrackIfNeeded(TrackingEvent.Reset  ); ResetImpl(); }

	}
}

//*/