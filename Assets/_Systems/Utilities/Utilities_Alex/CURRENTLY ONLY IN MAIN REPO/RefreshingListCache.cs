﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UObject = UnityEngine.Object;

namespace RevolitionGames
{
	public class RefreshingListCache<T> : IList<T>, IRefreshingCache
	{
		private static NotSupportedException WriteNotSupportedEx {
			get { return new NotSupportedException("Type is read-only."); }
		}

		private IList<T> _listOrNull = null;
		private IList<T> _list {
			get { return _listOrNull ?? (_listOrNull = Finder.Invoke()); }
		}

		public Func<IList<T>> Finder { get; private set; }

		public event EventHandler Refreshed;

		public RefreshingListCache(Func<IList<T>> finder)
		{
			if (finder == null) throw new ArgumentNullException("finder");
			this.Finder = finder;
		}

		public void Refresh()
		{
			_listOrNull = null;
			if (Refreshed != null) Refreshed.Invoke(this, EventArgs.Empty);
		}

		#region Interface implementations

		public int Count { get { return this._list.Count; } }
		bool ICollection<T>.IsReadOnly { get { return true; } }

		public T this[int index] {
			get { return this._list[index]; }
		}
		T IList<T>.this[int index] {
			get { return this[index]; }
			set { throw WriteNotSupportedEx; }
		}

		public int IndexOf(T item) { return this._list.IndexOf(item); }
		public bool Contains(T item) { return this._list.Contains(item); }
		public void CopyTo(T[] array, int arrayIndex) { this._list.CopyTo(array, arrayIndex); }
		public IEnumerator<T> GetEnumerator() { return this._list.GetEnumerator(); }
		IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

		void IList<T>.Insert(int index, T item) { throw WriteNotSupportedEx; }
		void IList<T>.RemoveAt(int index) { throw WriteNotSupportedEx; }
		void ICollection<T>.Add(T item) { throw WriteNotSupportedEx; }
		void ICollection<T>.Clear() { throw WriteNotSupportedEx; }
		bool ICollection<T>.Remove(T item) { throw WriteNotSupportedEx; }

		#endregion
	}
}

//*/