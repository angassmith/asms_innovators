﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames
{
	public interface IRefreshingCache
	{
		void Refresh();
		event EventHandler Refreshed;
	}

	//	public interface IRefreshableCache<T> : IRefreshableCache
	//	{
	//		T Value { get; }
	//	}
}

//*/