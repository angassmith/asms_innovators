﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace RevolitionGames.Utilities.Alex
{
	public static class SceneUtils
	{
		/// <summary>Fires when a scene is loaded, and clears all subscribers after firing. Triggered by <see cref="SceneManager.sceneLoaded"/></summary>
		public static event UnityAction<Scene, LoadSceneMode> SceneLoadedAutoUnsub;
		/// <summary>Fires when a scene is unloaded, and clears all subscribers after firing. Triggered by <see cref="SceneManager.sceneUnloaded"/></summary>
		public static event UnityAction<Scene> SceneUnloadedAutoUnsub;
		/// <summary>Fires when the active scene changes, and clears all subscribers after firing. Triggered by <see cref="SceneManager.activeSceneChanged"/></summary>
		public static event UnityAction<Scene, Scene> ActiveSceneChangedAutoUnsub;

		private static void RunSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (SceneLoadedAutoUnsub != null) {
				SceneLoadedAutoUnsub(scene, mode);
				SceneLoadedAutoUnsub = null;
			}
		}

		private static void RunSceneUnloaded(Scene scene)
		{
			if (SceneUnloadedAutoUnsub != null) {
				SceneUnloadedAutoUnsub(scene);
				SceneUnloadedAutoUnsub = null;
			}
		}

		private static void RunActiveSceneChanged(Scene oldScene, Scene newScene)
		{
			if (ActiveSceneChangedAutoUnsub != null) {
				ActiveSceneChangedAutoUnsub(oldScene, newScene);
				ActiveSceneChangedAutoUnsub = null;
			}
		}

		static SceneUtils()
		{
			SceneManager.sceneLoaded += RunSceneLoaded;
			SceneManager.sceneUnloaded += RunSceneUnloaded;
			SceneManager.activeSceneChanged += RunActiveSceneChanged;
		}
	}
}

//*/