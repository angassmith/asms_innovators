﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Utilities.Alex
{
	public static class UnityInit
	{
		public static T ThrowIfEditorFieldNull<T>(T field, string name)
		{
			InvalidOperationException ex;
			if (TryGetEditorFieldNullEx(field, name, out ex)) {
				throw ex;
			} else {
				return field;
			}
		}

		public static T LogIfEditorFieldNull<T>(this UnityEngine.Object context, T field, string name)
		{
			InvalidOperationException ex;
			if (TryGetEditorFieldNullEx(field, name, out ex)) {
				Debug.LogException(exception: ex, context: context);
			}
			return field;
		}

		public static T LogIfEditorFieldNull<T>(T field, string name)
		{
			InvalidOperationException ex;
			if (TryGetEditorFieldNullEx(field, name, out ex)) {
				Debug.LogException(exception: ex);
			}
			return field;
		}

		private static bool TryGetEditorFieldNullEx<T>(T field, string name, out InvalidOperationException ex)
		{
			if (field == null)
			{
				ex = new InvalidOperationException("Field '" + name + "' has not been assigned in the editor");
				return true;
			}
			else if (field is UnityEngine.Object && field.Equals(null))
			{
				//If field is a Unity-Object and is destroyed (yes, that's the best way to check it)
				ex = new InvalidOperationException(
					"Field '" + name + "' has been assigned in the editor to a destroyed Unity-Object"
				);
				return true;
			}
			else
			{
				ex = null;
				return false;
			}
		}
	}
}

//*/