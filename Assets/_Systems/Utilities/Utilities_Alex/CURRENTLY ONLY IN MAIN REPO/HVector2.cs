﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Utilities.Alex
{
	/// <summary>
	/// A 2D vector that specifies the horizontal coordinates (X and Z,
	/// whereas <see cref="Vector2"/> specifies the X and Y coordinates).
	/// </summary>
	public struct HVector2
	{
		/// <summary>
		/// Shortcut for "<see langword="default"/>(<see cref="HVector2"/>)"
		/// or "<see langword="new"/> <see cref="HVector2"/>(0, 0)".
		/// </summary>
		public static HVector2 Zero { get { return default(HVector2); } }
		/// <summary>Shortcut for "<see langword="new"/> <see cref="HVector2"/>(1, 1)".</summary>
		public static HVector2 One { get { return new HVector2(1, 1); } }
		
		/// <summary>Shortcut for "<see langword="new"/> <see cref="HVector2"/>(0, 1)".</summary>
		public static HVector2 Forward { get { return new HVector2(0, 1); } }
		/// <summary>Shortcut for "<see langword="new"/> <see cref="HVector2"/>(0, -1)".</summary>
		public static HVector2 Back { get { return new HVector2(0, -1); } }
		/// <summary>Shortcut for "<see langword="new"/> <see cref="HVector2"/>(-1, 0)".</summary>
		public static HVector2 Left { get { return new HVector2(-1, 0); } }
		/// <summary>Shortcut for "<see langword="new"/> <see cref="HVector2"/>(1, 0)".</summary>
		public static HVector2 Right { get { return new HVector2(1, 0); } }



		public readonly float X;
		public readonly float Z;

		public float SqrMagnitude { get { return X*X + Z*Z; } }
		public float Magnitude { get { return Mathf.Sqrt(SqrMagnitude); } }
		public HVector2 Normalized {
			get {
				float magnitude = this.Magnitude;
				return new HVector2(x: this.X / magnitude, z: this.Z / magnitude);
			}
		}

		/// <summary>
		/// Constructs an <see cref="HVector2"/>
		/// </summary>
		/// <param name="x"></param>
		/// <param name="z"></param>
		public HVector2(float x, float z)
		{
			this.X = x;
			this.Z = z;
		}
		
		/// <summary>
		/// Constructs an <see cref="HVector2"/> from the X and Z coordinates of a <see cref="Vector3"/>.
		/// </summary>
		/// <param name="vector"></param>
		public HVector2(Vector3 vector) : this(x: vector.x, z: vector.z) { }


		/// <summary>Returns a new <see cref="HVector2"/> with the current Z and specified X coordinates</summary>
		public HVector2 UpdateX(float newX) { return new HVector2(x: newX, z: this.Z); }
		/// <summary>Returns a new <see cref="HVector2"/> with the current X and specified Z coordinates</summary>
		public HVector2 UpdateZ(float newZ) { return new HVector2(x: this.X, z: newZ); }

		public Vector3 ToVector3() { return new Vector3(x: this.X, y: 0, z: this.Z); }
		public Vector3 ToVector3(float y) { return new Vector3(x: this.X, y: y, z: this.Z); }


		public override string ToString() {
			return "HVector2 { X: " + X + ", Z: " + Z + " }";
		}
		public static bool Equals(HVector2 a, HVector2 b) {
			return a.X == b.X && a.Z == b.Z;
		}
		public override bool Equals(object obj) {
			return obj is HVector2 && Equals(this, (HVector2)obj);
		}
		public override int GetHashCode() {
			unchecked {
				int hash = 17;
				hash = hash * 23 + X.GetHashCode();
				hash = hash * 23 + Z.GetHashCode();
				return hash;
			}
		}

		/// <summary>Returns the distance between a and b.</summary>
		public static float Distance(HVector2 a, HVector2 b) {
			return (a - b).Magnitude;
		}

		/// <summary>
		/// Returns the angle in degrees between lines from the origin to <paramref name="from"/>
		/// and <paramref name="to"/> (same as <see cref="Vector2.Angle(Vector2, Vector2)"/>, etc).
		/// </summary>
		public static float Angle(HVector2 from, HVector2 to) {
			return Vector2.Angle(new Vector2(x: from.X, y: from.Z), new Vector2(x: to.X, y: to.Z));
		}


		/// <summary>Returns a vector that is made from the largest components of two vectors.</summary>
		public static HVector2 Max(HVector2 a, HVector2 b) {
			return new HVector2(x: Math.Max(a.X, b.X), z: Math.Max(a.Z, b.Z));
		}
		/// <summary>Returns a vector that is made from the smallest components of two vectors.</summary>
		public static HVector2 Min(HVector2 a, HVector2 b) {
			return new HVector2(x: Math.Min(a.X, b.X), z: Math.Min(a.Z, b.Z));
		}

		

		public static bool operator ==(HVector2 a, HVector2 b) { return Equals(a, b); }
		public static bool operator !=(HVector2 a, HVector2 b) { return !(a == b); }

		public static HVector2 operator -(HVector2 vector) { return new HVector2(x: -vector.X, z: -vector.Z); }
		public static HVector2 operator +(HVector2 a, HVector2 b) { return new HVector2(x: a.X + b.X, z: a.Z + b.Z); }
		public static HVector2 operator -(HVector2 a, HVector2 b) { return new HVector2(x: a.X - b.X, z: a.Z - b.Z); }
		public static HVector2 operator *(HVector2 vector, float scalar) {
			return new HVector2(x: vector.X * scalar, z: vector.Z * scalar);
		}
		public static HVector2 operator *(float scalar, HVector2 vector) { return vector * scalar; }
		public static HVector2 operator /(HVector2 vector, float scalar) {
			return new HVector2(x: vector.X / scalar, z: vector.Z / scalar);
		}
	}
}

//*/