﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UObject = UnityEngine.Object;

namespace RevolitionGames.Utilities.Alex
{
	public abstract class LinkedMono<TMono, TObj> : MonoBehaviour
		where TMono : LinkedMono<TMono, TObj>
		where TObj : MonoLinkedObject<TMono, TObj>
	{
		private TObj _linkedObj;
		public TObj LinkedObj {
			get { return _linkedObj; }
			set {
				if (ReferenceEquals(_linkedObj, value)) return;

				if (_linkedObj != null)
				{
					//If the previous linked object is non-null, its link must be cleared (set to null).
					//Doing this would cause infinite recursion, as both objects would try to clear each others'
					//links before letting their own link be cleared (by the other object).
					//To fix this, the current linked object is stored in a temporary variable, and the field
					//set to null so that it's already cleared by the time the other object tries to clear it.
					var oldLinkedObj = _linkedObj;
					_linkedObj = null;
					oldLinkedObj.Mono = null;
				}

				_linkedObj = value;

				//Set the linked object's link, if it is not already set (to avoid infinite recursion)
				if (value != null && !ReferenceEquals(value.Mono, this)) {
					value.Mono = (TMono)this;
				}
			}
		}
	}

	

	public abstract class MonoLinkedObject<TMono, TObj>
		where TMono : LinkedMono<TMono, TObj>
		where TObj : MonoLinkedObject<TMono, TObj>
	{
		private TMono _mono;
		public TMono Mono {
			get { return _mono; }
			set {
				if (ReferenceEquals(_mono, value)) return;

				if (_mono != null)
				{
					//If the previous linked mono is non-null, its link must be cleared (set to null).
					//Doing this would cause infinite recursion, as both objects would try to clear each others'
					//links before letting their own link be cleared (by the other object).
					//To fix this, the current linked mono is stored in a temporary variable, and the field
					//set to null so that it's already cleared by the time the other object tries to clear it.
					var oldMono = _mono;
					_mono = null;
					oldMono.LinkedObj = null;
				}

				_mono = value;

				//Set the linked object's link, if it is not already set (to avoid infinite recursion)
				if (value != null && !ReferenceEquals(value.LinkedObj, this)) {
					value.LinkedObj = (TObj)this;
				}
			}
		}
	}

	public abstract class LinkedObject<TThis, TLinked> : MonoBehaviour
		where TLinked : LinkedObject<TLinked, TThis>
		where TThis : LinkedObject<TThis, TLinked>
	{
		private TLinked _linkedObj;
		protected TLinked LinkedObj {
			get { return _linkedObj; }
			set {
				if (_linkedObj != null) _linkedObj._linkedObj = null;
				_linkedObj = value;
				
				if (_linkedObj != null) _linkedObj._linkedObj = (TThis)this;
			}
		}
	}
}

//*/