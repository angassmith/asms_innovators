﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

[System.Serializable]
public class ExceptionCollection : Exception
{
	public readonly Exception[] Exceptions;

	public ExceptionCollection(Exception[] exceptions)
	{
		this.Exceptions = exceptions == null ? new Exception[0] : exceptions;
	}

	public override string ToString()
	{
		StringBuilder res = new StringBuilder();
		res.Append("Exception Collection: [\r\n");
		res.Append("  \r\n");
		foreach (Exception ex in Exceptions)
		{
			res.Append("  ");
			res.Append(Regex.Replace(ex == null ? "null" : ex.ToString(), "(\r\n|\r|\n)", "\r\n  "));
			res.Append("\r\n");
		}
		res.Append("  \r\n");
		res.Append("]");

		return res.ToString();
	}
}
