﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using UnityEngine;

[System.Serializable]
public class DelayedLogger {

	private List<DelayedLogItem> LogItems = new List<DelayedLogItem>();

	public DelayedLogger() { }

	public void Add(DelayedLogItem item)
	{
		if (item == null) throw new ArgumentNullException("item", "The item to log cannot be null");
		LogItems.Add(item);
	}

	public ReadOnlyCollection<DelayedLogItem> GetLogItems() {
		return LogItems.AsReadOnly();
	}

	public bool ContainsAnyOfAnyType(params DelayedLogItem.LogItemType[] logTypes)
	{
		foreach (DelayedLogItem logItem in LogItems) {
			for (int i = 0; i < logTypes.Length; i++) {
				if (logTypes[i] == DelayedLogItem.LogItemType.Context) {
					if ((logItem.LogType & DelayedLogItem.LogItemType.Context) != 0) return true;
				} else {
					if (logItem.LogType == logTypes[i]) return true;
				}
			}
		}
		return false;
	}

	public void ApplyAllIndividually()
	{
		foreach (DelayedLogItem item in LogItems)
		{
			item.Apply();
		}
	}

	public void ApplyAllTogether()
	{
		List<Exception> ToThrow = new List<Exception>();

		foreach (DelayedLogItem item in LogItems)
		{
			if (item.LogType == DelayedLogItem.LogItemType.ThrowException) {
				ToThrow.Add(item.Exception);
			} else {
				item.Apply();
			}

			//	switch (item.LogType)
			//	{
			//		case DelayedLogItem.LogItemType.LogMessage                                         : Debug.Log         (item.Message                ); break;
			//		case DelayedLogItem.LogItemType.LogMessage     | DelayedLogItem.LogItemType.Context: Debug.Log         (item.Message  , item.Context); break;
			//		case DelayedLogItem.LogItemType.LogWarning                                         : Debug.LogWarning  (item.Message                ); break;
			//		case DelayedLogItem.LogItemType.LogWarning     | DelayedLogItem.LogItemType.Context: Debug.LogWarning  (item.Message  , item.Context); break;
			//		case DelayedLogItem.LogItemType.LogError                                           : Debug.LogError    (item.Message                ); break;
			//		case DelayedLogItem.LogItemType.LogError       | DelayedLogItem.LogItemType.Context: Debug.LogError    (item.Message  , item.Context); break;
			//		case DelayedLogItem.LogItemType.LogException                                       : Debug.LogException(item.Exception              ); break;
			//		case DelayedLogItem.LogItemType.LogException   | DelayedLogItem.LogItemType.Context: Debug.LogException(item.Exception, item.Context); break;
			//		case DelayedLogItem.LogItemType.ThrowException                                     : ToThrow.Add(item.Exception); break; //[Difference]
			//		default:
			//			Debug.LogError(
			//					"The DelayedLogItem could not be applied"
			//				+ " (Message = "+ ToString(item.Message, true)
			//				+ ", Exception = " + ToString(item.Exception)
			//				+ ", Context = " + ToString(item.Context)
			//				+ ", LogType = " + ToString(item.LogType)
			//				+ ")"
			//			);
			//			break;
			//	}
		}

		//Throw any exceptions
		if (ToThrow.Count == 1)
		{
			throw ToThrow[0];
		}
		else if (ToThrow.Count > 1)
		{
			throw new ExceptionCollection(ToThrow.ToArray());
		}
	}

	private static string ToString(object obj, bool withQuotesIfString = false)
	{
		if (withQuotesIfString) {
			if (obj is string) return obj == null ? "null" : ("\"" + obj.ToString() + "\"");
			else return obj == null ? "null" : obj.ToString();
		} else {
			return obj == null ? "null" : obj.ToString();
		}
	}
}


[System.Serializable]
public class DelayedLogItem
{
	public enum LogItemType
	{
		LogMessage     = 0,
		LogError       = 1,
		LogWarning     = 2,
		LogException   = 3,
		ThrowException = 4,
		Context        = 8,
	}

	public readonly LogItemType LogType;
	public readonly string Message;
	public readonly Exception Exception;
	public readonly UnityEngine.Object Context;
	public readonly string StackTrace;
	
	private DelayedLogItem(LogItemType logType, Exception exception, UnityEngine.Object context = null)
	{
		this.LogType = logType;
		this.Exception = exception;
		this.Context = context;
		//	this.StackTrace = FormatStackTrace(new System.Diagnostics.StackTrace(2));
		this.StackTrace = StackTraceUtility.ExtractStackTrace();
	}
	private DelayedLogItem(LogItemType logType, string message, UnityEngine.Object context = null)
	{
		this.LogType = logType;
		this.Message = message;
		this.Context = context;
		//	this.StackTrace = FormatStackTrace(new System.Diagnostics.StackTrace(2));
		this.StackTrace = StackTraceUtility.ExtractStackTrace();
	}

	private static string FormatStackTrace(System.Diagnostics.StackTrace trace)
	{
		if (trace == null) throw new ArgumentNullException("trace");
		return (
			trace
			.GetFrames()
			.Aggregate("", (strSoFar, frame) => {
				MethodBase method = frame.GetMethod();
				return (
					strSoFar
					+ " at "
					+ method.DeclaringType.FullName + "." + method.Name + " "
					+ "(in file '" + frame.GetFileName() + "', "
					+ "line " + frame.GetFileLineNumber() + ", col " + frame.GetFileColumnNumber() + ")"
					+ "\r\n"
				);
			})
		);
	}

	public void Apply()
	{
		string msg = Message;
		msg = Regex.Replace(msg, @"(\r\n|\r|\n)", "\r\n");
		if (!msg.EndsWith("\r\n")) msg += "\r\n";
		msg += StackTrace;
		msg = Regex.Replace(msg, @"(\r\n|\r|\n)", "\r\n");
		msg = Regex.Replace(msg, @"(\r\n)*$", "");
		msg += "\r\n";

		switch (LogType)
		{
			case LogItemType.LogMessage                          : Debug.Log         (msg               ); break;
			case LogItemType.LogMessage     | LogItemType.Context: Debug.Log         (msg      , Context); break;
			case LogItemType.LogWarning                          : Debug.LogWarning  (msg               ); break;
			case LogItemType.LogWarning     | LogItemType.Context: Debug.LogWarning  (msg      , Context); break;
			case LogItemType.LogError                            : Debug.LogError    (msg               ); break;
			case LogItemType.LogError       | LogItemType.Context: Debug.LogError    (msg      , Context); break;
			case LogItemType.LogException                        : Debug.LogException(Exception         ); break;
			case LogItemType.LogException   | LogItemType.Context: Debug.LogException(Exception, Context); break;
			case LogItemType.ThrowException                      : throw Exception; //[Difference]
			default:
				Debug.LogError(
					  "The DelayedLogItem could not be applied"
					+ " (Message = "+ ToString(Message, true)
					+ ", Exception = " + ToString(Exception)
					+ ", Context = " + ToString(Context)
					+ ", LogType = " + ToString(LogType)
					+ ", StackTrace = " + ToString(StackTrace)
					+ ")"
				);
				break;
		}
	}

	public static DelayedLogItem CreateMessage(object message                                                 ) { return new DelayedLogItem(   LogItemType.LogMessage                          , ToString(message)                             ); }
	public static DelayedLogItem CreateMessage(object message, UnityEngine.Object context                     ) { return new DelayedLogItem(   LogItemType.LogMessage     | LogItemType.Context, ToString(message)                 , context   ); }
	public static DelayedLogItem CreateMessage(string message,                             object[] formatArgs) { return new DelayedLogItem(   LogItemType.LogMessage                          , string.Format(message, formatArgs)            ); }
	public static DelayedLogItem CreateMessage(string message, UnityEngine.Object context, object[] formatArgs) { return new DelayedLogItem(   LogItemType.LogMessage     | LogItemType.Context, string.Format(message, formatArgs), context   ); }
	public static DelayedLogItem CreateWarning(object message                                                 ) { return new DelayedLogItem(   LogItemType.LogWarning                          , ToString(message)                             ); }
	public static DelayedLogItem CreateWarning(object message, UnityEngine.Object context                     ) { return new DelayedLogItem(   LogItemType.LogWarning     | LogItemType.Context, ToString(message)                 , context   ); }
	public static DelayedLogItem CreateWarning(string message,                             object[] formatArgs) { return new DelayedLogItem(   LogItemType.LogWarning                          , string.Format(message, formatArgs)            ); }
	public static DelayedLogItem CreateWarning(string message, UnityEngine.Object context, object[] formatArgs) { return new DelayedLogItem(   LogItemType.LogWarning     | LogItemType.Context, string.Format(message, formatArgs), context   ); }
	public static DelayedLogItem CreateError  (object message                                                 ) { return new DelayedLogItem(   LogItemType.LogError                            , ToString(message)                             ); }
	public static DelayedLogItem CreateError  (object message, UnityEngine.Object context                     ) { return new DelayedLogItem(   LogItemType.LogError       | LogItemType.Context, ToString(message)                 , context   ); }
	public static DelayedLogItem CreateError  (string message,                             object[] formatArgs) { return new DelayedLogItem(   LogItemType.LogError                            , string.Format(message, formatArgs)            ); }
	public static DelayedLogItem CreateError  (string message, UnityEngine.Object context, object[] formatArgs) { return new DelayedLogItem(   LogItemType.LogError       | LogItemType.Context, string.Format(message, formatArgs), context   ); }

	public static DelayedLogItem CreateException      (Exception exception                            ) { return new DelayedLogItem(   LogItemType.LogException                        , exception            ); }
	public static DelayedLogItem CreateException      (Exception exception, UnityEngine.Object context) { return new DelayedLogItem(   LogItemType.LogException   | LogItemType.Context, exception, context   ); }
	public static DelayedLogItem CreateThrownException(Exception exception                            ) { return new DelayedLogItem(   LogItemType.ThrowException                      , exception            ); }

	private static string ToString(object obj, bool withQuotesIfString = false)
	{
		if (withQuotesIfString) {
			if (obj is string) return obj == null ? "null" : ("\"" + obj.ToString() + "\"");
			else return obj == null ? "null" : obj.ToString();
		} else {
			return obj == null ? "null" : obj.ToString();
		}
	}
}