﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Schema;
using UnityEngine;

namespace RevolitionGames.Utilities.Alex
{
	public static class XmlUtils
	{
		private static XNamespace XmlSchemaInstanceNamespace = XNamespace.Get(XmlSchema.InstanceNamespace);

		public static bool IsNil(this XElement element)
		{
			var nilAttr = element.Attributes(XmlSchemaInstanceNamespace + "nil").SingleOrDefault();
			return nilAttr != null && (nilAttr.Value == "true" || nilAttr.Value == "1");
		}

		public static string ValueOrNil(this XElement element)
		{
			if (element.IsNil()) return null;
			else return element.Value;
		}

		public static XAttribute Nil
		{
			get {
				return new XAttribute(XmlSchemaInstanceNamespace + "nil", "true");
			}
		}

		public static void XmlValidationExceptionUnityLogger(object sender, ValidationEventArgs e)
		{
			switch (e.Severity)
			{
				case XmlSeverityType.Error:
					Debug.LogException(e.Exception);
					break;

				case XmlSeverityType.Warning:
					Debug.LogWarning(e.Exception);
					break;

				default:
					Debug.LogException(
						new InvalidOperationException(
							"Xml event of unrecognised severity '" + e.Severity + "' occurred.",
							e.Exception
						)
					);
					break;
			}
		}
	}
}

//*/