﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Utilities.Alex
{
	public class CodeNameRef : MonoBehaviour
	{
		[SerializeField] //And edit in inspector
		private string _codeName;
		public string CodeName { get { return _codeName; } }
	}
}

//*/