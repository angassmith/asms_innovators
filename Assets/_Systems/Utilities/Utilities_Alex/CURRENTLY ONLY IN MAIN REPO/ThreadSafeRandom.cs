﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Utilities.Alex
{
	public static class ThreadSafeRandom
	{
		private static readonly Random SeedGenerator = new Random();

		[ThreadStatic]
		private static Random LocalRandom = null;

		public static Random Random {
			get {
				if (LocalRandom == null)
				{
					int seed;
					lock (SeedGenerator) seed = SeedGenerator.Next();

					LocalRandom = new Random(seed);
				}

				return LocalRandom;
			}
		}
	}
}

//*/