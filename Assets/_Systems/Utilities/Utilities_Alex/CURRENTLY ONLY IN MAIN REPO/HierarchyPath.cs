﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class HierarchyPath {

	public enum SceneMode
	{
		/// <summary>Returns a string of the form: "/" + rootGObj.name + "/" + childGObj.name ...</summary>
		ExcludeScene,
		/// <summary>Returns a string of the form: rootGObj.scene.name + "/" + rootGObj.name + "/" + childGObj.name ...</summary>
		IncludeSceneName,
		/// <summary>Returns a string of the form: "&lt;" + rootGObj.scene.path + "&gt;/" + rootGObj.name + "/" + childGObj.name ... (Note: "&lt;" and "&gt;" are invalid in Windows file paths)</summary>
		IncludeSceneRelativePath,
		/// <summary>Returns a string of the form: rootGObj.scene.name + "&lt;" + rootGObj.scene.path + "&gt;/" + rootGObj.name + "/" + childGObj.name ... (Note: "&lt;" and "&gt;" are invalid in Windows file paths)</summary>
		IncludeSceneNameAndPath,
	}

	/// <summary>
	/// Returns the path to the transform, separated with '/'.
	/// </summary>
	/// <param name="transform">The transform to return the path of</param>
	/// <param name="formatted">If true, the returned path will be surrounded with html tags to create colouring and other formatting when logged in Unity.</param>
	/// <param name="sceneMode">See the members of SceneMode</param>
	/// <returns></returns>
	private static string GetHierarchyPath(this Transform transform, bool formatted = true, SceneMode sceneMode = SceneMode.IncludeSceneNameAndPath)
	{
		if (transform == null) throw new ArgumentNullException("transform");
		
		string path = transform.name;
		Transform curTransform = transform;
		while (curTransform.parent != null)
		{
			curTransform = curTransform.parent;
			path = curTransform.gameObject.name + "/" + path;
		}

		
		switch (sceneMode)
		{
			case SceneMode.ExcludeScene:
				path = "/" + path;
				break;
			case SceneMode.IncludeSceneName:
				path = curTransform.gameObject.scene.name + "/" + path;
				break;
			case SceneMode.IncludeSceneRelativePath:
				path = "<" + curTransform.gameObject.scene.path + ">/" + path;
				break;
			case SceneMode.IncludeSceneNameAndPath:
				Scene rootGObjScene = curTransform.gameObject.scene;
				path = rootGObjScene.name + "<" + rootGObjScene.path + ">/" + path;
				break;
			default:
				throw new ArgumentException("Invalid sceneMode", "sceneMode");
		}
		
		if (formatted) {
			path = "<i><color=blue>" + path + "</color></i>";
		}

		return path;
	}

	/// <summary>Shortcut for component.transform.GetHierarchyPath()</summary>
	public static string GetHierarchyPath(this Component component, bool formatted = true, SceneMode sceneMode = SceneMode.IncludeSceneNameAndPath) {
		if (component == null) throw new ArgumentNullException("component");
		return GetHierarchyPath(component.transform, formatted, sceneMode);
	}

	/// <summary>Shortcut for gameobject.transform.GetHierarchyPath()</summary>
	public static string GetHierarchyPath(this GameObject gameobject, bool formatted = true, SceneMode sceneMode = SceneMode.IncludeSceneNameAndPath) {
		if (gameobject == null) throw new ArgumentNullException("gameobject");
		return GetHierarchyPath(gameobject.transform, formatted, sceneMode);
	}

	public static string GetDebugName(this UnityEngine.Object obj)
	{
		var component = obj as Component;
		if (component != null) {
			return component.GetHierarchyPath();
		} else {
			return obj.name;
		}
	}
}
