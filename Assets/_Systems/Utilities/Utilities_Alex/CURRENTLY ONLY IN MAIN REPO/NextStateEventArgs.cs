﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Utilities.Alex
{
	public class NextStateEventArgs<T> : EventArgs
	{
		public T NextState { get; private set; }

		public NextStateEventArgs(T nextState)
		{
			this.NextState = nextState;
		}
	}
}

//*/