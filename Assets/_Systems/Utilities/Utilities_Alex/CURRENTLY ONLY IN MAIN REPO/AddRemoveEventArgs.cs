﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Utilities.Alex
{
	public class AddRemoveEventArgs<T> : EventArgs
	{
		public T Item { get; private set; }
		
		public AddRemoveEventArgs(T item)
		{
			this.Item = item;
		}
	}
}

//*/