﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Utilities.Alex
{
	public abstract class AutoUnsubClosureBase<TDelegate>
	{
		protected readonly TDelegate Closure;
		protected readonly Action<TDelegate> Unsubscriber;

		public AutoUnsubClosureBase(TDelegate closure, Action<TDelegate> unsubscriber)
		{
			if (closure == null) throw new ArgumentNullException("closure");
			if (unsubscriber == null) throw new ArgumentNullException("unsubscriber");

			this.Closure = closure;
			this.Unsubscriber = unsubscriber;
		}
	}

	public class AutoUnsubClosure<TDelegate, TArg1> : AutoUnsubClosureBase<TDelegate> {
		public AutoUnsubClosure(TDelegate closure, Action<TDelegate> unsubscriber)
			: base(closure, unsubscriber)
		{ }
		public void Invoke(TArg1 arg1) {
			((Delegate)(object)Closure).DynamicInvoke(arg1);
			Unsubscriber((TDelegate)(object)Delegate.CreateDelegate(typeof(TDelegate), this.GetType().GetMethod("Invoke")));
		}
	}

	//-//	public class AutoUnsubClosure<TArg1, TArg2> : AutoUnsubClosureBase<Action<TArg1, TArg2>> {
	//-//		public AutoUnsubClosure(Action<TArg1, TArg2> closure, Action<Action<TArg1, TArg2>> unsubscriber)
	//-//			: base(closure, unsubscriber)
	//-//		{ }
	//-//		public void Invoke(TArg1 arg1, TArg2 arg2) {
	//-//			Closure(arg1, arg2);
	//-//			Unsubscriber(this.Invoke);
	//-//		}
	//-//		public static implicit operator Action<TArg1, TArg2>(AutoUnsubClosure<TArg1, TArg2> x) { return x.Invoke; }
	//-//	}

	public class AutoUnsubClosure<TArg1, TArg2, TArg3> : AutoUnsubClosureBase<Action<TArg1, TArg2, TArg3>> {
		public AutoUnsubClosure(Action<TArg1, TArg2, TArg3> closure, Action<Action<TArg1, TArg2, TArg3>> unsubscriber)
			: base(closure, unsubscriber)
		{ }
		public void Invoke(TArg1 arg1, TArg2 arg2, TArg3 arg3) {
			Closure(arg1, arg2, arg3);
			Unsubscriber(this.Invoke);
		}
		public static implicit operator Action<TArg1, TArg2, TArg3>(AutoUnsubClosure<TArg1, TArg2, TArg3> x) { return x.Invoke; }
	}

	public class AutoUnsubClosure<TArg1, TArg2, TArg3, TArg4> : AutoUnsubClosureBase<Action<TArg1, TArg2, TArg3, TArg4>> {
		public AutoUnsubClosure(Action<TArg1, TArg2, TArg3, TArg4> closure, Action<Action<TArg1, TArg2, TArg3, TArg4>> unsubscriber)
			: base(closure, unsubscriber)
		{ }
		public void Invoke(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4) {
			Closure(arg1, arg2, arg3, arg4);
			Unsubscriber(this.Invoke);
		}
		public static implicit operator Action<TArg1, TArg2, TArg3, TArg4>(AutoUnsubClosure<TArg1, TArg2, TArg3, TArg4> x) { return x.Invoke; }
	}

	//Would add more but .NET 3.5 Actions only go up to 4 arguments

	#region old

	//	//Expanded example: 
	//	//	public class AutoUnsubscribingClosure<TArg1, TArg2> : AutoUnsubClosureBase<Action<TArg1, TArg2>>
	//	//	{
	//	//		public AutoUnsubscribingClosure(Action<TArg1, TArg2> closure, Action<Action<TArg1, TArg2>> unsubscriber)
	//	//			: base(closure, unsubscriber)
	//	//		{ }
	//	//		
	//	//		public void Invoke(TArg1 arg1, TArg2 arg2)
	//	//		{
	//	//			Closure(arg1, arg2);
	//	//			Unsubscriber(this.Invoke);
	//	//		}
	//	//	}
	//	public class AutoUnsubClosure<TArg1                     > : AutoUnsubClosureBase<Action<TArg1                     >> { public AutoUnsubClosure(Action<TArg1                     > closure, Action<Action<TArg1                     >> unsubscriber) : base(closure, unsubscriber) { } public void Invoke(TArg1 arg1                                    ) { Closure(arg1                  ); Unsubscriber(this.Invoke); } }
	//	public class AutoUnsubClosure<TArg1, TArg2              > : AutoUnsubClosureBase<Action<TArg1, TArg2              >> { public AutoUnsubClosure(Action<TArg1, TArg2              > closure, Action<Action<TArg1, TArg2              >> unsubscriber) : base(closure, unsubscriber) { } public void Invoke(TArg1 arg1, TArg2 arg2                        ) { Closure(arg1, arg2            ); Unsubscriber(this.Invoke); } }
	//	public class AutoUnsubClosure<TArg1, TArg2, TArg3       > : AutoUnsubClosureBase<Action<TArg1, TArg2, TArg3       >> { public AutoUnsubClosure(Action<TArg1, TArg2, TArg3       > closure, Action<Action<TArg1, TArg2, TArg3       >> unsubscriber) : base(closure, unsubscriber) { } public void Invoke(TArg1 arg1, TArg2 arg2, TArg3 arg3            ) { Closure(arg1, arg2, arg3      ); Unsubscriber(this.Invoke); } }
	//	public class AutoUnsubClosure<TArg1, TArg2, TArg3, TArg4> : AutoUnsubClosureBase<Action<TArg1, TArg2, TArg3, TArg4>> { public AutoUnsubClosure(Action<TArg1, TArg2, TArg3, TArg4> closure, Action<Action<TArg1, TArg2, TArg3, TArg4>> unsubscriber) : base(closure, unsubscriber) { } public void Invoke(TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4) { Closure(arg1, arg2, arg3, arg4); Unsubscriber(this.Invoke); } }

	#endregion
}

//*/