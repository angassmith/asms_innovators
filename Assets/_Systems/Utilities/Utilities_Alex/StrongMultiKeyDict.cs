﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;


//	public class Base<T>
//	{
//		public class Nested { }
//	}
//	
//	public class Derived1<T1, T2> : Base<T1>
//		where T2 : Base<T1>.Nested
//	{ }
//	
//	public class Derived2<T1> : Derived1<T1, Base<T1>.Nested>
//	{ }


public abstract class StrongMultiKeyDict<TKey1, TValue>
	: StrongMultiKeyDictBase<MultiKeyDictEntry<TKey1, TValue>, MultiKeyDictEntry<TKey1, StrongDictBase<TValue>.ValHolder>, TValue>
{
	private MultiKeyDict<TKey1, ValHolder> dict = new MultiKeyDict<TKey1, ValHolder>();
	private Helper helper;

	public StrongMultiKeyDict() {
		Init();
	}

	//Because InitStrongEntries is called in the base constructor,
	//methods/properties can be called before this class's constructor runs.
	//Methods, properties, and the constructor therefore all call Init().
	private bool hasInited;
	private void Init()
	{
		if (!hasInited) {
			this.helper = new Helper(this);
			hasInited = true;
		}
	}

	public sealed override int Count { get { Init(); return dict.Count; } }

	public sealed override ReadOnlyCollection<TValue> Values { get { Init(); return helper.GetValues(dict.Values); } }

	public ReadOnlyCollection<TKey1> Key1s { get { Init(); return dict.Key1s; } }

	/// <summary>Used internally. Do not use (and especially do not set).</summary>
	protected sealed override IMultiKeyDictBase<MultiKeyDictEntry<TKey1, ValHolder>, ValHolder> InternalDict { get { Init(); return dict; } set { Init(); dict = (MultiKeyDict<TKey1, ValHolder>)value; } }

	protected sealed override MultiKeyDictEntry<TKey1, TValue> ConvertToPublicEntry(MultiKeyDictEntry<TKey1, ValHolder> entry) {
		Init(); return MultiKeyDictEntry.Create(entry.Key1, entry.Value.Get());
	}

	protected void InitStrongEntry(TKey1 key1, Func<TValue> getter, Action<TValue> setter, TValue val) {
		Init();
		dict.Add(key1, helper.HelpInitStrongEntry(getter, setter));
		setter(val);
	}

	public override bool ContainsAnyKeysOfEntry(MultiKeyDictEntry<TKey1, TValue> entry) {
		Init(); return dict.ContainsAnyKeysOfEntry(MultiKeyDictEntry.Create(entry.Key1, (ValHolder)new ValHolder.Runtime(entry.Value)));
	}

	protected void Add(TKey1 key1, TValue value) { Init(); dict.Add(MultiKeyDictEntry.Create(key1, (ValHolder)new ValHolder.Runtime(value))); }

	protected sealed override void Clear() { Init(); helper.Clear(new MultiKeyDict<TKey1, ValHolder>()); }

	public override bool ContainsValue(TValue value) { Init(); return helper.ContainsValue(dict.Values, value); }

	public bool ContainsKey1(TKey1 key) { Init(); return dict.ContainsKey1(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey1, out val); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey1, out val); }

	public TValue this[TKey1 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }

	public TValue GetValueByKey1(TKey1 key) { Init(); return dict[key].Get(); }

	protected bool RemoveByKey1(TKey1 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey1, dict.RemoveByKey1); }
}


public abstract class StrongMultiKeyDict<TKey1, TKey2, TValue>
	: StrongMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TValue>, MultiKeyDictEntry<TKey1, TKey2, StrongDictBase<TValue>.ValHolder>, TValue>
{
	private MultiKeyDict<TKey1, TKey2, ValHolder> dict = new MultiKeyDict<TKey1, TKey2, ValHolder>();
	private Helper helper;

	public StrongMultiKeyDict() {
		Init();
	}

	//Because InitStrongEntries is called in the base constructor,
	//methods/properties can be called before this class's constructor runs.
	//Methods, properties, and the constructor therefore all call Init().
	private bool hasInited;
	private void Init()
	{
		if (!hasInited) {
			this.helper = new Helper(this);
			hasInited = true;
		}
	}

	public sealed override int Count { get { Init(); return dict.Count; } }

	public sealed override ReadOnlyCollection<TValue> Values { get { Init(); return helper.GetValues(dict.Values); } }

	public ReadOnlyCollection<TKey1> Key1s { get { Init(); return dict.Key1s; } }
	public ReadOnlyCollection<TKey2> Key2s { get { Init(); return dict.Key2s; } }

	/// <summary>Used internally. Do not use (and especially do not set).</summary>
	protected sealed override IMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, ValHolder>, ValHolder> InternalDict { get { Init(); return dict; } set { Init(); dict = (MultiKeyDict<TKey1, TKey2, ValHolder>)value; } }

	protected sealed override MultiKeyDictEntry<TKey1, TKey2, TValue> ConvertToPublicEntry(MultiKeyDictEntry<TKey1, TKey2, ValHolder> entry) {
		Init(); return MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Value.Get());
	}

	protected void InitStrongEntry(TKey1 key1, TKey2 key2, Func<TValue> getter, Action<TValue> setter, TValue val) {
		Init();
		dict.Add(key1, key2, helper.HelpInitStrongEntry(getter, setter));
		setter(val);
	}

	public override bool ContainsAnyKeysOfEntry(MultiKeyDictEntry<TKey1, TKey2, TValue> entry) {
		Init(); return dict.ContainsAnyKeysOfEntry(MultiKeyDictEntry.Create(entry.Key1, entry.Key2, (ValHolder)new ValHolder.Runtime(entry.Value)));
	}

	protected void Add(TKey1 key1, TKey2 key2, TValue value) { Init(); dict.Add(MultiKeyDictEntry.Create(key1, key2, (ValHolder)new ValHolder.Runtime(value))); }

	protected sealed override void Clear() { Init(); helper.Clear(new MultiKeyDict<TKey1, TKey2, ValHolder>()); }

	public override bool ContainsValue(TValue value) { Init(); return helper.ContainsValue(dict.Values, value); }

	public bool ContainsKey1(TKey1 key) { Init(); return dict.ContainsKey1(key); }
	public bool ContainsKey2(TKey2 key) { Init(); return dict.ContainsKey2(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey1, out val); }
	public bool TryGetValueByKey2(TKey2 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey2, out val); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey1, out val); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey2, out val); }

	public TValue this[TKey1 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey2 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }

	public TValue GetValueByKey1(TKey1 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey2(TKey2 key) { Init(); return dict[key].Get(); }

	protected bool RemoveByKey1(TKey1 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey1, dict.RemoveByKey1); }
	protected bool RemoveByKey2(TKey2 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey2, dict.RemoveByKey2); }
}


public abstract class StrongMultiKeyDict<TKey1, TKey2, TKey3, TValue>
	: StrongMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue>, MultiKeyDictEntry<TKey1, TKey2, TKey3, StrongDictBase<TValue>.ValHolder>, TValue>
{
	private MultiKeyDict<TKey1, TKey2, TKey3, ValHolder> dict = new MultiKeyDict<TKey1, TKey2, TKey3, ValHolder>();
	private Helper helper;

	public StrongMultiKeyDict() {
		Init();
	}

	//Because InitStrongEntries is called in the base constructor,
	//methods/properties can be called before this class's constructor runs.
	//Methods, properties, and the constructor therefore all call Init().
	private bool hasInited;
	private void Init()
	{
		if (!hasInited) {
			this.helper = new Helper(this);
			hasInited = true;
		}
	}

	public sealed override int Count { get { Init(); return dict.Count; } }

	public sealed override ReadOnlyCollection<TValue> Values { get { Init(); return helper.GetValues(dict.Values); } }

	public ReadOnlyCollection<TKey1> Key1s { get { Init(); return dict.Key1s; } }
	public ReadOnlyCollection<TKey2> Key2s { get { Init(); return dict.Key2s; } }
	public ReadOnlyCollection<TKey3> Key3s { get { Init(); return dict.Key3s; } }

	/// <summary>Used internally. Do not use (and especially do not set).</summary>
	protected sealed override IMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, ValHolder>, ValHolder> InternalDict { get { Init(); return dict; } set { Init(); dict = (MultiKeyDict<TKey1, TKey2, TKey3, ValHolder>)value; } }

	protected sealed override MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> ConvertToPublicEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, ValHolder> entry) {
		Init(); return MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Value.Get());
	}

	protected void InitStrongEntry(TKey1 key1, TKey2 key2, TKey3 key3, Func<TValue> getter, Action<TValue> setter, TValue val) {
		Init();
		dict.Add(key1, key2, key3, helper.HelpInitStrongEntry(getter, setter));
		setter(val);
	}

	public override bool ContainsAnyKeysOfEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> entry) {
		Init(); return dict.ContainsAnyKeysOfEntry(MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, (ValHolder)new ValHolder.Runtime(entry.Value)));
	}

	protected void Add(TKey1 key1, TKey2 key2, TKey3 key3, TValue value) { Init(); dict.Add(MultiKeyDictEntry.Create(key1, key2, key3, (ValHolder)new ValHolder.Runtime(value))); }

	protected sealed override void Clear() { Init(); helper.Clear(new MultiKeyDict<TKey1, TKey2, TKey3, ValHolder>()); }

	public override bool ContainsValue(TValue value) { Init(); return helper.ContainsValue(dict.Values, value); }

	public bool ContainsKey1(TKey1 key) { Init(); return dict.ContainsKey1(key); }
	public bool ContainsKey2(TKey2 key) { Init(); return dict.ContainsKey2(key); }
	public bool ContainsKey3(TKey3 key) { Init(); return dict.ContainsKey3(key); }
	
	public bool TryGetValueByKey1(TKey1 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey1, out val); }
	public bool TryGetValueByKey2(TKey2 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey2, out val); }
	public bool TryGetValueByKey3(TKey3 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey3, out val); }
	
	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey1, out val); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey2, out val); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey3, out val); }
	
	public TValue this[TKey1 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey2 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey3 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	
	public TValue GetValueByKey1(TKey1 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey2(TKey2 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey3(TKey3 key) { Init(); return dict[key].Get(); }

	protected bool RemoveByKey1(TKey1 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey1, dict.RemoveByKey1); }
	protected bool RemoveByKey2(TKey2 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey2, dict.RemoveByKey2); }
	protected bool RemoveByKey3(TKey3 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey3, dict.RemoveByKey3); }
}


public abstract class StrongMultiKeyDict<TKey1, TKey2, TKey3, TKey4, TValue>
	: StrongMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue>, MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, StrongDictBase<TValue>.ValHolder>, TValue>
{
	private MultiKeyDict<TKey1, TKey2, TKey3, TKey4, ValHolder> dict = new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, ValHolder>();
	private Helper helper;

	public StrongMultiKeyDict() {
		Init();
	}

	//Because InitStrongEntries is called in the base constructor,
	//methods/properties can be called before this class's constructor runs.
	//Methods, properties, and the constructor therefore all call Init().
	private bool hasInited;
	private void Init()
	{
		if (!hasInited) {
			this.helper = new Helper(this);
			hasInited = true;
		}
	}

	public sealed override int Count { get { Init(); return dict.Count; } }

	public sealed override ReadOnlyCollection<TValue> Values { get { Init(); return helper.GetValues(dict.Values); } }

	public ReadOnlyCollection<TKey1> Key1s { get { Init(); return dict.Key1s; } }
	public ReadOnlyCollection<TKey2> Key2s { get { Init(); return dict.Key2s; } }
	public ReadOnlyCollection<TKey3> Key3s { get { Init(); return dict.Key3s; } }
	public ReadOnlyCollection<TKey4> Key4s { get { Init(); return dict.Key4s; } }

	/// <summary>Used internally. Do not use (and especially do not set).</summary>
	protected sealed override IMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, ValHolder>, ValHolder> InternalDict { get { Init(); return dict; } set { Init(); dict = (MultiKeyDict<TKey1, TKey2, TKey3, TKey4, ValHolder>)value; } }

	protected sealed override MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> ConvertToPublicEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, ValHolder> entry) {
		Init(); return MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Value.Get());
	}

	protected void InitStrongEntry(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, Func<TValue> getter, Action<TValue> setter, TValue val) {
		Init();
		dict.Add(key1, key2, key3, key4, helper.HelpInitStrongEntry(getter, setter));
		setter(val);
	}

	public override bool ContainsAnyKeysOfEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> entry) {
		Init();  return dict.ContainsAnyKeysOfEntry(MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, (ValHolder)new ValHolder.Runtime(entry.Value)));
	}

	protected void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TValue value) { Init(); dict.Add(MultiKeyDictEntry.Create(key1, key2, key3, key4, (ValHolder)new ValHolder.Runtime(value))); }

	protected sealed override void Clear() { Init(); helper.Clear(new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, ValHolder>()); }

	public override bool ContainsValue(TValue value) { Init(); return helper.ContainsValue(dict.Values, value); }

	public bool ContainsKey1(TKey1 key) { Init(); return dict.ContainsKey1(key); }
	public bool ContainsKey2(TKey2 key) { Init(); return dict.ContainsKey2(key); }
	public bool ContainsKey3(TKey3 key) { Init(); return dict.ContainsKey3(key); }
	public bool ContainsKey4(TKey4 key) { Init(); return dict.ContainsKey4(key); }
	
	public bool TryGetValueByKey1(TKey1 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey1, out val); }
	public bool TryGetValueByKey2(TKey2 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey2, out val); }
	public bool TryGetValueByKey3(TKey3 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey3, out val); }
	public bool TryGetValueByKey4(TKey4 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey4, out val); }
	
	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey1, out val); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey2, out val); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey3, out val); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey4, out val); }
	
	public TValue this[TKey1 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey2 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey3 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey4 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	
	public TValue GetValueByKey1(TKey1 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey2(TKey2 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey3(TKey3 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey4(TKey4 key) { Init(); return dict[key].Get(); }

	protected bool RemoveByKey1(TKey1 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey1, dict.RemoveByKey1); }
	protected bool RemoveByKey2(TKey2 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey2, dict.RemoveByKey2); }
	protected bool RemoveByKey3(TKey3 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey3, dict.RemoveByKey3); }
	protected bool RemoveByKey4(TKey4 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey4, dict.RemoveByKey4); }
}


public abstract class StrongMultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TValue>
	: StrongMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue>, MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, StrongDictBase<TValue>.ValHolder>, TValue>
{
	private MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, ValHolder> dict = new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, ValHolder>();
	private Helper helper;

	public StrongMultiKeyDict() {
		Init();
	}

	//Because InitStrongEntries is called in the base constructor,
	//methods/properties can be called before this class's constructor runs.
	//Methods, properties, and the constructor therefore all call Init().
	private bool hasInited;
	private void Init()
	{
		if (!hasInited) {
			this.helper = new Helper(this);
			hasInited = true;
		}
	}

	public sealed override int Count { get { Init(); return dict.Count; } }

	public sealed override ReadOnlyCollection<TValue> Values { get { Init(); return helper.GetValues(dict.Values); } }

	public ReadOnlyCollection<TKey1> Key1s { get { Init(); return dict.Key1s; } }
	public ReadOnlyCollection<TKey2> Key2s { get { Init(); return dict.Key2s; } }
	public ReadOnlyCollection<TKey3> Key3s { get { Init(); return dict.Key3s; } }
	public ReadOnlyCollection<TKey4> Key4s { get { Init(); return dict.Key4s; } }
	public ReadOnlyCollection<TKey5> Key5s { get { Init(); return dict.Key5s; } }

	/// <summary>Used internally. Do not use (and especially do not set).</summary>
	protected sealed override IMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, ValHolder>, ValHolder> InternalDict { get { Init(); return dict; } set { Init(); dict = (MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, ValHolder>)value; } }

	protected sealed override MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> ConvertToPublicEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, ValHolder> entry) {
		Init(); return MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Key5, entry.Value.Get());
	}

	protected void InitStrongEntry(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, Func<TValue> getter, Action<TValue> setter, TValue val) {
		Init();
		dict.Add(key1, key2, key3, key4, key5, helper.HelpInitStrongEntry(getter, setter));
		setter(val);
	}

	public override bool ContainsAnyKeysOfEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> entry) {
		Init(); return dict.ContainsAnyKeysOfEntry(MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Key5, (ValHolder)new ValHolder.Runtime(entry.Value)));
	}

	protected void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TValue value) { Init(); dict.Add(MultiKeyDictEntry.Create(key1, key2, key3, key4, key5, (ValHolder)new ValHolder.Runtime(value))); }

	protected sealed override void Clear() { Init(); helper.Clear(new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, ValHolder>()); }

	public override bool ContainsValue(TValue value) { Init(); return helper.ContainsValue(dict.Values, value); }

	public bool ContainsKey1(TKey1 key) { Init(); return dict.ContainsKey1(key); }
	public bool ContainsKey2(TKey2 key) { Init(); return dict.ContainsKey2(key); }
	public bool ContainsKey3(TKey3 key) { Init(); return dict.ContainsKey3(key); }
	public bool ContainsKey4(TKey4 key) { Init(); return dict.ContainsKey4(key); }
	public bool ContainsKey5(TKey5 key) { Init(); return dict.ContainsKey5(key); }
	
	public bool TryGetValueByKey1(TKey1 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey1, out val); }
	public bool TryGetValueByKey2(TKey2 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey2, out val); }
	public bool TryGetValueByKey3(TKey3 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey3, out val); }
	public bool TryGetValueByKey4(TKey4 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey4, out val); }
	public bool TryGetValueByKey5(TKey5 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey5, out val); }
	
	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey1, out val); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey2, out val); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey3, out val); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey4, out val); }
	public bool TryGetEntryByKey5(TKey5 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey5, out val); }
	
	public TValue this[TKey1 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey2 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey3 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey4 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey5 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	
	public TValue GetValueByKey1(TKey1 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey2(TKey2 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey3(TKey3 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey4(TKey4 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey5(TKey5 key) { Init(); return dict[key].Get(); }

	protected bool RemoveByKey1(TKey1 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey1, dict.RemoveByKey1); }
	protected bool RemoveByKey2(TKey2 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey2, dict.RemoveByKey2); }
	protected bool RemoveByKey3(TKey3 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey3, dict.RemoveByKey3); }
	protected bool RemoveByKey4(TKey4 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey4, dict.RemoveByKey4); }
	protected bool RemoveByKey5(TKey5 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey5, dict.RemoveByKey5); }
}


public abstract class StrongMultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue>
	: StrongMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue>, MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, StrongDictBase<TValue>.ValHolder>, TValue>
{
	private MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, ValHolder> dict = new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, ValHolder>();
	private Helper helper;

	public StrongMultiKeyDict() {
		Init();
	}

	//Because InitStrongEntries is called in the base constructor,
	//methods/properties can be called before this class's constructor runs.
	//Methods, properties, and the constructor therefore all call Init().
	private bool hasInited;
	private void Init()
	{
		if (!hasInited) {
			this.helper = new Helper(this);
			hasInited = true;
		}
	}

	public sealed override int Count { get { Init(); return dict.Count; } }

	public sealed override ReadOnlyCollection<TValue> Values { get { Init(); return helper.GetValues(dict.Values); } }

	public ReadOnlyCollection<TKey1> Key1s { get { Init(); return dict.Key1s; } }
	public ReadOnlyCollection<TKey2> Key2s { get { Init(); return dict.Key2s; } }
	public ReadOnlyCollection<TKey3> Key3s { get { Init(); return dict.Key3s; } }
	public ReadOnlyCollection<TKey4> Key4s { get { Init(); return dict.Key4s; } }
	public ReadOnlyCollection<TKey5> Key5s { get { Init(); return dict.Key5s; } }
	public ReadOnlyCollection<TKey6> Key6s { get { Init(); return dict.Key6s; } }
	
	/// <summary>Used internally. Do not use (and especially do not set).</summary>
	protected sealed override IMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, ValHolder>, ValHolder> InternalDict { get { Init(); return dict; } set { Init(); dict = (MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, ValHolder>)value; } }

	protected sealed override MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> ConvertToPublicEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, ValHolder> entry) {
		Init(); return MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Key5, entry.Key6, entry.Value.Get());
	}

	protected void InitStrongEntry(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, Func<TValue> getter, Action<TValue> setter, TValue val) {
		Init();
		dict.Add(key1, key2, key3, key4, key5, key6, helper.HelpInitStrongEntry(getter, setter));
		setter(val);
	}

	public override bool ContainsAnyKeysOfEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> entry) {
		Init(); return dict.ContainsAnyKeysOfEntry(MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Key5, entry.Key6, (ValHolder)new ValHolder.Runtime(entry.Value)));
	}

	protected void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TValue value) { Init(); dict.Add(MultiKeyDictEntry.Create(key1, key2, key3, key4, key5, key6, (ValHolder)new ValHolder.Runtime(value))); }

	protected sealed override void Clear() { Init(); helper.Clear(new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, ValHolder>()); }

	public override bool ContainsValue(TValue value) { Init(); return helper.ContainsValue(dict.Values, value); }

	public bool ContainsKey1(TKey1 key) { Init(); return dict.ContainsKey1(key); }
	public bool ContainsKey2(TKey2 key) { Init(); return dict.ContainsKey2(key); }
	public bool ContainsKey3(TKey3 key) { Init(); return dict.ContainsKey3(key); }
	public bool ContainsKey4(TKey4 key) { Init(); return dict.ContainsKey4(key); }
	public bool ContainsKey5(TKey5 key) { Init(); return dict.ContainsKey5(key); }
	public bool ContainsKey6(TKey6 key) { Init(); return dict.ContainsKey6(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey1, out val); }
	public bool TryGetValueByKey2(TKey2 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey2, out val); }
	public bool TryGetValueByKey3(TKey3 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey3, out val); }
	public bool TryGetValueByKey4(TKey4 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey4, out val); }
	public bool TryGetValueByKey5(TKey5 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey5, out val); }
	public bool TryGetValueByKey6(TKey6 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey6, out val); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey1, out val); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey2, out val); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey3, out val); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey4, out val); }
	public bool TryGetEntryByKey5(TKey5 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey5, out val); }
	public bool TryGetEntryByKey6(TKey6 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey6, out val); }

	public TValue this[TKey1 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey2 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey3 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey4 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey5 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey6 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }

	public TValue GetValueByKey1(TKey1 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey2(TKey2 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey3(TKey3 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey4(TKey4 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey5(TKey5 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey6(TKey6 key) { Init(); return dict[key].Get(); }

	protected bool RemoveByKey1(TKey1 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey1, dict.RemoveByKey1); }
	protected bool RemoveByKey2(TKey2 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey2, dict.RemoveByKey2); }
	protected bool RemoveByKey3(TKey3 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey3, dict.RemoveByKey3); }
	protected bool RemoveByKey4(TKey4 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey4, dict.RemoveByKey4); }
	protected bool RemoveByKey5(TKey5 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey5, dict.RemoveByKey5); }
	protected bool RemoveByKey6(TKey6 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey6, dict.RemoveByKey6); }
}


public abstract class StrongMultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue>
	: StrongMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue>, MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, StrongDictBase<TValue>.ValHolder>, TValue>
{
	private MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, ValHolder> dict = new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, ValHolder>();
	private Helper helper;

	public StrongMultiKeyDict() {
		Init();
	}

	//Because InitStrongEntries is called in the base constructor,
	//methods/properties can be called before this class's constructor runs.
	//Methods, properties, and the constructor therefore all call Init().
	private bool hasInited;
	private void Init()
	{
		if (!hasInited) {
			this.helper = new Helper(this);
			hasInited = true;
		}
	}

	public sealed override int Count { get { Init(); return dict.Count; } }

	public sealed override ReadOnlyCollection<TValue> Values { get { Init(); return helper.GetValues(dict.Values); } }

	public ReadOnlyCollection<TKey1> Key1s { get { Init(); return dict.Key1s; } }
	public ReadOnlyCollection<TKey2> Key2s { get { Init(); return dict.Key2s; } }
	public ReadOnlyCollection<TKey3> Key3s { get { Init(); return dict.Key3s; } }
	public ReadOnlyCollection<TKey4> Key4s { get { Init(); return dict.Key4s; } }
	public ReadOnlyCollection<TKey5> Key5s { get { Init(); return dict.Key5s; } }
	public ReadOnlyCollection<TKey6> Key6s { get { Init(); return dict.Key6s; } }
	public ReadOnlyCollection<TKey7> Key7s { get { Init(); return dict.Key7s; } }
	
	/// <summary>Used internally. Do not use (and especially do not set).</summary>
	protected sealed override IMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, ValHolder>, ValHolder> InternalDict { get { Init(); return dict; } set { Init(); dict = (MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, ValHolder>)value; } }

	protected sealed override MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> ConvertToPublicEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, ValHolder> entry) {
		Init(); return MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Key5, entry.Key6, entry.Key7, entry.Value.Get());
	}

	protected void InitStrongEntry(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, Func<TValue> getter, Action<TValue> setter, TValue val) {
		Init();
		dict.Add(key1, key2, key3, key4, key5, key6, key7, helper.HelpInitStrongEntry(getter, setter));
		setter(val);
	}

	public override bool ContainsAnyKeysOfEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> entry) {
		Init(); return dict.ContainsAnyKeysOfEntry(MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Key5, entry.Key6, entry.Key7, (ValHolder)new ValHolder.Runtime(entry.Value)));
	}

	protected void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TValue value) { Init(); dict.Add(MultiKeyDictEntry.Create(key1, key2, key3, key4, key5, key6, key7, (ValHolder)new ValHolder.Runtime(value))); }

	protected sealed override void Clear() { Init(); helper.Clear(new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, ValHolder>()); }

	public override bool ContainsValue(TValue value) { Init(); return helper.ContainsValue(dict.Values, value); }

	public bool ContainsKey1(TKey1 key) { Init(); return dict.ContainsKey1(key); }
	public bool ContainsKey2(TKey2 key) { Init(); return dict.ContainsKey2(key); }
	public bool ContainsKey3(TKey3 key) { Init(); return dict.ContainsKey3(key); }
	public bool ContainsKey4(TKey4 key) { Init(); return dict.ContainsKey4(key); }
	public bool ContainsKey5(TKey5 key) { Init(); return dict.ContainsKey5(key); }
	public bool ContainsKey6(TKey6 key) { Init(); return dict.ContainsKey6(key); }
	public bool ContainsKey7(TKey7 key) { Init(); return dict.ContainsKey7(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey1, out val); }
	public bool TryGetValueByKey2(TKey2 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey2, out val); }
	public bool TryGetValueByKey3(TKey3 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey3, out val); }
	public bool TryGetValueByKey4(TKey4 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey4, out val); }
	public bool TryGetValueByKey5(TKey5 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey5, out val); }
	public bool TryGetValueByKey6(TKey6 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey6, out val); }
	public bool TryGetValueByKey7(TKey7 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey7, out val); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey1, out val); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey2, out val); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey3, out val); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey4, out val); }
	public bool TryGetEntryByKey5(TKey5 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey5, out val); }
	public bool TryGetEntryByKey6(TKey6 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey6, out val); }
	public bool TryGetEntryByKey7(TKey7 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey7, out val); }

	public TValue this[TKey1 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey2 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey3 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey4 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey5 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey6 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey7 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }

	public TValue GetValueByKey1(TKey1 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey2(TKey2 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey3(TKey3 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey4(TKey4 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey5(TKey5 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey6(TKey6 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey7(TKey7 key) { Init(); return dict[key].Get(); }

	protected bool RemoveByKey1(TKey1 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey1, dict.RemoveByKey1); }
	protected bool RemoveByKey2(TKey2 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey2, dict.RemoveByKey2); }
	protected bool RemoveByKey3(TKey3 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey3, dict.RemoveByKey3); }
	protected bool RemoveByKey4(TKey4 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey4, dict.RemoveByKey4); }
	protected bool RemoveByKey5(TKey5 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey5, dict.RemoveByKey5); }
	protected bool RemoveByKey6(TKey6 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey6, dict.RemoveByKey6); }
	protected bool RemoveByKey7(TKey7 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey7, dict.RemoveByKey7); }
}


public abstract class StrongMultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>
	: StrongMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue>, MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, StrongDictBase<TValue>.ValHolder>, TValue>
{
	private MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, ValHolder> dict = new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, ValHolder>();
	private Helper helper;

	public StrongMultiKeyDict() {
		Init();
	}

	//Because InitStrongEntries is called in the base constructor,
	//methods/properties can be called before this class's constructor runs.
	//Methods, properties, and the constructor therefore all call Init().
	private bool hasInited;
	private void Init()
	{
		if (!hasInited) {
			this.helper = new Helper(this);
			hasInited = true;
		}
	}

	public sealed override int Count { get { Init(); return dict.Count; } }

	public sealed override ReadOnlyCollection<TValue> Values { get { Init(); return helper.GetValues(dict.Values); } }

	public ReadOnlyCollection<TKey1> Key1s { get { Init(); return dict.Key1s; } }
	public ReadOnlyCollection<TKey2> Key2s { get { Init(); return dict.Key2s; } }
	public ReadOnlyCollection<TKey3> Key3s { get { Init(); return dict.Key3s; } }
	public ReadOnlyCollection<TKey4> Key4s { get { Init(); return dict.Key4s; } }
	public ReadOnlyCollection<TKey5> Key5s { get { Init(); return dict.Key5s; } }
	public ReadOnlyCollection<TKey6> Key6s { get { Init(); return dict.Key6s; } }
	public ReadOnlyCollection<TKey7> Key7s { get { Init(); return dict.Key7s; } }
	public ReadOnlyCollection<TKey8> Key8s { get { Init(); return dict.Key8s; } }

	/// <summary>Used internally. Do not use (and especially do not set).</summary>
	protected sealed override IMultiKeyDictBase<MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, ValHolder>, ValHolder> InternalDict { get { Init(); return dict; } set { Init(); dict = (MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, ValHolder>)value; } }

	protected sealed override MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> ConvertToPublicEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, ValHolder> entry) {
		Init(); return MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Key5, entry.Key6, entry.Key7, entry.Key8, entry.Value.Get());
	}

	protected void InitStrongEntry(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TKey8 key8, Func<TValue> getter, Action<TValue> setter, TValue val) {
		Init();
		dict.Add(key1, key2, key3, key4, key5, key6, key7, key8, helper.HelpInitStrongEntry(getter, setter));
		setter(val);
	}

	public override bool ContainsAnyKeysOfEntry(MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> entry) {
		Init(); return dict.ContainsAnyKeysOfEntry(MultiKeyDictEntry.Create(entry.Key1, entry.Key2, entry.Key3, entry.Key4, entry.Key5, entry.Key6, entry.Key7, entry.Key8, (ValHolder)new ValHolder.Runtime(entry.Value)));
	}

	protected void Add(TKey1 key1, TKey2 key2, TKey3 key3, TKey4 key4, TKey5 key5, TKey6 key6, TKey7 key7, TKey8 key8, TValue value) { Init(); dict.Add(MultiKeyDictEntry.Create(key1, key2, key3, key4, key5, key6, key7, key8, (ValHolder)new ValHolder.Runtime(value))); }

	protected sealed override void Clear() { Init(); helper.Clear(new MultiKeyDict<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, ValHolder>()); }

	public override bool ContainsValue(TValue value) { Init(); return helper.ContainsValue(dict.Values, value); }

	public bool ContainsKey1(TKey1 key) { Init(); return dict.ContainsKey1(key); }
	public bool ContainsKey2(TKey2 key) { Init(); return dict.ContainsKey2(key); }
	public bool ContainsKey3(TKey3 key) { Init(); return dict.ContainsKey3(key); }
	public bool ContainsKey4(TKey4 key) { Init(); return dict.ContainsKey4(key); }
	public bool ContainsKey5(TKey5 key) { Init(); return dict.ContainsKey5(key); }
	public bool ContainsKey6(TKey6 key) { Init(); return dict.ContainsKey6(key); }
	public bool ContainsKey7(TKey7 key) { Init(); return dict.ContainsKey7(key); }
	public bool ContainsKey8(TKey8 key) { Init(); return dict.ContainsKey8(key); }

	public bool TryGetValueByKey1(TKey1 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey1, out val); }
	public bool TryGetValueByKey2(TKey2 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey2, out val); }
	public bool TryGetValueByKey3(TKey3 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey3, out val); }
	public bool TryGetValueByKey4(TKey4 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey4, out val); }
	public bool TryGetValueByKey5(TKey5 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey5, out val); }
	public bool TryGetValueByKey6(TKey6 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey6, out val); }
	public bool TryGetValueByKey7(TKey7 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey7, out val); }
	public bool TryGetValueByKey8(TKey8 key, out TValue val) { Init(); return helper.TryGetValue(key, dict.TryGetValueByKey8, out val); }

	public bool TryGetEntryByKey1(TKey1 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey1, out val); }
	public bool TryGetEntryByKey2(TKey2 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey2, out val); }
	public bool TryGetEntryByKey3(TKey3 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey3, out val); }
	public bool TryGetEntryByKey4(TKey4 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey4, out val); }
	public bool TryGetEntryByKey5(TKey5 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey5, out val); }
	public bool TryGetEntryByKey6(TKey6 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey6, out val); }
	public bool TryGetEntryByKey7(TKey7 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey7, out val); }
	public bool TryGetEntryByKey8(TKey8 key, out MultiKeyDictEntry<TKey1, TKey2, TKey3, TKey4, TKey5, TKey6, TKey7, TKey8, TValue> val) { Init(); return helper.TryGetEntry(key, dict.TryGetEntryByKey8, out val); }

	public TValue this[TKey1 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey2 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey3 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey4 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey5 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey6 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey7 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }
	public TValue this[TKey8 key] { get { Init(); return dict[key].Get(); } protected set { Init(); dict[key].Set(value); } }

	public TValue GetValueByKey1(TKey1 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey2(TKey2 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey3(TKey3 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey4(TKey4 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey5(TKey5 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey6(TKey6 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey7(TKey7 key) { Init(); return dict[key].Get(); }
	public TValue GetValueByKey8(TKey8 key) { Init(); return dict[key].Get(); }

	protected bool RemoveByKey1(TKey1 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey1, dict.RemoveByKey1); }
	protected bool RemoveByKey2(TKey2 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey2, dict.RemoveByKey2); }
	protected bool RemoveByKey3(TKey3 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey3, dict.RemoveByKey3); }
	protected bool RemoveByKey4(TKey4 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey4, dict.RemoveByKey4); }
	protected bool RemoveByKey5(TKey5 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey5, dict.RemoveByKey5); }
	protected bool RemoveByKey6(TKey6 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey6, dict.RemoveByKey6); }
	protected bool RemoveByKey7(TKey7 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey7, dict.RemoveByKey7); }
	protected bool RemoveByKey8(TKey8 key) { Init(); return helper.RemoveRuntimeEntry(key, dict.TryGetValueByKey8, dict.RemoveByKey8); }
}

//*/