﻿/*

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour {

    public float Delay = 3f;

	// Use this for initialization
	void Start () {
        StartCoroutine(LoadLevelAfterDelay(Delay));
	}

    private IEnumerator LoadLevelAfterDelay(float secs) {
        yield return new WaitForSeconds(secs);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}

//*/