﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using UObject = UnityEngine.Object;

namespace RevolitionGames
{
	public static class EditorGUIUtils
	{
		public static T ObjectField<T>(T obj, bool allowSceneObjects = true, params GUILayoutOption[] options)
			where T : UObject
		{
			return (T)EditorGUILayout.ObjectField(obj, typeof(T), allowSceneObjects, options);
		}
		public static T ObjectField<T>(GUIContent label, T obj, bool allowSceneObjects = true, params GUILayoutOption[] options)
			where T : UObject
		{
			return (T)EditorGUILayout.ObjectField(label, obj, typeof(T), allowSceneObjects, options);
		}
		public static T ObjectField<T>(string label, T obj, bool allowSceneObjects = true, params GUILayoutOption[] options)
			where T : UObject
		{
			return (T)EditorGUILayout.ObjectField(label, obj, typeof(T), allowSceneObjects, options);
		}


		public static void ObjectField<T>(ref T obj, bool allowSceneObjects = true, params GUILayoutOption[] options)
			where T : UObject
		{
			obj = (T)EditorGUILayout.ObjectField(obj, typeof(T), allowSceneObjects, options);
		}
		public static void ObjectField<T>(GUIContent label, ref T obj, bool allowSceneObjects = true, params GUILayoutOption[] options)
			where T : UObject
		{
			obj = (T)EditorGUILayout.ObjectField(label, obj, typeof(T), allowSceneObjects, options);
		}
		public static void ObjectField<T>(string label, ref T obj, bool allowSceneObjects = true, params GUILayoutOption[] options)
			where T : UObject
		{
			obj = (T)EditorGUILayout.ObjectField(label, obj, typeof(T), allowSceneObjects, options);
		}


		public static void SetForAllStates(this GUIStyle style, GUIStyleState state)
		{
			style.active = state;
			style.focused = state;
			style.hover = state;
			style.normal = state;
			style.onActive = state;
			style.onFocused = state;
			style.onHover = state;
			style.onNormal = state;
		}

		public static void SetForAllStates(this GUIStyle style, Color textColor)
		{
			style.active.textColor = textColor;
			style.focused.textColor = textColor;
			style.hover.textColor = textColor;
			style.normal.textColor = textColor;
			style.onActive.textColor = textColor;
			style.onFocused.textColor = textColor;
			style.onHover.textColor = textColor;
			style.onNormal.textColor = textColor;
		}

		public static void SetForAllStates(this GUIStyle style, Texture2D background)
		{
			style.active.background = background;
			style.focused.background = background;
			style.hover.background = background;
			style.normal.background = background;
			style.onActive.background = background;
			style.onFocused.background = background;
			style.onHover.background = background;
			style.onNormal.background = background;
		}

		public static void SetForAllStates(this GUIStyle style, Texture2D[] scaledBackgrounds)
		{
			style.active.scaledBackgrounds = scaledBackgrounds;
			style.focused.scaledBackgrounds = scaledBackgrounds;
			style.hover.scaledBackgrounds = scaledBackgrounds;
			style.normal.scaledBackgrounds = scaledBackgrounds;
			style.onActive.scaledBackgrounds = scaledBackgrounds;
			style.onFocused.scaledBackgrounds = scaledBackgrounds;
			style.onHover.scaledBackgrounds = scaledBackgrounds;
			style.onNormal.scaledBackgrounds = scaledBackgrounds;
		}

	}
}

#endif

//*/