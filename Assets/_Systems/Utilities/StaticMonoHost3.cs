﻿/*

using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Linq;
using System;

public class StaticMonoHost3 : MonoBehaviour {

	public StaticMonoHost3()
	{
		Assembly assembly = typeof(StaticMonoHost3).Assembly;
		var SMTypes = from type in assembly.GetTypes()
		              where Attribute.IsDefined(type, typeof(StaticMonoAttribute))
		              select type;
		foreach (Type SMType in SMTypes)
		{
			System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(SMType.TypeHandle);
		}
	}

	//Only relevant mono methods
	public static event EventHandler                                SMAwake;
	public static event EventHandler                                SMFixedUpdate;
	public static event EventHandler                                SMLateUpdate;
	public static event EventHandler                                SMOnApplicationFocus;
	public static event EventHandler                                SMOnApplicationPause;
	public static event EventHandler                                SMOnApplicationQuit;
	public static event EventHandler                                SMOnConnectedToServer;
	public static event EventHandler                                SMOnDisconnectedFromServer;
	public static event EventHandler                                SMOnFailedToConnect;
	public static event EventHandler                                SMOnFailedToConnectToMasterServer;
	public static event EventHandler                                SMOnMasterServerEvent;
	public static event EventHandler<NetworkInstantiateEventArgs>   SMOnNetworkInstantiate;
	public static event EventHandler                                SMOnPlayerConnected;
	public static event EventHandler                                SMOnPlayerDisconnected;
	public static event EventHandler<SerializeNetworkViewEventArgs> SMOnSerializeNetworkView;
	public static event EventHandler                                SMOnServerInitialized;
	public static event EventHandler                                SMStart;
	public static event EventHandler                                SMUpdate;


	//Only relevant mono methods
	private void Awake                          (                                         ) { if (SMAwake                           != null) SMAwake                          .Invoke(this, EventArgs.Empty                                ); }
	private void FixedUpdate                    (                                         ) { if (SMFixedUpdate                     != null) SMFixedUpdate                    .Invoke(this, EventArgs.Empty                                ); }
	private void LateUpdate                     (                                         ) { if (SMLateUpdate                      != null) SMLateUpdate                     .Invoke(this, EventArgs.Empty                                ); }
	private void OnApplicationFocus             (                                         ) { if (SMOnApplicationFocus              != null) SMOnApplicationFocus             .Invoke(this, EventArgs.Empty                                ); }
	private void OnApplicationPause             (                                         ) { if (SMOnApplicationPause              != null) SMOnApplicationPause             .Invoke(this, EventArgs.Empty                                ); }
	private void OnApplicationQuit              (                                         ) { if (SMOnApplicationQuit               != null) SMOnApplicationQuit              .Invoke(this, EventArgs.Empty                                ); }
	private void OnConnectedToServer            (                                         ) { if (SMOnConnectedToServer             != null) SMOnConnectedToServer            .Invoke(this, EventArgs.Empty                                ); }
	private void OnDisconnectedFromServer       (                                         ) { if (SMOnDisconnectedFromServer        != null) SMOnDisconnectedFromServer       .Invoke(this, EventArgs.Empty                                ); }
	private void OnFailedToConnect              (                                         ) { if (SMOnFailedToConnect               != null) SMOnFailedToConnect              .Invoke(this, EventArgs.Empty                                ); }
	private void OnFailedToConnectToMasterServer(                                         ) { if (SMOnFailedToConnectToMasterServer != null) SMOnFailedToConnectToMasterServer.Invoke(this, EventArgs.Empty                                ); }
	private void OnMasterServerEvent            (                                         ) { if (SMOnMasterServerEvent             != null) SMOnMasterServerEvent            .Invoke(this, EventArgs.Empty                                ); }
	private void OnNetworkInstantiate           (NetworkMessageInfo info                  ) { if (SMOnNetworkInstantiate            != null) SMOnNetworkInstantiate           .Invoke(this, new NetworkInstantiateEventArgs(info)          ); }
	private void OnPlayerConnected              (                                         ) { if (SMOnPlayerConnected               != null) SMOnPlayerConnected              .Invoke(this, EventArgs.Empty                                ); }
	private void OnPlayerDisconnected           (                                         ) { if (SMOnPlayerDisconnected            != null) SMOnPlayerDisconnected           .Invoke(this, EventArgs.Empty                                ); }
	private void OnSerializeNetworkView         (BitStream stream, NetworkMessageInfo info) { if (SMOnSerializeNetworkView          != null) SMOnSerializeNetworkView         .Invoke(this, new SerializeNetworkViewEventArgs(stream, info)); }
	private void OnServerInitialized            (                                         ) { if (SMOnServerInitialized             != null) SMOnServerInitialized            .Invoke(this, EventArgs.Empty                                ); }
	private void Start                          (                                         ) { if (SMStart                           != null) SMStart                          .Invoke(this, EventArgs.Empty                                ); }
	private void Update                         (                                         ) { if (SMUpdate                          != null) SMUpdate                         .Invoke(this, EventArgs.Empty                                ); }
}

[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
public class StaticMonoAttribute : Attribute
{

}



public class NetworkInstantiateEventArgs : EventArgs
{
	public NetworkMessageInfo info;
	public NetworkInstantiateEventArgs(NetworkMessageInfo info)
	{
		this.info = info;
	}
}

public class SerializeNetworkViewEventArgs : EventArgs
{
	public BitStream stream;
	public NetworkMessageInfo info;
	public SerializeNetworkViewEventArgs(BitStream stream, NetworkMessageInfo info)
	{
		this.stream = stream;
		this.info = info;
	}
}

//*/