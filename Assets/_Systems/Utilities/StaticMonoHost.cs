﻿/*

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StaticMonoHost : MonoBehaviour {

	public StaticMonoHost()
	{
		StaticMonoHostStatic.Host = this;
	}

	private void Awake () {
		foreach (Action awakeAction in StaticMonoHostStatic.AwakeActions)
		{
			if (awakeAction != null) awakeAction();
		}
	}

	//Only relevant mono methods
	public event EventHandler                                StatMonoFixedUpdate;
	public event EventHandler                                StatMonoLateUpdate;
	public event EventHandler                                StatMonoOnApplicationFocus;
	public event EventHandler                                StatMonoOnApplicationPause;
	public event EventHandler                                StatMonoOnApplicationQuit;
	public event EventHandler                                StatMonoOnConnectedToServer;
	public event EventHandler                                StatMonoOnDisconnectedFromServer;
	public event EventHandler                                StatMonoOnFailedToConnect;
	public event EventHandler                                StatMonoOnFailedToConnectToMasterServer;
	public event EventHandler                                StatMonoOnMasterServerEvent;
	public event EventHandler<NetworkInstantiateEventArgs>   StatMonoOnNetworkInstantiate;
	public event EventHandler                                StatMonoOnPlayerConnected;
	public event EventHandler                                StatMonoOnPlayerDisconnected;
	public event EventHandler<SerializeNetworkViewEventArgs> StatMonoOnSerializeNetworkView;
	public event EventHandler                                StatMonoOnServerInitialized;
	public event EventHandler                                StatMonoStart;
	public event EventHandler                                StatMonoUpdate;
	
    public class NetworkInstantiateEventArgs : EventArgs
    {
        public NetworkMessageInfo info;
        public NetworkInstantiateEventArgs(NetworkMessageInfo info) {
            this.info = info;
        }
    }

    public class SerializeNetworkViewEventArgs : EventArgs
    {
        public BitStream stream;
        public NetworkMessageInfo info;
        public SerializeNetworkViewEventArgs(BitStream stream, NetworkMessageInfo info)
        {
            this.stream = stream;
            this.info = info;
        }
    }

    //Only relevant mono methods
    private void FixedUpdate                    (                                         ) { if (StatMonoFixedUpdate                     != null) StatMonoFixedUpdate                    .Invoke(this, EventArgs.Empty            ); }
	private void LateUpdate                     (                                         ) { if (StatMonoLateUpdate                      != null) StatMonoLateUpdate                     .Invoke(this, EventArgs.Empty            ); }
	private void OnApplicationFocus             (                                         ) { if (StatMonoOnApplicationFocus              != null) StatMonoOnApplicationFocus             .Invoke(this, EventArgs.Empty            ); }
	private void OnApplicationPause             (                                         ) { if (StatMonoOnApplicationPause              != null) StatMonoOnApplicationPause             .Invoke(this, EventArgs.Empty            ); }
	private void OnApplicationQuit              (                                         ) { if (StatMonoOnApplicationQuit               != null) StatMonoOnApplicationQuit              .Invoke(this, EventArgs.Empty            ); }
	private void OnConnectedToServer            (                                         ) { if (StatMonoOnConnectedToServer             != null) StatMonoOnConnectedToServer            .Invoke(this, EventArgs.Empty            ); }
	private void OnDisconnectedFromServer       (                                         ) { if (StatMonoOnDisconnectedFromServer        != null) StatMonoOnDisconnectedFromServer       .Invoke(this, EventArgs.Empty            ); }
	private void OnFailedToConnect              (                                         ) { if (StatMonoOnFailedToConnect               != null) StatMonoOnFailedToConnect              .Invoke(this, EventArgs.Empty            ); }
	private void OnFailedToConnectToMasterServer(                                         ) { if (StatMonoOnFailedToConnectToMasterServer != null) StatMonoOnFailedToConnectToMasterServer.Invoke(this, EventArgs.Empty            ); }
	private void OnMasterServerEvent            (                                         ) { if (StatMonoOnMasterServerEvent             != null) StatMonoOnMasterServerEvent            .Invoke(this, EventArgs.Empty            ); }
	private void OnNetworkInstantiate           (NetworkMessageInfo info                  ) { if (StatMonoOnNetworkInstantiate            != null) StatMonoOnNetworkInstantiate           .Invoke(this, new NetworkInstantiateEventArgs(info)        ); }
	private void OnPlayerConnected              (                                         ) { if (StatMonoOnPlayerConnected               != null) StatMonoOnPlayerConnected              .Invoke(this, EventArgs.Empty            ); }
	private void OnPlayerDisconnected           (                                         ) { if (StatMonoOnPlayerDisconnected            != null) StatMonoOnPlayerDisconnected           .Invoke(this, EventArgs.Empty            ); }
	private void OnSerializeNetworkView         (BitStream stream, NetworkMessageInfo info) { if (StatMonoOnSerializeNetworkView          != null) StatMonoOnSerializeNetworkView         .Invoke(this, new SerializeNetworkViewEventArgs(stream, info)); }
	private void OnServerInitialized            (                                         ) { if (StatMonoOnServerInitialized             != null) StatMonoOnServerInitialized            .Invoke(this, EventArgs.Empty            ); }
	private void Start                          (                                         ) { if (StatMonoStart                           != null) StatMonoStart                          .Invoke(this, EventArgs.Empty            ); }
	private void Update                         (                                         ) { if (StatMonoUpdate                          != null) StatMonoUpdate                         .Invoke(this, EventArgs.Empty            ); }
}




//*/