﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class LoadAds : MonoBehaviour {

	// Use this for initialization
	void Start () {
		#if UNITY_ANDROID
		Advertisement.Initialize("1407500");
		#endif

		#if UNITY_IPHONE
		Advertisement.Initialize("1407501");
		#endif

		#if UNITY_EDITOR
		Advertisement.Initialize("1407500", true);
		#endif
	}

	public void OnAd()
	{
		if (UnityEngine.Random.Range (0f, 1f) < 1f / 3) 
			{
				Debug.Log ("displaying ad");
				//#if UNITY_ADS
				if (Advertisement.IsReady())
				{
					Advertisement.Show();
				}	
				//#endif
			}
	}

}
