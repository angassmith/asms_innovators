﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RevolitionGames.Quests;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.UI.Quests
{
	public abstract class QuestProgressSliderBase : MonoBehaviour 
	{
		private string _questCodeName;
		protected string QuestCodeName {
			get { return _questCodeName; }
			set {
				_questCodeName = value;
				dirty = true;
			}
		}

		private bool dirty;

		[SerializeField]
		private Image progressBar;

		[SerializeField]
		private Text textPercentage;

		protected virtual void Start()
		{
			var story = PlayModeStage.Current.Story;
			story.TaskStatusChanged += OnTaskStatusChanged;

			dirty = true;
		}

		private void OnTaskStatusChanged(object sender, StoryPartStatusEventArgs e)
		{
			var task = e.StoryPart as TaskSlot;
			if (task == null) throw new InvalidOperationException("Subscription or event is incorrect/not working");

			if (task.QuestSlot.CodeName == this.QuestCodeName) {
				dirty = true;
			}
		}

		void Update()
		{
			if (dirty)
			{
				var questProgress = PlayModeStage.Current.Story.Progress.GetQuestProgress(this.QuestCodeName);

				//TODO: Exclude hidden tasks from completed and available tasks collections (somehow)

				var completedTasks = questProgress.TaskProgresses.Values.Where(t => t.Status == StoryPartStatus.Completed);
				int numCompletedTasks = completedTasks.Count();

				var availableTasks = questProgress.TaskProgresses.Values.Where(t => t.Status != StoryPartStatus.Destroyed);
				int numAvailableTasks = availableTasks.Count();

				float progress = numCompletedTasks / (float)numAvailableTasks;
				if (float.IsNaN(progress)) progress = 0;

				progressBar.fillAmount = progress;
				textPercentage.text = Math.Round(progress * 100, MidpointRounding.AwayFromZero) + "%";

				dirty = false;

				//	QuestSlot quest;
				//	if (PlayModeStage.Current.Story.CurrentEpisode.Quests.TryGetValue(this.questCodeName, out quest))
				//	{
				//		if (quest.IsActive)
				//		{
				//			var availableTasks = quest.ActiveStoryPart.Tasks.Values.Where(t => t.Status != StoryPartStatus.Destroyed);
				//			int numAvailableTasks = availableTasks.Count();
				//	
				//			if (numAvailableTasks > 0)
				//			{
				//				progress = numCompletedTasks / (float)numAvailableTasks;
				//			}
				//			else
				//			{
				//				progress = numCompletedTasks > 0 ? 1 : 0;
				//			}
				//		}
				//		else
				//		{
				//			progress = numCompletedTasks > 0 ? 1 : 0;
				//		}
				//	}
				//	else
				//	{
				//		
				//	}
			}
		}

	}
}

