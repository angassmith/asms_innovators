﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.UI.Quests
{
	public class QuestPanel //Idk what you have to derive from
	{
		[SerializeField] //And edit in inspector
		private string _questCodeName;
		public string QuestCodeName { get { return _questCodeName; } }

		[SerializeField]
		private Text _statusText;

		private StoryPartStatus _questStatus;

		private bool _dirty;

		private void Start()
		{
			this._questStatus = PlayModeStage.Current.Story.Progress.GetQuestProgress(_questCodeName).Status;

			PlayModeStage.Current.Story.EpisodeActivated += (s, e) => {
				QuestSlot quest;
				if (e.StoryPart.ActiveStoryPart.Quests.TryGetValue(_questCodeName, out quest)) {
					quest.StatusChanged += OnQuestStatusChanged;
				}
			};

			PlayModeStage.Current.Story.EpisodeDeactivated += (s, e) => {
				QuestSlot quest;
				if (e.StoryPart.ActiveStoryPart.Quests.TryGetValue(_questCodeName, out quest)) {
					quest.StatusChanged -= OnQuestStatusChanged;
				}
			};
		}

		private void OnQuestStatusChanged(object sender, StoryPartStatusEventArgs e)
		{
			this._questStatus = e.NewStatus;
			this._dirty = true;
		}

		private void Update()
		{
			if (_dirty)
			{
				UpdateStatusText();
				UpdateBackgroundImage();
				_dirty = false;
			}
		}

		private void UpdateStatusText()
		{
			switch (_questStatus)
			{
				case StoryPartStatus.Unused:
					_statusText.text = "Locked Quest";
					break;
				case StoryPartStatus.Active:
					if (PlayModeStage.Current.Story.FocusQuest.CodeName == _questCodeName) {
						_statusText.text = "Focus Quest";
					} else {
						_statusText.text = "Active Quest";
					}
					break;
				case StoryPartStatus.Completed:
					_statusText.text = "Completed Quest";
					break;
				case StoryPartStatus.Destroyed:
					_statusText.text = "Removed Quest";
					break;
			}
		}

		private void UpdateBackgroundImage()
		{
			//TODO
		}
	}
}

//*/