﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.UI.Quests
{
	public class TaskSummaryList //idk what to derive from
	{
		[SerializeField] private GameObject _unusedTaskSummaryPrefab   ; public GameObject UnusedTaskSummaryPrefab    { get { return _unusedTaskSummaryPrefab   ; } }
		[SerializeField] private GameObject _activeTaskSummaryPrefab   ; public GameObject ActiveTaskSummaryPrefab    { get { return _activeTaskSummaryPrefab   ; } }
		[SerializeField] private GameObject _completedTaskSummaryPrefab; public GameObject CompletedTaskSummaryPrefab { get { return _completedTaskSummaryPrefab; } }
		[SerializeField] private GameObject _destroyedTaskSummaryPrefab; public GameObject DestroyedTaskSummaryPrefab { get { return _destroyedTaskSummaryPrefab; } }

		[SerializeField] private bool _showUnused   ; public bool ShowUnused    { get { return _showUnused   ; } }
		[SerializeField] private bool _showActive   ; public bool ShowActive    { get { return _showActive   ; } }
		[SerializeField] private bool _showCompleted; public bool ShowCompleted { get { return _showCompleted; } }
		[SerializeField] private bool _showDestroyed; public bool ShowDestroyed { get { return _showDestroyed; } }

		[SerializeField]
		private string _questCodeName;
		public string QuestCodeName { get { return _questCodeName; } }

		private void Start()
		{
			foreach (var taskProgress in PlayModeStage.Current.Story.Progress.GetQuestProgress(_questCodeName).TaskProgresses)
			{

			}
		}

		private void Update()
		{

		}
	}
}

//*/