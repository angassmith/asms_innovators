﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames.UI.Quests
{
	public sealed class CurrentQuestProgressSlider : QuestProgressSliderBase
	{
		protected override void Start()
		{
			base.Start();

			var story = PlayModeStage.Current.Story;
			story.FocusQuestChanged += (s, e) => {
				this.QuestCodeName = e.StoryPart.CodeName;
			};
		}


	}
}

//*/