﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RevolitionGames.UI.Quests
{
	public sealed class SingleQuestProgressSlider : QuestProgressSliderBase
	{

#if UNITY_EDITOR
		[CustomEditor(typeof(SingleQuestProgressSlider))]
		private class SliderEditor : Editor
		{
			public override void OnInspectorGUI()
			{
				base.OnInspectorGUI();

				var slider = (SingleQuestProgressSlider)target;

				slider.QuestCodeName = GUILayout.TextField(slider.QuestCodeName);
			}
		}
#endif
	}
}

//*/