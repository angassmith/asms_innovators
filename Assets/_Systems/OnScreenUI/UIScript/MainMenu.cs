﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RevolitionGames;
using UnityEngine.SceneManagement;
using System.IO;

public class MainMenu : MonoBehaviour {

    public GameObject resourcesPanel;

    public void PlayGame() {
		Game.Current.LoadAndPlay(); //hi
    }

	public void KidsHelpline()
    {
        Application.OpenURL("https://kidshelpline.com.au/");
    }

    public void Headspace()
    {
        Application.OpenURL("https://www.beyondblue.org.au/");
    }

    public void Respect()
    {
        Application.OpenURL("https://www.1800respect.org.au/");
    }

    public void OnResourcesPressed()
    {
        if(resourcesPanel.activeInHierarchy == true)
        {
            resourcesPanel.SetActive(false);
        } else if(resourcesPanel.activeInHierarchy != true)
        {
            resourcesPanel.SetActive(true);
        }
    }

    public void Exit() {
        Application.Quit();
    }
}
