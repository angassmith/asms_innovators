﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class nyoooom : MonoBehaviour
{

    public Text txt;
    public float nyooomSpd = 20;
    private bool nyyom = false;

    public void NYOOM()
    {

        nyyom = true;
    }

    void Update()
    {
        if (!nyyom)
        {
            txt.transform.position += new Vector3(0, 1, 0);
        }
        else if (nyyom)
        {
            txt.transform.position += new Vector3(0, 1 * nyooomSpd, 0);
        }
    }

    public void NYOOMExit()
    {

        nyyom = false;
    }
}
