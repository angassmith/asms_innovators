﻿using System.Collections;
using System.Collections.Generic;
using RevolitionGames.SceneManagement;
using UnityEngine;

namespace RevolitionGames.UI {

    /// <summary>
    /// Provides functions for the in game Pause menu.
    /// </summary>
    public class PauseMenu : MonoBehaviour {

        /// <summary>
        /// The reference to the scene changer.
        /// </summary>
        [SerializeField] // edit in inspector
        private AudioSource flowchartAudio;

        /// <summary>
        /// The sort order to give the pause menu canvas while paused.
        /// </summary>
        private const int PAUSE_SORT_ORDER = 2;

        /// <summary>
        /// The reference to canvas containing the pause menu.
        /// </summary>
        private Canvas uiCanvas;

        /// <summary>
        /// Whether or not audio was paused when the game was last paused.
        /// </summary>
        private bool audioWasPaused = false;

        // Use this for initialization
        void Awake() {
            if (flowchartAudio == null) {
                Debug.LogError("Flowchart audio not assigned in Editor.");
            }

            uiCanvas = transform.root.GetComponent<Canvas>();
            if (uiCanvas == null) {
                Debug.LogError("Canvas not found on " + transform.root.name);
            }
        }

        /// <summary>
        /// Pauses the game and shows the pause menu.
        /// </summary>
        public void Pause() {
            // Show the pause menu & put the canvas sorted to the top
            gameObject.SetActive(true);
            uiCanvas.sortingOrder = PAUSE_SORT_ORDER;

            Time.timeScale = 0f;

            // Pause audio if it is playing
            if (flowchartAudio != null && flowchartAudio.isPlaying) {
                flowchartAudio.Pause();
                audioWasPaused = true;
            }
            
        }

        /// <summary>
        /// Resumes the game and hides the pause menu.
        /// </summary>
        public void Resume() {
            // Hide the pause menu & reset the canvas sorting
            gameObject.SetActive(false);
            uiCanvas.sortingOrder = 0;

            Time.timeScale = 1f;

            // If audio was paused, resume the audio
            if (flowchartAudio != null && audioWasPaused) {
                flowchartAudio.Play();
                audioWasPaused = false;
            }
        }

        /// <summary>
        /// Loads the main menu scene.
        /// </summary>
        public void LoadMainMenu() {
			Game.Current.SwitchToMenu();
            Resume();
        }
    }
}
