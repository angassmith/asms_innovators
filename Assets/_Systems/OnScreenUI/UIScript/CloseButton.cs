﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RevolitionGames;

public class CloseButton : MonoBehaviour {

    public void CloseMenu()
    {
        gameObject.SetActive(!gameObject.activeSelf);
        PlayModeStage.Current.PlayerUI.DialogOpen = false;

    }


	
}
