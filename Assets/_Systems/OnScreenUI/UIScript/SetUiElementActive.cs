﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RevolitionGames;

public class SetUiElementActive : MonoBehaviour {

    public void SetElementActive()
    {
        gameObject.SetActive(!gameObject.activeSelf);
        PlayModeStage.Current.PlayerUI.DialogOpen = true;
        
    }


}
