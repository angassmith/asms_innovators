﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SettingsManager : MonoBehaviour {

    public Toggle fullscreenToggle;
    public Dropdown resolutionDropdown;
    public Dropdown textureQualityDropdown;
    public Slider musicVolumeSlider;

    public AudioSource musicSource;
    public Resolution[] resolutions;
    public GameSettings gameSettings;
    public Button applyButton;

	private string _settingsSavePath;
	private string settingsSavePath {
		get {
			if (_settingsSavePath == null) {
				_settingsSavePath = Application.persistentDataPath + "RevGames/Experience/gamesettings.json";
			}
			return _settingsSavePath;
		}
	}

    void OnEnable()
    {
        gameSettings = new GameSettings();

        fullscreenToggle.onValueChanged.AddListener(delegate { OnFullscreenToggle(); });
        resolutionDropdown.onValueChanged.AddListener(delegate { OnResolutionChange(); });
        textureQualityDropdown.onValueChanged.AddListener(delegate { OnTextureQualityChange(); });
        musicVolumeSlider.onValueChanged.AddListener(delegate { OnMusicVolumeChange(); });
        applyButton.onClick.AddListener(delegate { OnApplyButtonClick(); });

        resolutions = Screen.resolutions;
        foreach(Resolution resolution in resolutions)
        {
            resolutionDropdown.options.Add(new Dropdown.OptionData(resolution.ToString()));
        }

        LoadSettings();
    }

    public void OnFullscreenToggle()
    {
        gameSettings.fullscreen = Screen.fullScreen = fullscreenToggle.isOn;
    }

    public void OnResolutionChange()
    {
        Screen.SetResolution(resolutions[resolutionDropdown.value].width, resolutions[resolutionDropdown.value].height, Screen.fullScreen);
        gameSettings.resolutionIndex = resolutionDropdown.value;
    }

    public void OnTextureQualityChange()
    {
        QualitySettings.masterTextureLimit = gameSettings.textureQuality = textureQualityDropdown.value;
    }
    
    public void OnMusicVolumeChange()
    {
        musicSource.volume = gameSettings.musicVolume = musicVolumeSlider.value;
    }

    public void SaveSettings()
    {
        string jsonData = JsonUtility.ToJson(gameSettings, true);
        File.WriteAllText(settingsSavePath, jsonData);
    }

    public void OnApplyButtonClick()
    {
        SaveSettings();
    }

    public void LoadSettings()
    {
        if (File.Exists(settingsSavePath)) {
			gameSettings = JsonUtility.FromJson<GameSettings>(File.ReadAllText(settingsSavePath));
		} else {
			gameSettings = new GameSettings() {
				fullscreen = fullscreenToggle.isOn,
				textureQuality = textureQualityDropdown.value,
				resolutionIndex = resolutionDropdown.value,
				musicVolume = musicVolumeSlider.value
			};
		}

		musicVolumeSlider.value = gameSettings.musicVolume;
        textureQualityDropdown.value = gameSettings.textureQuality;
        resolutionDropdown.value = gameSettings.resolutionIndex;
        fullscreenToggle.isOn = gameSettings.fullscreen;

        resolutionDropdown.RefreshShownValue();
    }
}
