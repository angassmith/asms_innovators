﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RevolitionGames;

namespace RevolitionGames.UI {
    /// <summary>
    /// Provides a method to flip the active/inactive state of the game object it is attached to.
    /// </summary>
    public class ActiveStateFlipper : MonoBehaviour {

        /// <summary>
        /// Switches the game object to active or inactive depending on its current state.
        /// </summary>
        public void FlipActiveState() {
            // Ignore the call if an interaction is currently occurring
            if (!Game.Current.InteractionOccurring && PlayModeStage.Current.PlayerUI.DialogOpen == false) {
                gameObject.SetActive(!gameObject.activeSelf);
                PlayModeStage.Current.PlayerUI.DialogOpen = true;

            }

            else if(!Game.Current.InteractionOccurring && PlayModeStage.Current.PlayerUI.DialogOpen == true)
            {
                gameObject.SetActive(!gameObject.activeSelf);
                PlayModeStage.Current.PlayerUI.DialogOpen = false;

            }
        }
    }
}
