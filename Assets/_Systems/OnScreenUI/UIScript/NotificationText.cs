﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RevolitionGames.Inventory;
using RevolitionGames.Quests;


namespace RevolitionGames.UI {
    /// <summary>
    /// Displays notifications that fade over a set duration.
    /// </summary>
    public class NotificationText : MonoBehaviour {

        /// <summary>
        /// A reference to the player inventory.
        /// </summary>
        public PlayerInventory playerInventory;

        /// <summary>
        /// The duration that notifications should fade over.
        /// </summary>
        public float DisplayDuration = 3f;

        /// <summary>
        /// Reference to the current story being listened to.
        /// </summary>
        private Story currentStory = null;
        /// <summary>
        /// The notification display UI text element.
        /// </summary>
        private Text notificationDisplay;

        /// <summary>
        /// The initial text colour, before any fading occurs.
        /// </summary>
        private Color initialTextColour;
        /// <summary>
        /// The current text colour.
        /// </summary>
        private Color currentTextColour;

        /// <summary>
        /// Whether or not a notification is currently being displayed.
        /// </summary>
        private bool displayingNotification = false;
        /// <summary>
        /// The queue of notifications to display.
        /// </summary>
        private Queue<string> notifcationsToDisplay = new Queue<string>();

        // Use this for initialization
        void Awake() {

            // Subscribe to inventory events
            if (playerInventory != null) {
                playerInventory.ItemAdded += DisplayItemAdded;
                playerInventory.ItemUpdated += DisplayItemAdded;
            } else {
                Debug.LogError("No player inventory set in Editor");
            }

            // Initialise the text variables
            notificationDisplay = GetComponent<Text>();
            if (notificationDisplay != null) {
                initialTextColour = notificationDisplay.color;
            }
            else {
                Debug.LogError("No text component found on " + gameObject.name);
            }

            // Disable itself
            gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update() {
            if (displayingNotification) {
                UpdateDisplay();
            }
        }

        /// <summary>
        /// Subscribes to appropriate events for the specified story.
        /// </summary>
        /// <param name="story"></param>
        public void SubscribeToStoryEvents(Story story) {
            // Unsubscribe from previous story events
            if (currentStory != null) {
                currentStory.TaskCompleted -= DisplayTaskCompleted;
                currentStory.QuestCompleted -= DisplayQuestCompleted;
                currentStory.EpisodeCompleted -= DisplayEpisodeCompleted;
            }

            // Subscribe to the current story
            story.TaskCompleted += DisplayTaskCompleted;
            story.QuestCompleted += DisplayQuestCompleted;
            story.EpisodeCompleted += DisplayEpisodeCompleted;

            currentStory = story;
        }

        /// <summary>
        /// Displays an item added notification.
        /// </summary>
        /// <param name="item">The inventory item that was added.</param>
        /// <param name="e"></param>
        private void DisplayItemAdded(InventoryItem item, System.EventArgs e) {
            DisplayOrQueueNotification(item.ItemReference.DisplayName + " collected");
        }

        /// <summary>
        /// Displays a task completed notification.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayTaskCompleted(object sender, StoryPartEventArgs<TaskSlot> e) {
            TaskSlot task = e.StoryPart;
            if (task.Visible) {
                DisplayOrQueueNotification(task.DisplayName + " task completed");
            }
        }

        /// <summary>
        /// Displays a quest completed notification.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayQuestCompleted(object sender, StoryPartEventArgs<QuestSlot> e) {
            QuestSlot quest = e.StoryPart;
            DisplayOrQueueNotification(quest.DisplayName + " quest completed");
        }

        /// <summary>
        /// Displays an episode completed notification.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayEpisodeCompleted(object sender, StoryPartEventArgs<EpisodeSlot> e) {
            EpisodeSlot episode = e.StoryPart;
            DisplayOrQueueNotification(episode.DisplayName + " episode completed");
        }

        /// <summary>
        /// Displays the notification if none are showing,
        /// otherwise queues the notification.
        /// </summary>
        /// <param name="message">The notification message.</param>
        private void DisplayOrQueueNotification(string message) {
            if (!displayingNotification) {
                DisplayNotification(message);
            } else {
                notifcationsToDisplay.Enqueue(message);
            }
        }

        /// <summary>
        /// Displays the specified message.
        /// </summary>
        /// <param name="message">The notification message.</param>
        private void DisplayNotification(string message) {
            notificationDisplay.text = message;

            currentTextColour = initialTextColour;
            notificationDisplay.color = currentTextColour;

            gameObject.SetActive(true);
            displayingNotification = true;
        }

        /// <summary>
        /// Updates the currently displayed notification fade colour.
        /// Triggers the next notification if the fade is complete.
        /// </summary>
        private void UpdateDisplay() {
            currentTextColour.a -= Time.deltaTime / DisplayDuration;

            // Once the alpha reaches 0, the fade is complete
            if (currentTextColour.a <= 0) {
                currentTextColour.a = 0;

                // Display the next notification if one is queued
                if (notifcationsToDisplay.Count > 0) {
                    DisplayNotification(notifcationsToDisplay.Dequeue());
                }
                // Disable itself if none are queued
                else {
                    displayingNotification = false;
                    gameObject.SetActive(false);
                }
            }
            notificationDisplay.color = currentTextColour;
        }
    }
}
