﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RevolitionGames;
using Fungus;

namespace RevolitionGames.Interaction
{
    [CommandInfo("Enabling and Disabling", "Disable Joystick", "Disables the joystick and other interactable icons.")]

    public class MenuDialogJoystickDisable : Command
    {

        public GameObject joystick;

        public override void OnEnter()
        {
            joystick.GetComponent<UnityStandardAssets.CrossPlatformInput.Joystick>().disableJoystick = true;
            PlayModeStage.Current.PlayerUI.DialogOpen = true;
            Continue();
        }


    }

}