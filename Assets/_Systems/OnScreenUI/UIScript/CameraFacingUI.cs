﻿using System.Collections;
using System.Collections.Generic;
using RevolitionGames.Interaction;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.UI {
    /// <summary>
    /// Makes a UI element under a World Space canvas appear above an object and
    /// always face the camera.
    /// </summary>
    public class CameraFacingUI : MonoBehaviour {

        public Sprite[] icons;

        private Interactable targetInteractable;
		private InteractDetector targetDetector;

        private SpriteRenderer _spriteRenderer;
		private SpriteRenderer SpriteRenderer {
			get {
				if (_spriteRenderer == null) _spriteRenderer = this.GetComponent<SpriteRenderer>();
				return _spriteRenderer;
			}
		}

		/// <summary>
        /// The camera that the UI element should face towards.
        /// </summary>
        private Camera _cameraToFace;
		private Camera CameraToFace {
			get {
				if (_cameraToFace == null)
				{
                    //TODO: Handle multiple cameras in the scene - eg. the minimap
                    _cameraToFace = GameObject.FindGameObjectWithTag("MainCamera").GetComponentInChildren<Camera>();
					Debug.Log("CameraFacingUI searched for Camera and found '" + _cameraToFace + "'.", context: this);
				}
				return _cameraToFace;
			}
		}

        //	private float prevAngleY = 0;

        //	/// <summary>
        //	/// The offset of the UI element, 
        //	/// i.e. how far above the target object the UI element should be.
        //	/// </summary>
        //	private const float Y_POSITION_OFFSET = 0.1f;
		//	
        //	private const float Z_POSITION_OFFSET = 0.5f;

        //	/// <summary>
        //	/// The renderer of the target object.
        //	/// </summary>
        //	private Renderer targetRenderer;

		/// <summary>
		/// Find the camera on Awake.
		/// </summary>
		private void Awake()
		{

        }

        public void SetTarget(Interactable targetInteractable, InteractDetector detector, Sprite iconPref)
        {
			SpriteRenderer.sprite = iconPref;
            SpriteRenderer.material.renderQueue = 4000;
            this.targetInteractable = targetInteractable;
			this.targetDetector = detector;
        }

        /// <summary>
        /// Update the position and face the camera.
        /// </summary>
        void Update()
		{
			if (targetInteractable == null)
			{
				this.gameObject.SetActive(false); //Idk what this is for. TODO: Change??
				return;
			}

            MoveToTargetPosition(this.targetInteractable, this.targetDetector);

			FaceCamera(this.CameraToFace);
        }

        

		/// <summary>
		/// Updates the position of the UI element to the position
		/// specified by the target interactable.
		/// </summary>
		private void MoveToTargetPosition(Interactable target, InteractDetector targetsDetector)
		{
			transform.position = target.GetInteractIconPosition(detector: targetsDetector);
		}

		private void FaceCamera(Camera camera)
		{
            Vector3 cameraToFaceY = camera.transform.forward;
            float angleZ = 0;
            cameraToFaceY.y = 0;
            Vector3 relative = transform.InverseTransformPoint(camera.transform.position);
            angleZ = Mathf.Atan2(relative.x, relative.y) * Mathf.Rad2Deg;
            transform.Rotate(0, 0, -angleZ);
            transform.rotation = Quaternion.LookRotation(cameraToFaceY);
        }

		//	/// <summary>
        //	/// Sets the target object for the UI element to appear above.
        //	/// </summary>
        //	/// <param name="target"></param>
        //	public void SetTarget(GameObject target) {
        //	    // Get the renderer component on the target
        //	    // Use GetComponentInChildren as it may be on a child of the target
        //	    targetRenderer = target.GetComponentInChildren<Renderer>();
		//	
        //	    // Find the camera again, as we may have changed scenes.
        //	    cameraToFace = FindObjectOfType<Camera>();
        //	    UpdatePosition();
        //	}

        //	/// <summary>
        //	/// Updates the position of the UI element to be above the middle 
        //	/// of the target object.
        //	/// </summary>
        //	private void UpdatePosition()
		//	{
		//		
		//		//Debug.Log(iconType);
        //	    if (targetInteractable != "door")
        //	    {
        //	        float xPosition = targetRenderer.bounds.center.x;
        //	        float yPosition = targetRenderer.bounds.center.y + targetRenderer.bounds.size.y / 2;
        //	        float zPosition = targetRenderer.bounds.center.z;
		//		
        //	        transform.position = new Vector3(xPosition, yPosition + Y_POSITION_OFFSET, zPosition);
        //	    }
        //	    else if(targetInteractable == "door")
        //	    {
		//			if (PlayModeSceneData.IsCurrent)
		//			{
		//				var player = PlayModeSceneData.Current.Player;
		//				Renderer playerRenderer = player.GetComponentInChildren<Renderer>();
		//		
		//				if (playerRenderer == null) return; //TODO: Remove and fix
		//		
		//				float xPosition = playerRenderer.bounds.center.x;
		//				float yPosition = playerRenderer.bounds.center.y + (targetRenderer.bounds.size.y / 2) - Y_POSITION_OFFSET;
		//				float zPosition = playerRenderer.bounds.center.z;
		//		
		//				transform.position = new Vector3(xPosition, yPosition, zPosition);
        //	        }
		//			else
		//			{
		//				Debug.LogWarning("TODO: Handle PlayModeSceneData.IsCurrent == false");
		//			}
        //	    }
		//		else
        //	    {
        //	        Debug.Log(targetInteractable);
        //	    }
        //	}
    }
}
