﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RevolitionGames;
using Fungus;

namespace RevolitionGames.Interaction
{
    [CommandInfo("Enabling and Disabling", "Enable Joystick", "Enables the joystick and other interactable icons.")]

    public class MenuDialogJoystickEnable : Command
    {
        public GameObject joystick;

        public override void OnEnter()
        {
            joystick.GetComponent<UnityStandardAssets.CrossPlatformInput.Joystick>().disableJoystick = false;
            PlayModeStage.Current.PlayerUI.DialogOpen = false;
            Continue();
        }


    }

}