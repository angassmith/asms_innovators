﻿using RevolitionGames;
using RevolitionGames.Quests;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ItemsThatAreCollectedUI : MonoBehaviour
{

    //Properties

    private Color completedColour = new Color(0, 255, 0, 255);


    private Color notCompletedColor = new Color(0, 255, 255, 255);

    [SerializeField]
    private Text taskText;

    [SerializeField]
    private Image pauseMenuPanel;

    [SerializeField]
    private GameObject tasksgamebject; // gameobject that is the parent of all the text elements


    [SerializeField]
    private float prevTextYPos;

    public Dictionary<string, Text> TasksToBeCollected = new Dictionary<string, Text>();



    private Story currentStory { get { return PlayModeStage.Current.Story; } }

    // Use this for initialization
    void Start()
    {
        // Subscribe to the current story
        currentStory.TaskCompleted += OnTaskCompleted;
        currentStory.TaskActivated += OnTaskActivated;
        currentStory.QuestActivated += OnQuestCompleted;

        //set the y pos of the text
        prevTextYPos = taskText.rectTransform.position.y;

    }

    private void OnTaskCompleted(object sender, StoryPartEventArgs<TaskSlot> e)
    {
        TaskSlot completedTask = e.StoryPart;
        UpdateColours(completedTask);

    }

    private void UpdateColours(TaskSlot completedTask)
    {
        foreach (KeyValuePair<string, Text> task in TasksToBeCollected)
        {
            if (completedTask.CodeName == task.Key)
            {
                task.Value.color = completedColour;

            }
        }
    }

    private void OnTaskActivated(object sender, StoryPartEventArgs<TaskSlot> e)
    {
        TaskSlot task = e.StoryPart;
        if(task.Visible == true)
        {
            CreateTextElement(task);

        }


    }

    private void OnQuestCompleted(object sender, StoryPartEventArgs<QuestSlot> e)
    {
        // reset the y pos of the text elements
        prevTextYPos = taskText.rectTransform.position.y;
        DestroyTextElements();

    }

    private void CreateTextElement(TaskSlot activetasktobeputin)
    {
  
        var textElement = Instantiate(taskText, new Vector3(taskText.transform.position.x, prevTextYPos - 63), new Quaternion(0, 0, 0, 0), tasksgamebject.transform);
        textElement.text = activetasktobeputin.DisplayName;
        prevTextYPos = textElement.rectTransform.position.y;
        TasksToBeCollected.Add(activetasktobeputin.CodeName, textElement);
    }

    private void DestroyTextElements()
    {
        if (TasksToBeCollected != null)
        {

            // destroy the text gameobjects
            foreach (KeyValuePair<string, Text> textelement in TasksToBeCollected)
            {
               
                GameObject.Destroy(textelement.Value.gameObject);

            }

            // remove entries from dict

            List<string> textelementkeys = new List<string>(TasksToBeCollected.Keys);

            foreach(string key in textelementkeys)
            {
                TasksToBeCollected.Remove(key);
            }
        }


        else
        {
            throw new NullReferenceException("TasksToBeCollected is null");
        }

        }
    }

