﻿using UnityEngine;
using UnityEngine.UI;

/// Taken from an answer by slippdouglas (http://answers.unity3d.com/users/153967/slippdouglas.html)
/// to the question "UI panel without Image component as Raycast target? It is possible?"
/// http://answers.unity3d.com/questions/1091618/ui-panel-without-image-component-as-raycast-target.html
/// 
/// <summary>
/// A concrete subclass of the Unity UI `Graphic` class that just skips drawing.
/// </summary>
public class NonDrawingGraphic : Graphic {
    public override void SetMaterialDirty() { return; }
    public override void SetVerticesDirty() { return; }

    protected override void OnPopulateMesh(VertexHelper vh) {
        vh.Clear();
        return;
    }
}