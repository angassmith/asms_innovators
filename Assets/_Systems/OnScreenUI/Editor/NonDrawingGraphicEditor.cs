﻿using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

/// Taken from an answer by slippdouglas (http://answers.unity3d.com/users/153967/slippdouglas.html)
/// to the question "UI panel without Image component as Raycast target? It is possible?"
/// http://answers.unity3d.com/questions/1091618/ui-panel-without-image-component-as-raycast-target.html
/// 
/// <summary>
/// Hides the Color and Material settings for the NonDrawingGraphic class in the Editor.
/// </summary>
[CanEditMultipleObjects, CustomEditor(typeof(NonDrawingGraphic), false)]
public class NonDrawingGraphicEditor : GraphicEditor {
    public override void OnInspectorGUI() {
        base.serializedObject.Update();
        EditorGUILayout.PropertyField(base.m_Script, new GUILayoutOption[0]);
        // skipping AppearanceControlsGUI
        base.RaycastControlsGUI();
        base.serializedObject.ApplyModifiedProperties();
    }
}
