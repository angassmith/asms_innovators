﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.UI
{
    public class MapObj
    {
        public Image icon { get; set; }
        public GameObject owner { get; set; }
    }
    public class MiniMapController : MonoBehaviour
    {

        public Transform playerPos;
        public Camera mapCam;

        public static List<MapObj> mapObjects = new List<MapObj>();

        public static void RegisterMapObject(GameObject o, Image i)
        {
            Image image = Instantiate(i);
            mapObjects.Add(new MapObj() { owner = o, icon = image });
        }

        public static void RemoveMapObject(GameObject o)
        {
            List<MapObj> newList = new List<MapObj>();
            for (int i = 0; i  <mapObjects.Count; i++)
            {
                if(mapObjects[i].owner == o)
                {
                    Destroy(mapObjects[i].icon);
                    continue;
                }
                else
                {
                    newList.Add(mapObjects[i]);
                }
            }

            mapObjects.RemoveRange(0, mapObjects.Count);
            mapObjects.AddRange(newList);
        }

        void DrawMapIcons()
        {
            foreach (MapObj mo in mapObjects)
            {
                Vector2 mop = new Vector2(mo.owner.transform.position.x, mo.owner.transform.position.y);
                Vector2 pp = new Vector2(playerPos.position.x, playerPos.position.y);

                if(Vector2.Distance(mop,pp) > 50)
                {
                    mo.icon.enabled = false;
                    continue;
                }
                else
                {
                    mo.icon.enabled = true;
                }

                Vector3 screenPos = mapCam.WorldToViewportPoint(mo.owner.transform.position);
                mo.icon.transform.SetParent(this.transform);
                RectTransform rt = this.GetComponent<RectTransform>();
                Vector3[] corners = new Vector3[4];
                rt.GetWorldCorners(corners);
                screenPos.x = Mathf.Clamp(screenPos.x * rt.rect.width + corners[0].x, corners[0].x, corners[2].x);
                screenPos.y = Mathf.Clamp(screenPos.y * rt.rect.height + corners[0].y, corners[0].y, corners[1].y);
                screenPos.z = 0;
                mo.icon.transform.position = screenPos;
                mo.icon.transform.localScale = new Vector3(1, 1, 1);
            }
        }

        void Update()
        {

            if(playerPos == null)
            {
                playerPos = GameObject.FindGameObjectWithTag("Player").transform;
            }

            if (mapCam == null)
            {
                mapCam = GameObject.FindGameObjectWithTag("MiniMapCam").GetComponent<Camera>();
            }

            DrawMapIcons();
        }

    }
}
