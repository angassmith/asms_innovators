﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapPress : MonoBehaviour {

	public GameObject miniMap;

	void Update()
	{
		if (miniMap == null) {
			miniMap = GameObject.FindGameObjectWithTag ("Map");
		} else if (miniMap != null) 
		{
			//comment
		}
	}

	public void OnClickerino()
	{
		if (miniMap != null && miniMap.active == false) 
		{
			miniMap.SetActive (true);
		}
	}

	public void UnClickerino()
	{
		if (miniMap != null && miniMap.active == true) 
		{
			miniMap.SetActive (false);
		}
	}
}
