﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Interaction;
using RevolitionGames.Likeability;

namespace RevolitionGames.NPCs
{
	public class NPCData : NPCSet
	{
		//	private NPCMono _mono;
		//	public NPCMono Mono { get { return _mono; } }

		//public NPCPathfinder Pathfinder { get; private set; }

		//public NPCInteract NPCInteract { get; private set; }

		private SingleNPCLikeability _likeability = new SingleNPCLikeability();
		public override NPCSetLikeability Likeability { get { return _likeability; } }

		/// <summary>
		/// Creates a new <see cref="NPCData"/>, using <paramref name="initMono"/> to initialize various properties.
		/// Registers the new <see cref="NPCData"/> in <see cref="NPCHost.AllNPCs"/> and <see cref="NPCHost.AllNPCSets"/>.
		/// Destroys <paramref name="initMono"/> (in the next frame).
		/// </summary>
		public NPCData(string name)
			: base(name)
		{
			/*if (initMono == null) throw new ArgumentNullException("initMono");
			if (initMono.NPCInteract == null) throw new ArgumentNullException("initMono.NPCInteract");
			if (initMono.Pathfinder == null) throw new ArgumentNullException("initMono.Pathfinder");

			//this.Pathfinder = initMono.Pathfinder;
			//this.NPCInteract = initMono.NPCInteract;

			UnityEngine.Object.Destroy(initMono);*/
		}
		


	}
}

//*/