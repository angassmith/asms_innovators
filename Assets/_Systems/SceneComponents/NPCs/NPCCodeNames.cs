﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.NPCs
{
	public static class NPCCodeNames
	{
		public const string Sam = "Sam";
		public const string Ami = "Ami";
		public const string Liam = "Liam";

        public const string Dad = "Dad";
        public const string Jack = "Jack";

		public const string HomeroomTeacher = "homeroomTeacher";
		public const string MusicTeacher = "musicTeacher";
        public const string PETeacher = "peTeacher";
		public const string Principal = "principal";

		public static class NPCGroups
		{
			public const string AllNPCs = "all-npcs";

			public const string Students = "students";
			public const string Staff = "staff";
		}
	}
}

//*/