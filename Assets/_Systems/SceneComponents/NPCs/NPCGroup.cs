﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using RevolitionGames.Likeability;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.NPCs
{
	public class NPCGroup : NPCSet
	{
		// XML properties
		public static readonly XNamespace NPCNamespace = "http://www.revolitiongames.com/experience/npcs";

		//TODO: add values for these
		//	public static string NPCXMLPath;

		public const string NPCGroupSchemaPath = "TODO";
	

		private List<NPCData> _npcs = new List<NPCData>();
		public ReadOnlyCollection<NPCData> NPCs { get; private set; }

		public event EventHandler<AddRemoveEventArgs<NPCData>> NPCAdded;
		public event EventHandler<AddRemoveEventArgs<NPCData>> NPCRemoved;

		private readonly NPCGroupLikeability _likeability;
		public override NPCSetLikeability Likeability { get { return _likeability; } }



		public NPCGroup(string name, float propagationStrength, IEnumerable<NPCData> npcs = null)
			: base(name)
		{
			this._npcs = (npcs ?? Enumerable.Empty<NPCData>()).ToList();
			this.NPCs = _npcs.AsReadOnly();
			this._likeability = new NPCGroupLikeability(this, propagationStrength);
		}



		public void Add(NPCData npc)
		{
			if (npc == null) throw new ArgumentNullException("npc");

			_npcs.Add(npc);

			NPCAdded.Fire(this, new AddRemoveEventArgs<NPCData>(npc));

			
		}

		public bool Remove(NPCData npc)
		{
			if (npc == null) throw new ArgumentNullException("npc");

			bool wasPresent = _npcs.Remove(npc);
			if (wasPresent)
			{
				NPCRemoved.Fire(this, new AddRemoveEventArgs<NPCData>(npc));
				
			}

			return wasPresent;
		}



		

		




		public static NPCGroup FromXML(FileStream npcGroupFile)
		{
			FileStream groupSchemaFile = new FileStream(NPCGroupSchemaPath, FileMode.Open, FileAccess.Read);

			var groupSchema = XmlSchema.Read(
				new StreamReader(groupSchemaFile),
				XmlValidationExceptionLogger
			);

			var xreaderSettings = new XmlReaderSettings();
			xreaderSettings.Schemas.Add(groupSchema);
			xreaderSettings.ValidationType = ValidationType.Schema;
			xreaderSettings.ValidationEventHandler += XmlValidationExceptionLogger;

			var xreader = XmlReader.Create(npcGroupFile, xreaderSettings);
			//var xreader = System.Xml.XmlReader.Create(QuestXMLFile);
			XDocument xdoc = XDocument.Load(xreader);

			// TODO: add XML reading here
			// make sure to use NPCGroup.Add() above to add the npcs (as it subscribes to the event + more)

			throw new NotImplementedException();
		}

		private static void XmlValidationExceptionLogger(object sender, ValidationEventArgs e)
		{
			if (e.Severity == XmlSeverityType.Error)
			{
				Debug.LogException(e.Exception);
			}
			else
			{
				Debug.LogWarning(e.Exception);
			}
		}
	}

	
}

