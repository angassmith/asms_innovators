﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.NPCs
{
	public abstract class NPCProvider
	{
		private HashSet<NPCHost> InitializedNPCHosts = new HashSet<NPCHost>();

		public bool HasInitializedNPCs(NPCHost host) {
			return InitializedNPCHosts.Contains(host);
		}

		public void InitializeNPCs(NPCHost host)
		{
			if (!HasInitializedNPCs(host)) {
				InitializeNPCsImpl(host);
				InitializedNPCHosts.Add(host);
			}
		}

		protected abstract void InitializeNPCsImpl(NPCHost host);





		protected NPCData MakeNPC(NPCHost host, string name)
		{
			var npc = new NPCData(name);
			RegisterNPC(host, npc);
			return npc;
		}

		protected NPCGroup MakeNPCGroup(NPCHost host, string name, float propagationStrength, params NPCData[] npcs)
		{
			return MakeNPCGroup(host, name, propagationStrength, npcs.AsEnumerable());
		}

		protected NPCGroup MakeNPCGroup(NPCHost host, string name, float propagationStrength, IEnumerable<NPCData> npcs)
		{
			var group = new NPCGroup(name, propagationStrength, npcs == null ? null : npcs.Where(x => x != null));
			RegisterNPCGroup(host, group);
			return group;
		}

		protected void RegisterNPC(NPCHost host, NPCData npc)
		{
			host.AllNPCs.Add(npc);
			host.RegisterNPCSet(npc);
		}

		protected void RegisterNPCGroup(NPCHost host, NPCGroup group)
		{
			host.RegisterNPCSet(group);
		}
	}
}

//*/