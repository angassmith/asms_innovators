﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using UnityEngine;
using UObject = UnityEngine.Object;

namespace RevolitionGames.NPCs
{
	public class CSharpNPCProvider : NPCProvider
	{
		protected override void InitializeNPCsImpl(NPCHost host)
		{
			var liam = MakeNPC(host, NPCCodeNames.Liam);
			var ami = MakeNPC(host, NPCCodeNames.Ami);
			var sam = MakeNPC(host, NPCCodeNames.Sam);

            var jack = MakeNPC(host, NPCCodeNames.Jack);
            var dad = MakeNPC(host, NPCCodeNames.Dad);

			var teacher = MakeNPC(host, NPCCodeNames.HomeroomTeacher);
			var musicTeacher = MakeNPC(host, NPCCodeNames.MusicTeacher);
            var peTeacher = MakeNPC(host, NPCCodeNames.PETeacher);
			var principal = MakeNPC(host, NPCCodeNames.Principal);

			var students = MakeNPCGroup(host, NPCCodeNames.NPCGroups.Students, 0.1f, liam, ami, sam);
			var staff = MakeNPCGroup(host, NPCCodeNames.NPCGroups.Staff, 0.1f, teacher, musicTeacher, peTeacher, principal);
		}
	}
}

//*/