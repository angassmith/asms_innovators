﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using RevolitionGames.Likeability;
using UnityEngine;

namespace RevolitionGames.NPCs
{
	public abstract class NPCSet
	{
		//	private static Dictionary<string, NPCSet> _all;
		//	public static ReadOnlyDictionary<string, NPCSet> All { get; private set; }
		//	
		//	static NPCSet()
		//	{
		//		_all = new Dictionary<string, NPCSet>();
		//		All = new ReadOnlyDictionary<string, NPCSet>(_all);
		//	}


		public string Name { get; private set; }

		public abstract NPCSetLikeability Likeability { get; }

		public NPCSet(string name)
		{
			if (string.IsNullOrEmpty(name)) throw new ArgumentException("Cannot be null or empty", "name");

			this.Name = name;
		}
	}
}

//*/