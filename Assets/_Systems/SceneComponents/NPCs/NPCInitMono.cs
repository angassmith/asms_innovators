﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using RevolitionGames.Interaction;
using RevolitionGames.Likeability;
using RevolitionGames.Pathfinding;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.NPCs
{
	/// <summary>
	/// Provides references to the different components of an npc that need to become
	/// part of the <see cref="NPCData"/>. Destroyed upon creation of the <see cref="NPCData"/>.
	/// </summary>
	[RequireComponent(typeof(NPCInteract))]
	[RequireComponent(typeof(NPCPathfinder))]
	public class NPCInitMono : MonoBehaviour
	{
		[SerializeField] //And edit in inspector
		private CodeNameRef _NPCCodeName;
		public CodeNameRef NPCCodeName { get { return _NPCCodeName; } }

		[SerializeField]
		private NPCInteract _NPCInteract;
		public NPCInteract NPCInteract { get { return _NPCInteract; } }

		[SerializeField]
		private NPCPathfinder _pathfinder;
		public NPCPathfinder Pathfinder { get { return _pathfinder; } }

		//	[NonSerialized]
		//	private NPCData _data;
		//	public NPCData NPCData { get { return _data; } }

		//	/// <summary>
		//	/// This should only be called by <see cref="NPCs.NPCData"/>.
		//	/// </summary>
		//	internal void SetNPCData(NPCData npcData) {
		//		_data = npcData;
		//	}

		private void Start()
		{
			//	if (this.NPCData == null)
			//	{
			//		Debug.LogError("NPCMono is not attached to an NPC (NPCMono.NPC is null)", this);
			//	}
		}
	}
}

//*/