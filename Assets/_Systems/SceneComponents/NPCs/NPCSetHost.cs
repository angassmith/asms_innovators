﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.NPCs
{
	public class NPCHost
	{
		private Dictionary<string, NPCSet> _allNPCSets;
		public ReadOnlyDictionary<string, NPCSet> AllNPCSets { get; private set; }

		public NPCGroup AllNPCs { get; private set; }

		public NPCHost(NPCGroup allNPCsGroup, NPCProvider npcProvider)
		{
			if (allNPCsGroup == null) throw new ArgumentNullException("allNPCsGroup");
			if (npcProvider == null) throw new ArgumentNullException("provider");

			this._allNPCSets = new Dictionary<string, NPCSet>();
			this.AllNPCSets = new ReadOnlyDictionary<string, NPCSet>(this._allNPCSets);

			this.AllNPCs = allNPCsGroup;
			this.RegisterNPCSet(allNPCsGroup);

			npcProvider.InitializeNPCs(this);

			//	foreach (var set in npcSets)
			//	{
			//		try {
			//			AllNPCSets.Add(set.Name, set);
			//		} catch (ArgumentException ex) {
			//			throw new InvalidOperationException("The name '" + set.Name + "' is used by multiple NPCSets", ex);
			//		}
			//	}
		}

		public void RegisterNPCSet(NPCSet set)
		{
			NPCSet found;
			if (_allNPCSets.TryGetValue(set.Name, out found))
			{
				if (ReferenceEquals(set, found)) {
					return;
				} else {
					throw new InvalidOperationException("A different NPCSet with name '" + set.Name + "' is already registered");
				}
			}
			else
			{
				_allNPCSets.Add(set.Name, set);
			}
		}
	}
}

//*/