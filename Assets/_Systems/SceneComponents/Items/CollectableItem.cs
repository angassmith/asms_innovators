﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;
using RevolitionGames.Interaction;
using RevolitionGames.Inventory;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.BehaviourOverriding;
using UnityEngine;

namespace RevolitionGames.SceneComponents
{
	public class CollectableItem : Interactable<Player>, IBehaviourOverridable<CollectableItemInteractBehaviour>
	{
		public const float ItemMaxInteractAngle = 45; //Degrees //TODO: Find a good value
		public const float ItemMaxInteractHorizontalDist = 2; //TODO: Find a good value
		public const float ItemMaxInteractYDiff = 2; //TODO: Find a good value

		private readonly CollectableItemInteractBehaviour DefaultBehaviour = new CollectableItemInteractBehaviour("<default>", false, DiscardAction.None);

		[SerializeField] //And edit in inspector
		private string _collectableItemID = ""; //Null/empty is invalid, but makes behaviour overrides impossible, which effectively validates this (so no need to validate here (in the editor))
		public string CollectableItemID {
			get { return _collectableItemID; }
		}
		string IBehaviourOverridable<CollectableItemInteractBehaviour>.CodeName {
			get { return _itemCodeName; }
		}

        [SerializeField] //And edit in inspector
		private string _itemCodeName;
		[NonSerialized]
		private ItemDef _itemDef;
		public ItemDef ItemDef {
			get {
                // Use cached ItemDef if it exists
                if (_itemDef != null) {
                    return _itemDef;
                }

                // Otherwise, get the ItemDef from the inventory database
                if (String.IsNullOrEmpty(_itemCodeName)) {
                    Debug.LogError("CollectableItem " + gameObject.name + " has not been set an item codename");
                    return null;
                } else {
                    _itemDef = PlayModeStage.Current.PlayerUI.Inventory.ItemDatabase.GetByCodeName(_itemCodeName);
                    if (_itemDef == null) {
                        Debug.LogError("CollectableItem " + gameObject.name + ": There is no item with codename " + _itemCodeName + " in the inventory database");
                    }
                    return _itemDef;
                }
            }
		}

		[SerializeField] //And edit in inspector
		private int _count;
		public int Count { get { return _count; } }

		private CollectableItemInteractBehaviour __currentBehaviour;
		private CollectableItemInteractBehaviour CurrentBehaviour {
			get {
				if (__currentBehaviour == null) {
					IActiveStoryPart dummy;
					__currentBehaviour = PlayModeStage.Current.Story.BehaviourOverrideProvider.SelectCurrentBehaviour(
						target: this,
						context: out dummy,
						fallback: DefaultBehaviour
					);
				}
				return __currentBehaviour;
			}
		}

		private Renderer _renderer;
		private Renderer Renderer {
			get {
				if (_renderer == null) _renderer = this.GetComponentInChildren<Renderer>();
				return _renderer;
			}
		}

		//TODO: Disable or create CollectableItems at certain points in the game
		//This is needed eventually but is difficult

		public override bool CanTakeFocus { get { return gameObject.activeInHierarchy; } } //TODO: Decide on this

		public override float MaxInteractAngle { get { return ItemMaxInteractAngle; } }
		public override float MaxInteractHorizontalDist { get { return ItemMaxInteractHorizontalDist; } }
		public override float MaxInteractYDiff { get { return ItemMaxInteractYDiff; } }


		public event EventHandler<ItemEventArgs> Collected;
		public static event EventHandler<ItemEventArgs> AnyItemCollected;


		protected override void StartImpl()
		{
			base.StartImpl();
			PlayModeStage.Current.Story.ProgressChanged += (sender, e) => { __currentBehaviour = null; }; //Never unsubscribe

            if (String.IsNullOrEmpty(_itemCodeName)) {
                Debug.LogError("CollectableItem " + gameObject.name + " has not been set an item codename");
            }
		}

        private void Update()
        {

        }

		protected override void OnEnterInteractRegion(InteractDetector<Player> detector)
		{
			return;
		}

		protected override void OnExitInteractRegion(InteractDetector<Player> detector)
		{
			return;
		}

		protected override void OnGotFocus(InteractDetector<Player> detector)
		{
            if (CurrentBehaviour.AllowCollection)
            {
				var iconPref = PlayModeStage.Current.PlayerUI.Inventory.ItemDatabase.GetByCodeName(this._itemCodeName).Icon;
                PlayModeStage.Current.PlayerUI.DisplayInteractIcon(this, detector, iconPref);
            }
		}

		protected override void OnLostFocus(InteractDetector<Player> detector)
		{
            PlayModeStage.Current.PlayerUI.HideInteractIcon();
		}

		protected override void OnInteractTriggered(InteractDetector<Player> detector)
		{
			if (this.HasInteractFocus(detector))
			{
				if (CurrentBehaviour.AllowCollection)
				{
					Collect(CurrentBehaviour);
				}
			}
		}

		private void Collect(CollectableItemInteractBehaviour currentBehaviour)
		{
			// Add Count items of type ItemDef to inventory
            if (this.ItemDef != null) {
                PlayModeStage.Current.PlayerUI.Inventory.AddItem(this.ItemDef, Count);
            }

			this._count = 0;
			DiscardCollectableItem(currentBehaviour: currentBehaviour);

			AudioSource audio = FindObjectOfType<AudioSource>();
			var clip = PlayModeStage.Current.PlayerUI.ItemCollectSound;
			if (clip != null) {
				audio.clip = clip;
				audio.Play();
			}

			var e = new ItemEventArgs(this.ItemDef);
			Collected.Fire(this, e);
			AnyItemCollected.Fire(this, e);
		}

		private void DiscardCollectableItem(CollectableItemInteractBehaviour currentBehaviour)
		{
			switch (currentBehaviour.GameObjectDiscardAction)
			{
				case DiscardAction.None:
					break;
				case DiscardAction.Disable:
					this.gameObject.SetActive(false);
					break;
				case DiscardAction.Destroy:
					//Disable now (I assume), then destroy at the end of the frame
					this.gameObject.SetActive(false);
					Destroy(this.gameObject);
					break;
			}
		}

		public override Vector3 GetInteractIconPosition(InteractDetector detector)
		{
			return new Vector3(
				Renderer.bounds.center.x,
				Renderer.bounds.center.y + (Renderer.bounds.size.y / 2) + StandardInteractIconYOffset,
				Renderer.bounds.center.z
			);
		}
	}

	public class CollectableItemInteractBehaviour : BehaviourOverride
	{
		public bool AllowCollection { get; private set; }

		public DiscardAction GameObjectDiscardAction { get; private set; }

		public CollectableItemInteractBehaviour(string targetCodeName, bool allowCollection, DiscardAction gameObjectDiscardAction)
			: base(targetCodeName)
		{
			this.AllowCollection = allowCollection;
			this.GameObjectDiscardAction = gameObjectDiscardAction;
		}


	}
}

//*/