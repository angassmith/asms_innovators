﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;
using RevolitionGames.NPCs;
using RevolitionGames.Utilities.Alex;

namespace RevolitionGames.Pathfinding
{
	/// <summary>
	/// Navigates an NPC around the level.
	/// </summary>
	[RequireComponent(typeof(NPCInitMono))]
	[RequireComponent(typeof(NavMeshAgent))]
	[DisallowMultipleComponent]
	public class NPCPathfinder : MonoBehaviour
	{
		public const float WalkTargetAccuracy = 0.1f;

		public Animator Anim { get; private set; }

		/// <summary>
		/// The NPC's nav mesh agent.
		/// </summary>
		public NavMeshAgent Agent { get; private set; }

		private MovementState _state;
		public MovementState State {
			get { return _state; }
			private set {
				var oldState = _state;
				oldState.Finished -= OnStateFinished;
				_state = value;
				_state.Finished += OnStateFinished;

				StateChanged.Fire(this, new ValueChangedEventArgs<MovementState>(oldState, value));
			}
		}
		public event EventHandler<ValueChangedEventArgs<MovementState>> StateChanged;

		public INPCWalkTargetProvider WalkTargetProvider { get; private set; }

		public INPCWalkModifier WalkModifier { get; private set; }

		private void Awake()
		{
		}

		/// <summary>
		/// Sets up the NavMeshAgent and the NPC starts walking to the nearest waypoint.
		/// </summary>
		private void Start()
		{
			Agent = GetComponent<NavMeshAgent>();
			Anim = GetComponent<Animator>();

			var npcInitMono = GetComponent<NPCInitMono>();
			Debug.Log("#530: " + npcInitMono);

			WalkTargetProvider = new NPCNearbyWalkTargetProvider(npcInitMono, Agent, radius: 10f);
			WalkModifier = new StationaryRotateNPCWalkModifier(pathfinder: this);

			_state = new MovementState.Idle(pathfinder: this, duration: UnityEngine.Random.Range(0.5f, 3f));
			_state.Finished += OnStateFinished;
		}

		private void OnStateFinished(object sender, EventArgsTuple<MovementState> e)
		{
			this.State = e.Arg1;
		}

		/// <summary>
		/// Updates behaviour according to movement state.
		/// </summary>
		private void Update()
		{
			State.Update();
		}

		/// <summary>
		/// Makes the NPC talk to the specified transform.
		/// </summary>
		/// <param name="talkingTo">The transform of the object to talk to.</param>
		public void TalkStart(Transform talkTarget)
		{
			State = new MovementState.Talking(pathfinder: this, talkTarget: talkTarget);
		}

		/// <summary>
		/// Transitions the NPC back from the talking state.
		/// </summary>
		public void TalkEnd()
		{
			State = new MovementState.Idle(pathfinder: this, duration: UnityEngine.Random.Range(0.5f, 2f));
		}

		

		/// <summary>
		/// The NPC has collided with something.
		/// </summary>
		/// <param name="col">The collision details.</param>
		void OnCollisionEnter(Collision col) // when the NPC collides with an object
		{
			// TODO work out what handling is appropriate
			if (col.gameObject.tag == "Player") {
				Debug.Log("Player collided with NPC " + gameObject.name);
			}
			else {
				//Debug.Log("NPC " + gameObject.name + " collided with " + col.collider.gameObject.name);
			}
		}


		#region Utils

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion



		public abstract class MovementState
		{
			private readonly NPCPathfinder _pathfinder;
			public NPCPathfinder Pathfinder { get { return _pathfinder; } }

			public event EventHandlerTuple<MovementState> Finished;
			protected void Finish(MovementState next) { Finished.Fire(this, next); }

			public MovementState(NPCPathfinder pathfinder)
			{
				ThrowIfArgNull(pathfinder, "pathfinder");

				this._pathfinder = pathfinder;

				pathfinder.StateChanged += OnStateChanged;
			}

			private void OnStateChanged(object sender, ValueChangedEventArgs<MovementState> e)
			{
				if (ReferenceEquals(e.OldValue, this) && !ReferenceEquals(e.NewValue, this))
				{
					Pathfinder.StateChanged -= OnStateChanged;
					Discarded();
				}
			}

			public abstract void Update();

			protected virtual void Discarded() { }


			public class Idle : MovementState
			{
				public float StartTime { get; private set; }
				public float Duration { get; private set; }
				public float EndTime { get { return StartTime + Duration; } }

				public Idle(NPCPathfinder pathfinder, float duration) : base(pathfinder)
				{
					this.StartTime = Time.time;
					this.Duration = duration;

					if (Pathfinder.Anim != null) {
						Pathfinder.Anim.SetBool("walk", false);
						Pathfinder.Anim.SetBool("turnRight", false);
						Pathfinder.Anim.SetBool("turnLeft", false);
					}
				}

				public override void Update()
				{
					if (Time.time > this.EndTime)
					{
						NPCWalkTarget nextTarget = Pathfinder.WalkTargetProvider.SelectTarget(
							currentLocation: Pathfinder.transform.position
						);
						Debug.Log("nextTarget.Location: " + nextTarget.Location, Pathfinder);

						Finish(next: new Walking(Pathfinder, nextTarget.Location, nextTarget.ContinueWithoutIdling));
					}
				}
			}

			public class Walking : MovementState
			{
				public Vector3 Target { get; private set; }
				public bool ContinueWithoutIdling { get; private set; }
				public INPCWalkModifier Modifier { get; private set; }

				public Walking(NPCPathfinder pathfinder, Vector3 target, bool continueWithoutIdling)
					: base(pathfinder)
				{
					this.Target = target;
					this.ContinueWithoutIdling = continueWithoutIdling;

					//Unrelated comment (by Peter I think):
					// Currently the y position of the transform is used for the y position of the destination
					//TODO refactor waypoints to include their y position

					Pathfinder.Agent.destination = target;
					if (Pathfinder.Anim != null) {
						Pathfinder.Anim.SetBool("walk", true);
						Pathfinder.Anim.SetBool("turnRight", false);
						Pathfinder.Anim.SetBool("turnLeft", false);
					}
				}

				public override void Update()
				{
					if (Pathfinder.Agent.remainingDistance < WalkTargetAccuracy)
					{
						if (ContinueWithoutIdling)
						{
							NPCWalkTarget nextTarget = Pathfinder.WalkTargetProvider.SelectTarget(
								currentLocation: Pathfinder.transform.position
							);

							Finish(next: new Walking(this.Pathfinder, nextTarget.Location, nextTarget.ContinueWithoutIdling));
						}
						else
						{
							Finish(next: new Idle(this.Pathfinder, duration: UnityEngine.Random.Range(1f, 4f)));
						}
					}
					else
					{
						Pathfinder.WalkModifier.Update(walkAnimIfNotTurning: true);
					}
				}
			}

			public class Talking : MovementState
			{
				public Transform TalkTarget { get; private set; }

				public Talking(NPCPathfinder pathfinder, Transform talkTarget) : base(pathfinder)
				{
					ThrowIfArgNull(talkTarget, "talkTarget");
					this.TalkTarget = talkTarget;

					//Stop movement
					Pathfinder.Agent.destination = Pathfinder.transform.position;

					if (Pathfinder.Anim != null) {
						Pathfinder.Anim.SetBool("walk", false);
						Pathfinder.Anim.SetBool("turnRight", false);
						Pathfinder.Anim.SetBool("turnLeft", false);
					}
				}

				public override void Update()
				{
					Quaternion desiredRotation = Quaternion.LookRotation(
						TalkTarget.position - Pathfinder.transform.position
					);

					Pathfinder.transform.rotation = Quaternion.RotateTowards(
						from: Pathfinder.transform.rotation,
						to: desiredRotation,
						maxDegreesDelta: Time.deltaTime * Pathfinder.Agent.angularSpeed
					);

					Pathfinder.WalkModifier.Update(walkAnimIfNotTurning: false);
				}
			}
		}

		#region old

		//	/// <summary>
		//	/// Makes the NPC walk to the next waypoint.
		//	/// Sets state to walking.
		//	/// </summary>
		//	public void WalkToNextWaypoint() {
		//		// If there is no waypoint set, use the nearest
		//		if (currentWP == null) {
		//			SetCurrentWaypointToNearest();
		//		}
		//		// Currently the y position of the transform is used for the y position of the destination
		//		//TODO refactor waypoints to include their y position
		//		agent.destination = new Vector3(currentWP.coordinates.x, transform.position.y, currentWP.coordinates.y);
		//		if (anim != null) {
		//			anim.SetBool("walk", true);
		//		}
		//	
		//		//Debug.Log(gameObject.name + ": destination set to " + agent.destination);
		//		npcMovementState = MovementState.Walking;
		//	}

		//	/// <summary>
		//	/// Makes the NPC idle for an amount of time.
		//	/// </summary>
		//	/// <param name="time">The time to remain idle.</param>
		//	public void Idle(float time) {
		//		SetStateForTime(MovementState.Idle, time);
		//		if (anim != null) {
		//			anim.SetBool("walk", false);
		//		}
		//	}

		//	/// <summary>
		//	/// Makes the NPC talk to the specified transform.
		//	/// </summary>
		//	/// <param name="talkingTo">The transform of the object to talk to.</param>
		//	public void TalkStart(Transform talkingTo) {
		//		if (anim != null) {
		//			anim.SetBool("walk", false);
		//		}
		//	
		//		// Set the destination to the current position
		//		agent.destination = this.transform.position;
		//	
		//		talkTarget = talkingTo;
		//		npcMovementState = MovementState.Talking;
		//	}

		//	/// <summary>
		//	/// Transitions the NPC back into the walking state.
		//	/// </summary>
		//	public void TalkEnd() {
		//		WalkToNextWaypoint();
		//	}

		//	/// <summary>
		//	/// Sets the NPC to the specified state for an amount of time.
		//	/// </summary>
		//	/// <param name="state">The movement state to set to.</param>
		//	/// <param name="time">The time to remain in the state for.</param>
		//	private void SetStateForTime(MovementState state, float time) {
		//		if (time <= 0) {
		//			time = MINIMUM_TIME;
		//		}
		//		timeRemainingInState = time;
		//		npcMovementState = state;
		//	}

		//	/// <summary>
		//	/// Sets the current waypoint to the nearest waypoint.
		//	/// </summary>
		//	private void SetCurrentWaypointToNearest() {
		//	
		//		float bestDist = float.PositiveInfinity;
		//		Waypoint bestWP = null;
		//	
		//		// Set up the waypoints in the scene and verify there is at least one
		//		if (!npcPathfinderHost.HasSetup) npcPathfinderHost.SetupWaypoints();
		//		if (npcPathfinderHost.waypoints == null || npcPathfinderHost.waypoints.Count == 0) throw new InvalidOperationException("No waypoints found in scene");
		//	
		//		// Find the closest waypoint
		//		foreach (Waypoint wp in npcPathfinderHost.waypoints) {
		//			float dist = Vector2.Distance(wp.coordinates, General.Vector3ToVector2XZ(this.transform.position));
		//			if (dist < bestDist) {
		//				bestDist = dist;
		//				bestWP = wp;
		//			}
		//		}
		//	
		//		// Use the closest waypoint as the current waypoint
		//		if (bestWP != null) {
		//			currentWP = bestWP;
		//		}
		//		// No waypoints were found, log an exception
		//		// and set the NPC to Idle
		//		else {
		//			Debug.LogException(new System.Exception("The closest waypoint is null"));
		//			Idle(float.MaxValue);
		//		}
		//	}

		//	/// <summary>
		//	/// Sets the next waypoint and transitions the NPC to idle or starts the NPC
		//	/// walking to the next waypoint, as appropraite for the waypoint type.
		//	/// </summary>
		//	private void ReachedWaypoint() {
		//		//Debug.Log(gameObject.name + ": Reached waypoint " + currentWP.coordinates);
		//	
		//		//Find and set next waypoint
		//		//Debug.Log("currentWP: " + currentWP);
		//		if (currentWP != null) {
		//			//Debug.Log("currentWP.adjacentWPs: " + currentWP.adjacentWPs);
		//			if (currentWP.adjacentWPs != null) {
		//				//Debug.Log("currentWP.adjacentWPs.Count: " + currentWP.adjacentWPs.Count);
		//			}
		//		}
		//		Waypoint nextWP = currentWP.adjacentWPs[UnityEngine.Random.Range(0, currentWP.adjacentWPs.Count)];
		//	
		//		// If we reached a guiding point, set the next waypoint so it is not the previous waypoint
		//		if (currentWP.type == Waypoint.WayPointType.Guidingpoint) {
		//			int i = 0;
		//			while (ReferenceEquals(nextWP, prevWP) && i < 20) {
		//				nextWP = currentWP.adjacentWPs[UnityEngine.Random.Range(0, currentWP.adjacentWPs.Count)];
		//				i++;
		//			}
		//		}
		//	
		//		// Update the previous and current waypoints
		//		prevWP = currentWP;
		//		currentWP = nextWP;
		//	
		//		// If the reached waypoint was type Waypoint, go into idle state
		//		if (prevWP.type == Waypoint.WayPointType.Waypoint) {
		//			Idle(5f);
		//		}
		//		// Otherwise the reached waypoint was a guiding point, start walking to the next waypoint
		//		else {
		//			WalkToNextWaypoint();
		//		}
		//	}

		#endregion
	}

}
