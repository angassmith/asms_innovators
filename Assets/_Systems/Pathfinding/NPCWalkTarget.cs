﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.Pathfinding
{
	public struct NPCWalkTarget
	{
		public readonly Vector3 Location;
		public readonly bool ContinueWithoutIdling;

		public NPCWalkTarget(Vector3 location, bool continueWithoutIdling)
		{
			this.Location = location;
			this.ContinueWithoutIdling = continueWithoutIdling;
		}
	}
}

//*/