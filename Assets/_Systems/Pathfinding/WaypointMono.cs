﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RevolitionGames.Utilities.Alex;

// --- WaypointMono.cs ---
//Attach this to dummy waypoint GameObjects

public class WaypointMono : MonoBehaviour
{
	public List<GameObject> AdjacentWPs = new List<GameObject>();
	public Waypoint Waypoint;
	public WayPointType Type;

	public void Start() { }
	public void Update() { }

	public void SetupWaypoint()
	{
		Waypoint = new Waypoint(new HVector2(x: transform.position.x, z: transform.position.z), Type);

		//Remove null adjacent waypoints
		List<GameObject> newAdj = new List<GameObject>();
		foreach (GameObject adj in AdjacentWPs)
		{
			if (adj != null) { newAdj.Add(adj); }
		}
		AdjacentWPs = newAdj;
	}

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
	{
        // this shows the connectgions between waypoints in the Scene view
		WaypointEditor.ShowWaypointConnections();
	}
#endif

}