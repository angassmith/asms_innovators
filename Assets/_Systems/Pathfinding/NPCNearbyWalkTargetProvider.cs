﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.NPCs;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.AI;

namespace RevolitionGames.Pathfinding
{
	public class NPCNearbyWalkTargetProvider : INPCWalkTargetProvider
	{
		public NPCInitMono NPC { get; private set; }
		public NavMeshAgent Agent { get; private set; }
		public float Radius { get; private set; }
		public const int RadiusExpandAttempts = 1000;

		public NPCNearbyWalkTargetProvider(NPCInitMono npc, NavMeshAgent agent, float radius)
		{
			if (npc == null) throw new ArgumentNullException("npc");
			if (agent == null) throw new ArgumentNullException("agent");

			this.NPC = npc;
			this.Agent = agent;
			this.Radius = radius;
		}

		public NPCWalkTarget SelectTarget(Vector3 currentLocation)
		{
			var navMesh = (GameObject)Agent.navMeshOwner;
			var randomPosXY = UnityEngine.Random.insideUnitCircle; //Need (x,z) but this gives (x,y) so have to convert

			var idealTarget = currentLocation + (new Vector3(x: randomPosXY.x, y: 0, z: randomPosXY.y) * Radius);

			for (int i = 1; i <= RadiusExpandAttempts; i++)
			{
				var navmeshSearchRadius = Radius * i;

				NavMeshHit hit;
				//	var foundHit = NavMesh.FindClosestEdge(
				//		sourcePosition: idealTarget.ToVector3(),
				//		areaMask: int.MaxValue,
				//		hit: out hit
				//	);

				var foundHit = NavMesh.SamplePosition(
					sourcePosition: idealTarget,
					maxDistance: Radius * 2,
					areaMask: int.MaxValue,
					hit: out hit
				);

				if (foundHit) {
					return new NPCWalkTarget(
						hit.position,
						continueWithoutIdling: ThreadSafeRandom.Random.Next(0, 100) > 80
					);
				}
			}

			throw new InvalidOperationException("No position nearby on nav mesh. Current location:" + currentLocation + ", search origin: " + idealTarget);
		}
	}
}

//*/