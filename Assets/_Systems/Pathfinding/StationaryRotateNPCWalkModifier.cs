﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.AI;

namespace RevolitionGames.Pathfinding
{
	public class StationaryRotateNPCWalkModifier : INPCWalkModifier
	{
		public NPCPathfinder Pathfinder { get; private set; }
		private Vector3 prevPos;
		private Quaternion prevRotation;

		private const float RotationThreshold = 0.01f;
		private const float MaxMovementReset = 0.5f;

		public StationaryRotateNPCWalkModifier(NPCPathfinder pathfinder)
		{
			if (pathfinder == null) throw new ArgumentNullException("pathfinder");

			this.Pathfinder = pathfinder;
		}

		public void Update(bool walkAnimIfNotTurning) //It's fine for this to be public as extra calls do nothing
		{
			//Convert rotations to 2d (x, z) vectors
			//Source: http://answers.unity3d.com/questions/26783/how-to-get-the-signed-angle-between-two-quaternion.html
			var prevForwardXZ = new HVector2(prevRotation * Vector3.forward);
			var forwardXZ = new HVector2(Pathfinder.transform.rotation * Vector3.forward);

			//Convert forward vectors to angles (same source)
			var prevForwardAngle = Mathf.Atan2(prevForwardXZ.X, prevForwardXZ.Z) * Mathf.Rad2Deg;
			var forwardAngle = Mathf.Atan2(forwardXZ.X, forwardXZ.Z) * Mathf.Rad2Deg;

			//Get signed difference between these angles (this is why Quaternion.Angle
			//and HVector2.Angle aren't used)
			var xzRotationDelta = Mathf.DeltaAngle(prevForwardAngle, forwardAngle); //In degrees

			//	Debug.Log(
			//		"abs(xzRotationDelta): " + Mathf.Abs(xzRotationDelta).ToString("n8")
			//		+ ", Time.deltaTime: " + Time.deltaTime.ToString("n8")
			//		+ ", multiplied: " + (xzRotationDelta * Time.deltaTime).ToString("n8")
			//	);

			//Don't do anything if the npc moved more than MaxMovementReset - it was either pushed
			//by the player, just spawned (was at (0,0,0) previously), or teleported etc
			if (Vector3.Distance(prevPos, this.Pathfinder.transform.position) > MaxMovementReset) {
				return;
			}

			//If the npc is rotating, stop it from moving
			if (Mathf.Abs(xzRotationDelta) * Time.deltaTime > RotationThreshold) {
				Pathfinder.transform.position = prevPos;
			}

			//Set the relevant animations
			if (Pathfinder.Anim != null)
			{
				bool existingLogWarningsSetting = Pathfinder.Anim.logWarnings;
				Pathfinder.Anim.logWarnings = false;

				if (xzRotationDelta * Time.deltaTime > RotationThreshold)
				{
					Pathfinder.Anim.SetBool("turnRight", true);
					Pathfinder.Anim.SetBool("turnLeft", false);
					Pathfinder.Anim.SetBool("walk", false);
				}
				else if (xzRotationDelta * Time.deltaTime < -RotationThreshold)
				{
					Pathfinder.Anim.SetBool("turnRight", false);
					Pathfinder.Anim.SetBool("turnLeft", true);
					Pathfinder.Anim.SetBool("walk", false);
				}
				else
				{
					Pathfinder.Anim.SetBool("turnRight", false);
					Pathfinder.Anim.SetBool("turnLeft", false);
					Pathfinder.Anim.SetBool("walk", walkAnimIfNotTurning);
				}

				Pathfinder.Anim.logWarnings = existingLogWarningsSetting;
			}

			this.prevPos = Pathfinder.transform.position;
			this.prevRotation = Pathfinder.transform.rotation;
		}
	}
}

//*/