﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.NPCs;
using UnityEngine;
using UObject = UnityEngine.Object;

public class WaypointHost : MonoBehaviour
{
	private readonly List<Waypoint> _waypoints;
	public ReadOnlyCollection<Waypoint> Waypoints { get; private set; }

	public bool HasSetup { get; private set; }

	public WaypointHost()
	{
		this._waypoints = new List<Waypoint>();
		this.Waypoints = this._waypoints.AsReadOnly();
	}

    void Awake() {
        SetupWaypoints();
    }

	public bool SetupWaypoints()
	{
		if (HasSetup) return false;

		WaypointMono[] allWaypointMonos = FindObjectsOfType<WaypointMono>();

		//Setup Waypoint properties of WaypointMonos
		foreach (WaypointMono waypointMono in allWaypointMonos)
		{
			waypointMono.SetupWaypoint();
		}

		//Setup waypoints list, and destroy (or with modification disable) way-point gameobjects
		foreach (WaypointMono waypointMono in allWaypointMonos)
		{
			Waypoint waypoint = waypointMono.Waypoint;
			foreach (GameObject adj in waypointMono.AdjacentWPs)
			{
				WaypointMono adjMono = adj.GetComponent<WaypointMono>();
				if (adjMono != null)
				{
					waypoint.AddAdjacents(adjMono.Waypoint);
				}
			}
			_waypoints.Add(waypoint);
			Destroy(waypointMono.gameObject);
			//waypointMono.GetComponent<BoxCollider>().enabled = false;
		}

		HasSetup = true;
		return true;
	}
}
