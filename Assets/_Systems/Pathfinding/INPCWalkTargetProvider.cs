﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.Pathfinding
{
	public interface INPCWalkTargetProvider
	{
		NPCWalkTarget SelectTarget(Vector3 currentLocation);
	}
}

//*/