﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Pathfinding
{
	public interface INPCWalkModifier
	{
		void Update(bool walkAnimIfNotTurning);
	}
}

//*/