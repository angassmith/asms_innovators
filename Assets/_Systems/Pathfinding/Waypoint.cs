﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.NPCs;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UObject = UnityEngine.Object;

// --- Waypoint.cs ---
//Put this somewhere in the scripts folder

//This class contains properties of the waypoint, but is not a MonoBehaviour

public enum WayPointType
{
	Waypoint,
	Guidingpoint,
}

public class Waypoint
{
	public HVector2 Coordinates { get; private set; }
	public WayPointType Type { get; private set; }

	private readonly List<Waypoint> _adjacentWPs;
	public ReadOnlyCollection<Waypoint> AdjacentWPs { get; private set; }


	public Waypoint(HVector2 coordinates, WayPointType type)
	{
		this.Coordinates = coordinates;
		this.Type = type;
		this._adjacentWPs = new List<Waypoint>();
		this.AdjacentWPs = this._adjacentWPs.AsReadOnly();
	}

	/// <summary>Adds some waypoints to this waypoint's adjacent waypoint's list. Also adds this waypoint to the added waypoints' adjacent waypoints lists.
	/// <para />
	/// Doesn't add any waypoints that are already in the list</summary>
	public void AddAdjacents(params Waypoint[] adjacents)
	{
		foreach (Waypoint adjacent in adjacents)
		{
			if (!_adjacentWPs.Contains(adjacent))
			{
				_adjacentWPs.Add(adjacent);
			}
			if (!adjacent._adjacentWPs.Contains(this))
			{
				adjacent._adjacentWPs.Add(this);
			}
		}
	}
}