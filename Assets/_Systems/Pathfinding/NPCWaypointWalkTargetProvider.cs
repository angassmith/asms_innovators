﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.NPCs;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UObject = UnityEngine.Object;

namespace RevolitionGames.Pathfinding
{
	public class NPCWaypointWalkTargetProvider : INPCWalkTargetProvider
	{
		public NPCInitMono NPC { get; private set; }
		
		public Waypoint CurrentWaypoint { get; private set; }

		private readonly WaypointHost waypointHost;

		public NPCWaypointWalkTargetProvider(NPCInitMono npc)
		{
			if (npc == null) throw new ArgumentNullException("npc");
			this.NPC = npc;

			var npcPos = new HVector2(npc.transform.position);

			waypointHost = UObject.FindObjectOfType<WaypointHost>();
			if (waypointHost == null) {
				Debug.LogError("No " + typeof(WaypointHost).Name + " found in scene.");
				CurrentWaypoint = new Waypoint(npcPos, WayPointType.Waypoint);
				return;
			}

			if (!waypointHost.HasSetup) waypointHost.SetupWaypoints();
			if (waypointHost.Waypoints.Count == 0) {
				Debug.LogError("No waypoints found in scene", waypointHost);
				CurrentWaypoint = new Waypoint(npcPos, WayPointType.Waypoint);
				return;
			}

			//Find the closest waypoint
			//Enumerable.Min() returns a number rather that the actual item so we have to use Aggregate() instead
			var closestWP = waypointHost.Waypoints.Aggregate(
				(wp1, wp2) => {
					var wp1Dist = HVector2.Distance(wp1.Coordinates, npcPos);
					var wp2Dist = HVector2.Distance(wp2.Coordinates, npcPos);
					return wp1Dist < wp2Dist ? wp1 : wp2; //Note: Ties are irrelevant
				}
			);

			this.CurrentWaypoint = closestWP;
		}

		public NPCWalkTarget SelectTarget(Vector3 currentLocation)
		{
			//This method is intended to be called only once the waypoint has been reached,
			//but as it's public it should handle other uses, hence this check
			if (HasReachedWaypoint())
			{
				Debug.Log("Reached the waypoint", NPC);
				//If there are adjacent waypoints, pick one at random
				if (CurrentWaypoint.AdjacentWPs.Count > 0) {
					Debug.Log("Has adjacent waypoints", NPC);
					CurrentWaypoint = CurrentWaypoint.AdjacentWPs.RandomElement(ThreadSafeRandom.Random);
				}
			}

			return new NPCWalkTarget(
				location: CurrentWaypoint.Coordinates.ToVector3(y: NPC.transform.position.y), //TODO: Make waypoints have y coordinates and use that
				continueWithoutIdling: CurrentWaypoint.Type == WayPointType.Guidingpoint
			);
		}

		private bool HasReachedWaypoint()
		{
			var npcPos = new HVector2(NPC.transform.position);
			var wpPos = CurrentWaypoint.Coordinates;
			return HVector2.Distance(npcPos, wpPos) < NPCPathfinder.WalkTargetAccuracy;
		}
	}
}

//*/