﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Pathfinding
{
	public class WanderNPCWalkModifier : INPCWalkModifier
	{
		//This might not actaully be needed
		//When the npc doesn't have a specific place to walk to, it already effectively wanders
		//When it does, not wandering is reasonable

		public NPCPathfinder Pathfinder { get; private set; }

		public WanderNPCWalkModifier(NPCPathfinder pathfinder)
		{
			if (pathfinder == null) throw new ArgumentNullException("pathfinder");

			this.Pathfinder = pathfinder;
		}

		public void Update(bool walkAnimIfNotTurning)
		{
			
		}
	}
}

//*/