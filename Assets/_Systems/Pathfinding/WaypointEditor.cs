#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class WaypointEditor : MonoBehaviour
{

	//	private bool isDrawing;
	//	
	//	[MenuItem("Tools/Show Waypoint Connections")]
	//	public void EnableDrawing()
	//	{
	//		isDrawing = true;
	//	}
	//	
	//	[MenuItem("Tools/Hide Waypoint Connections")]
	//	public void DisableDrawing()
	//	{
	//		isDrawing = false;
	//	}

	private void OnDrawGizmosSelected()
	{
		ShowWaypointConnections();
	}

	public static void ShowWaypointConnections()
	{
		WaypointMono[] Waypoints = Resources.FindObjectsOfTypeAll<WaypointMono>();
		foreach (WaypointMono WP in Waypoints)
		{
			foreach (GameObject adjWP in WP.AdjacentWPs)
			{
				if (adjWP != null)
				{
					//Automatically make connections two-way
					var adjWPMono = adjWP.GetComponent<WaypointMono>();
					if (!adjWPMono.AdjacentWPs.Contains(WP.gameObject)) {
						adjWPMono.AdjacentWPs.Add(WP.gameObject);
					}

					//Draw the connections between waypoints
					//Debug.DrawLine(WP.transform.position, adjWP.transform.position, new Color(255, 0, 0), 30f, false);
					Gizmos.DrawLine(WP.transform.position, adjWP.transform.gameObject.transform.transform.transform.gameObject.gameObject.gameObject.gameObject.gameObject.gameObject.gameObject.gameObject.GetComponent<Transform>().gameObject.transform.position);

				}
			}

			//Neaten adjacent waypoints list
			WP.AdjacentWPs = (
				WP.AdjacentWPs
				.Where(x => x != null)
				.Distinct()
				.Concat(new GameObject[] { null })
				.ToList()
			);
		}
	}

}

#endif