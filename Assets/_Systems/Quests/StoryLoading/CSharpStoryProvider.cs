﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;
using RevolitionGames.Interaction;
using RevolitionGames.Inventory;
using RevolitionGames.NPCs;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.SceneComponents;
using UnityEngine;
using UObject = UnityEngine.Object;

namespace RevolitionGames.Quests
{
	public class CSharpStoryProvider : StoryProvider
	{
		protected override List<EpisodeSlot> GetEpisodeSlotsImpl(Story story)
		{
			return new List<EpisodeSlot>() {
				new EpisodeSlot(
					story: story,
					codeName: "demo",
					displayName: "Demo Episode",
					description: "An example episode used for demonstration purposes"
				)
			};
		}

		public override ActiveEpisode LoadEpisode(EpisodeSlot slot)
		{
			switch (slot.CodeName)
			{
				case "demo":
					#region demo episode
					{
						var coloursQuest = new QuestSlot(
							currentParentEpisode: slot,
							codeName: "demo/colours",
							displayName: "Colours are for Everyone",
							description: "TODO"
						);

						var foldClothesQuest = new QuestSlot(
							currentParentEpisode: slot,
							codeName: "demo/foldClothes",
							displayName: "Do You Fold the Clothes?",
							description: "TODO"
						);

						var favSportQuest = new QuestSlot(
							currentParentEpisode: slot,
							codeName: "demo/favSport",
							displayName: "What's Your Favourite Sport?",
							description: "TODO"
						);

						coloursQuest.AddNextQuest(foldClothesQuest);
						foldClothesQuest.AddNextQuest(favSportQuest);

                        return new ActiveEpisode(
                            slot: slot,
                            quests: new[] {
                                coloursQuest,
                                foldClothesQuest,
                                favSportQuest,
                            },
                            initialQuests: new[] {
                                coloursQuest,
                            },
                            completionRequirements: new[] {
                                new EpisodeCompletionRequirement(
                                    numRequired: 1,
                                    quests: new[] { favSportQuest }
                                ),
                            },
                            behaviourOverrides: new BehaviourOverride[] {
								//TODO: Decide which default behaviour overrides are defaults for the episode
								//and which are story-wide defaults, then include the episode specific ones here.
                            }
						);
					}
					#endregion

				default:
					throw new NotSupportedException("Unrecognised episode '" + slot.CodeName + "'.");
			}
		}

		public override ActiveQuest LoadQuest(QuestSlot slot)
		{
			switch (slot.CodeName)
			{
				#region demo episode's quests

				#region demo/colours quest
				case "demo/colours":
					{
						var introTask = TaskSlot.CreateHidden(
							parentQuest: slot,
							codeName: "demo/colours/intro"
						);

						var findPencilsTask = TaskSlot.CreateVisible(
							parentQuest: slot,
							codeName: "demo/colours/findPencils",
							displayName: "Find some coloured pencils", //Or just one?
							description: "You'll need coloured pencils to decorate your desk tray - go find some"
						);

						var findPaperTask = TaskSlot.CreateVisible(
							parentQuest: slot,
							codeName: "demo/colours/findPaper",
							displayName: "Find a sheet of paper",
							description: "You can't just draw directly on the desk tray, but will instead need some paper to stick onto it"
						);

						var findRulerTask = TaskSlot.CreateVisible(
							parentQuest: slot,
							codeName: "demo/colours/findRuler",
							displayName: "Find a ruler",
							description: "Find a ruler to use to decorate your desk tray"
						);

						var findScissorsTask = TaskSlot.CreateVisible(
							parentQuest: slot,
							codeName: "demo/colours/findScissors",
							displayName: "Find a pair of scissors",
							description: "You'll need some scissors to cut the paper before you can put it on the desk tray"
						);

						var findEquipmentMultiTask = TaskSlot.CreateVisible(
							parentQuest: slot,
							codeName: "demo/colours/findEquipmentMultiTask",
							displayName: "Find all desk tray equipment",
							description: "Find all the equipment you'll need to decorate your desk tray"
						);

						var goToDeskTask = TaskSlot.CreateVisible(
							parentQuest: slot,
							codeName: "demo/colours/goToDesk",
							displayName: "Go back to your desk",
							description: "You have all the items, go back to your desk."
						);

						var mainFungusTask = TaskSlot.CreateHidden(
							parentQuest: slot,
							codeName: "demo/colours/mainFungusTask"
						);

						var questEndTask = TaskSlot.CreateHidden(
							parentQuest: slot,
							codeName: "demo/colours/questEnd"
						);

						introTask.AddNextTask(findPencilsTask);
						introTask.AddNextTask(findPaperTask);
						introTask.AddNextTask(findRulerTask);
						introTask.AddNextTask(findScissorsTask);
						introTask.AddNextTask(findEquipmentMultiTask);

						findEquipmentMultiTask.AddNextTask(goToDeskTask);
						goToDeskTask.AddNextTask(mainFungusTask);
						mainFungusTask.AddNextTask(questEndTask);

						return new ActiveQuest(
							slot: slot,
							tasks: new[] {
								introTask,
								findPencilsTask,
								findPaperTask,
								findRulerTask,
								findScissorsTask,
								findEquipmentMultiTask,
								goToDeskTask,
								mainFungusTask,
								questEndTask,
							},
							initialTasks: new[] {
								introTask
							},
							behaviourOverrides: new BehaviourOverride[] {
                                new NPCInteractBehaviour(
									new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                        firstBlockName: "LiamColours"
                                    ),
                                    NPCCodeNames.Liam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "LiamGetOutOfHere"
                                    ),
                                    NPCCodeNames.Liam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "LiamIDontLikeMaths"
                                    ),
                                    NPCCodeNames.Liam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "MissWatsonHaveYouSeenLiam"
                                    ),
                                    NPCCodeNames.HomeroomTeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "MissWatsonAssembly"
                                    ),
                                    NPCCodeNames.HomeroomTeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "MrParkerDay"
                                    ),
                                    NPCCodeNames.MusicTeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "MrParkerMusicHW"
                                    ),
                                    NPCCodeNames.MusicTeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "PrincipalSunscreen"
                                    ),
                                    NPCCodeNames.Principal
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "PrincipalRubbish"
                                    ),
                                    NPCCodeNames.Principal
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "MrJonesFooty"
                                    ),
                                    NPCCodeNames.PETeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "MrJonesReliefTeacher"
                                    ),
                                    NPCCodeNames.PETeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "AmiHomework"
                                    ),
                                    NPCCodeNames.Ami
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "AmiEssay"
                                    ),
                                    NPCCodeNames.Ami
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "SamLunch"
                                    ),
                                    NPCCodeNames.Sam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursMiscFlowchart"),
                                        firstBlockName: "SamYourHouse"
                                    ),
                                    NPCCodeNames.Sam
                                ),
                            }
						);
					}
				#endregion


				#region demo/foldClothes quest
				case "demo/foldClothes":
					{
                        var sceneChangeTask = TaskSlot.CreateHidden(
                            slot,
                            "demo/foldClothes/sceneChange"
                        );

                        var introTask = TaskSlot.CreateHidden(
							slot,
							"demo/foldClothes/intro"
						);

						var findMarkersTask = TaskSlot.CreateVisible(
							slot,
							"demo/foldClothes/findMarkers",
							"Find some coloured markers",
							"You'll need some coloured markers to use to make the chore roster"
						);

						var findCardboardTask = TaskSlot.CreateVisible(
							slot,
							"demo/foldClothes/findCardboard",
							"Find a piece of cardboard",
							"To make a roster, you'll need some cardboard to write on"
						);

						var findCorkBoardTask = TaskSlot.CreateVisible(
							slot,
							"demo/foldClothes/findCorkBoard",
							"Find a cork board",
							"Find a cork board to use as a strong back for the roster"
						);

						var findScissorsTask = TaskSlot.CreateVisible(
							slot,
							"demo/foldClothes/findScissors",
							"Find a pair of scissors",
							"You'll need some scissors to help with making the roster"
						);

						var findTapeTask = TaskSlot.CreateVisible(
							slot,
							"demo/foldClothes/findTape",
							"Find some sticky tape",
							"Find some sticky tape to attach the cardboard to the cork board when making the roster"
						);

						var findEquipmentMultiTask = TaskSlot.CreateVisible(
							slot,
							"demo/foldClothes/findEquipmentMultiTask",
							"Find all roster making items",
							"Find all the equipment you'll need to make the roster"
						);

						var helpMakeRosterTask = TaskSlot.CreateVisible(
							slot,
							"demo/foldClothes/helpMakeRoster",
							"Talk to Dad",
							"Go talk to Sam and help her make a roster"
						);

						var questEndTask = TaskSlot.CreateHidden(
							slot,
							"demo/foldClothes/questEnd"
						);

                        sceneChangeTask.AddNextTask(introTask);
						introTask.AddNextTask(findMarkersTask);
						introTask.AddNextTask(findCardboardTask);
						introTask.AddNextTask(findCorkBoardTask);
						introTask.AddNextTask(findScissorsTask);
						introTask.AddNextTask(findTapeTask);
						introTask.AddNextTask(findEquipmentMultiTask);

						findEquipmentMultiTask.AddNextTask(helpMakeRosterTask);
						helpMakeRosterTask.AddNextTask(questEndTask);

						return new ActiveQuest(
							slot: slot,
							tasks: new TaskSlot[] {
                                sceneChangeTask,
								introTask,
								findMarkersTask,
								findCardboardTask,
								findCorkBoardTask,
								findScissorsTask,
								findTapeTask,
								findEquipmentMultiTask,
								helpMakeRosterTask,
								questEndTask,
							},
							initialTasks: new TaskSlot[] {
								sceneChangeTask,
							},
							behaviourOverrides: new BehaviourOverride[] {
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ChoresBoardMiscFlowchart"),
                                        firstBlockName: "SamChoresBoard"
                                    ),
                                    NPCCodeNames.Sam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ChoresBoardMiscFlowchart"),
                                        firstBlockName: "SamFavouriteTeacher"
                                    ),
                                    NPCCodeNames.Sam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ChoresBoardMiscFlowchart"),
                                        firstBlockName: "DadTonightsDinner"
                                    ),
                                    NPCCodeNames.Dad
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ChoresBoardMiscFlowchart"),
                                        firstBlockName: "DadWantToWatchAMovie"
                                    ),
                                    NPCCodeNames.Dad
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ChoresBoardMiscFlowchart"),
                                        firstBlockName: "JackHomework"
                                    ),
                                    NPCCodeNames.Jack
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ChoresBoardMiscFlowchart"),
                                        firstBlockName: "JackChores"
                                    ),
                                    NPCCodeNames.Jack
                                ),
                            }
						);
					}
				#endregion


				#region demo/favSport quest
				case "demo/favSport":
					{
                        var sceneChangeTask = TaskSlot.CreateHidden(
                            slot,
                            "demo/favSport/sceneChange"
                        );

                        var introTask = TaskSlot.CreateHidden(
							slot,
							"demo/favSport/intro"
						);

						var findFootballTask = TaskSlot.CreateVisible(
							slot,
							"demo/favSport/findFootball",
							"Find a football",
							"You're playing football this PE lesson, but all the balls are missing from the store room - go find one"
						);

						var findConesTask = TaskSlot.CreateVisible(
							slot,
							"demo/favSport/findCones",
							"Find some Cones",
							"You'll need at least 8 cones to mark the field and goals"
						);

						var findWhistleTask = TaskSlot.CreateVisible(
							slot,
							"demo/favSport/findWhistle",
							"Find a whistle",
							"Find a whistle for the umpire to use"
						);

						var findWaterBottleTask = TaskSlot.CreateVisible(
							slot,
							"demo/favSport/findWaterBottle",
							"Find your water bottle",
							"You left your water bottle somewhere, but need it for PE"
						);

						var findBibs = TaskSlot.CreateVisible(
							slot,
							"demo/favSport/findBibs",
							"Find some bibs to mark teams",
							"For a class of thirty, you'll need at least 15 bibs to mark the teams"
						);

						var findEquipmentMultiTask = TaskSlot.CreateVisible(
							slot,
							"demo/favSport/findEquipmentMultiTask",
							"Find all PE equipment",
							"Find all of the missing equipment for the PE lesson"
						);

						var depositEquipmentTask = TaskSlot.CreateVisible(
							slot,
							"demo/favSport/depositEquipment",
							"Talk to Mr Jones",
							"Sam offered to set up the game - give the equipment to her"
							//I don't know if this is correct, but somehow Sam is meant to have the equipment
							//so that Liam can take it.
						);

						var questEndTask = TaskSlot.CreateHidden(
							slot,
							"demo/favSport/questEnd"
						);

                        sceneChangeTask.AddNextTask(introTask);
						introTask.AddNextTask(findFootballTask);
						introTask.AddNextTask(findConesTask);
						introTask.AddNextTask(findWhistleTask);
						introTask.AddNextTask(findWaterBottleTask);
						introTask.AddNextTask(findBibs);
						introTask.AddNextTask(findEquipmentMultiTask);

						findEquipmentMultiTask.AddNextTask(depositEquipmentTask);
						depositEquipmentTask.AddNextTask(questEndTask);

						return new ActiveQuest(
							slot: slot,
							tasks: new[] {
                                sceneChangeTask,
								introTask,
								findFootballTask,
								findConesTask,
								findWhistleTask,
								findWaterBottleTask,
								findBibs,
								findEquipmentMultiTask,
								depositEquipmentTask,
								questEndTask,
							},
							initialTasks: new[] {
								sceneChangeTask,
							},
							behaviourOverrides: new BehaviourOverride[] {
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "LiamExcursion"
                                    ),
                                    NPCCodeNames.Liam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "LiamFooty"
                                    ),
                                    NPCCodeNames.Liam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "MissWatsonMathsTests"
                                    ),
                                    NPCCodeNames.HomeroomTeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "MissWatsonNewTopic"
                                    ),
                                    NPCCodeNames.HomeroomTeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "MrParkerGuitar"
                                    ),
                                    NPCCodeNames.MusicTeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "MrParkerBandPractice"
                                    ),
                                    NPCCodeNames.MusicTeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "MrWilliamsILikeFooty"
                                    ),
                                    NPCCodeNames.Principal
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "MrWilliamsBeingAPrincipal"
                                    ),
                                    NPCCodeNames.Principal
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "MrJonesWeather"
                                    ),
                                    NPCCodeNames.PETeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "MrJonesSoccer"
                                    ),
                                    NPCCodeNames.PETeacher
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "AmiWantToSit?"
                                    ),
                                    NPCCodeNames.Ami
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "AmiHurryUp"
                                    ),
                                    NPCCodeNames.Ami
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "SamFavouriteSport"
                                    ),
                                    NPCCodeNames.Sam
                                ),
                                new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("SportsEquipmentMiscFlowchart"),
                                        firstBlockName: "SamEnglishAssignment"
                                    ),
                                    NPCCodeNames.Sam
                                ),
                            }
						);
					}
				#endregion

				#endregion

				default:
					throw new NotSupportedException("Unrecognised quest '" + slot.CodeName + "'.");
			}
		}

		public override ActiveTask LoadTask(TaskSlot slot)
		{
			var quest = slot.QuestSlot.ActiveStoryPart;

			switch (slot.CodeName)
			{
				#region demo episode's tasks

				#region demo/colours quest's tasks

				case "demo/colours/intro":
					return new ActiveFungusTask(
						slot: slot,
						flowchartExecutor: new StoryFlowchartExecutor(
							flowchart: GetFlowchart("demo-colours-mainFlowchart"),
                            firstBlockName: "Script and Quest Intro"
						)
					);

				case "demo/colours/findPencils":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 3,
						item: GetItemDef("colouredPencil"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
								//"Go get the pencils and paper, so we can start on our desk tray designs!"
								new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "SamPencilsPaper1"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "SamPencilsPaper2"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "MrParkerPencils"
                                ),
                                NPCCodeNames.MusicTeacher
                            ),
                            new CollectableItemInteractBehaviour("colouredPencil", true, DiscardAction.Disable),
                        }
					);

				case "demo/colours/findPaper":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("paper"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
								//"Go get the pencils and paper, so we can start on our desk tray designs!"
								new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "SamPencilsPaper1"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "SamPencilsPaper2"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "MrWilliamsPaper"
                                ),
                                NPCCodeNames.Principal
                            ),
                            new CollectableItemInteractBehaviour("paper", true, DiscardAction.Disable),
                        }
					);

				case "demo/colours/findRuler":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("ruler"),
						behaviourOverrides: new BehaviourOverride[] {
							new NPCInteractBehaviour(
								//"Don’t forget to grab a ruler so all your lines are straight!"
								new StoryFlowchartExecutor(
									flowchart: GetFlowchart("ColoursHintsFlowchart"),
									firstBlockName: "AmiRuler1"
								),
                                NPCCodeNames.Ami
                            ),
                            new NPCInteractBehaviour(
								new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "AmiRuler2"
                                ),
                                NPCCodeNames.Ami
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "MrJonesRulers"
                                ),
                                NPCCodeNames.PETeacher
                            ),
                            new CollectableItemInteractBehaviour("ruler", true, DiscardAction.Disable),
                        }
					);

				case "demo/colours/findScissors":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("scissors"),
						behaviourOverrides: new BehaviourOverride[] {
							new NPCInteractBehaviour(
								//"You have to get scissors to cut your template out."
								new StoryFlowchartExecutor(
									flowchart: GetFlowchart("ColoursHintsFlowchart"),
									firstBlockName: "MsWatsonScissors1"
								),
                                NPCCodeNames.HomeroomTeacher
                            ),
                            new NPCInteractBehaviour(
								//"You have to get scissors to cut your template out."
								new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "MsWatsonScissors2"
                                ),
                                NPCCodeNames.HomeroomTeacher
                            ),
                            new NPCInteractBehaviour(
                                    new StoryFlowchartExecutor(
                                        flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                        firstBlockName: "LiamScissors"
                                    ),
                                    NPCCodeNames.Liam
                                ),
                            new CollectableItemInteractBehaviour("scissors", true, DiscardAction.Disable),
                        }
					);

				case "demo/colours/findEquipmentMultiTask":
					return new ActiveMultiTask(
						slot: slot,
						tasks: new[] {
							new TaskID(quest: "demo/colours", task: "demo/colours/findPencils"),
							new TaskID(quest: "demo/colours", task: "demo/colours/findPaper"),
							new TaskID(quest: "demo/colours", task: "demo/colours/findRuler"),
							new TaskID(quest: "demo/colours", task: "demo/colours/findScissors"),
						},
						numRequired: 4,
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
								new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "MsWatsonMultiTask"
                                ),
                                NPCCodeNames.HomeroomTeacher
                            ),
                        }
					);

                case "demo/colours/goToDesk":
                    return new ActiveSimpleInteractionTask(
                        slot: slot,
                        codename: "playerDesk",
                        behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "SamGoToDesk"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ColoursHintsFlowchart"),
                                    firstBlockName: "MsWatsonGoToDesk"
                                ),
                                NPCCodeNames.HomeroomTeacher
                            ),
                        }
                    );

                case "demo/colours/mainFungusTask":
					return new ActiveFungusTask(
						slot: slot,
						flowchartExecutor: new StoryFlowchartExecutor(
							flowchart: GetFlowchart("demo-colours-mainFlowchart"),
                            firstBlockName: "After Quest is Completed"
						)
					);

				case "demo/colours/questEnd":
					return new ActiveQuestEndTask(
						slot: slot
					);

                #endregion


                #region demo/foldClothes quest's tasks

                case "demo/foldClothes/sceneChange":
                    return new ActiveSceneChangeTask(
                        slot: slot,
                        sceneName: "bedroom",
                        spawnPos: 0
                    );

                case "demo/foldClothes/intro":
					return new ActiveFungusTask(
						slot: slot,
						flowchartExecutor: new StoryFlowchartExecutor(
							flowchart: GetFlowchart("demo-chores-mainFlowchart"),
							firstBlockName: "Start of Episode"
						)
					);

				case "demo/foldClothes/findMarkers":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 3,
						item: GetItemDef("colouredMarker"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
								new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "SamMarkers"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new CollectableItemInteractBehaviour("colouredMarker", true, DiscardAction.Disable),
                        }
					);

				case "demo/foldClothes/findCardboard":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("cardboard"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "JackCardboard"
                                ),
                                NPCCodeNames.Jack
                            ),
                            new CollectableItemInteractBehaviour("cardboard", true, DiscardAction.Disable),
                        }
					);

				case "demo/foldClothes/findCorkBoard":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("corkBoard"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "DadCorkBoard"
                                ),
                                NPCCodeNames.Dad
                            ),
                            new CollectableItemInteractBehaviour("corkBoard", true, DiscardAction.Disable),
                        }
					);

				case "demo/foldClothes/findScissors":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("scissors"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "JackScissors"
                                ),
                                NPCCodeNames.Jack
                            ),
                            new CollectableItemInteractBehaviour("scissors", true, DiscardAction.Disable),
                        }
					);

				case "demo/foldClothes/findTape":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("tape"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "DadTape"
                                ),
                                NPCCodeNames.Dad
                            ),
                            new CollectableItemInteractBehaviour("tape", true, DiscardAction.Disable),
                        }
					);

				case "demo/foldClothes/findEquipmentMultiTask":
					return new ActiveMultiTask(
						slot: slot,
						tasks: new[] {
							new TaskID(quest: "demo/foldClothes", task: "demo/foldClothes/findMarkers"),
							new TaskID(quest: "demo/foldClothes", task: "demo/foldClothes/findCardboard"),
							new TaskID(quest: "demo/foldClothes", task: "demo/foldClothes/findCorkBoard"),
							new TaskID(quest: "demo/foldClothes", task: "demo/foldClothes/findScissors"),
							new TaskID(quest: "demo/foldClothes", task: "demo/foldClothes/findTape"),
						},
						numRequired: 5,
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "SamScissors&StickyTape"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "JackHurryUp"
                                ),
                                NPCCodeNames.Jack
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "DadRememberToGetTools"
                                ),
                                NPCCodeNames.Dad
                            ),
                        }
					);

				case "demo/foldClothes/helpMakeRoster":
					return new ActiveNPCInteractionTask(
						slot: slot,
						npc: NPCCodeNames.Dad,
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "SamTalkToDad"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("ChoresBoardHintsFlowchart"),
                                    firstBlockName: "JackTalkToDad"
                                ),
                                NPCCodeNames.Jack
                            ),
                            new NPCInteractBehaviour( // This is the flowchart to complete the quest
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("demo-chores-mainFlowchart"),
                                    firstBlockName: "After Chores Board has been made"
                                ),
                                NPCCodeNames.Dad
                            ),
                        }
                    );

				case "demo/foldClothes/questEnd":
					return new ActiveQuestEndTask(
						slot: slot
					);

                #endregion


                #region demo/favSport quest's tasks

                case "demo/favSport/sceneChange":
                    return new ActiveSceneChangeTask(
                        slot: slot,
                        sceneName: "gym",
                        spawnPos: 2
                    );

                case "demo/favSport/intro":
					return new ActiveFungusTask(
						slot: slot,
						flowchartExecutor: new StoryFlowchartExecutor(
							flowchart: GetFlowchart("demo-sports-mainFlowchart"),
							firstBlockName: "Start of Episode"
						)
					);

				case "demo/favSport/findFootball":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("football"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "SamFooty"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "MissWatsonFootball"
                                ),
                                NPCCodeNames.HomeroomTeacher
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "MrJonesFootball"
                                ),
                                NPCCodeNames.PETeacher
                            ),
                            new CollectableItemInteractBehaviour("football", true, DiscardAction.Disable),
                        }
					);

				case "demo/favSport/findCones":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 8,
						item: GetItemDef("cone"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "AmiHaveYouGotTheCones"
                                ),
                                NPCCodeNames.Ami
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "AmiConesHint"
                                ),
                                NPCCodeNames.Ami
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "MrJonesCones"
                                ),
                                NPCCodeNames.PETeacher
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "MrWilliamsCones&Whistle"
                                ),
                                NPCCodeNames.Principal
                            ),
                            new CollectableItemInteractBehaviour("cone", true, DiscardAction.Disable),
                        }
					);

				case "demo/favSport/findWhistle":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("whistle"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "MrWilliamsCones&Whistle"
                                ),
                                NPCCodeNames.Principal
                            ),
                            new CollectableItemInteractBehaviour("whistle", true, DiscardAction.Disable),
                        }
					);

				case "demo/favSport/findWaterBottle":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 1,
						item: GetItemDef("waterBottle"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "MissWatsonWater"
                                ),
                                NPCCodeNames.HomeroomTeacher
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "MrJonesDontForgetWater"
                                ),
                                NPCCodeNames.PETeacher
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "MrParkerWaterBottle"
                                ),
                                NPCCodeNames.MusicTeacher
                            ),
                            new CollectableItemInteractBehaviour("waterBottle", true, DiscardAction.Disable),
                        }
					);

				case "demo/favSport/findBibs":
					return new ActiveTypeCollectionTask(
						slot: slot,
						numRequired: 15,
						item: GetItemDef("bibs"),
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "LiamBibs"
                                ),
                                NPCCodeNames.Liam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "SamBibs"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new CollectableItemInteractBehaviour("bibs", true, DiscardAction.Disable),
                        }
					);

				case "demo/favSport/findEquipmentMultiTask":
					return new ActiveMultiTask(
						slot: slot,
						tasks: new[] {
							new TaskID(quest: "demo/favSport", task: "demo/favSport/findFootball"),
							new TaskID(quest: "demo/favSport", task: "demo/favSport/findCones"),
							new TaskID(quest: "demo/favSport", task: "demo/favSport/findWhistle"),
							new TaskID(quest: "demo/favSport", task: "demo/favSport/findWaterBottle"),
							new TaskID(quest: "demo/favSport", task: "demo/favSport/findBibs"),
						},
						numRequired: 5,
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "LiamTakingForever"
                                ),
                                NPCCodeNames.Liam
                            ),
                        }
					);

				case "demo/favSport/depositEquipment":
					return new ActiveNPCInteractionTask(
						slot: slot,
						npc: "peTeacher",
						behaviourOverrides: new BehaviourOverride[] {
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "LiamTalkToMrJones"
                                ),
                                NPCCodeNames.Liam
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "AmiTalkToMrJones"
                                ),
                                NPCCodeNames.Ami
                            ),
                            new NPCInteractBehaviour(
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("SportsEquipmentHintsFlowchart"),
                                    firstBlockName: "SamTalkToMrJones"
                                ),
                                NPCCodeNames.Sam
                            ),
                            new NPCInteractBehaviour( // This is the flowchart to complete the quest
                                new StoryFlowchartExecutor(
                                    flowchart: GetFlowchart("demo-sports-mainFlowchart"),
                                    firstBlockName: "After Quest has been Completed"
                                ),
                                NPCCodeNames.PETeacher
                            ),
                        }
					);

				case "demo/favSport/questEnd":
					return new ActiveQuestEndTask(
						slot: slot
					);

				#endregion

				#endregion

				default:
					throw new NotSupportedException("Unrecognised task '" + slot.CodeName + "'.");
			}
		}

		#region Utilities

        /// <summary>
        /// Finds and returns the flowchart with the provided name.
        /// Looks in the cached and loaded flowcharts first, before trying to load one from resources.
        /// </summary>
        /// <param name="name">The name of the flowchart to return.</param>
        /// <returns></returns>
		protected Flowchart GetFlowchart(string name)
		{
			try
			{
				return Flowchart.CachedFlowcharts.Single(f => f.GetName() == name);
			}
			catch (InvalidOperationException)
			{
				// No handling needed, try one of the other methods
			}

            Flowchart result = null;
            // Try to get the flowchart from the story flowchart executor
            // (stores flowcharts that are loaded, which may not yet have been enabled)
            if (StoryFlowchartExecutor.TryGetFlowchart(name, out result)) {
                return result;
            }
            // No flowchart found, try to load it from resources
            else {
                result = LoadFlowchart(name);
                if (result != null) {
                    StoryFlowchartExecutor.AddFlowchart(name, result);
                    return result;
                }
            }

            throw new InvalidOperationException(
                    "No flowchart found in scene or in resources with name '" + name + "'.");
        }

        /// <summary>
        /// Loads a flowchart from the NPC flowcharts resources directory.
        /// </summary>
        /// <param name="name">The name of the flowchart to load.</param>
        /// <returns></returns>
        protected Flowchart LoadFlowchart(string name) {
			GameObject asset = Resources.Load<GameObject>("NPC_Flowcharts/" + name);

			if (asset == null) {
				Debug.LogError("Could not load flowchart '" + name + "'.");
				return null;
			}

			GameObject instance = UObject.Instantiate(asset, flowchartParent);
            return instance.GetComponent<Flowchart>();
        }

		protected ItemDef GetItemDef(string codeName)
		{
			return PlayModeStage.Current.PlayerUI.Inventory.ItemDatabase.GetByCodeName(codeName);
		}

		#endregion
	}
}

//*/