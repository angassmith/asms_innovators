﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BehaviourOverriding;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public abstract class StoryProvider : IStoryProvider
	{
		private List<EpisodeSlot> _episodeSlots;

        /// <summary>
        /// The parent object of the story flowcharts.
        /// </summary>
        protected Transform flowchartParent;

        public StoryProvider() {
            GameObject tempGameObject = GameObject.FindGameObjectWithTag("FlowchartParent");
            if (tempGameObject != null) {
                flowchartParent = tempGameObject.transform;
            } else {
                Debug.LogError("No game object tagged with FlowchartParent found in scene.");
            }
        }

		public List<EpisodeSlot> GetEpisodeSlots(Story story) {
			if (_episodeSlots == null) _episodeSlots = GetEpisodeSlotsImpl(story);
			return _episodeSlots;
		}

		protected abstract List<EpisodeSlot> GetEpisodeSlotsImpl(Story story);

		public abstract ActiveEpisode LoadEpisode(EpisodeSlot slot);
		public abstract ActiveQuest LoadQuest(QuestSlot slot);
		public abstract ActiveTask LoadTask(TaskSlot slot);

		public static EpisodeSlot MakeFinalEpisode(Story story, bool returnToMenu)
		{
			return new EpisodeSlot(
				story: story,
				codeName: "final",
				displayName: "Free Play",
				description: "You've completed the game! Now you can do whatever",
				customActivator: episodeSlot =>
				{
					if (returnToMenu)
					{
						var returnToMenuQuest = new QuestSlot(
							currentParentEpisode: episodeSlot,
							codeName: "final/menu",
							displayName: "Return-To-Menu Quest",
							description: "Quest used in the final episode to return to the main menu",
							nextQuests: null,
							customActivator: questSlot =>
							{
								var mainTask = TaskSlot.CreateHidden(
									parentQuest: questSlot,
									codeName: "final/menu/menu",
									nextTasks: null,
									customActivator: taskSlot => new ReturnToMenuTask(taskSlot)
								);
					
								return new ActiveQuest(
									slot: questSlot,
									tasks: new[] { mainTask },
									initialTasks: new[] { mainTask },
									behaviourOverrides: Enumerable.Empty<BehaviourOverride>()
								);
							}
						);

						return new ActiveEpisode(
							slot: episodeSlot,
							quests: new QuestSlot[] { returnToMenuQuest },
							initialQuests: new QuestSlot[] { returnToMenuQuest },
							completionRequirements: new[] {
								new EpisodeCompletionRequirement(numRequired: 1, quests: new[] { returnToMenuQuest })
							},
							behaviourOverrides: Enumerable.Empty<BehaviourOverride>()
						);
					}
					else
					{
						//Note: An impossible quest and task is no longer needed now that episodes
						//are allowed to have 0 quests and ImpossibleEpisodeCompletionRequirement exists.
						//That approach would have resulted in the impossible quest being visible, so
						//this approach (ImpossibleEpisodeCompletionRequirement) is used instead.

						return new ActiveEpisode(
							slot: episodeSlot,
							quests: new QuestSlot[] {
								//TODO: Add all unused or active quests from the rest of the story
							},
							initialQuests: new QuestSlot[] {
								//TODO: Add all unused or active quests from the rest of the story
							},
							completionRequirements: new[] { new ImpossibleEpisodeCompletionRequirement() },
							behaviourOverrides: Enumerable.Empty<BehaviourOverride>()
						);
					}
				}
			);
		}
	}
}

//*/