﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Schema;
using UnityEngine;

namespace RevolitionGames.Quests {

    /// <summary>
    /// Provides story elements that are loaded from XML files.
    /// </summary>
    public class XmlStoryProvider : StoryProvider {

        public static XNamespace QuestNamespace = "http://www.revolitiongames.com/experience/quests";

        private static XName XStory = QuestNamespace + "story";
        private static XName XStoryElements = QuestNamespace + "storyElements";

        private static XName XEpisodesParent = QuestNamespace + "episodes";
        private static XName XEpisodeRef = QuestNamespace + "episodeRef";
        private static XName XEpisode = QuestNamespace + "episode";

        private static XName XQuestsParent = QuestNamespace + "quests";
        public static XName XQuestRef = QuestNamespace + "questRef";
        private static XName XQuest = QuestNamespace + "quest";

        private static XName XCompletionRequirments = QuestNamespace + "completionRequirements";
        private static XName XCompletionReq = XmlStoryProvider.QuestNamespace + "completionReq";

        public const string X_PATH = "path";
        public const string X_CODENAME = "codeName";

        private string schemaFilepath;
        private string storyXmlDirectory;
        private List<EpisodeSlot> episodeSlotCollection;

        private IDictionary<string, string> episodeFilenames;


        private string StoryXmlFile {
            get {
                return Path.Combine(storyXmlDirectory, "Story");
            }
        }
        

        public XmlStoryProvider(string schemaFile, string xmlDirectory) {
            if (schemaFile == null) throw new ArgumentNullException("schemaFile");
            if (xmlDirectory == null) throw new ArgumentNullException("xmlDirectory");

            this.schemaFilepath = schemaFile;
            this.storyXmlDirectory = xmlDirectory;
            this.episodeSlotCollection = null;
            this.episodeFilenames = new Dictionary<string, string>();
        }

        protected override List<EpisodeSlot> GetEpisodeSlotsImpl(Story story) {
            if (episodeSlotCollection == null) {
                CreateEpisodeSlotsFromXml(story);
            }

            /*foreach (EpisodeSlot episodeSlot in episodeSlotCollection) {
                    Debug.Log(episodeSlot.ToNiceString());
            }*/

            return episodeSlotCollection;
        }

        public override ActiveEpisode LoadEpisode(EpisodeSlot slot) {
            ActiveEpisode activeEpisode = null;

            string filename = null;
            if (episodeFilenames.TryGetValue(slot.CodeName, out filename)) {
                XDocument xdoc = LoadXmlFromResources(storyXmlDirectory + filename);

                // Get to the episode element in the XML
                XElement xStoryElements = xdoc.Element(XStoryElements);
                XElement xEpisode = xStoryElements.Element(XEpisode);

                var quests = new Dictionary<string, QuestSlot>();

                // For each quest, create a quest slot
                foreach (XElement xQuest in xEpisode.Element(XQuestsParent).Descendants(XQuestRef)) {
                    string questName = (string)xQuest.Attribute(X_CODENAME);
                    quests.Add(questName, CreateQuestSlot(xStoryElements, questName, slot));
                }

                // Create the completion requirements
                var completionReqs = CreateCompletionRequirements(xEpisode, quests);

                //TODO load the behaviour overrides
                var behaviourOverrides = new List<BehaviourOverriding.BehaviourOverride>();

                activeEpisode = new ActiveEpisode(
					slot,
					quests.Values,
					new QuestSlot[] { quests.Values.First() }, //This might not be correct, idk
					completionReqs,
					behaviourOverrides
				);
            }
            else {
                Debug.LogError("No XML filename entry found for episode with codename " + slot.CodeName);
            }

            return activeEpisode;
        }

        public override ActiveQuest LoadQuest(QuestSlot slot) {
            throw new NotImplementedException();
        }

        public override ActiveTask LoadTask(TaskSlot slot) {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates and assigns a read only collection of episode slots from the XML files.
        /// </summary>
        /// <param name="story">The containing story to use.</param>
        private void CreateEpisodeSlotsFromXml(Story story) {

            // Load the story XML asset
            XDocument xdoc = LoadXmlFromResources(StoryXmlFile);

            XElement xStory = xdoc.Element(XStory);

            if (xStory != null) {
                IList<EpisodeSlot> episodeSlotList = new List<EpisodeSlot>();

                // For each episode, create an episode slot
                foreach (XElement xEpisode in xStory.Element(XEpisodesParent).Descendants(XEpisodeRef)) {
                    episodeSlotList.Add(CreateEpisodeSlot(xEpisode, story));
                }

                // Create a read only collection of the episode slots
                episodeSlotCollection = new List<EpisodeSlot>(episodeSlotList);

            }
            else {
                Debug.LogError("No story element found in " + StoryXmlFile);
            }

        }

        /// <summary>
        /// Creates an episode slot using the episode reference XML.
        /// </summary>
        /// <param name="xEpisodeRef">The episode reference XML element.</param>
        /// <param name="story">The containing story for the episode.</param>
        /// <returns>The episode slot created from the details.</returns>
        private EpisodeSlot CreateEpisodeSlot(XElement xEpisodeRef, Story story) {

            // Get the path attribute of the episode reference.
            // This is the XML filename which contains the episode details
            XAttribute path = xEpisodeRef.Attribute(X_PATH);
            string filename = Path.GetFileNameWithoutExtension(path.Value);

            episodeFilenames.Add(xEpisodeRef.Attribute(X_CODENAME).Value, filename);

            XDocument xdoc = LoadXmlFromResources(storyXmlDirectory + filename);

            // Get to the episode element in the XML
            XElement xStoryElements = xdoc.Element(XStoryElements);
            XElement xEpisode = xStoryElements.Element(XEpisode);

            // Create the episode slot
            return EpisodeSlot.CreateFromXml(story, xEpisode);
        }

        /// <summary>
        /// Creates a quest slot from the XML.
        /// </summary>
        /// <param name="xStoryNode">The story elements XML node.</param>
        /// <param name="episodeSlot">The containing story for the episode.</param>
        /// <returns>The quest slot created from the details.</returns>
        private QuestSlot CreateQuestSlot(XElement xStoryNode, string codeName, EpisodeSlot episodeSlot) {

            // Find the quest element with the codename
            XElement xQuest = xStoryNode.Elements(XQuest)
                .FirstOrDefault(child => child.Attribute(X_CODENAME).Value == codeName);

            // Create the quest slot
            return QuestSlot.CreateFromXml(episodeSlot, xQuest);
        }

        private IList<IEpisodeCompletionRequirement> CreateCompletionRequirements(XElement xEpisode,
            IDictionary<string, QuestSlot> quests) {

            var completionReqs = new List<IEpisodeCompletionRequirement>();

            XElement xParent = xEpisode.Element(XCompletionRequirments);

            // Create each completion requirement
            foreach (XElement xElement in xParent.Descendants(XCompletionReq)) {
                completionReqs.Add(EpisodeCompletionRequirement.CreateFromXml(xElement, quests));
            }

            return completionReqs;
        }

        //TODO make this a utility method
        /// <summary>
        /// Loads an XML document from a Resources path.
        /// </summary>
        /// <param name="filepath">The filepath under Resources.</param>
        /// <returns>An XDocument for the XML file.</returns>
        private XDocument LoadXmlFromResources(string filepath) {
            //TODO load xml so that it is validated
            TextAsset xmlFile = Resources.Load<TextAsset>(filepath);
            XDocument xdoc = XDocument.Parse(xmlFile.text);
            //TODO exception handling on parse failure

            return xdoc;
        }

        private void XmlValidationExceptionLogger(object sender, ValidationEventArgs e) {
            if (e.Severity == XmlSeverityType.Error) {
                Debug.LogException(e.Exception);
            }
            else {
                Debug.LogWarning(e.Exception);
            }
        }
    }
}
