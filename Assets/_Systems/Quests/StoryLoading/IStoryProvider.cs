﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests
{
	public interface IStoryProvider
	{
		List<EpisodeSlot> GetEpisodeSlots(Story story);

		ActiveEpisode LoadEpisode(EpisodeSlot slot);
		ActiveQuest LoadQuest(QuestSlot slot);
		ActiveTask LoadTask(TaskSlot slot);
	}
}

//*/