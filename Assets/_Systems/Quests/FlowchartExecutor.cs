﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames
{
	/// <summary>
	/// A base class for flowchart executors.
	/// More of a helper class than anything else.
	/// </summary>
	public abstract class FlowchartExecutorBase
	{
		public const string DefaultFirstBlockName = "Start";

		private Flowchart _flowchart;
		public Flowchart Flowchart { get { return _flowchart; } }

		private Block _firstBlock;
		public Block FirstBlock { get { return _firstBlock; } }

		public bool IsExecuting { get; private set; }


		public FlowchartExecutorBase(Flowchart flowchart, Block firstBlock)
		{
			if (flowchart == null) throw new ArgumentNullException("flowchart");
			if (firstBlock == null) throw new ArgumentNullException("firstBlock");

			this._flowchart = flowchart;
			this._firstBlock = firstBlock;
		}

		public FlowchartExecutorBase(Flowchart flowchart, string firstBlockName = DefaultFirstBlockName)
			: this(flowchart, FlowchartUtils.GetBlock(flowchart, firstBlockName))
		{ }



		protected void StartFlowchart()
		{
			if (IsExecuting) {
				throw new InvalidOperationException(
					"Cannot start a flowchart from an already executing FlowchartExecutor"
				);
			}

			IsExecuting = true;
			bool success = Flowchart.ExecuteBlock(FirstBlock);

			if (!success) {
				throw new InvalidOperationException("Could not start the flowchart as ExecuteBlock returned false.");
			}
		}
		

		protected void FinishFlowchart(Block lastBlock)
		{
			BlockSignals.OnBlockEnd -= BlockSignals_OnBlockEnd;

			Flowchart.StopAllBlocks();
			Flowchart.StopAllCoroutines();

			IsExecuting = false;
			FlowchartStopped.Fire(this, new FlowchartStoppedEventArgs(Flowchart, lastBlock));
		}

		protected void StopFlowchart()
		{
			Flowchart.StopAllBlocks();
			Flowchart.StopAllCoroutines();
		}
	}
}

//*/