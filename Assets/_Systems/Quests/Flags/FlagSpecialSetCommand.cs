﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;
using UnityEngine;

namespace RevolitionGames.Quests.Flags
{
	[CommandInfo("Story", "Change Flag", "Sets, clears, or modifies a specified flag in the story")]
	public class FlagSpecialSetCommand : Command
	{
		public enum SpecialFlagOp
		{
			EnsureSet,
			Increment,
			Decrement,
			Clear,
		}

		[SerializeField] //And edit in inspector
		private string _flagName;
		public string FlagName { get { return _flagName; } }

		[SerializeField]
		private SpecialFlagOp _operation;
		public SpecialFlagOp Operation { get { return _operation; } }

		public override void OnEnter()
		{
			var flags = gameObject.GetComponent<StoryFlowchartExecutionContext>().ControllingTask.Story.Flags;
			switch (Operation)
			{
				case SpecialFlagOp.EnsureSet:
					flags.EnsureFlagSet(_flagName);
					break;
				case SpecialFlagOp.Clear:
					flags.ClearFlag(_flagName);
					break;
				case SpecialFlagOp.Increment:
					flags.IncrementFlag(_flagName);
					break;
				case SpecialFlagOp.Decrement:
					flags.DecrementFlag(_flagName);
					break;
			}
		}
	}
}

//*/