﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.Flags
{
	public class StoryFlags
	{
		private Dictionary<string, int> _flags;
		public ReadOnlyDictionary<string, int> Flags;

		public StoryFlags() : this(new Dictionary<string, int>()) { }

		public StoryFlags(Dictionary<string, int> flags)
		{
			this._flags = flags;
			this.Flags = new ReadOnlyDictionary<string, int>(this._flags);
		}



		public void EnsureFlagSet(string flagName)
		{
			if (flagName == null) throw new ArgumentNullException("flagName");

			int value;
			if (!_flags.TryGetValue(flagName, out value) || value == 0)
			{
				_flags.Add(flagName, 1);
			}
		}

		public void IncrementFlag(string flagName) {
			ChangeFlag(flagName, 1);
		}

		public void DecrementFlag(string flagName) {
			ChangeFlag(flagName, -1);
		}

		public void ClearFlag(string flagName)
		{
			if (flagName == null) throw new ArgumentNullException("flagName");

			_flags.Remove(flagName);
		}

		public void SetFlagTo(string flagName, int value)
		{
			if (flagName == null) throw new ArgumentNullException("flagName");

			SetFlagValueInternal(flagName, value);
		}

		/// <summary>
		/// Changes the value of the specified flag by the specified amount.
		/// Flags less than or equal to zero are removed. Returns the new value (0 if removed).
		/// </summary>
		/// <param name="flagName">The name of the flag to set. Cannot be null.</param>
		/// <param name="changeAmount">The positive or negative change amount</param>
		public int ChangeFlag(string flagName, int changeAmount)
		{
			if (flagName == null) throw new ArgumentNullException("flagName");

			int currentValue;
			if (_flags.TryGetValue(flagName, out currentValue)) {
				return SetFlagValueInternal(flagName, currentValue + changeAmount);
			} else {
				return SetFlagValueInternal(flagName, changeAmount);
			}
		}

		private int SetFlagValueInternal(string flagName, int value)
		{
			if (value <= 0) {
				_flags.Remove(flagName);
			} else {
				_flags[flagName] = value;
			}
			return value;
		}


		public int GetFlagValue(string flagName)
		{
			if (flagName == null) throw new ArgumentNullException("flagName");

			int flagValue;
			if (_flags.TryGetValue(flagName, out flagValue)) {
				return flagValue;
			} else {
				return 0;
			}
		}

		public bool IsFlagSet(string flagName)
		{
			//In case there's a zero left in by accident, don't just check using Contains
			int flagValue = GetFlagValue(flagName);
			return flagValue > 0;
		}
	}
}

//*/