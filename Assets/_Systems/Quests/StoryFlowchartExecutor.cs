﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;
using RevolitionGames.Utilities.Alex;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public class StoryFlowchartExecutor
	{
		public const string DefaultFirstBlockName = "Start";

        private static IDictionary<string, Flowchart> flowchartsCreated = new Dictionary<string, Flowchart>();

		private Flowchart _flowchart;
		public Flowchart Flowchart { get { return _flowchart; } }

		private Block _firstBlock;
		public Block FirstBlock { get { return _firstBlock; } }

		public bool IsExecutorRunning { get; private set; }

		public event EventHandler<TaskFlowchartStoppedEventArgs> FlowchartStopped;

		public StoryFlowchartExecutor(Flowchart flowchart, Block firstBlock)
		{
			if (flowchart == null) throw new ArgumentNullException("flowchart");
			if (firstBlock == null) throw new ArgumentNullException("firstBlock");

			this._flowchart = flowchart;
			this._firstBlock = firstBlock;
		}

		public StoryFlowchartExecutor(Flowchart flowchart, string firstBlockName = DefaultFirstBlockName)
			: this(flowchart, FlowchartUtils.GetBlock(flowchart, firstBlockName))
		{ }

		

		public void StartFlowchart(TaskSlot controllingTask = null)
		{
			Debug.Log(
				"Attempting to start flowchart '" + Flowchart.name + "', "
				+ "controlled by task '" + (controllingTask == null ? "" : controllingTask.CodeName) + "'."
			);

			if (IsExecutorRunning || Flowchart.GetComponent<StoryFlowchartExecutionContext>() != null || Flowchart.HasExecutingBlocks()) {
				throw new InvalidOperationException("The flowchart '" + Flowchart.GetName() + "' is already executing.");
			}
			StoryFlowchartExecutionContext.CreateForFlowchart(Flowchart, controllingTask);

			SubscribeToTaskEndCommands();
			BlockSignals.OnBlockEnd += OnAnyBlockEnded;
            Game.Current.InteractionOccurring = true;

			PlayModeStage.Current.PlayerUI.DialogOpen = true;

            Flowchart.enabled = true;
            Flowchart.gameObject.SetActive(true);

            Flowchart.ExecuteBlock(FirstBlock); //Guaranteed to return true because of checks done beforehand

			Debug.Log("Started flowchart.");
		}

        public static bool TryGetFlowchart(string name, out Flowchart flowchart) {
            return flowchartsCreated.TryGetValue(name, out flowchart);
        }

        public static void AddFlowchart(string name, Flowchart flowchart) {
            flowchartsCreated.Add(name, flowchart);
        }

        public static void RemoveFlowchart(string name) {
            flowchartsCreated.Remove(name);
        }

        private void SubscribeToTaskEndCommands()
		{
			foreach (TaskFlowchartCustomEnd customEnd in GetTaskEndCommands()) {
				customEnd.FlowchartStopped += OnTaskEndCommandExecuted;
			}
		}

		private void UnsubscribeFromTaskEndCommands()
		{
			foreach (TaskFlowchartCustomEnd customEnd in GetTaskEndCommands()) {
				customEnd.FlowchartStopped -= OnTaskEndCommandExecuted;
			}
		}

		public IEnumerable<TaskFlowchartCustomEnd> GetTaskEndCommands()
		{
			return from block in Flowchart.GetComponents<Block>()
			       from end in block.CommandList.OfType<TaskFlowchartCustomEnd>()
			       select end;
		}

		//Flowcharts have no finished event.
		//This is because they never really finish - though there may be no
		//running blocks, there may be one waiting for an event.
		//This finish detection does not account for that - as soon as there is
		//nothing running, the flowchart exits. To continue the flowchart, just use a
		//wait command or something.
		private void OnAnyBlockEnded(Block block)
		{
			//Debug.Log("#300");
			if (ReferenceEquals(block.GetFlowchart(), Flowchart) && FlowchartUtils.HasStalled(Flowchart))
			{
				//Debug.Log("#301");
				FinishFlowchart(
					new TaskFlowchartStoppedEventArgs(
						flowchart: Flowchart,
						lastBlock: block,
						additionalNextTasks: Enumerable.Empty<string>()
					)
				);
			}
		}

		private void OnTaskEndCommandExecuted(object sender, TaskFlowchartStoppedEventArgs e)
		{
			FinishFlowchart(e);
		}

		private void FinishFlowchart(TaskFlowchartStoppedEventArgs e)
		{
			UnsubscribeFromTaskEndCommands();
			BlockSignals.OnBlockEnd -= OnAnyBlockEnded;

			Flowchart.StopAllBlocks();
			Flowchart.StopAllCoroutines();

			var executionCtx = Flowchart.gameObject.GetComponent<StoryFlowchartExecutionContext>();
			if (executionCtx != null) UnityEngine.Object.Destroy(executionCtx); //Should be ok to destroy in the next frame

            Game.Current.InteractionOccurring = false;
			IsExecutorRunning = false;
			//Debug.Log("#470");
			PlayModeStage.Current.PlayerUI.DialogOpen = false;

			FlowchartStopped.Fire(this, e);
		}
	}

	public class TaskFlowchartStoppedEventArgs : EventArgs
	{
		public Flowchart Flowchart { get; private set; }
		public Block LastBlock { get; private set; }
		public IEnumerable<string> AdditionalNextTasks { get; private set; }

		public TaskFlowchartStoppedEventArgs(Flowchart flowchart, Block lastBlock, IEnumerable<string> additionalNextTasks)
		{
			if (flowchart == null) throw new ArgumentNullException("flowchart");
			if (lastBlock == null) throw new ArgumentNullException("lastBlock");
			if (additionalNextTasks == null) throw new ArgumentNullException("nextTasks");

			this.Flowchart = flowchart;
			this.LastBlock = lastBlock;
			this.AdditionalNextTasks = additionalNextTasks;
		}
	}
}

//*/