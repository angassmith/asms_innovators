﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using RevolitionGames.Quests.BaseStoryParts;

namespace RevolitionGames.Quests {
    /// <summary>
    /// Provides a method to compare two story part slots for equality, 
    /// based on their codename, display name and description.
    /// </summary>
    public class StoryPartSlotEqualityComparer : EqualityComparer<IStoryPartSlot> {

        public override bool Equals(IStoryPartSlot x, IStoryPartSlot y) {
            if (Object.ReferenceEquals(x, y)) {
                return true;
            }
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) {
                return false;
            }
            bool attributesEqual = (x.CodeName).Equals(y.CodeName) &&
                                    (x.DisplayName).Equals(y.DisplayName) &&
                                    (x.Description).Equals(y.Description);

            return attributesEqual;
        }

        public override int GetHashCode(IStoryPartSlot obj) {
            if (Object.ReferenceEquals(obj, null)) {
                return 0;
            }

            int hash = 17;
            hash = hash * 23 + obj.CodeName.GetHashCode();
            hash = hash * 23 + obj.DisplayName.GetHashCode();
            hash = hash * 23 + obj.Description.GetHashCode();

            return hash;
        }
    }

    /// <summary>
    /// Provides a method to compare two quest slots for equality.
    /// </summary>
    public class QuestSlotEqualityComparer : StoryPartSlotEqualityComparer {

        public override bool Equals(IStoryPartSlot x, IStoryPartSlot y) {
            if (Object.ReferenceEquals(x, y)) {
                return true;
            }

            if (!base.Equals(x,y)) {
                return false;
            } else {
                QuestSlot a = (QuestSlot)x;
                QuestSlot b = (QuestSlot)y;

                bool storiesEqual = base.Equals(a.Story.CurrentEpisodeSlot, b.Story.CurrentEpisodeSlot);
                //bool nextQuestsEqual = Enumerable.SequenceEqual(a.NextQuests, b.NextQuests, this); //TODO

                return storiesEqual;
            }
        }
    }

    /// <summary>
    /// Provides a method to compare two story part slots for equality, 
    /// based on their codename, display name and description.
    /// </summary>
    public class ActiveEpisodeEqualityComparer : EqualityComparer<ActiveEpisode> {

        public override bool Equals(ActiveEpisode x, ActiveEpisode y) {
            if (Object.ReferenceEquals(x, y)) {
                return true;
            }
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) {
                return false;
            }

            IEnumerable<IStoryPartSlot> sortedXQuests = (x.Quests.Values.OrderBy(i => i.CodeName)).Cast<IStoryPartSlot>();
            IEnumerable<IStoryPartSlot> sortedYQuests = (y.Quests.Values.OrderBy(i => i.CodeName)).Cast<IStoryPartSlot>();

            bool questsAreEqual = Enumerable.SequenceEqual(sortedXQuests, sortedYQuests, new StoryPartSlotEqualityComparer());

            return questsAreEqual; //TODO finish implementation
        }

        public override int GetHashCode(ActiveEpisode obj) {
            if (Object.ReferenceEquals(obj, null)) {
                return 0;
            }

            int hash = 17;
            hash = hash * 23 + obj.GetHashCode(); //TODO fix this

            return hash;
        }
    }
}
