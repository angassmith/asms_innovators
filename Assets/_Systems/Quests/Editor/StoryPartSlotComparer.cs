﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using RevolitionGames.Quests.BaseStoryParts;

namespace RevolitionGames.Quests {
    /// <summary>
    /// Provides a method to compare two story part slot objects.
    /// </summary>
    public class StoryPartSlotComparer : Comparer<IStoryPartSlot> {

        private StoryPartSlotEqualityComparer equalityComparer;

        public StoryPartSlotComparer() {
            equalityComparer = new StoryPartSlotEqualityComparer();
        }

		public override int Compare(IStoryPartSlot x, IStoryPartSlot y)
		{
			if (equalityComparer.Equals(x, y)) {
                return 0;
            } else {
                return equalityComparer.GetHashCode(x) - equalityComparer.GetHashCode(y);
            }
		}
    }
}
