﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using UnityEngine;

namespace RevolitionGames.Quests {

    /// <summary>
    /// Unit tests the XmlStoryProvider class.
    /// </summary>
    [TestFixture]
    public class XmlStoryProviderTest {

        private const string schemaFilepath = "Story/Schema/Story.xsd";
        private const string xmlStoryDirectory = "StoryProviderTest/";

        private XmlStoryProvider xmlStoryProvider;
        private Story story;

        [SetUp]
        public void Setup() {
            xmlStoryProvider = new XmlStoryProvider(schemaFilepath, xmlStoryDirectory);
            //-//	story = new Story(xmlStoryProvider);
        }

        [Test]
        public void T00GetDemoEpisodeSlot() {
            List<EpisodeSlot> episodeSlotCollection = xmlStoryProvider.GetEpisodeSlots(story);

            var expectedCollection = new List<EpisodeSlot>();
            var expectedEpisodeSlot = new EpisodeSlot(story, "demo", "Demo Episode", "An initial episode to demonstrate how the app works");
            expectedCollection.Add(expectedEpisodeSlot);

            /*Assert.AreEqual(1, episodeSlotCollection.Count);
            Assert.AreEqual("demo", episodeSlotCollection[0].CodeName);
            Assert.AreEqual("Demo Episode", episodeSlotCollection[0].DisplayName);
            Assert.AreEqual("An initial episode to demonstrate how the app works", episodeSlotCollection[0].Description);*/

            CollectionAssert.AreEqual(expectedCollection, episodeSlotCollection, new StoryPartSlotComparer());
        }

        [Test]
        public void T01LoadDemoEpisodeSlot() {
            List<EpisodeSlot> episodeSlotCollection = xmlStoryProvider.GetEpisodeSlots(story);

            ActiveEpisode activeEpisode = xmlStoryProvider.LoadEpisode(episodeSlotCollection[0]);

            QuestSlot expectedQuest = new QuestSlot(
				episodeSlotCollection[0],
                "demo/tutorial",
                "Tutorial",
                "Learn how to play the game"
			);
            var expectedQuests = new List<QuestSlot>();
            expectedQuests.Add(expectedQuest);

            IEpisodeCompletionRequirement expectedReq = new EpisodeCompletionRequirement(1, expectedQuests);
            var expectedCompletionReqs = new List<IEpisodeCompletionRequirement>();
            expectedCompletionReqs.Add(expectedReq);

            ActiveEpisode expectedActiveEpisode = new ActiveEpisode(episodeSlotCollection[0],
                expectedQuests,
				new QuestSlot[] { expectedQuests.First() }, //This might not be correct, idk
                expectedCompletionReqs,
                new List<BehaviourOverriding.BehaviourOverride>()
			);

            Assert.That(new ActiveEpisodeEqualityComparer().Equals(activeEpisode, expectedActiveEpisode));
        }
    }
}
