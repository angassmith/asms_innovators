﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.BaseStoryParts
{
	public sealed class UnusedStoryPartState<TActive, TSlot> : IStoryPartState<TActive, TSlot>
		where TActive : ActiveStoryPart<TActive, TSlot>
		where TSlot : StoryPartSlot<TActive, TSlot>
	{
		public Func<TActive> Activator { get; private set; }

		public UnusedStoryPartState(Func<TActive> activator)
		{
			if (activator == null) throw new ArgumentNullException("activator");
			this.Activator = activator;
		}

		public StoryPartStatus ToStoryPartStatus() { return StoryPartStatus.Unused; }

		public TActive Activate(bool throwIfActive) {
			return Activator.Invoke();
		}

		public DestroyedStoryPartState<TActive, TSlot> Destroy() {
			return new DestroyedStoryPartState<TActive, TSlot>();
		}
	}
}

//*/