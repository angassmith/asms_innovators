﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.BaseStoryParts
{
	public interface IStoryPartState
	{
		StoryPartStatus ToStoryPartStatus();
	}

	public interface IStoryPartState<TActive, TSlot> : IStoryPartState
		where TActive : ActiveStoryPart<TActive, TSlot>
		where TSlot : StoryPartSlot<TActive, TSlot>
	{
		TActive Activate(bool throwIfActive);
		DestroyedStoryPartState<TActive, TSlot> Destroy();
	}
}

//*/