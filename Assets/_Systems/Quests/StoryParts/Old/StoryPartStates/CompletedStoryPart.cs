﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.BaseStoryParts
{
	public sealed class CompletedStoryPartState<TActive, TSlot> : IStoryPartState<TActive, TSlot>
		where TActive : ActiveStoryPart<TActive, TSlot>
		where TSlot : StoryPartSlot<TActive, TSlot>
	{
		public StoryPartStatus ToStoryPartStatus() { return StoryPartStatus.Completed; }

		public TActive Activate(bool throwIfActive) {
			throw new InvalidOperationException("Cannot activate a completed story part.");
		}

		public DestroyedStoryPartState<TActive, TSlot> Destroy() {
			throw new InvalidOperationException(
				"Cannot destroy a completed story part (both destroyed and completed effectively "
				+ "mean destroyed - the difference is how the story part got there)."
			);
		}
	}
}

//*/