﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.BaseStoryParts
{
	public sealed class DestroyedStoryPartState<TActive, TSlot> : IStoryPartState<TActive, TSlot>
		where TActive : ActiveStoryPart<TActive, TSlot>
		where TSlot : StoryPartSlot<TActive, TSlot>
	{
		public StoryPartStatus ToStoryPartStatus() { return StoryPartStatus.Destroyed; }

		public TActive Activate(bool throwIfActive) {
			throw new InvalidOperationException("Cannot activate a destroyed story part.");
		}

		public DestroyedStoryPartState<TActive, TSlot> Destroy() {
			return this;
		}
	}
}

//*/