﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BaseStoryParts;

namespace RevolitionGames.Quests
{
	public class StoryPartEventArgs : EventArgs
	{
		public IStoryPartSlot StoryPart { get; private set; }

		public StoryPartEventArgs(IStoryPartSlot storyPart)
		{
			if (storyPart == null) throw new ArgumentNullException("storyPart");
			this.StoryPart = storyPart;
		}
	}

	public class StoryPartEventArgs<TSlot> : StoryPartEventArgs
		where TSlot : IStoryPartSlot
	{
		public new TSlot StoryPart { get; private set; }

		public StoryPartEventArgs(TSlot storyPart) : base(storyPart)
		{
			if (storyPart == null) throw new ArgumentNullException("storyPart");
			this.StoryPart = storyPart;
		}
	}

	//	public class StoryPartStateEventArgs : StoryPartEventArgs
	//	{
	//		public IStoryPartState OldState { get; private set; }
	//		public IStoryPartState NewState { get; private set; }
	//	
	//		public StoryPartStateEventArgs(IStoryPartState oldState, IStoryPartState newState, IStoryPartSlot storyPart)
	//			: base(storyPart)
	//		{
	//			if (oldState == null) throw new ArgumentNullException("oldState");
	//			if (newState == null) throw new ArgumentNullException("newState");
	//	
	//			this.OldState = oldState;
	//			this.NewState = newState;
	//		}
	//	}

	public class StoryPartStatusEventArgs : StoryPartEventArgs
	{
		public StoryPartStatus OldStatus { get; private set; }
		public StoryPartStatus NewStatus { get; private set; }

		public StoryPartStatusEventArgs(StoryPartStatus oldStatus, StoryPartStatus newStatus, IStoryPartSlot storyPart)
			: base(storyPart)
		{
			this.OldStatus = oldStatus;
			this.NewStatus = newStatus;
		}
	}
}

//*/