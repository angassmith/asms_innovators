﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests.BaseStoryParts
{
	public interface IActiveStoryPart : IDiscardable
	{
		IStoryPartSlot Slot { get; }
		ILookup<string, BehaviourOverride> BehaviourOverrides { get; }
	}
}

//*/