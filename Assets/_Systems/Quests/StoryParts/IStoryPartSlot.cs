﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests.BaseStoryParts
{
	public interface IStoryPartSlot : IDiscardable
	{
		string CodeName { get; }
		string DisplayName { get; }
		string Description { get; }
		Story Story { get; }

		StoryPartStatus Status { get; }
		IActiveStoryPart ActiveStoryPart { get; }

		event EventHandler<StoryPartStatusEventArgs> StatusChanged;
		event EventHandler<StoryPartEventArgs> Activated;
		event EventHandler<StoryPartEventArgs> Completed;
		event EventHandler<StoryPartEventArgs> Destroyed;
		event EventHandler<StoryPartEventArgs> Deactivated;
	}
}