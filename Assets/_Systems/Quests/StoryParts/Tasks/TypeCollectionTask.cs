﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Inventory;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.SceneComponents;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public class ActiveTypeCollectionTask : ActiveTask
	{
		public int NumRequired { get; private set; }

		public ItemDef Item { get; private set; }


		public ActiveTypeCollectionTask(TaskSlot slot, int numRequired, ItemDef item, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			if (numRequired <= 0) throw new ArgumentOutOfRangeException("numRequired", numRequired, "Must be greater than 0.");
			if (item == null) throw new ArgumentNullException("item");

			this.NumRequired = numRequired;
			this.Item = item;
		}

		public ActiveTypeCollectionTask(TaskSlot slot, int numRequired, ItemDef item, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, numRequired, item, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
			CheckExistingInventory();

			PlayModeStage.Current.PlayerUI.Inventory.ItemAdded += OnItemAddedOrUpdated;
			PlayModeStage.Current.PlayerUI.Inventory.ItemUpdated += OnItemAddedOrUpdated;
		}

		public void OnItemAddedOrUpdated(InventoryItem invItem, EventArgs e) {
			if (invItem.ItemReference == Item) {
				CompleteIfFinished(invItem);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		private void CheckExistingInventory()
		{
			InventoryItem invItem;
			if (PlayModeStage.Current.PlayerUI.Inventory.TryGetItem(Item, out invItem))
			{
				CompleteIfFinished(invItem);
			}
		}

		/// <summary>
		/// Checks whether the required items have been collected 
		/// and completes the task if they have.
		/// </summary>
		private void CompleteIfFinished(InventoryItem invItem) {

			if (invItem.Count >= NumRequired) {
				Debug.Log("Found required number of " + Item.CodeName + " items for task");
				Complete(Slot.NextTasks);
			}
		}
	}
}

//*/