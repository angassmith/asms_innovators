﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests
{
	class ActiveImpossibleDummyTask : ActiveTask
	{
		public ActiveImpossibleDummyTask(TaskSlot slot, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{ }

		public ActiveImpossibleDummyTask(TaskSlot slot, IEnumerable<BehaviourOverride> behaviourOverrides = null)
			: this(slot, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise() { }
	}
}

//*/