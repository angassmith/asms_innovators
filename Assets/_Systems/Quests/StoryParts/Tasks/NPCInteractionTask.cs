﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Interaction;
using RevolitionGames.Quests.BehaviourOverriding;
using UnityEngine.SceneManagement;

namespace RevolitionGames.Quests
{
	public class ActiveNPCInteractionTask : ActiveTask
	{
		public NPCInteract NPC { get; private set; }

        private string npcCodename;

        public ActiveNPCInteractionTask(TaskSlot slot, string npc, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			if (npc == null) throw new ArgumentNullException("npc");

			npcCodename = npc;
		}

		public ActiveNPCInteractionTask(TaskSlot slot, string npc, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, npc, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
            SceneManager.activeSceneChanged += OnActiveSceneChanged;
            SetNpcReference();
        }

		private void InteractCompleted(object sender, TaskFlowchartStoppedEventArgs e)
		{
			Complete(
				Enumerable.Concat(
					e.AdditionalNextTasks.Select(codeName => this.Quest.GetTask(codeName)),
					this.Slot.NextTasks
				)
			);
            SceneManager.activeSceneChanged -= OnActiveSceneChanged;
        }

        private void OnActiveSceneChanged(Scene sc1, Scene sc2) {
            // Remove the previous scene's NPC interact reference
            if (NPC != null) {
                NPC.InteractFinished -= InteractCompleted;
                NPC = null;
            }

            // Find the NPC in the new scene
            SetNpcReference();
        }

        private void SetNpcReference() {
            NPC = FindNpcInScene(npcCodename);
            if (NPC != null) {
                this.NPC.InteractFinished += InteractCompleted;
            }
        }

        private static NPCInteract FindNpcInScene(string codeName) {
            return UnityEngine.Object.FindObjectsOfType<NPCInteract>()
                   .Where(x => x != null)
                   .SingleOrDefault(x => x.NPCCodeName.CodeName.Equals(codeName));
        }
    }
}

//*/