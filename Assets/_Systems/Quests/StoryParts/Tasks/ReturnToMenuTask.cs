﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests
{
	public class ReturnToMenuTask : ActiveTask
	{
		public ReturnToMenuTask(TaskSlot slot, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			
		}

		public ReturnToMenuTask(TaskSlot slot, IEnumerable<BehaviourOverride> behaviourOverrides = null)
			: this(slot, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
			Game.Current.SwitchToMenu();
		}
	}
}

//*/