﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests
{
	public class ActiveFungusTask : ActiveTask
	{
		public StoryFlowchartExecutor FlowchartExecutor { get; private set; }

		public ActiveFungusTask(TaskSlot slot, StoryFlowchartExecutor flowchartExecutor, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			if (flowchartExecutor == null) throw new ArgumentNullException("flowchartExecutor");

			this.FlowchartExecutor = flowchartExecutor;
		}

		public ActiveFungusTask(TaskSlot slot, StoryFlowchartExecutor flowchartExecutor, IEnumerable<BehaviourOverride> behaviourOverrides = null)
			: this(slot, flowchartExecutor, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
			FlowchartExecutor.FlowchartStopped += OnFlowchartStopped;

			FlowchartExecutor.StartFlowchart(Slot);
		}

		private void OnFlowchartStopped(object sender, TaskFlowchartStoppedEventArgs e)
		{
			FlowchartExecutor.FlowchartStopped -= OnFlowchartStopped;
			this.Complete(
				Enumerable.Concat(
					Slot.NextTasks,
					e.AdditionalNextTasks.Select(taskCodeName => this.Quest.GetTask(taskCodeName))
				)
			);
		}
	}
}

//*/