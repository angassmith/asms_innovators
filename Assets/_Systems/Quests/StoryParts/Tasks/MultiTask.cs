﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MiscCollections;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.Quests.Saving;

namespace RevolitionGames.Quests
{
	public class ActiveMultiTask : ActiveTask
	{
		private readonly HashSet<TaskID> _taskIDs;
		public IEnumerable<TaskID> TaskIDs { get { return _taskIDs; } }

		public int NumRequired { get; private set; }

		public int NumCompleted { get; private set; }

		/// <param name="taskIDs">Note: Can be null</param>
		public ActiveMultiTask(TaskSlot slot, IEnumerable<TaskID> taskIDs, int numRequired, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			if (taskIDs == null) taskIDs = Enumerable.Empty<TaskID>();
			if (numRequired < 1) throw new ArgumentOutOfRangeException("numRequired", numRequired, "Must be at least 1.");
			taskIDs.ValidateItemsNonNull(message => new ArgumentException(message, "tasks"));

			this._taskIDs = new HashSet<TaskID>(taskIDs);
			this.NumRequired = numRequired;

			if (this._taskIDs.Count < numRequired) throw new ArgumentException("taskCodeNames contains fewer than numRequired distinct code-names.");
		}

		/// <param name="tasks">Note: Can be null</param>
		public ActiveMultiTask(TaskSlot slot, IEnumerable<TaskID> tasks, int numRequired, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, tasks, numRequired, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
            //StoryProgress storyProgress = this.Slot.Story.Progress; //TODO Uncomment and use when Story.Progress is implemented
            //NumCompleted = this.TaskIDs.Count(x => storyProgress.GetTaskProgress(x).Status == StoryPartStatus.Completed);
            NumCompleted = 0;
			if (NumCompleted >= NumRequired) {
				this.Complete(Slot.NextTasks);
			} else {
				this.Slot.Story.TaskCompleted += OnTaskCompleted;
			}
		}

		///<summary>Should only be used by IStoryProvider implementations</summary>
		public void AddTask(TaskID task)
		{
			if (task.IsEmpty) throw new ArgumentException("Cannot be empty.", "task");

			_taskIDs.Add(task);
		}

		private void OnTaskCompleted(object sender, StoryPartEventArgs<TaskSlot> e)
		{
			var evtTaskID = new TaskID(
				quest: e.StoryPart.QuestSlot.CodeName,
				task: e.StoryPart.CodeName
			);
			if (_taskIDs.Contains(evtTaskID))
			{
				NumCompleted++;

				if (NumCompleted >= NumRequired)
				{
					this.Slot.Story.TaskCompleted -= OnTaskCompleted;
					this.Complete(Slot.NextTasks);
				}
			}
		}
	}
}

//*/