﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.Quests.BaseStoryParts;
using MiscCollections;

namespace RevolitionGames.Quests
{
	public sealed class TaskSlot : StoryPartSlot<ActiveTask, TaskSlot>
	{
		public QuestSlot QuestSlot { get; private set; }

		public bool Visible { get; private set; }

		private HashSet<TaskSlot> _nextTasks;
		public IEnumerable<TaskSlot> NextTasks { get { return _nextTasks; } }

		public TaskID TaskID {
			get { return new TaskID(this.QuestSlot.CodeName, this.CodeName); }
		}

		public sealed override bool IsDiscarded {
			get { return this.QuestSlot.IsDiscarded || this.QuestSlot.IsDeactivated; }
		}
		private bool _hasRunDiscard;

		private TaskSlot(QuestSlot parentQuest, string codeName, string displayName, string description, IEnumerable<TaskSlot> nextTasks, bool visible, Func<TaskSlot, ActiveTask> customActivator = null)
			: base(
				story: ThrowIfArgNull(parentQuest, "parentQuest").Story,
				codeName: codeName,
				displayName: displayName,
				description: description,
				customActivator: customActivator
			)
		{
			ThrowIfArgNull(parentQuest, "parentQuest");
			if (nextTasks != null) {
				nextTasks.ValidateItemsNonNull(message => new ArgumentException(message, "nextTasks"));
			}

			this.QuestSlot = parentQuest;
			this._nextTasks = new HashSet<TaskSlot>(nextTasks ?? Enumerable.Empty<TaskSlot>());

			this.Visible = visible;

			this.QuestSlot.Discarded += OnDiscarded;
			this.QuestSlot.Deactivated += OnDiscarded;
		}

		/// <summary>Creates a new <see cref="TaskSlot"/> that represents a visible task.</summary>
		public static TaskSlot CreateVisible(QuestSlot parentQuest, string codeName, string displayName, string description, IEnumerable<TaskSlot> nextTasks = null, Func<TaskSlot, ActiveTask> customActivator = null)
		{
			return new TaskSlot(
				parentQuest: parentQuest,
				codeName: codeName,
				displayName: displayName,
				description: description,
				nextTasks: nextTasks,
				visible: true,
				customActivator: customActivator
			);
		}

		/// <summary>Creates a new <see cref="TaskSlot"/> that represents a hidden task with a custom display name and description.</summary>
		public static TaskSlot CreateHidden(QuestSlot parentQuest, string codeName, string displayName, string description, IEnumerable<TaskSlot> nextTasks = null, Func<TaskSlot, ActiveTask> customActivator = null)
		{
			return new TaskSlot(
				parentQuest: parentQuest,
				codeName: codeName,
				displayName: displayName,
				description: description,
				nextTasks: nextTasks,
				visible: false,
				customActivator: customActivator
			);
		}

		/// <summary>Creates a new <see cref="TaskSlot"/> that represents a hidden task with an automatic display name and description.</summary>
		public static TaskSlot CreateHidden(QuestSlot parentQuest, string codeName, IEnumerable<TaskSlot> nextTasks = null, Func<TaskSlot, ActiveTask> customActivator = null)
		{
			return new TaskSlot(
				parentQuest: parentQuest,
				codeName: codeName,
				displayName: "[Hidden Task '" + codeName + "']",
				description: "[A hidden task with code name '" + codeName + "'. If you can see this, there's a bug]",
				nextTasks: nextTasks,
				visible: false,
				customActivator: customActivator
			);
		}

		public void LoadAndApplyProgress()
		{
			if (Status != StoryPartStatus.Unused) throw CreateLoadProgressWhenNotUnusedEx();

			var progress = this.Story.Progress.GetTaskProgress(this.TaskID);

			SetLoadedStatus(progress.Status);
		}



		protected override ActiveTask FallbackActivateImpl()
		{
			return Story.StoryProvider.LoadTask(this);
		}

		public new ActiveTask Activate(bool throwIfActive)
		{
			return base.Activate(throwIfActive);
		}

		///<summary>Should only be used by IStoryProvider implementations</summary>
		public void AddNextTask(TaskSlot next)
		{
			if (next == null) throw new ArgumentNullException("next");
			_nextTasks.Add(next);
		}

		private void OnDiscarded(object sender, EventArgs e)
		{
			if (!_hasRunDiscard)
			{
				_hasRunDiscard = true;

				FireDiscarded();
			}
		}
	}

	//	public class SpecificCollectionTaskData : TaskSlot<ActiveSpecificCollectionTask, SpecificCollectionTaskData>
	//	{
	//		
	//	
	//		public ReadOnlyCollection<ITaskData> NextTasksList { get; private set; }
	//	
	//		public SpecificCollectionTaskData(QuestData containingQuest, string codeName, string displayName, string description, IEnumerable<BehaviourOverride> behaviourOverrides, string collectableItemName, int quantity, List<ITaskData> nextTasksList)
	//			: base(containingQuest, codeName, displayName, description, behaviourOverrides)
	//		{
	//			if (string.IsNullOrEmpty(collectableItemName)) throw new ArgumentException("collectableItemName is null or empty");
	//			if (quantity <= 0) throw new ArgumentOutOfRangeException("quantity", quantity, "quantity must be greater than 0");
	//			if (nextTasksList == null) throw new ArgumentNullException("nextTasksList");
	//	
	//			this.Quantity = quantity;
	//			this.CollectableItemName = collectableItemName;
	//			this.NextTasksList = nextTasksList.AsReadOnly();
	//		}
	//	
	//		protected override ActiveSpecificCollectionTask CreateInternal()
	//		{
	//			return new ActiveSpecificCollectionTask(this, ActiveStoryPartCtorToken);
	//		}
	//	
	//	
	//	}
	//	
	//	public class TypeCollectionTaskData : TaskSlot<ActiveTypeCollectionTask, TypeCollectionTaskData>
	//	{
	//		public int Quantity { get; private set; }
	//	
	//		public string ItemCodeName { get; private set; }
	//	
	//		public ReadOnlyCollection<ITaskData> NextTasksList { get; private set; }
	//	
	//		public TypeCollectionTaskData(QuestData containingQuest, string codeName, string displayName, string description, string itemCodeName, IEnumerable<BehaviourOverride> behaviourOverrides, int quantity, List<ITaskData> nextTasksList)
	//			: base(containingQuest, codeName, displayName, description, behaviourOverrides)
	//		{
	//			if (quantity <= 0) throw new ArgumentOutOfRangeException("quantity", quantity, "quantity must be greater than 0");
	//			if (string.IsNullOrEmpty(itemCodeName)) throw new ArgumentException("itemCodeName is null or empty");
	//			if (nextTasksList == null) throw new ArgumentNullException("nextTasksList");
	//	
	//			this.Quantity = quantity;
	//			this.ItemCodeName = itemCodeName;
	//			this.NextTasksList = nextTasksList.AsReadOnly();
	//		}
	//	
	//		protected override ActiveTypeCollectionTask CreateInternal()
	//		{
	//			return new ActiveTypeCollectionTask(this, ActiveStoryPartCtorToken);
	//		}
	//	}
	//	
	//	
	//	public class BehaviourOverrideTaskData : TaskSlot<ActiveBehaviourOverrideTask, BehaviourOverrideTaskData>
	//	{
	//		public CompletableBehaviourOverride Behaviour { get; private set; }
	//	
	//		
	//	
	//		public BehaviourOverrideTaskData(QuestData containingQuest, string codeName, string displayName, string description, CompletableBehaviourOverride behaviour, IEnumerable<BehaviourOverride> behaviourOverrides, List<ITaskData> nextTasksList)
	//			: base(containingQuest, codeName, displayName, description, behaviourOverrides)
	//		{
	//			if (behaviour == null) throw new ArgumentNullException("behaviour");
	//			if (nextTasksList == null) throw new ArgumentNullException("nextTasksList");
	//	
	//			this.Behaviour = behaviour;
	//			this.NextTasksList = nextTasksList.AsReadOnly();
	//		}
	//	
	//		protected override ActiveBehaviourOverrideTask CreateInternal()
	//		{
	//			return new ActiveBehaviourOverrideTask(this, ActiveStoryPartCtorToken);
	//		}
	//	}
	//	
	//	public class DummyTaskData : TaskSlot<ActiveDummyTask, DummyTaskData>
	//	{
	//		public ReadOnlyCollection<ITaskData> NextTasksList { get; private set; }
	//	
	//		public DummyTaskData(QuestData containingQuest, string codeName, string displayName, string description, IEnumerable<BehaviourOverride> behaviourOverrides, List<ITaskData> nextTasksList) 
	//			: base(containingQuest, codeName, displayName, description, behaviourOverrides)
	//		{
	//			if (nextTasksList == null) throw new ArgumentNullException("nextTasksList");
	//			this.NextTasksList = nextTasksList.AsReadOnly();
	//		}
	//	
	//		protected override ActiveDummyTask CreateInternal()
	//		{
	//			return new ActiveDummyTask(this, ActiveStoryPartCtorToken);
	//		}
	//	
	//	}
}

//*/