﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Choices;
using RevolitionGames.Quests.BehaviourOverriding;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public class ActiveQuestEndTask : ActiveTask
	{
		public ActiveQuestEndTask(TaskSlot slot, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{ }

		public ActiveQuestEndTask(TaskSlot slot, IEnumerable<BehaviourOverride> behaviourOverrides = null)
			: this(slot, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
            // Create the quest end canvas and subscribe to the close event
            QuestEndCanvas qeCanvas = PlayModeStage.Current.PlayerUI.DisplayQuestEnd(Quest.Slot.DisplayName, Quest.ChoicesMade);
            qeCanvas.DialogClosed += QuestFinished;
		}

        // Activate the next tasks
        private void QuestFinished(GameObject g, EventArgs e) {
            Complete(Slot.NextTasks);
        }
	}
}

//*/