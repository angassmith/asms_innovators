﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests
{
	public class ActiveDummyTask : ActiveTask
	{
		public ActiveDummyTask(TaskSlot slot, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			//Do not Complete() here as the Activate event hasn't yet fired. Instead, use OnFullyActivated()
		}

		public ActiveDummyTask(TaskSlot slot, IEnumerable<BehaviourOverride> behaviourOverrides = null)
			: this(slot, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
			Complete(Slot.NextTasks);
		}
	}
}

//*/