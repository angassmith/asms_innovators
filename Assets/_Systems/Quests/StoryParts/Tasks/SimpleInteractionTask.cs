﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevolitionGames.Interaction;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames.Quests
{
	public class ActiveSimpleInteractionTask : ActiveTask
	{
		private SimpleInteract _interactionTarget;
		public SimpleInteract InteractionTarget {
			get {
				if (_interactionTarget == null)
				{
                    FindInteractionTarget();
				}
                return _interactionTarget;
			}
		}

        private string _interactionCodename;

		public ActiveSimpleInteractionTask(TaskSlot slot, string codename, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			if (codename == null) throw new ArgumentNullException("codename");

            this._interactionCodename = codename;
		}

		public ActiveSimpleInteractionTask(TaskSlot slot, string codename, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, codename, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
			SceneManager.activeSceneChanged += OnActiveSceneChanged;
			//FIXED (using OnDiscarded() methods)
			//	//BUG: This is never unsubscribed from, which means that after entering a minigame
			//	//the SimpleInteractionTask, and all of the rest of the Story it is part of, is still
			//	//referenced, and still subscribes to its target's InteractOccurred event. When the
			//	//target is interact with, Complete runs, and other stuff in the old story runs.
			//	//Potential fixes:
			//	//This could be fixed by using a scene change event that is relative to the current
			//	//PlayModeStage, however using OnDiscarded() methods might be better, as that would
			//	//avoid needing new events in every similar situation. There also might be similar problems
			//	//with quests being discarded when changing episodes, or tasks being discarded when
			//	//changing quests, which relative events would not handle very well.

            FindInteractionTarget();
        }

		private void InteractCompleted(object sender, EventArgs e)
		{
			_interactionTarget.InteractOccurred -= InteractCompleted;
			SceneManager.activeSceneChanged -= OnActiveSceneChanged;
			
			_interactionTarget.IsInteractable = false;
			
			Complete(Slot.NextTasks);
        }

        private void OnActiveSceneChanged(Scene sc1, Scene sc2)
		{
            // Remove the previous scene's interaction target
			if (!ReferenceEquals(_interactionTarget, null)) {
				_interactionTarget.InteractOccurred -= InteractCompleted;
				_interactionTarget = null;
			}

            // Find the interaction target in the new scene
            FindInteractionTarget();
        }

        private void FindInteractionTarget()
		{
			this._interactionTarget = null;
            var newInteractionTarget = FindInteractable(_interactionCodename);

			if (newInteractionTarget != null)
			{
				this._interactionTarget = newInteractionTarget; //Only set field if not null **or destroyed**.
                _interactionTarget.InteractOccurred += InteractCompleted;
                _interactionTarget.IsInteractable = true;
            }
        }

        private static SimpleInteract FindInteractable(string codeName)
		{
			return UnityEngine.Object.FindObjectsOfType<SimpleInteract>()
			       .Where(x => x != null)
				   .SingleOrDefault(x => x.SimpleCodeName.CodeName == codeName);
        }

		protected override void OnDiscardedImpl()
		{
			base.OnDiscardedImpl();

			Debug.Log("Discarding simple interaction task");

			if (!ReferenceEquals(_interactionTarget, null))
			{
				_interactionTarget.IsInteractable = false;
				_interactionTarget.InteractOccurred -= InteractCompleted;
				_interactionTarget = null;
			}

			SceneManager.activeSceneChanged -= OnActiveSceneChanged;

			Debug.Log("Discarded simple interaction task");
		}
	}
}

//*/