﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.SceneComponents;

namespace RevolitionGames.Quests
{
	//TODO: Possibly remove this as so far it hasn't been used

	public class ActiveSpecificCollectionTask : ActiveTask
	{
		public int NumRequired { get; private set; }

		public CollectableItem Item { get; private set; }

		public int NumCollected { get; private set; }

		public ActiveSpecificCollectionTask(TaskSlot slot, int numRequired, CollectableItem item, ILookup<string, BehaviourOverride> behaviourOverrides) 
			: base(slot, behaviourOverrides)
		{
			if (numRequired <= 0) throw new ArgumentOutOfRangeException("numRequired", numRequired, "Must be greater than 0.");
			if (item == null) throw new ArgumentNullException("item");

			this.NumRequired = numRequired;
			this.Item = item;

			this.NumCollected = 0;
		}

		public ActiveSpecificCollectionTask(TaskSlot slot, int numRequired, CollectableItem item, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, numRequired, item, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
			//TODO: Check if already collected
			this.Item.Collected += OnItemCollected;
		}


		private void OnItemCollected(object sender, ItemEventArgs e)
		{
			NumCollected += 1;

			if (NumCollected >= NumRequired)
			{
				Complete(Slot.NextTasks);
			}
		}
	}
}

//*/