﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames;
using RevolitionGames.Quests.BehaviourOverriding;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RevolitionGames.Quests
{
	public class ActiveSceneChangeTask : ActiveTask
	{
        private string sceneToChangeTo;
        private int spawnPosition;

		public ActiveSceneChangeTask(TaskSlot slot, string sceneName, int spawnPos, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
            if (sceneName == null) throw new ArgumentNullException("sceneName");
            if (String.IsNullOrEmpty(sceneName)) throw new ArgumentException("Scene name for scene change task was not specified.", sceneName);
            if (spawnPos < 0) throw new ArgumentOutOfRangeException("spawnPos", spawnPos, "Must be greater than or equal to 0.");

            sceneToChangeTo = sceneName;
            spawnPosition = spawnPos;
        }

		public ActiveSceneChangeTask(TaskSlot slot, string sceneName, int spawnPos, IEnumerable<BehaviourOverride> behaviourOverrides = null)
			: this(slot, sceneName, spawnPos, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
            // Clear the inventory of items //TODO determine if this should be removed in future
            PlayModeStage.Current.PlayerUI.Inventory.ClearAllItems();

			var transition = PlayModeStage.Current.SwitchToPlaymodeScene(
				sceneName: sceneToChangeTo,
				spawnPointNum: spawnPosition
			);
			transition.Completed += SceneChangeFinished;
        }

		// Activate the next tasks
		private void SceneChangeFinished(object sender, SceneTransitionEventArgs e)
		{
			e.Transition.Completed -= SceneChangeFinished;
			Complete(Slot.NextTasks);
		}
    }
}

//*/