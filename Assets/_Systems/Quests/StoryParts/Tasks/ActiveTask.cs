﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RevolitionGames.Interaction;
using RevolitionGames.Inventory;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.SceneComponents;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public abstract class ActiveTask : ActiveStoryPart<ActiveTask, TaskSlot>
	{
		public ActiveQuest Quest {
			get { return Slot.QuestSlot.ActiveStoryPart; }
		}

		public sealed override bool IsDiscarded { get { return this.Slot.IsDiscarded || this.Slot.IsDeactivated; } }
		private bool _hasRunDiscard;

		protected ActiveTask(TaskSlot slot, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			if (!slot.QuestSlot.IsActive) {
				//Throwing an error makes constructing dummy tasks for testing difficult
				Debug.LogWarning(
					"The provided " + typeof(TaskSlot).Name + "'s parent " + typeof(QuestSlot).Name + " is inactive."
				);
			}

			this.Slot.Discarded += OnDiscarded;
			this.Slot.Deactivated += OnDiscarded;
		}

		protected ActiveTask(TaskSlot slot, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected void Complete(IEnumerable<TaskSlot> nextTasks)
		{
			//Unlock all next tasks (unless they're completed or destroyed)
			foreach (TaskSlot task in nextTasks)
			{
				task.ActivateIfPossible();
			}

			Complete();
		}

		private void OnDiscarded(object sender, EventArgs e)
		{
			if (!_hasRunDiscard)
			{
				_hasRunDiscard = true;

				OnDiscardedImpl();

				FireDiscarded();
			}
		}

		protected virtual void OnDiscardedImpl() { }
	}



	//I don't think this is necessary or a good idea
	//It seems to add complexity more than reducing it, for no relevant gain
	//It also doesn't work for replacing eg. interaction tasks, as it only allows one response
	//from the npc, rather than a random selection from multiple. The same applies to other tasks.
	//
	//	public class ActiveBehaviourOverrideTask : ActiveTask
	//	{
	//		public CompletableBehaviourOverride Behaviour { get; private set; }
	//	
	//		public ActiveBehaviourOverrideTask(TaskSlot slot, CompletableBehaviourOverride behaviour, ILookup<string, BehaviourOverride> behaviourOverrides)
	//			: base(slot, behaviourOverrides)
	//		{
	//			if (behaviour == null) throw new ArgumentNullException("behaviour");
	//	
	//			this.Behaviour = behaviour;
	//		}
	//	
	//		public ActiveBehaviourOverrideTask(TaskSlot slot, CompletableBehaviourOverride behaviour, IEnumerable<BehaviourOverride> behaviourOverrides)
	//			: this(slot, behaviour, MakeBehaviourOverridesLookup(behaviourOverrides))
	//		{ }
	//	
	//		private void Behaviour_Completed(object sender, EventArgs e)
	//		{
	//			Complete(Slot.NextTasks);
	//		}
	//	}
}




