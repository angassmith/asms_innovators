﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MiscCollections;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests.BaseStoryParts
{
	public abstract class ActiveStoryPart<TActive, TSlot> : IActiveStoryPart
		where TActive : ActiveStoryPart<TActive, TSlot>
		where TSlot : StoryPartSlot<TActive, TSlot>
	{
		public TSlot Slot { get; private set; }
		IStoryPartSlot IActiveStoryPart.Slot { get { return Slot; } }

		public ILookup<string, BehaviourOverride> BehaviourOverrides { get; private set; }

		public event EventHandler Completed;

		public abstract bool IsDiscarded { get; }
		public event EventHandler<DiscardEventArgs> Discarded;
		protected void FireDiscarded() {
			Discarded.Fire(this, new DiscardEventArgs(this));
			Discarded = null;
		}

		public ActiveStoryPart(TSlot slot, ILookup<string, BehaviourOverride> behaviourOverrides)
		{
			if (slot == null) throw new ArgumentNullException("slot");
			if (behaviourOverrides == null) throw new ArgumentNullException("behaviourOverrides");
			behaviourOverrides.SelectMany(x => x).ValidateItemsNonNull(message => new ArgumentException(message, "behaviourOverrides"));

			if (slot.IsDiscarded) throw new ArgumentDiscardedException("slot");

			this.Slot = slot;
			this.BehaviourOverrides = behaviourOverrides;

			slot.AfterActivated += (s, e) => Initialise();
			// ^ Not firing yet so safe to subscribe.
			//Running Activated subscribers before initialisation occurs is not ideal,
			//as they can't see eg. activated initial tasks, but there's no good alternative,
			//as the initial tasks could complete immediately, completing the quest (and making
			//Completed fire before Activated).

			slot.Destroying += (s, e) => OnDestroying();
		}

		public ActiveStoryPart(TSlot slot, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		/// <summary>
		/// Run as the last subscriber to Slot.AfterActivated.
		/// Use this to start various processes (eg. Fungus flowcharts), subscribe to events,
		/// check for immediate completion, activate initial stub-story-parts, and other things.
		/// If these are attempted in the constructor, it risks trying to destroy or complete the
		/// story part while still in the constructor -- which would cause problems as the constructor
		/// runs before the story part is activated. Additionally, some parts of the initialisation,
		/// such as activating initial tasks in a quest, require the quest to be in the Active state,
		/// which is not the case while the ActiveStoryPart is still being constructed.
		/// </summary>
		protected abstract void Initialise();

		protected virtual void OnDestroying() { }

		protected void Complete()
		{
			Completed.Fire(this, EventArgs.Empty);
		}

		/// <summary>
		/// Converts a collection of <see cref="BehaviourOverride"/>s
		/// into a <see cref="Lookup{string, BehaviourOverride}"/>,
		/// keyed by the <see cref="BehaviourOverride.TargetCodeName"/>.
		/// </summary>
		/// <param name="behaviourOverrides">Note: Can be null. If null, an empty ILookup will be returned.</param>
		public static ILookup<string, BehaviourOverride> MakeBehaviourOverridesLookup(IEnumerable<BehaviourOverride> behaviourOverrides)
		{
			if (behaviourOverrides == null) behaviourOverrides = Enumerable.Empty<BehaviourOverride>();

			return behaviourOverrides.ToLookup(x => x.TargetCodeName);
		}
	}
}

//*/