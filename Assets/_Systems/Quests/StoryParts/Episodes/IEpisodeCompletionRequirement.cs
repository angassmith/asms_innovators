﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests
{
	public interface IEpisodeCompletionRequirement
	{
		/// <summary>
		/// True if the <see cref="IEpisodeCompletionRequirement"/> is possible, otherwise false.
		/// See <see cref="ImpossibleEpisodeCompletionRequirement"/>
		/// </summary>
		bool IsPossible { get; }

		/// <summary>
		/// True if <see cref="NumCompleted"/> is greater than or equal to <see cref="NumRequired"/>.
		/// Always false if <see cref="IsPossible"/> is false.
		/// </summary>
		bool CriteriaAreMet { get; }

		/// <summary>
		/// The number of quests in <see cref="Quests"/> that have been completed.
		/// Always 0 if <see cref="IsPossible"/> is false.
		/// </summary>
		int NumCompleted { get; }

		/// <summary>
		/// The number of quests in <see cref="Quests"/> that must be completed to meet
		/// the <see cref="EpisodeCompletionRequirement"/>.
		/// Always 1 if <see cref="IsPossible"/> is false.
		/// </summary>
		int NumRequired { get; }

		/// <summary>
		/// The quests whose completion contributes toward meeting this <see cref="IEpisodeCompletionRequirement"/>.
		/// If <see cref="IsPossible"/> is false, this should return an empty collection
		/// (there's not really any sensible value to return).
		/// </summary>
		ReadOnlyCollection<QuestSlot> Quests { get; }

		/// <summary>
		/// Fired after <see cref="CriteriaAreMet"/> becomes true.
		/// </summary>
		event EventHandler CriteriaMet;

		/// <summary>
		/// Fired after <see cref="NumCompleted"/> is incremented.
		/// </summary>
		event EventHandler<StoryPartEventArgs<QuestSlot>> QuestCompleted;
	}
}