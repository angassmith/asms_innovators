﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.BehaviourOverriding;
using UnityEngine;

namespace RevolitionGames.Quests
{
    /// <summary>
    /// Contains the details of an episode, and provides a method to activate it.
    /// </summary>
	public sealed class EpisodeSlot : StoryPartSlot<ActiveEpisode, EpisodeSlot>
	{
        /// <summary>Fired when the relevant event is fired by a contained quest. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestActivated;
		/// <summary>Fired when the relevant event is fired by a contained quest. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestCompleted;
		/// <summary>Fired when the relevant event is fired by a contained quest. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestDestroyed;
		/// <summary>Fired when the relevant event is fired by a contained quest. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestDeactivated;
		/// <summary>Fired when the relevant event is fired by a contained quest. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartStatusEventArgs> QuestStatusChanged;
		
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskActivated;
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskCompleted;
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskDestroyed;
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskDeactivated;
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartStatusEventArgs> TaskStatusChanged;

		/// <summary>Fired when <see cref="StoryPartSlot{TActive, TSlot}.StatusChanged"/>, <see cref="QuestStatusChanged"/>, or <see cref="TaskStatusChanged"/> is fired. Cleared automatically when the episode is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartStatusEventArgs> ProgressChanged;

		public sealed override bool IsDiscarded { get { return this.Story.IsDiscarded; } }

		public static EpisodeSlot CreateFromXml(Story containingStory, XElement xElement) {
            if (containingStory == null) throw new ArgumentNullException("containingStory");
            if (xElement == null) throw new ArgumentNullException("xElement");

            return new EpisodeSlot(containingStory,
                GetCodeNameFromXml(xElement),
                GetDisplayNameFromXml(xElement),
                GetDescriptionFromXml(xElement)
            );
        }

        public EpisodeSlot(Story story, string codeName, string displayName, string description, Func<EpisodeSlot,  ActiveEpisode> customActivator = null)
			: base(
				story: story,
				codeName: codeName,
				displayName: displayName,
				description: description,
				customActivator: customActivator
			)
		{
			this.StatusChanged += (s, e) => ProgressChanged.Fire(this, e);

			this.Deactivated += (s, e) => {
				this.QuestActivated = null;
				this.QuestCompleted = null;
				this.QuestDestroyed = null;
				this.QuestDeactivated = null;
				this.QuestStatusChanged = null;
				this.TaskActivated = null;
				this.TaskCompleted = null;
				this.TaskDestroyed = null;
				this.TaskDeactivated = null;
				this.TaskStatusChanged = null;
			};

			story.Discarded += OnDiscarded;
		}

		public void LoadAndApplyProgress()
		{
			if (Status != StoryPartStatus.Unused) throw CreateLoadProgressWhenNotUnusedEx();

			StoryPartStatus progress = this.Story.Progress.GetEpisodeProgress(this.CodeName);

			SetLoadedStatus(progress);

			//Sub-quests' progress is loaded and applied by ActiveEpisode.Initialise()
		}

		protected override ActiveEpisode FallbackActivateImpl()
		{
			var active = Story.StoryProvider.LoadEpisode(this);
			foreach (var quest in active.Quests.Values)
			{
				quest.Activated += OnQuestActivated;
				quest.StatusChanged += OnQuestStatusChanged;

				quest.TaskActivated += OnTaskActivated; //Some tasks are activated immediately so we can't put these
				quest.TaskCompleted += OnTaskCompleted; //in OnQuestActivated (as that runs after the
				quest.TaskDestroyed += OnTaskDestroyed; //immediately-activated tasks are activated,
				quest.TaskStatusChanged += OnTaskStatusChanged; //and potentially completed or destroyed)
			}

			//Unsubscribe from events if this episode is deactivated before the quests/tasks
			//are activated, completed etc.
			this.Deactivated += delegate
			{
				foreach (var quest in active.Quests.Values)
				{
					quest.Activated -= OnQuestActivated;
					quest.Completed -= OnQuestCompleted;
					quest.Destroyed -= OnQuestDestroyed;
					quest.StatusChanged -= OnQuestStatusChanged;
					quest.TaskActivated -= OnTaskActivated;
					quest.TaskCompleted -= OnTaskCompleted;
					quest.TaskDestroyed -= OnTaskDestroyed;
					quest.TaskStatusChanged -= OnTaskStatusChanged;
				}
			};
			
			return active;
		}

		private void OnQuestStatusChanged(object sender, StoryPartStatusEventArgs e) {
			QuestStatusChanged.Fire(this, e);
			ProgressChanged.Fire(this, e);
		}
		private void OnQuestActivated(object sender, StoryPartEventArgs<QuestSlot> e) {
			QuestActivated.Fire(this, e);
			e.StoryPart.Completed += OnQuestCompleted;
			e.StoryPart.Destroyed += OnQuestDestroyed;
		}
		private void OnQuestCompleted(object sender, StoryPartEventArgs<QuestSlot> e) {
			QuestCompleted.Fire(this, e);
			QuestDeactivated.Fire(this, e);
		}
		private void OnQuestDestroyed(object sender, StoryPartEventArgs<QuestSlot> e) {
			QuestDestroyed.Fire(this, e);
			QuestDeactivated.Fire(this, e);
		}
		private void OnTaskStatusChanged(object sender, StoryPartStatusEventArgs e) {
			TaskStatusChanged.Fire(this, e);
			ProgressChanged.Fire(this, e);
		}
		private void OnTaskActivated(object sender, StoryPartEventArgs<TaskSlot> e) {
			TaskActivated.Fire(this, e);
		}
		private void OnTaskCompleted(object sender, StoryPartEventArgs<TaskSlot> e) {
			TaskCompleted.Fire(this, e);
			TaskDeactivated.Fire(this, e);
		}
		private void OnTaskDestroyed(object sender, StoryPartEventArgs<TaskSlot> e) {
			TaskDestroyed.Fire(this, e);
			TaskDeactivated.Fire(this, e);
		}

		private void OnDiscarded(object sender, EventArgs e)
		{
			FireDiscarded();
		}
	}
}



