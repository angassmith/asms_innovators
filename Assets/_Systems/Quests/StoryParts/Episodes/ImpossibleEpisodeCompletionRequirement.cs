﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests
{
	public class ImpossibleEpisodeCompletionRequirement : IEpisodeCompletionRequirement
	{
		public bool IsPossible { get { return false; } }
		public bool CriteriaAreMet { get { return false; } }

		public int NumCompleted { get { return 0; } }
		public int NumRequired { get { return 1; } }

		private ReadOnlyCollection<QuestSlot> _quests = new List<QuestSlot>().AsReadOnly();
		public ReadOnlyCollection<QuestSlot> Quests { get { return _quests; } }

		public event EventHandler CriteriaMet;
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestCompleted;

		public ImpossibleEpisodeCompletionRequirement() { }
	}
}

//*/