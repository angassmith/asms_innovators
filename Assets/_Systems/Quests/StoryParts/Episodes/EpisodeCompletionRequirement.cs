﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using RevolitionGames.Quests.BaseStoryParts;

namespace RevolitionGames.Quests
{
	//This could be abstracted to allow different types of requirements,
	//but that would be a pain for the UI, would take time, and is not necessary

	public class EpisodeCompletionRequirement : IEpisodeCompletionRequirement
	{
		public bool IsPossible { get { return true; } }

		public ReadOnlyCollection<QuestSlot> Quests { get; private set; }
		public int NumRequired { get; private set; }

		public int NumCompleted { get; private set; }
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestCompleted;

		public bool CriteriaAreMet { get { return NumCompleted >= NumRequired; } }

		/// <summary>Note: Only fired once, so is cleared automatically.</summary>
		public event EventHandler CriteriaMet;

        
        private static XName XNumQuestsReq = XmlStoryProvider.QuestNamespace + "numQuestsRequired";
        private static XName XPossibleQuests = XmlStoryProvider.QuestNamespace + "possibleQuests";

        public static EpisodeCompletionRequirement CreateFromXml(XElement xElement, IDictionary<string, QuestSlot> quests) {
            if (xElement == null) throw new ArgumentNullException("xElement");
            if (xElement == null) throw new ArgumentNullException("quests");

            return new EpisodeCompletionRequirement(GetNumQuestsRequired(xElement), GetQuestList(xElement, quests));

        }

        public EpisodeCompletionRequirement(int numRequired, IList<QuestSlot> quests)
		{
			if (quests == null) throw new ArgumentNullException("quests");
			if (quests.Count == 0) throw new ArgumentException("Cannot be empty.", "quests");
			if (numRequired <= 0) throw new ArgumentOutOfRangeException("Must be at least 1.", "numRequired");
			if (numRequired > quests.Count) throw new ArgumentException("numRequired is greater than the number of quests.");

			this.Quests = new ReadOnlyCollection<QuestSlot>(quests);
			this.NumRequired = numRequired;

			foreach (var quest in quests) {
				quest.Completed += OnQuestCompleted;
			}

			this.NumCompleted = quests.Where(q => q.Status == StoryPartStatus.Completed).Count();
			CompleteIfCriteriaMet();
		}

		public EpisodeCompletionRequirement(int numRequired, params QuestSlot[] quests)
			: this(numRequired, (IList<QuestSlot>)quests)
		{ }

		private void OnQuestCompleted(object sender, StoryPartEventArgs<QuestSlot> e)
		{
			NumCompleted += 1;

			QuestCompleted.Fire(this, e);

			CompleteIfCriteriaMet();
		}

		private void CompleteIfCriteriaMet()
		{
			if (NumCompleted >= NumRequired)
			{
				foreach (var quest in this.Quests) {
					quest.Completed -= OnQuestCompleted;
				}
				CriteriaMet.Fire(this, EventArgs.Empty);
				CriteriaMet = null;
			}
		}



        private static int GetNumQuestsRequired(XElement xCompletionReq) {
            return (int)xCompletionReq.Element(XNumQuestsReq);
        }

        private static IList<QuestSlot> GetQuestList(XElement xCompletionReq, IDictionary<string, QuestSlot> quests) {

            var possibleQuests = new List<QuestSlot>();

            foreach (XElement xQuestRef in xCompletionReq.Element(XPossibleQuests).Descendants(XmlStoryProvider.XQuestRef)) {
                QuestSlot quest = null;
                if (quests.TryGetValue(xQuestRef.Attribute(XmlStoryProvider.X_CODENAME).Value, out quest)) {
                    possibleQuests.Add(quest);
                }
            }

            return possibleQuests;
        }
    }
}

//*/