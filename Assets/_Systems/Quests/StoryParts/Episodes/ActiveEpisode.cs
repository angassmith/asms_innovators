﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MiscCollections;
using RevolitionGames.Choices;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.BehaviourOverriding;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public sealed class ActiveEpisode : ActiveStoryPart<ActiveEpisode, EpisodeSlot>
	{
		private readonly Dictionary<string, QuestSlot> _quests;
		public ReadOnlyDictionary<string, QuestSlot> Quests { get; private set; }

		public ReadOnlyCollection<QuestSlot> InitialQuests { get; private set; }

		public IEnumerable<ActiveQuest> ActiveQuests { get { return Quests.Values.Where(x => x.IsActive).Select(x => x.ActiveStoryPart); } }

		public ReadOnlyCollection<IEpisodeCompletionRequirement> CompletionRequirements { get; private set; }

		public sealed override bool IsDiscarded { get { return this.Slot.IsDiscarded || this.Slot.IsDeactivated; } }
		private bool _hasRunDiscard;

		public ActiveEpisode(EpisodeSlot slot, IEnumerable<QuestSlot> quests, IList<QuestSlot> initialQuests, IList<IEpisodeCompletionRequirement> completionRequirements, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			if (quests == null) throw new ArgumentNullException("quests");
			if (initialQuests == null) throw new ArgumentNullException("initialQuests");
			if (completionRequirements == null) throw new ArgumentNullException("completionRequirements");
			if (completionRequirements.Count == 0) throw new ArgumentException("Cannot be empty.", "completionRequirements");
			quests.ValidateItemsNonNull(message => new ArgumentException(message, "quests"));
			initialQuests.ValidateItemsNonNull(message => new ArgumentException(message, "initialQuests"));
			completionRequirements.ValidateItemsNonNull(message => new ArgumentException(message, "completionRequirements"));
			initialQuests.ValidateItems(
				validationName: "contained in quests",
				isValidPredicate: x => quests.Contains(x),
				exceptionFactory: message => new ArgumentException(message, "initialQuests")
			);

			this._quests = quests.ToDictionary(x => x.CodeName); //Note: ToDictionary means that if two quests have the same CodeName, an exception will be thrown.
			this.Quests = new ReadOnlyDictionary<string, QuestSlot>(this._quests);

			this.InitialQuests = new ReadOnlyCollection<QuestSlot>(initialQuests);
			this.CompletionRequirements = new ReadOnlyCollection<IEpisodeCompletionRequirement>(completionRequirements);

			this.Slot.Discarded += OnDiscarded;
			this.Slot.Deactivated += OnDiscarded;
		}

		public ActiveEpisode(EpisodeSlot slot, IEnumerable<QuestSlot> quests, IList<QuestSlot> initialQuests, IList<IEpisodeCompletionRequirement> completionRequirements, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, quests, initialQuests, completionRequirements, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
			//Load and apply progress of sub-quests (now that everything's had a chance to subscribe to
			//events on them etc, but before activating the initial quests)
			foreach (var quest in this.Quests.Values) {
				quest.LoadAndApplyProgress();
			}

			bool complete = CompleteIfAllRequirementsMet();

			if (!complete)
			{
				foreach (IEpisodeCompletionRequirement requirement in this.CompletionRequirements) {
					requirement.CriteriaMet += OnCompletionRequirementMet;
				}

				//Activate initial quests (after subscribing to completion requirement met events,
				//and loading any saved progress of the sub-quests)
				foreach (var initialQuest in this.InitialQuests) {
					initialQuest.ActivateIfPossible();
				}
			}
		}



		private void OnCompletionRequirementMet(object sender, EventArgs e)
		{
			CompleteIfAllRequirementsMet();
		}

		private bool CompleteIfAllRequirementsMet()
		{
			if (CompletionRequirements.All(req => req.CriteriaAreMet))
			{
				foreach (IEpisodeCompletionRequirement requirement in this.CompletionRequirements) {
					requirement.CriteriaMet -= OnCompletionRequirementMet;
				}
				Complete();
				return true;
			}
			return false;
		}

		public void ActivateNext(EpisodeSlot next)
		{
			next.Activate(throwIfActive: true);

			//	foreach 
			//	next.ActiveStoryPart.Quests.Ad
		}

		private void OnDiscarded(object sender, EventArgs e)
		{
			if (!_hasRunDiscard)
			{
				_hasRunDiscard = true;

				FireDiscarded();
			}
		}
    }
}