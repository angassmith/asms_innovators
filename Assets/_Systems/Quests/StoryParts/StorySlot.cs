﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using RevolitionGames.Saving.Types;

namespace RevolitionGames.Quests
{
	public sealed class StorySlot
	{
		private Story _story;
		public Story Story {
			get {
				if (_story == null) throw new InvalidOperationException("The story is not active.");
				else return _story;
			}
		}

		public bool IsActive { get { return _story != null; } }

		public IStoryProvider StoryProvider { get; private set; }
		public StoryProgressSaveData SaveData { get; private set; }

		public StorySlot(IStoryProvider storyProvider, StoryProgressSaveData saveData)
		{
			if (storyProvider == null) throw new ArgumentNullException("storyProvider");
			if (saveData == null) throw new ArgumentNullException("saveData");

			this.StoryProvider = storyProvider;
			this.SaveData = saveData;
		}

		/// <summary>
		/// Creates and starts the story. Should only be called by <see cref="PlayModeStage"/>
		/// </summary>
		public void Activate(PlayModeStage containingStage)
		{
			_story = new Story(
				containingStage: containingStage,
				storyProvider: this.StoryProvider,
				savedData: this.SaveData
			);
		}
	}
}

//*/