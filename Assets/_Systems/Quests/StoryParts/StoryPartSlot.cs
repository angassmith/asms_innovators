using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests.BaseStoryParts
{



	public abstract class StoryPartSlot<TActive, TSlot> : IStoryPartSlot
		where TActive : ActiveStoryPart<TActive, TSlot>
		where TSlot : StoryPartSlot<TActive, TSlot>
	{
		public const string X_CODENAME = "codeName";
		public const string X_DISPLAYNAME = "displayName";
		public const string X_DESCRIPTION = "description";

		public Story Story { get; private set; }

		public string CodeName { get; private set; }
		public string DisplayName { get; private set; }
		public string Description { get; private set; }

		private StoryPartState _state;

		public StoryPartStatus Status { get { return _state.Status; } }

		private readonly Func<TSlot, TActive> CustomActivator;

		/// <summary>TODO: Docs</summary>
		/// <exception cref="InvalidOperationException">State is not an <see cref="ActiveStoryPart{TActive, TSlot}"/></exception>
		public TActive ActiveStoryPart {
			get {
				if (!_state.IsActive) {
					throw new InvalidOperationException(
						"The story part " + DisplayName + " is not active (its Status property is equal to '" + Status + "' "
						+ "instead of '" + StoryPartStatus.Active + "')."
					);
				}
				return _state.ActiveStoryPart;
			}
		}

		public bool IsActive { get { return Status == StoryPartStatus.Active; } }
		/// <summary>
		/// True if the <see cref="StoryPartSlot{TActive, TSlot}"/> is completed or destroyed.
		/// </summary>
		public bool IsDeactivated {
			get { return Status == StoryPartStatus.Completed || Status == StoryPartStatus.Destroyed; }
		}
		

		/// <summary>
		/// Fired when the <see cref="Status"/> property has changed, after
		/// <see cref="Activated"/>/<see cref="Completed"/>/<see cref="Destroyed"/>/<see cref="Deactivated"/>.
		/// Cleared automatically when the story part is completed or destroyed (as the status can no longer change)
		/// (so often no need to unsubscribe).
		/// </summary>
		public event EventHandler<StoryPartStatusEventArgs> StatusChanged;

		/// <summary>
		/// Fired when the story part is activated, before <see cref="StatusChanged"/>.
		/// Only fires once, so is cleared automatically after firing (often no need to unsubscribe).
		/// </summary>
		public event EventHandler<StoryPartEventArgs<TSlot>> Activated;
		/// <summary>
		/// Fired when the story part is completed, after <see cref="ActiveStoryPart{TActive, TSlot}.Completed"/>,
		/// and before <see cref="StatusChanged"/>.
		/// Only fires once, so is cleared automatically after firing (often no need to unsubscribe).
		/// </summary>
		public event EventHandler<StoryPartEventArgs<TSlot>> Completed;
		/// <summary>
		/// Fired when the story part is destroyed, after <see cref="Destroying"/>, and before <see cref="StatusChanged"/>.
		/// Only fires once, so is cleared automatically after firing (often no need to unsubscribe).
		/// </summary>
		public event EventHandler<StoryPartEventArgs<TSlot>> Destroyed;
		/// <summary>
		/// Fired after <see cref="Completed"/> or <see cref="Destroyed"/> is fired.
		/// Only fires once, so is cleared automatically after firing (often no need to unsubscribe).
		/// </summary>
		public event EventHandler<StoryPartEventArgs<TSlot>> Deactivated;
		//Separate events rather than lambda-forwarded events to make sure unsubscribing works
		//Also less code

		/// <summary>
		/// Fired immediately after the StatusChanged event when the story part is activated.
		/// Triggers the <see cref="ActiveStoryPart{TActive, TSlot}"/> to initialise,
		/// which involves activating initial sub-story-parts, subscribing to events, and
		/// checking for completion. Fires even when loading (unlike Activated, StatusChanged, Completed,
		/// etc).
		/// <para/>
		/// Should usually only be used by <see cref="ActiveStoryPart{TActive, TSlot}"/>.
		/// </summary>
		public event EventHandler<StoryPartEventArgs<TSlot>> AfterActivated;

		/// <summary>
		/// Fired after the <see cref="StoryPartSlot{TActive, TSlot}"/> has changed to the
		/// destroyed state (Status has updated and ActiveStoryPart is no longer available), and
		/// either before or after the <see cref="ActiveStoryPart{TActive, TSlot}"/> has cleaned
		/// up any state (depending on the subscription order), but before the <see cref="Destroyed"/>
		/// event is fired.
		/// <para/>
		/// Should usually only be used by <see cref="ActiveStoryPart{TActive, TSlot}"/>.
		/// </summary>
		public event EventHandler<StoryPartEventArgs<TSlot>> Destroying;

		public abstract bool IsDiscarded { get; }
		public event EventHandler<DiscardEventArgs> Discarded;
		protected void FireDiscarded() {
			Discarded.Fire(this, new DiscardEventArgs(this));
			Discarded = null;
		}


		public StoryPartSlot(Story story, string codeName, string displayName, string description, Func<TSlot, TActive> customActivator = null)
		{
			ThrowIfArgNull(story, "story");
			if (string.IsNullOrEmpty(codeName)) throw new ArgumentException("Cannot be null or empty", "codeName");
			if (string.IsNullOrEmpty(displayName)) throw new ArgumentException("Cannot be null or empty", "displayName");
			if (string.IsNullOrEmpty(description)) throw new ArgumentException("Cannot be null or empty", "description");

			if (story.IsDiscarded) throw new ArgumentDiscardedException("story");

			this.Story = story;
			this.CodeName = codeName;
			this.DisplayName = displayName;
			this.Description = description;
			this.CustomActivator = customActivator;

			this._state = StoryPartState.Unused;

			SetupInterfaceImplementations();
		}

		//I tried setting the loaded progress in the constructor, however that meant the story part
		//was activated before the derived constructor had a chance to run.
		//I then tried setting the loaded progress in the derived constructor, using this method. However,
		//that meant that while eg. a quest was activating (while loading), it would construct a new task,
		//which would rely on the parent quest having been activated - and so fail.
		//Because of this, this method, with the 'loaded' bool parameters and SetLoadedStatus, is the best
		//I could think of.
		protected void SetLoadedStatus(StoryPartStatus status)
		{
			if (Status != StoryPartStatus.Unused) throw CreateLoadProgressWhenNotUnusedEx();

			switch (status)
			{
				case StoryPartStatus.Unused: break;
				case StoryPartStatus.Active: Activate(throwIfActive: true, loading: true); break;
				case StoryPartStatus.Completed: Complete(loading: true); break;
				case StoryPartStatus.Destroyed: Destroy(loading: true); break;
				default: throw new ArgumentException("Invalid StoryPartStatus '" + status + "'.", "status");
			}
		}

		protected static InvalidOperationException CreateLoadProgressWhenNotUnusedEx() {
			return new InvalidOperationException("Only Unused story parts can have their progress loaded and applied.");
		}


		/// <summary>
		/// Returns a new <see cref="ActiveStoryPart{TActive, TSlot}"/> corresponding
		/// to the current <see cref="StoryPartSlot{TActive, TSlot}"/>. Must not return
		/// null. May throw an <see cref="InvalidOperationException"/>, eg. if it is an
		/// episode that needs to be purchased/downloaded, or the xml is invalid, etc.
		/// <para/>
		/// TODO: Make CustomActivator mandatory and remove this??
		/// </summary>
		protected abstract TActive FallbackActivateImpl();

		/// <summary>
		/// Activates the story part.
		/// Throws an <see cref="InvalidOperationException"/> if the story part is
		/// completed or destroyed, is already active and <paramref name="throwIfActive"/>
		/// is true, or otherwise cannot be activated (eg. if it is an episode 
		/// that has not been purchased/downloaded).
		/// </summary>
		/// <param name="throwIfActive">If true, then if the story part is already active, an <see cref="InvalidOperationException"/> will be thrown.</param>
		/// <returns>The active story part</returns>
		/// <exception cref="InvalidOperationException">The story part is completed or destroyed, is already active and <paramref name="throwIfActive"/> is true, or otherwise cannot be activated (<see cref="FallbackActivateImpl()"/> throws an exception).</exception>
		public TActive Activate(bool throwIfActive)
		{
			return Activate(throwIfActive: throwIfActive, loading: false);
		}

		private TActive Activate(bool throwIfActive, bool loading)
		{
			switch (Status)
			{
				case StoryPartStatus.Unused:
					var oldStatus = Status;

					TActive active;
					try {
						active = CustomActivator != null ? CustomActivator((TSlot)this) : FallbackActivateImpl();
					} catch (Exception ex) {
						throw new InvalidOperationException("Could not activate the story part, as CustomActivator or FallbackActivateImpl threw an exception.", ex);
					}

					_state = StoryPartState.Active(active);

					active.Completed += OnActivePartCompleted;

					if (!loading) {
						Activated.Fire(this, new StoryPartEventArgs<TSlot>((TSlot)this));
						StatusChanged.Fire(this, new StoryPartStatusEventArgs(oldStatus, Status, this));
					}
					//AfterActivated is used by ActiveStoryPart.Initialise() so must be fired even when loading
					AfterActivated.Fire(this, new StoryPartEventArgs<TSlot>((TSlot)this));

					//Unsubscribe all as events will never fire again
					Activated = null;
					AfterActivated = null;
					_IStoryPartSlot_Activated = null;

					return active;

				case StoryPartStatus.Active:
					if (throwIfActive) throw new InvalidOperationException("The story part has already been activated, and throwIfActive was true.");
					else return ActiveStoryPart;

				case StoryPartStatus.Completed:
					throw new InvalidOperationException("Cannot activate a completed story part.");

				case StoryPartStatus.Destroyed:
					throw new InvalidOperationException("Cannot activate a destroyed story part.");

				default: //Required by the compiler
					throw new InvalidOperationException("Invalid StoryPartStatus '" + Status + "'.");
			}
		}

		public void ActivateIfPossible()
		{
			if (Status == StoryPartStatus.Unused)
			{
				try {
					Activate(throwIfActive: false);
				} catch (InvalidOperationException e) {
					Debug.LogException(e);
				}
			}
		}

		private void OnActivePartCompleted(object sender, EventArgs e)
		{
			Complete(loading: false);
		}

		private void Complete(bool loading)
		{
			var oldStatus = Status;
			_state = StoryPartState.Completed;

			if (Status != oldStatus)
			{
				if (!loading) {
					Completed.Fire(this, new StoryPartEventArgs<TSlot>((TSlot)this));
					Deactivated.Fire(this, new StoryPartEventArgs<TSlot>((TSlot)this));
					StatusChanged.Fire(this, new StoryPartStatusEventArgs(oldStatus, Status, this));
				}

				//Unsubscribe all as events will never fire again
				Completed = null;
				Deactivated = null;
				StatusChanged = null; //Status can no longer change
				_IStoryPartSlot_Completed = null;
				_IStoryPartSlot_Deactivated = null;
			}
		}

		public void Destroy()
		{
			Destroy(loading: false);
		}

		private void Destroy(bool loading)
		{
			if (Status == StoryPartStatus.Destroyed || Status == StoryPartStatus.Completed) {
				return;
			}

			if (Status == StoryPartStatus.Active) {
				ActiveStoryPart.Completed -= OnActivePartCompleted;
			}

			var oldStatus = Status;
			_state = StoryPartState.Destroyed;

			if (!loading)
			{
				Destroying.Fire(this, new StoryPartEventArgs<TSlot>((TSlot)this));
			
				Destroyed.Fire(this, new StoryPartEventArgs<TSlot>((TSlot)this));
				Deactivated.Fire(this, new StoryPartEventArgs<TSlot>((TSlot)this));
				StatusChanged.Fire(this, new StoryPartStatusEventArgs(oldStatus, Status, this));
			}

			//Unsubscribe all as events will never fire again
			Destroying = null;
			Destroyed = null;
			Deactivated = null;
			StatusChanged = null; //Status can no longer change
			_IStoryPartSlot_Deactivated = null;
			_IStoryPartSlot_Destroyed = null;
		}


		#region Interface implementations

		private void SetupInterfaceImplementations()
		{
			//Subscribe to (generic) events for (non-generic) IStoryPartSlot implementation
			//Lambdas are needed because co/contra-variance isn't available in unity
			this.Activated += (s, e) => _IStoryPartSlot_Activated.Fire(s, e);
			this.Completed += (s, e) => _IStoryPartSlot_Completed.Fire(s, e);
			this.Destroyed += (s, e) => _IStoryPartSlot_Destroyed.Fire(s, e);
			this.Deactivated += (s, e) => _IStoryPartSlot_Deactivated.Fire(s, e);
		}

		//Covariance isn't available here, so the explicit implementations can't be forwarded.
		//Separate events are used instead (which will definitely allow unsubscribing to work,
		//as opposed to forwarding using lambdas or new delegates)
		private event EventHandler<StoryPartEventArgs> _IStoryPartSlot_Activated;
		private event EventHandler<StoryPartEventArgs> _IStoryPartSlot_Completed;
		private event EventHandler<StoryPartEventArgs> _IStoryPartSlot_Destroyed;
		private event EventHandler<StoryPartEventArgs> _IStoryPartSlot_Deactivated;

		event EventHandler<StoryPartEventArgs> IStoryPartSlot.Activated   { add { _IStoryPartSlot_Activated   += value; } remove { _IStoryPartSlot_Activated   -= value; } }
		event EventHandler<StoryPartEventArgs> IStoryPartSlot.Completed   { add { _IStoryPartSlot_Completed   += value; } remove { _IStoryPartSlot_Completed   -= value; } }
		event EventHandler<StoryPartEventArgs> IStoryPartSlot.Destroyed   { add { _IStoryPartSlot_Destroyed   += value; } remove { _IStoryPartSlot_Destroyed   -= value; } }
		event EventHandler<StoryPartEventArgs> IStoryPartSlot.Deactivated { add { _IStoryPartSlot_Deactivated += value; } remove { _IStoryPartSlot_Deactivated -= value; } }

		IActiveStoryPart IStoryPartSlot.ActiveStoryPart { get { return ActiveStoryPart; } }

		#endregion


		protected static string GetCodeNameFromXml(XElement xElement) {
			return (string)xElement.Attribute(X_CODENAME);
		}

		protected static string GetDisplayNameFromXml(XElement xElement) {
			return (string)xElement.Element(XmlStoryProvider.QuestNamespace + X_DISPLAYNAME);
		}

		protected static string GetDescriptionFromXml(XElement xElement) {
			return (string)xElement.Element(XmlStoryProvider.QuestNamespace + X_DESCRIPTION);
		}


		/// <summary>
		/// Holds both the <see cref="StoryPartStatus"/>,
		/// and the active story part if the there is one.
		/// Ensures that the active story part is non-null if and only if
		/// the <see cref="StoryPartStatus"/> is equal to <see cref="StoryPartStatus.Active"/>.
		/// </summary>
		private struct StoryPartState
		{
			public StoryPartStatus Status { get; private set; }
			public TActive ActiveStoryPart { get; private set; }
			public bool IsActive { get { return ActiveStoryPart != null; } }

			private StoryPartState(StoryPartStatus status, TActive activeStoryPart)
			{
				this.Status = status;
				this.ActiveStoryPart = activeStoryPart;
			}

			public static StoryPartState Active(TActive activeStoryPart) {
				if (activeStoryPart == null) throw new ArgumentNullException("activeStoryPart");
				return new StoryPartState(StoryPartStatus.Active, activeStoryPart);
			}
			public static StoryPartState Unused    { get { return new StoryPartState(StoryPartStatus.Unused   , null); } }
			public static StoryPartState Completed { get { return new StoryPartState(StoryPartStatus.Completed, null); } }
			public static StoryPartState Destroyed { get { return new StoryPartState(StoryPartStatus.Destroyed, null); } }
		}


		#region Utilities

		protected static T ThrowIfArgNull<T>(T arg, string name)
		{
			if (arg == null) throw new ArgumentNullException(name);
			else return arg;
		}

		#endregion
	}

}

