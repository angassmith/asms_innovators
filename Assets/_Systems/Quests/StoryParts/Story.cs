﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.Quests.Flags;
using System.Runtime.Serialization;
using RevolitionGames.Saving;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.Saving;
using System.Linq;
using System.IO;
using System.Xml.Schema;
using UnityEngine.Analytics;
using RevolitionGames.Saving.Types;

namespace RevolitionGames.Quests
{
	public sealed class Story : IDiscardable
	{
		public PlayModeStage ContainingStage { get; private set; }

		public IStoryProvider StoryProvider { get; private set; }

		private List<EpisodeSlot> _episodes;
		public ReadOnlyCollection<EpisodeSlot> Episodes { get; private set; }

		public int CurrentEpisodeIndex { get; private set; }
		public EpisodeSlot CurrentEpisodeSlot { get { return Episodes[CurrentEpisodeIndex]; } }
		public ActiveEpisode CurrentEpisode { get { return CurrentEpisodeSlot.ActiveStoryPart; } }

		private QuestSlot _focusQuest;
		/// <summary>
		/// Note: This may be null
		/// </summary>
		/// <exception cref="InvalidOperationException">When setting, the new value is non-null and not active</exception>
		public QuestSlot FocusQuest {
			get { return _focusQuest; }
			set {
				if (value != null && !value.IsActive) throw new InvalidOperationException("The focus quest must be active");

				if (_focusQuest != value)
				{
					if (_focusQuest != null) _focusQuest.Deactivated -= OnFocusQuestDeactivated;
					_focusQuest = value;
					if (_focusQuest != null) _focusQuest.Deactivated += OnFocusQuestDeactivated;
					FocusQuestChanged.Fire(this, new StoryPartEventArgs<QuestSlot>(_focusQuest));
				}
			}
		}
		public event EventHandler<StoryPartEventArgs<QuestSlot>> FocusQuestChanged;

		public StoryFlags Flags { get; private set; }

		public BehaviourOverrideProvider BehaviourOverrideProvider { get; private set; }

		public StoryProgress Progress { get; private set; }

		public bool IsDiscarded { get { return this.ContainingStage.IsDiscarded; } }
		public event EventHandler<DiscardEventArgs> Discarded;

		public Story(PlayModeStage containingStage, IStoryProvider storyProvider, StoryProgressSaveData savedData)
		{
			if (containingStage == null) throw new ArgumentNullException("containingStage");
			if (storyProvider == null) throw new ArgumentNullException("storyProvider");
			if (savedData == null) throw new ArgumentNullException("savedData");

			if (containingStage.IsDiscarded) throw new ArgumentException("containingStage has been discarded.", "containingStage");

			this.ContainingStage = containingStage;
			this.StoryProvider = storyProvider;

			this.ContainingStage.Discarded += delegate
			{
				Discarded.Fire(this, new DiscardEventArgs(this));
				Discarded = null;
			};

			this.Progress = new StoryProgress(story: this, savedData: savedData);

			this._episodes = storyProvider.GetEpisodeSlots(this);
			this.Episodes = this._episodes.AsReadOnly();

			//Create and add a final, impossible episode
			this._episodes.Add(
				Quests.StoryProvider.MakeFinalEpisode(story: this, returnToMenu: true)
			);

			this.Flags = new StoryFlags();

			this.BehaviourOverrideProvider = new BehaviourOverrideProvider(this);


			SetupAggregateEvents();

			//Set current episode to the first episode that's loaded and automatically activated.
			//If no episodes are active, because there is no saved progress,
			//the saved codename is invalid, or it needs to be purchased etc, activate the first
			//episode instead.
			//If the player has progressed beyond the first episode, then so long as
			//the completion requirements haven't changed or anything, this will trigger switching to
			//the next episode, and so on (but may result in undesirable UI stuff, i.e. "you have completed ___").
			var currentEpisode = Episodes.FirstOrDefault(e => e.IsActive);
			if (currentEpisode == null) {
				currentEpisode = Episodes.First();
				currentEpisode.Activate(throwIfActive: true);
			}
			this.CurrentEpisodeIndex = Episodes.IndexOf(currentEpisode);

			//	//Load and apply saved progress (also works if there is none)
			//	try
			//	{
			//		var currentEpisode = Episodes.Single(e => e.CodeName == Progress.CurrentEpisodeCodeName);
			//		currentEpisode.Activate(throwIfActive: false); //This also triggers the sub-quests and tasks to load their progress
			//		this.CurrentEpisodeIndex = Episodes.IndexOf(currentEpisode);
			//	}
			//	catch (InvalidOperationException)
			//	{
			//		//If the episode could not be activated, because there is no saved progress,
			//		//the saved codename is invalid, or it needs to be purchased etc, activate the first
			//		//episode instead.
			//		//If the player has progressed beyond the first episode, then so long as
			//		//the completion requirements haven't changed or anything, this will trigger switching to
			//		//the next episode, and so on.
			//		Episodes[0].Activate(throwIfActive: false);
			//	}

			this.CurrentEpisodeSlot.Completed += (s, e) => MoveToNextEpisode(); //Unsubscribed automatically

			var focusQuest = CurrentEpisode.ActiveQuests.FirstOrDefault();
			this.FocusQuest = focusQuest == null ? null : focusQuest.Slot; //Setting this also subscribes to deactivated events

			//Set up analytics events
			//More story analytics is in:
			// - ActiveQuest
			this.QuestCompleted += (s, e) => {
				Analytics.CustomEvent(
					customEventName: "QuestCompleted",
					eventData: new Dictionary<string, object>() {
						{ "UTCTime", DateTime.UtcNow }, //Temp??
						{ "Quest", e.StoryPart.CodeName },
					}
				);
			};
        }

		//TODO: IMPLEMENT THIS:
		//Notes on how quests are saved
		//Progress through quests is not stored in any particular episode.
		//Instead, progress through all quests that are part of the current or a previous
		//episode are stored in one big group.
		//When switching episodes, the next episode specifies what quests are included in it.
		//The story provider will then find that quest, and pass it to the loader.
		//That will then look for any existing progress through the specified quest. If it finds it,
		//it will apply it (not sure exactly how at the moment).
		//This means that even if a quest is left out of some episodes then come back to, the
		//progress will still be there. It also makes episode carry overs really easy. Finally,
		//it will hopefully allow changes to the story without breaking any saved progress.
		//TODO: Do something else that solves similar problems within each quest.

		#region Aggregate Contained Story Part Events
		
		/// <summary>Fired when the relevant event is fired by the current episode. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<EpisodeSlot>> EpisodeActivated;
		/// <summary>Fired when the relevant event is fired by the current episode. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<EpisodeSlot>> EpisodeCompleted;
		/// <summary>Fired when the relevant event is fired by the current episode. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<EpisodeSlot>> EpisodeDestroyed;
		/// <summary>Fired when the relevant event is fired by the current episode. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<EpisodeSlot>> EpisodeDeactivated;
		/// <summary>Fired when the relevant event is fired by a contained episode. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartStatusEventArgs> EpisodeStatusChanged;

		/// <summary>Fired when the relevant event is fired by a contained quest. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestActivated;
		/// <summary>Fired when the relevant event is fired by a contained quest. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestCompleted;
		/// <summary>Fired when the relevant event is fired by a contained quest. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestDestroyed;
		/// <summary>Fired when the relevant event is fired by a contained quest. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<QuestSlot>> QuestDeactivated;
		/// <summary>Fired when the relevant event is fired by a contained quest. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartStatusEventArgs> QuestStatusChanged;

		/// <summary>Fired when the relevant event is fired by a contained task. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskActivated;
		/// <summary>Fired when the relevant event is fired by a contained task. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskCompleted;
		/// <summary>Fired when the relevant event is fired by a contained task. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskDestroyed;
		/// <summary>Fired when the relevant event is fired by a contained task. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskDeactivated;
		/// <summary>Fired when the relevant event is fired by a contained task. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartStatusEventArgs> TaskStatusChanged;

		/// <summary>Fired when <see cref="EpisodeStatusChanged"/>, <see cref="QuestStatusChanged"/>, or <see cref="TaskStatusChanged"/> is fired. (Not cleared automatically)</summary>
		public event EventHandler<StoryPartStatusEventArgs> ProgressChanged;

		private void SetupAggregateEvents()
		{
			foreach (var episode in Episodes)
			{
				episode.Activated += OnEpisodeActivated;
				episode.StatusChanged += OnEpisodeStatusChanged;

				//Some quests and tasks are activated immediately so we can't do these subscriptions in
				//OnEpisodeActivated (as that runs after the immediately-activated quests and tasks are activated,
				//and potentially completed or destroyed)
				episode.QuestActivated += OnQuestActivated;
				episode.QuestCompleted += OnQuestCompleted;
				episode.QuestDestroyed += OnQuestDestroyed;
				episode.QuestStatusChanged += OnQuestStatusChanged;
				episode.TaskActivated += OnTaskActivated;
				episode.TaskCompleted += OnTaskCompleted;
				episode.TaskDestroyed += OnTaskDestroyed;
				episode.TaskStatusChanged += OnTaskStatusChanged;
			}
		}
		private void OnEpisodeStatusChanged(object sender, StoryPartStatusEventArgs e)
		{
			EpisodeStatusChanged.Fire(this, e);
			ProgressChanged.Fire(this, e);
		}
		private void OnEpisodeActivated(object sender, StoryPartEventArgs<EpisodeSlot> e) {
			EpisodeActivated.Fire(this, e);
			e.StoryPart.Completed += OnEpisodeCompleted;
			e.StoryPart.Destroyed += OnEpisodeDestroyed;
		}
		private void OnEpisodeCompleted(object sender, StoryPartEventArgs<EpisodeSlot> e) {
			EpisodeCompleted.Fire(this, e);
			EpisodeDeactivated.Fire(this, e);
		}
		private void OnEpisodeDestroyed(object sender, StoryPartEventArgs<EpisodeSlot> e) {
			EpisodeCompleted.Fire(this, e);
			EpisodeDeactivated.Fire(this, e);
		}
		private void OnQuestStatusChanged(object sender, StoryPartStatusEventArgs e) {
			QuestStatusChanged.Fire(this, e);
			ProgressChanged.Fire(this, e);
		}
		private void OnQuestActivated(object sender, StoryPartEventArgs<QuestSlot> e) {
			QuestActivated.Fire(this, e);
		}
		private void OnQuestCompleted(object sender, StoryPartEventArgs<QuestSlot> e) {
			QuestCompleted.Fire(this, e);
			QuestDeactivated.Fire(this, e);
		}
		private void OnQuestDestroyed(object sender, StoryPartEventArgs<QuestSlot> e) {
			QuestDestroyed.Fire(this, e);
			QuestDeactivated.Fire(this, e);
		}
		private void OnTaskStatusChanged(object sender, StoryPartStatusEventArgs e) {
			TaskStatusChanged.Fire(this, e);
			ProgressChanged.Fire(this, e);
		}
		private void OnTaskActivated(object sender, StoryPartEventArgs<TaskSlot> e) {
			TaskActivated.Fire(this, e);
		}
		private void OnTaskCompleted(object sender, StoryPartEventArgs<TaskSlot> e) {
			TaskCompleted.Fire(this, e);
			TaskDeactivated.Fire(this, e);
			//TODO: Fix the fact that this (and other ___Deactived events) runs before
			//TaskSlot.Deactivated (which runs after TaskSlot.Activated, not as part of it, like this method)
		}
		private void OnTaskDestroyed(object sender, StoryPartEventArgs<TaskSlot> e) {
			TaskDestroyed.Fire(this, e);
			TaskDeactivated.Fire(this, e);
		}

		#endregion

		private void OnFocusQuestDeactivated(object sender, StoryPartEventArgs<QuestSlot> e)
		{
			FocusQuest = this.CurrentEpisode.ActiveQuests.FirstOrDefault().Slot;
		}

		private void MoveToNextEpisode()
		{
			if (CurrentEpisodeIndex + 1 >= Episodes.Count) {
				Debug.LogError("The last episode should be impossible to complete");
				return;
			}

			var prevEpisode = CurrentEpisodeSlot;
			var nextEpisode = Episodes[CurrentEpisodeIndex + 1];

			nextEpisode.Activate(throwIfActive: false); //Loads both structure and progress
			this.CurrentEpisodeIndex++;

			var focusQuest = nextEpisode.ActiveStoryPart.ActiveQuests.FirstOrDefault(); //Order doesn't matter, and null is ok
			this.FocusQuest = focusQuest != null ? null : focusQuest.Slot;

			//TODO: Save at this point
			//Might have to do things like stop all flowcharts first (though there shouldn't be any running)

			nextEpisode.Completed += (s, e) => MoveToNextEpisode();
		}



		//	public void GetObjectData(SerializationInfo info, StreamingContext context)
		//	{
		//		if (info == null) throw new ArgumentNullException("info");
		//	
		//		info.AddValue("CurrentActiveEpisode", CurrentEpisode.CreationResult, typeof(ActiveEpisode));
		//		info.AddValue("CurrentEpisodeIndex", CurrentEpisodeIndex);
		//	}
		//	
		//	public void SetObjectData(SerializationInfo info, StreamingContext context)
		//	{
		//		if (info == null) throw new ArgumentNullException("info");
		//	
		//		this.CurrentEpisodeIndex = info.GetInt32("CurrentEpisodeIndex");
		//		var savedEpisode = (ActiveEpisode)info.GetValue("CurrentActiveEpisode", typeof(ActiveEpisode));
		//	
		//		this._episodes[CurrentEpisodeIndex]
		//	}


		//	private void OnGUI()
		//	{
		//		GUILayout.BeginArea(new Rect(20, 20, Screen.width - 40, Screen.height - 40));
		//		GUILayout.BeginVertical();
		//		Quest01.Display();
		//		GUILayout.EndVertical();
		//	
		//		GUILayout.BeginHorizontal();
		//		if (GUILayout.Button("Begin Quest"))
		//		{
		//			if (Quest01.Status != Quest.QuestState.Complete)
		//			{
		//				Quest01.Status = Quest.QuestState.Active;
		//			}
		//	
		//			foreach (Task t in Quest01.Tasks)
		//			{
		//				t.State = Task.TaskState.Active;
		//	
		//			}
		//	
		//		}
		//	
		//		if (GUILayout.Button("Collect the English Book"))
		//		{
		//			//ItemDef.FireCollect(new ItemDef("englishbook1", GameObject.Find("englishbook"), 0));  
		//	
		//		}
		//	
		//		if (GUILayout.Button("Collect the Pencil"))
		//		{
		//	
		//		}
		//		if (GUILayout.Button("Complete Quest"))
		//		{
		//			if (Quest01.Status == Quest.QuestState.Pending)
		//			{
		//				Quest01.Status = Quest.QuestState.Complete;
		//			}
		//		}
		//		GUILayout.EndHorizontal();
		//		GUILayout.EndArea();
		//	}

		
	}
}



