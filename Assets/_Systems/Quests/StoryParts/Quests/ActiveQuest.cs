﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MiscCollections;
using RevolitionGames.Choices;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.BehaviourOverriding;
using RevolitionGames.Utilities.Alex;
using UnityEngine;
using UnityEngine.Analytics;

namespace RevolitionGames.Quests
{
	public sealed class ActiveQuest : ActiveStoryPart<ActiveQuest, QuestSlot>
	{
		public ReadOnlyDictionary<string, TaskSlot> Tasks { get; private set; }

		public IEnumerable<TaskSlot> InitialTasks { get; private set; }

		public IEnumerable<ActiveTask> ActiveTasks { get { return Tasks.Values.Where(x => x.IsActive).Select(x => x.ActiveStoryPart); } }

        // List of choices made by the player in that current quest
        private List<ChoiceData> _choicesmade;

        public ReadOnlyCollection<ChoiceData> ChoicesMade { get { return new ReadOnlyCollection<ChoiceData>(_choicesmade); } }

		public bool IsFocus { get { return ReferenceEquals(this.Slot, this.Slot.Story.FocusQuest); } }

		public sealed override bool IsDiscarded { get { return this.Slot.IsDiscarded || this.Slot.IsDeactivated; } }
		private bool _hasRunDiscard;

		public ActiveQuest(QuestSlot slot, IEnumerable<TaskSlot> tasks, IEnumerable<TaskSlot> initialTasks, ILookup<string, BehaviourOverride> behaviourOverrides)
			: base(slot, behaviourOverrides)
		{
			if (tasks == null) throw new ArgumentNullException("tasks");
			if (initialTasks == null) throw new ArgumentNullException("initialTasks");
			tasks.ValidateItemsNonNull(message => new ArgumentException(message, "tasks"));
			initialTasks.ValidateItemsNonNull(message => new ArgumentException(message, "initialTasks"));
			initialTasks.ValidateItems(
				validationName: "contained in tasks",
				isValidPredicate: x => tasks.Contains(x),
				exceptionFactory: message => new ArgumentException(message, "initialTasks")
			);


			this.Tasks = new ReadOnlyDictionary<string, TaskSlot>(tasks.ToDictionary(x => x.CodeName, x => x));
			this.InitialTasks = initialTasks;
            _choicesmade = new List<ChoiceData>();

			this.Slot.Discarded += OnDiscarded;
			this.Slot.Deactivated += OnDiscarded;
		}

		public ActiveQuest(QuestSlot slot, IEnumerable<TaskSlot> tasks, IEnumerable<TaskSlot> initialTasks, IEnumerable<BehaviourOverride> behaviourOverrides)
			: this(slot, tasks, initialTasks, MakeBehaviourOverridesLookup(behaviourOverrides))
		{ }

		protected override void Initialise()
		{
			//Load and apply progress of sub-tasks (now that everything's had a chance to subscribe to
			//events on them etc, but before activating the initial tasks)
			foreach (var task in this.Tasks.Values) {
				task.LoadAndApplyProgress();
			}

			if (this.Tasks.Values.OfType<ActiveQuestEndTask>().Any(t => t.Slot.Status == StoryPartStatus.Completed))
			{
				//If already complete
				DestroyAllTasks();
				Complete();
			}
			else
			{
				foreach (var task in this.Tasks.Values) {
					task.Activated += OnTaskActivated;
				}

				//Activate initial tasks (after subscribing to Activated events,
				//and loading any saved progress of the sub-tasks)
				foreach (var initialTask in this.InitialTasks) {
					initialTask.ActivateIfPossible();
				}
			}
		}

		private void OnTaskActivated(object sender, StoryPartEventArgs<TaskSlot> e)
		{
			if (e.StoryPart.ActiveStoryPart is ActiveQuestEndTask)
			{
				e.StoryPart.Completed += OnQuestEndTaskCompleted;
			}
		}

		private void OnQuestEndTaskCompleted(object sender, StoryPartEventArgs<TaskSlot> e)
		{
			DestroyAllTasks();
			this.Complete();
		}

		public TaskSlot GetTask(string codeName)
		{
			TaskSlot found;
			if (Tasks.TryGetValue(codeName, out found)) {
				return found;
			} else {
				throw new ArgumentException("No task with code-name '" + codeName + "' exists inside the quest");
			}
		}

        public void AddChoice(ChoiceData choice)
        {
            //Debug.Log("Choice made " + choice.Description.ToString());
            _choicesmade.Add(choice);

			//This should probably use a ChoiceMade event, but that would also require events in Episode
			//and Story, so I can't be bothered at the moment (and time is limited)
			Analytics.CustomEvent(
				customEventName: "ChoiceMade",
				eventData: new Dictionary<string, object>() {
					{ "UTCTime", DateTime.UtcNow }, //Temp??
					{ "ContainingQuest", this.Slot.CodeName },
					{ "ChoiceDescription", choice.Description.Substring(0, Math.Max(choice.Description.Length, 10)) },
				}
			);
        }

		protected override void OnDestroying()
		{
			base.OnDestroying();
			DestroyAllTasks();
		}

		private void DestroyAllTasks()
		{
			foreach (var task in this.Tasks.Values) {
				task.Destroy();
			}
		}

		private void OnDiscarded(object sender, EventArgs e)
		{
			if (!_hasRunDiscard)
			{
				_hasRunDiscard = true;

				FireDiscarded();
			}
		}

		#region old
		//	MAY ALSO NEED THIS FOR LATER AND PARTS ARE INCOMPLETE
		//	
		//	public void  (ITaskCreator task)
		//	{
		//		if (task == null) throw new ArgumentNullException("task");
		//		Task.OnComplete += Task_OnComplete;
		//		_taskcreators.Add(task);
		//	}
		//	
		//	
		//	// OnGUI method that displays the quest
		//	
		//	public void Display()
		//	{
		//		GUILayout.Label(string.Format("Status: \t\t{0}", Status.ToString()));
		//		GUILayout.Label(string.Format("Name: \t\t{0}", Name));
		//		GUILayout.Label(string.Format("Description: \t{0}", Description));
		//		Debug.Log(TaskCreators.Count);
		//	
		//		foreach (ITaskCreator task in TaskCreatorsForQuest)
		//		{
		//			GUILayout.Space(40);
		//			task.Display();
		//		}
		//	}
		//	
		//	
		//	
		//	public void OnComplete()
		//	{
		//		// can do this because the event is static, if the event was
		//		// not static, you would have to create a new instance of the Task class,
		//		// and it would only listen for that specific task
		//		// this is adding a listener for the event
		//		//Task.OnComplete += Task_OnComplete;
		//	
		//	}
		//	
		//	private void Task_OnComplete(Task task)
		//	{
		//		bool AllComplete = true;
		//	   
		//		foreach(Task t in ) // if one task is complete, changes it to pending, if another task is also complete, it is already partial. 
		//		{
		//			if(t.State != Task.TaskState.Complete)
		//			{
		//				AllComplete = false;
		//			}
		//			else
		//			{
		//				Status = QuestState.Partial;
		//			}
		//		}
		//	
		//		if (AllComplete == true)
		//		{
		//			Status = QuestState.Pending; // the type is stored in a variabe.
		//		}
		//	}
		#endregion
	}
}


