﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using MiscCollections;
using RevolitionGames.Quests.BaseStoryParts;
using RevolitionGames.Quests.BehaviourOverriding;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public sealed class QuestSlot : StoryPartSlot<ActiveQuest, QuestSlot>
	{
		/// <summary>
		/// The parent episode of this <see cref="QuestSlot"/> instance (which is one
		/// of multiple that, at different times, represent the same quest).
		/// </summary>
		public EpisodeSlot EpisodeSlot { get; private set; }

		private HashSet<QuestSlot> _nextQuests;
		public IEnumerable<QuestSlot> NextQuests { get { return _nextQuests; } }

		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the quest is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskActivated;
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the quest is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskCompleted;
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the quest is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskDestroyed;
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the quest is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartEventArgs<TaskSlot>> TaskDeactivated;
		/// <summary>Fired when the relevant event is fired by a contained task. Cleared automatically when the quest is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartStatusEventArgs> TaskStatusChanged;

		/// <summary>Fired when <see cref="StoryPartSlot{TActive, TSlot}.StatusChanged"/> or <see cref="TaskStatusChanged"/> is fired. Cleared automatically when the quest is deactivated (often no need to unsubscribe).</summary>
		public event EventHandler<StoryPartStatusEventArgs> ProgressChanged;

		public sealed override bool IsDiscarded {
			get { return this.EpisodeSlot.IsDiscarded || this.EpisodeSlot.IsDeactivated; }
		}
		private bool _hasRunDiscard;

		public static QuestSlot CreateFromXml(EpisodeSlot parentEpisode, XElement xElement) {
            if (parentEpisode == null) throw new ArgumentNullException("parentEpisode");
            if (xElement == null) throw new ArgumentNullException("xElement");

            return new QuestSlot(parentEpisode,
                GetCodeNameFromXml(xElement),
                GetDisplayNameFromXml(xElement),
                GetDescriptionFromXml(xElement)
            );
        }

        public QuestSlot(EpisodeSlot currentParentEpisode, string codeName, string displayName, string description, IEnumerable<QuestSlot> nextQuests = null, Func<QuestSlot, ActiveQuest> customActivator = null)
			: base(
				story: ThrowIfArgNull(currentParentEpisode, "currentParentEpisode").Story,
				codeName: codeName,
				displayName: displayName,
				description: description,
				customActivator: customActivator
			)
		{
			ThrowIfArgNull(currentParentEpisode, "currentParentEpisode");
			if (nextQuests != null) {
				nextQuests.ValidateItemsNonNull(message => new ArgumentException(message, "nextQuests"));
			}

			if (currentParentEpisode.IsDiscarded) throw new ArgumentDiscardedException("currentParentEpisode");

			this.EpisodeSlot = currentParentEpisode;
			this._nextQuests = new HashSet<QuestSlot>(nextQuests ?? Enumerable.Empty<QuestSlot>());

			this.Completed += OnCompleted;

			this.StatusChanged += (s, e) => { ProgressChanged.Fire(this, e); };

			this.Deactivated += (s, e) => {
				this.TaskActivated = null;
				this.TaskCompleted = null;
				this.TaskDestroyed = null;
				this.TaskDeactivated = null;
				this.TaskStatusChanged = null;
			};

			this.EpisodeSlot.Deactivated += OnDiscarded;
			this.EpisodeSlot.Discarded += OnDiscarded;
		}

		public void LoadAndApplyProgress()
		{
			if (Status != StoryPartStatus.Unused) throw CreateLoadProgressWhenNotUnusedEx();

			var progress = this.Story.Progress.GetQuestProgress(this.CodeName);

			SetLoadedStatus(progress.Status);

			//Sub-tasks' progress is loaded and applied by ActiveQuest.Initialise()
		}

		///<summary>Should only be used by IStoryProvider implementations</summary>
		public void AddNextQuest(QuestSlot next)
		{
			if (next == null) throw new ArgumentNullException("next");
			_nextQuests.Add(next);
		}

		protected override ActiveQuest FallbackActivateImpl()
		{
			var active = Story.StoryProvider.LoadQuest(this);
			foreach (var task in active.Tasks.Values)
			{
				task.Activated += OnTaskActivated;
				task.StatusChanged += OnTaskStatusChanged;

				//Unsubscribe from events if this quest is deactivated before the tasks are activated, completed etc.
				this.Deactivated += (s, e) => {
					task.Activated -= OnTaskActivated;
					task.Completed -= OnTaskCompleted;
					task.Destroyed -= OnTaskDestroyed;
					task.StatusChanged -= OnTaskStatusChanged;
				};
			}
			return active;
		}

		private void OnTaskStatusChanged(object sender, StoryPartStatusEventArgs e) {
			TaskStatusChanged.Fire(this, e);
			ProgressChanged.Fire(this, e);
		}
		private void OnTaskActivated(object sender, StoryPartEventArgs<TaskSlot> e)
		{
			TaskActivated.Fire(sender, e);
			e.StoryPart.Completed += OnTaskCompleted; //Can't just directly add the TaskDestroyed etc events as handlers, as the actual delegate instance is immutable so new subscriptions would no longer work
			e.StoryPart.Destroyed += OnTaskDestroyed;
		}
		private void OnTaskDestroyed(object sender, StoryPartEventArgs<TaskSlot> e) {
			TaskDestroyed.Fire(this, e);
			TaskDeactivated.Fire(this, e);
		}
		private void OnTaskCompleted(object sender, StoryPartEventArgs<TaskSlot> e) {
			TaskCompleted.Fire(this, e);
			TaskDeactivated.Fire(this, e);
		}

		private void OnCompleted(object sender, StoryPartEventArgs<QuestSlot> e)
		{
			//Unlock all quests in NextQuests (if they're not already completed/destroyed)
			foreach (QuestSlot quest in this.NextQuests)
			{
				quest.ActivateIfPossible();
			}
		}

		private void OnDiscarded(object sender, EventArgs e)
		{
			if (!_hasRunDiscard)
			{
				_hasRunDiscard = true;

				//(Add other code here if needed)

				FireDiscarded();
			}
		}

	}
}


//*/