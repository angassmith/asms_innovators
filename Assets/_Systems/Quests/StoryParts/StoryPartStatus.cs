﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests
{
	public enum StoryPartStatus
	{
		Unused = 0, //Do not change this
		Active,
		Completed,
		Destroyed,
	}
}

//*/