﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.Choices {

    public class ChoiceDisplay : MonoBehaviour {

        [SerializeField]
        private Text ChoiceDescription;

        public void SetChoiceData(ChoiceData choiceData) {
            ChoiceDescription.text = choiceData.Description;
        }
        
    }
}
