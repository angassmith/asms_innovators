﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RevolitionGames.Choices
{

    public class ChoiceData
    {
        public string Description { get; private set; }

        public ChoiceData(string description)
        {
            Description = description;
        }

        public void DeliverResults()
        {


        }

    }
}

