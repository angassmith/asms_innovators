﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace RevolitionGames.Choices {
    public class QuestEndCanvas : MonoBehaviour {

        public GameObject ChoicePrefab;

        public Transform ChoicesParent;

        public Text QuestName;

        public event EventHandler<GameObject, EventArgs> DialogClosed;

        // Use this for initialization
        void Start() {

        }

        public void SetQuestName(string name) {
            QuestName.text = name;
        }

        public void AddChoices(IEnumerable<ChoiceData> choices) {
            foreach (ChoiceData choice in choices) {
                AddChoice(choice);
            }
        }

        /// <summary>
        /// Adds a choice to be displayed.
        /// </summary>
        /// <param name="choiceData">The choice to display.</param>
        public void AddChoice(ChoiceData choiceData) {
            // Create a choice data element
            GameObject choiceObject = Instantiate(ChoicePrefab, ChoicesParent) as GameObject;
            ChoiceDisplay choiceDisplay = choiceObject.GetComponent<ChoiceDisplay>();

            if (choiceDisplay != null) {
                choiceDisplay.SetChoiceData(choiceData);
            }
            else {
                Debug.LogError("No ChoiceDisplay found on " + choiceObject.GetDebugName());
            }
        }

        public void CloseQuestEndDialog() {
            DialogClosed.Fire(this.gameObject, EventArgs.Empty);
            Destroy(gameObject);
        }
    }
}
