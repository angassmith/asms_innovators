﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
using RevolitionGames.Quests;

namespace RevolitionGames.Choices
{
    [CommandInfo("Choices", "Record Choice", "Records a choice made by the player")]

    public class RecordChoice : Command
    {

        [SerializeField]
        [TextArea(3,5)]
        private string _choiceDesc;

        public string ChoiceDescription { get { return _choiceDesc; } }

        
        public override void OnEnter()
        {
            // Get the active quest from the execution context
            var context = GetComponent<StoryFlowchartExecutionContext>();
            ActiveQuest activeQuest = context.ControllingTask.QuestSlot.ActiveStoryPart;

            // Add the choice to the quest
            ChoiceData choiceData = new ChoiceData(ChoiceDescription);
            activeQuest.AddChoice(choiceData);

            Continue();
        }
    }
}

