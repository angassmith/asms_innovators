﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Fungus;
using RevolitionGames.Quests;
using UnityEngine;
using RevolitionGames.Quests.BehaviourOverriding;

namespace RevolitionGames.Quests
{
	[RequireComponent(typeof(Flowchart))]
	[DisallowMultipleComponent]
	public class StoryFlowchartExecutionContext : MonoBehaviour
	{

		public TaskSlot ControllingTask { get; private set; }

		void Start()
		{

		}
	
		// Update is called once per frame
		void Update () {
		
		}

		public static StoryFlowchartExecutionContext CreateForFlowchart(Flowchart flowchart, TaskSlot controllingTask = null)
		{
			if (flowchart == null) throw new ArgumentNullException("flowchart");

			if (flowchart.gameObject.GetComponent<StoryFlowchartExecutionContext>() != null) {
				throw new InvalidOperationException(
					"Cannot add a " + typeof(StoryFlowchartExecutionContext).Name + " "
					+ "to a Flowchart that already has one attached."
				);
			}

			StoryFlowchartExecutionContext res = flowchart.gameObject.AddComponent<StoryFlowchartExecutionContext>();
			res.ControllingTask = controllingTask;

			return res;
		}
	}
}
