﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RevolitionGames.Choices
{

    public enum Choice
    {

        S1SpeakUp,
        S1StaySilent,
        S1PutHandUp,
        S1LetThemContinue,
        S2FoldLaundry,
        S2DoNotFoldLaundry,
        S3SpeakUp,
        S3StaySilent,
        S3AgreeWithTeacher,
        S3AgreeWithLiam,

    }
}

