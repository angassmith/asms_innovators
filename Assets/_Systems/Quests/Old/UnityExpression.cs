﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace RevolitionGames.UnityExpressions
{
	//Better to just add to Expressions (using implicit casts probably)
	//However expressions are compiled (not interpreted, even optionally) which doesn't work on AOT/iOS
	//Solution: Create and expression interpreter
	//	public abstract class UnityExpression
	//	{
	//		public abstract object EvaluateUntyped();
	//	}
	//	
	//	public abstract class UnityExpression<TResult> : UnityExpression
	//	{
	//		public abstract TResult Evaluate();
	//	
	//		public sealed override object EvaluateUntyped() { return Evaluate(); }
	//	}
	//	
	//	public static class UnityExpressions
	//	{
	//		public UnityBinaryExpression<int, int, int> Add(UnityExpression<int> left, UnityExpression<int> right)
	//		{
	//			return new UnityBinaryExpression<int, int, int>((left, right) => )
	//		}
	//	}
	//	
	//	public class UnityBinaryExpression<T1, T2, TResult> : UnityExpression<TResult>
	//	{
	//		public Func<T1, T2, TResult> Func { get; private set; }
	//		public UnityExpression<T1> Left { get; private set; }
	//		public UnityExpression<T2> Right { get; private set; }
	//	
	//		public UnityBinaryExpression(Func<T1, T2, TResult> func, UnityExpression<T1> left, UnityExpression<T2> right)
	//		{
	//			this.Func = func;
	//			this.Left = left;
	//			this.Right = right;
	//		}
	//	
	//		public override TResult Evaluate()
	//		{
	//			return Func()
	//		}
	//	}
	
	//	class foo
	//	{
	//		void a()
	//		{
	//			var expr = Expression.Lambda<Func<int>>(Expression.Add(Expression.Constant(1), Expression.Constant(2)));
	//			((MulticastDelegate)expr.Compile()).DynamicInvoke()
	//		}
	//	}
}

//*/