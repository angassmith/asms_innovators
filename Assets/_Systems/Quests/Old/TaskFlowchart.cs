﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public class TaskFlowchart : Flowchart
	{
		public const string StartBlockName = "Start";

		public event EventHandler<NextTasksEventArgs> Finished;

		[SerializeField]
		private List<string> _nextTasks;
		public ReadOnlyCollection<string> CommonNextTasks { get { return _nextTasks.AsReadOnly(); } }

		[NonSerialized]
		private Block _startBlock;
		public Block StartBlock { get { return _startBlock; } }

		public TaskFlowchart()
		{
			BlockSignals.OnBlockEnd += BlockSignals_OnBlockEnd;
		}

		protected override void Start()
		{
			foreach (Block block in this.GetComponents<Block>())
			{
				foreach (Command command in block.CommandList)
				{
					var end = command as TaskFlowchartCustomEnd;
					if (end != null) end.FlowchartStopped += Finish;
				}
			}
			base.Start();
		}

		public void StartFlowchart()
		{

		}

		//Flowcharts have no finished event or virtual method.
		//This is because they're not guaranteed to finish, as though there may be no
		//running blocks, there may be one waiting for an event.
		//This finish detection does not account for that - as soon as there are no
		//blocks running, the flowchart exits. To continue the flowchart, just use a
		//wait command or something.
		private void BlockSignals_OnBlockEnd(Block block)
		{
			if (ReferenceEquals(block.GetFlowchart(), this))
			{
				if (!this.HasExecutingBlocks())
				{
					//TODO: Check that neither of the following is true:
					// - HasExecutingBlocks is always false for a short period between blocks,
					//   during which BlockSignals.OnBlockEnd is fired,
					// - HasExecutingBlocks is always true at this point because a block was just executing.

					Finish(this, new NextTasksEventArgs(CommonNextTasks)); //No next tasks
				}
			}
		}

		private void Finish(object sender, NextTasksEventArgs e)
		{
			this.StopAllBlocks();
			this.StopAllCoroutines();
			Finished.Fire(sender, e);
		}
	}
}


//*/