﻿/*

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

namespace RevolitionGames.Quests
{
	public interface ITaskSlot : IStoryPartSlot
	{
		new event EventHandler<StoryPartStatusEventArgs<ITaskSlot>> StatusChanged;
		new event EventHandler<StoryPartEventArgs<ITaskSlot>> Activated;
		new event EventHandler<StoryPartEventArgs<ITaskSlot>> Completed;
		new event EventHandler<StoryPartEventArgs<ITaskSlot>> Destroyed;
		new event EventHandler<StoryPartEventArgs<ITaskSlot>> Deactivated;
		bool IsFinal { get; }
		new IActiveTask Create(bool canReturnExisting);
		new IActiveTask CreationResult { get; }
		QuestData ContainingQuest { get; }
		EpisodeData ContainingEpisode { get; }
	}
}

//*/