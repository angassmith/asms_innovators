/*


using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using UnityEngine;


public class TestForLoop : MonoBehaviour
{
	private Dictionary<string, QuestCreator> _quests = new Dictionary<string, QuestCreator>(); // you can add to this

	public ReadOnlyDictionary<string, QuestCreator> Quests // but then THIS is read-only
	{
		get { return new ReadOnlyDictionary<string, QuestCreator>(_quests); }
	}
	


	void Start()
	{
		

		foreach (XElement xquest in xdoc.Element("questList").Descendants("quest"))
		{
			string questCodeName = (string)xquest.Element("questCodeName");
			string questName = (string)xquest.Element("name");
			string questDescription = (string)xquest.Element("description");
			List<TaskCreator> tasks = new List<TaskCreator>();
			

				
			foreach (XElement xtask in xquest.Element("taskList").Descendants())
			{
				switch (xtask.Name.ToString())
				{
					case "specificCollectionTask":
						tasks.Add(
							new SpecificCollectionTaskCreator(
								(string)xtask.Element("taskName"),
								(string)xtask.Element("description"),
								(bool)xtask.Attribute("isOptional"),
								(string)xtask.Element("collectableItemName"),
								(int)xtask.Element("quantity")
							)
						);
						break;
					case "typeCollectionTask":
						tasks.Add(
							new TypeCollectionTaskCreator(
								(string)xtask.Element("taskName"),
								(string)xtask.Element("description"),
								(bool)xtask.Attribute("isOptional"),
								(string)xtask.Element("collectableItemName"),
								(int)xtask.Element("quantity")
							)
						);
						break;
				}
			}

			_quests.Add(questCodeName, new QuestCreator(questCodeName, questName, questDescription, "id", tasks)); 
																			  
		}

	}

}

*/





