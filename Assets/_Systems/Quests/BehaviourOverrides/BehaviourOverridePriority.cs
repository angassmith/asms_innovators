﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.BehaviourOverriding
{
	public enum BehaviourOverridePriority
	{
		//Do not change the values, as they are used for determining priorities etc
		Episode = 0,
		Quest = 1,
		Task = 2,
		FocusedQuest = 3,
		FocusedQuestsTask = 4,

		MinPriority = Episode,
		MaxPriority = FocusedQuestsTask,
	}
}


//*/