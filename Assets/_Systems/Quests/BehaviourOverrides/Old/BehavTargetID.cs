﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.BehaviourOverriding
{
	//Not needed as for a given IBehaviourOverridable<TBehav>, each different behaviour kind
	//uses a different version of the IBehaviourOverridable<TBehav> interface, so a different
	//TargetCodeName is used (if needed).
	//The behaviour-overridden object then fetches only the behaviours of a certain type,
	//which serves the same function as BehaviourKind (filtering by behaviour type).
	//
	//	[Serializable]
	//	public struct BehavTargetID
	//	{
	//		public readonly string BehaviourKind;
	//		public readonly string TargetCodeName;
	//	
	//		public bool IsEmpty { get { return BehaviourKind == null; } }
	//	
	//		public BehavTargetID(string behaviourKind, string targetCodeName)
	//		{
	//			if (string.IsNullOrEmpty(behaviourKind)) throw new ArgumentException("Cannot be null or empty", "behaviourKind");
	//			if (string.IsNullOrEmpty(targetCodeName)) throw new ArgumentException("Cannot be null or empty", "targetCodeName");
	//	
	//			this.BehaviourKind = behaviourKind;
	//			this.TargetCodeName = targetCodeName;
	//		}
	//	
	//		public override string ToString()
	//		{
	//			return (
	//				typeof(BehavTargetID).Name + " "
	//				+ "{ "
	//				+ "BehaviourKind = '" + BehaviourKind + "', "
	//				+ "TargetCodeName = '" + TargetCodeName + "'"
	//				+ " }"
	//			);
	//		}
	//	
	//		public override int GetHashCode()
	//		{
	//			int hash = 17;
	//			hash = hash * 23 + BehaviourKind.GetHashCode();
	//			hash = hash * 23 + TargetCodeName.GetHashCode();
	//			return hash;
	//		}
	//	
	//		public static bool Equals(BehavTargetID a, BehavTargetID b)
	//		{
	//			return a.BehaviourKind == b.BehaviourKind && a.TargetCodeName == b.TargetCodeName;
	//		}
	//	
	//		public override bool Equals(object obj)
	//		{
	//			if (obj is BehavTargetID) return Equals((BehavTargetID)obj);
	//			else return false;
	//		}
	//	
	//		public static bool operator ==(BehavTargetID a, BehavTargetID b) { return Equals(a, b); }
	//		public static bool operator !=(BehavTargetID a, BehavTargetID b) { return !Equals(a, b); }
	//	}
}

//*/