﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;
using RevolitionGames.Quests;

public abstract class BehaviourOverride_original
{
	private static System.Random random = new System.Random();

	public StaticIdentifier TargetID { get; private set; }

	public BehaviourOverride_original(StaticIdentifier targetID)
	{
		if (targetID.Kind == null || targetID.CodeName == null) throw new ArgumentException("targetID.Kind == null || targetID.CodeName == null", "targetID");
		this.TargetID = targetID;
	}



	/// <summary>
	/// Casts a <see cref="BehaviourOverride_original"/> to the specified derived type, throwing a descriptive <see cref="InvalidOperationException"/> if the cast is invalid.
	/// </summary>
	/// <exception cref="InvalidOperationException">behavOverride is not of type <typeparamref name="TBehav"/></exception>
	public static TBehav CastOverride<TBehav>(BehaviourOverride_original behavOverride)
		where TBehav : BehaviourOverride_original
	{
		if (behavOverride == null) throw new ArgumentNullException("behavOverride");

		TBehav casted = behavOverride as TBehav;

		if (casted == null)
		{
			throw new InvalidOperationException(
				"The provided BehaviourOverride with target \""
				+ behavOverride.TargetID
				+ "\" and of type \""
				+ behavOverride.GetType() 
				+ "\" could not be downcast to type \""
				+ typeof(TBehav).FullName
				+ "\"."
			);
		}

		return casted;
	}

	/// <summary>
	/// Returns all of the overrides that are active and target the specified <see cref="IBehaviourOverridable{TBehav}"/>,
	/// regardless of their priority.
	/// </summary>
	/// <exception cref="ArgumentNullException"><paramref name="story"/> or <paramref name="overridden"/> is null</exception>
	/// <exception cref="InvalidOperationException">An active <see cref="StoryBehaviourOverride"/> that targets the specified <see cref="IBehaviourOverridable{TBehav}"/> is not of type <typeparamref name="TBehav"/></exception>
	public static IEnumerable<Tuple<TBehav, BehaviourOverridePriority>> GetAllActiveOverrides<TBehav>(Story story, IBehaviourOverridable_original<TBehav> overridden)
		where TBehav : BehaviourOverride_original
	{
		if (story == null) throw new ArgumentNullException("story");
		if (overridden == null) throw new ArgumentNullException("overridden");
		
		StaticIdentifier overriddenID = overridden.GetID();
		
		foreach (QuestData quest in story.CurrentEpisode.Quests.Values.Where(q => q.Status == StoryPartStatus.Active))
		{
			foreach (ITaskData task in quest.Tasks.Values.Where(t => t.Status == StoryPartStatus.Active))
			{
				BehaviourOverrideTaskData behaviourOverrideTask = task as BehaviourOverrideTaskData;
				if (behaviourOverrideTask != null && behaviourOverrideTask.Behaviour.TargetID == overriddenID) {
					yield return Tuple.Create(CastOverride<TBehav>(behaviourOverrideTask.Behaviour), BehaviourOverridePriority.TaskMain);
				}

				foreach (BehaviourOverride_original found in task.BehaviourOverrides[overriddenID]) {
					yield return Tuple.Create(CastOverride<TBehav>(found), BehaviourOverridePriority.TaskPassive);
				}
			}
			
			foreach (BehaviourOverride_original found in quest.BehaviourOverrides[overriddenID]) {
				yield return Tuple.Create(CastOverride<TBehav>(found), BehaviourOverridePriority.Quest);
			}
		}
		
		foreach (BehaviourOverride_original found in story.CurrentEpisode.BehaviourOverrides[overriddenID]) {
			yield return Tuple.Create(CastOverride<TBehav>(found), BehaviourOverridePriority.Episode);
		}
	}

	/// <summary>
	/// Returns the current highest-priority overrides that are active and target the specified <see cref="IBehaviourOverridable{TBehav}"/>.
	/// </summary>
	/// <exception cref="ArgumentNullException"><paramref name="story"/> or <paramref name="overridden"/> is null</exception>
	/// <exception cref="InvalidOperationException">Thrown by <see cref="GetAllActiveOverrides{TBehav}(Story, IBehaviourOverridable{TBehav})"/>, which is used internally</exception>
	public static IEnumerable<TBehav> GetCurrentOverrides<TBehav>(Story story, IBehaviourOverridable_original<TBehav> overridden)
		where TBehav : BehaviourOverride_original
	{

		IEnumerable<Tuple<TBehav, BehaviourOverridePriority>> allActiveOverrides = GetAllActiveOverrides(story, overridden);

		BehaviourOverridePriority highestPriority = (BehaviourOverridePriority)allActiveOverrides.Max(x => (int)x.Item2);

		IEnumerable<TBehav> highestPriorityOverrides = (
			allActiveOverrides.Where(x => x.Item2 == highestPriority).Select(x => x.Item1)
		);

		if (highestPriorityOverrides.Count() > 0)
		{
			return highestPriorityOverrides;
		}
		else
		{
			throw new InvalidOperationException(
				"The are no BehaviourOverrides for the specified IBehaviourOverrideable '"
				+ overridden.GetID().ToString()
				+ "' in any active tasks or quests, or the current episode."
			); //Make a custom exception class for incorrect xml?
		}
	}


	/// <summary>
	/// Selects a random element from <see cref="GetCurrentOverrides{TBehav}(Story, IBehaviourOverridable{TBehav})"/>
	/// </summary>
	/// <exception cref="ArgumentNullException"><paramref name="story"/> or <paramref name="overridden"/> is null</exception>
	/// <exception cref="InvalidOperationException">Thrown by <see cref="GetCurrentOverrides{TBehav}(Story, IBehaviourOverridable{TBehav})"/>, which is used internally</exception>
	public static BehaviourOverride_original SelectCurrentOverride<TBehav>(Story game, IBehaviourOverridable_original<TBehav> overridden)
		where TBehav : BehaviourOverride_original
	{
		IEnumerable<TBehav> currentOverrides = GetCurrentOverrides(game, overridden);
		return currentOverrides.ElementAt(random.Next(0, currentOverrides.Count()));
	}
}

//	public abstract class BehaviourOverride<TOverridden> : BehaviourOverride
//		where TOverridden : IBehaviourOverridable
//	{
//		public TOverridden Target { get; private set; }
//	
//		public override IBehaviourOverridable UntypedTarget { get { return Target; } }
//	
//		public BehaviourOverride()
//		{
//	
//		}
//	}

//*/