﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Quests.BaseStoryParts;

namespace RevolitionGames.Quests.BehaviourOverriding
{
	public struct BehaviourOverrideWithContext<TBehav>
		where TBehav : BehaviourOverride
	{
		public TBehav Behaviour { get; private set; }
		public BehaviourOverridePriority Priority { get; private set; }
		public IActiveStoryPart Context { get; private set; }

		public BehaviourOverrideWithContext(TBehav behaviour, BehaviourOverridePriority priority, IActiveStoryPart context)
		{
			if (behaviour == null) throw new ArgumentNullException("behaviour");
			if (context == null) throw new ArgumentNullException("context");

			this.Behaviour = behaviour;
			this.Priority = priority;
			this.Context = context;
		}
	}
}

//*/