﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RevolitionGames.Utilities.Alex;
using RevolitionGames.Quests.BaseStoryParts;

namespace RevolitionGames.Quests.BehaviourOverriding
{
	public sealed class BehaviourOverrideProvider
	{
		public Story Story { get; private set; }

		public BehaviourOverrideProvider(Story story)
		{
			if (story == null) throw new ArgumentNullException("story");

			this.Story = story;
		}





		/// <summary>
		/// Returns all of the overrides that are active and target the specified <see cref="IBehaviourOverrideable"/>,
		/// regardless of their priority.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="Story"/> or <paramref name="target"/> is null</exception>
		public IEnumerable<BehaviourOverrideWithContext<TBehav>> GetAllActiveBehaviours<TBehav>(IBehaviourOverridable<TBehav> target)
			where TBehav : BehaviourOverride
		{
			if (target == null) throw new ArgumentNullException("target");

			if (Story.Episodes.Count == 0) yield break;
			if (!Story.CurrentEpisodeSlot.IsActive) yield break;

			foreach (ActiveQuest quest in Story.CurrentEpisode.ActiveQuests)
			{
				var questBehavPriority = quest.IsFocus ? BehaviourOverridePriority.FocusedQuest : BehaviourOverridePriority.Quest;
				var taskBehavPriority = quest.IsFocus ? BehaviourOverridePriority.FocusedQuestsTask : BehaviourOverridePriority.Task;

				foreach (ActiveTask task in quest.ActiveTasks)
				{
					foreach (var behav in task.BehaviourOverrides[target.CodeName].OfType<TBehav>()) {
						yield return new BehaviourOverrideWithContext<TBehav>(behav, taskBehavPriority, task);
					}
				}

				foreach (var behav in quest.BehaviourOverrides[target.CodeName].OfType<TBehav>()) {
					yield return new BehaviourOverrideWithContext<TBehav>(behav, questBehavPriority, quest);
				}
			}

			foreach (var behav in Story.CurrentEpisode.BehaviourOverrides[target.CodeName].OfType<TBehav>()) {
				yield return new BehaviourOverrideWithContext<TBehav>(behav, BehaviourOverridePriority.Episode, Story.CurrentEpisode);
			}
		}


		/// <summary>
		/// Returns the highest <see cref="BehaviourOverridePriority"/> from a collection.
		/// Stops enumeration and returns immediately if <see cref="BehaviourOverridePriority.MaxPriority"/> is found.
		/// Returns <see langword="null"/> if <paramref name="collection"/> is empty.
		/// </summary>
		/// <param name="collection"></param>
		/// <returns></returns>
		public BehaviourOverridePriority? GetHighestPriority(IEnumerable<BehaviourOverridePriority> collection)
		{
			const int MinPriority = (int)BehaviourOverridePriority.MinPriority;
			const int MaxPriority = (int)BehaviourOverridePriority.MaxPriority;

			int highestPriority = checked(MinPriority - 1);
			foreach (int priority in collection.Select(x => (int)x))
			{
				if (priority > highestPriority) highestPriority = priority;

				//Stop early if possible (the maximum priority has been reached, so nothing can be higher)
				if (priority == MaxPriority) return (BehaviourOverridePriority)highestPriority;
			}

			return highestPriority == (MinPriority - 1) ? null : (BehaviourOverridePriority?)highestPriority;
		}


		/// <summary>
		/// Returns the current highest-priority overrides that are active and target the specified
		/// <see cref="IBehaviourOverrideable"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="story"/> or <paramref name="target"/> is null</exception>
		/// <exception cref="InvalidOperationException">
		/// Thrown by <see cref="GetAllActiveBehaviours{TBehav}(IBehaviourOverridable{TBehav})"/>,
		/// which is used internally
		/// </exception>
		public IEnumerable<BehaviourOverrideWithContext<TBehav>> GetCurrentBehaviours<TBehav>(IBehaviourOverridable<TBehav> target)
			where TBehav : BehaviourOverride
		{
			if (target == null) throw new ArgumentNullException("target");

			var allActiveBehaviours = GetAllActiveBehaviours(target);

			BehaviourOverridePriority? highestPriority = GetHighestPriority(allActiveBehaviours.Select(x => x.Priority));

			if (highestPriority.HasValue)
			{
				return allActiveBehaviours.Where(x => x.Priority == highestPriority);
			}
			else
			{
				return Enumerable.Empty<BehaviourOverrideWithContext<TBehav>>();
			}
		}


		/// <summary>
		/// Selects a random element from
		/// <see cref="GetCurrentBehaviours{TBehav}(IBehaviourOverridable{TBehav})"/>.
		/// The <see cref="IActiveStoryPart"/> containing the selected <see cref="BehaviourOverride"/>
		/// is provided through an out parameter.
		/// If no <see cref="BehaviourOverride"/>s are found, a fallback (and optionally fallback context)
		/// may optionally be used.
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="target"/> is null</exception>
		/// <exception cref="InvalidOperationException">
		/// Thrown by <see cref="GetCurrentBehaviours{TBehav}(IBehaviourOverridable{TBehav})"/>,
		/// which is used internally
		/// </exception>
		public TBehav SelectCurrentBehaviour<TBehav>(IBehaviourOverridable<TBehav> target, out IActiveStoryPart context, TBehav fallback = null, IActiveStoryPart fallbackContext = null)
			where TBehav : BehaviourOverride
		{
			if (target == null) throw new ArgumentNullException("target");

			var currentOverrides = GetCurrentBehaviours(target);

			BehaviourOverrideWithContext<TBehav> random;
			if (currentOverrides.TryGetRandomElement(ThreadSafeRandom.Random, out random)) {
				context = random.Context;
				return random.Behaviour;
			} else {
				if (fallback != null) {
					context = fallbackContext;
					return fallback;
				} else {
					throw CreateNoBehavioursException(target);
				}
			}
		}


		public static InvalidOperationException CreateNoBehavioursException<TBehav>(IBehaviourOverridable<TBehav> target)
			where TBehav : BehaviourOverride
		{
			return new InvalidOperationException(
				"The are no " + typeof(BehaviourOverride) + "s "
				+ "for the specified IBehaviourOverrideable<TBehav> '" + target.CodeName.ToString() + "' "
				+ "of type '" + target.GetType() + "' "
				+ "in any active tasks or quests, or the current episode (and no fallback was provided)."
			);
		}
	}
}

//*/