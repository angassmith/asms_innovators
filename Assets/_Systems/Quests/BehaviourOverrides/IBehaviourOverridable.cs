﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.BehaviourOverriding
{
	/// <summary>
	/// Note: The generic parameter allows a single class to have multiple behaviours overridden,
	/// by implementing multiple variants of this interface explicitly.
	/// </summary>
	public interface IBehaviourOverridable<TBehav>
		where TBehav : BehaviourOverride
	{
		string CodeName { get; }
	}
}

//*/