﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.BehaviourOverriding
{
	public abstract class BehaviourOverride
	{
		public string TargetCodeName { get; private set; }

		protected BehaviourOverride(string targetCodeName)
		{
			if (string.IsNullOrEmpty(targetCodeName)) {
				throw new ArgumentException("Cannot be null or empty", "targetCodeName");
			}
			this.TargetCodeName = targetCodeName;
		}


		/// <summary>
		/// Downcasts the current <see cref="BehaviourOverride"/> to the specified derived type,
		/// throwing a descriptive <see cref="InvalidOperationException"/> if the cast is invalid.
		/// </summary>
		/// <exception cref="InvalidOperationException">The current object is not of type <typeparamref name="TBehav"/></exception>
		public TBehav CastToBehaviourType<TBehav>()
			where TBehav : BehaviourOverride
		{
			TBehav casted = this as TBehav;
		
			if (casted == null)
			{
				throw new InvalidOperationException(
					"The provided " + typeof(BehaviourOverride).Name + " with target \""
					+ TargetCodeName
					+ "\" and of type \""
					+ this.GetType() 
					+ "\" could not be cast to type \""
					+ typeof(TBehav).FullName
					+ "\"."
				);
			}
			else
			{
				return casted;
			}
		}




		#region Utilities
	
		protected static T ThrowIfArgNull<T>(T arg, string argName)
		{
			if (ReferenceEquals(arg, null)) throw new ArgumentNullException(argName);
			else return arg;
		}
	
		#endregion

	}

	#region Old

	//Not needed (as the only purpose of the generic parameter is for use within the class - outside classes
	//can always use a generic parameter when needed)
	//
	//	public abstract class StoryPartBehaviour<TBehav> : StoryPartBehaviour
	//		where TBehav : StoryPartBehaviour<TBehav>
	//	{
	//		private StoryPartBehaviour()
	//		{
	//			if (!(this is TBehav)) {
	//				throw new InvalidOperationException(
	//					"The StoryPartBehaviour<TBehav> of type '" + this.GetType().FullName + "' "
	//					+ "is not convertible to TBehav type '" + typeof(TBehav).GetType() + "'."
	//				);
	//			}
	//		}
	//	}



	#region Old

	//This version attempted to cache the currently active behaviour and find the new one
	//each time the story progress changed.
	//However, I forgot that if for example multiple npc dialogue behaviours are active,
	//and the player speaks to the npc multiple times, then even though the story progress
	//stays the same, a different random dialogue should be used each time.
	//Also this strategy isn't necessarily more efficient anyway, as all behaviours must be
	//refreshed every time the relevant part of the progress changes, while normally one
	//behaviour must be re-found every time eg. the player talks to an npc.
	//Finally, its more complex (and doesn't work in its current format).
	//
	//	public abstract class StoryPartBehaviour : IStoryPartBehaviour
	//	{
	//		private readonly IBehavActivationController _activationController;
	//	
	//		public event EventHandler<StoryPartBehaviourSwitchedEventArgs> Activated;
	//		public event EventHandler<StoryPartBehaviourSwitchedEventArgs> Deactivated;
	//	
	//		protected StoryPartBehaviour(IBehavActivationController activationController)
	//		{
	//			ThrowIfArgNull(activationController, "activationController");
	//			this._activationController = activationController;
	//		}
	//	
	//		protected void SwitchTo(StoryPartBehaviour newBehaviour)
	//		{
	//			this._activationController.Deactivate();
	//			newBehaviour._activationController.Activate();
	//	
	//			var e = new StoryPartBehaviourSwitchedEventArgs(this, newBehaviour);
	//			this.Deactivated.Fire(this, e);
	//			newBehaviour.Activated.Fire(newBehaviour, e);
	//		}
	//		
	//	
	//	
	//		/// <summary>
	//		/// Downcasts the current <see cref="StoryPartBehaviour"/> to the specified derived type,
	//		/// throwing a descriptive <see cref="InvalidOperationException"/> if the cast is invalid.
	//		/// </summary>
	//		/// <exception cref="InvalidOperationException">The current object is not of type <typeparamref name="TBehav"/></exception>
	//		public TBehav AsBehaviourType<TBehav>()
	//			where TBehav : StoryPartBehaviour
	//		{
	//			TBehav casted = this as TBehav;
	//	
	//			if (casted == null)
	//			{
	//				throw new InvalidOperationException(
	//					"The provided " + typeof(StoryPartBehaviour) + " with target \""
	//					+ "<TODO: Target ID>"
	//					+ "\" and of type \""
	//					+ this.GetType() 
	//					+ "\" could not be cast to type \""
	//					+ typeof(TBehav).FullName
	//					+ "\"."
	//				);
	//			}
	//			else
	//			{
	//				return casted;
	//			}
	//		}
	//	
	//	
	//		protected interface IBehavActivationController
	//		{
	//			void Activate();
	//			void Deactivate();
	//			//	void OnStoryPartDeactivated();
	//			//	void OnSubStoryPartActivated();
	//		}
	//	
	//		#region Utilities
	//	
	//		protected static T ThrowIfArgNull<T>(T arg, string argName)
	//		{
	//			if (ReferenceEquals(arg, null)) throw new ArgumentNullException(argName);
	//			else return arg;
	//		}
	//	
	//		#endregion
	//	}
	//	
	//	public abstract class StoryPartBehaviour<TBehav> : StoryPartBehaviour
	//		where TBehav : StoryPartBehaviour<TBehav>
	//	{
	//	
	//	
	//	
	//	
	//		private StoryPartBehaviour(IBehavActivationController activationController)
	//			: base(activationController)
	//		{
	//			if (!(this is TBehav)) {
	//				throw new InvalidOperationException(
	//					"The StoryPartBehaviour<TBehav> of type '" + this.GetType().FullName + "' "
	//					+ "is not convertible to TBehav type '" + typeof(TBehav).GetType() + "'."
	//				);
	//			}
	//		}
	//	
	//		protected StoryPartBehaviour(EpisodeData episode)
	//			: this(new EpisodeBehavActivationController(episode))
	//		{ }
	//		
	//		protected StoryPartBehaviour(QuestData quest)
	//			: this(new QuestBehavActivationController(quest))
	//		{ }
	//		
	//		protected StoryPartBehaviour(ITaskData task)
	//			: this(new TaskBehavActivationController(task))
	//		{ }
	//	
	//	
	//	
	//	
	//	
	//	
	//		private class EpisodeBehavActivationController : IBehavActivationController
	//		{
	//			public EpisodeData Episode { get; private set; }
	//		
	//			public EpisodeBehavActivationController(EpisodeData episode) {
	//				ThrowIfArgNull(episode, "episode");
	//				this.Episode = episode;
	//			}
	//		
	//			public void Activate()
	//			{
	//				Episode.Deactivated += OnEpisodeDeactivated;
	//			
	//				foreach (var quest in Episode.Quests.Values) {
	//					quest.Activated += OnContainedQuestActivated;
	//				}
	//			}
	//		
	//			public void Deactivate()
	//			{
	//				Episode.Deactivated -= OnEpisodeDeactivated;
	//		
	//				foreach (var quest in Episode.Quests.Values) {
	//					quest.Activated -= OnContainedQuestActivated;
	//				}
	//			}
	//		
	//			private void OnEpisodeDeactivated(object sender, StoryPartEventArgs<EpisodeData> e)
	//			{
	//				//TODO
	//			}
	//		
	//			private void OnContainedQuestActivated(object sender, StoryPartEventArgs<QuestData> e)
	//			{
	//				//TODO
	//			}
	//		}
	//		
	//		private class QuestBehavActivationController : IBehavActivationController
	//		{
	//			public QuestData Quest { get; private set; }
	//		
	//			public QuestBehavActivationController(QuestData quest) {
	//				ThrowIfArgNull(quest, "quest");
	//				this.Quest = quest;
	//			}
	//		
	//			public void Activate()
	//			{
	//				Quest.Deactivated += OnQuestDeactivated;
	//			
	//				foreach (var quest in Quest.Tasks.Values) {
	//					quest.Activated += OnContainedTaskActivated;
	//				}
	//			}
	//		
	//			public void Deactivate()
	//			{
	//				Quest.Deactivated -= OnQuestDeactivated;
	//		
	//				foreach (var task in Quest.Tasks.Values) {
	//					task.Activated -= OnContainedTaskActivated;
	//				}
	//			}
	//		
	//			private void OnQuestDeactivated(object sender, StoryPartEventArgs<QuestData> e)
	//			{
	//				//TODO
	//			}
	//		
	//			private void OnContainedTaskActivated(object sender, StoryPartEventArgs<ITaskData> e)
	//			{
	//				//TODO
	//			}
	//		}
	//		
	//		private class TaskBehavActivationController : IBehavActivationController
	//		{
	//			public ITaskData Task { get; private set; }
	//		
	//			public TaskBehavActivationController(ITaskData task) {
	//				ThrowIfArgNull(task, "task");
	//				this.Task = task;
	//			}
	//		
	//			public void Activate()
	//			{
	//				Task.Deactivated += OnTaskDeactivated;
	//			}
	//		
	//			public void Deactivate()
	//			{
	//				Task.Deactivated -= OnTaskDeactivated;
	//			}
	//		
	//			private void OnTaskDeactivated(object sender, StoryPartEventArgs<ITaskData> e)
	//			{
	//				//TODO
	//			}
	//		}
	//	
	//	
	//		
	//	}
	//	
	//	public class StoryPartBehaviourSwitchedEventArgs : EventArgs
	//	{
	//		public StoryBehaviourOverride OldBehaviour { get; private set; }
	//		public StoryBehaviourOverride NewBehaviour { get; private set; }
	//		
	//		public StoryPartBehaviourSwitchedEventArgs(StoryBehaviourOverride oldBehaviour, StoryBehaviourOverride newBehaviour)
	//		{
	//			if (oldBehaviour == null) throw new ArgumentNullException("oldBehaviour");
	//			if (newBehaviour == null) throw new ArgumentNullException("newBehaviour");
	//	
	//			this.OldBehaviour = oldBehaviour;
	//			this.NewBehaviour = newBehaviour;
	//		}
	//	}

	#region Old

	//	public abstract class EpisodeBehaviour : StoryPartBehaviour
	//	{
	//		public EpisodeData Episode { get; private set; }
	//	
	//		public EpisodeBehaviour(EpisodeData episode)
	//		{
	//			ThrowIfArgNull(episode, "episode");
	//			this.Episode = episode;
	//		}
	//	
	//		protected override void Activate()
	//		{
	//			Episode.Deactivated += OnEpisodeDeactivated;
	//			
	//			foreach (QuestData quest in Episode.Quests.Values) {
	//				quest.Activated += OnContainedQuestActivated;
	//			}
	//		}
	//	
	//		protected override void Deactivate()
	//		{
	//			Episode.Deactivated -= OnEpisodeDeactivated;
	//	
	//			foreach (QuestData quest in Episode.Quests.Values) {
	//				quest.Activated -= OnContainedQuestActivated;
	//			}
	//		}
	//	
	//		private void OnEpisodeDeactivated(object sender, EventArgs e)
	//		{
	//			//TODO
	//		}
	//	
	//		private void OnContainedQuestActivated(object sender, StoryPartEventArgs<QuestData> e)
	//		{
	//			//TODO
	//		}
	//	}

	#region Old

	//	public abstract class BehaviourOverride
	//	{
	//	
	//	}
	//	
	//	public class ExampleBehaviourOverrideable : IBehaviourOverrideable<ExampleBehaviourOverride>
	//	{
	//		public ExampleBehaviourOverride CurrentBehaviour { get; private set; }
	//	
	//		public ExampleBehaviourOverrideable()
	//		{
	//	
	//		}
	//	
	//		private void OnBehaviourDeactivated(object sender, EventArgs e)
	//		{
	//	
	//		}
	//	}
	//	
	//	public class ExampleBehaviourOverride : BehaviourOverride
	//	{
	//		public event EventHandler<ExampleBehaviourOverride, EventArgs> Deactivated;
	//	
	//		public ExampleBehaviourOverride()
	//		{
	//	
	//		}
	//	
	//		public virtual void TalkTo(Player player)
	//		{
	//	
	//		}
	//	
	//		private void StoryPartChanged(object sender, EventArgs e)
	//		{
	//			Deactivated.Fire(this, EventArgs.Empty);
	//		}
	//	}

	#endregion

	#endregion

	#endregion

	#endregion
}

//*/