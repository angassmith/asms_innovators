﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Fungus;

namespace RevolitionGames.Utilities.Alex
{
	public static class FlowchartUtils
	{
		public static bool HasStalled(Flowchart flowchart)
		{
			//This has been tested to show the correct value when BlockSignals.OnBlockEnd is fired - it is
			//neither always false to represent a transition phase, nor always true to represent that the
			//previous block is executing.
			if (flowchart.HasExecutingBlocks()) return false;

			//Menus do not involve any blocks executing, so also need to be checked
			//This returns true if any flowchart has a menu open, so might be problematic
			//Hopefully this should be ok though
			var activeMenu = MenuDialog.ActiveMenuDialog;
			if (activeMenu != null && activeMenu.isActiveAndEnabled && activeMenu.DisplayedOptionsCount > 0) return false;

			//There will probably need to be more checks here

			return true;
		}

		/// <summary>
		/// Calls <see cref="Flowchart.FindBlock(string)"/>, and throws an <see cref="ArgumentException"/>
		/// if it returns null (the block is not found).
		/// </summary>
		/// <exception cref="ArgumentNullException"><paramref name="flowchart"/> or <paramref name="blockName"/> is null</exception>
		/// <exception cref="ArgumentException"><paramref name="flowchart"/> does not contain a block named <paramref name="blockName"/></exception>
		public static Block GetBlock(Flowchart flowchart, string blockName)
		{
			if (flowchart == null) throw new ArgumentNullException("flowchart");
			if (blockName == null) throw new ArgumentNullException("blockName");

			Block found = flowchart.FindBlock(blockName);
			if (found == null) {
				throw new ArgumentException(
					"The flowchart '" + flowchart + "' does not contain a block named '" + blockName + "'."
				);
			}
			return found;
		}
	}
}

//*/