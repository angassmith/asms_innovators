﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Fungus;
using UnityEngine;

namespace RevolitionGames.Quests
{
	[CommandInfo("Flow", 
		"Task Flowchart Custom End",
		"Stops the flowchart, and (through an event) provides a list of additional tasks to unlock next.")]
	public class TaskFlowchartCustomEnd : Command
	{
		[SerializeField] //And edit in inspector
		private List<string> _nextTasks;
		public ReadOnlyCollection<string> NextTasks { get { return _nextTasks.AsReadOnly(); } }

		public event EventHandler<TaskFlowchartStoppedEventArgs> FlowchartStopped;

		public override void OnEnter()
		{
			//Stop the flowchart (there would usually be a TaskFlowchartExecutor to do this, but there's no guarantee)
			Flowchart flowchart = this.GetFlowchart();
			flowchart.StopAllBlocks();
			flowchart.StopAllCoroutines();

			FlowchartStopped.Fire(this, new TaskFlowchartStoppedEventArgs(flowchart, this.ParentBlock, NextTasks));
		}
	}
}


