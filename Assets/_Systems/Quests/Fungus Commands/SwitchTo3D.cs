﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

namespace RevolitionGames
{

    [CommandInfo("Camera", "switch to 3d", "switch to 3d")]
    public class SwitchTo3D : Command
    {

        public override void OnEnter()
        {
            var twodimcamera = GameObject.FindGameObjectWithTag("2DCamera");

            var threedimcamera = GameObject.FindGameObjectWithTag("MainCamera");

            twodimcamera.GetComponent<Camera>().enabled = false;
            threedimcamera.GetComponent<Camera>().enabled = true;

            Continue();
        }



    }

}