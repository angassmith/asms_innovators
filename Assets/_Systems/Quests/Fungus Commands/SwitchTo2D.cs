﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

namespace RevolitionGames
{

    [CommandInfo("Camera", "Switch to 2d", "change to the 2d scene")]
    public class SwitchTo2D : Command
    {

        public override void OnEnter()
        {
            var twodimcamera = GameObject.FindGameObjectWithTag("2DCamera");

            var threedimcamera = GameObject.FindGameObjectWithTag("MainCamera");

            twodimcamera.GetComponent<Camera>().enabled = true;
            threedimcamera.GetComponent<Camera>().enabled = false;

            Continue();
        }



    }

}