﻿using System.IO;
using System.Xml.Schema;
using UnityEngine;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Xml;
using RevolitionGames.Quests.BehaviourOverriding;
using System;

namespace RevolitionGames.Quests
{

	[Obsolete("This is here for now so that the code in it can be used by StoryProvider implementations", error: true)]
	public class StoryDatabase
	{
		public Story ContainingStory { get; private set; }

		public static XNamespace QuestNamespace = "http://www.revolitiongames.com/experience/quests";


		public string queststring { get; set; }



		private Dictionary<string, QuestSlot> _quests = new Dictionary<string, QuestSlot>(); // you can add to this

		public ReadOnlyDictionary<string, QuestSlot> Quests // but then THIS is read-only
		{
			get { return new ReadOnlyDictionary<string, QuestSlot>(_quests); }
		}


		public static string QuestXMLPath
		{
			get
			{
				const string path = "QuestSystem/examplequest.xml";
				return Path.Combine(Application.dataPath, path);
			}
		}

		public static string QuestXMLSchemaPath
		{
			get
			{
				const string path = "QuestSystem/QuestSystemSchemav2.xml";
				return Path.Combine(Application.dataPath, path);
			}
		}

		public StoryDatabase()
		{
			
			FileStream QuestXMLFile = new FileStream(QuestXMLPath, FileMode.Open, FileAccess.Read);
			FileStream QuestXMLSchemaFile = new FileStream(QuestXMLSchemaPath, FileMode.Open, FileAccess.Read);

			var QuestXMLSchema = XmlSchema.Read(
				QuestXMLSchemaFile,
				XmlValidationExceptionLogger
			);
		

			var xreaderSettings = new System.Xml.XmlReaderSettings();
			xreaderSettings.Schemas.Add(QuestXMLSchema);
			xreaderSettings.ValidationType = System.Xml.ValidationType.Schema;
			xreaderSettings.ValidationEventHandler += XmlValidationExceptionLogger;

			var xreader = System.Xml.XmlReader.Create(QuestXMLFile, xreaderSettings);
			//var xreader = System.Xml.XmlReader.Create(QuestXMLFile);
			XDocument xdoc = XDocument.Load(xreader);

			XElement questListElement = xdoc.Element(QuestNamespace + "questList");
			IEnumerable<XElement> questElements = questListElement.Descendants(QuestNamespace + "quest");
		
		
			foreach (XElement xquest in questElements)
			{
				string questCodeName = (string)xquest.Element(QuestNamespace + "questCodeName");
				bool questisOptional = (bool)xquest.Element(QuestNamespace + "isOptional");
				string questName = (string)xquest.Element(QuestNamespace + "name");
				string questDescription = (string)xquest.Element(QuestNamespace + "description");
				List<TaskSlot> tasks = new List<TaskSlot>();


				//TODO: Uncomment and fix
				//	foreach (XElement xtask in xquest.Element(QuestNamespace + "taskList").Descendants())
				//	{
				//		switch (xtask.Name.LocalName)
				//		{
				//			case "specificCollectionTask":
				//				tasks.Add(
				//					new SpecificCollectionTaskData(
				//						(string)xtask.Element(QuestNamespace + "taskName"),
				//						(string)xtask.Element(QuestNamespace + "description"),
				//						(string)xtask.Element(QuestNamespace + "collectableItemName"),
				//						(int)xtask.Element(QuestNamespace + "quantity")
				//					)
				//				);
				//				break;
				//			case "typeCollectionTask":
				//				tasks.Add(
				//					new TypeCollectionTaskData(
				//						(string)xtask.Element(QuestNamespace + "taskName"),
				//						(string)xtask.Element(QuestNamespace + "description"),
				//						(string)xtask.Element(QuestNamespace + "itemCodeName"),
				//						(int)xtask.Element(QuestNamespace + "quantity")
				//					)
				//				);
				//				break;
				//		}
				//	}
				//	
				//	_quests.Add(questCodeName, new QuestData(questCodeName, questName, questDescription, questisOptional, "id", tasks));
																												  
			}

			Debug.Log(_quests.ToNiceString(true));

		}

		private void XmlValidationExceptionLogger(object sender, ValidationEventArgs e)
		{
			if (e.Severity == XmlSeverityType.Error)
			{
				Debug.LogException(e.Exception);
			}
			else
			{
				Debug.LogWarning(e.Exception);
			}
		}

	


	}
}
