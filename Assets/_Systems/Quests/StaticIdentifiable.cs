﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// An object that is identifiable through a manually-specified code name. Not suitable for objects that are given ID's dynamically. Suitable for objects that are given ID's through manually-written XML.
/// </summary>
public interface IStaticIdentifiable
{
	string Kind { get; }
	string CodeName { get; }
}

/// <summary>
/// The complete identifier for an <see cref="IStaticIdentifiable"/>.
/// </summary>
public struct StaticIdentifier
{
	public string Kind { get; private set; }
	public string CodeName { get; private set; }

	public StaticIdentifier(string kind, string codeName)
	{
		if (kind == null) throw new ArgumentNullException("kind");
		if (codeName == null) throw new ArgumentNullException("codeName");

		this.Kind = kind;
		this.CodeName = codeName;
	}

	public override string ToString()
	{
		return "StaticIdentifier { Kind = '" + Kind + "', CodeName = '" + CodeName + "' }";
	}

	public override int GetHashCode()
	{
		return Tuple.CombineHashCodes(Kind.GetHashCode(), CodeName.GetHashCode());
	}

	public bool Equals(StaticIdentifier other)
	{
		return this.Kind == other.Kind && this.CodeName == other.CodeName;
	}

	public override bool Equals(object obj)
	{
		if (obj is StaticIdentifier) return Equals((StaticIdentifier)obj);
		else return false;
	}

	public static bool operator ==(StaticIdentifier a, StaticIdentifier b) { return a.Equals(b); }
	public static bool operator !=(StaticIdentifier a, StaticIdentifier b) { return !a.Equals(b); }
}

public static class StaticIdentifiableExtensions
{
	public static StaticIdentifier GetID(this IStaticIdentifiable identifiable)
	{
		return new StaticIdentifier(identifiable.Kind, identifiable.CodeName);
	}
}