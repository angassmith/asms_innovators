﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests
{
	public struct TaskID
	{
		public string QuestCodeName { get; private set; }
		public string TaskCodeName { get; private set; }
		
		public bool IsEmpty {
			get { return QuestCodeName == null; }
		}

		public TaskID(string quest, string task)
		{
			if (quest == null) throw new ArgumentNullException("quest");
			if (task == null) throw new ArgumentNullException("task");

			this.QuestCodeName = quest;
			this.TaskCodeName = task;
		}

		public override int GetHashCode()
		{
			int hash = 17;
			hash = hash * 23 + QuestCodeName.GetHashCode();
			hash = hash * 23 + TaskCodeName.GetHashCode();
			return hash;
		}

		public override bool Equals(object obj)
		{
			return obj is TaskID && Equals(this, (TaskID)obj);
		}

		public static bool Equals(TaskID a, TaskID b)
		{
			return a.QuestCodeName == b.QuestCodeName && a.TaskCodeName == b.TaskCodeName;
		}

		public static bool operator ==(TaskID a, TaskID b) { return Equals(a, b); }
		public static bool operator !=(TaskID a, TaskID b) { return !(a == b); }
	}
}

//*/