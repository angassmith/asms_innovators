﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests
{
	/// <summary>
	/// Represents the progress through a task.
	/// Note that no distinction is made between the progress of unused-but-defined tasks
	/// and tasks that are invalid, to be implemented, etc
	/// (all of these are represented by <see cref="TaskProgress.Unused"/>).
	/// <para/>
	/// Note: Tasks must be designed such any more detailed progress (eg. number of tasks in a MultiTask
	/// that have been completed) can be calculated upon activation.
	/// </summary>
	public struct TaskProgress
	{
		public static TaskProgress Unused { get { return new TaskProgress(StoryPartStatus.Unused); } }
		public static TaskProgress Active { get { return new TaskProgress(StoryPartStatus.Active); } }
		public static TaskProgress Completed { get { return new TaskProgress(StoryPartStatus.Completed); } }
		public static TaskProgress Destroyed { get { return new TaskProgress(StoryPartStatus.Destroyed); } }

		public StoryPartStatus Status { get; private set; }

		public TaskProgress(StoryPartStatus status)
		{
			this.Status = status;
		}

		public override int GetHashCode() {
			return this.Status.GetHashCode();
		}
		public static bool Equals(TaskProgress a, TaskProgress b) {
			return a.Status == b.Status;
		}
		public override bool Equals(object obj) {
			return obj is TaskProgress && Equals(this, (TaskProgress)obj);
		}
		public static bool operator ==(TaskProgress a, TaskProgress b) { return Equals(a, b); }
		public static bool operator !=(TaskProgress a, TaskProgress b) { return !(a == b); }
	}
}

//*/