﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using System.Future;
using System.Xml.Linq;
using UnityEngine;
using RevolitionGames.Utilities.Alex;
using XmlReader = System.Xml.XmlReader;
using RevolitionGames.Saving.Types;

namespace RevolitionGames.Quests.Saving
{
	public sealed class StoryProgress
	{
		private readonly Story _story;
		private readonly StoryProgressSaveData _savedData;

		private readonly List<string> _completedEpisodes;
		public ReadOnlyCollection<string> CompletedEpisodes { get; private set; }

		public string CurrentEpisode { get; private set; } //TODO: Set, update, and save/load this
		public string FocusQuest { get; private set; } //TODO: Set, update, and save/load this



		private readonly Dictionary<string, InternalQuestProgress> _questProgresses;
		public IEnumerable<KeyValuePair<string, QuestProgress>> QuestProgresses {
			get {
				return _questProgresses.Select(
					kvp => new KeyValuePair<string, QuestProgress>(kvp.Key, kvp.Value)
				);
			}
		}

		public StoryProgress(Story story, StoryProgressSaveData savedData)
		{
			if (story == null) throw new ArgumentNullException("story");
			if (savedData == null) throw new ArgumentNullException("savedData");

			this._story = story;
			this._savedData = savedData;

			this._completedEpisodes = new List<string>(savedData.MainStoryProgress.CompletedEpisodes);
			this.CompletedEpisodes = new ReadOnlyCollection<string>(this._completedEpisodes);

			this.CurrentEpisode = savedData.MainStoryProgress.CurrentEpisode;
			this.FocusQuest = savedData.MainStoryProgress.FocusQuest;

			this._questProgresses = savedData.QuestProgresses.ToDictionary(
				keySelector: kvp => kvp.Key,
				elementSelector: kvp => new InternalQuestProgress(
					parentStoryProgress: this,
					codeName: kvp.Value.CodeName,
					status: kvp.Value.Status,
					taskProgresses: new Dictionary<string, TaskProgress>(kvp.Value.TaskProgresses)
				)
			);

			story.QuestStatusChanged += OnQuestStatusChanged;
			story.TaskStatusChanged += OnTaskStatusChanged;
			story.EpisodeActivated += OnEpisodeActivated;
			story.EpisodeCompleted += OnEpisodeCompleted;
			story.FocusQuestChanged += OnFocusQuestChanged;
		}



		//TODO: Fix the fact that if a task is added to a quest in an update,
		//it won't be included in the unused tasks

		//Note: Originally, unused TaskProgresses were not stored, however this did not allow
		//the fraction of the quest that has been completed to be calculated. Some code or comments
		//from the original approach might remain though
		//Possibly this should be changed, idk

		#region Progress tracking and updating (using events)

		private void OnQuestStatusChanged(object sender, StoryPartStatusEventArgs e)
		{
			var quest = e.StoryPart as QuestSlot;
			if (quest == null) throw new InvalidOperationException("Subscription is incorrect or event has a bug.");

			InternalQuestProgress existing;
			if (_questProgresses.TryGetValue(quest.CodeName, out existing)) {
				existing.UpdateToMatch(quest);
			} else {
				_questProgresses.Add(
					quest.CodeName,
					new InternalQuestProgress(
						parentStoryProgress: this,
						codeName: quest.CodeName,
						status: e.NewStatus,
						taskProgresses: ( //Note: See comment before InternalQuestProgress constructor
							quest.IsActive
							? quest.ActiveStoryPart.Tasks.Values.ToDictionary(
								keySelector: t => t.CodeName,
								elementSelector: t => new TaskProgress(t.Status)
							)
							: new Dictionary<string, TaskProgress>()
						)
					)
				);
			}
		}

		private void OnTaskStatusChanged(object sender, StoryPartStatusEventArgs e)
		{
			var task = e.StoryPart as TaskSlot;
			if (task == null) throw new InvalidOperationException("Subscription is incorrect or event has a bug.");

			InternalQuestProgress questProgress = _questProgresses[task.QuestSlot.CodeName]; //The quest is active so this will succeed (if it fails, there's a bug somewhere else)
			questProgress.UpdateTaskProgressToMatch(task);
		}

		private void OnEpisodeActivated(object sender, StoryPartEventArgs<EpisodeSlot> e)
		{
			this.CurrentEpisode = e.StoryPart.CodeName;
		}

		private void OnEpisodeCompleted(object sender, StoryPartEventArgs<EpisodeSlot> e)
		{
			this._completedEpisodes.Add(e.StoryPart.CodeName);
		}

		private void OnFocusQuestChanged(object sender, StoryPartEventArgs<QuestSlot> e)
		{
			this.FocusQuest = e.StoryPart.CodeName;
		}

		#endregion



		#region Public interface & related

		public StoryPartStatus GetEpisodeProgress(string codeName)
		{
			if (CurrentEpisode == codeName) return StoryPartStatus.Active;

			if (_completedEpisodes.Contains(codeName)) return StoryPartStatus.Completed;

			//All other episodes are unused (no episodes are ever destroyed as none are optional)
			return StoryPartStatus.Unused;
		}

		/// <summary>
		/// Returns the progress for the quest with the specified code name
		/// if it is stored in memory,
		/// otherwise loads it from disk if it is stored on disk,
		/// otherwise returns <see cref="QuestProgress.Unused"/>.
		/// <para/>
		/// Note that no distinction is made between the progress of unused-but-defined quests
		/// and quests that are invalid, to be implemented, etc.
		/// </summary>
		public QuestProgress GetQuestProgress(string codeName)
		{
			InternalQuestProgress questProgress;
			if (_questProgresses.TryGetValue(codeName, out questProgress))
			{
				return questProgress;
			}
			else
			{
				QuestProgressSaveData savedQuestProgress = _savedData.GetQuestProgress(codeName);
				questProgress = new InternalQuestProgress(
					parentStoryProgress: this,
					codeName: savedQuestProgress.CodeName,
					status: savedQuestProgress.Status,
					taskProgresses: new Dictionary<string, TaskProgress>(savedQuestProgress.TaskProgresses)
				);

				_questProgresses.Add(codeName, questProgress);

				return questProgress;
			}
		}

		/// <summary>
		/// Shortcut for <code>GetQuestProgress(taskID.QuestCodeName).GetTaskProgress(taskID.TaskCodeName)</code>
		/// </summary>
		public TaskProgress GetTaskProgress(TaskID taskID)
		{
			return GetQuestProgress(taskID.QuestCodeName).GetTaskProgress(taskID.TaskCodeName);
		}


		private void UnloadProgressOfUnneededQuests()
		{
			var referencedQuests = _story.CurrentEpisode.Quests;

			List<string> unneededCodeNames = (
				_questProgresses.Keys
				.Where(x => !referencedQuests.ContainsKey(x))
				.ToList()
			);

			foreach (var unneeded in unneededCodeNames) {
				_questProgresses.Remove(unneeded);
			}
		}

		//	internal void UnloadUnneededQuestSaveIDs()
		//	{
		//		var referencedQuests = _story.CurrentEpisode.Quests;
		//	
		//		List<string> unneededCodeNames = (
		//			_questSaveIDs.Keys
		//			.Where(x => !referencedQuests.ContainsKey(x))
		//			.ToList()
		//		);
		//	
		//		foreach (var unneeded in unneededCodeNames) {
		//			_questSaveIDs.Remove(unneeded);
		//		}
		//	}

		#endregion



		#region Quest progress class

		private class InternalQuestProgress : QuestProgress
		{
			internal static InternalQuestProgress Unused(StoryProgress parentStoryProgress, string codeName)
			{
				return new InternalQuestProgress(
					parentStoryProgress,
					codeName,
					StoryPartStatus.Unused,
					new Dictionary<string, TaskProgress>()
				);
			}

			//Note: Originally, unused TaskProgresses were not stored, however this did not allow
			//the fraction of the quest that has been completed to be calculated. Some code or comments
			//from the original approach might remain though

			internal InternalQuestProgress(StoryProgress parentStoryProgress, string codeName, StoryPartStatus status, Dictionary<string, TaskProgress> taskProgresses)
				: base(parentStoryProgress, codeName, status, taskProgresses)
			{
					
			}

			/// <summary>
			/// Updates the <see cref="InternalQuestProgress"/> to match the specified quest
			/// (which must have the same code name). If the quest is active, updates the <see cref="TaskProgress"/>es
			/// dictionary, by adding and replacing (but not removing) <see cref="TaskProgress"/>es.
			/// </summary>
			internal void UpdateToMatch(QuestSlot quest)
			{
				if (quest.CodeName != this.CodeName) {
					throw new ArgumentException(
						"Provided quest has code-name '" + quest.CodeName + "' but code-name '"
						+ this.CodeName + "' is required.",
						"quest"
					);
				}

				this.Status = quest.Status;

				//Note: See comment before constructor
				if (quest.IsActive)
				{
					foreach (var task in quest.ActiveStoryPart.Tasks.Values) {
						this._taskProgresses[task.CodeName] = new TaskProgress(task.Status);
					}
				}
			}

			internal void UpdateTaskProgressToMatch(TaskSlot task)
			{
				this._taskProgresses[task.CodeName] = new TaskProgress(task.Status);
			}
		}

		#endregion

	}
}

//*/