﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace RevolitionGames.Quests.Saving
{
	/// <summary>
	/// Represents the progress through a quest.
	/// Note that no distinction is made between the progress of unused-but-defined quests
	/// and quests that are invalid, to be implemented, etc (all of these have Status ==
	/// <see cref="StoryPartStatus.Unused"/>).
	/// </summary>
	public abstract class QuestProgress
	{
		public StoryProgress ParentStoryProgress { get; private set; }

		public string CodeName { get; private set; }

		public StoryPartStatus Status { get; protected set; }

		protected readonly Dictionary<string, TaskProgress> _taskProgresses;
		/// <summary>
		/// Note: This is a wrapper for a mutable dictionary, so though you might not be able to
		/// modify it, it may change.
		/// </summary>
		public ReadOnlyDictionary<string, TaskProgress> TaskProgresses { get; private set; }

		internal QuestProgress(StoryProgress parentStoryProgress, string codeName, StoryPartStatus status, IEnumerable<KeyValuePair<string, TaskProgress>> taskProgresses)
		{
			if (parentStoryProgress == null) throw new ArgumentNullException("parentStoryProgress");
			if (string.IsNullOrEmpty(codeName)) throw new ArgumentException("Cannot be null or empty.", "codeName");
			if (taskProgresses == null) throw new ArgumentNullException("taskProgresses");

			this.ParentStoryProgress = parentStoryProgress;
			this.CodeName = codeName;
			this.Status = status;
			this._taskProgresses = taskProgresses.ToDictionary(x => x.Key, x => x.Value);

			if (status == StoryPartStatus.Unused && this._taskProgresses.Count != 0) {
				throw new ArgumentException("status == StoryPartStatus.Unused but taskProgresses is not empty.", "taskProgresses");
			}

			this.TaskProgresses = new ReadOnlyDictionary<string, TaskProgress>(this._taskProgresses);
		}

		/// <summary>
		/// Returns the <see cref="TaskProgress"/> for the task with the specified code-name
		/// if it is found in <see cref="TaskProgresses"/>, otherwise returns <see cref="TaskProgress.Unused"/>.
		/// </summary>
		public TaskProgress GetTaskProgress(string codeName)
		{
			TaskProgress existing;
			if (_taskProgresses.TryGetValue(codeName, out existing)) return existing;
			else return TaskProgress.Unused;
		}
	}
}

//*/