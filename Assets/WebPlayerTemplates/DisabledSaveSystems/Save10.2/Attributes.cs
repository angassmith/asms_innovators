﻿using System;
using System.Collections;
using UnityEngine;

namespace Save
{
	/// <summary>
	/// Indicates that a property of a SavedMonoBehaviour should be saved. Will not be saved or loaded if the get or set accessor (respectively) throws an exception.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class SavedPropertyAttribute : Attribute
	{
		public SavedPropertyAttribute() { }
	}

	/// <summary>
	/// Indicates that a field of a SavedMonoBehaviour should not be saved.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
	public class NonSavedFieldAttribute : Attribute
	{
		public NonSavedFieldAttribute() { }
	}

	/// <summary>Has no function; only used for clarity</summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public class DeliberatelyNonSerializableAttribute : Attribute
	{
		public readonly string[] Reasons;
		public DeliberatelyNonSerializableAttribute() { }
		public DeliberatelyNonSerializableAttribute(params string[] reasons)
		{ this.Reasons = reasons; }
	}
}