﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Save;

[DeliberatelyNonSerializable]
public class SaveTesting14 : SavedMonoBehaviour
{
	
	public double testDouble = 0;
	[NonSavedField]
	public Vector3[] testArr = new Vector3[10];
	[NonSavedField]
	public List<Vector3> testList = new List<Vector3>();
	[NonSavedField]
	public Dictionary<string, Vector3> testDict = new Dictionary<string, Vector3>();

#region SaveProperties
	[SavedProperty]
	public SerVector3[] STestArr {
		get { return testArr.ConvertItems(x => new SerVector3(x)); }
		set { testArr = value.ConvertItems(x => x.CastToNonSer()); }
	}
	[SavedProperty]
	public List<SerVector3> STestList {
		get { return testList.ConvertItems(x => new SerVector3(x)); }
		set { testList = value.ConvertItems(x => x.CastToNonSer()); }
	}
	[SavedProperty]
	public Dictionary<string, SerVector3> STestDict {
		get { return testDict.ConvertValues(x => new SerVector3(x)); }
		set { testDict = value.ConvertValues(x => x.CastToNonSer()); }
	}
#endregion
	
	public override void NormalStart()
	{
		testArr[3] = new Vector3(1, 2, 3);
		testList.Add(new Vector3(4, 5, 6));
		testDict.Add("someDictEntry", new Vector3(7, 8, 9));
	}
	public override void OnLoadStart()
	{
		
	}
	public override void CommonStart()
	{
		Debug.Log("#51: " + testArr.ToNiceString());
		Debug.Log("#52: " + testList.ToNiceString());
		Debug.Log("#53: " + testDict.ToNiceString());
		Debug.Log("#54: " + Application.persistentDataPath);
	}
	
	
	
	public override void NormalUpdate()
	{
		
	}
	public override void PreSaveUpdate()
	{
		
	}
	public override void CommonUpdate()
	{
		testDouble += 1;
		testArr[3].x += 5;
		testDict["someDictEntry"] = testDict["someDictEntry"].Apply(v => { v.x += 3; return v; });
		//	testDict["someDictEntry"] = testDict["someDictEntry"].Apply(delegate(ref Vector3 x) { x.x += 3; });
		//	Vector3 temp = testDict["someDictEntry"]; //These three lines are necessary because Vector3 is a struct
		//	temp.x += 3;
		//	testDict["someDictEntry"] = temp;
		
		if (testDouble % 50 == 0) Debug.Log(testDouble + ", " + testArr[3].x + ", " + testDict["someDictEntry"].x);

		if ((testDouble + 1) % 500 == 0) //499, 999, 1499, not 0
		{
			SaveHostMono.Main.SaveAfterNextFrame();
		}
	}
	
	
#region Save preparation and application methods
	public override void ImmediatelyBeforeSave()
	{
		
	}
	public override void ImmediatelyAfterLoad()
	{
		
	}
#endregion
	
}

//*/

