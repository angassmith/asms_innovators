﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerRect : SaveableEquiv<SerRect, Rect>
	{
		public override SaveEquivReg.Entry<SerRect, Rect> RegInfo {
			get {
				return null;
			}
		}

		public float x;
		public float y;
		public float width;
		public float height;

		public SerRect() : base(VoidClass.Void) { }
		public SerRect(Rect nonSer) : base(nonSer) { }

		//Rect Constructors
		public SerRect(SerRect source) : base(VoidClass.Void)
		{
			x      = source.x;
			y      = source.y;
			width  = source.width;
			height = source.height;
		}
		public SerRect(SerVector2 position, SerVector2 size) : base(VoidClass.Void)
		{
			x      = position.x;
			y      = position.y;
			width  = size.x;
			height = size.y;
		}

		public override void CopyFromNonNullNonSer(Rect nonSer)
		{
			this.x      = nonSer.x;
			this.y      = nonSer.y;
			this.width  = nonSer.width;
			this.height = nonSer.height;
		}

		public override void CopyOverNonNullNonSer(ref Rect nonSer)
		{
			nonSer.x      = this.x;
			nonSer.y      = this.y;
			nonSer.width  = this.width;
			nonSer.height = this.height;
		}

		public override Rect CreateBlankNonSer()
		{
			return new Rect();
		}

		//	public static explicit operator SerRect(Rect nonSer)
		//	{ return new SerRect(nonSer); }
		//	public static explicit operator Rect(SerRect ser)
		//	{ return new Rect(ser.x, ser.y, ser.width, ser.height); }
	}

}