﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerRay : SaveableEquiv<SerRay, Ray>
	{
		public override SaveEquivReg.Entry<SerRay, Ray> RegInfo {
			get {
				return null;
			}
		}

		public SerVector3 direction;
		public SerVector3 origin;

		public SerRay() : base(VoidClass.Void) { }
		public SerRay(Ray nonSer) : base(nonSer) { }

		//Ray Constructors
		public SerRay(SerVector3 origin, SerVector3 direction) : base(VoidClass.Void)
		{
			this.direction = direction;
			this.origin    = origin;
		}

		public override void CopyFromNonNullNonSer(Ray nonSer)
		{
			this.direction = (SerVector3)nonSer.direction;
			this.origin    = (SerVector3)nonSer.origin;
		}

		public override void CopyOverNonNullNonSer(ref Ray nonSer)
		{
			nonSer.direction = (Vector3)this.direction;
			nonSer.origin    = (Vector3)this.origin;
		}

		public override Ray CreateBlankNonSer()
		{
			return new Ray();
		}

		//	public static explicit operator SerRay(Ray nonSer)
		//	{ return new SerRay(nonSer); }
		//	public static explicit operator Ray(SerRay ser)
		//	{ return new Ray((Vector3)ser.origin, (Vector3)ser.direction); }
	}

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerRay2D : SaveableEquiv<SerRay2D, Ray2D>
	{
		public override SaveEquivReg.Entry<SerRay2D, Ray2D> RegInfo {
			get {
				return null;
			}
		}

		public SerVector2 direction;
		public SerVector2 origin;

		public SerRay2D() : base(VoidClass.Void) { }
		public SerRay2D(Ray2D nonSer) : base(nonSer) { }

		//Ray2D Constructors
		public SerRay2D(Vector2 origin, Vector2 direction) : base(VoidClass.Void)
		{
			this.direction = (SerVector2)direction;
			this.origin    = (SerVector2)origin;
		}
		public SerRay2D(SerVector2 origin, SerVector2 direction) : base(VoidClass.Void)
		{
			this.direction = direction;
			this.origin = origin;
		}

		public override void CopyFromNonNullNonSer(Ray2D nonSer)
		{
			this.direction = (SerVector2)nonSer.direction;
			this.origin    = (SerVector2)nonSer.origin;
		}

		public override void CopyOverNonNullNonSer(ref Ray2D nonSer)
		{
			nonSer.direction = (Vector2)this.direction;
			nonSer.origin    = (Vector2)this.origin;
		}

		public override Ray2D CreateBlankNonSer()
		{
			return new Ray2D();
		}

		//	public static explicit operator SerRay2D(Ray2D nonSer)
		//	{ return new SerRay2D(nonSer); }
		//	public static explicit operator Ray2D(SerRay2D ser)
		//	{ return new Ray2D((Vector2)ser.origin, (Vector2)ser.direction); }
	}

}