﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerQuaternion : SaveableEquiv<SerQuaternion, Quaternion>
	{
		public override SaveEquivReg.Entry<SerQuaternion, Quaternion> RegInfo {
			get {
				return null;
			}
		}

		public float w;
		public float x;
		public float y;
		public float z;

		public SerQuaternion() : base(VoidClass.Void) { }
		public SerQuaternion(Quaternion nonSer) : base(nonSer) { }

		//Quaternion Constructors
		public SerQuaternion(float x, float y, float z, float w) : base(VoidClass.Void)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override void CopyFromNonNullNonSer(Quaternion nonSer)
		{
			this.w = nonSer.w;
			this.x = nonSer.x;
			this.y = nonSer.y;
			this.z = nonSer.z;
		}

		public override void CopyOverNonNullNonSer(ref Quaternion nonSer)
		{
			nonSer.w = this.w;
			nonSer.x = this.x;
			nonSer.y = this.y;
			nonSer.z = this.z;
		}

		public override Quaternion CreateBlankNonSer()
		{
			return new Quaternion();
		}

		//	public static explicit operator SerQuaternion(Quaternion nonSer)
		//	{ return new SerQuaternion(nonSer); }
		//	public static explicit operator Quaternion(SerQuaternion ser)
		//	{ return new Quaternion(ser.x, ser.y, ser.z, ser.w); }
	}

}