﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerVector2 : SaveableEquiv<SerVector2, Vector2>
	{
		public override SaveEquivReg.Entry<SerVector2, Vector2> RegInfo {
			get {
				return null;
			}
		}

		public float x;
		public float y;

		public SerVector2() : base(VoidClass.Void) { }
		public SerVector2(Vector2 nonSer) : base(nonSer) { }

		//Vector2 Constructors
		public SerVector2(float x, float y) : base(VoidClass.Void)
		{
			this.x = x;
			this.y = y;
		}

		public override void CopyFromNonNullNonSer(Vector2 nonSerSource)
		{
			this.x = nonSerSource.x;
			this.y = nonSerSource.y;
		}
		public override void CopyOverNonNullNonSer(ref Vector2 nonSerDest)
		{
			nonSerDest.x = this.x;
			nonSerDest.y = this.y;
		}
		public override Vector2 CreateBlankNonSer()
		{
			return new Vector2();
		}

		//	public static explicit operator SerVector2(Vector2 nonSer)
		//	{ return new SerVector2(nonSer); }
		//	public static explicit operator Vector2(SerVector2 ser)
		//	{ return new Vector2(ser.x, ser.y); }
	}

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerVector3 : SaveableEquiv<SerVector3, Vector3>
	{
		public override SaveEquivReg.Entry<SerVector3, Vector3> RegInfo {
			get {
				return null;
			}
		}

		public float x;
		public float y;
		public float z;

		public SerVector3() : base(VoidClass.Void) { }
		public SerVector3(Vector3 nonSer) : base(nonSer) { }

		//Vector3 Constructors
		public SerVector3(float x, float y) : base(VoidClass.Void)
		{
			this.x = x;
			this.y = y;
		}
		public SerVector3(float x, float y, float z) : base(VoidClass.Void)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override void CopyFromNonNullNonSer(Vector3 nonSerSource)
		{
			this.x = nonSerSource.x;
			this.y = nonSerSource.y;
			this.z = nonSerSource.z;
		}
		public override void CopyOverNonNullNonSer(ref Vector3 nonSerDest)
		{
			nonSerDest.x = this.x;
			nonSerDest.y = this.y;
			nonSerDest.z = this.z;
		}
		public override Vector3 CreateBlankNonSer()
		{
			return new Vector3();
		}

		//	public static explicit operator SerVector3(Vector3 nonSer)
		//	{ return new SerVector3(nonSer); }
		//	public static explicit operator Vector3(SerVector3 ser)
		//	{ return new Vector3(ser.x, ser.y, ser.z); }
	}

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerVector4 : SaveableEquiv<SerVector4, Vector4>
	{
		public override SaveEquivReg.Entry<SerVector4, Vector4> RegInfo {
			get {
				return null;
			}
		}

		public float w;
		public float x;
		public float y;
		public float z;

		public SerVector4() : base(VoidClass.Void) { }
		public SerVector4(Vector4 nonSer) : base(nonSer) { }

		//Vector4 Constructors
		public SerVector4(float x, float y) : base(VoidClass.Void)
		{
			this.x = x;
			this.y = y;
		}
		public SerVector4(float x, float y, float z) : base(VoidClass.Void)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}
		public SerVector4(float x, float y, float z, float w) : base(VoidClass.Void)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override void CopyFromNonNullNonSer(Vector4 nonSerSource)
		{
			this.w = nonSerSource.w;
			this.x = nonSerSource.x;
			this.y = nonSerSource.y;
			this.z = nonSerSource.z;
		}
		
		public override void CopyOverNonNullNonSer(ref Vector4 nonSerDest)
		{
			nonSerDest.w = this.w;
			nonSerDest.x = this.x;
			nonSerDest.y = this.y;
			nonSerDest.z = this.z;
		}

		public override Vector4 CreateBlankNonSer()
		{
			return new Vector4();
		}

		//	public static explicit operator SerVector4(Vector4 nonSer)
		//	{ return new SerVector4(nonSer); }
		//	public static explicit operator Vector4(SerVector4 ser)
		//	{ return new Vector4(ser.x, ser.y, ser.z, ser.w); }
	}

}