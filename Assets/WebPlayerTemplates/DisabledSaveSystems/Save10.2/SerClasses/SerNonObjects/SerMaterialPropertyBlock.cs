﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace Save
{
	/// <summary>
	/// Status:As complete as possible (isEmpty (and even that not properly) and nothing else - will not work)
	/// </summary>
	[System.Serializable]
	public sealed class SerMaterialPropertyBlock : SaveableEquiv<SerMaterialPropertyBlock, MaterialPropertyBlock> //MaterialPropertyBlock doesn't inherit from anything
	{
		public override SaveEquivReg.Entry<SerMaterialPropertyBlock, MaterialPropertyBlock> RegInfo {
			get {
				throw new NotImplementedException();
			}
		}

		public bool isEmpty;

		public SerMaterialPropertyBlock() : base(VoidClass.Void) { }
		public SerMaterialPropertyBlock(MaterialPropertyBlock nonSerSource) : base(nonSerSource) { }

		public override void CopyFromNonNullNonSer(MaterialPropertyBlock nonSerSource)
		{
			this.isEmpty = nonSerSource.isEmpty;
		}

		public override void CopyOverNonNullNonSer(ref MaterialPropertyBlock nonSerDest)
		{
			if (this.isEmpty)
			{
				nonSerDest.Clear();
			}
		}

		public override MaterialPropertyBlock CreateBlankNonSer()
		{
			return new MaterialPropertyBlock();
		}

	}
}
