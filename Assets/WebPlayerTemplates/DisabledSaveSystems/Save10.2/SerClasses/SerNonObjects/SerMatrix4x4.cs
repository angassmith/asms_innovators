﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerMatrix4x4 : SaveableEquiv<SerMatrix4x4, Matrix4x4>
	{
		public override SaveEquivReg.Entry<SerMatrix4x4, Matrix4x4> RegInfo {
			get {
				return null;
			}
		}

		public float m00;
		public float m01;
		public float m02;
		public float m03;
		public float m10;
		public float m11;
		public float m12;
		public float m13;
		public float m20;
		public float m21;
		public float m22;
		public float m23;
		public float m30;
		public float m31;
		public float m32;
		public float m33;

		public SerMatrix4x4() : base(VoidClass.Void) { }
		public SerMatrix4x4(Matrix4x4 nonSer) : base(nonSer) { }

		//Matrix4x4 Constructors
		//[none]

		public override void CopyFromNonNullNonSer(Matrix4x4 nonSer)
		{
			this.m00 = nonSer.m00;
			this.m01 = nonSer.m01;
			this.m02 = nonSer.m02;
			this.m03 = nonSer.m03;
			this.m10 = nonSer.m10;
			this.m11 = nonSer.m11;
			this.m12 = nonSer.m12;
			this.m13 = nonSer.m13;
			this.m20 = nonSer.m20;
			this.m21 = nonSer.m21;
			this.m22 = nonSer.m22;
			this.m23 = nonSer.m23;
			this.m30 = nonSer.m30;
			this.m31 = nonSer.m31;
			this.m32 = nonSer.m32;
			this.m33 = nonSer.m33;
		}

		public override void CopyOverNonNullNonSer(ref Matrix4x4 nonSer)
		{
			nonSer.m00 = this.m00;
			nonSer.m01 = this.m01;
			nonSer.m02 = this.m02;
			nonSer.m03 = this.m03;
			nonSer.m10 = this.m10;
			nonSer.m11 = this.m11;
			nonSer.m12 = this.m12;
			nonSer.m13 = this.m13;
			nonSer.m20 = this.m20;
			nonSer.m21 = this.m21;
			nonSer.m22 = this.m22;
			nonSer.m23 = this.m23;
			nonSer.m30 = this.m30;
			nonSer.m31 = this.m31;
			nonSer.m32 = this.m32;
			nonSer.m33 = this.m33;
		}

		public override Matrix4x4 CreateBlankNonSer()
		{
			return new Matrix4x4();
		}

		//	public static explicit operator SerMatrix4x4(Matrix4x4 nonSer)
		//	{ return new SerMatrix4x4(nonSer); }
		//	public static explicit operator Matrix4x4(SerMatrix4x4 ser)
		//	{
		//		Matrix4x4 nonSer = new Matrix4x4();
		//		nonSer.m00 = ser.m00;
		//		nonSer.m01 = ser.m01;
		//		nonSer.m02 = ser.m02;
		//		nonSer.m03 = ser.m03;
		//		nonSer.m10 = ser.m10;
		//		nonSer.m11 = ser.m11;
		//		nonSer.m12 = ser.m12;
		//		nonSer.m13 = ser.m13;
		//		nonSer.m20 = ser.m20;
		//		nonSer.m21 = ser.m21;
		//		nonSer.m22 = ser.m22;
		//		nonSer.m23 = ser.m23;
		//		nonSer.m30 = ser.m30;
		//		nonSer.m31 = ser.m31;
		//		nonSer.m32 = ser.m32;
		//		nonSer.m33 = ser.m33;
		//		return nonSer;
		//	}
	}

}