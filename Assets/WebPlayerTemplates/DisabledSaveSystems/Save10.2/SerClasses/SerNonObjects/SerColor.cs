﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerColor : SaveableEquiv<SerColor, Color>
	{
		public override SaveEquivReg.Entry<SerColor, Color> RegInfo {
			get {
				return null;
			}
		}

		public float r;
		public float g;
		public float b;
		public float a;

		public SerColor() : base(VoidClass.Void) { }
		public SerColor(Color nonSer) : base(nonSer)
		{
			r = nonSer.r;
			g = nonSer.g;
			b = nonSer.b;
			a = nonSer.a;
		}

		//Color Constructors
		public SerColor(float r, float g, float b) : base(VoidClass.Void)
		{
			this.r = r;
			this.g = g;
			this.b = b;
		}
		public SerColor(float r, float g, float b, float a) : base(VoidClass.Void)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}

		public override void CopyFromNonNullNonSer(Color nonSer)
		{
			this.r = nonSer.r;
			this.g = nonSer.g;
			this.b = nonSer.b;
			this.a = nonSer.a;
		}

		public override void CopyOverNonNullNonSer(ref Color nonSer)
		{
			nonSer.r = this.r;
			nonSer.g = this.g;
			nonSer.b = this.b;
			nonSer.a = this.a;
		}

		public override Color CreateBlankNonSer()
		{
			return new Color();
		}

		//	public static explicit operator SerColor(Color nonSer)
		//	{ return new SerColor(nonSer); }
		//	public static explicit operator Color(SerColor ser)
		//	{ return new Color(ser.r, ser.g, ser.b, ser.a); }
	}

	/// <summary>Status:Complete</summary>
	[System.Serializable]
	public class SerColor32 : SaveableEquiv<SerColor32, Color32>
	{
		public override SaveEquivReg.Entry<SerColor32, Color32> RegInfo {
			get {
				return null;
			}
		}

		public byte r;
		public byte g;
		public byte b;
		public byte a;

		public SerColor32() : base(VoidClass.Void) { }
		public SerColor32(Color32 nonSer) : base(nonSer) { }

		//Color32 Constructors
		public SerColor32(byte r, byte g, byte b, byte a) : base(VoidClass.Void)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}

		public override void CopyFromNonNullNonSer(Color32 nonSer)
		{
			this.r = nonSer.r;
			this.g = nonSer.g;
			this.b = nonSer.b;
			this.a = nonSer.a;
		}

		public override void CopyOverNonNullNonSer(ref Color32 nonSer)
		{
			nonSer.r = this.r;
			nonSer.g = this.g;
			nonSer.b = this.b;
			nonSer.a = this.a;
		}

		public override Color32 CreateBlankNonSer()
		{
			return new Color32();
		}

		//	public static explicit operator SerColor32(Color32 nonSer)
		//	{ return new SerColor32(nonSer); }
		//	public static explicit operator Color32(SerColor32 ser)
		//	{ return new Color32(ser.r, ser.g, ser.b, ser.a); }
	}

}