﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{
	[System.Serializable]
	public class SerGameObject : SaveableEquiv<SerGameObject, GameObject> //Complete
	{
		public override SaveEquivReg.Entry<SerGameObject, GameObject> RegInfo {
			get {
				return null;
			}
		}

		public bool activeSelf;
		//public bool isStatic;
		public HideFlags hideFlags;
		public int layer;
		public string name;
		public string tag;
		public SerTransform transform;

		public SerGameObject() : base(VoidClass.Void) { }
		public SerGameObject(GameObject nonSer) : base(nonSer) { }

		public override void CopyFromNonNullNonSer(GameObject nonSer)
		{
			this.activeSelf =               nonSer.activeSelf;
			this.hideFlags  =               nonSer.hideFlags;
			this.layer      =               nonSer.layer;
			this.name       =               nonSer.name;
			this.tag        =               nonSer.tag;
			this.transform  = (SerTransform)nonSer.transform;
		}

		public override void CopyOverNonNullNonSer(ref GameObject existing)
		{
			existing.hideFlags = this.hideFlags;
			existing.layer     = this.layer;
			existing.name      = this.name;
			existing.tag       = this.tag == null ? "" : this.tag;
			existing.SetActive(this.activeSelf); //activeSelf is read only, but SetActive() is used to write to it
			Transform existing_transform = existing.transform;
			this.transform.CopyOverNonSer(ref existing_transform); //transform is read only
		}

		public override GameObject CreateBlankNonSer()
		{
			return new GameObject();
		}

		//GameObject Constructors
		public SerGameObject(string name) : base(VoidClass.Void) {
			this.name = name;
		}

		//	public static explicit operator SerGameObject(GameObject nonSer)
		//	{ return new SerGameObject(nonSer); }
		//	public static explicit operator GameObject(SerGameObject ser) {
		//		GameObject nonSer = new GameObject();
		//		ser.CopyOverNonSer(nonSer);
		//		return nonSer;
		//	}
		
	}
}

