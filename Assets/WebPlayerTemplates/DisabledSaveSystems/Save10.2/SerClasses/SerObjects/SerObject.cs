﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{
	/// <summary>
	/// Status:See base
	/// </summary>
	[System.Serializable]
	public sealed class SerObject : SerObject<SerObject, UnityEngine.Object>
	{
		public override SaveEquivReg.Entry<SerObject, UnityEngine.Object> RegInfo {
			get {
				return null;
			}
		}

		public SerObject() : base(VoidClass.Void) { }
		public SerObject(UnityEngine.Object nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerObject(UnityEngine.Object nonSerSource) { }
		protected override void CopyOverNonNullNonSerObject(UnityEngine.Object nonSerDest) { }

		public override UnityEngine.Object CreateBlankNonSer()
		{
			return new UnityEngine.Object();
		}

	}

}