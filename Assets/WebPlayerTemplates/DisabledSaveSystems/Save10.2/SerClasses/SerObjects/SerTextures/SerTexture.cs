﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{
	///<summary>Status:?</summary>
	[System.Serializable]
	public sealed class SerTexture : SerTexture<SerTexture, Texture>, ISerTexture
	{
		public override SaveEquivReg.Entry<SerTexture, Texture> RegInfo {
			get {
				return null;
			}
		}

		public SerTexture() : base(VoidClass.Void) { }
		public SerTexture(Texture nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerTexture(Texture nonSerSource) { }
		protected override void CopyOverNonNullNonSerTexture(Texture nonSerDest) { }

		public override Texture CreateBlankNonSer()
		{
			return new Texture();
		}

		//	public static explicit operator SerTexture(Texture nonSer)
		//	{ if (nonSer == null) return null; return new SerTexture(nonSer); }
		//	public static explicit operator Texture(SerTexture ser)
		//	{
		//		if (ser == null) { return null; }
		//		Texture nonSer = new Texture();
		//		nonSer.anisoLevel = ser.anisoLevel;
		//		nonSer.filterMode = ser.filterMode;
		//		nonSer.mipMapBias = ser.mipMapBias;
		//		nonSer.wrapMode = ser.wrapMode;
		//		return nonSer;
		//	}
	}
}