﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{
	public interface ISerTexture : ISerObject
	{ }

	///<summary>Status:?</summary>
	[System.Serializable]
	public abstract class SerTexture<TSer, TNonSer> : SerObject<TSer, TNonSer>, ISerTexture
		where TSer : SerTexture<TSer, TNonSer>, new()
		where TNonSer : Texture
	{
		public int anisoLevel;
		public FilterMode filterMode; //Enum
		public float mipMapBias;
		public TextureWrapMode wrapMode; //Enum

		//The actual *texture* is difficult, if not impossible, to get/set/store
		//Texture.GetNativeTexturePtr() might help, but the object the IntPtr points to
		//seems to differ between graphics platforms (and on some platforms return null)

		public SerTexture(VoidClass none) : base(VoidClass.Void) { }
		public SerTexture(TNonSer nonSerSource) : base(nonSerSource) { }

		protected sealed override void CopyFromNonNullNonSerObject(TNonSer nonSerSource)
		{
			this.anisoLevel = nonSerSource.anisoLevel;
			this.filterMode = nonSerSource.filterMode;
			this.mipMapBias = nonSerSource.mipMapBias;
			this.wrapMode   = nonSerSource.wrapMode;
			CopyFromNonNullNonSerTexture(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerTexture(TNonSer nonSerSource);

		protected sealed override void CopyOverNonNullNonSerObject(TNonSer nonSerDest)
		{
			nonSerDest.anisoLevel = this.anisoLevel;
			nonSerDest.filterMode = this.filterMode;
			nonSerDest.mipMapBias = this.mipMapBias;
			nonSerDest.wrapMode   = this.wrapMode;
			CopyOverNonNullNonSerTexture(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerTexture(TNonSer nonSerDest);

		//	public static explicit operator SerTexture(Texture nonSer)
		//	{ if (nonSer == null) return null; return new SerTexture(nonSer); }
		//	public static explicit operator Texture(SerTexture ser)
		//	{
		//		if (ser == null) { return null; }
		//		Texture nonSer = new Texture();
		//		nonSer.anisoLevel = ser.anisoLevel;
		//		nonSer.filterMode = ser.filterMode;
		//		nonSer.mipMapBias = ser.mipMapBias;
		//		nonSer.wrapMode = ser.wrapMode;
		//		return nonSer;
		//	}
	}
}