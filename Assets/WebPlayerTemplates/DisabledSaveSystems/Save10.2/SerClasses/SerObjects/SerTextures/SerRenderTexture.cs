﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{
	///<summary>As complete as possible (might not work)</summary>
	[System.Serializable]
	public sealed class SerRenderTexture : SerTexture<SerRenderTexture, RenderTexture>
	{
		public override SaveEquivReg.Entry<SerRenderTexture, RenderTexture> RegInfo {
			get {
				return null;
			}
		}

		public int antiAliasing;
		public int depth;
		public bool enableRandomWrite;
		public RenderTextureFormat format; //Enum
		public bool generateMips;
		public int height;
		public bool isCreated;
		public bool isCubemap;
		public bool isPowerOfTwo;
		public bool isVolume;
		public RenderTextureReadWrite readWrite;
		//public bool sRGB;
		public bool useMipMap;
		public int volumeDepth;
		public int width;

		public SerRenderTexture() : base(VoidClass.Void) { }
		public SerRenderTexture(RenderTexture nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerTexture(RenderTexture nonSerSource)
		{
			this.anisoLevel        = nonSerSource.anisoLevel;
			this.antiAliasing      = nonSerSource.antiAliasing;
			this.depth             = nonSerSource.depth;
			this.enableRandomWrite = nonSerSource.enableRandomWrite;
			this.filterMode        = nonSerSource.filterMode;
			this.format            = nonSerSource.format;
			this.generateMips      = nonSerSource.autoGenerateMips;
			this.height            = nonSerSource.height;
			this.isCreated         = nonSerSource.IsCreated();
			//updated in future version  this.isCubemap         = nonSerSource.isCubemap;
			this.isPowerOfTwo      = nonSerSource.isPowerOfTwo;
			//updated in future version  this.isVolume          = nonSerSource.isVolume;
			this.mipMapBias        = nonSerSource.mipMapBias;
			this.useMipMap         = nonSerSource.useMipMap;
			this.volumeDepth       = nonSerSource.volumeDepth;
			this.width             = nonSerSource.width;
			this.wrapMode          = nonSerSource.wrapMode;
			if (nonSerSource.sRGB)
			{
				this.readWrite = RenderTextureReadWrite.sRGB;
			}
		}

		protected override void CopyOverNonNullNonSerTexture(RenderTexture nonSerDest)
		{
			nonSerDest.anisoLevel        = this.anisoLevel;
			nonSerDest.antiAliasing      = this.antiAliasing;
			nonSerDest.depth             = this.depth;
			nonSerDest.enableRandomWrite = this.enableRandomWrite;
			nonSerDest.filterMode        = this.filterMode;
			nonSerDest.format            = this.format;
			nonSerDest.autoGenerateMips      = this.generateMips;
			nonSerDest.height            = this.height;
			//updated in future version  nonSerDest.isCubemap         = this.isCubemap;
			nonSerDest.isPowerOfTwo      = this.isPowerOfTwo;
			//updated in future version  nonSerDest.isVolume          = this.isVolume;
			nonSerDest.mipMapBias        = this.mipMapBias;
			nonSerDest.useMipMap         = this.useMipMap;
			nonSerDest.volumeDepth       = this.volumeDepth;
			nonSerDest.width             = this.width;
			nonSerDest.wrapMode          = this.wrapMode;
			if (this.isCreated)
			{
				nonSerDest.Create();
			}
		}

		public override RenderTexture CreateBlankNonSer()
		{
			return new RenderTexture(this.width, this.height, this.depth, this.format, this.readWrite);
			// ^ readWrite can only be set in the constructor (also there is no parameterless constructor anyway)
		}

		//RenderTexture Constructors
		public SerRenderTexture(int width, int height, int depth) : base(VoidClass.Void)
		{
			this.width  = width;
			this.height = height;
			this.depth  = depth;
		}
		public SerRenderTexture(int width, int height, int depth, RenderTextureFormat format) : base(VoidClass.Void)
		{
			this.width  = width;
			this.height = height;
			this.depth  = depth;
			this.format = format;
		}
		public SerRenderTexture(int width, int height, int depth, RenderTextureFormat format, RenderTextureReadWrite readWrite) : base(VoidClass.Void)
		{
			this.width     = width;
			this.height    = height;
			this.depth     = depth;
			this.format    = format;
			this.readWrite = readWrite;
		}

		//Do not remove this. It sets the readWrite property, which cannot be set outside the constructor
		public static explicit operator RenderTexture(SerRenderTexture serSource)
		{
			if (serSource == null) { return null; }
			RenderTexture nonSer = new RenderTexture(serSource.width, serSource.height, serSource.depth, serSource.format, serSource.readWrite);
			serSource.CopyOverNonNullNonSer(ref nonSer);
			return nonSer;
		}
		//	public static explicit operator SerRenderTexture(RenderTexture nonSer)
		//	{ if (nonSer == null) return null; return new SerRenderTexture(nonSer); }
	}
}