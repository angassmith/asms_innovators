﻿using UnityEngine;
using System.Collections;

namespace Save
{
	public interface ISerObject : ISaveableEquiv
	{ }

	/// <summary>
	/// Status:Complete
	/// </summary>
	[System.Serializable]
	public abstract class SerObject<TSer, TNonSer>
		: SaveableEquiv<TSer, TNonSer>, ISerObject
		where TSer : SerObject<TSer, TNonSer>, new()
		where TNonSer : Object
	{
		public string name;
		public HideFlags hideFlags;

		protected SerObject(VoidClass none) : base(VoidClass.Void) { }
		protected SerObject(TNonSer nonSerSource) : base(nonSerSource) { }

		public sealed override void CopyFromNonNullNonSer(TNonSer nonSerSource) {
			this.name      = nonSerSource.name;
			this.hideFlags = nonSerSource.hideFlags;
			CopyFromNonNullNonSerObject(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerObject(TNonSer nonSerSource);


		public sealed override void CopyOverNonNullNonSer(ref TNonSer nonSerDest) {
			nonSerDest.name      = this.name;
			nonSerDest.hideFlags = this.hideFlags;
			CopyOverNonNullNonSerObject(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerObject(TNonSer nonSerDest);
		
	}
}
