﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{
	[System.Serializable]
	public sealed class SerScriptableObject : SerScriptableObject<SerScriptableObject, ScriptableObject>
	{
		public override SaveEquivReg.Entry<SerScriptableObject, ScriptableObject> RegInfo {
			get {
				return null;
			}
		}

		public SerScriptableObject() : base(VoidClass.Void) { }
		public SerScriptableObject(ScriptableObject nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerScriptableObject(ScriptableObject nonSerSource) { }
		protected override void CopyOverNonNullNonSerScriptableObject(ScriptableObject nonSerDest) { }

		public override ScriptableObject CreateBlankNonSer()
		{
			return new ScriptableObject();
		}
	}
}
