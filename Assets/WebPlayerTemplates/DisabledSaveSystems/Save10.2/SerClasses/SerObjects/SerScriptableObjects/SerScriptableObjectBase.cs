﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{
	public interface ISerScriptableObject : ISerObject
	{ }

	[System.Serializable]
	public abstract class SerScriptableObject<TSer, TNonSer> : SerObject<TSer, TNonSer>
		where TSer : SerScriptableObject<TSer, TNonSer>, new()
		where TNonSer : ScriptableObject
	{
		public SerScriptableObject(VoidClass none) : base(VoidClass.Void) { }
		public SerScriptableObject(TNonSer nonSerSource) : base(nonSerSource) { }
		
		//May be used for future versions of Unity
		protected sealed override void CopyFromNonNullNonSerObject(TNonSer nonSerSource)
		{
			CopyFromNonNullNonSerScriptableObject(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerScriptableObject(TNonSer nonSerSource);

		//May be used for future versions of Unity
		protected sealed override void CopyOverNonNullNonSerObject(TNonSer nonSerDest)
		{
			CopyOverNonNullNonSerScriptableObject(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerScriptableObject(TNonSer nonSerDest);
	}
}
