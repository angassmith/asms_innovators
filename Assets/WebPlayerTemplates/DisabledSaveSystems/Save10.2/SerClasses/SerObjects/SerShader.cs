﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{

	///<summary>Status:As complete as possible (might not work)</summary>
	[System.Serializable]
	public sealed class SerShader : SerObject<SerShader, Shader>
	{
		public override SaveEquivReg.Entry<SerShader, Shader> RegInfo {
			get {
				return null;
			}
		}

		public int maximumLOD;

		public SerShader() : base(VoidClass.Void) { }
		public SerShader(Shader nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerObject(Shader nonSerSource)
		{
			this.maximumLOD = nonSerSource.maximumLOD;
		}

		protected override void CopyOverNonNullNonSerObject(Shader nonSerDest)
		{
			nonSerDest.maximumLOD = this.maximumLOD;
		}

		public override Shader CreateBlankNonSer()
		{
			return new Shader();
		}

		//	public static explicit operator SerShader(Shader nonSer)
		//	{ return new SerShader(nonSer); }
		//	public static explicit operator Shader(SerShader ser)
		//	{
		//		Shader nonSer = new Shader();
		//		nonSer.maximumLOD = ser.maximumLOD;
		//		return nonSer;
		//	}
	}
}