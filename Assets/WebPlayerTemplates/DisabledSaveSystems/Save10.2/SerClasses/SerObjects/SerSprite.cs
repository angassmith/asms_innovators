﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{
	///<summary>Status:As complete as possible (might not work)</summary>
	[System.Serializable]
	public sealed class SerSprite : SerObject<SerSprite, Sprite>
	{
		public override SaveEquivReg.Entry<SerSprite, Sprite> RegInfo {
			get {
				return null;
			}
		}

		public SerVector2[] vertices;
		public ushort[] triangles;

		public SerSprite() : base(VoidClass.Void) { }
		public SerSprite(Sprite nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerObject(Sprite nonSerSource)
		{
			this.vertices  = nonSerSource.vertices.ConvertItems(x => (SerVector2)x);
			this.triangles = nonSerSource.triangles;
		}

		protected override void CopyOverNonNullNonSerObject(Sprite nonSerDest)
		{
			nonSerDest.OverrideGeometry(
				this.vertices.ConvertItems(x => (Vector2)x),
				this.triangles
			);
		}

		public override Sprite CreateBlankNonSer()
		{
			return new Sprite();
		}

		//	public static explicit operator SerSprite(Sprite nonSer)
		//	{ return new SerSprite(nonSer); }
		//	public static explicit operator Sprite(SerSprite ser)
		//	{
		//		Sprite nonSer = new Sprite();
		//		Vector2[] vertices = Array.ConvertAll(ser.vertices, item => (Vector2)item);
		//		nonSer.OverrideGeometry(vertices, ser.triangles);
		//		return nonSer;
		//	}
	}
}