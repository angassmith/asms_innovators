﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save
{
	///<summary>Status:?</summary>
	[System.Serializable]
	public sealed class SerMaterial : SerObject<SerMaterial, Material>
	{
		public override SaveEquivReg.Entry<SerMaterial, Material> RegInfo {
			get {
				return null;
			}
		}

		public SerColor color;
		public SerTexture mainTexture;
		public SerVector2 mainTextureOffset;
		public SerVector2 mainTextureScale;
		public int renderQueue;
		public SerShader shader;
		public string[] shaderKeywords;

		public SerMaterial() : base(VoidClass.Void) { }
		public SerMaterial(Material nonSer) : base(nonSer) { }

		//Material Constructors
		public SerMaterial(SerMaterial source) : base(VoidClass.Void)
		{
			this.color = source.color;
			this.mainTexture = source.mainTexture;
			this.mainTextureOffset = source.mainTextureOffset;
			this.mainTextureScale = source.mainTextureScale;
			this.renderQueue = source.renderQueue;
			this.shader = source.shader;
			this.shaderKeywords = source.shaderKeywords;
		}
		public SerMaterial(SerShader shader) : base(VoidClass.Void)
		{
			this.shader = shader;
		}
		//SerMaterial(string contents) - Material(string contents) is deprecated/obsolete,
		//and this is only made known in the docs for some reason

		protected override void CopyFromNonNullNonSerObject(Material nonSer)
		{
			this.color             = (SerColor)  nonSer.color;
			this.mainTexture       = (SerTexture)nonSer.mainTexture;
			this.mainTextureOffset = (SerVector2)nonSer.mainTextureOffset;
			this.mainTextureScale  = (SerVector2)nonSer.mainTextureScale;
			this.renderQueue       =             nonSer.renderQueue;
			this.shader            = (SerShader) nonSer.shader;
			this.shaderKeywords    =             nonSer.shaderKeywords;
		}

		protected override void CopyOverNonNullNonSerObject(Material nonSer)
		{
			nonSer.color             = (Color)  this.color;
			nonSer.mainTexture       = (Texture)this.mainTexture;
			nonSer.mainTextureOffset = (Vector2)this.mainTextureOffset;
			nonSer.mainTextureScale  = (Vector2)this.mainTextureScale;
			nonSer.renderQueue       =          this.renderQueue;
			nonSer.shader            = (Shader) this.shader;
			nonSer.shaderKeywords    =          this.shaderKeywords;
		}

		public override Material CreateBlankNonSer()
		{
			return new Material(new Shader());
		}

		//	public static explicit operator SerMaterial(Material nonSer)
		//	{ return new SerMaterial(nonSer); }
		//	public static explicit operator Material(SerMaterial ser)
		//	{
		//		Material nonSer = new Material(new Shader());
		//		nonSer.color = (Color)ser.color;
		//		nonSer.mainTexture = (Texture)ser.mainTexture;
		//		nonSer.mainTextureOffset = (Vector2)ser.mainTextureOffset;
		//		nonSer.mainTextureScale = (Vector2)ser.mainTextureScale;
		//		nonSer.renderQueue = ser.renderQueue;
		//		nonSer.shader = (Shader)ser.shader;
		//		nonSer.shaderKeywords = ser.shaderKeywords;
		//		return nonSer;
		//	}
	}
}
