﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save {

	///<summary>Status:?</summary>
	[System.Serializable]
	public sealed class SerSpriteRenderer : SerRenderer<SerSpriteRenderer, SpriteRenderer>
	{
		public override SaveEquivReg.Entry<SerSpriteRenderer, SpriteRenderer> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerSpriteRenderer, SpriteRenderer>(
					"SerSpriteRenderer",
					"SpriteRenderer",
					new SaveEquivReg.ClassStatus<SerSpriteRenderer, SpriteRenderer>(
						x => (SerSpriteRenderer)x,
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.Unknown,
						SaveEquivReg.SavedDataCompletedness.UnknownOrUntested,
						SaveEquivReg.LoadedDataSucess.UnknownOrUntested,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.SerComponent | SaveEquivReg.Uses.NormalDataStorage | SaveEquivReg.Uses.UseableForGeneralSaveNode,
						SaveEquivReg.Additional.None,
						null
					)
				);
			}
		}

		public SerColor color;
		public bool flipX;
		public bool flipY;
		public SerSprite sprite;

		public SerSpriteRenderer() : base(VoidClass.Void) { }
		public SerSpriteRenderer(SpriteRenderer nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerRenderer(SpriteRenderer nonSerSource)
		{
			this.color                       = (SerColor) nonSerSource.color;
			this.flipX                       =            nonSerSource.flipX;
			this.flipY                       =            nonSerSource.flipY;
			this.sprite                      = (SerSprite)nonSerSource.sprite;
		}

		protected override void CopyOverNonNullNonSerRenderer(SpriteRenderer nonSerDest)
		{
			nonSerDest.color                       = (Color) this.color;
			nonSerDest.flipX                       =         this.flipX;
			nonSerDest.flipY                       =         this.flipY;
			nonSerDest.sprite                      = (Sprite)this.sprite;
		}

		public override SpriteRenderer CreateBlankNonSer()
		{
			return new SpriteRenderer();
		}

		//	public static explicit operator SerSpriteRenderer(SpriteRenderer nonSer)
		//	{ return new SerSpriteRenderer(nonSer); }
		//	public static explicit operator SpriteRenderer(SerSpriteRenderer ser)
		//	{
		//		SpriteRenderer nonSer = new SpriteRenderer();
		//		
		//		ser.CopyOverExisting(ref nonSer);
		//		return nonSer;
		//	}
	}

}