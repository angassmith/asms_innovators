﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{

	///<summary>Status:As complete as possible (might not work)</summary>
	[System.Serializable]
	public sealed class SerRenderer : SerRenderer<SerRenderer, Renderer>
	{
		public override SaveEquivReg.Entry<SerRenderer, Renderer> RegInfo {
			get {
				return null;
			}
		}

		public SerRenderer() : base(VoidClass.Void) { }
		public SerRenderer(Renderer nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerRenderer(Renderer nonSerSource) { }
		protected override void CopyOverNonNullNonSerRenderer(Renderer nonSerDest) { }

		public override Renderer CreateBlankNonSer()
		{
			return new Renderer();
		}
	}


}
