﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{
	public interface ISerRenderer : ISerComponent
	{ }

	///<summary>Status:As complete as possible (might not work)</summary>
	[System.Serializable]
	public abstract class SerRenderer<TSer, TNonSer>
		: SerComponent<TSer, TNonSer>, ISerRenderer
		where TSer : SerRenderer<TSer, TNonSer>, new()
		where TNonSer : Renderer
	{

		public bool enabled;
		public SerMaterialPropertyBlock propertyBlock;
		public int lightmapIndex;
		public SerVector4 lightmapScaleOffset;
		//public SerMaterial material;
		public SerMaterial[] materials;
		public SerTransform probeAnchor;
		public int realtimeLightmapIndex;
		public SerVector4 realtimeLightmapScaleOffset;
		public bool receiveShadows;
		public UnityEngine.Rendering.ReflectionProbeUsage reflectionProbeUsage; //Enum
		public UnityEngine.Rendering.ShadowCastingMode shadowCastingMode;
		//public SerMaterial sharedMaterial;
		public SerMaterial[] sharedMaterials;
		public int sortingLayerID;
		public string sortingLayerName;
		public int sortingOrder;
		public bool useLightProbes;

		protected SerRenderer(VoidClass none) : base(VoidClass.Void) { }
		protected SerRenderer(TNonSer nonSerSource) : base(nonSerSource) { }

		protected sealed override void CopyFromNonNullNonSerComponent(TNonSer nonSerSource)
		{
			this.enabled                     =               nonSerSource.enabled;
			this.lightmapIndex               =               nonSerSource.lightmapIndex;
			this.lightmapScaleOffset         = (SerVector4)  nonSerSource.lightmapScaleOffset;
			this.materials                   =               nonSerSource.materials.ConvertItems(x => (SerMaterial)x);
			this.probeAnchor                 = (SerTransform)nonSerSource.probeAnchor;
			this.realtimeLightmapIndex       =               nonSerSource.realtimeLightmapIndex;
			this.realtimeLightmapScaleOffset = (SerVector4)  nonSerSource.realtimeLightmapScaleOffset;
			this.receiveShadows              =               nonSerSource.receiveShadows;
			this.reflectionProbeUsage        =               nonSerSource.reflectionProbeUsage;
			this.shadowCastingMode           =               nonSerSource.shadowCastingMode;
			this.sharedMaterials             =               nonSerSource.sharedMaterials.ConvertItems(x => (SerMaterial)x);
			this.sortingLayerID              =               nonSerSource.sortingLayerID;
			this.sortingLayerName            =               nonSerSource.sortingLayerName;
			this.sortingOrder                =               nonSerSource.sortingOrder;
			//updated in future version  this.useLightProbes              =               nonSerSource.useLightProbes;

			MaterialPropertyBlock propBlock = new MaterialPropertyBlock();
			nonSerSource.GetPropertyBlock(propBlock);
			this.propertyBlock = (SerMaterialPropertyBlock)propBlock;

			CopyFromNonNullNonSerRenderer(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerRenderer(TNonSer nonSerSource);

		protected sealed override void CopyOverNonNullNonSerComponent(TNonSer nonSerDest)
		{
			nonSerDest.enabled                     =            this.enabled;
			nonSerDest.lightmapIndex               =            this.lightmapIndex;
			nonSerDest.lightmapScaleOffset         = (Vector4)  this.lightmapScaleOffset;
			nonSerDest.materials                   =            this.materials.ConvertItems(x => (Material)x);
			nonSerDest.probeAnchor                 = (Transform)this.probeAnchor;
			nonSerDest.realtimeLightmapIndex       =            this.realtimeLightmapIndex;
			nonSerDest.realtimeLightmapScaleOffset = (Vector4)  this.realtimeLightmapScaleOffset;
			nonSerDest.receiveShadows              =            this.receiveShadows;
			nonSerDest.reflectionProbeUsage        =            this.reflectionProbeUsage;
			nonSerDest.shadowCastingMode           =            this.shadowCastingMode;
			nonSerDest.sharedMaterials             =            this.sharedMaterials.ConvertItems(x => (Material)x);
			nonSerDest.sortingLayerID              =            this.sortingLayerID;
			nonSerDest.sortingLayerName            =            this.sortingLayerName;
			nonSerDest.sortingOrder                =            this.sortingOrder;
			//updated in future version  nonSerDest.useLightProbes              =            this.useLightProbes;

			nonSerDest.SetPropertyBlock((MaterialPropertyBlock)this.propertyBlock);

			CopyOverNonNullNonSerRenderer(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerRenderer(TNonSer nonSerDest);
	}

}