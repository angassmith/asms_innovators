﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{

	/// <summary>
	/// Status:See base
	/// </summary>
	[System.Serializable]
	public class SerBehaviour : SerBehaviour<SerBehaviour, Behaviour>
	{
		public override SaveEquivReg.Entry<SerBehaviour, Behaviour> RegInfo {
			get {
				return null;
			}
		}

		public SerBehaviour() : base(VoidClass.Void) { }
		public SerBehaviour(Behaviour nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerBehaviour(Behaviour nonSerSource) { }
		protected override void CopyOverNonNullNonSerBehaviour(Behaviour nonSerDest) { }

		public override Behaviour CreateBlankNonSer()
		{
			return new Behaviour();
		}

	}

}
