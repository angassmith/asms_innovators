﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Linq;

namespace Save
{
	public interface ISerBehaviour : ISerComponent
	{ }

	/// <summary>
	/// Status:Complete
	/// </summary>
	[System.Serializable]
	public abstract class SerBehaviour<TSer, TNonSer>
		: SerComponent<TSer, TNonSer>, ISerBehaviour
		where TSer : SerBehaviour<TSer, TNonSer>, new()
		where TNonSer : Behaviour
	{
		public bool enabled;

		protected SerBehaviour(VoidClass none) : base(VoidClass.Void) { }
		protected SerBehaviour(TNonSer nonSerSource) : base(nonSerSource) { }

		protected sealed override void CopyFromNonNullNonSerComponent(TNonSer nonSerSource) {
			this.enabled = nonSerSource.enabled;
			CopyFromNonNullNonSerBehaviour(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerBehaviour(TNonSer nonSerSource);

		protected sealed override void CopyOverNonNullNonSerComponent(TNonSer nonSerDest) {
			nonSerDest.enabled = this.enabled;
			CopyOverNonNullNonSerBehaviour(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerBehaviour(TNonSer nonSerDest);

	}
}
