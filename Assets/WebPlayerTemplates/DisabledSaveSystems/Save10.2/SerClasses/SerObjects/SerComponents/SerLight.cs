﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save {

	/// <summary>
	/// Status:Currently only what seems necessary
	/// </summary>
	[System.Serializable]
	public sealed class SerLight : SerComponent<SerLight, Light>
	{
		public override SaveEquivReg.Entry<SerLight, Light> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerLight, Light>(
					"SerLight",
					"Light",
					new SaveEquivReg.ClassStatus<SerLight, Light>(
						x => (SerLight)x,
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.NormalCSharpCurrentlyOnlyWhatSeemsNecessaryButSomeUnknown,
						SaveEquivReg.SavedDataCompletedness.UnknownOrUntested,
						SaveEquivReg.LoadedDataSucess.UnknownOrUntested,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.SerComponent | SaveEquivReg.Uses.NormalDataStorage | SaveEquivReg.Uses.UseableForGeneralSaveNode,
						SaveEquivReg.Additional.None,
						null
					)
				);
			}
		}

		public bool alreadyLightmapped;
		//public SerVector2 areaSize; //Editor only (build will fail if included)
		public float bounceIntensity;
		public SerColor color;
		//public SerCommandBuffer[] commandBuffers; //Does not work
		//public int commandBufferCount; //Does not work
		public SerTexture cookie;
		public float cookieSize;
		public int cullingMask;
		public bool enabled;
		public SerFlare flare;
		public float intensity;
		public float range;
		public LightRenderMode renderMode; //This type is an enum. Enums can be serialized
		public float shadowBias;
		public float shadowNearPlane;
		public float shadowNormalBias;
		public LightShadows shadows; //This type is an enum. Enums can be serialized
		public float shadowStrength;
		public float spotAngle;
		public LightType type; //This type is an enum. Enums can be serialized

		public SerLight() : base(VoidClass.Void) { }
		public SerLight(Light nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerComponent(Light nonSer)
		{
			//updated in future version  alreadyLightmapped =             nonSer.alreadyLightmapped;
			bounceIntensity    =             nonSer.bounceIntensity;
			color              = (SerColor)  nonSer.color;
			cookie             = (SerTexture)nonSer.cookie;
			cookieSize         =             nonSer.cookieSize;
			cullingMask        =             nonSer.cullingMask;
			enabled            =             nonSer.enabled;
			flare              = (SerFlare)  nonSer.flare;
			intensity          =             nonSer.intensity;
			range              =             nonSer.range;
			renderMode         =             nonSer.renderMode;
			shadowBias         =             nonSer.shadowBias;
			shadowNearPlane    =             nonSer.shadowNearPlane;
			shadowNormalBias   =             nonSer.shadowNormalBias;
			shadows            =             nonSer.shadows;
			shadowStrength     =             nonSer.shadowStrength;
			spotAngle          =             nonSer.spotAngle;
			type               =             nonSer.type;
		}

		protected override void CopyOverNonNullNonSerComponent(Light nonSer)
		{
			//updated in future version  nonSer.alreadyLightmapped =          this.alreadyLightmapped;
			nonSer.bounceIntensity    =          this.bounceIntensity;
			nonSer.color              = (Color)  this.color;
			nonSer.cookie             = (Texture)this.cookie;
			nonSer.cookieSize         =          this.cookieSize;
			nonSer.cullingMask        =          this.cullingMask;
			nonSer.enabled            =          this.enabled;
			nonSer.flare              = (Flare)  this.flare;
			nonSer.intensity          =          this.intensity;
			nonSer.range              =          this.range;
			nonSer.renderMode         =          this.renderMode;
			nonSer.shadowBias         =          this.shadowBias;
			nonSer.shadowNearPlane    =          this.shadowNearPlane;
			nonSer.shadowNormalBias   =          this.shadowNormalBias;
			nonSer.shadows            =          this.shadows;
			nonSer.shadowStrength     =          this.shadowStrength;
			nonSer.spotAngle          =          this.spotAngle;
			nonSer.type               =          this.type;
		}

		public override Light CreateBlankNonSer()
		{
			return new Light();
		}

		//	//Casts
		//	public static explicit operator SerLight(Light nonSer)
		//	{ return nonSer == null ? null : new SerLight(nonSer); }
		//	public static explicit operator Light(SerLight ser)
		//	{ return ser == null ? null : ser.CreateNonSer(); }
	}

}