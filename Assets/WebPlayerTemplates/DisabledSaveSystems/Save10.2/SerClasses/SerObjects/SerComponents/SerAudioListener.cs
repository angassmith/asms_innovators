﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save {

	///<summary>Status:?</summary>
	[System.Serializable]
	public sealed class SerAudioListener : SerBehaviour<SerAudioListener, AudioListener>
	{
		public override SaveEquivReg.Entry<SerAudioListener, AudioListener> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerAudioListener, AudioListener>(
					"SerAudioListener",
					"AudioListener",
					new SaveEquivReg.ClassStatus<SerAudioListener, AudioListener>(
						x => (SerAudioListener)x,
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.NormalCSharpInProgress,
						SaveEquivReg.SavedDataCompletedness.UnknownOrUntested,
						SaveEquivReg.LoadedDataSucess.UnknownOrUntested,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.NormalDataStorage | SaveEquivReg.Uses.SerComponent | SaveEquivReg.Uses.UseableForGeneralSaveNode,
						SaveEquivReg.Additional.None,
						null
					)
				);
			}
		}

		public AudioVelocityUpdateMode velocityUpdateMode;

		public SerAudioListener() : base(VoidClass.Void) { }
		public SerAudioListener(AudioListener nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerBehaviour(AudioListener nonSerSource)
		{
			this.velocityUpdateMode = nonSerSource.velocityUpdateMode;
		}

		protected override void CopyOverNonNullNonSerBehaviour(AudioListener nonSerDest)
		{
			nonSerDest.velocityUpdateMode = this.velocityUpdateMode;
		}

		public override AudioListener CreateBlankNonSer()
		{
			return new AudioListener();
		}

		//	//Casts
		//	public static explicit operator SerAudioListener(AudioListener nonSer)
		//		{ return new SerAudioListener(nonSer); }
		//	public static explicit operator AudioListener(SerAudioListener ser)
		//	{
		//		AudioListener nonSer = new AudioListener();
		//		ser.CopyOverExisting(ref nonSer);
		//		return nonSer;
		//	}
	}

}