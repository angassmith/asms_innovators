﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save {

	/// <summary>
	/// Status:As complete as possible (might not work)
	/// </summary>
	[System.Serializable]
	public sealed class SerCamera : SerComponent<SerCamera, Camera>
	{
		public override SaveEquivReg.Entry<SerCamera, Camera> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerCamera, Camera>(
					"SerCamera",
					"Camera",
					new SaveEquivReg.ClassStatus<SerCamera, Camera>(
						x => (SerCamera)x,
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.AllCodeComplete,
						SaveEquivReg.SavedDataCompletedness.SavesAllKnownData,
						SaveEquivReg.LoadedDataSucess.SomeDataFails,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.SerComponent | SaveEquivReg.Uses.NormalDataStorage | SaveEquivReg.Uses.UseableForGeneralSaveNode,
						SaveEquivReg.Additional.None,
						"rect, pixelRect, projectionMatrix, and worldToCameraMatrix cause heaps of errors when applied to an existing Camera. Hopefully gameObject.Transform, fieldOfView, and aspect are enough."
					)
				);
			}
		}

		public float aspect;
		public SerColor backgroundColor;
		public CameraType cameraType; //Enum
		public CameraClearFlags clearFlags; //Enum
		public bool clearStencilAfterLightingPass;
		//public SerCommandBuffer[] commandBuffers; //Does not work
		//public int commandBufferCount; //Does not work
		public int cullingMask;
		public float depth;
		public DepthTextureMode depthTextureMode; //Enum
		public bool enabled;
		public int eventMask;
		public float farClipPlane;
		public float fieldOfView;
		public bool hdr;
		public float[] layerCullDistances;
		public bool layerCullSpherical;
		public float nearClipPlane;
		public UnityEngine.Rendering.OpaqueSortMode opaqueSortMode; //Enum
		public bool orthographic;
		public float orthographicSize;
		public RenderingPath renderingPath; //Enum
		public float stereoConvergence;
		public bool stereoMirrorMode;
		public float stereoSeparation;
		public int targetDisplay;
		public TransparencySortMode transparencySortMode; //Enum
		public bool useOcclusionCulling;
		public SerRenderTexture targetTexture;
		//For some reason setting any of pixelRect, projectionMatrix, rect, or worldToCameraMatrix on an existing Camera
		//causes a bunch of error, including:
		// - Scene is missing a main camera
		// - Screen position out of view frustum
		// - "IsNormalized (normal, 0.001f)"
		//	public SerRect pixelRect;
		//	public SerMatrix4x4 projectionMatrix;
		//	public SerRect rect;
		//	public SerMatrix4x4 worldToCameraMatrix;

		public SerCamera() : base(VoidClass.Void) { }
		public SerCamera(Camera nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerComponent(Camera nonSer)
		{
			this.aspect                        =                   nonSer.aspect;
			this.backgroundColor               = (SerColor)        nonSer.backgroundColor;
			this.cameraType                    =                   nonSer.cameraType;
			this.clearFlags                    =                   nonSer.clearFlags;
			this.clearStencilAfterLightingPass =                   nonSer.clearStencilAfterLightingPass;
			this.cullingMask                   =                   nonSer.cullingMask;
			this.depth                         =                   nonSer.depth;
			this.depthTextureMode              =                   nonSer.depthTextureMode;
			this.enabled                       =                   nonSer.enabled;
			this.eventMask                     =                   nonSer.eventMask;
			this.farClipPlane                  =                   nonSer.farClipPlane;
			this.fieldOfView                   =                   nonSer.fieldOfView;
			this.hdr                           =                   nonSer.hdr;
			this.layerCullDistances            =                   nonSer.layerCullDistances;
			this.layerCullSpherical            =                   nonSer.layerCullSpherical;
			this.nearClipPlane                 =                   nonSer.nearClipPlane;
			this.opaqueSortMode                =                   nonSer.opaqueSortMode;
			this.orthographic                  =                   nonSer.orthographic;
			this.orthographicSize              =                   nonSer.orthographicSize;
			this.renderingPath                 =                   nonSer.renderingPath;
			this.stereoConvergence             =                   nonSer.stereoConvergence;
			this.stereoMirrorMode              =                   nonSer.stereoMirrorMode;
			this.stereoSeparation              =                   nonSer.stereoSeparation;
			this.targetDisplay                 =                   nonSer.targetDisplay;
			this.transparencySortMode          =                   nonSer.transparencySortMode;
			this.useOcclusionCulling           =                   nonSer.useOcclusionCulling;
			this.targetTexture                 = (SerRenderTexture)nonSer.targetTexture;
			//	this.pixelRect                     = (SerRect)         nonSer.pixelRect;
			//	this.projectionMatrix              = (SerMatrix4x4)    nonSer.projectionMatrix;
			//	this.rect                          = (SerRect)         nonSer.rect;
			//	this.worldToCameraMatrix           = (SerMatrix4x4)    nonSer.worldToCameraMatrix;
		}

		protected override void CopyOverNonNullNonSerComponent(Camera existing)
		{
			existing.aspect                        =                this.aspect;
			existing.backgroundColor               = (Color)        this.backgroundColor;
			existing.cameraType                    =                this.cameraType;
			existing.clearFlags                    =                this.clearFlags;
			existing.clearStencilAfterLightingPass =                this.clearStencilAfterLightingPass;
			existing.cullingMask                   =                this.cullingMask;
			existing.depth                         =                this.depth;
			existing.depthTextureMode              =                this.depthTextureMode;
			existing.enabled                       =                this.enabled;
			existing.eventMask                     =                this.eventMask;
			existing.farClipPlane                  =                this.farClipPlane;
			existing.fieldOfView                   =                this.fieldOfView;
			existing.hdr                           =                this.hdr;
			existing.layerCullDistances            =                this.layerCullDistances;
			existing.layerCullSpherical            =                this.layerCullSpherical;
			existing.nearClipPlane                 =                this.nearClipPlane;
			existing.opaqueSortMode                =                this.opaqueSortMode;
			existing.orthographic                  =                this.orthographic;
			existing.orthographicSize              =                this.orthographicSize;
			existing.renderingPath                 =                this.renderingPath;
			existing.stereoConvergence             =                this.stereoConvergence;
			existing.stereoMirrorMode              =                this.stereoMirrorMode;
			existing.stereoSeparation              =                this.stereoSeparation;
			existing.targetDisplay                 =                this.targetDisplay;
			existing.transparencySortMode          =                this.transparencySortMode;
			existing.useOcclusionCulling           =                this.useOcclusionCulling;
			existing.targetTexture                 = (RenderTexture)this.targetTexture;
			//	existing.pixelRect                     = (Rect)         this.pixelRect;
			//	existing.projectionMatrix              = (Matrix4x4)    this.projectionMatrix;
			//	existing.rect                          = (Rect)         this.rect;
			//	existing.worldToCameraMatrix           = (Matrix4x4)    this.worldToCameraMatrix;
		}

		public override Camera CreateBlankNonSer()
		{
			return new Camera();
		}

		//	//Casts
		//	public static explicit operator SerCamera(Camera nonSer)
		//	{ return nonSer == null ? null : new SerCamera(nonSer); }
		//	public static explicit operator Camera(SerCamera ser)
		//	{ return ser == null ? null : ser.CreateNonSer(); }
	}

}