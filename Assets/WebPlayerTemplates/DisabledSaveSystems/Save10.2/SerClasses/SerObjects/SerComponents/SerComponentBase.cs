﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Linq;

namespace Save
{
	public interface ISerComponent : ISerObject
	{ }

	/// <summary>
	/// Status:Complete
	/// </summary>
	[System.Serializable]
	public abstract class SerComponent<TSer, TNonSer>
		: SerObject<TSer, TNonSer>, ISerComponent
		where TSer : SerComponent<TSer, TNonSer>, new()
		where TNonSer : Component
	{
		
		protected SerComponent(VoidClass none) : base(VoidClass.Void) { }
		protected SerComponent(TNonSer nonSerSource) : base(nonSerSource) { }

		//May be used for future versions of Unity
		protected sealed override void CopyFromNonNullNonSerObject(TNonSer nonSerSource)
		{
			CopyFromNonNullNonSerComponent(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerComponent(TNonSer nonSerSource);

		//May be used for future versions of Unity
		protected sealed override void CopyOverNonNullNonSerObject(TNonSer nonSerDest)
		{
			CopyOverNonNullNonSerComponent(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerComponent(TNonSer nonSerDest);

		//	public void CallCopyFromNonSer(Component nonSer)
		//	{
		//		if (nonSer == null) return;
		//		TNonSer castedNonSer = nonSer as TNonSer;
		//		if (castedNonSer != null) {
		//			CopyFromNonNullNonSer(castedNonSer);
		//		} else {
		//			throw new SerComponentTypeException(typeof(TSer), typeof(TNonSer), typeof(SerComponent<TSer, TNonSer>), typeof(TSer), nonSer.GetType(), "The serializable and non-serializable types do not match");
		//		}
		//	}
		//	
		//	
		//	public void CallCopyOverNonSer(Component nonSer)
		//	{
		//		if (nonSer == null) return;
		//		TNonSer castedNonSer = nonSer as TNonSer;
		//		if (castedNonSer != null) {
		//			CopyOverNonNullNonSer(castedNonSer);
		//		} else {
		//			throw new SerComponentTypeException(typeof(TSer), typeof(TNonSer), typeof(SerComponent<TSer, TNonSer>), typeof(TSer), nonSer.GetType(), "The serializable and non-serializable types do not match");
		//		}
		//	}
		//	
		//	
		//	public Component CallCreateNonSer()
		//	{
		//		return this.CreateNonSer();
		//	}


		//	//?//	public static explicit operator SerComponent<TSer, TNonSer>(TNonSer nonSer) {
		//	//?//		if (nonSer == null) return null;
		//	//?//		else {
		//	//?//			TSer ser = new TSer();
		//	//?//			ser.CopyFromNonNullNonSer(nonSer);
		//	//?//			return ser;
		//	//?//		}
		//	//?//	}
		//	//?//	public static explicit operator TNonSer(SerComponent<TSer, TNonSer> ser) {
		//	//?//		if (ser == null) return null;
		//	//?//		else return ser.CreateNonSer();
		//	//?//	}
	}
}
