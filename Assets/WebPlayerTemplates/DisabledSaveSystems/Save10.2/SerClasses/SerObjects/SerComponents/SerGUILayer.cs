﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save {

	/// <summary>
	/// Status:?
	/// </summary>
	[System.Serializable]
	public sealed class SerGUILayer : SerBehaviour<SerGUILayer, GUILayer>
	{
		public override SaveEquivReg.Entry<SerGUILayer, GUILayer> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerGUILayer, GUILayer>(
					"SerGUILayer",
					"GUILayer",
					new SaveEquivReg.ClassStatus<SerGUILayer, GUILayer>(
						x => (SerGUILayer)x,
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.NormalCSharpInProgress,
						SaveEquivReg.SavedDataCompletedness.UnknownOrUntested,
						SaveEquivReg.LoadedDataSucess.UnknownOrUntested,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.SerComponent | SaveEquivReg.Uses.UseableForGeneralSaveNode,
						SaveEquivReg.Additional.None,
						null
					)
				);
			}
		}

		//No public data

		public SerGUILayer() : base(VoidClass.Void) { }
		public SerGUILayer(GUILayer nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerBehaviour(GUILayer nonSerSource)
		{
			//No public data
		}

		protected override void CopyOverNonNullNonSerBehaviour(GUILayer nonSerSource)
		{
			//No public data
		}

		public override GUILayer CreateBlankNonSer()
		{
			return new GUILayer();
		}

		//	//Casts
		//	public static explicit operator SerGUILayer(GUILayer nonSer)
		//		{ return new SerGUILayer(nonSer); }
		//	public static explicit operator GUILayer(SerGUILayer ser)
		//	{
		//		GUILayer nonSer = new GUILayer();
		//		ser.CopyOverExisting(ref nonSer);
		//		return nonSer;
		//	}
	}

}