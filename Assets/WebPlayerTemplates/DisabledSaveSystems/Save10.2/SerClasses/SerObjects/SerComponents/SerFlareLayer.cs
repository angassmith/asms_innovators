﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save {

	/// <summary>
	/// Status:As complete as possible (might not work)
	/// </summary>
	[System.Serializable]
	public sealed class SerFlareLayer : SerBehaviour<SerFlareLayer, FlareLayer>
	{
		public override SaveEquivReg.Entry<SerFlareLayer, FlareLayer> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerFlareLayer, FlareLayer>(
					"SerFlareLayer",
					"FlareLayer",
					new SaveEquivReg.ClassStatus<SerFlareLayer, FlareLayer>(
						x => (SerFlareLayer)x,
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.NormalCSharpComplete,
						SaveEquivReg.SavedDataCompletedness.UnknownOrUntested,
						SaveEquivReg.LoadedDataSucess.UnknownOrUntested,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.SerComponent | SaveEquivReg.Uses.UseableForGeneralSaveNode,
						SaveEquivReg.Additional.None,
						null
					)
				);
			}
		}

		//No public data

		public SerFlareLayer() : base(VoidClass.Void) { }
		public SerFlareLayer(FlareLayer nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerBehaviour(FlareLayer nonSerSource)
		{
			//No public data
		}

		protected override void CopyOverNonNullNonSerBehaviour(FlareLayer nonSerSource)
		{
			//No public data
		}

		public override FlareLayer CreateBlankNonSer()
		{
			throw SaveableEquivHelper.CreateTypeNotInstantiableException("FlareLayer");
		}

		//	//Casts
		//	public static explicit operator SerFlareLayer(FlareLayer nonSer)
		//	{ return new SerFlareLayer(nonSer); }
		//	public static explicit operator FlareLayer(SerFlareLayer ser)
		//	{
		//		//No accessible constructor
		//		FlareLayer nonSer = null;
		//		ser.CopyOverExisting(ref nonSer);
		//		return nonSer;
		//	}
	}

}