﻿using UnityEngine;
using System.Collections;
using System;

namespace Save
{
	/// <summary>
	/// Status:See base
	/// </summary>
	[System.Serializable]
	public sealed class SerComponent : SerComponent<SerComponent, Component>
	{
		public override SaveEquivReg.Entry<SerComponent, Component> RegInfo {
			get {
				return null;
			}
		}

		public SerComponent() : base(VoidClass.Void) { }
		public SerComponent(Component nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerComponent(Component nonSerSource) { }
		protected override void CopyOverNonNullNonSerComponent(Component nonSerDest) { }

		public override Component CreateBlankNonSer()
		{
			return new Component();
		}

	}

}