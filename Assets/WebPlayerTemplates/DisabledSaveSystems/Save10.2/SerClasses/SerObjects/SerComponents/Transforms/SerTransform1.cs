﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using UnityEngine.SceneManagement;

/*

namespace Save
{
	//	public static class SerTransformMap
	//	{
	//		public static Dictionary<int, SerTransform> MapTransformsInCurrentScene()
	//		{
	//			//Setup
	//			Dictionary<int, SerTransform> ongoingDict = new Dictionary<int, SerTransform>();
	//	
	//			//Get all root Transforms in scene
	//			GameObject[] rootGObjs = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
	//			List<Transform> rootTransforms = new List<Transform>();
	//			foreach (GameObject rootGObj in rootGObjs) {
	//				if (rootGObj != null && rootGObj.transform != null) rootTransforms.Add(rootGObj.transform);
	//			}
	//	
	//			//Map all root Transforms
	//			foreach (Transform root in rootTransforms)
	//			{
	//				MapTransform(root, null, ongoingDict);
	//			}
	//	
	//			return ongoingDict;
	//		}
	//	
	//		public static Dictionary<int, SerTransform> MapTransformsInAllScenes()
	//		{
	//			//Setup
	//			Dictionary<int, SerTransform> ongoingDict = new Dictionary<int, SerTransform>();
	//	
	//			//Get all root Transforms in all scenes
	//			Dictionary<int, Transform> rootTransforms = new Dictionary<int, Transform>();
	//			int sceneCount = SceneManager.sceneCount;
	//			for (int i = 0; i < sceneCount; i++) {
	//				Scene scene = SceneManager.GetSceneAt(i);
	//				foreach (GameObject rootGObj in scene.GetRootGameObjects()) {
	//					if (
	//						   rootGObj != null
	//						&& rootGObj.transform != null
	//						&& rootGObj.transform.parent == null
	//					) {
	//						SaveNodeBase rootGObjSaveNode = rootGObj.GetComponent<SaveNodeBase>();
	//						if (rootGObjSaveNode != null && rootGObjSaveNode.ShouldSave) {
	//							Transform rootTransform = rootGObj.transform;
	//							int instID = rootTransform.GetInstanceID();
	//							if (!rootTransforms.ContainsKey(instID)) rootTransforms.Add(instID, rootTransform);
	//						}
	//					}
	//				}
	//			}
	//	
	//			//Map all root Transforms
	//			foreach (KeyValuePair<int, Transform> rootTransform in rootTransforms)
	//			{
	//				MapTransform(rootTransform.Value, null, ongoingDict);
	//			}
	//	
	//			return ongoingDict;
	//		}
	//	
	//		private static void MapTransform(Transform transform, SerTransform parent, Dictionary<int, SerTransform> ongoingDict)
	//		{
	//			//Create serTransform
	//			SerTransform serTransform = new SerTransform(transform, parent);
	//	
	//			//Add serTransform to map lookup dictionary
	//			ongoingDict.Add(transform.GetInstanceID(), serTransform);
	//	
	//			//Map all child transforms
	//			foreach (Transform child in transform)
	//			{
	//				MapTransform(child, serTransform, ongoingDict);
	//			}
	//		}
	//	}

	[System.Serializable]
	public class SerTransform : SerComponent<SerTransform, Transform> //Complete
	{
		//public SerVector3 eulerAngles;
		//public SerVector3 forward;
		public bool hasChanged;
		//public SerVector3 localEulerAngles;
		public SerVector3 localPosition;
		public SerQuaternion localRotation;
		public SerVector3 localScale;
		//public SerTransform parent;
		//public SerVector3 position;
		//public SerVector3 right;
		//public SerQuaternion rotation;
		//public SerVector3 up;

		////	private int? backingSourceID = null;
		////	public int? sourceID { get { return backingSourceID; } }
		////	
		////	private List<SerTransform> children = new List<SerTransform>();
		////	private SerTransform backingParent;
		////	
		////	public SerTransform parent { get { return backingParent; } }
		////	
		////	public int ChildCount {
		////		get { return children.Count; }
		////	}

		

		public SerTransform() : base(VoidClass.Void) { }
		public SerTransform(Transform nonSer) : base(nonSer) { }
		public SerTransform(Transform nonSer, SerTransform parent) : this(nonSer) {
			////	this.backingParent = parent;
		}

		protected override void CopyFromNonNullNonSerComponent(Transform nonSer)
		{
			////	this.backingSourceID = nonSer.GetInstanceID();

			this.hasChanged    =                nonSer.hasChanged;
			this.localScale    = (SerVector3)   nonSer.localScale;
			this.localPosition = (SerVector3)   nonSer.localPosition;
			this.localRotation = (SerQuaternion)nonSer.localRotation;
			////	int childCount = nonSer.childCount;
			////	foreach (Transform child in nonSer) {
			////		this.children.Add(new SerTransform(child, this));
			////	}
		}

		protected override void CopyOverNonNullNonSerComponent(Transform nonSer)
		{
			nonSer.hasChanged    =             this.hasChanged;
			nonSer.localScale    = (Vector3)   this.localScale;
			nonSer.localPosition = (Vector3)   this.localPosition;
			nonSer.localRotation = (Quaternion)this.localRotation;
		}

		public override Transform CreateBlankNonSer()
		{
			throw SaveableEquivExtensions.GetTypeNotInstantiableException("Transform");
		}

		////	public void CopySourceIDFrom(Transform nonSer)
		////	{
		////		if (nonSer == null) throw new ArgumentNullException("Cannot copy a source-ID from a null Transform");
		////		else backingSourceID = nonSer.GetInstanceID();
		////	}
		////	
		////	/// <summary>
		////	/// Returns:<para/>
		////	/// if (other == null) return false;<para/>
		////	/// else if (this.sourceID == null || other.sourceID == null) return ReferenceEquals(this, other);<para/>
		////	/// else return this.sourceID == other.sourceID;
		////	/// </summary>
		////	public bool Equals(SerTransform other)
		////	{
		////		if (other == null) return false;
		////		else if (this.backingSourceID == null || other.backingSourceID == null) return ReferenceEquals(this, other);
		////		else return this.backingSourceID == other.backingSourceID;
		////	}
		////	
		////	public SerTransform GetChild(int i) {
		////		return children[i];
		////	}
		////	
		////	public void SetChild(int i, SerTransform newChild) {
		////		ValidatePotentialChild(newChild);
		////		children[i].backingParent = null;
		////		children[i] = newChild;
		////		newChild.backingParent = this;
		////	}
		////	
		////	public void AddChild(SerTransform newChild)
		////	{
		////		ValidatePotentialChild(newChild);
		////		children.Add(newChild);
		////		newChild.backingParent = this;
		////	}
		////	
		////	public void RemoveChild(int i)
		////	{
		////		children[i].backingParent = null;
		////		children.RemoveAt(i);
		////	}
		////	
		////	public void InsertChild(int i, SerTransform newChild)
		////	{
		////		children.Insert(i, newChild);
		////		newChild.backingParent = this;
		////	}
		////	
		////	public void RemoveAllChildren()
		////	{
		////		foreach (SerTransform child in children) {
		////			try { child.backingParent = null; } catch (NullReferenceException) { }
		////		}
		////		children.Clear();
		////	}
		////	
		////	public void SetParent(SerTransform newParent)
		////	{
		////		ValidatePotentialParent(newParent);
		////		if (this.backingParent != null) {
		////			int indexOfThisInParent = this.parent.children.FindIndex((x) => ReferenceEquals(x, this));
		////			this.backingParent.RemoveChild(indexOfThisInParent);
		////		}
		////		this.backingParent = null;
		////		if (newParent != null) newParent.AddChild(this);
		////	}
		////	
		////	/// <summary>Throws an ArgumentException if newChild is null, newChild.parent is not null, or newChild is a parent of the current SerTransform</summary>
		////	/// <exception cref="ArgumentException"></exception>
		////	public void ValidatePotentialChild(SerTransform newChild)
		////	{
		////		if (newChild == null) throw new ArgumentException("The new child SerTransform cannot be null");
		////		if (newChild.backingParent != null) throw new ArgumentException("The new child SerTransform already has a parent");
		////		if (this.IsChildOf(newChild)) throw new ArgumentException("The new child SerTransform is a parent of the current SerTransform");
		////	}
		////	
		////	/// <summary>Throws an ArgumentException if newChild is null, newChild.parent is not null, or newChild is a parent of the current SerTransform</summary>
		////	/// <exception cref="ArgumentException"></exception>
		////	public void ValidatePotentialParent(SerTransform newParent)
		////	{
		////		if (newParent != null && newParent.IsChildOf(this)) throw new ArgumentException("The new parent SerTransform is a child of the current SerTransform");
		////	}
		////	
		////	/// <summary>Returns true if potentialParent is the current SerTransform, potentialParent is in the parent chain of the current SerTransform, or potentialParent is null (the root parent is always null)</summary>
		////	public bool IsChildOf(SerTransform potentialParent)
		////	{
		////		if (potentialParent == null) return true; //All SerTransforms have null as the root parent
		////		SerTransform current = this;
		////		while (current != null)
		////		{
		////			if (ReferenceEquals(current, potentialParent)) return true;
		////			current = current.backingParent;
		////		}
		////		return false;
		////	}

		//	public static explicit operator SerTransform(Transform nonSer)
		//	{ return new SerTransform(nonSer); }
		//	//[Cast not possible]
	}
}

//*/