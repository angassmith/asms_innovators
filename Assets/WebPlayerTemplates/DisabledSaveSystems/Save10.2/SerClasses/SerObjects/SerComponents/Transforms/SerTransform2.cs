﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;
using UnityEngine.SceneManagement;

namespace Save
{
	[System.Serializable]
	///<summary>Status:See base</summary>
	public sealed class SerTransform : SerTransform<SerTransform, Transform>
	{
		public override SaveEquivReg.Entry<SerTransform, Transform> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerTransform, Transform>(
					"SerTransform",
					"Transform",
					new SaveEquivReg.ClassStatus<SerTransform, Transform>(
						x => (SerTransform)x,
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.AllCodeComplete,
						SaveEquivReg.SavedDataCompletedness.SavesAllKnownData,
						SaveEquivReg.LoadedDataSucess.AllDataWorksSoFar,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.SerComponent | SaveEquivReg.Uses.NormalDataStorage,
						SaveEquivReg.Additional.None,
						"Local/global position+rotation+scale might not work (due to application of the SerTransform class, not the actual class);"
					)
				);
			}
		}

		public SerTransform() : base(VoidClass.Void) { }
		public SerTransform(Transform nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerTransform(Transform nonSer) { }
		protected override void CopyOverNonNullNonSerTransform(Transform nonSer) { }

		public override Transform CreateBlankNonSer()
		{
			throw SaveableEquivHelper.CreateTypeNotInstantiableException("Transform");
		}
		
		//	public static explicit operator SerTransform(Transform nonSer)
		//	{ return new SerTransform(nonSer); }
		//	//[Cast not possible]
	}
}