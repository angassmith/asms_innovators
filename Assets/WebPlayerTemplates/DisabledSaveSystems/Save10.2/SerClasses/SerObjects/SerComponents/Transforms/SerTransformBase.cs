﻿using UnityEngine;
using System.Collections;

namespace Save
{

	public interface ISerTransform : ISerComponent
	{ }

	[System.Serializable]
	///<summary>Status:Complete</summary>
	public abstract class SerTransform<TSer, TNonSer>
		: SerComponent<TSer, TNonSer>, ISerTransform
		where TSer : SerTransform<TSer, TNonSer>, new()
		where TNonSer : Transform
	{
		//public SerVector3 eulerAngles;
		//public SerVector3 forward;
		public bool hasChanged;
		//public SerVector3 localEulerAngles;
		public SerVector3 localPosition;
		public SerQuaternion localRotation;
		public SerVector3 localScale;
		//public SerTransform parent;
		//public SerVector3 position;
		//public SerVector3 right;
		//public SerQuaternion rotation;
		//public SerVector3 up;

		public SerTransform(VoidClass none) : base(VoidClass.Void) { }
		public SerTransform(TNonSer nonSerSource) : base(nonSerSource) { }

		protected sealed override void CopyFromNonNullNonSerComponent(TNonSer nonSerSource)
		{
			this.hasChanged    =                nonSerSource.hasChanged;
			this.localScale    = (SerVector3)   nonSerSource.localScale;
			this.localPosition = (SerVector3)   nonSerSource.localPosition;
			this.localRotation = (SerQuaternion)nonSerSource.localRotation;
			CopyFromNonNullNonSerTransform(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerTransform(TNonSer nonSerSource);

		protected sealed override void CopyOverNonNullNonSerComponent(TNonSer nonSerDest)
		{
			nonSerDest.hasChanged    =             this.hasChanged;
			nonSerDest.localScale    = (Vector3)   this.localScale;
			nonSerDest.localPosition = (Vector3)   this.localPosition;
			nonSerDest.localRotation = (Quaternion)this.localRotation;
			CopyOverNonNullNonSerTransform(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerTransform(TNonSer nonSerDest);

	}
}
