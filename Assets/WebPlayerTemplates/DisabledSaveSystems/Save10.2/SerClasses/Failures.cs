﻿using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;

///<summary>Complete Failure</summary>
[System.Serializable]
public class SerFlare {
	public static explicit operator SerFlare(Flare nonSer) { if (nonSer == null) return null; return new SerFlare(); }
	public static explicit operator Flare(SerFlare ser   ) { if (ser == null   ) return null; return new Flare();    }
}

///<summary>Complete Failure</summary>
[System.Serializable]
public class SerCommandBuffer {
	public static explicit operator SerCommandBuffer(CommandBuffer nonSer) { if (nonSer == null) return null; return new SerCommandBuffer(); }
	public static explicit operator CommandBuffer(SerCommandBuffer ser   ) { if (ser == null   ) return null; return new CommandBuffer();    }
}



