﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Save
{
	public static class SaveHelper
	{
		public static Dictionary<TKey, TResultValue> ConvertValues<TKey, TSourceValue, TResultValue>(this Dictionary<TKey, TSourceValue> source, Func<TSourceValue, TResultValue> itemValueConverter)
		{
			if (source == null) return null;
			if (itemValueConverter == null) throw new ArgumentNullException("itemValueConverter", "The valueConverter argument cannot be null");
			Dictionary<TKey, TResultValue> res = new Dictionary<TKey, TResultValue>();
			foreach (KeyValuePair<TKey, TSourceValue> kvp in source)
			{
				res.Add(kvp.Key, itemValueConverter(kvp.Value));
			}
			return res;
		}

		public static List<TResult> ConvertItems<TSource, TResult>(this List<TSource> source, Func<TSource, TResult> itemConverter)
		{
			if (source == null) return null;
			if (itemConverter == null) throw new ArgumentNullException("itemConverter", "The valueConverter argument cannot be null");
			List<TResult> res = new List<TResult>();
			foreach (TSource item in source)
			{
				res.Add(itemConverter(item));
			}
			return res;
		}

		public static TResult[] ConvertItems<TSource, TResult>(this TSource[] source, Func<TSource, TResult> itemConverter)
		{
			if (source == null) return null;
			if (itemConverter == null) throw new ArgumentNullException("itemConverter", "The itemConverter argument cannot be null");
			int len = source.Length;
			TResult[] res = new TResult[len];
			for (int i = 0; i < len; i++) {
				res[i] = itemConverter(source[i]);
			}
			return res;
		}

		
		///<summary>Create files, with directories created automatically</summary>
		public static FileStream CreateFile(string path)
		{
			Directory.CreateDirectory(Path.GetDirectoryName(path));
			return File.Open(path, FileMode.Create, FileAccess.Write);
		}


		public static void ForceSerializeable(Type type)
		{
			ForceSerializeable(type, new SerializationException("The type '" + type.FullName + "' is missing the [System.Serializeable] attribute (SaveHelper.ForceSerializeable(Type type) has been called on this type)"));
		}
		public static void ForceSerializeable(Type type, Exception potentialException)
		{
			if (type == null) throw new ArgumentNullException("type", "The 'type' argument cannot be null");
			if (!type.IsDefined(typeof(SerializableAttribute), false)) throw potentialException;
		}

		public static void ForceSerializeableUpTo(Type type, Type highestParent)
		{
			if (type == null) throw new ArgumentNullException("type", "The 'type' argument cannot be null");
			if (highestParent == null) throw new ArgumentNullException("highestParent", "The 'highestParent' argument cannot be null");
			ForceSerializeableUpTo(
				type,
				highestParent,
				t => new SerializationException(
					  "The type '" + t.FullName
					+ "' is missing the [System.Serializeable] attribute "
					+ "(SaveHelper.ForceSerializeableUpTo(Type type, Type highestParent)"
					+ " has been called with type = '" + type.FullName
					+ "', highestParent = '" + highestParent.FullName
					+ "')"
				)
			);
		}
		public static void ForceSerializeableUpTo(Type type, Type highestParent, Func<Type, Exception> potentialException)
		{
			if (potentialException == null) throw new ArgumentNullException("potentialException", "The 'potentialException' argument cannot be null.");
			Type nonSerializableType = GetFirstNonSerializeableTypeUpTo(type, highestParent);
			if (nonSerializableType != null)
			{
				throw potentialException(type);
			}
		}
		//Note: Checks the parent type, not just descended types
		public static Type GetFirstNonSerializeableTypeUpTo(Type type, Type highestParent)
		{
			if (type == null) throw new ArgumentNullException("type", "The 'type' argument cannot be null");
			if (highestParent == null) throw new ArgumentNullException("highestParent", "The 'highestParent' argument cannot be null");
			Type current = type;
			if (type.IsSubclassOf(highestParent) || type.Equals(highestParent))
			{
				if (!type.IsDefined(typeof(SerializableAttribute), false)) return type;
				while (!highestParent.Equals(current))
				{
					current = current.BaseType;
					if (!type.IsDefined(typeof(SerializableAttribute), false)) return type;
				}
				return null;
			} else {
				throw new ArgumentException("The 'highestParent' argument is not a parent class of the 'type' argument");
			}
		}


		/// <summary>
		/// Gets all root SaveNodes in all scenes recognised by the SceneManager class
		/// </summary>
		public static ReadOnlyCollection<SaveNodeBase> GetRootSaveNodesSetToSaveInAllSceneManagedScenes()
		{
			//Setup
			List<int> rootSNodeIDs = new List<int>();
			List<SaveNodeBase> rootSNodes = new List<SaveNodeBase>();

			//Loop through all scenes
			int sceneCount = SceneManager.sceneCount;
			for (int i = 0; i < sceneCount; i++) {
				Scene scene = SceneManager.GetSceneAt(i);

				//For each scene, Loop through all root gameobjects in
				foreach (GameObject rootGObj in scene.GetRootGameObjects()) {
					
					//Check if the gameobject is valid
					if (
						   rootGObj != null
						&& rootGObj.transform != null
						&& rootGObj.transform.parent == null //Just in case
					) {

						//Get the SaveNode if there is one, and check if it is set to be saved
						SaveNodeBase rootGObjSaveNode = rootGObj.GetComponent<SaveNodeBase>();
						if (rootGObjSaveNode != null && rootGObjSaveNode.ShouldSave) {

							//Add the SaveNode if it has not already been added (check this using the instance-id)
							int instID = rootGObjSaveNode.GetInstanceID();
							if (!rootSNodeIDs.Contains(instID)) {
								rootSNodeIDs.Add(instID);
								rootSNodes.Add(rootGObjSaveNode);
							}
						}
					}
				}
			}

			return rootSNodes.AsReadOnly();
		}

		/// <summary>
		/// Gets all root SaveNodes in the current scene
		/// </summary>
		public static ReadOnlyCollection<SaveNodeBase> GetRootSaveNodesSetToSaveInCurrentScene()
		{
			//Setup
			List<int> rootSNodeIDs = new List<int>();
			List<SaveNodeBase> rootSNodes = new List<SaveNodeBase>();

			Scene scene = SceneManager.GetActiveScene();
			
			//For each scene, Loop through all root gameobjects in
			foreach (GameObject rootGObj in scene.GetRootGameObjects()) {
					
				//Check if the gameobject is valid
				if (
						rootGObj != null
					&& rootGObj.transform != null
					&& rootGObj.transform.parent == null //Just in case
				) {

					//Get the SaveNode if there is one, and check if it is set to be saved
					SaveNodeBase rootGObjSaveNode = rootGObj.GetComponent<SaveNodeBase>();
					if (rootGObjSaveNode != null && rootGObjSaveNode.ShouldSave) {

						//Add the SaveNode if it has not already been added (check this using the instance-id)
						int instID = rootGObjSaveNode.GetInstanceID();
						if (!rootSNodeIDs.Contains(instID)) {
							rootSNodeIDs.Add(instID);
							rootSNodes.Add(rootGObjSaveNode);
						}
					}
				}
			}

			return rootSNodes.AsReadOnly();
		}


		/// <summary>
		/// Gets all root SaveNodes in the current scene, without using SceneManager (which can throw exceptions when called during the Awake() stage)
		/// </summary>
		public static ReadOnlyCollection<SaveNodeBase> GetRootSaveNodesSetToSaveInCurrentSceneWhenSceneMightBeLoading()
		{
			//Setup
			List<int> rootSNodeIDs = new List<int>();
			List<SaveNodeBase> rootSNodes = new List<SaveNodeBase>();

			foreach (SaveNodeBase snode in Resources.FindObjectsOfTypeAll<SaveNodeBase>())
			{
				if (
					snode != null
					&& snode.transform != null
					&& snode.transform.parent == null //Note the ==
				) {
					//If the SaveNode is valid and attached to a root GameObject
					
					if (snode.ShouldSave)
					{
						//Add the SaveNode if it has not already been added (check this using the instance-id)
						int instID = snode.GetInstanceID();
						if (!rootSNodeIDs.Contains(instID)) {
							rootSNodeIDs.Add(instID);
							rootSNodes.Add(snode);
						}
					}
				}
			}

			return rootSNodes.AsReadOnly();
		}



		public static bool Single<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate, out TSource match)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (predicate == null) throw new ArgumentNullException("predicate");

			int matchCount = 0;
			match = default(TSource);

			foreach (TSource item in source)
			{
				if (predicate(item)) {
					if (matchCount == 0) {
						matchCount = 1;
						match = item;
					} else {
						matchCount = -1;
					}
				}
			}

			return matchCount == 1;
		}


		public static bool HasFlagAndNonNegative(this Enum target, Enum flag)
		{
			TypeCode targetTypeCode = target.GetTypeCode(); //Or maybe Enum.GetUnderlyingType() (online info is very lacking)
			object targetVal = Convert.ChangeType(target, targetTypeCode);

			TypeCode flagTypeCode = flag.GetTypeCode(); //Or maybe Enum.GetUnderlyingType() (online info is very lacking)
			object flagVal = Convert.ChangeType(flag, flagTypeCode);
			
			if (flagTypeCode != targetTypeCode) throw new ArgumentException("The underlying storage types for the target and flag parameters are different.");

			if (targetTypeCode == TypeCode.Byte  ) { var targetValCasted = (Byte  )targetVal; return targetValCasted >= 0 && (targetValCasted & (Byte  )flagVal) != 0; }
			if (targetTypeCode == TypeCode.SByte ) { var targetValCasted = (SByte )targetVal; return targetValCasted >= 0 && (targetValCasted & (SByte )flagVal) != 0; }
			if (targetTypeCode == TypeCode.Int16 ) { var targetValCasted = (Int16 )targetVal; return targetValCasted >= 0 && (targetValCasted & (Int16 )flagVal) != 0; }
			if (targetTypeCode == TypeCode.UInt16) { var targetValCasted = (UInt16)targetVal; return targetValCasted >= 0 && (targetValCasted & (UInt16)flagVal) != 0; }
			if (targetTypeCode == TypeCode.Int32 ) { var targetValCasted = (Int32 )targetVal; return targetValCasted >= 0 && (targetValCasted & (Int32 )flagVal) != 0; }
			if (targetTypeCode == TypeCode.UInt32) { var targetValCasted = (UInt32)targetVal; return targetValCasted >= 0 && (targetValCasted & (UInt32)flagVal) != 0; }
			if (targetTypeCode == TypeCode.Int64 ) { var targetValCasted = (Int64 )targetVal; return targetValCasted >= 0 && (targetValCasted & (Int64 )flagVal) != 0; }
			if (targetTypeCode == TypeCode.UInt64) { var targetValCasted = (UInt64)targetVal; return targetValCasted >= 0 && (targetValCasted & (UInt64)flagVal) != 0; }

			throw new ArgumentException("The target has an unknown underlying storage type");
		}


		/// <summary>
		/// Same as obj.GetType(), but returns null if obj is null instead of throwing a NullReferenceException
		/// </summary>
		public static Type GetType(object obj) {
			return obj == null ? null : obj.GetType();
		}



		public delegate void StructApplier<T>(ref T source);

		/// <summary>
		/// Applies a function to a struct, and returns the modified struct.
		/// The specified function does not return the modified struct, but instead uses ref.
		/// <para/>
		/// Unfortunately, ref cannot be used in a lambda expression,
		/// however the full delegate syntax can be used instead,
		/// eg. "foo.Apply(delegate(ref Blah x) { x.bla = 5; })
		/// </summary>
		/// <exception cref="ArgumentNullException">source or func is null</exception>
		public static T Apply<T>(this T source, StructApplier<T> func)
			where T : struct
		{
			if (func == null) throw new ArgumentNullException("func");
			func(ref source);
			return source;
		}

		/// <summary>
		/// Applies a function to an object, and returns the modified object. The specified function should modify/replace and return the object.
		/// </summary>
		/// <exception cref="ArgumentNullException">source or func is null</exception>
		public static T Apply<T>(this T source, Func<T, T> func)
		{
			if (source == null) throw new ArgumentNullException("source");
			if (func == null) throw new ArgumentNullException("func");
			return func(source);
		}

		/// <summary>
		/// Applies a function to a class, and returns the modified class.
		/// </summary>
		/// <exception cref="ArgumentNullException">source or func is null</exception>
		public static T Apply<T>(this T source, Action<T> func)
			where T : class
		{
			if (source == null) throw new ArgumentNullException("source");
			if (func == null) throw new ArgumentNullException("func");
			func(source);
			return source;
		}

	}
}
