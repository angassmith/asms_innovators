﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Save
{
	[System.Serializable]
	public class SavedGame : ISaveable
	{
		public List<SaveNodeBase.SaveNodeSaveData> RootSaveNodeData = new List<SaveNodeBase.SaveNodeSaveData>();
		public List<StaticSaveData> StaticData = new List<StaticSaveData>();
		public SavedGame() { }
		public SavedGame(List<SaveNodeBase.SaveNodeSaveData> rootSaveNodeData, List<StaticSaveData> staticData) {
			this.RootSaveNodeData = rootSaveNodeData;
			this.StaticData = staticData;
		}
	}
}
