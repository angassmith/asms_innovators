﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEditor;

namespace Save
{

	[CustomEditor(typeof(SavedMonoBehaviour), editorForChildClasses:true)]
	[DeliberatelyNonSerializable]
	public class SavedMonoBehaviourEditor : Editor
	{
		public virtual void OnInspectorGUI2() { }

		public sealed override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			((SavedMonoBehaviour)target).CheckForSaveNode(x => DestroyImmediate(x, true)); //Unity counts prefabs as assets

			OnInspectorGUI2();
		}
	}


}

#endif