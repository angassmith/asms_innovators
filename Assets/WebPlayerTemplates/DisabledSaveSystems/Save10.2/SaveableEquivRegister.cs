﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections;
using Entry = MultiKeyDictEntry<string, string, System.Type, System.Type, Save.SaveEquivReg.ClassStatus>;

namespace Save
{
	public static class SaveEquivReg {

		public static MultiKeyDict<string, string, Type, Type, ClassStatus> SaveableEquivTypes = new MultiKeyDict<string, string, Type, Type, ClassStatus>();

		public static void BuildReg()
		{
			SaveableEquivTypes.Clear();

			Add(new SerAudioListener());
			Add(new SerBehaviour());
			Add(new SerCamera());
			Add(new SerColor());
			Add(new SerColor32());
			//Add(new SerCommandBuffer()); //TODO: Fix SerCommandBuffer
			Add(new SerComponent());
			//Add(new SerFlare()); //TODO: Fix SerFlare
			Add(new SerFlareLayer());
			Add(new SerGameObject());
			Add(new SerGeneralSaveNodeComponentHandler());
			Add(new SerGUILayer());
			Add(new SerLight());
			Add(new SerMaterial());
			Add(new SerMaterialPropertyBlock());
			Add(new SerMatrix4x4());
			Add(new SerObject());
			Add(new SerQuaternion());
			Add(new SerRay());
			Add(new SerRay2D());
			Add(new SerRect());
			Add(new SerRenderer());
			Add(new SerRenderTexture());
			Add(new SerScriptableObject());
			Add(new SerShader());
			Add(new SerSprite());
			Add(new SerSpriteRenderer());
			Add(new SerTexture());
			Add(new SerTransform());
			Add(new SerVector2());
			Add(new SerVector3());
			Add(new SerVector4());
#if UNITY_EDITOR
			//	Add(new Docs.SerBASEEXAMPLE());
			//	Add(new Docs.SerBASETYPE());
			//	Add(new Docs.SerDERIVEDEXAMPLE());
			//	Add(new Docs.SerDERIVEDTYPE());
#endif
			//Add more here as more types are created
		}

		[Flags]
		public enum CodeType
		{
			TODO = -2,
			Unknown = -1,
			NormalCSharp = 0,
			Reflection = 1<<1,
			Activator = 1<<2,
			SerializationFormatterServices = 1<<3,
			OtherHighLevelLanguage = 1<<4,
			MSIL = 1<<5,
			ReflectionEmit = 1<<6,
		}

		public enum CodeCompletedness
		{
			TODO = -1,
			Unknown = 0,
			NormalCSharpInProgress = 1,
			NormalCSharpCurrentlyOnlyWhatSeemsNecessaryButSomeUnknown,
			NormalCSharpComplete = 2,
			NormalCSharpComplete_SpecialCodeInProgress = 3,
			NormalCSharpComplete_SpecialCodeCurrentlyOnlyWhatSeemsNecessaryButSomeUnknown = 3,
			AllCodeComplete = 4,
		}

		public enum SavedDataCompletedness
		{
			TODO = -1,
			UnknownOrUntested = 0,
			SavesAllKnownData = 1,
			MissingSomeData = 2,
			MissingMuchData = 3,
			MissingMostData = 4,
		}

		public enum LoadedDataSucess
		{
			TODO = -1,
			UnknownOrUntested = 0,
			AllDataWorksSoFar = 1,
			SomeDataFails = 2,
			MostDataFails = 3,
			AllDataFails = 4,
		}

		public enum ApplicationSucess
		{
			TODO = -1,
			UnknownOrUntested = 0,
			RegistersCorrectlySoFar = 0,
			DoesntRegisterWhenMain = 1,
			SometimesDoesntRegister = 2,
			UsuallyDoesntRegister = 3,
			NeverRegisters = 4,
		}

		public enum Additional
		{
			TODO = -2,
			Unknown = -1,
			None = 0,
			//Add additional non-flag-type options here
		}

		//Note: Normal C# is code excluding Reflection, Reflection.Emit,
		//Activator, Serialization.FormatterServices, other languages, MSIL, etc.

		[Flags]
		public enum Uses
		{
			NoneSoFar = -5,
			SpecialUnusable = -4,
			FailsWithError = -3,
			WorksButHasNoUse = -2,
			TODO = -1,
			Unknown = 0,
			/// <summary>The class stores some data, and so can be used for data storage</summary>
			NormalDataStorage = 1<<0,
			/// <summary>The class derives from SerComponent&lt;TSer, TNonSer&gt;, and can be used to save and load a component</summary>
			SerComponent = 1<<1,
			/// <summary>The class derives from SerComponent&lt;TSer, TNonSer&gt;, and corresponds to a type that can be attached to GameObjects like most components (eg. not Transform)</summary>
			UseableForGeneralSaveNode = 1<<2,
			//Add more uses here
		}

		private static bool Add<TSer, TNonSer>(ISaveableEquiv<TSer, TNonSer> inst)
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			if (inst == null) throw new ArgumentNullException("inst");

			Entry<TSer, TNonSer> regInfo;
			try {
				regInfo = inst.RegInfo;
				if (regInfo == null) {
					throw new NotImplementedException("go to catch");
				} else {
					var dictEntry = regInfo.AsDictEntry();
					if (!SaveableEquivTypes.ContainsEntry(dictEntry)) {
						SaveableEquivTypes.Add(regInfo.AsDictEntry());
						return true;
					}
				}
			} catch (NotImplementedException) {
				Debug.LogWarning("ISaveableEquiv type '" + inst.GetType() + "' does not provide SaveableEquiv registry info.");
			}
			return false;
		}

		public abstract class Entry
		{
			public readonly string SerTypeName;
			public readonly string NonSerTypeName;
			public readonly Type SerType;
			public readonly Type NonSerType;
			public abstract ClassStatus UntypedStatus { get; }
			internal Entry(string serTypeName, string nonSerTypeName, Type serType, Type nonSerType) {
				if (serTypeName    == null) throw new ArgumentNullException("SerTypeName"   );
				if (nonSerTypeName == null) throw new ArgumentNullException("NonSerTypeName");
				if (serType        == null) throw new ArgumentNullException("SerType"       );
				if (nonSerType     == null) throw new ArgumentNullException("NonSerType"    );
				this.SerTypeName    = serTypeName;
				this.NonSerTypeName = nonSerTypeName;
				this.SerType        = serType;
				this.NonSerType     = nonSerType;
			}
			/// <summary>
			/// Returns a MultiKeyDictEntry of the format "Key1 = SerTypeName, Key2 = NonSerTypeName, Key3 = SerType, Key4 = NonSerType, Value = UntypedStatus"
			/// </summary>
			public MultiKeyDictEntry<string, string, Type, Type, ClassStatus> AsDictEntry() {
				return MultiKeyDictEntry.Create(
					this.SerTypeName,
					this.NonSerTypeName,
					this.SerType,
					this.NonSerType,
					this.UntypedStatus
				);
			}
		}

		public sealed class Entry<TSer, TNonSer> : Entry
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			public readonly ClassStatus<TSer, TNonSer> Status;
			public override ClassStatus UntypedStatus { get { return Status; } }
			public Entry(string serTypeName, string nonSerTypeName, ClassStatus<TSer, TNonSer> status)
				: base(serTypeName, nonSerTypeName, typeof(TSer), typeof(TNonSer))
			{
				if (status == null) throw new ArgumentNullException("status");
				this.Status = status;
			}
		}


		public abstract class ClassStatus {
			public readonly CodeType               CodeType;
			public readonly CodeCompletedness      CodeCompletedness;
			public readonly SavedDataCompletedness SavedDataCompletedness;
			public readonly LoadedDataSucess       LoadedDataSucess;
			public readonly ApplicationSucess      ApplicationSucess;
			public readonly Uses                   Uses;
			public readonly Additional             Additional;
			public readonly string Notes;
			
			internal ClassStatus(
				CodeType               codeType,
				CodeCompletedness      codeCompletedness,
				SavedDataCompletedness savedDataCompletedness,
				LoadedDataSucess       loadedDataSucess,
				ApplicationSucess      applicationSucess,
				Uses                   uses,
				Additional             additional,
				string notes
			) {
				this.CodeType               = codeType;
				this.CodeCompletedness      = codeCompletedness;
				this.SavedDataCompletedness = savedDataCompletedness;
				this.LoadedDataSucess       = loadedDataSucess;
				this.ApplicationSucess      = applicationSucess;
				this.Uses                   = uses;
				this.Additional             = additional;
				this.Notes                  = notes;
			}

			public abstract ISaveableEquiv Convert(object nonSer);
		}

		public sealed class ClassStatus<TSer, TNonSer> : ClassStatus
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			public readonly Func<TNonSer, TSer> Converter;

			public ClassStatus(
				Func<TNonSer, TSer> converter,
				CodeType               codeType,
				CodeCompletedness      codeCompletedness,
				SavedDataCompletedness savedDataCompletedness,
				LoadedDataSucess       loadedDataSucess,
				ApplicationSucess      applicationSucess,
				Uses                   uses,
				Additional             additional,
				string notes
			)
			: base(
				codeType,
				codeCompletedness,
				savedDataCompletedness,
				loadedDataSucess,
				applicationSucess,
				uses,
				additional,
				notes
			) {
				if (converter == null) throw new ArgumentNullException("converter");
				this.Converter = converter;
			}

			public override ISaveableEquiv Convert(object nonSer)
			{
				if (nonSer == null || nonSer is TNonSer) return Converter((TNonSer)nonSer);
				else throw new ArgumentException("Parameter nonSer is of type '" + nonSer.GetType() + "' instead of type '" + typeof(TNonSer) + "'.");
			}
		}


	}
}

