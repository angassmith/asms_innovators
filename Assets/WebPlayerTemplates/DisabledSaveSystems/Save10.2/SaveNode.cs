﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Save
{	
	[DisallowMultipleComponent]
	[DeliberatelyNonSerializable]
	public abstract class SaveNodeBase : MonoBehaviour
	{
		public static GameObject OrphanedGameObjectHolder = null;
		public static List<GameObject> OrphanedGameObjects = new List<GameObject>();

		public readonly Type ComponentDataType;

		///<summary>Whether this GameObject should be saved</summary>
		[HideInInspector]
		public bool ShouldSave = true;

		[HideInInspector]
		public string CustomID = Guid.NewGuid().ToString();

		[HideInInspector]
		public string SourcePrefabName;

		public SaveNodeBase(Type componentDataType)
		{
			this.ComponentDataType = componentDataType;
			
			//Unity doesn't allow blocking attaching prefabs here (not main thread), so it's done in Awake()
		}
		
		public ReadOnlyCollection<SaveNodeBase> Children {
			get {
				//Setup
				List<SaveNodeBase> childSNodes = new List<SaveNodeBase>();
				//Loop through child transforms
				Transform transform = this.gameObject.transform;
				foreach (Transform childTransform in transform) {
					//Extract child SaveNodeBase if there is one
					GameObject childGObj = childTransform.gameObject;
					SaveNodeBase childSNode = childGObj.GetComponent<SaveNodeBase>();
					if (childSNode != null) {
						childSNodes.Add(childSNode);
					}
				}
				//Return
				return childSNodes.AsReadOnly();
			}
		}
		public ReadOnlyCollection<SaveNodeBase> ChildrenSetToSave {
			get {
				//Setup
				List<SaveNodeBase> childSNodes = new List<SaveNodeBase>();
				//Loop through child transforms
				Transform transform = this.gameObject.transform;
				foreach (Transform childTransform in transform) {
					//Extract child SaveNodeBase if there is one and it is set to save
					GameObject childGObj = childTransform.gameObject;
					SaveNodeBase childSNode = childGObj.GetComponent<SaveNodeBase>();
					if (childSNode != null && childSNode.ShouldSave) {
						childSNodes.Add(childSNode);
					}
				}
				//Return
				return childSNodes.AsReadOnly();
			}
		}


#if UNITY_EDITOR
        public void Awake()
		{
			//Block attaching to prefabs
			PrefabType prefabType = PrefabUtility.GetPrefabType(this.gameObject);
			if (prefabType == PrefabType.Prefab || prefabType == PrefabType.ModelPrefab) {
				DestroyImmediate(this, true); //Unity counts components attached to prefabs as assets. Also note Destroy() is not allowed in the editor
				Debug.LogError("SaveNodes cannot be attached to prefabs" + ("" + new object() + new object()));
				return;
			}
		}
#endif

        public abstract IComponentData CallGetDataFromComponents(DelayedLogger logger);
		public abstract void CallCreateComponentsFromData(IComponentData cdata, DelayedLogger logger);

		public static SaveNodeSaveData GetSaveData(SaveNodeBase snode, DelayedLogger logger)
		{
			
			if (snode == null) {
				logger.Add(DelayedLogItem.CreateError("Null SaveNodeBase passed to SaveNodeBase.GetSaveData(). The passed SaveNodeBase was saved (as null) but will not be loaded."));
				return null;
			}
			
			//Setup & easy properties
			SaveNodeSaveData sdata = new SaveNodeSaveData(snode.GetType())
			{
				ShouldSave = snode.ShouldSave,
				SourcePrefabName = snode.SourcePrefabName,
				CustomID = snode.CustomID,
				GObjData = new SerGameObject(snode.gameObject),
			};
			
			//SavedMonos
			foreach (SavedMonoBehaviour smono in snode.gameObject.GetComponents<SavedMonoBehaviour>())
			{
				try {
					sdata.SavedMonos.Add(SavedMonoBehaviour.GetSaveData(smono, logger));
				} catch (Exception e) {
					logger.Add(DelayedLogItem.CreateError(
						  "The SavedMonoBehaviour of type '"
						+ (smono == null ? "null" : smono.GetType().ToString())
						+ "' could not be saved: "
						+ e.ToString(),
						snode.gameObject
					));
				}
			}
			
			//Components
			sdata.ComponentData = snode.CallGetDataFromComponents(logger);
			
			//Children
			foreach (SaveNodeBase childSNode in snode.ChildrenSetToSave)
			{
				SaveNodeSaveData childSNodeSData = GetSaveData(childSNode, logger);
				sdata.ChildrenData.Add(childSNodeSData);
			}
			
			return sdata;
		}

		public static void GetFromSaveDataAndApply(Transform parent, SaveNodeSaveData sdata, DelayedLogger logger)
		{
			if (sdata == null) {
				logger.Add(DelayedLogItem.CreateError("The SaveNodeSaveData passed to SaveNodeBase.GetFromSaveDataAndApply() was null. The SaveNode it was meant to contain could therefore not be loaded. (Note: the Context of this message is the parent Transform)", parent));
				return;
			}

			GameObject sourcePrefab;
			if (
				!string.IsNullOrEmpty(sdata.SourcePrefabName)
				&& SaveHostMono.Main.SaveNodeSourcePrefabs.Single(
					x => x != null && x.name == sdata.SourcePrefabName,
					out sourcePrefab
				)
				//If SourcePrefabName is not null/empty, and there is exactly one prefab with that name
				//(in the process get that one prefab).
			) {
				//If the SourcePrefabName identifies a valid prefab,
				//clone the prefab, create a new SaveNode attached to the clone,
				//and call the method that actually does stuff.
				GameObject gobj = Instantiate(sourcePrefab);
				SaveNodeBase snode = (SaveNodeBase)gobj.AddComponent(sdata.SaveNodeType);
				GetFromSaveDataAndApply(parent, snode, sdata, logger);
			}
			else
			{
				//Otherwise, create a new GameObject, create a new SaveNode attached to it,
				//and call the method that actually does stuff.
				GameObject gobj = new GameObject();
				SaveNodeBase snode = (SaveNodeBase)gobj.AddComponent(sdata.SaveNodeType);
				GetFromSaveDataAndApply(parent, snode, sdata, logger);
			}
		}

		
		public static void GetFromSaveDataAndApply(Transform parent, SaveNodeBase existing, SaveNodeSaveData sdata, DelayedLogger logger) {

			//Set parent (must be done before sdata.GObjData)
			existing.transform.SetParent(parent); //Don't care if the transform is modified, as it will be set later

			//Easy properties
			existing.ShouldSave = sdata.ShouldSave;
			existing.SourcePrefabName = sdata.SourcePrefabName;
			existing.CustomID = sdata.CustomID;
			GameObject existing_gameObject = existing.gameObject;
			sdata.GObjData.CopyOverNonSer(ref existing_gameObject);
			
			//SavedMonos
			foreach (SavedMonoSaveData smonoData in sdata.SavedMonos)
			{
				try {
					SavedMonoBehaviour.GetFromSaveData(existing, smonoData, logger);
				} catch (Exception e) {
					logger.Add(DelayedLogItem.CreateError(
						  "The SavedMonoBehaviour of type '"
						+ (smonoData == null ? "null" :
							(smonoData.SavedMonoType == null ? "null" : smonoData.SavedMonoType.ToString())
						)
						+ "' could not be loaded: "
						+ e.ToString(),
						existing.gameObject
					));
					continue;
				}
			}
			
			//Components
			existing.CallCreateComponentsFromData(sdata.ComponentData, logger);

			//Children
			foreach (SaveNodeSaveData childSData in sdata.ChildrenData)
			{
				GetFromSaveDataAndApply(existing.transform, childSData, logger);
			}
		}

		/// <summary>
		/// Destroys the GameObject the SaveNode is attached to, calls Kill() on any child SaveNodes that are set to save, and moves any child non-saveable GameObjects (no SaveNode component, or set to not save) to a new 'Orphaned GameObjects' GameObject.
		/// </summary>
		public void Kill()
		{
			if (this.transform != null)
			{
				foreach (Transform child in this.transform)
				{
					SaveNodeBase childSNode = child.GetComponent<SaveNodeBase>();
					if (childSNode != null)
					{
						if (childSNode.ShouldSave) {
							childSNode.Kill();
						} else {
							if (ReferenceEquals(OrphanedGameObjectHolder, null)) {
								OrphanedGameObjectHolder = new GameObject("Orphaned GameObjects (Non-Saved Children of Killed SaveNodes)");
							}
							OrphanedGameObjects.Add(child.gameObject);
							child.SetParent(OrphanedGameObjectHolder.transform, true);
						}
					}
				}
			}
			Destroy(this.gameObject);
		}
	
		public interface IComponentData
		{ }

		[System.Serializable]
		public sealed class SaveNodeSaveData
		{
			public bool ShouldSave;
			public string SourcePrefabName;
			public string CustomID;
			public Type SaveNodeType;
			public List<SavedMonoSaveData> SavedMonos = new List<SavedMonoSaveData>();
			public IComponentData ComponentData;
			public SerGameObject GObjData;
			public List<SaveNodeSaveData> ChildrenData = new List<SaveNodeSaveData>();
		
			public SaveNodeSaveData(Type saveNodeType) {
				this.SaveNodeType = saveNodeType;
			}
		}
	}

	[DisallowMultipleComponent]
	[DeliberatelyNonSerializable]
	public abstract class SaveNode<TComponentData> : SaveNodeBase
		where TComponentData : SaveNode<TComponentData>.ComponentDataBase, new()
	{
		[System.Serializable]
		public abstract class ComponentDataBase : IComponentData
		{ }

		public SaveNode(): base(typeof(TComponentData))
		{
			
		}
	
		public sealed override IComponentData CallGetDataFromComponents(DelayedLogger logger)
		{
			return GetDataFromComponents(logger);
		}
		public sealed override void CallCreateComponentsFromData(IComponentData cdata, DelayedLogger logger)
		{
			if (cdata == null) CreateComponentsFromData(null, logger);
			TComponentData castedCData = cdata as TComponentData;
			if (castedCData != null) {
				CreateComponentsFromData(castedCData, logger);
			} else {
				throw new ArgumentException("The cdata type '" + cdata.GetType().FullName + "' is not equal to, and does not inherit from, the TComponentData type '" + typeof(TComponentData).FullName + "'.");
			}
		}

		public abstract TComponentData GetDataFromComponents(DelayedLogger logger);
		public abstract void CreateComponentsFromData(TComponentData cdata, DelayedLogger logger);
	}
}



//MonoBehaviour methods (don't remove; could be useful in future, and a pain to write out/format)
//	public void Awake                          (                                         ) { }
//	public void FixedUpdate                    (                                         ) { }
//	public void LateUpdate                     (                                         ) { }
//	public void OnAnimatorIK                   (                                         ) { }
//	public void OnAnimatorMove                 (                                         ) { }
//	public void OnApplicationFocus             (                                         ) { }
//	public void OnApplicationPause             (                                         ) { }
//	public void OnApplicationQuit              (                                         ) { }
//	public void OnAudioFilterRead              (float[] data, int channels               ) { }
//	public void OnBecameInvisible              (                                         ) { }
//	public void OnBecameVisible                (                                         ) { }
//	public void OnCollisionEnter               (                                         ) { }
//	public void OnCollisionEnter2D             (                                         ) { }
//	public void OnCollisionExit                (                                         ) { }
//	public void OnCollisionExit2D              (                                         ) { }
//	public void OnCollisionStay                (                                         ) { }
//	public void OnCollisionStay2D              (                                         ) { }
//	public void OnConnectedToServer            (                                         ) { }
//	public void OnControllerColliderHit        (                                         ) { }
//	public void OnDestroy                      (                                         ) { }
//	public void OnDisable                      (                                         ) { }
//	public void OnDisconnectedFromServer       (                                         ) { }
//	public void OnDrawGizmos                   (                                         ) { }
//	public void OnDrawGizmosSelected           (                                         ) { }
//	public void OnEnable                       (                                         ) { }
//	public void OnFailedToConnect              (                                         ) { }
//	public void OnFailedToConnectToMasterServer(                                         ) { }
//	public void OnGUI                          (                                         ) { }
//	public void OnJointBreak                   (                                         ) { }
//	public void OnJointBreak2D                 (                                         ) { }
//	public void OnMasterServerEvent            (                                         ) { }
//	public void OnMouseDown                    (                                         ) { }
//	public void OnMouseDrag                    (                                         ) { }
//	public void OnMouseEnter                   (                                         ) { }
//	public void OnMouseExit                    (                                         ) { }
//	public void OnMouseOver                    (                                         ) { }
//	public void OnMouseUp                      (                                         ) { }
//	public void OnMouseUpAsButton              (                                         ) { }
//	public void OnNetworkInstantiate           (NetworkMessageInfo info                  ) { }
//	public void OnParticleCollision            (                                         ) { }
//	public void OnParticleTrigger              (                                         ) { }
//	public void OnPlayerConnected              (                                         ) { }
//	public void OnPlayerDisconnected           (                                         ) { }
//	public void OnPostRender                   (                                         ) { }
//	public void OnPreCull                      (                                         ) { }
//	public void OnPreRender                    (                                         ) { }
//	public void OnRenderImage                  (RenderTexture src, RenderTexture dest    ) { }
//	public void OnRenderObject                 (                                         ) { }
//	public void OnSerializeNetworkView         (BitStream stream, NetworkMessageInfo info) { }
//	public void OnServerInitialized            (                                         ) { }
//	public void OnTransformChildrenChanged     (                                         ) { }
//	public void OnTransformParentChanged       (                                         ) { }
//	public void OnTriggerEnter                 (                                         ) { }
//	public void OnTriggerEnter2D               (                                         ) { }
//	public void OnTriggerExit                  (                                         ) { }
//	public void OnTriggerExit2D                (                                         ) { }
//	public void OnTriggerStay                  (                                         ) { }
//	public void OnTriggerStay2D                (                                         ) { }
//	public void OnValidate                     (                                         ) { }
//	public void OnWillRenderObject             (                                         ) { }
//	public void Reset                          (                                         ) { }
//	public void Start                          (                                         ) { }
//	public void Update                         (                                         ) { }
