﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

namespace Save {

	[DeliberatelyNonSerializable]
	public abstract class SavedMonoBehaviour : MonoBehaviour
	{
		[NonSerialized]
		private static Dictionary<Type, Obj<List<FieldInfo>, List<PropertyInfo>>> InstancePFInfos = new Dictionary<Type, Obj<List<FieldInfo>, List<PropertyInfo>>>();
		

		//TODO: Specify to user the invalid member names for serialization (invalid due to hiding (shadowing) public and private members)
		
		protected SavedMonoBehaviour() {
			
		}

		//Serialization methods:
		//https://msdn.microsoft.com/en-us/library/system.runtime.serialization.formatterservices.aspx
		
		//	//MonoBehaviour methods
		//	public virtual void Awake() { }
		//	public virtual void FixedUpdate() { }
		//	public virtual void LateUpdate() { }
		//	public virtual void OnAnimatorIK() { }
		//	public virtual void OnAnimatorMove() { }
		//	public virtual void OnApplicationFocus() { }
		//	public virtual void OnApplicationPause() { }
		//	public virtual void OnApplicationQuit() { }
		//	public virtual void OnAudioFilterRead(float[] data, int channels) { }
		//	public virtual void OnBecameInvisible() { }
		//	public virtual void OnBecameVisible() { }
		//	public virtual void OnCollisionEnter() { }
		//	public virtual void OnCollisionEnter2D() { }
		//	public virtual void OnCollisionExit() { }
		//	public virtual void OnCollisionExit2D() { }
		//	public virtual void OnCollisionStay() { }
		//	public virtual void OnCollisionStay2D() { }
		//	public virtual void OnConnectedToServer() { }
		//	public virtual void OnControllerColliderHit() { }
		//	public virtual void OnDestroy() { }
		//	public virtual void OnDisable() { }
		//	public virtual void OnDisconnectedFromServer() { }
		//	public virtual void OnDrawGizmos() { }
		//	public virtual void OnDrawGizmosSelected() { }
		//	public virtual void OnEnable() { }
		//	public virtual void OnFailedToConnect() { }
		//	public virtual void OnFailedToConnectToMasterServer() { }
		//	public virtual void OnGUI() { }
		//	public virtual void OnJointBreak() { }
		//	public virtual void OnJointBreak2D() { }
		//	public virtual void OnMasterServerEvent() { }
		//	public virtual void OnMouseDown() { }
		//	public virtual void OnMouseDrag() { }
		//	public virtual void OnMouseEnter() { }
		//	public virtual void OnMouseExit() { }
		//	public virtual void OnMouseOver() { }
		//	public virtual void OnMouseUp() { }
		//	public virtual void OnMouseUpAsButton() { }
		//	public virtual void OnNetworkInstantiate(NetworkMessageInfo info) { }
		//	public virtual void OnParticleCollision() { }
		//	public virtual void OnParticleTrigger() { }
		//	public virtual void OnPlayerConnected() { }
		//	public virtual void OnPlayerDisconnected() { }
		//	public virtual void OnPostRender() { }
		//	public virtual void OnPreCull() { }
		//	public virtual void OnPreRender() { }
		//	public virtual void OnRenderImage(RenderTexture src, RenderTexture dest) { }
		//	public virtual void OnRenderObject() { }
		//	public virtual void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) { }
		//	public virtual void OnServerInitialized() { }
		//	public virtual void OnTransformChildrenChanged() { }
		//	public virtual void OnTransformParentChanged() { }
		//	public virtual void OnTriggerEnter() { }
		//	public virtual void OnTriggerEnter2D() { }
		//	public virtual void OnTriggerExit() { }
		//	public virtual void OnTriggerExit2D() { }
		//	public virtual void OnTriggerStay() { }
		//	public virtual void OnTriggerStay2D() { }
		//	public virtual void OnValidate() { }
		//	public virtual void OnWillRenderObject() { }
		//	public virtual void Reset() { }
		//	//Start() and Update() are deliberately omitted

		public void CheckForSaveNode(Action<SavedMonoBehaviour> destroyer)
		{
			if (destroyer == null) throw new ArgumentNullException("destroyer");
			if (this.gameObject.GetComponent<SaveNodeBase>() == null) {
				destroyer(this);
				throw new InvalidOperationException("SavedMonoBehaviours cannot be added to GameObjects that do not have a SaveNode attached");
			}
		}
		
		public void Awake()
		{
			CheckForSaveNode(Destroy);
			OnAwake();
		}
		public void Start()  {
			if (SaveHostMono.Main.State == SaveHostMono.SavingState.InFrameAfterLoad) {
				OnLoadStart(); 
			} else {
				NormalStart();
			}
			CommonStart();
		}
		public void Update() {
			if (SaveHostMono.Main.State == SaveHostMono.SavingState.WillSaveAfterCurrentFrame) {
				PreSaveUpdate();
			} else {
				NormalUpdate();
			}
			CommonUpdate();
		}

		/// <summary>Start(), when not loading</summary>
		public virtual void NormalStart() { }
		/// <summary>Start(), after loading</summary>
		public virtual void OnLoadStart() { }
		/// <summary>Start(), after NormalStart() and OnLoadStart()</summary>
		public virtual void CommonStart() { }
		/// <summary>Update(), when not about to save</summary>
		public virtual void NormalUpdate() { }
		/// <summary>Update(), in the frame immediately before saving</summary>
		public virtual void PreSaveUpdate() { }
		/// <summary>Update(), after NormalUpdate() and PreSaveUpdate()</summary>
		public virtual void CommonUpdate() { }
		/// <summary>Awake(), after some custom SavedMonoBehaviour initialization code</summary>
		public virtual void OnAwake() { }
		
		/// <summary>Called immediately before retrieving the saved-mono. Should not modify other saved GameObjects, SavedMonoBehaviours or Components</summary>
		public virtual void ImmediatelyBeforeSave() { }
		/// <summary>Called immediately after creating the saved-vars. Should not rely on other saved GameObjects, SavedMonoBehaviours or Components existing</summary>
		public virtual void ImmediatelyAfterLoad() { }

		private static void SetupInstancePFInfos(Type smonoType)
		{
			const BindingFlags instanceBindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

			List<FieldInfo>    singleTypeInstanceFieldInfos    = new List<FieldInfo   >();
			List<PropertyInfo> singleTypeInstancePropertyInfos = new List<PropertyInfo>();

			//Get instance field infos
			foreach (FieldInfo finfo in smonoType.GetFields(instanceBindingFlags)) {
				if (!finfo.IsDefined(typeof(NonSavedFieldAttribute), false))
				{
					singleTypeInstanceFieldInfos.Add(finfo);
				}
			}

			//Get instance property infos
			foreach (PropertyInfo pinfo in smonoType.GetProperties(instanceBindingFlags))
			{
				MethodInfo[] paccessors = pinfo.GetAccessors(true);
				if (paccessors.Length > 0 && paccessors[0] != null && !paccessors[0].IsStatic) {
					if (pinfo.IsDefined(typeof(SavedPropertyAttribute), false))
					{
						singleTypeInstancePropertyInfos.Add(pinfo);
					}
				} //else the property is static or invalid, both of which are ignored
			}

			InstancePFInfos.Add(smonoType, Obj.Create(singleTypeInstanceFieldInfos, singleTypeInstancePropertyInfos));
			// ^ This is at the end to avoid it running if an exception is thrown
		}

		public static SavedMonoSaveData GetSaveData(SavedMonoBehaviour smono, DelayedLogger logger)
		{
			return GetSaveData(smono, false, logger);
		}

		private static SavedMonoSaveData GetSaveData(SavedMonoBehaviour smono, bool isValidating, DelayedLogger logger)
		{
			if (smono == null) return null;

			//Trigger custom stuff
			if (!isValidating) smono.ImmediatelyBeforeSave();

			//Setup
			Type smonoType = smono.GetType();
			SavedMonoSaveData sdata = new SavedMonoSaveData(smonoType);

			//Setup PFInfos if they haven't already been setup
			if (!InstancePFInfos.ContainsKey(smonoType)) SetupInstancePFInfos(smonoType);

			//Get values of fields
			foreach (FieldInfo finfo in InstancePFInfos[smonoType].Item1)
			{
				//TODO: Error handling (using logger.Add())
				sdata.SavedFields.Add(finfo.Name, finfo.GetValue(smono));
			}


			//Get values of properties
			foreach (PropertyInfo pinfo in InstancePFInfos[smonoType].Item2)
			{
				//TODO: Error handling (using logger.Add())
				sdata.SavedProperties.Add(pinfo.Name, pinfo.GetValue(smono, null));
			}

			return sdata;
		}

		public static SavedMonoBehaviour GetFromSaveData(SaveNodeBase snode, SavedMonoSaveData sdata, DelayedLogger logger)
		{
			if (sdata == null) return null;

			//Setup
			Type smonoType = sdata.SavedMonoType;

			//Get new SavedMonoBehaviour
			SavedMonoBehaviour smono = (SavedMonoBehaviour)snode.gameObject.AddComponent(sdata.SavedMonoType);

			//Setup PFInfos if they haven't already been setup
			if (!InstancePFInfos.ContainsKey(smonoType)) SetupInstancePFInfos(smonoType);
			
			//Set values of fields
			foreach (FieldInfo finfo in InstancePFInfos[smonoType].Item1)
			{
				object val;
				if (sdata.SavedFields.TryGetValue(finfo.Name, out val)) {
					//TODO?: Error handling (using logger.Add())
					finfo.SetValue(smono, val);
				} else {
					logger.Add(DelayedLogItem.CreateError(
						"The saved data was missing the field '" + finfo.Name
						+ "' for a SavedMonoBehaviour of type '" + smono.GetType().FullName
						+ "' attached to gameObject '" + smono.gameObject
						+ "'."
					));
				}
			}
			
			//Call property setters with values from data
			foreach (PropertyInfo pinfo in InstancePFInfos[smonoType].Item2)
			{
				object val;
				if (sdata.SavedProperties.TryGetValue(pinfo.Name, out val))
				{
					Action<Exception> exHandler = (e) => {
						logger.Add(DelayedLogItem.CreateError("Failed to set the property '" + pinfo.Name + "' on SavedMonoBehaviour of type '" + smono.GetType().FullName + "': " + e.ToString()));
					};

					try {
						pinfo.SetValue(smono, val, null);
					}
					catch (ArgumentException             e) { exHandler(e); }
					catch (MethodAccessException         e) { exHandler(e); }
					catch (TargetException               e) { exHandler(e); }
					catch (TargetInvocationException     e) { exHandler(e); }
					catch (TargetParameterCountException e) { exHandler(e); }
				}
				else
				{
					logger.Add(DelayedLogItem.CreateError("The saved data was missing the saved property '" + pinfo.Name + "' for a SavedMonoBehaviour of type '" + smono.GetType().FullName + "' attached to gameObject '" + smono.gameObject + "'."));
				}
			}
			
			//Trigger custom stuff
			smono.ImmediatelyAfterLoad();
			
			return smono;
		}



		

#if UNITY_EDITOR

		public void ValidateSerialization(DelayedLogger logger) {
			//	//Setup
			//	Type thisType = this.GetType();
			
			//Setup
			BinaryFormatter bf = new BinaryFormatter();

			logger.Add(DelayedLogItem.CreateMessage("<i><color=blue>Getting save-data for SavedMonoBehaviour</color></i>"));
			SavedMonoSaveData sdata = GetSaveData(this, true, logger);

			//Attempt to serialize each field/property
			logger.Add(DelayedLogItem.CreateMessage("<i><color=blue>Attempting to serialize fields/properties stored in the save-data</color></i>"));
			foreach (var propOrField in sdata.SavedFields.Concat(sdata.SavedProperties))
			{
				if (propOrField.Value == null) {
					logger.Add(DelayedLogItem.CreateMessage("<i><color=grey>Property or field '" + propOrField.Key + "' was null and so could not be validated</color></i>"));
					continue;
					//Can't do anything more than this (like serialize a blank instance), as the type is unknown
				}

				try {
					using (MemoryStream stream = new MemoryStream()) { //Apparently it's just as efficient to create new MemoryStreams as to reuse existing ones.
						bf.Serialize(stream, propOrField.Value);
					}
				} catch (SerializationException e) {
					logger.Add(DelayedLogItem.CreateError("<i><color=red>Property or field '" + propOrField.Key + "' could not be serialized</color></i>:\r\n" + e.ToString()));
				}
			}
			
			//Attempt to serialize the whole save-data
			try {
				using (MemoryStream stream = new MemoryStream()) {
					bf.Serialize(stream, sdata);
				}
			} catch (SerializationException e) {
				logger.Add(
					DelayedLogItem.CreateError(
						"<i><color=red>"
						+ "The full save-data instance could not be serialized:</color></i>\r\n"
						+ e.ToString()
					)
				);
			}
		}

#endif

	}

	[System.Serializable]
	public sealed class SavedMonoSaveData
	{
		public readonly Type SavedMonoType;
		public Dictionary<string, object> SavedFields;
		public Dictionary<string, object> SavedProperties;
		public SavedMonoSaveData(Type savedMonoType, Dictionary<string, object> savedFields = null, Dictionary<string, object> savedProperties = null) {
			this.SavedMonoType = savedMonoType;
			this.SavedFields = savedFields ?? new Dictionary<string, object>();
			this.SavedProperties = savedProperties ?? new Dictionary<string, object>();
		}
	}
}

//*/






/* Random other code

public class LikeabilityScale
{
	public enum ChangeType
	{
		MadeFriends,
		HitPerson,

	}

	public static event EventHandler<LikeabilityChangeEventArgs> StudentLikeabilityChanged;

	public static int StudentLikeability { get; private set; }
	public static int TeacherLikeability { get; private set; }

	public static void Change(ChangeType ctype)
	{
		switch (ctype)
		{
			case ChangeType.MadeFriends: StudentLikeability += 5; if (StudentLikeabilityChanged != null) StudentLikeabilityChanged(null, new LikeabilityChangeEventArgs(StudentLikeability)); break;
			
		}
	}


	void Start()
	{
		LikeabilityScale.StudentLikeabilityChanged += (s, e) => {

		};
	}

	void StudentLikeabilityChanged(object sender, LikeabilityChangeEventArgs e)
	{
		
	}

}


class LikeabilityFungus
{
	
}


public class LikeabilityChangeEventArgs : EventArgs
{
	public int NewValue;

	public LikeabilityChangeEventArgs(int newVal) {
		this.NewValue = newVal;
	}
}

*/
