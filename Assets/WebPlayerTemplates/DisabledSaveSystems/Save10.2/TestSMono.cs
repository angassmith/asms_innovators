﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Save;

[DeliberatelyNonSerializable]
public class TestSMono : SavedMonoBehaviour
{

	#region SaveProperties

	#endregion

	public bool TalkedToTeacher;
	public bool PickedUpTheItem;

	//public int frame;

	public override void NormalStart()
	{
		
	}
	public override void OnLoadStart()
	{
		
	}
	public override void CommonStart()
	{
		
	}
	
	
	
	public override void NormalUpdate()
	{
		
	}
	public override void PreSaveUpdate()
	{
		
	}
	public override void CommonUpdate()
	{
		//frame++;

		//if (frame == 500) SaveHostMono.Main.SaveAfterNextFrame();
	}
	
	
#region Save preparation and application methods
	public override void ImmediatelyBeforeSave()
	{
		
	}
	public override void ImmediatelyAfterLoad()
	{
		
	}
#endregion
	
}
