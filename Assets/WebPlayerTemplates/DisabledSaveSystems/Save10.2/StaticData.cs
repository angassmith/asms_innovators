﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


namespace Save
{
	/// <summary>
	/// Note: Derived types can never be properly instantiated, so should have no autoinitialized instance fields (eg. public int a = 5;).
	/// </summary>
	[System.Serializable]
	public abstract class StaticSaveData  {

		private static HashSet<StaticSaveData> StaticDataSeeds = null;

		/// <summary>
		/// This will throw an exception if called (UninstantiableClass is used to discourage calling,
		/// and make sure this is read).
		/// Descended classes will be instantiated automatically using
		/// System.Runtime.Serialization.FormatterServices.GetUninitializedObject(Type type),
		/// which avoids calling any constructors or field initializers.
		/// Calling constructors should be protected, and take exactly one parameter, of type UninstantiableClass
		/// </summary>
		protected StaticSaveData(UninstantiableClass none)
		{
			throw new InvalidOperationException("StaticSaveData descendant classes may never be instantiated through normal means (constructors)");
		}

		/// <summary>
		/// Should copy static fields of another class to instance fields of the deriving class. The other class may be a parent nested class.
		/// <para/>
		/// logger should be used to log non-fatal errors.
		/// </summary>
		public abstract void StoreStaticData(DelayedLogger logger);
		/// <summary>
		/// Should copy instance fields of the deriving class to static fields of another class. The other class may be a parent nested class
		/// <para/>
		/// logger should be used to log non-fatal errors.
		/// </summary>
		public abstract void ApplyStaticData(DelayedLogger logger);

		/// <summary>
		/// Sets up uninitialised instances of all descendants of StaticDataType. Returns false if this has already been done.
		/// </summary>
		public static bool SetupStaticDataSeeds()
		{
			if (StaticDataSeeds != null) return false;

			StaticDataSeeds = new HashSet<StaticSaveData>();
			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (Type type in assembly.GetTypes())
				{
					if (
						   type != null
						&& type.IsClass
						&& !type.IsAbstract
						&& type.IsSubclassOf(typeof(StaticSaveData))
					) {
						StaticDataSeeds.Add(
							(StaticSaveData)
							System.Runtime.Serialization.FormatterServices.GetUninitializedObject(type)
						);
					}
				}
			}

			return true;
		}

		public static List<StaticSaveData> GetAllStaticData(DelayedLogger logger)
		{
			SetupStaticDataSeeds();

			List<StaticSaveData> storedStaticData = new List<StaticSaveData>();
			foreach (StaticSaveData staticData in StaticDataSeeds)
			{
				staticData.StoreStaticData(logger);
				storedStaticData.Add(staticData);
			}

			return storedStaticData;
		}

		public static void ApplyAllStaticData(List<StaticSaveData> storedStaticData, DelayedLogger logger)
		{
			foreach (StaticSaveData storedStaticDataItem in storedStaticData)
			{
				storedStaticDataItem.ApplyStaticData(logger);
			}
		}


		/// <summary>
		/// Returns true if the target object is not null and is of the same type as the current StaticSaveData
		/// </summary>
		public sealed override bool Equals(object obj)
		{
			return obj != null && this.GetType() == obj.GetType();
		}

		/// <summary>
		/// Returns the hash code of the current StaticSaveData's type
		/// </summary>
		public sealed override int GetHashCode()
		{
			return this.GetType().GetHashCode();
		}

		/// <summary>
		/// Returns "StaticSaveData of type '" + this.GetType().FullName + "'"
		/// </summary>
		public sealed override string ToString()
		{
			return "StaticSaveData of type '" + this.GetType().FullName + "'";
		}
	}
}
