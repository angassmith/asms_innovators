﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Threading;
using UnityEngine.SceneManagement;
using System.Collections.ObjectModel;

//TODO: Use code from RepeatableThread<T1, T2> (using WaitOne() instead of Sleep(), and actually starting the thread)
//in other RepeatableThread variants

namespace Save
{
	public static class SaveHost
	{
		//Save path
		//Application.persistentDataPath can only be gotten from the main thread, so savePath cannot be set here
		private static string _savePath = SaveConstants.pathUnset;
		public static string SavePath
		{
			get {
				return _savePath;
			}
			set {
				if (inMainThreadSection || isSaving) {
					throw saveRunningException;
				} else {
					_savePath = value;
				}
			}
		}

		private static bool _isSaving = false;
		public static bool isSaving { get { return _isSaving; } }
		private static bool inMainThreadSection = false;

		private static RepeatableThread<string, MemoryStream> SaveToDiskWorker = new RepeatableThread<string, MemoryStream>(SaveSelectedToDisk);

		private static readonly SaveInProgressException saveRunningException = new SaveInProgressException("Properties and methods may not be accessed while saving (except GetSavingStatus())");
		
		//	private static void BlockIfPathUnset()
		//	{
		//		if (SavePath == SaveConstants.pathUnset) {
		//			throw new Exception("SavePath has not been set. SaveHostMono must be attached to a GameObject for SavePath to be set");
		//		}
		//	}


		public static void SaveAll()
		{
			if (inMainThreadSection) {
				throw saveRunningException;
			}
			inMainThreadSection = true;
			if (isSaving) {
				inMainThreadSection = false;
				throw saveRunningException;
			}
			_isSaving = true;
			if (_savePath == SaveConstants.pathUnset) {
				inMainThreadSection = false;
				throw new SaveSetupException("savePath has not been set. SaveHostMono must be attached to a GameObject for savePath to be set");
			}

			try
			{
				Debug.Log("Saving...");
				DelayedLogger logger = new DelayedLogger();

				//Select game objects
				ReadOnlyCollection<SaveNodeBase> rootGameObjs = SaveHelper.GetRootSaveNodesSetToSaveInCurrentScene();

				//Access/Convert/Copy
				SavedGame savedGame = GetAllSaveData(rootGameObjs, logger);

				//Apply delayed-logged items (log/log warning/log error/throw)
				logger.ApplyAllTogether();

				//Serialize to prevent modifications
				BinaryFormatter bf = new BinaryFormatter();
				MemoryStream memstream = new MemoryStream();
				bf.Serialize(memstream, savedGame);

				//Write to disk (in new thread)
				StartSaveSelectedToDiskWorker(_savePath, memstream);
			}
			catch (Exception e)
			{
				inMainThreadSection = false;
				throw new SaveException("Saving failed due to an unknown error: '" + e + "'", e); //Needs checking
			}
			inMainThreadSection = false;
		}

		private static SavedGame GetAllSaveData(ReadOnlyCollection<SaveNodeBase> rootSNodes, DelayedLogger logger)
		{
			//Setup
			List<SaveNodeBase.SaveNodeSaveData> saveNodesData = new List<SaveNodeBase.SaveNodeSaveData>();

			//Get save data for all root SaveNodes
			//This recurses into the child SaveNodes
			foreach (SaveNodeBase rootSNode in rootSNodes)
			{
				saveNodesData.Add(SaveNodeBase.GetSaveData(rootSNode, logger));
			}

			//Get static data
			List<StaticSaveData> staticData = StaticSaveData.GetAllStaticData(logger);

			return new SavedGame(saveNodesData, staticData);
		}


		private static void StartSaveSelectedToDiskWorker(string savePath, MemoryStream savedGame)
		{
			SaveToDiskWorker.RunTask(savePath, savedGame);
		}

		private static void SaveSelectedToDisk(string savePath, MemoryStream savedGame) {
			try {
				Debug.Log("SavePath = " + savePath);
				Directory.CreateDirectory(savePath);
				using (FileStream savedGameFile = File.Open(savePath + SaveConstants.saveGameFileName, FileMode.Create, FileAccess.Write))
				{
					savedGame.WriteTo(savedGameFile);
				}

			} catch (Exception e) {
				Debug.LogException(new SaveException("Saving failed (the asynchronous/multithreaded part): " + e.Message, e));
			}
		}

		public static void TryFinishSaving()
		{
			if (isSaving && !inMainThreadSection && !SaveToDiskWorker.HalfRunning)
			{
				RefreshSavePath();
				_isSaving = false;
				Debug.Log("Finished Saving.");
			}
		}

		public static void RefreshSavePath()
		{
			int newestSaveNum = GetNewestSaveNum();
			_savePath = Application.persistentDataPath + SaveConstants.saveFolderName + SaveConstants.saveGameFolderPrefix + (newestSaveNum + 1);

			Debug.Log("SavePath = '" + _savePath);
		}

		public static int GetNewestSaveNum()
		{
			int newestDirNum = 0;
            Directory.CreateDirectory(Application.persistentDataPath + SaveConstants.saveFolderName);
            string[] dirs = Directory.GetDirectories(Application.persistentDataPath + SaveConstants.saveFolderName, SaveConstants.saveGameFolderPrefixNoSlash + "*");
			foreach (string dir in dirs)
			{
				try {
					int dirNum = int.Parse(dir.Substring(dir.Replace("\\", "/").LastIndexOf(SaveConstants.saveGameFolderPrefix) + 8));
					if (dirNum > newestDirNum)
					{
						newestDirNum = dirNum;
					}
				} catch { }
			}
			return newestDirNum;
		}
	}
}

//*/