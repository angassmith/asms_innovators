﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Save
{

	/// <summary>
	/// Marker for custom classes that can be saved. Not currently mandatory for anything, but should still be inherited from wherever possible
	/// </summary>
	public interface ISaveable
	{ }
	

	/// <summary>
	/// Non-generic base of ISaveableEquiv&lt;TSer, TNonSer&gt;. Should not be directly implemented (use the generic version instead)
	/// </summary>
	public interface ISaveableEquiv : ISaveable
	{
		SaveEquivReg.Entry UntypedRegInfo { get; }
		//	bool HasInited { get; }
		Type SerType { get; }
		Type NonSerType { get; }
		object CreateBlankUntypedNonSer();
		void CopyFromUntypedNonSer(object nonSerSource);
		void CopyOverUntypedNonSer(object nonSerDest);
		object CastToUntypedNonSer();
	}

	/// <summary>
	/// Defines (and implements through extension methods) methods for saveable types that convert to and from a non-saveable type. Where no other base class is needed, SaveableEquiv&lt;TSer, TNonSer&gt; should be used instead
	/// </summary>
	/// <typeparam name="TSer">The type that is implementing this interface; the saveable type</typeparam>
	/// <typeparam name="TNonSer">The type that the saveable type can be converted to and from; the non-saveable type</typeparam>
	public interface ISaveableEquiv<TSer, TNonSer>
		: ISaveableEquiv
		where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
	{
		SaveEquivReg.Entry<TSer, TNonSer> RegInfo { get; }
		void CopyFromNonNullNonSer(TNonSer nonSerSource);
		void CopyOverNonNullNonSer(ref TNonSer nonSerDest);
		TNonSer CreateBlankNonSer();
	}


	/// <summary>
	/// Base class for saveable classes that convert to and from a non-saveable type.
	/// Where another base class is needed, ISaveableEquiv&lt;TSer, TNonSer&gt; can be used instead
	/// <para/>
	/// Defines and implements constructors, a SerType and NonSerType fields, and casts
	/// </summary>
	/// <typeparam name="TSer">The type that derives from this class; the saveable type</typeparam>
	/// <typeparam name="TNonSer">The type that the saveable type can be converted to and from; the non-saveable type</typeparam>
	[System.Serializable]
	public abstract class SaveableEquiv<TSer, TNonSer>
		: ISaveableEquiv<TSer, TNonSer>
		where TSer : SaveableEquiv<TSer, TNonSer>, new()
	{
		private readonly Type BackingSerType;
		private readonly Type BackingNonSerType;

		public Type SerType { get { return BackingSerType; } }
		public Type NonSerType { get { return BackingNonSerType; } }
		
		public abstract SaveEquivReg.Entry<TSer, TNonSer> RegInfo { get; }
		SaveEquivReg.Entry ISaveableEquiv.UntypedRegInfo { get { return RegInfo; } }

		protected SaveableEquiv(VoidClass none)
		{
			this.BackingSerType = typeof(TSer);
			this.BackingNonSerType = typeof(TNonSer);
			this.Validate();
		}
		protected SaveableEquiv(TNonSer nonSerSource) : this(VoidClass.Void)
		{
			this.CopyFromNonSer(nonSerSource);
		}

		public abstract void CopyFromNonNullNonSer(TNonSer nonSerSource);
		public abstract void CopyOverNonNullNonSer(ref TNonSer nonSerDest);
		public abstract TNonSer CreateBlankNonSer();

		object ISaveableEquiv.CreateBlankUntypedNonSer() { return CreateBlankNonSer(); }
		void ISaveableEquiv.CopyFromUntypedNonSer(object nonSerSource) {
			if (nonSerSource == null) throw new ArgumentNullException("nonSerSource");
			if (!(nonSerSource is TNonSer)) throw new ArgumentException("Object passed for parameter nonSerSource (type = '" + nonSerSource.GetType() + "', toString = '" + nonSerSource.ToString() + "') is not of type '" + typeof(TNonSer) + ".");
			CopyFromNonNullNonSer((TNonSer)nonSerSource);
		}
		void ISaveableEquiv.CopyOverUntypedNonSer(object nonSerDest) {
			if (nonSerDest == null) throw new ArgumentNullException("nonSerDest");
			if (!(nonSerDest is TNonSer)) throw new ArgumentException("Object passed for parameter nonSerSource (type = '" + nonSerDest.GetType() + "', toString = '" + nonSerDest.ToString() + "') is not of type '" + typeof(TNonSer) + ".");
			TNonSer typedNonSerDest = (TNonSer)nonSerDest;
			CopyOverNonNullNonSer(ref typedNonSerDest);
		}
		object ISaveableEquiv.CastToUntypedNonSer() {
			return this.CastToNonSer();
		}

		//Note: Adding non-extension methods messed up type argument inference for some reason (in SaveHelper.ConvertItems and related)

		//AFAIK these allow any derived class to be safely cast to and from it's equivalent.
		//The only potentially unsafe part is casting from SaveableEquiv<TSer, TNonSer> to TSer
		//(pseudo-implicitly when casting from TNonSer to TSer), but this is regulated (at runtime) in
		//the SaveableEquiv constructors so that TSer must derive from SaveableEquiv<TSer, TNonSer>.
		//Additionally, methods, etc. could throw exceptions, but that's normal
		/// <summary>Contains only "return ISaveableEquiv.CastToSer&lt;TSer, TNonSer&gt;(nonSerSource);"</summary>
		public static explicit operator SaveableEquiv<TSer, TNonSer>(TNonSer nonSerSource) {
			return SaveableEquivHelper.CastToSer<TSer, TNonSer>(nonSerSource);
		}
		/// <summary>Contains only "return ISaveableEquiv.CastToNonSer(serSource);"</summary>
		public static explicit operator TNonSer(SaveableEquiv<TSer, TNonSer> serSource) {
			return SaveableEquivHelper.CastToNonSer(serSource);
		}
	}

	public static class SaveableEquivHelper
	{

		//	// ---------------- Init ----------------
		//	
		//	public static void Init<TSer, TNonSer>(this ISaveableEquiv<TSer, TNonSer> serTarget)
		//		where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		//	{
		//		if (serTarget == null) {
		//			throw new ArgumentNullException("serTarget", "Cannot initialize a null ISaveableEquiv (of specified type " + typeof(TSer) + ").");
		//		}
		//		//TODO: Combine validate and init
		//		Validate(serTarget);
		//	
		//		SaveEquivReg.Entry regInfo;
		//		try {
		//			regInfo = serTarget.RegInfo;
		//			if (regInfo == null) {
		//				throw new NotImplementedException("go to catch");
		//			} else {
		//				var dictEntry = regInfo.AsDictEntry();
		//				if (!SaveEquivReg.SaveableEquivTypes.ContainsKey3(dictEntry.Key3)) {
		//					SaveEquivReg.SaveableEquivTypes.Add(regInfo.AsDictEntry());
		//				}
		//			}
		//		} catch (NotImplementedException) {
		//			Debug.LogWarning("ISaveableEquiv type '" + serTarget.GetType() + "' does not provide SaveableEquiv registry info.");
		//		}
		//		
		//	}
		//	
		//	// ---------------- Validate ----------------

		private static List<Type> ValidatedSaveableEquivTypes = new List<Type>();

		/// <summary>
		/// Validates that an ISaveableEquiv&lt;TSer, TNonSer&gt; has the correct SerType and NonSerType properties, and that its type derives from TSer and can be serialized (tested using a blank instance).
		/// </summary>
		/// <typeparam name="TSer"></typeparam>
		/// <typeparam name="TNonSer"></typeparam>
		/// <param name="serTarget"></param>
		public static void Validate<TSer, TNonSer>(this ISaveableEquiv<TSer, TNonSer> serTarget)
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
#if UNITY_EDITOR //Don't validate in proper build
			if (serTarget == null) {
				throw new ArgumentNullException("serTarget", "Cannot validate a null ISaveableEquiv (of specified type " + typeof(TSer) + ").");
			}
			
			Type serTargetType = serTarget.GetType();

			//Validate instance-specific stuff
			if (serTarget.SerType != typeof(TSer)) {
				throw new SaveImplementationException("The ISaveableEquiv (of type " + serTargetType + ") is not valid, as it's SerType property does not match the TSer type argument.");
			}
			if (serTarget.NonSerType != typeof(TNonSer)) {
				throw new SaveImplementationException("The ISaveableEquiv (of type " + serTargetType + ") is not valid, as it's NonSerType property does not match the TNonSer type argument.");
			}

			//Validate type-specific stuff (only needs to be done once for each type)
			if (!ValidatedSaveableEquivTypes.Contains(serTargetType))
			{
				ValidatedSaveableEquivTypes.Add(serTargetType);

				//Check that serTarget's type inherits from TSer
				if (!(serTarget is TSer)) {
					throw new SaveImplementationException("The ISaveableEquiv (of type " + serTargetType + ") is not valid, as it's type does not equal or inherit from the type specified by the TSer type argument.");
				}

				//Attempt to serialize a blank object of type serTargetType
				//This means that even if serTarget has, for example, an object field with a
				//non-serializable type in it, it won't fail (i.e. this validates the *type*,
				//not the instance, so it is consistent when only validating each type once)
				object uninit = FormatterServices.GetUninitializedObject(serTargetType);
				BinaryFormatter bf = new BinaryFormatter();
				MemoryStream mem = new MemoryStream();
				try {
					bf.Serialize(mem, uninit);
				} catch (SerializationException e) {
					throw new SaveImplementationException("The ISaveableEquiv (of type " + serTargetType + ") is not valid, as when an attempt was made to serialize a blank instance of its type, the following exception was thrown: " + e.ToString());
				}
			}
#endif
		}
		
		// ---------------- Copy values between ser and non-ser ----------------

		/// <summary>Performs null checks (which throw ArgumentNullExceptions), then calls serDest.CopyFromNonNullNonSer(nonSerSource)</summary>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="Whatever serDest.CopyFromNonNullNonSer(nonSerSource) throws"></exception>
		public static void CopyFromNonSer<TSer, TNonSer>(this ISaveableEquiv<TSer, TNonSer> serDest, TNonSer nonSerSource)
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			if      (serDest == null && nonSerSource == null) throw new ArgumentNullException("serDest and nonSerSource", "Cannot copy a"  + " NULL" + " ISaveableEquiv (of specified type " + typeof(TSer).FullName +                                                 ") FROM a"  + " NULL" + " equivalent object (of specified type " + typeof(TNonSer).FullName +                                                      ")");
			else if (serDest == null                        ) throw new ArgumentNullException("serDest"              , "Cannot copy a"  + " NULL" + " ISaveableEquiv (of specified type " + typeof(TSer).FullName +                                                 ") FROM an" +           " equivalent object (of specified type " + typeof(TNonSer).FullName + ", actual type " + nonSerSource.GetType().FullName + ")");
			else if (                   nonSerSource == null) throw new ArgumentNullException("nonSerSource"         , "Cannot copy an" +           " ISaveableEquiv (of specified type " + typeof(TSer).FullName + ", actual type " + serDest.GetType().FullName + ") FROM a"  + " NULL" + " equivalent object (of specified type " + typeof(TNonSer).FullName +                                                      ")");
			else serDest.CopyFromNonNullNonSer(nonSerSource);
		}

		/// <summary>Performs null checks (which throw ArgumentNullExceptions), then calls serSource.CopyOverNonNullNonSer(nonSerDest)</summary>
		/// <exception cref="ArgumentNullException">serSource, nonSerDest, or both are null</exception>
		/// <exception cref="Whatever serSource.CopyOverNonNullNonSer(nonSerDest) throws"></exception>
		public static void CopyOverNonSer<TSer, TNonSer>(this ISaveableEquiv<TSer, TNonSer> serSource, ref TNonSer nonSerDest)
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			if      (serSource == null && nonSerDest == null) throw new ArgumentNullException("serSource and nonSerDest", "Cannot copy a"  + " NULL" + " ISaveableEquiv (of specified type " + typeof(TSer).FullName +                                                   ") OVER a"  + " NULL" + " equivalent object (of specified type " + typeof(TNonSer).FullName +                                                    ")");
			else if (serSource == null                      ) throw new ArgumentNullException("serSource"               , "Cannot copy a"  + " NULL" + " ISaveableEquiv (of specified type " + typeof(TSer).FullName +                                                   ") OVER an" +           " equivalent object (of specified type " + typeof(TNonSer).FullName + ", actual type " + nonSerDest.GetType().FullName + ")");
			else if (                     nonSerDest == null) throw new ArgumentNullException("nonSerDest"              , "Cannot copy an" +           " ISaveableEquiv (of specified type " + typeof(TSer).FullName + ", actual type " + serSource.GetType().FullName + ") OVER a"  + " NULL" + " equivalent object (of specified type " + typeof(TNonSer).FullName +                                                    ")");
			else serSource.CopyOverNonNullNonSer(ref nonSerDest);
		}

		// ---------------- Create ser from non-ser, and vice versa, throwing ArgumentNullExceptions ----------------

		/// <summary>If serSource is null, throws an exception, otherwise uses serSource.CreateBlankNonSer() to create a new TNonSer, and if the new TNonSer is not null, uses serSource.CopyOverNonSer() to copy values over it</summary>
		/// <exception cref="ArgumentNullException">serSource is null</exception>
		/// <exception cref="Whatever serSource.CreateBlankNonSer() or serSource.CopyOverNonNullNonSer(newNonSer) throws"></exception>
		public static TNonSer CreateNonSer<TSer, TNonSer>(ISaveableEquiv<TSer, TNonSer> serSource)
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			if (serSource == null) throw new ArgumentNullException("Cannot create an equivalent object (of specified type " + typeof(TNonSer).FullName + ") from a null ISaveableEquiv (of specified type " + typeof(TSer).FullName + ")");
			else {
				TNonSer newNonSer = serSource.CreateBlankNonSer();
				if (newNonSer != null) serSource.CopyOverNonNullNonSer(ref newNonSer);
				return newNonSer;
			}
		}

		//This converts directly from TNonSer to TSer (which is impossible
		//with proper casts, as the enclosing type must be involved),
		//so there's no need to worry about TSer not inheriting from
		//(I)SaveableEquiv<TSer, TNonSer>.
		/// <summary>If nonSerSource is null, throws an exception, otherwise creates a new TSer and uses newSer.CopyFromNonSer(nonSerSource) to copy values to it</summary>
		/// <exception cref="ArgumentNullException">nonSerSource is null</exception>
		/// <exception cref="Whatever new TSer() or serSource.CopyFromNonNullNonSer(newNonSer) throws"></exception>
		public static TSer CreateSer<TSer, TNonSer>(TNonSer nonSerSource)
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			if (nonSerSource == null) throw new ArgumentNullException("Cannot create an ISaveableEquiv (of specified type " + typeof(TSer).FullName + ") from a null equivalent object (of specified type " + typeof(TNonSer).FullName + ")");
			else {
				TSer newSer = new TSer();
				newSer.CopyFromNonNullNonSer(nonSerSource);
				return newSer;
			}
		}

		// ---------------- Create ser from non-ser, and vice versa, without throwing ArgumentNullExceptions ----------------

		/// <summary>Same as CreateNonSer(), but returns default(TNonSer) instead of throwing an exception if serSource is null</summary>
		public static TNonSer CastToNonSer<TSer, TNonSer>(this ISaveableEquiv<TSer, TNonSer> serSource)
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			if (serSource == null) return default(TNonSer);
			else {
				TNonSer newNonSer = serSource.CreateBlankNonSer();
				if (newNonSer != null) serSource.CopyOverNonSer(ref newNonSer);
				return newNonSer;
			}
		}

		//This one's a bit weird, because it is accessible through TNonSer,
		//but it is still compile-time-checked.
		//It converts directly from TNonSer to TSer (which is impossible
		//with proper casts, as the enclosing type must be involved),
		//so there's no need to worry about TSer not inheriting from
		//(I)SaveableEquiv<TSer, TNonSer>.
		/// <summary>Same as CreateSer(), but returns default(TNonSer) instead of throwing an exception if nonSerSource is null</summary>
		public static TSer CastToSer<TSer, TNonSer>(this TNonSer nonSerSource)
			where TSer : class, ISaveableEquiv<TSer, TNonSer>, new()
		{
			if (nonSerSource == null) return default(TSer);
			else {
				TSer newSer = new TSer();
				newSer.CopyFromNonNullNonSer(nonSerSource);
				return newSer;
			}
		}

		// ---------------- Shortcuts ----------------

		public static InvalidOperationException CreateTypeNotInstantiableException(string typeName)
		{
			return new InvalidOperationException("New objects of type " + typeName + " cannot be created outside Unity code (" + typeName + " has no public constructors)");
		}

		public static InvalidOperationException CreateMembersOnlySettableInCtorException(string typeName)
		{
			return new InvalidOperationException("Some properties/methods/fields in type " + typeName + " cannot be set outside the constructor");
		}
	}
}

//*/