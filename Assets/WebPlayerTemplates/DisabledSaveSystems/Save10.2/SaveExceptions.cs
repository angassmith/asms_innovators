﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Runtime.Serialization;

namespace Save
{
	public class SaveException : Exception
	{
		public SaveException() { }
		public SaveException(string message) : base(message) { }
		public SaveException(string message, Exception innerException) : base(message, innerException) { }
		protected SaveException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}

	public class SerComponentTypeException : SaveException
	{
		public readonly Type CorrectSerType;
		public readonly Type CorrectNonSerType;
		public readonly Type CorrectBaseSerType;
		public readonly Type PassedSerType;
		public readonly Type PassedNonSerType;
		public SerComponentTypeException(Type correctSerType, Type correctNonSerType, Type passedSerType, Type passedNonSerType, string message = "")
			: base((string.Format("[CorrectSerType = {0}, CorrectNonSerType = {1}, CorrectBaseSerType = {2} PassedSerType = {3}, PassedNonSerType = {4}] ", correctSerType, correctNonSerType, "null", passedSerType, passedNonSerType) + message).Trim())
		{
			this.CorrectSerType    = correctSerType;
			this.CorrectNonSerType = correctNonSerType;
			this.PassedSerType     = passedSerType;
			this.PassedNonSerType  = passedNonSerType;
		}
		public SerComponentTypeException(Type correctSerType, Type correctNonSerType, Type passedSerType, Type passedNonSerType, Exception innerException, string message = "")
			: base((string.Format("[CorrectSerType = {0}, CorrectNonSerType = {1}, CorrectBaseSerType = {2} PassedSerType = {3}, PassedNonSerType = {4}] ", correctSerType, correctNonSerType, "null", passedSerType, passedNonSerType) + message).Trim(), innerException)
		{
			this.CorrectSerType    = correctSerType;
			this.CorrectNonSerType = correctNonSerType;
			this.PassedSerType     = passedSerType;
			this.PassedNonSerType  = passedNonSerType;
		}
		public SerComponentTypeException(Type correctSerType, Type correctNonSerType, Type correctBaseSerType, Type passedSerType, Type passedNonSerType, string message = "")
			: base((string.Format("[CorrectSerType = {0}, CorrectNonSerType = {1}, CorrectBaseSerType = {2} PassedSerType = {3}, PassedNonSerType = {4}] ", correctSerType, correctNonSerType, correctBaseSerType, passedSerType, passedNonSerType) + message).Trim())
		{
			this.CorrectSerType    = correctSerType;
			this.CorrectNonSerType = correctNonSerType;
			this.PassedSerType     = passedSerType;
			this.PassedNonSerType  = passedNonSerType;
		}
		public SerComponentTypeException(Type correctSerType, Type correctNonSerType, Type correctBaseSerType, Type passedSerType, Type passedNonSerType, Exception innerException, string message = "")
			: base((string.Format("[CorrectSerType = {0}, CorrectNonSerType = {1}, CorrectBaseSerType = {2} PassedSerType = {3}, PassedNonSerType = {4}] ", correctSerType, correctNonSerType, correctBaseSerType, passedSerType, passedNonSerType) + message).Trim(), innerException)
		{
			this.CorrectSerType    = correctSerType;
			this.CorrectNonSerType = correctNonSerType;
			this.PassedSerType     = passedSerType;
			this.PassedNonSerType  = passedNonSerType;
		}
		protected SerComponentTypeException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}

	public class SaveImplementationException : SaveException
	{
		public SaveImplementationException() { }
		public SaveImplementationException(string message) : base(message) { }
		public SaveImplementationException(string message, Exception innerException) : base(message, innerException) { }
		protected SaveImplementationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}

	public class SaveInProgressException : SaveException
	{
		public SaveInProgressException() { }
		public SaveInProgressException(string message) : base(message) { }
		public SaveInProgressException(string message, Exception innerException) : base(message, innerException) { }
		protected SaveInProgressException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}

	public class SaveSetupException : SaveException
	{
		public SaveSetupException() { }
		public SaveSetupException(string message) : base(message) { }
		public SaveSetupException(string message, Exception innerException) : base(message, innerException) { }
		protected SaveSetupException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}

	public class InvalidSavedMonoException : SaveException
	{
		private Type BackingMonoType;
		/// <summary>The SavedMonoBehaviour type that is invalid</summary>
		public Type MonoType { get { return BackingMonoType; } }
		
		private string BackingInvalidProperty;
		/// <summary>The property that was detected to be invalid, or null</summary>
		public string InvalidProperty { get { return BackingInvalidProperty; } }

		public InvalidSavedMonoException(Type monoType, string message) : base("[Type '" + monoType.FullName + "'] " + message)
		{
			this.BackingMonoType = monoType;
		}
		
		public InvalidSavedMonoException(Type monoType, string message, string invalidProp) : base("[Type '" + monoType.FullName + "'] " + message)
		{
			this.BackingMonoType = monoType;
			this.BackingInvalidProperty = invalidProp;
		}
		
		public InvalidSavedMonoException(Type monoType, string message, Exception innerException) : base("[Type '" + monoType.FullName + "'] " + message, innerException)
		{
			this.BackingMonoType = monoType;
		}
		
		public InvalidSavedMonoException(Type monoType, string message, string invalidProp, Exception innerException) : base("[Type '" + monoType.FullName + "'] " + message, innerException)
		{
			this.BackingMonoType = monoType;
			this.BackingInvalidProperty = invalidProp;
		}
		
		protected InvalidSavedMonoException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
	
	public class LoadException : SaveException
	{
		public LoadException() { }
		public LoadException(string message) : base(message) { }
		public LoadException(string message, Exception innerException) : base(message, innerException) { }
		protected LoadException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
