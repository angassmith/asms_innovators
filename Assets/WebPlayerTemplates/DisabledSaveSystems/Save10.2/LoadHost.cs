﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Collections.ObjectModel;

namespace Save {
	public static class LoadHost
	{
		//LoadPath
		//Application.persistentDataPath can only be gotten from the main thread, so loadPath cannot be set here
		public static string LoadPath = SaveConstants.pathUnset;

		public static List<GameObject> NonSavedGObjsWithHierarchyBrokenOnLoad = new List<GameObject>();

		public static void LoadAll()
		{
			try {
				Debug.Log("Loading...");
				BlockIfPathInvalid();
				DelayedLogger logger = new DelayedLogger();

				SavedGame loadedGame = LoadFromDisk(LoadPath);

				KillExistingRootSaveNodesSetToSave();
				
				//Create loaded gameobjects, using prefabs where specified
				ApplyLoadedGameObjects(loadedGame, logger);

				StaticSaveData.ApplyAllStaticData(loadedGame.StaticData, logger);

				//Apply delayed-logged items (log/log warning/log error/throw)
				logger.ApplyAllTogether();
			} catch (Exception e)
			{
				throw new LoadException("Loading failed: " + e.Message, e);
			}
		}

		private static void BlockIfPathInvalid()
		{
			if (LoadPath == SaveConstants.pathUnset) throw new Exception("LoadPath has not been set. SaveHostMono must be attached to a GameObject for LoadPath to be set.");
			if (LoadPath == SaveConstants.noSavesAvailable) throw new Exception("There are no game saves to load.");
			if (!Directory.Exists(LoadPath)) throw new Exception("The load directory could not be found.");
		}

		private static SavedGame LoadFromDisk(string loadPath)
		{
			using (FileStream savedGameFS = File.OpenRead(loadPath + SaveConstants.saveGameFileName)) {
				BinaryFormatter bf = new BinaryFormatter();
				SavedGame savedGame = (SavedGame)bf.Deserialize(savedGameFS);
				return savedGame;
			}
		}

		private static void ApplyLoadedGameObjects(SavedGame loadedGame, DelayedLogger logger)
		{
			foreach (SaveNodeBase.SaveNodeSaveData sNodeData in loadedGame.RootSaveNodeData)
			{
				SaveNodeBase.GetFromSaveDataAndApply(null, sNodeData, logger);
			}
		}

		private static void KillExistingRootSaveNodesSetToSave()
		{
			foreach (SaveNodeBase rootSNode in SaveHelper.GetRootSaveNodesSetToSaveInCurrentSceneWhenSceneMightBeLoading()) {
				rootSNode.Kill();
			}
		}

		public static void RefreshLoadPath()
		{
			int newestSaveNum = SaveHost.GetNewestSaveNum();
			if (newestSaveNum == 0) {
				LoadPath = SaveConstants.noSavesAvailable;
			} else {
				LoadPath = Application.persistentDataPath + SaveConstants.saveFolderName + SaveConstants.saveGameFolderPrefix + newestSaveNum;
			}

			Debug.Log("LoadPath = '" + LoadPath);
		}
	}
}

//*/