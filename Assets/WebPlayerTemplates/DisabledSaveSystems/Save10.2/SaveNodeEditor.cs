﻿#if UNITY_EDITOR

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections;

namespace Save
{
	[CustomEditor(typeof(SaveNodeBase), editorForChildClasses:true)]
	public class SaveNodeEditor : Editor
	{
		[NonSerialized] //Just in case
		private static List<Type> SavedMonoTypes = null;

		private GUIStyle invalidTextAreaStyle = null;
		private GUIStyle validTextAreaStyle = null;

		private static List<Type> GetSavedMonoTypes()
		{
			//Setup
			List<Type> smonoTypes = new List<Type>();

			//Loop through all types, and add seeds for those that are valid and inherit from SavedMonoBehaviour
			foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (Type type in assembly.GetTypes())
				{
					if (
						   type != null
						&& type.IsClass
						&& !type.IsAbstract
						&& type.IsSubclassOf(typeof(SavedMonoBehaviour))
					) {
						smonoTypes.Add(
							type
						);
					}
				}
			}

			//Return
			return smonoTypes;
		}

		public virtual void OnInspectorGUI2() { }

		public sealed override void OnInspectorGUI()
		{
			//Once off initialization
			if (SavedMonoTypes == null) SavedMonoTypes = GetSavedMonoTypes();
			if (invalidTextAreaStyle == null) {
				invalidTextAreaStyle = new GUIStyle(GUI.skin.textField);
				if (invalidTextAreaStyle.normal    != null) invalidTextAreaStyle.normal   .textColor = Color.red;
				if (invalidTextAreaStyle.hover     != null) invalidTextAreaStyle.hover    .textColor = Color.red;
				if (invalidTextAreaStyle.focused   != null) invalidTextAreaStyle.focused  .textColor = Color.red;
				if (invalidTextAreaStyle.active    != null) invalidTextAreaStyle.active   .textColor = Color.red;
				if (invalidTextAreaStyle.onNormal  != null) invalidTextAreaStyle.onNormal .textColor = Color.red;
				if (invalidTextAreaStyle.onHover   != null) invalidTextAreaStyle.onHover  .textColor = Color.red;
				if (invalidTextAreaStyle.onFocused != null) invalidTextAreaStyle.onFocused.textColor = Color.red;
				if (invalidTextAreaStyle.onActive  != null) invalidTextAreaStyle.onActive .textColor = Color.red;
			}
			if (validTextAreaStyle == null) {
				validTextAreaStyle = new GUIStyle(GUI.skin.textField);
				if (validTextAreaStyle.normal    != null) validTextAreaStyle.normal   .textColor = Color.green;
				if (validTextAreaStyle.hover     != null) validTextAreaStyle.hover    .textColor = Color.green;
				if (validTextAreaStyle.focused   != null) validTextAreaStyle.focused  .textColor = Color.green;
				if (validTextAreaStyle.active    != null) validTextAreaStyle.active   .textColor = Color.green;
				if (validTextAreaStyle.onNormal  != null) validTextAreaStyle.onNormal .textColor = Color.green;
				if (validTextAreaStyle.onHover   != null) validTextAreaStyle.onHover  .textColor = Color.green;
				if (validTextAreaStyle.onFocused != null) validTextAreaStyle.onFocused.textColor = Color.green;
				if (validTextAreaStyle.onActive  != null) validTextAreaStyle.onActive .textColor = Color.green;
			}

			SaveNodeBase snode = (SaveNodeBase)target;

			//Block attaching to prefabs
			PrefabType prefabType = PrefabUtility.GetPrefabType(snode/*.gameObject*/);
			if (prefabType == PrefabType.Prefab || prefabType == PrefabType.ModelPrefab) {
				DestroyImmediate(snode, true); //Unity counts components attached to prefabs as assets. Also note Destroy() is not allowed in the editor
				EditorUtility.DisplayDialog("SaveNode Attached to Prefab", "SaveNodes cannot be attached to prefabs", "Cancel");
				return;
			}

			DrawDefaultInspector();

			snode.ShouldSave = EditorGUILayout.Toggle("Should Save", snode.ShouldSave);
			snode.CustomID = EditorGUILayout.TextField("Custom ID", snode.CustomID);

			EditorGUILayout.BeginHorizontal();
			{
				if (string.IsNullOrEmpty(snode.SourcePrefabName)) {
					EditorGUILayout.PrefixLabel("Src. Prefab Name", GUI.skin.textField, new GUIStyle() { normal = new GUIStyleState() { textColor = Color.Lerp(Color.green, Color.black, 0.5f) } });
				} else if (SaveHostMono.Main.SaveNodeSourcePrefabs.Any(x => x != null && x.name == snode.SourcePrefabName)) {
					EditorGUILayout.PrefixLabel("Src. Prefab Name", GUI.skin.textField, new GUIStyle() { normal = new GUIStyleState() { textColor = Color.Lerp(Color.green, Color.black, 0.5f) } });
				} else {
					EditorGUILayout.PrefixLabel("Src. Prefab Name", GUI.skin.textField, new GUIStyle() { normal = new GUIStyleState() { textColor = Color.red } });
				}
				snode.SourcePrefabName = EditorGUILayout.TextField(snode.SourcePrefabName);
			}
			EditorGUILayout.EndHorizontal();

			if (GUILayout.Button(new GUIContent("Randomise Custom ID", "Sets the Custom ID to a random GUID")))
			{
				snode.CustomID = Guid.NewGuid().ToString();
			}

			GUILayout.Label(new GUIContent("Add Saved Mono Behaviour:", "A shortcut for adding a SavedMonoBehaviour (you can also add SaveMonoBehaviours like any other MonoBehaviour or Component)"));
			int newSMonoTypeIndex = (
				EditorGUILayout.Popup(
					0,
					new string[] { "[select]" }
					.Concat(
						SavedMonoTypes
						.Select(x => x.Name)
					)
					.ToArray()
				) - 1
			);
			if (newSMonoTypeIndex >= 0 && newSMonoTypeIndex < SavedMonoTypes.Count) {
				snode.gameObject.AddComponent(SavedMonoTypes[newSMonoTypeIndex]);
			}

			OnInspectorGUI2();
		}

		#region Old manual inspector code (might be useful in future)
		//	void Something() {
		//		EditorGUILayout.Separator();
		//		
		//		foreach (SavedMonoBehaviour sMono in snode.SavedMonos)
		//		{
		//			if (sMono == null) continue;
		//			
		//			GUILayout.Space(6);
		//			GUILayout.Label(sMono.name, new GUIStyle() { fontStyle = FontStyle.Bold, fontSize = 12 });
		//			
		//			Type sMonoType = sMono.GetType();
		//			BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Default | BindingFlags.DeclaredOnly;
		//		
		//			//Get properties and fields
		//			List<Obj<MemberInfo, object, Type, Action<object>>> propFieldData = (
		//				System.Linq.Enumerable.Concat( //Combine the property and field enumerables (below)
		//					sMonoType
		//					.GetProperties(bindingFlags) //Get all property-infos
		//					.Where( //Filter to those that have the [DisplayPropertyInInspector] attribute
		//						(pinfo) => { return GetAttribute<DisplayPropertyInInspectorAttribute>(pinfo) != null; }
		//					)
		//					.Select( //Get their values and convert them to a type that works with field-infos
		//						(pinfo) => {
		//							return new Obj<MemberInfo, object, Type, Action<object>>(
		//								pinfo, pinfo.GetValue(sMono, null), pinfo.PropertyType, (val) => { pinfo.SetValue(sMono, val, null); }
		//							);
		//						}
		//					),
		//					sMonoType
		//					.GetFields(bindingFlags) //Get all field-infos
		//					.Select( //Get their values and convert them to a type that works with property-infos
		//						(finfo) => {
		//							return new Obj<MemberInfo, object, Type, Action<object>>(
		//								finfo, finfo.GetValue(sMono), finfo.FieldType, (val) => { finfo.SetValue(sMono, val); }
		//							);
		//						}
		//					)
		//				)
		//				.OrderBy((x) => x.Item1.Name) //Sort alphabetically by the property/field name
		//				.ToList() //Convert to list
		//			);
		//				
		//			//Loop through all (sorted) properties/fields, and draw the inspector for each
		//			foreach (var propOrField in propFieldData)
		//			{
		//				DrawPropOrFieldInspector(propOrField.Item1, propOrField.Item1.Name, propOrField.Item2, propOrField.Item3, propOrField.Item4, sMono.GetInstanceID().ToString() + "////" + propOrField.Item1.Name);
		//			}
		//		}
		//	}
		//
		//	private void DrawPropOrFieldInspector(MemberInfo PFInfo, string PFName, object PFValue, Type PFType, Action<object> PFSetter, string path)
		//	{
		//		if (PFType == null)
		//		{
		//			GUILayout.Label("An unknown property could not be displayed");
		//			return;
		//		}
		//	
		//		//SpaceAttribute
		//		float spaceHeight = 0;
		//		foreach (SpaceAttribute spaceAttrib in GetAttributes<SpaceAttribute>(PFInfo)) {
		//			spaceHeight += spaceAttrib.height;
		//		}
		//		GUILayout.Space(spaceHeight);
		//	
		//		//HeaderAttribute
		//		try {
		//			HeaderAttribute headerAttrib = GetAttribute<HeaderAttribute>(PFInfo);
		//			if (headerAttrib != null) { GUILayout.Label(headerAttrib.header); }
		//		} catch (AmbiguousMatchException) { }
		//	
		//		//RangeAttribute (used later for number types)
		//		RangeAttribute rangeAttribute = GetAttribute<RangeAttribute>(PFInfo);
		//	
		//		//Number types
		//		if      (PFType == typeof(sbyte  )) PFSetter(   (sbyte  ) EditorGUILayout.IntField   (PFName, rangeAttribute == null ? (sbyte)          PFValue : (sbyte )General.Math.Clamp((float)(sbyte)      PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(short  )) PFSetter(   (short  ) EditorGUILayout.IntField   (PFName, rangeAttribute == null ? (short)          PFValue : (short )General.Math.Clamp((float)(short)      PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(int    )) PFSetter(   (int    ) EditorGUILayout.IntField   (PFName, rangeAttribute == null ? (int)            PFValue : (int   )General.Math.Clamp((float)(int)        PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(long   )) PFSetter(   (long   ) EditorGUILayout.LongField  (PFName, rangeAttribute == null ? (long)           PFValue : (long  )General.Math.Clamp((float)(long)       PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(byte   )) PFSetter(   (byte   ) EditorGUILayout.IntField   (PFName, rangeAttribute == null ? (byte)           PFValue : (byte  )General.Math.Clamp((float)(byte)       PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(ushort )) PFSetter(   (ushort ) EditorGUILayout.IntField   (PFName, rangeAttribute == null ? (ushort)         PFValue : (ushort)General.Math.Clamp((float)(ushort)     PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(uint   )) PFSetter(   (uint   ) EditorGUILayout.IntField   (PFName, rangeAttribute == null ? (int)(uint)      PFValue : (int   )General.Math.Clamp((float)(int)(uint)  PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(ulong  )) PFSetter(   (ulong  ) EditorGUILayout.LongField  (PFName, rangeAttribute == null ? (long)(ulong)    PFValue : (long  )General.Math.Clamp((float)(long)(ulong)PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(float  )) PFSetter(   (float  ) EditorGUILayout.FloatField (PFName, rangeAttribute == null ? (float)          PFValue : (float )General.Math.Clamp((float)(float)      PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(double )) PFSetter(   (double ) EditorGUILayout.DoubleField(PFName, rangeAttribute == null ? (double)         PFValue : (double)General.Math.Clamp((double)            PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//		else if (PFType == typeof(decimal)) PFSetter(   (decimal) EditorGUILayout.DoubleField(PFName, rangeAttribute == null ? (double)(decimal)PFValue : (double)General.Math.Clamp((double)(decimal)   PFValue, rangeAttribute.min, rangeAttribute.max))   );
		//	
		//		//Bool
		//		else if (PFType == typeof(bool)) PFSetter(   EditorGUILayout.Toggle(PFName, (bool)PFValue)   );
		//	
		//		//String
		//		else if (PFType == typeof(string)) {
		//			if (GetAttribute<TextAreaAttribute>(PFInfo) != null) {
		//				EditorGUILayout.BeginScrollView(Vector2.zero);
		//				{
		//					EditorGUILayout.LabelField(PFName);
		//					PFSetter(   EditorGUILayout.TextArea((string)PFValue)   );
		//				}
		//				EditorGUILayout.EndScrollView();
		//			} else if (GetAttribute<MultilineAttribute>(PFInfo) != null) {
		//				EditorGUILayout.LabelField(PFName);
		//				PFSetter(   EditorGUILayout.TextArea((string)PFValue)   );
		//			} else {
		//				PFSetter(   EditorGUILayout.TextField(PFName, (string)PFValue)   );
		//			}
		//		}
		//	
		//		//Enum
		//		else if (PFType.IsEnum) {
		//			if (GetAttribute<FlagsAttribute>(PFInfo) != null) {
		//				PFSetter(   EditorGUILayout.EnumMaskPopup(new GUIContent(PFName), (Enum)PFValue)   );
		//			} else {
		//				PFSetter(   EditorGUILayout.EnumPopup(PFName, (Enum)PFValue)   );
		//			}
		//		}
		//	
		//		//Basic Unity types (those that are possible)
		//		else if (PFType == typeof(Vector2)) PFSetter(   EditorGUILayout.Vector2Field(PFName, (Vector2)PFValue)   );
		//		else if (PFType == typeof(Vector3)) PFSetter(   EditorGUILayout.Vector3Field(PFName, (Vector3)PFValue)   );
		//		else if (PFType == typeof(Vector4)) PFSetter(   EditorGUILayout.Vector4Field(PFName, (Vector4)PFValue)   );
		//		else if (PFType == typeof(Rect   )) PFSetter(   EditorGUILayout.RectField   (PFName, (Rect   )PFValue)   );
		//		else if (PFType == typeof(Color)) {
		//			ColorUsageAttribute colorAttrib = GetAttribute<ColorUsageAttribute>(PFInfo);
		//			if (colorAttrib == null) PFSetter(   EditorGUILayout.ColorField(PFName, (Color)PFValue)   );
		//			else PFSetter(
		//				EditorGUILayout.ColorField(
		//					new GUIContent(PFName), (Color)PFValue,
		//					true, colorAttrib.showAlpha, colorAttrib.hdr,
		//					new ColorPickerHDRConfig(
		//						colorAttrib.minBrightness, colorAttrib.maxBrightness,
		//						colorAttrib.minExposureValue, colorAttrib.maxExposureValue
		//					)
		//				)
		//			);
		//		}
		//		else if (PFType == typeof(Color32)) {
		//			ColorUsageAttribute attrib = GetAttribute<ColorUsageAttribute>(PFInfo);
		//			if (attrib == null) PFSetter(   (Color32) EditorGUILayout.ColorField(PFName, (Color32)PFValue)   );
		//			else PFSetter(
		//				(Color32) EditorGUILayout.ColorField(
		//					new GUIContent(PFName), (Color32)PFValue,
		//					true, attrib.showAlpha, attrib.hdr,
		//					new ColorPickerHDRConfig(
		//						attrib.minBrightness, attrib.maxBrightness,
		//						attrib.minExposureValue, attrib.maxExposureValue
		//					)
		//				)
		//			);
		//		}
		//		else if (PFType == typeof(AnimationCurve)) PFSetter(   EditorGUILayout.CurveField(PFName, (AnimationCurve)PFValue)   );
		//	
		//		//General Unity Object type
		//		else if (typeof(UnityEngine.Object).IsAssignableFrom(PFType)) { //PFType inherits from Object
		//			Debug.Log("130: " + path);
		//			PFSetter(   EditorGUILayout.ObjectField(PFName, (UnityEngine.Object)PFValue, PFType, true)   );
		//		}
		//	
		//		//Array
		//		else if (PFType.IsArray) {
		//			if (PFValue == null) {
		//				PFSetter(Array.CreateInstance(PFType.GetElementType(), 0)); //Set PF to an empty array instead of null
		//				return;
		//			}
		//			
		//			//Find whether this particular array should be folded
		//			bool unfold;
		//			if (UnfoldingData.ContainsKey(path)) unfold = UnfoldingData[path];
		//			else { unfold = true; UnfoldingData.Add(path, unfold); }
		//	
		//			//Draw the foldout, using the current folding value as the default,
		//			//and if it ends up unfolded, draw the array
		//			if (EditorGUILayout.Foldout(unfold, PFName))
		//			{
		//				//Increase indentation
		//				FoldingLevel++;
		//				string foldingPrefix = "";
		//				for (int j = 0; j < FoldingLevel; j++) foldingPrefix += "    ";
		//	
		//				//Setup
		//				Array arr = (Array)PFValue;
		//	
		//				//Allow the array to be resized
		//				int currentLength = arr.Length;
		//				int newLength = General.Math.ClampBottom(EditorGUILayout.IntField(foldingPrefix + "Size", currentLength), 0);
		//				if (newLength != currentLength) {
		//					//Create new array
		//					Array newArr = Array.CreateInstance(arr.GetType().GetElementType(), newLength);
		//					//Copy all elements within the bounds of both arrays
		//					Array.Copy(arr, 0, newArr, 0, Math.Min(currentLength, newLength));
		//					//Replace old array
		//					arr = newArr;
		//					PFSetter(newArr);
		//					newArr = null;
		//				}
		//	
		//				//Draw the elements of the array
		//				int i = 0;
		//				foreach (object item in arr)
		//				{
		//					DrawPropOrFieldInspector(
		//						null,
		//						foldingPrefix + "Element " + i,
		//						item,
		//						PFType.GetElementType(),
		//						(val) => { arr.SetValue(val, i); },
		//						path + "////Element " + i
		//					);
		//				}
		//	
		//				//Decrease indentation
		//				FoldingLevel--;
		//			}
		//		}
		//	
		//		//List
		//		else if (typeof(IList).IsAssignableFrom(PFType)) //I would be more specific than this
		//		{
		//			if (PFValue == null) {
		//				return; //Cannot guarantee that an instance can be created
		//			}
		//			
		//			//Find whether this particular list should be folded
		//			bool unfold;
		//			if (UnfoldingData.ContainsKey(path)) unfold = UnfoldingData[path];
		//			else { unfold = true; UnfoldingData.Add(path, unfold); }
		//	
		//			//Draw the foldout, using the current folding value as the default,
		//			//and if it ends up unfolded, draw the array
		//			if (EditorGUILayout.Foldout(unfold, PFName))
		//			{
		//				//Increase indentation
		//				FoldingLevel++;
		//				string foldingPrefix = "";
		//				for (int j = 0; j < FoldingLevel; j++) foldingPrefix += "  ";
		//	
		//				//Setup
		//				IList list = (IList)PFValue;
		//	
		//				//Allow the list to be resized
		//				int currentLength = list.Count;
		//				int newLength = General.Math.ClampBottom(EditorGUILayout.IntField(foldingPrefix + "Size", currentLength), 0);
		//				int diff = newLength - currentLength;
		//				if (diff > 0) {
		//					for (int j = 0; j < diff; j++) {
		//						list.Add(General.GetDefault(list.GetType().GetGenericArguments()[0]));
		//					}
		//				} else if (diff < 0) {
		//					for (int j = 0; j > diff; j--) {
		//						list.RemoveAt(list.Count - 1);
		//					}
		//				}
		//	
		//				//Draw the elements of the list
		//				int i = 0;
		//				foreach (object item in list)
		//				{
		//					DrawPropOrFieldInspector(
		//						null,
		//						foldingPrefix + "Element " + i,
		//						item,
		//						PFType.GetGenericArguments()[0],
		//						(val) => { list[i] = val; },
		//						path + "////ListElement " + i
		//					);
		//				}
		//	
		//				//Decrease indentation
		//				FoldingLevel--;
		//			}
		//		}
		//	
		//		//Otherwise
		//		else
		//		{
		//			Debug.Log("131: " + path);
		//			//	Debug.Log("#120: " + path + ", " + PFType + ", " + (PFValue == null ? "null" : PFValue.GetType().ToString()) + ", " + PFValue);
		//			//Display the object as a read-only string
		//			EditorGUILayout.LabelField(PFName, PFValue == null ? "null" : PFValue.ToString());
		//		}
		//	}
		//	
		//	/// <exception cref="NotSupportedException">info is not a constructor, method, property, event, type, field, or null</exception>
		//	/// <exception cref="AmbiguousMatchException">More than one of the requested attributes was found</exception>
		//	/// <exception cref="TypeLoadException">A custom attribute type cannot be loaded</exception>
		//	private static T GetAttribute<T>(MemberInfo info, bool inherit = false) where T : Attribute
		//	{
		//		if (info == null) return null;
		//		Attribute attribute = Attribute.GetCustomAttribute(info, typeof(T), inherit);
		//		return (T)attribute;
		//	}
		//	
		//	/// <exception cref="NotSupportedException">info is not a constructor, method, property, event, type, field, or null</exception>
		//	/// <exception cref="TypeLoadException">A custom attribute type cannot be loaded</exception>
		//	private static T[] GetAttributes<T>(MemberInfo info, bool inherit = false) where T : Attribute
		//	{
		//		if (info == null) return new T[0];
		//		Attribute[] attributes = Attribute.GetCustomAttributes(info, typeof(T), inherit);
		//		return (T[])attributes;
		//	}
		//	
		//	private static bool Inherits(Type target, Type potentialBase)
		//	{
		//		return potentialBase.IsAssignableFrom(target);
		//	}

		#endregion
	}
}

#endif