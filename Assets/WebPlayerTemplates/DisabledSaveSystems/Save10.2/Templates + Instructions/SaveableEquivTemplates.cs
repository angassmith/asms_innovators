﻿/*

#if UNITY_EDITOR

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Save.SaveableEquivs;

namespace Save.Docs { //Usually this would be "Save.SaveableEquivs"

	// --- Generic Abstract Base Template ---

	public interface ISerBASETYPE
	{ }
	
	/// <summary>
	/// Status:Incomplete
	/// </summary>
	[System.Serializable]
	public abstract class SerBASETYPE<TSer, TNonSer> : SerClass<TSer, TNonSer>, ISerBASETYPE
		where TSer : SerBASETYPE<TSer, TNonSer>, new()
		where TNonSer : BASETYPE
	{	
		public SerBASETYPE(VoidClass none) : base(VoidClass.Void) { }
		public SerBASETYPE(TNonSer nonSer) : base(nonSer) { }
		
		public sealed override void CopyFromNonNullNonSerClass(TNonSer nonSerSource)
		{

			CopyFromNonNullNonSerBASETYPE(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerBASETYPE(TNonSer nonSerSource);

		public sealed override void CopyOverNonNullNonSerClass(TNonSer nonSerDest)
		{

			CopyOverNonNullNonSerBASETYPE(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerBASETYPE(TNonSer nonSerSource);
	}

	// --- Non-Generic Sealed Base Template ---

	/// <summary>
	/// Status:See base
	/// </summary>
	[System.Serializable]
	public sealed class SerBASETYPE : SerBASETYPE<SerBASETYPE, BASETYPE>
	{
		public override SaveEquivReg.Entry<SerBASETYPE, BASETYPE> RegInfo {
			get {
				return null;
			}
		}
		
		public SerBASETYPE() : base(VoidClass.Void) { }
		public SerBASETYPE(BASETYPE nonSer) : base(nonSer) { }
		
		protected override void CopyFromNonNullNonSerBASETYPE(BASETYPE nonSerSource) { }
		protected override void CopyOverNonNullNonSerBASETYPE(BASETYPE nonSerDest) { }

		public override BASETYPE CreateBlankNonSer()
		{
			return new BASETYPE();
		}
	}

	// --- Sealed Derived Template ---
	
	/// <summary>
	/// Status:Incomplete
	/// </summary>
	[System.Serializable]
	public sealed class SerDERIVEDTYPE : SerBASETYPE<SerDERIVEDTYPE, DERIVEDTYPE>
	{
		public override SaveEquivReg.Entry<SerDERIVEDTYPE, DERIVEDTYPE> RegInfo {
			get {
				return null;
			}
		}
		
		public SerDERIVEDTYPE() : base(VoidClass.Void) { }
		public SerDERIVEDTYPE(DERIVEDTYPE nonSer) : base(nonSer) { }
		
		protected override void CopyFromNonNullNonSerBASETYPE(DERIVEDTYPE nonSerSource)
		{
			
		}

		protected override void CopyOverNonNullNonSerBASETYPE(DERIVEDTYPE nonSerDest)
		{
			
		}

		public override DERIVEDTYPE CreateBlankNonSer()
		{
			return new DERIVEDTYPE();
		}
	}
	

	///<summary>Dummy class</summary>
	public class BASETYPE
	{ }

	///<summary>Dummy class</summary>
	public class DERIVEDTYPE : BASETYPE
	{ }

}

#endif

//*/