﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Save.Docs
{
	[System.Serializable]
	public class SavedMonoFullTemplate : SavedMonoBehaviour
	{
		
		//Declare saved variables like normal variables
		//To stop a variable from being saved, prepend it with [NonSavedField]
		//IMPORTANT: Make sure to only use serializable types in saved variables
		//Serializable types include: primitive types, strings, decimals, Enums,
		//anything prepended with 'Ser' in the Save namespace, and anything else
		//with the [System.Serializable] attribute.
	
	#region SaveProperties
		//If you need a non-serializable type to be saved, create a property of
		//the equivalent serializable type, prepend it with [SavedProperty], and define get
		//and set accessors that convert a non-serializable, [NonSavedField] field to and from the serializable type
	#endregion
	
		//Start(), when not loading.
		public override void NormalStart()
		{
		
		}
		//Start(), after loading
		public override void OnLoadStart()
		{
		
		}
		//Start(), after NormalStart() and OnLoadStart()
		public override void CommonStart()
		{
		
		}
	
	
	
		//Update(), when not about to save
		public override void NormalUpdate()
		{
		
		}
		//Update(), in the frame immediately before saving.
		public override void PreSaveUpdate()
		{
		
		}
		//Update(), after NormalUpdate() and PreSaveUpdate()
		public override void CommonUpdate()
		{
		
		}
	
	#region Save preparation and application methods
		//Called immediately before saving the saved mono.
		//Should not modify other saved GameObjects or their (Saved/not)MonoBehaviours and Components
		public override void ImmediatelyBeforeSave()
		{
		
		}
	
		//Called immediately after loading the saved mono.
		//Should not rely on other saved GameObjects or their (Saved/not)MonoBehaviours and Components existing
		public override void ImmediatelyAfterLoad()
		{
		
		}
	#endregion
	
	}

	[System.Serializable]
	public class SavedMonoShortTemplate : SavedMonoBehaviour
	{
	
#region SaveProperties
	
#endregion
	
		public override void NormalStart()
		{
		
		}
		public override void OnLoadStart()
		{
		
		}
		public override void CommonStart()
		{
		
		}
	
	
	
		public override void NormalUpdate()
		{
		
		}
		public override void PreSaveUpdate()
		{
		
		}
		public override void CommonUpdate()
		{
		
		}
	
	
#region Save preparation and application methods
		public override void ImmediatelyBeforeSave()
		{
		
		}
		public override void ImmediatelyAfterLoad()
		{
		
		}
#endregion
	
	}

}


#endif