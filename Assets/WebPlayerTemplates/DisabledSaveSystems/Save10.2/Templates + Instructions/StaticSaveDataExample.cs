﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using Save;
using Save.SaveableEquivs;

namespace Save.Docs
{
	[DeliberatelyNonSerializable]
	public static class StaticClass {

		public static int someInt = 5;
		private static List<string> someList = new List<string>(new string[] { "foo", "bar", "blah" });
		public static Vector3 someVector3 = new Vector3(1, 2, 3);
		
		[System.Serializable]
		public sealed class StaticClassSaver : StaticSaveData //public/private/protected/internal, it doesn't matter
		{
			public int          StaticClass_someInt    ; //public/private/protected/internal, it doesn't matter
			public List<string> StaticClass_someList   ; // ^
			public SerVector3   StaticClass_someVector3; // ^

			private StaticClassSaver() : base(null) { }

			public override void ApplyStaticData(DelayedLogger logger)
			{
				StaticClass.someInt     =          StaticClass_someInt    ;
				StaticClass.someList    =          StaticClass_someList   ;
				StaticClass.someVector3 = (Vector3)StaticClass_someVector3;
			}

			public override void StoreStaticData(DelayedLogger logger)
			{
				StaticClass_someInt     =             StaticClass.someInt    ;
				StaticClass_someList    =             StaticClass.someList   ;
				StaticClass_someVector3 = (SerVector3)StaticClass.someVector3;
			}
		}
	}
}

//*/