﻿/*
#if UNITY_EDITOR


using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Save;

namespace Save.Docs { //Usually this would be "Save.SaveableEquivs"
	
	// --- Generic Abstract Base Example ---

	public interface ISerBASEEXAMPLE : ISaveableEquiv
	{ }
	
	/// <summary>
	/// Status:Complete
	/// </summary>
	[System.Serializable]
	public abstract class SerBASEEXAMPLE<TSer, TNonSer> : SerClass<TSer, TNonSer>, ISerBASEEXAMPLE
		where TSer : SerBASEEXAMPLE<TSer, TNonSer>, new()
		where TNonSer : BASEEXAMPLE
	{
		public int SomeProperty;

		public SerBASEEXAMPLE(VoidClass none) : base(VoidClass.Void) { }
		public SerBASEEXAMPLE(TNonSer nonSerSource) : base(nonSerSource) { }

		public sealed override void CopyFromNonNullNonSerClass(TNonSer nonSerSource)
		{
			this.SomeProperty = nonSerSource.SomeProperty;
			CopyFromNonNullNonSerBASEEXAMPLE(nonSerSource);
		}
		protected abstract void CopyFromNonNullNonSerBASEEXAMPLE(TNonSer nonSerSource);

		public sealed override void CopyOverNonNullNonSerClass(TNonSer nonSerDest)
		{
			nonSerDest.SomeProperty = this.SomeProperty;
			CopyOverNonNullNonSerBASEEXAMPLE(nonSerDest);
		}
		protected abstract void CopyOverNonNullNonSerBASEEXAMPLE(TNonSer nonSerDest);
	}

	// --- Non-Generic Sealed Base Example ---

	/// <summary>
	/// Status:See base
	/// </summary>
	[System.Serializable]
	public sealed class SerBASEEXAMPLE : SerBASEEXAMPLE<SerBASEEXAMPLE, BASEEXAMPLE>
	{
		public override SaveEquivReg.Entry<SerBASEEXAMPLE, BASEEXAMPLE> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerBASEEXAMPLE, BASEEXAMPLE>(
					"SerBASEEXAMPLE",
					"BASEEXAMPLE",
					new SaveEquivReg.ClassStatus<SerBASEEXAMPLE, BASEEXAMPLE>(
						x => new SerBASEEXAMPLE(x),
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.AllCodeComplete,
						SaveEquivReg.SavedDataCompletedness.UnknownOrUntested,
						SaveEquivReg.LoadedDataSucess.UnknownOrUntested,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.NormalDataStorage,
						SaveEquivReg.Additional.None,
						SaveEquivReg.CreateNewNonSerPossible.ShouldWorkCorrectly,
						SaveEquivReg.CreateNewNonSerUseable.ShouldWorkCorrectly,
						null
					)
				);
			}
		}

		public SerBASEEXAMPLE() : base(VoidClass.Void) { }
		public SerBASEEXAMPLE(BASEEXAMPLE nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerBASEEXAMPLE(BASEEXAMPLE nonSerSource) { }
		protected override void CopyOverNonNullNonSerBASEEXAMPLE(BASEEXAMPLE nonSerDest) { }

		public override BASEEXAMPLE CreateBlankNonSer()
		{
			return new BASEEXAMPLE();
		}
	}

	// --- Sealed Derived Example ---
	
	/// <summary>
	/// Status:Complete
	/// </summary>
	[System.Serializable]
	public sealed class SerDERIVEDEXAMPLE : SerBASEEXAMPLE<SerDERIVEDEXAMPLE, DERIVEDEXAMPLE>
	{
		public override SaveEquivReg.Entry<SerDERIVEDEXAMPLE, DERIVEDEXAMPLE> RegInfo {
			get {
				return new SaveEquivReg.Entry<SerDERIVEDEXAMPLE, DERIVEDEXAMPLE>(
					"SerDERIVEDEXAMPLE",
					"DERIVEDEXAMPLE",
					new SaveEquivReg.ClassStatus<SerDERIVEDEXAMPLE, DERIVEDEXAMPLE>(
						x => new SerDERIVEDEXAMPLE(x),
						SaveEquivReg.CodeType.NormalCSharp,
						SaveEquivReg.CodeCompletedness.AllCodeComplete,
						SaveEquivReg.SavedDataCompletedness.UnknownOrUntested,
						SaveEquivReg.LoadedDataSucess.UnknownOrUntested,
						SaveEquivReg.ApplicationSucess.UnknownOrUntested,
						SaveEquivReg.Uses.NormalDataStorage,
						SaveEquivReg.Additional.None,
						SaveEquivReg.CreateNewNonSerPossible.ShouldWorkCorrectly,
						SaveEquivReg.CreateNewNonSerUseable.ShouldWorkCorrectly,
						null
					)
				);
			}
		}

		public int someProperty;
		public SerVector3 someOtherProperty;
		public float someReadonlyProperty;
		
		public SerDERIVEDEXAMPLE() : base(VoidClass.Void) { }
		public SerDERIVEDEXAMPLE(DERIVEDEXAMPLE nonSer) : base(nonSer) { }

		protected override void CopyFromNonNullNonSerBASEEXAMPLE(DERIVEDEXAMPLE nonSerSource)
		{	
			someOtherProperty    = (SerVector3)nonSerSource.SomeOtherProperty  ;
			someReadonlyProperty =             nonSerSource.SomeReadonlyProperty;
		}

		protected override void CopyOverNonNullNonSerBASEEXAMPLE(DERIVEDEXAMPLE nonSerDest)
		{
			nonSerDest.SomeOtherProperty    = (Vector3)this.someOtherProperty;
			nonSerDest.SetSomeReadonlyProperty(this.someReadonlyProperty);
		}

		public override DERIVEDEXAMPLE CreateBlankNonSer()
		{
			return new DERIVEDEXAMPLE();
		}
	}

	///<summary>Dummy class</summary>
	[DeliberatelyNonSerializable]
	public class BASEEXAMPLE
	{
		public int SomeProperty; //Actually a field, but that's irrelevant
	}

	///<summary>Dummy class</summary>
	[DeliberatelyNonSerializable]
	public class DERIVEDEXAMPLE : BASEEXAMPLE
	{
		public Vector3 SomeOtherProperty { get; set; }
		public float SomeReadonlyProperty { get; private set; }
		public void SetSomeReadonlyProperty(float value) { }
	}
}

#endif

//*/