﻿#if UNITY_EDITOR

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Save.Docs {

	//Setting up the template:
	//   Select the template(s):
	//    - If the non-ser type is sealed, use the 'Sealed Derived Template'
	//    - If the non-ser type is abstract, use the 'Generic Abstract Template'.
	//    - Otherwise, use both the 'Generic Abstract Template' and 'Non-Generic Sealed Template'.
	//   In all cases derive from the saveable equivalent of the the non-ser type's base class.
	//   (If there isn't one, create it).


	//When creating a SaveableEquiv type:
	//   
	//   Step 1: In the CopyFromNonNullNonSer[...] method, list the properties that are in TNonSer
	//   Exclude [deprecated] properties, inherited properties, and properties that cannot be set through any means
	//   Add spaces to make the semicolons line up.
	//   
	//   Step 2: Copy some of the text so that the properties will be copied from nonSerSource
	//   Some of these may involve function calls, etc.
	//   Use casts to convert to and from SaveableEquiv types.
	//   
	//   Step 3: Copy some of the text to define public fields, and add the types manually
	//   If the property is not a primitive type, Enum, or other serializable type, prepend the type with 'Ser'.
	//   If the resulting type is not defined, create an SaveableEquiv class for it.
	//   
	//   Step 4: Copy some of the text and adapt it to copy to nonSerDest.
	//   Some of these may involve function calls, etc.
	//   Use casts to convert to and from SaveableEquiv types.
	//   
	//   Step 5: Set the Status comment, and RegInfo property. Add the class to the SaveEquivReg.BuildReg() method.
	//   Continue to revise the Status and RegInfo as improvements are made and tests are done.
	//   
	//   More:
	//    - Add whatever methods/constructors you want (optional & ongoing)
	//    - Add non-normal-C# code to get and set additional properties
	//   
	//   Notes:
	//    - The status comment is a summary that will appear to people using the class
	//    - The RegInfo property gives more detail (replace the null)

}

#endif