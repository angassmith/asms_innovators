﻿using UnityEngine;
using System.Collections;

namespace Save
{
	public static class SaveConstants
	{
		public const string saveFolderName = "/Save"; //There may also be other folders such as Settings, etc
		public const string saveGameFileName = "/SaveGame.expsv";
		public const string saveGameFolderPrefix = "/save---";
		public const string saveGameFolderPrefixNoSlash = "save---";
		public const string noSavesAvailable = "/NOSAVES";
		public const string pathUnset = "/UNSET";
	}
}
