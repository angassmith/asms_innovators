﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;
using System;
using System.IO;

namespace Save {

	public class ValidateSavedMonos
	{
		[MenuItem("Tools/Validate Saved Monos")]
		public static void Validate()
		{
			try
			{
				// --- Validate a new instance of each SavedMonoBehaviour type ---
				Debug.Log("<b><color=blue> --- Validating SavedMonoBehaviour Types --- </color></b>");

				GameObject dummyGObj = new GameObject();

				foreach (Type t in Assembly.GetAssembly(typeof(SaveEquivReg)).GetTypes())
				{
					if (t.IsSubclassOf(typeof(SavedMonoBehaviour)))
					{
						DelayedLogger logger = new DelayedLogger();
						logger.Add(DelayedLogItem.CreateMessage("<i><color=blue>Validating SavedMonoBehaviour type '" + t.FullName + "'</color></i>"));
						
						//Create instance and validate, adding to logger
						try {
							SavedMonoBehaviour smInst = (SavedMonoBehaviour)dummyGObj.AddComponent(t);
							smInst.ValidateSerialization(logger);
						} catch (Exception e) {
							logger.Add(DelayedLogItem.CreateError("<i><color=red>Error while validating SavedMonoBehaviour:</color></i>\r\n" + e.ToString()));
						}

						if (logger.ContainsAnyOfAnyType(DelayedLogItem.LogItemType.LogError, DelayedLogItem.LogItemType.LogException, DelayedLogItem.LogItemType.ThrowException)) {
							logger.Add(DelayedLogItem.CreateMessage("<i><color=red>SaveMonoBehaviour type '" + t.FullName + "' is not valid</color></i>"));
						} else {
							logger.Add(DelayedLogItem.CreateMessage("<i><color=green>SaveMonoBehaviour type '" + t.FullName + "' is valid</color></i>"));
						}

						logger.ApplyAllTogether();
					}
				}

				UnityEngine.Object.DestroyImmediate(dummyGObj);


				// --- Validate each existing SavedMonoBehaviour instance ---
				Debug.Log("<b><color=blue> --- Validating SavedMonoBehaviour Instances --- </color></b>");

				foreach (SavedMonoBehaviour smono in Resources.FindObjectsOfTypeAll<SavedMonoBehaviour>())
				{
					DelayedLogger logger = new DelayedLogger();
					logger.Add(DelayedLogItem.CreateMessage("<i><color=blue>Validating SavedMonoBehaviour instance of type '" + smono.GetType().FullName + "' attached to GameObject '" + smono.gameObject.name + "'</color></i>"));

					//Validate instance, adding to logger
					try {
						smono.ValidateSerialization(logger);
					} catch (Exception e) {
						logger.Add(DelayedLogItem.CreateError("<i><color=red>Error while validating SavedMonoBehaviour:</color></i>\r\n" + e.ToString()));
					}

					if (logger.ContainsAnyOfAnyType(DelayedLogItem.LogItemType.LogError, DelayedLogItem.LogItemType.LogException, DelayedLogItem.LogItemType.ThrowException)) {
						logger.Add(DelayedLogItem.CreateMessage("<i><color=red>SaveMonoBehaviour instance of type '" + smono.GetType().FullName + "' attached to GameObject '" + smono.gameObject.name + "' is not valid</color></i>"));
					} else {
						logger.Add(DelayedLogItem.CreateMessage("<i><color=green>SaveMonoBehaviour instance of type '" + smono.GetType().FullName + "' attached to GameObject '" + smono.gameObject.name + "' is valid</color></i>"));
					}

					logger.ApplyAllTogether();
				}

				Debug.Log("<b><color=blue> --- Finished validating SavedMonoBehaviours --- </color></b>");
			}
			catch (Exception e)
			{
				Debug.Log("<i><color-red>An unknown error occurred while validating the SavedMonoBehaviour*s*:</color></i>\r\n" + e.ToString());
			}

		}
	}

}

#endif