﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Save;

[DisallowMultipleComponent]
public class SaveHostMono : MonoBehaviour {

	public bool AutoLoad; //(set in the editor)

	[NonSerialized]
	private static SaveHostMono backingMain;
	public static SaveHostMono Main {
		//Yes, getters shouldn't throw exceptions, but this property will almost
		//always be used in situations that would throw a NullReferenceException otherwise
		get {
			if (backingMain != null) return backingMain;
			else throw new InvalidOperationException(
				"There must be at least 1 SaveHostMono in the game for SaveHostMono.Main"
				+ " to be non-null and therefore accessible."
			);
		}
	}
	public static bool MainIsNull { get { return backingMain == null; } }

	[NonSerialized]
	private SavingState backingState = SavingState.Other;
	public SavingState State { get { return backingState; } }

	public List<GameObject> SaveNodeSourcePrefabs = new List<GameObject>();

	public enum SavingState
	{
		/// <summary>When set to this, nothing is guaranteed</summary>
		Other = -2,
		/// <summary>Constant throughout all frames (except possibly some times before Awake())</summary>
		Disabled = -1,
		/// <summary>May be changed from this value at any time during a frame. Also does not indicate that multithreaded saving is not in progress</summary>
		RunningNormally = 0,
		/// <summary>Constant throughout all Update() calls in a given frame</summary>
		InFrameAfterLoad = 1,
		/// <summary>May be set to this value at any time during a frame</summary>
		WillSaveAfterNextFrame = 2,
		/// <summary>Constant throughout all Update() calls in a given frame</summary>
		WillSaveAfterCurrentFrame = 3,
		/// <summary>Constant throughout all Update() calls in a given frame</summary>
		InFrameAfterSave = 4,
	}

	static SaveHostMono()
	{
		SaveEquivReg.BuildReg();
	}

	public SaveHostMono()
	{
		//Note: Can't rely on the following working, as I don't know if Unity always calls the default constructor
		//This is therefore duplicated in Awake() (almost duplicated).

		//Set Main and check if should disable
		if (backingMain == null) {
			backingMain = this;
		} else if (ReferenceEquals(backingMain, this)) {
			//Do nothing
		} else {
			backingState = SavingState.Disabled;
			throw new SaveSetupException("Cannot have more than one SaveHostMono in the game. The SaveHostMono attached to GameObject '" + this.gameObject + "' could therefore not be created.");
		}
	}

	public void BlockIfDisabled()
	{
		if (backingState == SavingState.Disabled) {
			throw new SaveSetupException("SaveHostMonos are disabled, as there is more than one in the scene.");
		}
	}

	void Awake() {
		//Check if should disable and set Main
		if (backingMain == null) {
			backingMain = this;
		} else if (ReferenceEquals(backingMain, this)) {
			//Do nothing
		} else {
			backingState = SavingState.Disabled;
			throw new SaveSetupException("Cannot have more than one SaveHostMono in the game. The SaveHostMono attached to GameObject '" + this.gameObject + "' is therefore now disabled.");
		}

		backingState = SavingState.Other;
		SaveHost.RefreshSavePath();
		LoadHost.RefreshLoadPath();
		if (AutoLoad && LoadHost.LoadPath != SaveConstants.pathUnset)
		{
			LoadHost.LoadAll();
			backingState = SavingState.InFrameAfterLoad;
		} else {
			backingState = SavingState.RunningNormally;
		}
	}

	

	public void SaveAfterNextFrame()
	{
		BlockIfDisabled();

		backingState = SavingState.WillSaveAfterNextFrame;
	}

	//Might be used
	//?//	IEnumerator Update() {
	//?//		yield return new WaitForEndOfFrame(); //Maybe use OnApplicationPause() instead?
	//?//	}

	void LateUpdate()
	{
		BlockIfDisabled();

		switch (backingState)
		{
			case SavingState.WillSaveAfterNextFrame:
				backingState = SavingState.WillSaveAfterCurrentFrame;
				break;
			case SavingState.WillSaveAfterCurrentFrame:
				backingState = SavingState.Other;
				SaveHost.SaveAll();
				backingState = SavingState.RunningNormally;
				break;
			case SavingState.InFrameAfterLoad:
				backingState = SavingState.RunningNormally;
				break;
			case SavingState.InFrameAfterSave:
				backingState = SavingState.RunningNormally;
				break;
			default:
				break;
		}

		//Try to finish off multithreaded stuff
		if (SaveHost.isSaving) SaveHost.TryFinishSaving();
	}
}
