﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Save
{
	[DisallowMultipleComponent]
	[AddComponentMenu("Save/General Save Node")]
	[DeliberatelyNonSerializable]
	public class GeneralSaveNode : SaveNode<GeneralSaveNode.ComponentData>  {
		
		[HideInInspector] 
		[SerializeField]
		public List<GeneralSaveNodeComponentHandler> Components = new List<GeneralSaveNodeComponentHandler>();

		[System.Serializable]
		public class ComponentData : ComponentDataBase
		{
			public List<SerGeneralSaveNodeComponentHandler> Components = new List<SerGeneralSaveNodeComponentHandler>();

			public ComponentData() { }


		}


		public GeneralSaveNode() { }

		public virtual ComponentData GetDataFromAdditionalComponents(DelayedLogger logger) {
			return new ComponentData();
		}
		public virtual void CreateAdditionalComponentsFromData(ComponentData cdata, DelayedLogger logger) { }

		public sealed override ComponentData GetDataFromComponents(DelayedLogger logger)
		{
			ComponentData cdata = GetDataFromAdditionalComponents(logger);

			foreach (GeneralSaveNodeComponentHandler comp in this.Components)
			{
				if (ReferenceEquals(comp, null)) continue;
				if (comp.Behav == GeneralSaveNodeComponentHandler.Behaviour.Ignore) continue;
				cdata.Components.Add(new SerGeneralSaveNodeComponentHandler(comp));
			}

			return cdata;
		}

		public sealed override void CreateComponentsFromData(ComponentData cdata, DelayedLogger logger)
		{
			List<Type> loadedCompTypes = new List<Type>();
			foreach (SerGeneralSaveNodeComponentHandler loadedCompHandler in cdata.Components) {
				if (loadedCompHandler == null) continue;
				if (loadedCompHandler.Component == null) continue;
				Type loadedCompType = loadedCompHandler.Component.NonSerType;
				if (!loadedCompTypes.Contains(loadedCompType)) {
					loadedCompTypes.Add(loadedCompType);
				}
			}

			Dictionary<Type, Component[]> existingCompsByType = new Dictionary<Type, Component[]>();
			foreach (Type loadedCompType in loadedCompTypes) {
				existingCompsByType.Add(loadedCompType, this.gameObject.GetComponents(loadedCompType));
			}

			foreach (SerGeneralSaveNodeComponentHandler loadedCompHandler in cdata.Components)
			{
				if (loadedCompHandler == null) continue;
				if (loadedCompHandler.Component == null) {
					logger.Add(DelayedLogItem.CreateWarning("One of the loaded GeneralSaveNode's components was null, so could not be applied", this));
					continue;
				}

				switch (loadedCompHandler.Behav)
				{
					case GeneralSaveNodeComponentHandler.Behaviour.Ignore:
						break;

					case GeneralSaveNodeComponentHandler.Behaviour.SaveAndOverwrite: {
						Component[] existingOfType = existingCompsByType[loadedCompHandler.Component.NonSerType]; //Will be in the dict (no need to check)
						if (existingOfType != null && existingOfType.Length == 1) {
							//Set data on existing component
							loadedCompHandler.Component.CopyOverUntypedNonSer(existingOfType[0]);
							
							//Set up component handler for future saves
							GeneralSaveNodeComponentHandler newCompHandler = ScriptableObject.CreateInstance<GeneralSaveNodeComponentHandler>();
							newCompHandler.Component = existingOfType[0];
							newCompHandler.Behav = loadedCompHandler.Behav;
							this.Components.Add(newCompHandler);
						} else {
							goto case GeneralSaveNodeComponentHandler.Behaviour.SaveAndAdd;
						}
					} break;

					case GeneralSaveNodeComponentHandler.Behaviour.SaveAndAdd: {
						//Create new component and set its data
						Component newComp = this.gameObject.AddComponent(loadedCompHandler.Component.NonSerType);
						loadedCompHandler.Component.CopyOverUntypedNonSer(newComp);
						
						//Set up component handler for future saves
						GeneralSaveNodeComponentHandler newCompHandler = ScriptableObject.CreateInstance<GeneralSaveNodeComponentHandler>();
						newCompHandler.Component = newComp;
						newCompHandler.Behav = loadedCompHandler.Behav;
						this.Components.Add(newCompHandler);
					} break;
				}
			}

			CreateAdditionalComponentsFromData(cdata, logger);
		}
		
	}

}