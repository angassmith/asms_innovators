﻿using UnityEngine;
using System.Collections;

namespace Save
{
	[DisallowMultipleComponent]
	[AddComponentMenu("Save/Basic Save Node")]
	[DeliberatelyNonSerializable]
	public sealed class BasicSaveNode : SaveNode<BasicSaveNode.ComponentData> {

		[System.Serializable]
		public class ComponentData : ComponentDataBase
		{
			public ComponentData() { }
		}

		public BasicSaveNode() { }

		public override ComponentData GetDataFromComponents(DelayedLogger logger) { return new ComponentData(); }
		public override void CreateComponentsFromData(ComponentData data, DelayedLogger logger) { }
	}
}