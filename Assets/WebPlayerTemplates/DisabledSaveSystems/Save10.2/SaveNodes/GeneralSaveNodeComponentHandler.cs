﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Save
{

	[DeliberatelyNonSerializable]
	public class GeneralSaveNodeComponentHandler : ScriptableObject
	{
		public enum Behaviour {
			Ignore = 0,
			SaveAndAdd = 1,
			SaveAndOverwrite = 2,
		}

		[SerializeField]
		public Behaviour Behav = Behaviour.SaveAndOverwrite;

		[SerializeField]
		private Component _component;
		public Component Component {
			get { return _component; }
			set {
				if (value == null)
					this._component = null;

				else if (value.GetType() == typeof(Transform))
					throw new ArgumentException("Transforms are saved automatically, so cannot be added to GeneralSaveNodes (classes derived from Transform, however, can be)");

				else if (value is SaveNodeBase)
					throw new ArgumentException("SaveNodes cannot be added to GeneralSaveNodes");

				else if (!SaveEquivReg.SaveableEquivTypes.ContainsKey4(value.GetType()))
					throw new SaveImplementationException("The Component type '" + value.GetType() + "' has not been registered in SaveEquivReg, and so cannot be saved by a GeneralSaveNode.");

				else if (!SaveEquivReg.SaveableEquivTypes.GetValueByKey4(value.GetType()).Uses.HasFlagAndNonNegative(SaveEquivReg.Uses.UseableForGeneralSaveNode))
					throw new ArgumentException("The Component type '" + value.GetType() + "' has been registered in SaveEquivReg as not being usable in a GeneralSaveNode.");

				else this._component = value;
			}
		}


		public GeneralSaveNodeComponentHandler() { }
	}




	[System.Serializable]
	public class SerGeneralSaveNodeComponentHandler : SerScriptableObject<SerGeneralSaveNodeComponentHandler, GeneralSaveNodeComponentHandler>
	{
		public override SaveEquivReg.Entry<SerGeneralSaveNodeComponentHandler, GeneralSaveNodeComponentHandler> RegInfo {
			get {
				return null;
			}
		}

		public ISerComponent Component;
		public GeneralSaveNodeComponentHandler.Behaviour Behav = GeneralSaveNodeComponentHandler.Behaviour.SaveAndOverwrite;

		public SerGeneralSaveNodeComponentHandler() : base(VoidClass.Void) { }
		public SerGeneralSaveNodeComponentHandler(GeneralSaveNodeComponentHandler nonSerSource) : base(nonSerSource) { }

		protected override void CopyFromNonNullNonSerScriptableObject(GeneralSaveNodeComponentHandler nonSerSource) {
			this.Behav = nonSerSource.Behav;

			if (ReferenceEquals(nonSerSource.Component, null)) this.Component = null;
			else
			{
				SaveEquivReg.ClassStatus info;
				if (SaveEquivReg.SaveableEquivTypes.TryGetValueByKey4(nonSerSource.Component.GetType(), out info))
				{
					if (info.Uses.HasFlagAndNonNegative(SaveEquivReg.Uses.UseableForGeneralSaveNode)) {
						this.Component = (ISerComponent)info.Convert(nonSerSource.Component);
					} else {
						throw new SaveSetupException("Component type '" + nonSerSource.Component.GetType() + "' has been registered in SaveEquivReg as not being usable for GeneralSaveNode ComponentHandlers");
					}
				} else {
					throw new SaveImplementationException("Component type '" + nonSerSource.Component.GetType() + "' has not been registered in SaveEquivReg. This type cannot, therefore, be saved by GeneralSaveNodes");
				}
			}
		}
		protected override void CopyOverNonNullNonSerScriptableObject(GeneralSaveNodeComponentHandler nonSerDest) {
			nonSerDest.Behav = this.Behav;

			if (ReferenceEquals(this.Component, null)) nonSerDest.Component = null;
			else nonSerDest.Component = (Component)this.Component.CastToUntypedNonSer();
		}
		public override GeneralSaveNodeComponentHandler CreateBlankNonSer() {
			return new GeneralSaveNodeComponentHandler();  
		}
	}

}