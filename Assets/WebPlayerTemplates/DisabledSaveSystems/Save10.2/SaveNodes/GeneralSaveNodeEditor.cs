﻿#if UNITY_EDITOR

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;

namespace Save
{

	[CustomEditor(typeof(GeneralSaveNode), editorForChildClasses:true)]
	public class GeneralSaveNodeEditor : SaveNodeEditor
	{
		private GUIStyle thinButtonGUIStyle = null;
		private GUIStyle boldTextGUIStyle = null;

		public virtual void OnInspectorGUI3() { }

		public sealed override void OnInspectorGUI2()
		{
			if (thinButtonGUIStyle == null) thinButtonGUIStyle = new GUIStyle(GUI.skin.button) { fixedHeight = 14 };
			if (boldTextGUIStyle == null) boldTextGUIStyle = new GUIStyle() { fontStyle = FontStyle.Bold };
			
			//Don't call DrawDefaultInspector();
	
			GeneralSaveNode generalSNode = (GeneralSaveNode)target;

			GUILayout.Label("Components", boldTextGUIStyle);

			//Add and remove component handlers
			EditorGUILayout.BeginHorizontal();
			{	
				if (GUILayout.Button(new GUIContent("    Add    ", "Add a new ComponentHandler"), thinButtonGUIStyle)) {
					GeneralSaveNodeComponentHandler newComp = CreateInstance<GeneralSaveNodeComponentHandler>();
					generalSNode.Components.Add(newComp);
				}

				if (GUILayout.Button(new GUIContent("    Clear    ", "Remove all ComponentHandlers"), thinButtonGUIStyle)) {
					generalSNode.Components.Clear();
				}

				string[] componentHandlerIndexOptions = new string[generalSNode.Components.Count + 1];
				componentHandlerIndexOptions[0] = "Remove At";
				for (int j = 1; j < componentHandlerIndexOptions.Length; j++) { //Deliberately i = 1
					componentHandlerIndexOptions[j] = j.ToString();
				}
				int selectedToRemove = EditorGUILayout.Popup(0, componentHandlerIndexOptions);
				if (selectedToRemove > 0) {
					generalSNode.Components.RemoveAt(selectedToRemove - 1);
				}
			}
			EditorGUILayout.EndHorizontal();

			//Draw all component handlers
			int i = 0;
			Rect allPos = EditorGUILayout.BeginVertical();
			{

				foreach (GeneralSaveNodeComponentHandler comp in generalSNode.Components)
				{
					if (comp == null) continue;
					
					Rect rowPos = new Rect(allPos.x, allPos.y + (i*20), allPos.width, 17);

					const int behavOffset = 0;
					const int behavWidth = 75;
					const int compOffset = behavOffset + behavWidth + 5;
					const int compWidth = 35;
					const int typeOffset = compOffset + compWidth + 5;
					const int typeWidth = 2000;

					comp.Behav = (GeneralSaveNodeComponentHandler.Behaviour)EditorGUI.Popup(new Rect(rowPos.x + behavOffset, rowPos.y, behavWidth, rowPos.height), (int)comp.Behav, new string[] { "Ignore", "Save+Add", "Save+O.w." });
					
					Component newComponent = (Component)EditorGUI.ObjectField(new Rect(rowPos.x + compOffset, rowPos.y, compWidth, rowPos.height), comp.Component, typeof(Component), true);
					if (newComponent == null) comp.Component = null;
					else if (newComponent.gameObject != generalSNode.gameObject) EditorUtility.DisplayDialog("Invalid Component", "GeneralSaveNodes can only save components attached to the same GameObject", "Cancel");
					else comp.Component = newComponent;

					GUI.Label(new Rect(rowPos.x + typeOffset, rowPos.y, typeWidth, rowPos.height), comp.Component == null ? "none" : comp.Component.GetType().Name, new GUIStyle() { margin = new RectOffset(80, 0, 0, 0), alignment = TextAnchor.MiddleLeft, fixedWidth = 80, fixedHeight = 17 });

					i++;
				}
			}
			EditorGUILayout.EndVertical();

			//Make Unity take the manual editor layout into account (otherwise the editor is too short and it overflows)
			GUILayout.Box(new GUIContent(), new GUIStyle() { fixedHeight = i*20 });			

			OnInspectorGUI3();
		}
	}

}
	

#endif