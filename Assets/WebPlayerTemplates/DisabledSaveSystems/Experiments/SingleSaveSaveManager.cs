﻿/*

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace RevolitionGames.Saving
{
	public class SingleSaveSaveManager : SaveManager
	{
		public DirectoryInfo PlayModeStageSaveDir { get; private set; }
		public DirectoryInfo LikeabilitySaveDir { get; private set; }
		public DirectoryInfo NPCHostSaveDir { get; private set; }
		public DirectoryInfo StorySaveDir { get; private set; }

		public DirectoryInfo GetSceneSaveDir(string sceneName)
		{
			throw new NotImplementedException();
		}

		private SingleSaveSaveManager(DirectoryInfo rootSavePath)
		{
			if (rootSavePath == null) throw new ArgumentNullException("rootSavePath");
		}

		public SingleSaveSaveManager()
			: this(new DirectoryInfo(Path.Combine(Application.persistentDataPath, Path.Combine("RevolitionGames", "Save"))))
		{ }
	}
}

//*/