﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RevolutionGames.Saving
{

    /// <summary>
    /// Used to save and load data of individual objects
    /// </summary>
    public abstract class SavedBehavior : MonoBehaviour
    {
        /// <summary>
        /// Function called to save item to file
        /// </summary>
        public abstract System.Object Save();


        /// <summary>
        /// Function called to load item from file
        /// </summary>
        public abstract void Load(Dictionary<string, System.Object> p);

    }
}